# Begin ~/.profile
# Baseado Beyond Linux From Scratch-8.2
# Escrito por Unicode Tec <unicodetec@gmail.com>
# by 64728912024

if [ -d "$HOME/bin" ] ; then
  pathprepend $HOME/bin
fi

# Set up user specific i18n variables
#export LANG=<ll>_<CC>.<charmap><@modifiers>

# End ~/.profile
