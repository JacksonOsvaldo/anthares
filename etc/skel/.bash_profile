# Begin ~/.bash_profile
# Baseado Beyond Linux From Scratch-8.2
# Escrito por Unicode Tec <unicodetec@gmail.com>
# by 64728912024

if [ -f "$HOME/.bashrc" ] ; then
  source $HOME/.bashrc
fi

if [ -d "$HOME/bin" ] ; then
  pathprepend $HOME/bin
fi

# Having . in the PATH is dangerous
#if [ $EUID -gt 99 ]; then
#  pathappend .
#fi

# End ~/.bash_profile
