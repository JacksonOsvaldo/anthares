��          �      \      �  &   �  �   �          �     �     �     �     �      �     �     �          .     6     C     I  	   O  C   Y     �  �  �  &   i  `  �     �     �       #   2  "   V     y     �  5   �  "   �  I        K     j     �     �     �  �   �  6   Y	                                                        
                  	                         (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2006-01-04 12:57-0600
Last-Translator: Deepayan Sarkar <deepayan@bengalinux.org>
Language-Team: Bengali <kde-translation@bengalinux.org>
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>স্টাইল</h1>এই মডিউলটির সাহায্যে আপনি ইউসার ইন্টারফেস-এর বিভিন্ন উপাদানের (যেমন উইজেট স্টাইল এবং বিভিন্ন এফেক্ট) চেহারা বদলাতে পারেন। বাটন চেক-বক্স কম্বো-বক্স ক&নফিগার করো... %1 কনফিগার করো বর্ণনা: %1 deepayan@bengalinux.org কে.ডি.ই. স্টাইল মডিউল দীপায়ন সরকার কোন বর্ণনা পাওয়া যাচ্ছে না। প্রাকদর্শন রেডিও বাটন ট্যাব ১ ট্যাব ২ শুধু লেখা এই স্টাইল-এর কনফিগারেশন ডায়ালগ লোড করতে সমস্যা হয়েছে। ডায়ালগ লোড করা যায়নি 