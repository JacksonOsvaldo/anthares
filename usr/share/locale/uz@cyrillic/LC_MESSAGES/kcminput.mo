��          �   %   �      @     A  
   G     R     g  /   p  '   �      �  �   �     �     �  9   �     (     1  E   >     �      �     �     �     �     �     �               ,  �  :     �     �  #        (  B   6  I   y  +   �  r  �  !   b	     �	  U   �	     �	     
  v   '
  ;   �
     �
     �
       "        A     R  (   n     �     �                                                                
                                                   	            msec  pixel/sec &Acceleration delay: &General &Move pointer with keyboard (using the num pad) &Single-click to open files and folders (c) 1997 - 2005 Mouse developers <h1>Mouse</h1> This module allows you to choose various options for the way in which your pointing device works. Your pointing device may be a mouse, trackball, or some other hardware that performs a similar function. Acceleration &profile: Acceleration &time: Activates and opens a file or folder with a single click. Advanced Button Order Dou&ble-click to open files and folders (select icons on first click) Double click interval: EMAIL OF TRANSLATORSYour emails Icons Le&ft handed Ma&ximum speed: Mouse NAME OF TRANSLATORSYour names Pointer acceleration: R&epeat interval: Righ&t handed Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-05 03:17+0100
PO-Revision-Date: 2005-10-19 21:17+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
  мс  пиксел/сек &Тезланиш кечикиши: &Умумий &Сичқончани тугматаг билан бошқариш &Бир марта босиш файл ва жилдларни очади (C) 1997-2005, Mouse тузувчилари <h1>Сичқонча</h1> Бу модул ёрдамида кўрсатгич ускунасининг турли параметрларини танлашингиз мумкин. Кўрсатгич ускунаси сичқонча, трэкбол ёки шунга ўхшган аммалларни бажарувчи бошқа ускуна бўлиши мумкин. Тезланиш &профили: Тезланиш &вақти: Бир марта босиб файл ёки жилдни танлаш ва опиш. Қўшимча Тугмалар тартиби &Икки марта босиш файл ва жилдларни очади (бир марта - белгилайди) Икки марта босиш орасидаги вақт: kmashrab@uni-bremen.de Нишончалар Ча&п қўл учун &Энг катта тезлиги: Сичқонча Машраб Қуватов Курсорнинг тезланиши: &Қайтариш вақти: Ў&нг қўл учун 