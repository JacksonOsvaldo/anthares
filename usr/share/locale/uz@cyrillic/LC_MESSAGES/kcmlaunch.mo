��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  1       9  �  ?	  �   :  &   �  #        >  L   U     �  "   �  1   �  7                                             
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-10-11 21:21+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
  сония &Ишга тушириш учун таймаут: <H1>Вазифалар панелида хабарнома</H1>Бу усулда вазифалар панелида дастур ишга тушаётгани ҳақида хабар берувчи ҳаракатланувчи қум соати кўринади. Баъзи дастурларда бундай имконият йўқ бўлиши мумкин бу ҳолда, хабарнома "Ишга тушириш учун таймаут"да кўрсатилган вақтдан кейин йўқолади. <H1>Банд курсор</H1>Бу усулда сичқончанинг ёнида дастур ишга тушаётгани ҳақида хабар берувчи дастурнинг ҳаракатланувчи нишончаси кўринади. Баъзи дастурларда бундай имконият йўқ бўлиши мумкин бу ҳолда, хабарнома "Ишга тушириш учун таймаут"да кўрсатилган вақтдан кейин йўқолади. <h1>Дастур ишга тушиш хабарномаси</h1>Бу ерда дастур ишга тушиш ҳақида хабарнома усулини мослаш мумкин. Ўчиб-ёнадиган курсор Сакрайдиган курсор &Банд курсор Вазифалар панелида &хабарномани кўрсатиш Банд курсорсиз Пассив банд курсор Ишга тушириш учун &таймаут: &Вазифалар панелида хабарнома 