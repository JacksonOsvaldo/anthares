��          �      �       0  `   1     �     �     �      �     �                 ?   &  Y   f  +   �  �  �  h   �       2        J     Y     p     �  #   �  $   �  e   �  �   D  G   �           	          
                                     A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-10-19 21:17+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
 %1 мавзуси аллақачон мавжуд. Уни алмаштиришни истайсизми? Тасдиқлаш Курсор мосламалари ўзгарди Таърифи kmashrab@uni-bremen.de Машраб Қуватов Номи Мавзуни алмаштириш Мавзуни олиб ташлаш %1 файли ҳақиқий курсор мавзусининг архивига ўхшамайди. Курсор мавзусининг архивини ёзиб олиб бўлмади. Илтимос %1 манзили тўғрилигини текширинг. Курсор мавзусининг %1 архиви топилмади. 