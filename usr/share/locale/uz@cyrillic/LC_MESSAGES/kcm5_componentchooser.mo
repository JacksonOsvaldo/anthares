��          �   %   �      0     1  $   B  $   g     �     �  /   �  p   �  X   L  *   �  /   �                 $     E     d  )   }     �  &   �  =   �     +     3  "   R  2   u  �  �  $   b  b   �  T   �  :   ?     z  >   �  �   �  t   f	  U   �	  V   1
  -   �
     �
     �
     �
  '   	  A   1  K   s  J   �  y   
     �  H   �  G   �  *   &                                     
                                                                        	                  &Run in terminal &Use KMail as preferred email client &Use Konsole as terminal application (c), 2002 Joseph Wenninger ... <qt>Open <b>http</b> and <b>https</b> URLs</qt> Activate this option if you want the selected email client to be executed in a terminal (e.g. <em>Konsole</em>). Choose from the list below which component should be used by default for the %1 service. Click here to browse for terminal program. Click here to browse for the mail program file. Component Chooser Default Component EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names No description available Select preferred Web browser application: Select preferred email client: Select preferred terminal application: Select this option if you want to use any other mail program. Unknown Use a different &email client: Use a different &terminal program: in an application based on the contents of the URL Project-Id-Version: kcmcomponentchooser
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-11 07:06+0200
PO-Revision-Date: 2006-03-29 22:24+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.1
Plural-Forms: nplurals=1; plural=0;
 &Терминалда бажариш &Хат-хабар клиенти сифатида KMail дастуридан фойдаланиш &Терминал сифатида Konsole дастуридан фойдаланиш (C) 2002, Жозеф Веннингер (Joseph Wenninger) Танлаш <qt><b>HTTP</b> ва <b>HTTPS</b> URL'ларни очиш</qt> Танланган хат-хабар дастурини терминалда (м-н <em>Konsole</em>) ишга тушириш учун шуни танланг. Қуйидаги рўйхатдан "%1" хизмати учун андоза компонентни танланг. Терминал дастурини танлаш учун шу ерни босинг. Хат-хабар дастурини танлаш учун шу ерни босинг. Компонентларни танлагич Андоза компонент kmashrab@uni-bremen.de Машраб Қуватов Ҳеч қандай таъриф йўқ Афзал кўрган веб-браузерни танланг: Афзал кўрган хат-хабар клиентини танланг Афзал кўрган терминал дастурини танланг Бошқа хат-хабар дастуридан фойдаланмоқчи бўлсангиз, шуни танланг. Номаълум &Бошқа хат-хабар клиентидан фойдаланиш: &Бошқа терминал дастуридан фойдаланиш: мос келадиган дастурда 