��    (      \  5   �      p     q     z     �     �     �     �  -   �  r   �  	   g  	   q     {     �     �     �     �      �     �     �     �     �          5  	   :     D     J     R     _     m     �     �     �     �  +   �     �            S     )   k     �  �  �     P     a     p     �     �  0   �  Y   �  �   8	     �	     �	     
     
     $
     7
  <   F
  (   �
  %   �
  
   �
     �
  ;   �
  B   .     q     z  
   �     �  $   �     �  9   �  ;   +  B   g     �     �  K   �     $     2     C  �   a  O     )   \                            "         #             %              &                    
                   '                          $   !   (         	                                &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Co&lor: Colorize Confirmation Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Icons Icons Control Panel Module NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2006-12-16 15:28+0000
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
 &Миқдори: &Эффект: &Иккинчи ранг: Ярим ша&ффоф Ма&взу (c) 2000-2003 Герт Янсен (Geert Jansen) <qt><strong>%1</strong> нишончалар мавзуси ўрнатилмоқда</qt> Ўрнатиш давомида хато рўй берди. Шундай бўлса ҳам, архивдаги мавзулардан кўписи ўрнатилди Қўшимч&а Ҳамма нишончалар &Ранги: Бўяш Тасдиқлаш Таърифи Мавзунинг манзилини (URL) киритинг kmashrab@uni-bremen.de, mavnur@gmail.com Эффект параметрлари Гамма Нишончалар Нишончалар учун бошқарув модули Машраб Қуватов, Нурали Абдураҳмонов Номи Эффектсиз Панел Кўриб чиқиш Мавзуни олиб ташлаш Эффектни мослаш Актив нишонча эффектини мослаш Андоза нишонча эффектини мослаш Актив эмас нишонча эффектини мослаш Ўлчами: Кичик нишончалар Файл нишончалар мавзусининг архиви эмас. Оқ-қора Монохром Асбоблар панели Нишончалар мавзусининг архивини ёзиб олиб бўлмади.
Илтимос %1 манзили тўғрилигини текширинг. Нишончалар мавзусининг %1 архиви топилмади. Нишончадан фойдаланиш 