��          �      �       H     I  /   N     ~     �     �     �     �     �     �  "   �       6   0  *   g  �  �     A  '   H  -   p     �  X   �  I   �     E     R     o  J   s  D   �  o     K   s           	                                 
                      to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2006-12-17 12:09+0000
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
  то  Мутаносиб эмас шрифт. Ҳамма ш&рифтларни мослаш BGR Ҳамма шрифтларни ўзгартириш учун шу ерни босинг Шрифтларни текислаш мосламалари мослаш Мослаш Қўл&ланилмасин: RGB Меню ва контекст менюси учун ишлатилади. Ойнанинг сарлавҳаси учун ишлатилади. Оддий матн (м-н тугма ёзуви, рўйхат бандлари) учун ишлатилади. Нишончалар ёнидаги матн учун ишлатилади. 