��          �      |      �  &   �  �        �     �     �     �     �     �      �  c        h     y     �     �     �     �     �  	   �  C   �  j   !     �  �  �  `   Q  �   �  
   �     �     �     �     �            �   6     �     �  (   �     '	     =	     Y	     b	     k	  e   	  �   �	  &   n
                                                       
                 	                         (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2006-03-05 12:04+0100
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
 (c) 2002 Карол Сцвед (Karol Szwed), Даниэл Молкентин (Daniel Molkentin) <h1>Услуб</h1>Ушбу модул ёрдамида виджет услуби ва эффектларга ўхшаган фойдаланувчи интерфейсининг ташқи кўринишини мослаш мумкин. Тугма Белгилаш катаги Танлаш рўйхати &Мослаш... %1 мосламаси Таърифи: %1 kmashrab@uni-bremen.de Агар белгиланса, KDE дастурлари баъзи муҳим тугмаларда нишончаларни кўрсатади. KDE услуб модули Машраб Қуватов Ҳеч қандай таъриф йўқ. Кўриб чиқиш Танлаш тугмаси Таб 1 Таб 2 Фақат матн Ушбу услуб учун мослаш ойнасини юклашда хато рўй берди. Бу ерда танланган улубни қўлламасдан унинг ташқи кўринишини кўриш мумкин. Ойнани юклаб бўлмади 