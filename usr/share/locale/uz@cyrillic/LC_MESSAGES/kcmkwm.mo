��    4      �  G   \      x     y     }     �     �     �  )   �     �     �     �     �  	          4        I     R     a  0   i      �  a   �  c     _   �  a   �  `   C     �     �     �     �     �     �               +  
   <     G     L     [     d     i     �     �     �     �     �     �     �     �     �     �     �     	     	  �  '	     �
     �
     �
     �
  *     9   @     z  ,   �  .   �  
   �     �       N        _  +   p     �  �   �     /  �   F  �   $  �     �     �   �     �  -   �     *     F     Y      f  "   �     �      �     �     �     �  
   
       1   $     V     r     �     �     �     �  %   �               $     1  "   M     p         1       $                  0          .                )             2   (   
      4      	          ,       "       &          '      +                              3   /   !   *   #   %                              -                               ms &Focus &Moving &Placement: &Titlebar Actions (c) 1997 - 2002 KWin and KControl Authors Activate Activate & Lower Activate & Raise Active Ad&vanced Alt Behavior on <em>double</em> click into the titlebar. Centered Change Opacity Dela&y: Display window &geometry when moving or resizing EMAIL OF TRANSLATORSYour emails In this column you can customize mouse clicks into the titlebar or the frame of an active window. In this column you can customize mouse clicks into the titlebar or the frame of an inactive window. In this row you can customize left click behavior when clicking into the titlebar or the frame. In this row you can customize middle click behavior when clicking into the titlebar or the frame. In this row you can customize right click behavior when clicking into the titlebar or the frame. Inactive Inactive Inner Window Keep Above/Below Left button: Lower Maximize (horizontal only) Maximize (vertical only) Maximize Button Maximize/Restore Maximizing Meta Middle button: Minimize Move Move to Previous/Next Desktop NAME OF TRANSLATORSYour names Nothing Operations Menu Raise Raise/Lower Random Resize Right button: Shade Shading Toggle Raise & Lower Window Actio&ns Windows Project-Id-Version: kcmkwm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-05-30 17:40+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
  мс &Фокуслаш &Кўчириш &Жойлаштириш: &Сарлавҳанинг амаллари (C) 1997 - 2002 KWin ва KControl муаллифлари Активлаштириш Активлаштириш ва орқага Активлаштириш ва олдинга Актив &Қўшимча Alt Сарлавҳани <em>икки марта</em> босиш амаллари. Марказда Шаффофликни ўзгартириш К&ечикиш: Кўчиришда ёки ўлчамини ўзгартиришда ойнанинг &геометриясини кўрсатиш kmashrab@uni-bremen.de Бу ерда актив ойнанинг сарлавҳасини ёки чегарасини сичқонча билан босиш натижасида бажариладиган амални мослаш мумкин. Бу ерда актив бўлмаган ойнанинг сарлавҳасини ёки чегарасини сичқонча билан босиш натижасида бажариладиган амални мослаш мумкин. Бу ерда ойнанинг сарлавҳасини ёки чегарасини сичқончанинг чап тугмаси билан босиш натижасида бажариладиган амални мослаш мумкин. Бу ерда ойнанинг сарлавҳасини ёки чегарасини сичқончанинг ўрта тугмаси билан босиш натижасида бажариладиган амални мослаш мумкин. Бу ерда ойнанинг сарлавҳасини ёки чегарасини сичқончанинг ўнг тугмаси билан босиш натижасида бажариладиган амални мослаш мумкин. Актив эмас Актив бўлмаган ички ойна Юқорига/пастга Чап тугма: Орқага Ёйиш (фақат энига) Ёйиш (фақат бўйига) Ёйиш тугмаси Ёйиш/Қайта тиклаш Ёйиш Мета Ўрта тугма: Йиғиш Кўчириш Кейинги/олдинги иш столига Машраб Қуватов Ҳеч нарса Амаллар менюси Олдинга Юқорига/Пастга Тасодифий Ўлчамини ўзгартириш Ўнг тугма: Соялаш Соялаш Олдинга/орқага Ойнанинг &амаллари Ойналар 