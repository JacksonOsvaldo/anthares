��    !      $  /   ,      �     �            +         A     b  ;   x     �     �     �     �           ?  9   M     �  3   �  	   �     �  )   �  |   	  5   �  *   �     �                     -  +   J  (   v     �  n   �  3   .  �  b     	     0	  	   G	  0   Q	     �	     �	  I   �	     
      
     8
  2   V
     �
     �
  7   �
     �
  N   �
     E     M  #   b  �   �  ?     =   L     �     �     �     �     �  (   �  .   �  #   ,  �   P  5   �                          !      
                                                       	                                                                        %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: If you already have an emoticon theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons Start a new theme by assigning it a name. Then use the Add button on the right to add emoticons to this theme. This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2008-12-27 01:50+0200
Last-Translator: Omer Ensari <oensari@gmail.com>
Language-Team: Kurdish <ku@li.org>
Language: ku
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: Kurdish
 Dirba %1 jixwe heye Vegotin Lê Zêde Bike Têxê... Cureyê dirba vegotinê hilbijêre û biafirîne Nikare Dirba Vegotinê Saz Bike Vegotina nû biafirîne Vegotina nû biafirîne bi vê re îkon û hinek nivîsar zêdekirinê ve Vegotinê jê bibe Dirba vegotinekê nû biafirîne Tu dixwazî %1 jî jê bibî? URL ya Dirba Vegotinê Bikişîne an jî Binivîse oensari@gmail.com Vegotin Biguherîne Îkon an jî nivîsara vegotina hilbijartî biguherîne Biguherîne... Dirbên vegotinê pêwist e jê pelên di komputera te de hene bên saz kirin. Vegotin Rêveberê Vegotinan Navê dirba vegotina nû binivîse: Heke tu jixwe di komputera te de arşîva dirba vegotinekê hebe, ev bişkojk ê arşîvê veke û vê ji bo sepanên KDE amade bike. Pela arşîva dirbê yê jixwe di komputera te de heye saz bike Îkona an jî nivîsara vegotinê yê hilbijartî biguherîne Omer Ensari Dirba Vegotina Nû Rake Dirbî Rake Vegotina hilbijartî rake Vegotina hilbijartî jê dîska xwe rake Dirbên hatine hilbijartin jê dîska xwe bibe Cihê hewce ji bo derdora vegotinan Bi nav danê ve destpê dirba nû bike. Paşê pêl bişkojka Têxê yê di milê rastê de ye bike û vegotinan lê vê dirbê zêde bike. Ev wê dirbên hatine hilbijartin jê dîska te bibe. 