��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  6  �     4     <     S     e  	   r     |     �  "   �     �  !   �  +   �  4   *  "   _  '   �     �     �     �  $   �     �     �     �     	     5	     :	  
   X	     c	  
   {	  ,   �	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-08-13 15:45+0200
Last-Translator: Erdal Ronahî <erdal.ronahi@nospam.gmail.com>
Language-Team: Kurdish Team http://pckurd.net
Language: ku
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.6.1
X-Launchpad-Export-Date: 2007-11-26 09:44+0000
X-Poedit-Language: Kurdish
X-Poedit-Country: Kurdistan
X-Poedit-SourceCharset: utf-8
 Giştî Skrîpteke nû têxê. Lê zêde bike... Betal bîke? Şîrove: erdal.ronahi@nospam.gmail.com Sererastkirin Skrîpta nîşankirî serast bike. Sererastkirin... Skrîpta hilbijartî bixebitîne. Ji bo ravekara "%1" skrîpt nehat afirandin Ji bo pelê skrîpt ê "%1" ravekar nehat diyarkirin Barkirina Ravekara "%1" biserneket Vekirina pelê skrîpta "%1" biserneket Pel: Sembol: Şirovekar: Ji bo ravekara Ruby asta ewlekariyê Erdal Ronahi Nav: Foksiyona bi navê "%1" tune Ravekareke bi navê "%1" tune Rake Skrîptên nîşankirî rake. Xebitandin Pelê Skrîpta %1 tune. Rawestîne Xebitandina skrîpta hilbijartî rawestîne. Nivîs: 