��            )   �      �  &   �  �   �     ?     W     ^     g     p     ~     �     �      �  	   �  �   �  c   �               !     @     Z     b     o     {     �  	   �  C   �  j   �     @     V  �  d  &   0  �   W     �     �     �  	   	     	     ,	     ;	     L	     Z	     l	  �   y	  z   V
     �
     �
     �
     �
               ,     C     S     b  Q   o  a   �     #     ;            	                                                                                 
                                             (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2008-12-28 07:31+0200
Last-Translator: Omer Ensari <oensari@gmail.com>
Language-Team: Kurdish <ubuntu-l10n-kur@lists.ubuntu.com>
Language: ku
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: Kurdish
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Teşe</h1>Ev modûl dihêle hûn xuyakirina hêmanên bikarhêner yên navrû, yên wekî teşeyê alavê û efektan, biguherînin. Sazkirina &Bikitekit Bişkojk Qutiya Nîşankirinê Pir-Qutî Vea&vakirin... Veavakirina %1 Daniel Molkentin Daxuyanî: %1 oensari@gmail.com Qutiya Komê Li vir tu dikarî jê lîsteyê alavên pêşdanasî hilbijêrî, (mînak, bişkojkên nexşe kirî) yên di be ku bi dirbekê re girêdayî bin an jî na (agahiyên zêdetir wekî nîgarê mermerê an jî rengên sade). Heke hûn vê bijare yê çalak bikin, Sepanên KDE dê îkonên biçûk ber rexê hinek bişkojkên girîng nîşan bide. KDE Modûla Teşeyê  Karol Szwed Omer Ensari Danasîn tuneye. Pêşdîtin Bişkojka radyo (C) 2008 Ralf Habacker Hilpekîna 1em  Hilpekîna 2em Tenê Nivîs Di barkirina pace ya veavakirinê de yê ji bo vê teşeyê çewtiyek çê bû.   Ev qad pêşdîtina teşe yên hilbijartî nîşan dide, bê sepandina vê jê hemû sermasê re. Nikare Paceyê Bar Bike Teşeyê alavê: 