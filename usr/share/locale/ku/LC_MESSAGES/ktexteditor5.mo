��    S      �  q   L             
        #     )     <  
   Q  
   \     g     t     |     �     �     �     �     �     �     �     �  	   �     �  
   �  	   �       H        Z     c     k  
   t          �  )   �     �     �     �     	     	     +	     4	     <	     I	     U	     g	     v	  	   	  
   �	     �	  $   �	  F   �	  1   
  5   9
  	   o
  	   y
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
     	  &        8     E     V  &   g  %   �  	   �     �  >   �          1     G  
   S     ^     c  
   u     �  	   �     �  �  �     �     �     �  	   �     �  
   �     �     �       	                  $     9  
   A     L     `     p     u  	   �     �  	   �  
   �  A   �     �       	             $     )  #   D     h  !   �     �     �  !   �     �     �               ,     >     P     V     _     q  3   }  _   �  1     /   C     s  	   �     �     �     �     �     �     �  #   �     �          #     2  *   K     v  %   �  &   �  =   �  6        R     c  ?        �     �     �          -     2     B     T     [     i     6   Q          :   	   J   &       L                 E   D       "      4   8      C   $   R          P                         M   ;             (              /      *   S              I       G             0      2      K   5   ,      <   7      F   B           O          >   9      +         H                  !          =       3   ?   @   1           #   
   '   N   .   %   A   )                 -               &Align &Always On &Bold &Clean Indentation &Configure Editor... &Encoding: &Filetype: &Go to line: &Indent &Italic &New &New... &Next: %1 - "%2" &Off &Prefix: &Previous: %1 - "%2" &Read Only Mode &Schema &Section: &Suffix: &Underline &Unindent &Variables: A file named "%1" already exists. Are you sure you want to overwrite it? Bookmark Borders C&omment Capitalize Colors Command "%1" failed. Configure various aspects of this editor. Copy as &HTML Could not access view Create a new file type. DOS/Windows Delete the current file type. Disabled Editing Enlarge Font File Format File e&xtensions: Fonts & Colors Function Installed Join Lines Latest Lock/unlock the document for writing Look up the first occurrence of a piece of text or regular expression. Look up the next occurrence of the search phrase. Look up the previous occurrence of the search phrase. Lowercase Macintosh Name Name: New Filetype No such command: "%1" Normal Text Off Overwrite File? Print the current document. Properties Properties of %1 Reloa&d Reload the current document from disk. Replace &All Reuse Word Above Reuse Word Below Revert the most recent editing actions Revert the most recent undo operation Save File Save the current document Save the current document to disk, with a name of your choice. Select to End of Document Select to End of Line Shrink Font Spellcheck UNIX Unable to open %1 Unco&mment Untitled Uppercase What do you want to do? Project-Id-Version: kdelibs
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-31 03:12+0100
PO-Revision-Date: 2008-09-22 02:29+0300
Last-Translator: Erdal Ronahi <erdal.ronahi@nospam.gmail.com>
Language-Team: Kurdish <ubuntu-l10n-kur@lists.ubuntu.com>
Language: ku
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Launchpad-Export-Date: 2008-09-22 00:08+0000
X-Generator: Lokalize 0.2
 &Bike texmek &Her dem vekirî &Stûr &Zik rake &Pêvekan bipergalîne... &Kodkirin: Cureyê &Pel: &Biçe Rêzikê &Zik &Îtalîk &Nû &Nû... P&êş de: %1 - "%2" &Girtî &Pêşgir: P&aş de: %1 - "%2" Moda &Xwînerî Dirb &Hilbijartin: P&aşgir: &Binîxêzkirî &Zik rake &Guherbar: Pelê bi navê "%1" jixwe heye. Tu dixwazî li ser wê binivîse? Nîşana cih Kêlek Şîr&ove Tîpan Mezin Bike Reng Fermana "%1" bi ser neket. Gelek dîtinên pergalker ava bike. Wekî &HTML ji ber bigire Pêwendî bi dîtinê re çênabe Cureyekî nû yê pelan çêke DOS/Windows Cureyekî pelan ê heyî jê bibe Neçalak Sererastkirin Curetîpan Mezin Bike Formata Pelî Dûçikên pelan: Curenivîs & Reng Kirin Sazkirî Rêzikan Bike Yek Herî Dawî Bila li pelgeyê neyê nivîsandin/were nivîsandin Bêje an jî derbirîneke bi pergal a hatiye têketin cara pêşî li ku derbas bûye, bibîne. Cihê piştre yê derbirîna lêgerandî bibîne. Cihê derbirîna lêgerandî ya paşde bibîne. Tîpên Biçûk Macintosh Nav Nav: Cureyekî nû yê pelan Ev femran tune: "%1" Deqa Asayî Girtî Bila Li Ser Pelî Were Nivîsandin? Pelgeya Çalak Bide Çapê. Taybetmendî Taybetiyên %1 &Ji nû ve lê bara bike Pelgeya çalak ji dîskê dubare bixwîne. Bila &Hemû Were Guhartin Bêjeya Jorîn Ji Nû ve Bi Kar Bîne Bêjeya Jêrîn Ji Nû ve Bi Kar Bîne Çalakiyên pergalkirinê yên herî dawî paşde vedigerîne Kirinên Paşde Vegerîne yên dawî paşde vegerîne. Pelî Tomar Bike Pelgeya dixebite tomar bike Pelgeya çalak bi navekî te hilbijartiye li dîskê tomar bike Heta Dawiya Belgeyê Hilbijêre Heta Dawiya Rêzikê Hilbijêre Curetîpan Biçûk Bike Kontrola rastnivîsê UNIX Nikarî %1 veke Şî&roveyê rake Bênav Tîpên Mezin Hûn dixwazin çi bikin? 