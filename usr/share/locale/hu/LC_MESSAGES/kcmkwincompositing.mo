��    0      �  C         (  6   )  M   `  K   �     �          
       	   2  A   <  >   ~  9   �  9   �  9   1  W   k  E   �     	                 A  7   ^      �     �  X   �     %     -     C  �   [     �     	     	     0	  
   @	  
   K	     V	     t	  E  �	     �
     �
     	  w        �     �     �     �     �  	   �     �  �  �  M   �  �   �  �   w     �                     5     A     V     b     z  
   �  0   �     �     �     �  (   �  $     8   4  $   m      �  S   �            %   #  �   I          /  !   4     V  
   e  
   p     {       Q  �  &   �  #   �        �   2     �     �     �     �     �                               	      &                     *   !          .       +       (                       %                ,   "   0                                                 $       #           /   )   -   
                '            "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Always Animation speed: Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-02-24 22:17+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 A „Teljes színtér újrarajzolása” teljesítményproblémákat okozhat. A „Csak ha olcsó” csak a teljes képernyős alkalmazásoknál, például videolejátszásnál akadályozza meg a kép szétesését. A „Képernyőtartalom újrahasználata” komoly teljesítményproblémákat okozhat MESA illesztőprogram használata esetén. Pontos Mindig Animálási sebesség: Szerző: %1
Licenc: %2 Automatikus Akadálymentesítés Megjelenés Grafikai kiegészítők Fókusz Eszközök Virtuális asztalok közti váltás animációja Ablakkezelés Szűrőbeállítás Gyors ulysses@kubuntu.org,tszanto@interware.hu Kompozitor bekapcsolása induláskor A kompozitor által nem támogatot effektusok kizárása Belső asztali effektusok kizárása Teljes színtér újrarajzolása Tipp: Az effektusok megértéséhez tekintse át azok beállítási lehetőségeit. Azonnali A KWin fejlesztői Ablakok bélyegképének megtartása: Az ablakok bélyegképének megtartása kölcsönhatásba léphet az ablak minimalizált állapotával. Ez okozhatja azt, hogy az ablak nem függeszti fel a működését minimalizált állapotban. Kiszel Kristóf,Szántó Tamás Soha Csak a megjelenített ablakokról Csak ha olcsó OpenGL 2.0 OpenGL 3.1 EGL GLX Az (alapértelmezett) OpenGL a KWin összeomlását okozta régebben.
Ezt valószínűleg az illesztőprogram hibája okozta.
Ha úgy gondolja, hogy időközben egy stabil illesztőprogramra frissített,
eltávolíthatja ezt a védelmet, ez azonban azonnali összeomlást is okozhat!
Alternatívaként használhatja az XRender modult is. OpenGL felismerés újra bekapcsolása Képernyőtartalom újrahasználata Renderelő modul: A „Pontos” nagyítási módot nem minden hardver támogatja, ezért teljesítményproblémákat és renderelési hibákat okozhat. Nagyítási mód: Keresés Sima Sima (lassabb) Szakadás megelőzés (VSync): Nagyon lassú XRender 