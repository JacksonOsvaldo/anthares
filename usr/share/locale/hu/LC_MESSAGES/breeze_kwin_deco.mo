��    B      ,  Y   <      �     �     �     �  !   �  "   �  &     ,   /  #   \  &   �  !   �  &   �  '   �  "     "   ;  '   ^     �  +   �  2   �     �  
   �     
          %     ,     @     H     O     b     {  !   �  +   �     �     �      �     	     (	     F	     U	     ]	  !   s	     �	  	   �	     �	     �	     �	     �	  &   �	     !
     @
     G
     b
     h
     p
     w
     |
     �
  $   �
     �
     �
     �
     �
          (     5  >   O  �  �     6     :      <     ]     f     k     w     �     �     �     �     �     �     �     �     �  G   �  E   /     u     �     �     �  	   �      �  
   �     �  )   �           @  (   Q  5   z  (   �     �  .   �       &   1     X     g     s     �     �     �     �  $   �      �       ,     (   D     m  $   |     �  
   �     �     �     �     �  0   �       '   $     L     _     k  	   �      �     �     '             6      8             $       %   <   ,      B                     "   >       #              	   @      =   7   3                          +      
   &   !               0   :      ;           (   4              2                      A       -   )      9          *         1       /          .   ?   5         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-01-02 22:12+0100
Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  ms % Ablaktulajdonság ill&esztése:  Hatalmas Nagy Nincs keret Nincsenek oldalszegélyek Normál Túlméretezett Apró Óriási Nagyon nagy Nagy Kicsi Nagyon nagy Hozzáadás Fogantyú hozzáadása a szegély nélküli ablakok átméretezéséhez A maximalizált ablakok legyenek átméretezhetőek az ablakszéleken &Animációk időtartama: Animációk &Gombméret: Szegélyméret: Középre Középre, teljes szélességgel Osztály:  Szín: Ablakkeret-megjelenítési beállítások Ablaktulajdonságok felismerése Párbeszédablak Kör rajzolása a bezárás gomb körül Elválasztó rajzolása a címsor és az ablak közé Színátmenetes ablakháttér rajzolása Szerkesztés Kivétel szerkesztése - Breeze beállítások Animációk engedélyezése Ezen kivétel engedélyezése/tiltása Kivételtípus Általános Ablak címsorának elrejtése A kijelölt ablak jellemzői Balra Le Fel Új kivétel - Breeze beállítások Kérdés - Breeze beállítások Reguláris kifejezés A reguláris kifejezés szintaxisa helytelen Az illesz&tendő reguláris kifejezés:  Eltávolítás Eltávolítja a kijelölt kivételt? Jobbra Árnyékok Mé&ret: Apró &Címsorigazítás: Cím:  Ablakosztály használata (a teljes alkalmazás) Ablakcím használata Figyelmeztetés - Breeze beállítások Ablakosztály neve Azonosító Ablaktulajdonság kijelölése Ablakcím Ablakspecifikus felülbírálás E&rősség: 