��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �  $   k     �     �  .   �     �     �  '   �  %     "   <  	   _     i  /   m     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-02-03 15:14+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Képernyő zárolása ébredéskor: Aktiválás Hiba Nem sikerült kipróbálni a képernyőzárat. Azonnal Munkamenet zárolása Képernyő zárolása ennyi idő után: Képernyő zárolása felébredéskor Jelszó ké&rése zárolás után: perc perc s s A képernyőzároló globális gyorsbillentyű. Háttérkép &típusa: 