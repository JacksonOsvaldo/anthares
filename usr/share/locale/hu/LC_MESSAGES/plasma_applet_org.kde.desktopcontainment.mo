��    R      �  m   <      �     �     �     
               &     2     C     Q     Y  /   a  -   �     �  
   �     �     �     �     �     �            
             (     7     O     b     n     u     �     �  	   �     �     �     �  	   �     �     �     �     �     �     �     	     	     	     .	     3	     8	  <   ;	     x	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     $
     8
  )   F
     p
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                  E   1  �  w  	   &     0     C     [     g     u     �     �     �     �     �     �  
   �                    "     +     @     M     T  
   e     p     �     �     �  	   �     �     �     �               (  !   /     Q  
   Z     e     l     q     w     }     �     �     �     �     �     �     �  P   �          9     S     b     q     �     �     �     �     �     �     �     �           %  ?   ;     {  '   �     �     �     �  	   �  
   �     �     �     �               "     .     ;  [   K                J   :   R   .      E       M      B       O   ?                3   #       7       8          L   )         9   I   F   ;   *      2   5              N          C       "   K   
      +            Q      %                    	       4                 ,   H   '   G           >   <   =          (   -           1      D   /       @   0   &   A          !   $   6           P                     &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-02-12 13:22+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Törlés A Kuka ü&rítése &Áthelyezés a Kukába Megny&itás &Beillesztés &Tulajdonságok Az asztal &frissítése A né&zet frissítése Új&ratöltés Átne&vezés Választás… Ikon visszaállítása Igazítás Elrendezés ebben Vissza Mégsem Oszlopok Asztal beállítása Egyéni cím Dátum Alapértelmezés Csökkenő A kijelölés megszüntetése Asztali elrendezés Itt adjon meg egyéni címet Fájlnévminta: Típusok: Szűrő Felugró előnézet Mappák előre Mappák előre Teljes elérési út Értem Az illeszkedő fájlok elrejtése Hatalmas Ikonméret Ikonok Nagy Balra Lista Hely Rögzítés Zárolt Közepes Több előnézetbeállítás… Név Nincs OK Elemek lenyomása és tartása a mozgatáshoz és a kezelők megjelenítéséhez Bővítmények előnézete Bélyegképek előnézete Eltávolítás Átméretezés Visszaállítás Jobbra Elforgatás Sorok Fájltípus… Minden kijelölése Mappa kiválasztása Kijelölés módja Összes fájl megjelenítése Az illeszkedő fájlok mutatása Hely megjelenítése: Az aktuális aktivitáshoz kapcsolódó fájlok megjelenítése Az asztali mappa mutatása Asztali eszközkészlet megjelenítése Méret Kicsi Kicsi Rendezés Rendezés: Mappa: Apró Finomhangolások Típus Elérési út vagy URL Rendezetlen Elemkezelés Elemek feloldva Lenyomhatja és tarthatja az elemeket a mozgatásukhoz és a kezelőik megjelenítéséhez. 