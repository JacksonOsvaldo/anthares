��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     O     W     [  	   r  �   |  ;        I     V  &   p  $   �  "   �  
   �     �     �     	     $	     2	     ?	  	   O	     Y	     q	  1   �	  5   �	  .   �	     $
     9
     H
  	   e
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-03-27 14:10+0100
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 (%2) %1: (C) Red Hat Inc., 2009 Művelet: Egy alkalmazás olyan műveletet próbál végrehajtani, ami jogosultságokat igényel. Hitelesítés szükséges a művelet végrehajtásához. Egy másik kliens már hitelesít, próbálja meg később. Alkalmazás: Hitelesítés szükséges Hitelesítési hiba, próbálja újra. Kattintson a(z) %1 szerkesztéséhez Kattintson a(z) %1 megnyitásához Részletek ulysses@kubuntu.org Korábbi karbantartó Jaroslav Reznik Lukáš Tinkl Karbantartó Kiszel Kristóf &Jelszó: Adja meg %1 jelszavát: Adja meg a root jelszavát: Adja meg %1 jelszavát, vagy húzza le az ujját: Adja meg a root jelszavát, vagy húzza le az ujját: Adja meg a jelszót, vagy húzza le az ujját: Adja meg a jelszót: PolicyKit1-KDE Felhasználó kiválasztása Gyártó: 