��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �  !   �  �  �     S     d  /   t  -   �     �     �     �               /         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: KDE 4.1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-12-06 15:45-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 (c) A KDE fejlesztői, 2002-2006. <h1>Rendszerüzenetek</h1>A Plasmaban könnyen beállíthatja, hogy a különféle eseményekről hogyan szeretne értesülni. Az értesítés módja a következők közül választható:<ul><li>Ahogy az alkalmazásban eredetileg szerepelt.</li><li>Hangjelzéssel.</li><li>Szöveges üzenetablakkal, mely további információkat tartalmaz.</li><li>Naplófájlba írás ablak és hangjelzés nélkül.</li></ul> Carsten Pfeiffer Charles Samuels Hangok kikapcsolása ezeknél az eseményeknél kiszel.kristof@gmail.com,tszanto@interware.hu Az üzenet forrása: KNotify Kiszel Kristóf,Szántó Tamás Olivier Goffart Első változat Rendszerüzenet vezérlőmodul 