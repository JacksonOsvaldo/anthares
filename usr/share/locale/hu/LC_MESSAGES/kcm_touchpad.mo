��    ;      �  O   �        %   	     /     <  #   L     p  "   �     �  !   �     �     �          *     :  C   U  	   �     �     �      �  !   �  #   �     #  "   C     f  e   v  W   �  e   4     �     �     �     �  (   	     0	     O	  u   l	     �	  B   �	  
   5
  	   @
     J
     i
  1   �
     �
     �
     �
     �
     �
        !   8     Z     r     �     �     �     �  1   �  0        O     ]  �  t           =     J  6   Z  &   �  3   �     �  #   �          >     W  
   l     w  J   �  
   �     �  
     &        7     @     P  	   _     i  o   �  �   �  f   �     �     �  	          C   '     k     x  �   �       Q     
   q  
   |     �     �  <   �     �     �     �  
     	   !     +     ;     L     [  
   k     v     �     �  E   �  O   �     O     ]     "         &   ,          
                   6               .      :   1   *   2                 4   (           !          #          /                -                    0   )         5   $             ;            7   8              '           +   3          	   %   9           @title:windowEnable/Disable Touchpad Alex Fiestas Alexander Mezin Cannot apply touchpad configuration Cannot connect to X server Cannot read touchpad configuration Click me Copyright © 2013 Alexander Mezin Copyright © 2016 Roman Gilg CreditsDeveloper of synclient CreditsHelped a bit CreditsTesting CreditsUsability, testing Critical error on reading fundamental device infos for touchpad %1. Developer Disable Touchpad Drag me EMAIL OF TRANSLATORSYour emails Emulated mouse buttonLeft button Emulated mouse buttonMiddle button Emulated mouse buttonNo action Emulated mouse buttonRight button Enable Touchpad Error while adding newly connected device. Please reconnect it and restart this configuration module. Error while loading default values. Failed to set some options to their default values. Error while loading values. See logs for more informations. Please restart this configuration module. Mouse buttonLeft button Mouse buttonMiddle button Mouse buttonRight button NAME OF TRANSLATORSYour names No touchpad found. Connect touchpad now. Noise cancellationHorizontal: Noise cancellationVertical: Not able to save all changes. See logs for more informations. Please restart this configuration module and try again. Peter Osterlund Querying input devices failed. Please reopen this settings module. Roman Gilg Scrolling Scrolling directionHorizontal Scrolling directionVertical System Settings module for managing your touchpad Testing area Thomas Pfeiffer Toggle Touchpad Touchpad EdgeAll edges Touchpad EdgeBottom edge Touchpad EdgeBottom left corner Touchpad EdgeBottom right corner Touchpad EdgeLeft edge Touchpad EdgeRight edge Touchpad EdgeTop edge Touchpad EdgeTop left corner Touchpad EdgeTop right corner Touchpad KCM Touchpad disconnected. Closed its setting dialog. Touchpad disconnected. No other touchpads found. Vadim Zaytsev Violetta Raspryagayeva Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-10 05:48+0100
PO-Revision-Date: 2017-12-11 13:21-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Érintőtábla be-/kikapcsolása Alex Fiestas Alexander Mezin Nem lehet alkalmazni az érintőtábla beállításait Nem lehet kapcsolódni az X szerverhez Nem lehet olvasni az érintőtábla beállításait Kattintson ide Copyright © Alexander Mezin, 2013. Copyright © Roman Gilq, 2016. A synclient fejlesztője Segített egy kicsit Tesztelés Használhatóság, tesztelés Kritikus hiba a(z) %1 érintőtábla eszközinformációinak olvasásakor. Fejlesztő Érintőtábla letiltása Húzza ezt ulysses@kubuntu.org,urbalazs@gmail.com Bal gomb Középső gomb Nincs művelet Jobb gomb Érintőtábla engedélyezése Hiba az újonnan csatlakoztatott eszköz hozzáadásakor. Csatlakoztassa újra és indítsa újra ezt a modult. Hiba az alapértelmezett értékek betöltésekor. Nem mindegyik vagy egyik beállítás sem állítható az alapértelmezett értékre. Hiba az értékek betöltésekor. Lásd a naplót további információkért. Indítsa újra a modult. Bal gomb Középső gomb Jobb gomb Kiszel Kristóf,Úr Balázs Nem található érintőtábla. Csatlakoztassa az érintőtáblát. Vízszintes: Függőleges: Nem lehet minden módosítást menteni. Lásd a naplót további információkért. Indítsa újra a modult, és próbálja meg újból. Peter Osterlund Nem sikerült lekérdezni a beviteli eszközöket. Nyissa meg újra ezt a modult. Roman Gilg Görgetés Vízszintes Függőleges Rendszerbeállítások modul az érintőtábla kezeléséhez Tesztterület Thomas Pfeiffer Érintőtábla kapcsolása Minden él Alsó él Bal alsó sarok Jobb alsó sarok Bal oldali él Jobb oldali él Felső él Bal felső sarok Jobb felső sarok Érintőtábla beállítómodul Az értintőtábla nincs csatlakoztatva. A beállítóablak bezárva. Az értintőtábla nincs csatlakoztatva. Nem található másik érintőtábla. Vadim Zaytsev Violetta Raspryagayeva 