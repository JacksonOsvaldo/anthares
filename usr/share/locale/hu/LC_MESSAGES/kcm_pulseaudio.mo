��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �     �     	  $   	     @	     ^	     |	  
   �	  
   �	     �	  
   �	  _   �	  !   
  O   5
  	   �
     �
     �
     �
  	   �
     �
     �
     �
  	                       1  5   H  D   ~     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-12-10 08:56-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 100% Szerző Copyright Harald Sitter, 2015. Harald Sitter Nincs hanglejátszás Nincs hangfelvétel Nincsenek elérhető eszközprofilok Nincsenek bemeneti eszközök Nincsenek kimeneti eszközök Profil: PulseAudio Speciális Alkalmazások Eszközök Virtuális kimeneti eszköz hozzáadása minden helyi hangkártyához szimultán lejátszáshoz Speciális kimenet-beállítások Lejátszások automatikus átirányítása, ha új kimenet válik elérhetővé Felvétel Alapértelmezett Eszközprofilok ulysses@kubuntu.org Bemenetek Hang némítása Kiszel Kristóf Értesítési hangok Kimenetek Lejátszás Port (nem érhető el) (nincs csatlakoztatva) A „module-qconf” PulseAudio modulra van szükség Ez a modul lehetővé teszi a Pulseaudio alrendszer beállítását. %1: %2 100% %1% 