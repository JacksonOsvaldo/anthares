��          �      ,      �  V  �     �  -   �     &     4     H     V     c     }     �     �     �     �     �  !   �  !     �  4  @  �  &        A     J  $   Y     ~     �  (   �  #   �     �            %   *  !   P     r     {                           
                       	                            <qt>Specifies how Jovie should speak the event when received.  If you select "Speak custom text", enter the text in the box.  You may use the following substitution strings in the text:<dl><dt>%e</dt><dd>Name of the event</dd><dt>%a</dt><dd>Application that sent the event</dd><dt>%m</dt><dd>The message sent by the application</dd></dl></qt> Configure Notifications Description of the notified eventDescription Log to a file Mark &taskbar entry Play a &sound Run &command Select the command to run Select the sound to play Show a message in a &popup Sp&eech Speak Custom Text Speak Event Message Speak Event Name State of the notified eventState Title of the notified eventTitle Project-Id-Version: KDE 4.4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-10-23 11:23+0200
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 <qt>Az esemény felolvasási módját lehet itt megadni. Jelölje ki „Adott szöveg felolvasása” opciót, majd írja be a szöveget. A következő helyettesítő szimbólumokat lehet használni a szövegben:<dl><dt>%e</dt><dd>Esemény</dd><dt>%a</dt><dd>Küldő alkalmazás</dd><dt>%m</dt><dd>Üzenet</dd></dl></qt> Az értesítő üzenetek beállítása Leírás Fájlba írás Eszköztárbeje&gyzés megjelölése &Hanglejátszás Parancs &végrehajtása Válassza ki a végrehajtandó parancsot A lejátszandó hang kiválasztása Ü&zenetablak &Felolvasás Adott szöveg felolvasása Az esemény szövegének felolvasása Az esemény nevének felolvasása Állapot Cím 