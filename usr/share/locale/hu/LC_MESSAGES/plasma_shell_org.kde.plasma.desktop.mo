��    E      D  a   l      �  
   �  
   �  
             !     %     9     H     N  	   ]     g  	   o     y     �     �  
   �     �     �  	   �     �     �     �     �     �     �               "     )  
   ;     F  2   Y  ?   �     �     �     �     �     �     �     
               .     <     ?     O     \     b     o  	   {     �     �     �     �  b   �  �   	     �	     �	  	   �	     �	     �	  
   �	  	   �	     	
     
     !
     '
     9
  �  J
     �     �          /     F     J     _     p     |     �     �     �     �     �     �     �  	   �  	                  5     Q     V     i     r     z     �  	   �     �     �     �  @   �  L   (     u     �     �     �     �     �     �     �     �     �                    -  	   4     >     N     Z     `     y     �  j   �  �        �     �     �               6     D     Q     f     m     y     �     *       D   B   3       %       C           
   ;      )   8               E      .   "                @       ?   <                        :                  A             9       &           /         0   1         !                       ,   '       5   >         =              #   -   $   +              (       7       2   4   	                  6    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-02-03 15:22+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aktivitások Művelet hozzáadása Elválasztó hozzáadása Elemek hozzáadása… Alt Alternatív widgetek Mindig látható Alkalmazás Beállítások alkalmazása Alkalmazás most Szerző: Automatikus elrejtés Visszaléptető gomb Lentre Mégsem Kategóriák Középre Bezárás Beállítások Aktivitás beállítása Aktivitás létrehozása… Ctrl Jelenleg használt Törlés E-mail: Előre léptető gomb Új widgetek letöltése Magasság Vízszintes görgetés Bemenet itt Gyorsbillentyűk A felület nem változtatható, amíg a widgetek zárolva vannak A felület változtatásait alkalmazni kell a további módosítások előtt Elrendezés: Balra Bal gomb Licenc: Elemek zárolása Panel maximalizálása Meta Középső gomb További beállítások… Egérműveletek OK Panel igazítása Panel eltávolítása Jobbra Jobb gomb Képernyőszél Keresés… Shift Aktivitás leállítása Leállított aktivitások: Váltás Az aktuális modul beállításai megváltoztak. Szeretné menteni a módosításokat, vagy elveti azokat? Ez a gyorsbillentyű aktiválja a kisalkalmazást: átadja neki a billentyűzet fókuszát, és ha a kisalkalmazásnak van felugró eleme (mint például az indítómenünek), a felugró elem megnyílik. Fentre Eltávolítás visszavonása Eltávolítás Widget eltávolítása Függőleges görgetés Láthatóság Háttérkép Háttérkép típus: Elemek Szélesség Az ablakok eltakarhatják Az ablakok elé kerülhet 