��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  
   �	     �	     �	     
     
     (
     ,
  	   ;
     E
     Y
     a
  !   t
  
   �
     �
     �
     �
     �
     �
     �
                4     E     T     e     n     u     �     �     �     �     �     �     �     �     �     �  D   �     :  )   I     s     |     ~  +   �     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-03-19 19:04+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 % %1 %2 %1: Alkalmazás energiafogyasztása Akkumulátor Kapacitás Töltés százaléka Feltöltöttségi állapot Töltés Áramerősség °C Részletek: %1 Kisütés ulysses@kubuntu.org Energia Energiafogyasztás Energiafogyasztási statisztikák Környezet Teljes nézet Teljesen feltöltött Van tápegység Kai Uwe Broulik Utolsó 12 órában Utolsó 2 órában Utolsó 24 órában Utolsó 48 órában Utolsó 7 napban Utolsó teljes Utolsó órában Gyártó Modell Kiszel Kristóf Nem Nincs töltés PID: %1 Elérési út: %1 Újratölthető Frissítés Sorozatszám W Rendszer Hőmérséklet Ez a fajta előzmény jelenleg nem érhető el ehhez az eszközhöz. Időtartomány A megjelenítendő adatok időtartománya Gyártó V Feszültség Másodpercenkénti felébredések: %1 (%2%) W Wh Igen Fogyasztás % 