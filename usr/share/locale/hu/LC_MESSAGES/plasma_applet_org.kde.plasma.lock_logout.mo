��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  8   �  7   #     [  
   g     r  +     	   �     �  	   �     �  F   �     '  ,   +  ;   X     �     �     �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: KDE 4.3
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-02-14 22:43+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Szeretne felfüggeszteni a memóriába (alvó állapot)? Szeretne felfüggeszteni a merevlemezre (hibernálás)? Általános Műveletek Hibernálás Hibernálás (felfüggesztés merevlemezre) Kilépés Kilépés… Zárolás Képernyőzárolás Kijelentkezés és a számítógép kikapcsolása vagy újraindítása Nem Alvó állapot (felfüggesztés memóriába) Párhuzamos munkamenet indítása másik felhasználóként Felfüggesztés Felhasználóváltás Felhasználóváltás Igen 