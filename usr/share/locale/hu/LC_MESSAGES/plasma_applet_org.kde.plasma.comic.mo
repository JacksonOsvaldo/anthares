��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  U   /  &   �     �     �      �     �  !      
   "     -  &   5     \  (   h  #   �     �     �     �     �     	  :     %   T  "   z     �  ?   �     �     �  4   
  :   ?     z     �     �      �  (   �     �  
   �               -     B  �   S  ;     +   U     �  ?   �     �     �  '   �     %     :      Q     r  '   ~  !   �     �     �     �  #   �                         !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: KDE 4.3
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-27 14:07+0100
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 

Válassza az előző képsort a legutóbbi gyorsítótárazott képsorra ugráshoz. Képregényar&chívum létrehozása… Men&tés mint... &Adott számúra: *.cbz|Képregényarchívum (ZIP) Eredeti mé&ret Mentés a jelenlegi &pozícióban Speciális Összes Hiba történt a(z) %1 azonosítóval. Megjelenés Nem sikerült archiválni a képregényt Automatikus képregényfrissítés: Gyorsítótár Új képregények keresése: Képregény Képregény-gyorsítótár: Beállítás… Nem lehetett létrehozni az archívumot a megadott helyen. %1 képregényarchívum létrehozása Képregényarchívum létrehozása Cél: Hiba megjelenítése, ha nem sikerül letölteni a képregényt Képregények letöltése Hibakezelés Nem sikerült hozzáadni egy fájlt az archívumhoz. Nem sikerült létrehozni a fájlt a(z) %1 azonosítóval. Az elejétől… A végétől… Általános Új képregények beszerzése… Nem sikerült letölteni a képregényt: Ugrás Jellemzők Ugrás az &aktuálisra Ugrás az &elsőre Ugrás az elsőre... Kézi tartomány Lehet hogy nincs internetkapcsolat.
Lehet hogy a képregénymodul törött.
Egy másik lehetséges ok: nincs képregény ehhez a naphoz/számhoz/sztringhez, érdemes lehet egy másikat választani. Megjelenítés az eredeti méretben középső kattintásra Nincs létező ZIP-fájl, félbeszakítás. Tartomány: Nyilak csak akkor látszódjanak, ha az egérmutató fölé ér Webcím megjelenítése Szerző megjelenítése Képregény-azonosító megjelenítése Cím megjelenítése Képregényazonosító Az archiválandó képregények. Frissítés A képregény weboldalának megnyitása A web&bolt oldalának megnyitása # %1 nap yyyy. MM. dd. Következő lap új képregé&nnyel Ettől: Eddig: perc képsor képregényenként 