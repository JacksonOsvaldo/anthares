��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  =   �  x   �     >  _   ]     �     �  O   �     6  K   D     �     �     �     �  .   �  .     /   1     a     }  j   �     �     �  �        �  <   �     �  	          
   "     -     C     K     a     r     �     �     �     �     �     �     �     �     �     	          "     1     G     T     b     n  &   }     �     �     �     �  �   �     �  H   �  �   �     �  Q   �  1     2   7  *   j  $   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: KDE 4.2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-27 12:44+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.4
 A modulváltoztatás alkalmazásához jelentkezzen ki és be. A telepített Phonon rendszermodulok listája. A beállított prioritási sorrendnek megfelelően lesznek felhasználva. Eszközsorrend alkalmazása… A jelenleg látható eszközsorrend alkalmazása a következő hanglejátszási kategóriákra: Hangeszköz-beállítás Hanglejátszás Alapértelmezett hanglejátszóeszköz-sorrend ebben a kategóriában: „%1” Hangfelvétel Alapértelmezett felvételieszköz-sorrend ebben a kategóriában: „%1” Rendszermodulok Colin Guthrie Csatlakozó © Matthias Kretz, 2006. Alapértelmezett hanglejátszóeszköz-sorrend Alapértelmezett hangfelvételieszköz-sorrend Alapértelmezett videofelvételieszköz-sorrend Alapértelmezett kategória Le Beállítja az eszközök alapértelmezett sorrendjét. Ez a sorrend kategóriánként felülbírálható. Eszközbeállítás Eszközbeállítás A számítógépben található, a kijelölt kategóriához megfelelő eszközök. Válassza ki, melyik eszközt használják az alkalmazások. ulysses@kubuntu.org Nem sikerült beállítani a kijelölt hangkimeneti eszközt Bal középső Bal elöl Bal első-középső Jobb első Jobb első-középső Hardver Független eszközök Bemeneti szintek Érvénytelen KDE hangeszköz-beállítás Matthias Kretz Monó Kiszel Kristóf Phonon beállítómodul Lejátszás (%1) Fel Profil Hátsó középső Bal hátsó Jobb hátsó Felvétel (%1) Speciális eszközök Bal oldalsó Jobb oldalsó Hangkártya Hangeszközök Hangszóró-elhelyezés és tesztelés Mélysugárzó Próba A kijelölt eszköz tesztelése %1 tesztelése A sorrend határozza meg a kimeneti eszköz prioritását. Ha valamilyen ok miatt az első eszköz nem használható, a Phonon a másodikkal próbálkozik (és így tovább). Ismeretlen csatorna A jelenleg megjelenített eszközlista használata több kategóriához. A különböző médiahasználatokhoz illő kategóriák. Minden kategóriában külön-külön megadhatja, melyik eszközt használják a elsődlegesen Phonon alkalmazások. Videofelvétel Alapértelmezett videofelvételieszköz-sorrend ebben a kategóriában: „%1”  Az Ön modulja nem támogathatja a hangfelvételt Az Ön modulja nem támogathatja a videofelvételt nincs prioritás a kiválasztott eszközre a kiválasztott eszköz kijelölése 