��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     t     �     �     �     �     �          #     6     >     L     k     w     �  
   �     �     �     �     �     �       A     	   ^  %   h     �      �     �     �               1  $   I     n     z     �     �     �     �  	   �     �     �     �  	   �          #     B     T     h     �     �     �     �     �     �     �               2     F     Z     x     �     �      �     �          
           ,  
   @  %   K     q  #   �  /   �  ,   �  .     (   @  %   i  '   �     �     �     �  ;   �     )     9     U     m     �     �     �  !   �  8   �  >        V         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-12-04 14:18-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 „%1” kezelése… Választás… Ikon visszaállítása Hozzáadás az asztalhoz Hozzáadás a kedvencekhez Hozzáadás a panelhez (widget) Találatok igazítása alulra  Minden alkalmazás %1 (%2) Alkalmazások Alkalmazások és dokumentumok Viselkedés Kategóriák Számítógép Névjegyek Leírás (név) Csak leírás Dokumentumok Alkalmazás szerkesztése… Alkalmazások szerkesztése… Munkamenet befejezése Keresés kiterjesztése könyvjelzőkre, fájlokra és e-mailekre Kedvencek Menü összenyomása egyetlen szintre Összes elfelejtése Összes alkalmazás elfelejtése Összes névjegy elfelejtése Összes dokumentum elfelejtése Alkalmazás elfelejtése Névjegy elfelejtése Dokumentum elfelejtése Legutóbbi dokumentumok elfelejtése Általános %1 (%2) Hibernálás %1 elrejtése Alkalmazás elrejtése Ikon: Zárolás Képernyő zárolása Kijelentkezés Név (leírás) Csak név Gyakran használt alkalmazások Gyakran használt dokumentumok Gyakran használt Minden aktivitáson A jelenlegi aktivitáson Megnyitás ezzel: Rögzítés a panelen Helyek Energiakezelés / Munkamenet Tulajdonságok Újraindítás Legutóbbi alkalmazások Legutóbbi névjegyek Legutóbbi dokumentumok Legutóbb használt Legutóbb használt Eltávolítható háttértár Eltávolítás a kedvencekből Számítógép újraindítása Parancs futtatása… Parancs vagy keresés futtatása Munkamenet mentése Keresés Keresési eredmények Keresés… „'%1” keresése Munkamenet Névjegy adatainak megjelenítése… Megjelenítés a Kedvenceknél Alkalmazások megjelenítése mint: Gyakran használt alkalmazások megjelenítése Gyakran használt névjegyek megjelenítése Gyakran használt dokumentumok megjelenítése Legutóbbi alkalmazások megjelenítése Legutóbbi névjegyek megjelenítése Legutóbbi dokumentumok megjelenítése Megjelenítés: Leállítás Rendezés ABC sorrendben Párhuzamos munkamenet indítása másik felhasználóként Felfüggesztés Felfüggesztés memóriába Felfüggesztés lemezre Felhasználóváltás Rendszer Rendszerműveletek Számítógép kikapcsolása Írja be a keresési kifejezést. Alkalmazások rejtésének megszüntetése itt: „%1” Alkalmazások rejtésének megszüntetése ebben az almenüben Elemek 