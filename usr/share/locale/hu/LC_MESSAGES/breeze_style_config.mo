��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     @  )   D     n  -   �  2   �     �               0  +   F  )   r  "   �  D   �  N   	  #   S	  0   w	  )   �	  *   �	     �	  *   
     E
  5   a
     �
     �
     �
     �
     �
  A   �
  	        !                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-12-04 14:35-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  ms &Billentyűzetgyorsítók láthatósága: &Felfelé nyíl gomb típusa: Mindig rejtse el a billentyűzetgyorsítókat Mindig jelenítse meg a billentyűzetgyorsítókat Animációk i&dőtartama: Animációk Lefelé nyíl go&mb típusa: Breeze beállítások Lapozósáv lapjainak középre igazítása Ablakok húzása minden üres területtel Ablakok húzása csak a címsorral Ablakok húzása a címsorral, a menüsorral és az eszköztárakkal Vékony vonal rajzolása a fókusz jelölésére a menükben és menüsávokon Fókuszjelző rajzolása listákban Keretek rajzolása a dokkolható panelek körül Keretek rajzolása az oldalcímek körül Keretek rajzolása az oldalpanelek körül Csúszkajelölők rajzolása Eszköztár-elválasztó elemek rajzolása Animációk engedélyezése Kiterjesztett átméretező fogantyú engedélyezése Keretek Általános Nincsenek gombok Egy gomb Görgetősávok Akkor jelenítse meg a billentyűzetgyorsítókat, ha szükséges Két gomb Ablakok húzásának módja: 