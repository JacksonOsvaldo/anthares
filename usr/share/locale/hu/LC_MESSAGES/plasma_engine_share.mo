��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  '   �  +     0   3  /   d  3   �  '   �  $   �       1   %         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-27 17:19+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.2
 Nem ismerhető fel a fájl MIME-típusa Nem található minden szükséges funkció Nem található szolgáltató a megadott címmel Hiba történt a szkript végrehajtása közben A kért szolgáltató elérési útja érvénytelen Nem lehetett olvasni a kijelölt fájlt A szolgáltatás nem volt elérhető Ismeretlen hiba Meg kell adnia egy URL-címet a szolgáltatáshoz 