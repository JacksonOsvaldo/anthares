��    1      �  C   ,      8     9     =     B  .   Q  5   �     �     �     �     �  	   �     �               &  1   :     l     r          �  
   �     �  '   �     �     �     �            '        @     O     V     ^  5   g     �     �     �     �     �  $   �  A   �  �   @     &     -  C   >  '   �     �     �     �  �  �     �
     �
     �
     �
     �
     �
     �
  	   �
     	       $   *     O     f     {  F   �     �     �  &        -     <     N     f     t     w  "        �     �  F   �                /     >  D   K     �     �     �     �     �     �     �       	   !     +     @     [     b     t  *   |               #   /       -                      $                +         (   1          .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-12-04 13:51-0500
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1% Vissza Akkumulátor: %1% Kiosztásváltás Virtuális billentyűzet Mégsem A Caps Lock be van kapcsolva Bezárás Keresés bezárása Beállítás Kereső bővítmények beállítása Asztali munkamenet: %1 Másik felhasználó Billentyűzetkiosztás: %1 Kijelentkezés 1 másodperc múlva Kijelentkezés %1 másodperc múlva Bejelentkezés Sikertelen bejelentkezés Bejelentkezés másik felhasználóval Kijelentkezés Következő szám Nincs médialejátszás Nem használt OK Jelszó Média indítása/szüneteltetése Előző szám Újraindítás Újraindítás 1 másodperc múlva Újraindítás %1 másodperc múlva Legutóbbi lekérdezések Eltávolítás Újraindítás Kikapcsolás Újraindítás 1 másodperc múlva Leállítás %1 másodperc múlva Új munkamenet indítása Felfüggesztés Váltás Munkamenetváltás Felhasználóváltás Keresés… Keresés: „%1”… Plasma, a KDE-től Feloldás Sikertelen feloldás ezen: TTY%1 (Kijelző: %2) TTY %1 Felhasználónév %1 (%2) a legutóbbi lekérdezések kategóriában 