��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �     �          "     8  
   D     O     W     `     p  8   �     �     �     �     �       
        &     C  6   c  9   �     �  "   �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: KDE 4.1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-22 15:49+0100
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-i18n-doc@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 fájl %1 fájl %1 könyvtár %1 könyvtár %2 / %1 feldolgozva %2 / %1 feldolgozva - %3/s %1 feldolgozva %1 feldolgozva - %2/s Megjelenés Működés Mégsem Törlés Beállítás... Befejezett feladatok Futó fájlátvitelek és feladatok listája (kuiserver) Áthelyezés más listába Áthelyezés más listába Szünet Eltávolítás Eltávolítás. Folytatás Minden feladat kilistázása Minden feladat megjelenítése. Minden feladat kilistázása fastruktúrába szervezve Minden feladat megjelenítése fastruktúrába szervezve. Külön ablakok Megjelenítés külön ablakokban. 