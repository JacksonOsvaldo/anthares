��          �   %   �      P     Q  /   e     �     �  6   �  N      E   O  W   �  @   �     .     G      d  !   �  %   �  )   �      �  c     -   |  D   �     �       !   )  B   K  @   �     �  �  �     �      �     �     �  "   �  A   �  1   ?	  D   q	  7   �	  (   �	  .   
  .   F
  -   u
  3   �
  3   �
  &     c   2  -   �  U   �       )   :     d  W   �  4   �                                                                   
                                                  	             @info:creditAuthor @info:creditCopyright 1999-2014 KDE Developers @info:creditDavid Faure @info:creditWaldo Bastian @info:shell command-line optionCreate global database @info:shell command-line optionDisable incremental update, re-read everything @info:shell command-line optionPerform menu generation test run only @info:shell command-line optionSwitch QStandardPaths to test mode, for unit tests only @info:shell command-line optionTrack menu id for debug purposes Could not launch Browser Could not launch Mail Client Could not launch Terminal Client Could not launch the browser:

%1 Could not launch the mail client:

%1 Could not launch the terminal client:

%1 EMAIL OF TRANSLATORSYour emails Error launching %1. Either KLauncher is not running anymore, or it failed to start the application. Function must be called from the main thread. KLauncher could not be reached via D-Bus. Error when calling %1:
%2
 NAME OF TRANSLATORSYour names No service implementing %1 The provided service is not valid The service '%1' provides no library or the Library key is missing application descriptionRebuilds the system configuration cache. application nameKBuildSycoca Project-Id-Version: KDE 4.4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-07 08:40+0100
PO-Revision-Date: 2014-08-30 11:04+0200
Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Szerző © A KDE fejlesztői, 1999-2014. David Faure Waldo Bastian Globális adatbázis létrehozása Az inkrementális frissítés kikapcsolása, teljes újraolvasás A menügenerálás teszt módban való indítása A QStandardPaths átállítása teszt módba, egységtesztelésekhez A menüazonosító követése (nyomkövetési célból) A böngészőt nem sikerült elindítani A levelezőprogramot nem sikerült elindítani A terminálprogramot nem sikerült elindítani A böngészőt nem sikerült elindítani:

%1 A levelezőprogramot nem sikerült elindítani:

%1 A terminálprogramot nem sikerült elindítani:

%1 ulysses@kubuntu.org,taszanto@gmail.com Nem sikerült elindítani: %1. A KLauncher már nem fut vagy nem tudta elindítani az alkalmazást. A függvényt a fő szálból kell meghívni. A KLaunchert nem sikerült elérni a D-Buson keresztül (hívott függvény: %1):
%2
 Kiszel Kristóf,Szántó Tamás Egy szolgáltatás sem implementálja: %1 A szolgáltatás érvénytelen A(z) „%1” szolgáltatásnak nincs programkönyvtára vagy a Library kulcs hiányzik Újraépíti a rendszer-konfigurációs gyorstárat. KBuildSycoca 