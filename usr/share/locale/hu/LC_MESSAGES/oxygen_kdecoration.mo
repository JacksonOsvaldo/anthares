��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j           3     <     A     M     g     o          �     �     �     �     �     �     �     �  G   �     %     1     >     N  	   a      k  
   �  m   �  [     )   a      �     �     �  .   �  &   �           /     ;     Y     v     |       $   �      �     �  ,   �  (        5  $   D     i  
   p     {     �  C   �  0   �       '   !     I     \     v     �  	   �  4   �      �         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-02-06 22:02+0100
Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>
Language-Team: Hungarian <kde-l10n-hu@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Ablaktulajdonság ill&esztése:  Hatalmas Nagy Nincs keret Nincsenek oldalszegélyek Normál Túlméretezett Apró Óriási Nagyon nagy Nagy Normál Kicsi Nagyon nagy Aktív ablak ragyogása Hozzáadás Fogantyú hozzáadása a szegély nélküli ablakok átméretezéséhez Animációk &Gombméret: Szegélyméret: Ablakgomb-átmenet Középre Középre, teljes szélességgel Osztály:  Az aktív állapotának változásakor lévő ablakárnyék és fény közötti elhalványulás beállítása Az ablakgombok kiemelési animációjának beállítása, ha az egérmutató föléjük ér Ablakkeret-megjelenítési beállítások Ablaktulajdonságok felismerése Párbeszédablak Szerkesztés Kivétel szerkesztése - Oxygen beállítások Ezen kivétel engedélyezése/tiltása Kivételtípus Általános Ablak címsorának elrejtése A kijelölt ablak jellemzői Balra Le Fel Új kivétel - Oxygen beállítások Kérdés - Oxygen beállítások Reguláris kifejezés A reguláris kifejezés szintaxisa helytelen Az illesz&tendő reguláris kifejezés:  Eltávolítás Eltávolítja a kijelölt kivételt? Jobbra Árnyékok &Címsorigazítás: Cím:  Ugyanazon szín használata a címsávhoz és az ablak tartalmához Ablakosztály használata (a teljes alkalmazás) Ablakcím használata Figyelmeztetés - Oxygen beállítások Ablakosztály neve Az ablak vetett árnyéka Azonosító Ablaktulajdonság kijelölése Ablakcím Az ablak aktív állapota változásának átmenetei Ablakspecifikus felülbírálás 