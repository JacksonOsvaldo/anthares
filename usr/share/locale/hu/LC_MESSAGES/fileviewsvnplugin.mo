��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  �  �       &   "  *   I  :   t  5   �  +   �  $     4   6  .   k  >   �  l   �  -   F	     t	     �	     �	     �	     �	     �	  &   �	  !   
  	   5
  
   ?
     J
                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-04-23 14:10+0000
Last-Translator: Tamas Krutki
Language-Team: Hungarian <kde-i18n-doc@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=(n != 1);
 Kommit Fájlok hozzáadva az SVN adattárhoz. Fájlok hozzáadása az SVN adattárhoz... A fájlok hozzáadása az SVN addatárhoz sikertelen volt. Az SVN változtatások kommitálása sikertelen volt. Az SVN változtatások kommittálva lettek. SVN változtatások kommitálása... A fájlok el lettek távolítva az SVN adattárból. Fájlok eltávolítása az SVN adattárból... A fájlok eltávolítása az SVN adattárból sikertelen volt. Az SVN állapotfrissítés sikertelen volt. Az "SVN frissítések megjelenítése" opció ki lett kapcsolva. Az SVN adattár frissítése sikertelen volt. Az SVN adattár frissült. SVN adattár frissítése... Hozzáadás (SVN) Kommit (SVN) Törlés (SVN) Frissítés (SVN) Helyi SVN változások megjelenítése SVN frissítések megjelenítése Leírás: SVN kommit Frissítések megjelenítése 