��          4      L       `   $   a   /   �   �  �   J   C  >   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: KDE 4.3
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-08-26 11:11+0100
Last-Translator: Tamas Szanto <taszanto@gmail.com>
Language-Team: Hungarian <kde-i18n-doc@kde.org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 A következő kifejezéshez illeszkedő Konsole-munkameneteket keres: :q:. Kilistázza az azonosítóhoz tartozó Konsole-munkameneteket. 