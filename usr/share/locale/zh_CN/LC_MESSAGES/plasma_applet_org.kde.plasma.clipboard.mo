��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            *        K     ^     k     {     �     �     �     �     �     �     �     �     �                    +     2                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.clipboard.pot
 修改条码类型 清除历史 剪贴板内容 剪贴板历史为空。 剪贴板为空 Code 39 Code 93 配置剪贴板... 创建条码失败 Data Matrix 编辑内容 +%1 调用动作 QR 码 删除历史项 返回剪贴板 搜索 显示条形码 