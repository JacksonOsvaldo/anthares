��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  .  �     �  	   �     �     �  4   
     ?     L     b     o     |     �     �     �     �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.fifteenpuzzle.pot
 外观 浏览... 选择图像 十五子拼图 图像文件 (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) 数字颜色 自定义图像路径 拼图颜色 显示数字 洗牌 大小 顺序重排解迷 已解决！再来一次。 时间：%1 使用自定义图像 