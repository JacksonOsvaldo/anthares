��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5    K     Y     ^  B  }  %  �  E   �	     ,
     9
     F
     W
     q
     �
     �
     �
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmlaunch.pot
  秒 启动指示超时(秒)(&S)： <H1>任务栏通知</H1>
您可以使用第二种启动指示的方法，在任务栏中显示一个旋转的沙漏，
表示您所启动的应用程序正在载入。
有时候可能某些应用程序不支持启动通知。在这种情况下，按扭在
“启动指示超时”栏中所指定的时间之后将会消失 <h1>忙碌光标</h1>
KDE 在程序启动时显示忙碌光标以通知状态。
要启用忙碌光标，请从组合框中选择一种可见的反馈。
有可能某些程序不支持这种启动通知，
这种情况下，光标在“启动指示超时”指定的时间之后停止闪烁。 <h1>启动反馈</h1>您可以在此配置应用程序启动反馈。 闪烁光标 弹起光标 忙碌光标(&Y) 启用任务栏通知(&T) 无忙碌光标 被动忙碌光标 启动指示超时(&U)： 任务栏通知(&N) 