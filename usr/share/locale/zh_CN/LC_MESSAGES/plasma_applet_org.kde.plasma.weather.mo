��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  (       C     H     N     V     ]     n     |          �     �  
   �     �     �     �     �     �     �  
          	   $     .     @     L     S     e  	   q     {     �     �  	   �     �     �     �     �  	   �  	   �                 	   (     2  	   9     C     L     Z     l     x     �     �     �     �     �     �     �     �     �     �     �  	   �  	               	     	   &     0     7     ;  	   B  	   L     V     ]     j  	   n  	   x     �     �     �     �     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.weather.pot
  分 %1 %2 %1 (%2) 外观 蒲福风级 bft 摄氏度 °C ° 细节 华氏度 °F %1 天 百帕 hPa 最高：%1 最低：%2 最高温度：%1 英寸汞柱 inHg 开尔文 K 千米 千米/小时 km/h 千帕 kPa 海里/小时 kt 位置： 最低温度：%1 米/秒 m/s 英里 英里/小时 mph 毫巴 mbar 不可用 未找到“%1”的天气站 注意 % 气压： 搜索 选择气象服务提供商 - 以紧凑模式显示温度： 请配置 温度： 单位 更新间隔： 能见度： 天气站 平缓 风速： %1 (%2%) 湿度：%1%2 能见度：%1 %2 露点：%1 湿润度：%1 下降 上升 稳定 气压趋势：%1 气压：%1 %2 %1%2 能见度：%1 %1 (%2) 发布警告： 留意事项： 东 东北东 东南东 北 北东 北北东 北北西 北西 南 南东 南南东 南南西 南西 无法测量 西 西北西 西南西 %1 %2 %3 无风 风寒：%1 阵风：%1 %2 