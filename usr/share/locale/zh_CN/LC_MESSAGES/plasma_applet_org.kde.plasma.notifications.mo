��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  .  	     7     D     M  "   f  	   �  	   �     �     �     �     �     �     �     �                    #     *     7     G     W     g  	   z     �     �     �  !   �     �  !   �          %  	   2     <     C     J     P     \                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.notifications.pot
 %1 个通知 %1/%2 %3 %1 个运行中的任务 配置事件通知和动作(&C)... 10 秒前 30 秒前 显示细节 隐藏细节 清除通知 复制 复制链接地址 %1 个目录中的 %2 个 %1 个文件中的 %2 个 历史 %1/%2 +%1 信息 任务失败 任务已完成 更多选项... 无新通知。 无通知和任务 打开... %1 (已暂停) 全部选中 显示通知历史 显示应用程序和系统通知 %1 (%2 剩余) 跟踪文件传送和其他任务 弹出通知使用自定位置 %1 分钟前 %1 天前 昨天 刚刚 %1： %1：失败 %1：已完成 