��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  *  	     :     >  
   E     P     ]     j     q     �     �     �     �     �     �     �     �     �               '  	   .     8  	   T     ^     e     l  	   �     �     �     �     �     �     �     �     �     �                    !  	   .     8  
   J     U     \     i  	   �  	   �     �     �               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_lookandfeel_org.kde.lookandfeel.pot
 %1% 后退 电量 %1% 切换布局 虚拟键盘 取消 大写锁定已开启 关闭 关闭搜索 配置 配置搜索插件 桌面会话：%1 不同用户 键盘布局：%1 在 %1 秒后注销 登录 登录失败 以不同的用户登录 注销 下一曲 没有正在播放的媒体 未使用 确定 密码 播放或暂停媒体 上一曲 重启 在 %1 秒后重启 最近的命令 删除 重启 显示媒体控件： 关机 在 %1 秒后关机 启动新会话 挂起 切换 切换会话 切换用户 搜索... 搜索“%1”... KDE Plasma 解锁 解锁失败 在终端 %1 (显示器 %2) 终端 %1 用户名 %1 (%2) 在最近查询类别中 