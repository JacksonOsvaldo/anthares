��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6    <     I	     V	     Z	     n	     {	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     
     $
     +
     ;
     K
     [
     k
     ~
     �
     �
     �
  
   �
     �
  	   �
     �
     �
                 	   4     >  6   V     �     �     �     �  	   �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_sddm.pot
 %1 (Wayland) ... (可用大小：%1) 选择图像 高级 作者 自动登录(&L) 背景： 清除图片 命令 无法解压归档 光标主题： 自定义主题 默认 描述 下载新的 SDDM 主题 kde-china@kde.org 常规 获取新主题 关机命令： 从文件安装 安装主题。 无效的主题包 从文件装入... SDDM 登录屏幕  最大 UID： 最小 UID： KDE 中国 名称 无预览 重启命令： 退出后重新登录 删除主题 SDDM KDE 配置 SDDM 主题安装器 会话： SDDM 默认光标主题 要安装的主题，必须为已存的压缩文件。 主题 无法安装主题 卸载主题。 用户 用户： 