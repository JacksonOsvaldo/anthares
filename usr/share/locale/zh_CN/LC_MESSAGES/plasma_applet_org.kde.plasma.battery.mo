��    &      L  5   |      P  9   Q     �     �     �     �     �     �  7     	   G     Q     Z  �   t     C     O     b     z     �     �     �     �     �  #   �  %   �          6  �   F  P     o   c  5   �  �   	     �     �     �     �  g   �  )   <	  ?   f	  (  �	     �     �     �               )     C  	   S  	   ]     g     t  �   �          !     .     A     N     U  	   b     l  	   �     �     �     �     �  :   �     �        *   8  �   c     �     �       	     `        }     �               %       
                           #                     !                "       $                            &   	                                                      %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging %1%. Plugged in %1%. Plugged in, not Charging &Configure Power Saving... Battery and Brightness Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Disabling power management will prevent your screen and computer from turning off automatically.

Most applications will automatically suppress power management when they don't want to have you interrupted. Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled Show percentage Some Application and n others are currently suppressing PM%2 and %1 other application are currently suppressing power management. %2 and %1 other applications are currently suppressing power management. Some Application is suppressing PM%1 is currently suppressing power management. Some Application is suppressing PM: Reason provided by the app%1 is currently suppressing power management: %2 The battery applet has enabled system-wide inhibition The capacity of this battery is %1%. This means it is broken and needs a replacement. Please contact your hardware vendor for more details. Time To Empty: Time To Full: Used for measurement100% Vendor: Your notebook is configured not to suspend when closing the lid while an external monitor is connected. battery percentage below battery icon%1% short symbol to signal there is no battery curently available- Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.battery.pot
 %1 剩余 (%2%) %1% 电池剩余 %1% 正在充电 %1% 已插入 %1% 已插入，未充电 配置电源设置(&C)... 电池和亮度 不可用 容量： 正在充电 配置电源设置... 禁用电源管理将阻止屏幕和计算机自动关闭。

大多数程序将在它们不想您被打断时自动阻止电源管理。 正在用电 显示亮度 启用电源管理 充电完成 常规 键盘亮度 型号： 没有可用的电池 未充电 %1% %1% 电源管理已被禁用 显示百分比 %2 和 %1 个其他应用程序正在阻止电源管理。 %1 正在阻止电源管理。 %1 正在阻止电源管理：%2 电池小程序已经禁止了系统休眠 此电池的容量为 %1% 。这意味着它可能已经损坏并且需要更换。请联系您的硬件提供商来获得更多信息。 耗尽时间： 充满时间： 100% 厂商： 您的笔记本电脑被配置为在连接了外部显示器时合上笔记本盖不会睡眠。 %1% - 