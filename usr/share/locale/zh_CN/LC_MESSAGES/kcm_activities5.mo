��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �    �     �	     �	     �	     

     
  	   
     "
     )
     6
     F
     S
     Z
     g
  &   t
  '   �
     �
     �
  	   �
  6   �
     *     >     K     X     k     �     �     �  	   �     �     �     �  $   �     �  6     !   C  	   e     o     v     }     �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_activities5.pot
 不记住(&D) 遍历活动 反向遍历活动 应用 取消 更改... 创建 活动设置 创建新活动 删除活动 活动 活动信息 切换活动 您确定您想要删除“%1”吗？ 黑名单所有不在列表中的程序 清除最近的历史 创建活动... 描述： 加载 QML 错误。请检查您的安装。
缺少 %1 对所有程序(&L) 忘记一天 忘记所有 忘记上个小时 忘记上两个小时 常规 图标 保持历史 名称： 只对指定的程序(&N) 其它 隐私 隐私 - 不跟踪此活动的使用 记住打开的文档： 记住每个活动的当前虚拟桌面 (需要重启) 切换到此活动的快捷键： 快捷键 切换 壁纸 为  月 永远 