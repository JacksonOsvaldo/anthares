��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �    �     �     �     �  	               	        &     *     1     8     <     @     D     K     [  !   b  <   �     �     �     �     �            	   '  K   1  *   }     �     �  	   �     �     �     �               !     7     M     Q     X     _     |     �     �     �     �     �     �             	     '   %      M     n     �     �     �     �     �     �     �          $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/oxygen_kdecoration.pot
 匹配窗口属性(&M)： 巨大 大 无边框 无侧边框 中 超特大 小 超大 很大 大 中 小 很大 最小化窗口 添加 为无边框窗口添加缩放柄 允许从窗口边缘调整已经最大化的窗口的大小 动画 按钮大小(&U)： 边框大小： 鼠标划过按钮时的变换 居中 居中(完整宽度) 分类： 配置窗口活动状态更改时窗口阴影的淡入淡出和弹性变换 配置鼠标滑过按钮时的加亮动画 装饰选项 检测窗口属性 对话框 编辑 编辑例外 - Oxygen 设置 启用/禁用此例外 例外类型 常规 隐藏窗口标题栏 所选窗口的信息 左 下移 上移 新建例外 - Oxygen 设置 问题 - Oxygen 设置 正则表达式 正则表达式语法错误 匹配正则表达式(&T)： 删除 移除所选例外吗？ 右 阴影 标题排列(&L)： 标题： 统一标题栏和窗口内容的颜色 使用窗口分类(整个程序) 使用窗口标题 警告 - Oxygen 设置 窗口分类名 窗口拖痕阴影 窗口标识符 窗口属性选择 窗口标题 窗口活动状态变化 特定窗口优先规则 