��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b    y     �     �     �     �     �     �  	   �     �  	   �  (   �     '	     .	  	   G	     Q	  &   j	  ,   �	     �	      �	  	   �	  	   
     
     
     <
  	   R
     \
     z
     �
     �
     �
      �
     �
     �
  	      (   
     3                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2018-02-01 07:11-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-trunk/messages/frameworks/kross5.pot
 作者 (C) 2006 Sebastian Sauer Sebastian Sauer 要执行的脚本 常规 添加新脚本。 添加... 取消吗？ 注释： kde-china@kde.org, chaofeng111@gmail.com 编辑 编辑选中的脚本。 编辑... 执行选中的脚本。 无法为解释器“%1”创建脚本 为脚本文件“%1”决定解释器失败 装入解释器“%1”失败 打开脚本文件“%1”失败 文件： 图标： 解释器： Ruby 解释器的安全级别 KDE 中国, Feng Chao 名称： 没有这样的函数“%1” 没有解释器“%1” 删除 删除选中的脚本。 运行 脚本文件“%1”不存在。 停止 停止选中脚本的执行。 文本： 运行 Kross 脚本的命令行工具。 Kross 