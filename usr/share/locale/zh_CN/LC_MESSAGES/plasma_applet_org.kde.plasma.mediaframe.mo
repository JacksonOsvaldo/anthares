��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  +  1     ]     m  	   �     �     �     �     �     �     �  -   �            	        '     =     V     o       *   �     �     �  B   �  0   &	  '   W	  '   	     �	     �	     �	     �	                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.mediaframe.pot
 添加文件... 添加文件夹... 背景框 变更图片每 选择文件夹 选择文件 配置小部件... 填充模式： 常规 左键单击图片在外部查看器中打开 粘贴 路径 路径： 鼠标悬停时暂停 裁剪并保持宽高比 缩放并保持宽高比 项目随机化 拉伸 图片会在水平和垂直方向上重复 图片会保持原样 图片缩放到适合大小 图片一致地缩放到适合填充的大小，必要时会裁剪 图片一致地缩放到合适大小而不剪裁 图片会被水平拉伸并垂直平铺 图片会被垂直拉伸并水平平铺 平铺 水平平铺 垂直平铺 秒 