��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �    �     
     	
     
     
     .
     5
     <
     L
     Y
     f
     m
     q
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
               (     9     J     W     d  	   t     ~  
   �     �  	   �     �     �  	   �     �  	   �     �     �     �  *   �       !     	   >     H     J     Q     l     p     s     w     ~     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_energyinfo.pot
 % %1 %2 %1： 应用程序能源消耗 电池 容量 充电百分比 充电状态 正在充电 电流 °C 细节：%1 正在用电 kde-china@kde.org 能源 能源消耗 能源消耗统计 环境 充满设计值 充电完成 有电源供应 Kai Uwe Broulik 最近 12 小时 最近 2 小时 最近 24 小时 最近 48 小时 最近 7 天 上次充满 最近 1 小时 制造商 型号 KDE 中国 否 未充电 进程号：%1 路径：%1 可充电 刷新 序列号 W 系统 温度 此类型的历史对当前设备不可用 时间区间 要显示的数据的时间区间 供应商 V 电压 每秒唤醒数：%1 (%2%) 瓦 Wh 是 消耗 % 