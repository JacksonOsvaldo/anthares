��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m    s     �     �     �     �  	   �     �     �     �          &     3     F     M     Z     a     w     �     �  	   �     �     �     �     �     �     �               (     /                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.image.pot
 %1 作者 %2 添加自定义壁纸 添加文件夹... 添加图像... 背景： 模糊 居中 更换图片间隔： 显示幻灯片壁纸的目录 下载壁纸 获得新壁纸... 小时 图像文件 分钟 下一张壁纸图片 打开所在文件夹 打开图像 打开壁纸图片 位置： 推荐的壁纸文件 移除壁纸 恢复壁纸 缩放 缩放并裁剪 缩放，保持比例 秒 选择背景色 纯色 平铺 