��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F    �     �  q   �  i   2	     �	     �	  a   �	     %
     ,
     H
     U
     \
     x
     �
     �
  !   �
     �
  
   �
     �
     �
       :     I   J  '   �  A   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmmousetheme.pot
 (c) 2003-2007 Fredrik Höglund <qt>您确定要删除 <i>%1</i> 鼠标指针主题吗？<br />这将删除此主题安装的全部文件。</qt> <qt>您无法删除目前正在使用的主题。<br />您必须是先切换到另外一种主题。</qt> (可用大小：%1) 依赖于分辨率 已经在您的图标主题文件夹中存在名为 %1 的主题。您是否想要将其覆盖？ 确认 鼠标指针设置已更改 光标主题 描述 拖曳或输入主题地址 kde-china@kde.org Fredrik Höglund 获取新主题 从互联网获得新配色方案 从文件安装 KDE 中国 名称 覆盖主题吗？ 删除主题 文件 %1 好像不是有效的鼠标指针主题存档。 无法下载鼠标指针主题存档；请检查地址 %1 是否正确。 找不到鼠标指针主题存档 %1。 要让这些更改生效，您必须重新启动 Plasma 会话。 