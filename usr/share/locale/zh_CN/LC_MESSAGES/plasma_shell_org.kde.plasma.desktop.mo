��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  '  �
     �     �     �     �     �     �     �                 	   )     3  	   @     J     Q     X     _     f     m     o     v     �     �     �     �     �  	   �     �     �     �     �     �  $     3   2  	   f  	   p     z     �     �     �     �     �     �     �     �     �     �  	   �          	  	              &     3     I  K   P  �   �     6     =     J     Q     ^  	   k     u     |     �     �     �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_shell_org.kde.plasma.desktop.pot
 活动 添加动作 添加间距 添加部件... Alt 可替换部件 总是可见 应用 应用设置 立即应用 作者： 自动隐藏 后退键 底部 取消 类别 居中 关闭 + 配置 配置活动 创建活动... Ctrl 当前正在使用 删除 电子邮件： 前进键 获得新部件 高度 水平滚轮 在此输入 键盘快捷键 部件被锁定时无法变更布局 必须先保存布局更改才能更改其他选项 布局： 左对齐 左键 许可协议： 锁定部件 最大化面板 Meta 中键 更多设置... 鼠标动作 确定 面板对齐 删除面板 右对齐 右键 屏幕边缘 搜索... Shift 停止活动 已停止的活动： 切换 当前模块的设置已经被修改。您想要应用还是丢弃更改？ 这个快捷键将会激活小部件：它将使键盘聚焦在小部件上，如果小部件有弹出框 (例如开始菜单)，弹出框将会打开。 顶部 撤销卸载 卸载 卸载部件 垂直滚轮 可见性 壁纸 壁纸类型： 部件 宽度 窗口可覆盖 窗口位于后面 