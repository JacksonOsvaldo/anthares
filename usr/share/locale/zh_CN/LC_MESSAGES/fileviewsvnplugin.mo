��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |    �     �      �  #   �  #   �     	     "	     9	      T	  #   u	  #   �	  #   �	  #   �	  #   
  B   )
     l
     �
     �
  
   �
     �
  
   �
  
   �
  
   �
     �
     
  	     
   &     1                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2018-02-04 20:35-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kdesdk/fileviewsvnplugin.pot
 提交 文件已向 SVN 仓库添加。 正在向 SVN 仓库添加文件... 向 SVN 仓库添加文件失败。 SVN 更改提交失败。 SVN 更改已提交。 正在提交 SVN 更改... 文件已从 SVN 仓库删除。 正在从 SVN 仓库删除文件... 从 SVN 仓库删除文件失败。 从 SVN 仓库还原文件成功。 正在从 SVN 仓库还原文件... 从 SVN 仓库还原文件失败。 SVN 状态更新失败。已禁用选项“显示 SVN 更新”。 SVN 仓库更新失败。 SVN 仓库已更新。 正在更新 SVN 仓库... SVN 添加 SVN 提交... SVN 删除 SVN 还原 SVN 更新 显示本地 SVN 更改 显示 SVN 更新 描述： SVN 提交 显示更新 