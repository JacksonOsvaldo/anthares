��          |      �             !  *   <     g  @   �  �   �  B   Q  R   �  &   �  *     ,   9  4   f  %  �     �  -   �       E   !  J   g     �  $   �  !   �  $     $   7     \        	       
                                            Could not eject this disc. Could not mount this device as it is busy. Could not mount this device. One or more files on this device are open within an application. One or more files on this device are opened in application "%2". One or more files on this device are opened in following applications: %2. Remove is less technical for unmountCould not remove this device. Remove is less technical for unmountYou are not authorized to remove this device. This device can now be safely removed. You are not authorized to eject this disc. You are not authorized to mount this device. separator in list of apps blocking device unmount,  Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_engine_devicenotifications.pot
 无法弹出此光盘。 无法挂载设备，因为此设备正忙。 无法挂载此设备。 此设备上的一个或多个文件在一个应用程序中打开。 此设备上的一个或多个文件在下列应用程序中打开：%2。 无法移除此设备。 您缺少移除此设备的权限。 此设备现在可安全移除。 您缺少弹出此光盘的权限。 您缺少挂载此设备的权限。 ， 