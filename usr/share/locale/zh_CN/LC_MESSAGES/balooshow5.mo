��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  
  �     �     �     �       (        A     V     v  	   �     �     �     �     �  $   �  \   �  
   Z  -   e     �     �     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-02-01 07:11-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-trunk/messages/frameworks/balooshow5.pot
 %1 个术语：%2 (c) 2012, Vishesh Handa Baloo 展示 文件的设备 id kde-china@kde.org, chaofeng111@gmail.com 文件名术语：%1 要显示的文件的 Inode 号 内部信息 维护者 KDE 中国, Feng Chao 未找到索引信息 打印内部信息 术语：%1 Baloo 数据查看器 - 调试工具 无法打开 Baloo 索引。请运行 "%1" 以查看 Baloo 是否已启用且工作正常。 文件 URL fileID 不等于实际 Baloo 文件 fileID。 这是一个错误 Vishesh Handa 