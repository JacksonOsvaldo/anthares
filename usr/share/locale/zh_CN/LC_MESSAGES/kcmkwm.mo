��    �      \  �   �
      (     )     /     3     :     M     `     p          �     �     �     �     �     �     �     �     �  )     J  7  �   �  O  &  !  v  �  �  ?  ?  �    �  *     �     �               -     ?     V     s     �     �     �  	   �     �  #   �  4   �  X   *  Z   �  9   �  Z     ;   s  Y   �  :   	      D      R      n      v            �      �      �      �      �      �   0   �       !  �   '!     �!     �!     "  v   2"  �   �"  �   O#  �   �#  �   �$  h   B%  *   �%  .   �%     &     
&  �   &  a   �&  c   �&  }   _'  �   �'  _   e(  �   �(  a   O)  �   �)  `   :*     �*     �*     �*     �*     �*     �*     +     +     +     +     /+     =+     N+     h+     �+     �+     �+     �+     �+  
   �+     �+     �+     �+     ,     ,     ,     -,     2,     P,     f,     �,     �,     �,     �,  
   �,     �,     �,     �,     �,     �,     �,     �,     -     -     (-  l   /-     �-     �-     �-     �-  
   �-  #   �-     �-     �-  &   .  3   :.  �  n.  m   1  d   v1     �1     �1     �1     
2     2     $2  �   12  \   �2  >  "3  �   a4  �   75  B   �5  l   6    x6     �7  $   �7     �7     �7     �7     �7     �7     �7     8  
  '8     2:     8:     @:     H:     _:     v:     �:     �:  
   �:     �:  
   �:     �:  "   �:     
;     ;  
   2;     =;  (   T;    };  �   �<  )  *=  �   T>  t  S?    �@  e  �A  �  NC     GG     NG     dG     zG     �G     �G  $   �G     �G     H  	   H     H  
   7H     BH  !   FH  '   hH  N   �H  Q   �H  0   1I  N   bI  0   �I  N   �I  0   1J     bJ     pJ     �J     �J     �J     �J     �J     �J     �J     �J     �J  7   �J     7K  �   IK     �K     �K     L  Y   &L  �   �L  {   8M  ~   �M  ~   3N  P   �N  *   O  *   .O     YO     ]O  u   dO  c   �O  ]   >P  q   �P  t   Q  T   �Q  t   �Q  T   MR  t   �R  T   S     lS     yS     �S  !   �S     �S     �S     �S     �S      T     T     T     /T     @T     ZT  	   sT     }T     �T     �T     �T  	   �T     �T     �T     �T     �T  	   U     U     &U     -U     JU  
   ]U     hU  	   lU     vU     �U  
   �U     �U     �U     �U     �U     �U     �U     �U     �U     �U     V  Q   V     oV     vV     �V     �V  	   �V  "   �V     �V     �V     �V  '   W  M  *W  ]   xY  ]   �Y  	   4Z     >Z     QZ  	   ^Z     hZ     vZ  �   �Z  R   [    ^[  �   k\  �   ]  T   �]  ]   ^  �   j^     W_     h_     �_     �_     �_     �_     �_     �_     �_            �   �          1   {       �       n   ^           T          *   �   �      �   %   �   Z   �   z   G   d   9       p   �      �               
       i   t          �   B      k   [      $   �   l   r       W   s   \           K      _       Q       j             A   R   u       L       g      �   �           F              I                N   O      X   (      -          C   S      �   �   0   D       �   `   f       >           �   6               :   P   <   �       �   �   2   Y   �   b       �       h   @      x   a   U       �   o   w   m   E       |       �   V   &   v   !   �   ~           "   �   8   }       �       �   �   ,   �       ?          '   +       �       	   �   =   M   �   )   J   c   4   H      y   7   #       3   e   ;   �   /   �   ]          .       �          �          �   �   5      q       �                                     +   ms  pixel &Border snap zone: &Center snap zone: &Delay focus by &Double-click: &Enable hover &Focus &Left button: &Moving &Placement: &Raise on hover, delayed by &Right button: &Titlebar Actions &Wheel &Window snap zone: (c) 1997 - 2002 KWin and KControl Authors <b>Click To Focus - Mouse Precedence</b><br>
This is mostly the same as <i>Click To Focus</i><br><br>
If an active window has to be chosen by the system<br>
(eg. because the currently active one was closed) <br>
the window under the mouse is the preferred candidate.<br><br>
Unusual, but possible variant of <i>Click To Focus</i>. <b>Click To Focus</b><br>
A window becomes active when you click into it.<br><br>
This behaviour is common on other operating systems and<br>
likely what you want. <b>Focus Follows Mouse - Mouse Precedence</b><br>
This is mostly the same as <i>Focus Follows Mouse</i><br><br>
If an active window has to be chosen by the system<br>
(eg. because the currently active one was closed) <br>
the window under the mouse is the preferred candidate.<br><br>
Choose this, if you want a hover controlled focus. <b>Focus Follows Mouse</b><br>
Moving the mouse onto a window will activate it.<br><br>
Eg. windows randomly appearing under the mouse will not gain the focus.<br>
Focus stealing prevention takes place as usual.<br><br>
Think as <i>Click To Focus</i> just without having to actually click. <b>Focus Strictly Under Mouse</b><br>
The focus is always on the window under the mouse - in doubt nowhere -<br>
very much like the focus behaviour in an unmanaged legacy X11 environment.<br><br>

Notice:<br>
<b>Focus stealing prevention</b> and the <b>tabbox ("Alt+Tab")</b><br>
contradict the policy and <b>will not work</b>.<br><br>
You very likely want to use<br>
<i>Focus Follows Mouse - Mouse Precedence</i> instead! <b>Focus Under Mouse</b><br>
The focus always remains on the window under the mouse.<br><br>

Notice:<br>
<b>Focus stealing prevention</b> and the <b>tabbox ("Alt+Tab")</b><br>
contradict the policy and <b>will not work</b>.<br><br>
You very likely want to use<br>
<i>Focus Follows Mouse - Mouse Precedence</i> instead! <p><h1>Window Behavior</h1> Here you can customize the way windows behave when being moved, resized or clicked on. You can also specify a focus policy as well as a placement policy for new windows.</p> <p>Please note that this configuration will not take effect if you do not use KWin as your window manager. If you do use a different window manager, please refer to its documentation for how to customize window behavior.</p> <p>This option specifies how much KWin will try to prevent unwanted focus stealing caused by unexpected activation of new windows. (Note: This feature does not work with the Focus Under Mouse or Focus Strictly Under Mouse focus policies.)
<ul>
<li><em>None:</em> Prevention is turned off and new windows always become activated.</li>
<li><em>Low:</em> Prevention is enabled; when some window does not have support for the underlying mechanism and KWin cannot reliably decide whether to activate the window or not, it will be activated. This setting may have both worse and better results than the medium level, depending on the applications.</li>
<li><em>Medium:</em> Prevention is enabled.</li>
<li><em>High:</em> New windows get activated only if no window is currently active or if they belong to the currently active application. This setting is probably not really usable when not using mouse focus policy.</li>
<li><em>Extreme:</em> All windows must be explicitly activated by the user.</li>
</ul></p>
<p>Windows that are prevented from stealing focus are marked as demanding attention, which by default means their taskbar entry will be highlighted. This can be changed in the Notifications control module.</p> Activate Activate & Lower Activate & Pass Click Activate & Raise Activate & Scroll Activate, Raise & Move Activate, Raise & Pass Click Activate, Raise & Scroll Activating windows Active Active screen follows &mouse Ad&vanced Alt Automatically group similar windows Behavior on <em>double</em> click into the titlebar. Behavior on <em>left</em> click into the titlebar or frame of an <em>active</em> window. Behavior on <em>left</em> click into the titlebar or frame of an <em>inactive</em> window. Behavior on <em>left</em> click onto the maximize button. Behavior on <em>middle</em> click into the titlebar or frame of an <em>active</em> window. Behavior on <em>middle</em> click onto the maximize button. Behavior on <em>right</em> click into the titlebar or frame of an <em>active</em> window. Behavior on <em>right</em> click onto the maximize button. Bernd Wuebben C&lick raises active window Cascade Centered Change Opacity Click Close Cristian Tibirna Daniel Molkentin Decrease Opacity Dela&y: Display window &geometry when moving or resizing EMAIL OF TRANSLATORSYour emails Enable this option if you want a window's geometry to be displayed while it is being moved or resized. The window position relative to the top-left corner of the screen is displayed together with its size. Extreme Focus &stealing prevention Handle mouse wheel events Here you can customize KDE's behavior when scrolling with the mouse wheel in a window while pressing the modifier key. Here you can set that windows will be only snapped if you try to overlap them, i.e. they will not be snapped if the windows comes only near another window or border. Here you can set the snap zone for screen borders, i.e. the 'strength' of the magnetic field which will make windows snap to the border when moved near it. Here you can set the snap zone for the screen center, i.e. the 'strength' of the magnetic field which will make windows snap to the center of the screen when moved near it. Here you can set the snap zone for windows, i.e. the 'strength' of the magnetic field which will make windows snap to each other when they are moved near another window. Here you select whether holding the Meta key or Alt key will allow you to perform the following actions. Hide inactive window tabs from the taskbar Hide utility windows for inactive applications High Hover If Shade Hover is enabled, a shaded window will un-shade automatically when the mouse pointer has been over the title bar for some time. In this column you can customize mouse clicks into the titlebar or the frame of an active window. In this column you can customize mouse clicks into the titlebar or the frame of an inactive window. In this row you can customize behavior when scrolling into an inactive inner window ('inner' means: not titlebar, not frame). In this row you can customize left click behavior when clicking into an inactive inner window ('inner' means: not titlebar, not frame). In this row you can customize left click behavior when clicking into the titlebar or the frame. In this row you can customize middle click behavior when clicking into an inactive inner window ('inner' means: not titlebar, not frame). In this row you can customize middle click behavior when clicking into the titlebar or the frame. In this row you can customize right click behavior when clicking into an inactive inner window ('inner' means: not titlebar, not frame). In this row you can customize right click behavior when clicking into the titlebar or the frame. Inactive Inactive Inner Window Increase Opacity Inner Window, Titlebar & Frame Keep Above/Below Left &button Left button: Low Lower M&iddle button: M&ouse wheel: Matthias Ettrich Matthias Hoelzer-Kluepfel Matthias Kalle Dalheimer Maximize Maximize (horizontal only) Maximize (vertical only) Maximize Button Maximize/Restore Maximizing Medium Meta Middle b&utton: Middle button: Minimize Modifier &key: Move Move to Previous/Next Desktop Multiscreen behaviour NAME OF TRANSLATORSYour names None Nothing On All Desktops Operations Menu Pat Dowler Policy Raise Raise/Lower Raising windows Random Resize Ri&ght button: Right button: S&eparate screen focus Scroll Sets the time in milliseconds before the window unshades when the mouse pointer goes over the shaded window. Shade Shade/Unshade Shading Smart Snap Zones Snap windows onl&y when overlapping Special Windows Start Window Tab Drag Switch to Window Tab to the Left/Right Switch to automatically grouped windows immediately The placement policy determines where a new window will appear on the desktop.<br><ul>
<li><em>Smart</em> will try to achieve a minimum overlap of windows</li>
<li><em>Maximizing</em> will try to maximize every window to fill the whole screen. It might be useful to selectively affect placement of some windows using the window-specific settings.</li>
<li><em>Cascade</em> will cascade the windows</li>
<li><em>Random</em> will use a random position</li>
<li><em>Centered</em> will place the window centered</li>
<li><em>Zero-Cornered</em> will place the window in the top-left corner</li>
<li><em>Under Mouse</em> will place the window under the pointer</li>
</ul> This is the delay after which the window that the mouse pointer is over will automatically come to the front. This is the delay after which the window the mouse pointer is over will automatically receive focus. Titlebar Titlebar & Frame Toggle Raise & Lower Under Mouse Waldo Bastian Wheel event: When this option is enabled, a window in the background will automatically come to the front when the mouse pointer has been over it for some time. When this option is enabled, focus operations are limited only to the active Xinerama screen When this option is enabled, the active Xinerama screen (where new windows appear, for example) is the screen containing the mouse pointer. When disabled, the active Xinerama screen is the screen containing the focused window. By default this option is disabled for Click to focus and enabled for other focus policies. When this option is enabled, the active window will be brought to the front when you click somewhere into the window contents. To change it for inactive windows, you need to change the settings in the Actions tab. When turned on attempt to automatically detect when a newly opened window is related to an existing one and place them in the same window group. When turned on hide all tabs that are not active from the taskbar. When turned on immediately switch to any new window tabs that were automatically added to the current group. When turned on, utility windows (tool windows, torn-off menus,...) of inactive applications will be hidden and will be shown only when the application becomes active. Note that applications have to mark the windows with the proper window type for this feature to work. Window Actio&ns Window Behavior Configuration Module Window Tabbing Windows Wynn Wilkes Zero-Cornered no border snap zone no center snap zone no window snap zone Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmkwm.pot
    +   毫秒  像素 边界吸附区(&B)： 中间吸附区(&C)： 延迟聚焦(&D)： 双击(&D)： 悬停展开(&E) 焦点(&F) 鼠标左键(&L)： 移动(&M) 放置策略(&P)： 提升鼠标悬停(&R)，延迟： 鼠标右键(&R)： 标题栏动作(&T) 滚轮(&W) 窗口吸附区(&W)： (c) 1997 - 2002 KWin 和 KControl 作者 <b>单击聚焦 - 鼠标优先</b><br>
非常类似于 <i>单击聚焦</i><br><br>
如果一个活动窗口被系统选中<br>
(例如，由于当前活动窗口被关闭) <br>
鼠标下的窗口将是优先候选。<br><br>
不常见，但是不同于 <i>单击聚焦</i>。 <b>点击以聚焦</b><br>
一个窗口在您单击它时被激活。<br><br>
该行为在其他操作系统中较常见<br>
可能是您所希望的。 <b>焦点跟随鼠标 - 鼠标优先</b><br>
非常类似于 <i>焦点跟随鼠标</i><br><br>
如果一个活动窗口被系统选中<br>
(例如，由于当前活动窗口被关闭) <br>
鼠标下的窗口将是优先候选。<br><br>
如果您想使用鼠标悬停控制焦点，选择此项。 <b>焦点跟随鼠标</b><br>
移动鼠标到窗口上将激活该窗口。<br><br>
例如，随机出现在鼠标下的窗口将不回获得焦点。<br>
阻止焦点抢占正常生效。<br><br>
类似 <i>点击聚焦</i> 只是不需要真的点击。 <b>焦点严格置于鼠标下</b><br>
焦点永远在鼠标下的窗口 - 无论何处 -<br>
与无管理传统 X11 环境中的聚焦策略非常相似。<br><br>

请注意：<br>
<b>阻止焦点抢占</b> 和 <b>Tab快捷键 ("Alt+Tab")</b><br>
与该策略冲突 <b>将不会生效</b>.<br><br>
您很可能希望使用<br>
<i>焦点跟随鼠标 - 鼠标优先</i>！ <b>焦点置于鼠标下</b><br>
焦点永远保留在鼠标下的窗口<br><br>

请注意：<br>
<b>阻止焦点抢占</b> 和 <b>Tab快捷键 ("Alt+Tab")</b><br>
与该策略冲突 <b>将不会生效</b><br><br>
您很可能希望使用<br>
<i>焦点跟随鼠标 - 鼠标优先</i>！ <p><h1>窗口行为</h1> 您可以在这里自定义窗口移动、改变大小或被点击时的行为。您可以指定焦点策略和新窗口的放置策略。</p><p>请注意，只有您使用 KWin 作为窗口管理器，这里的配置才有效。如果您使用其它的窗口管理器，请参考其文档以了解如何自定义窗口行为。</p> <p>此选项指定了 KWin 应该如何阻止因新窗口的无意激活而导致失去焦点。(请注意：此特性与“焦点置于鼠标下”和“焦点严格置于鼠标下”策略不兼容。)
<ul>
<li><em>禁用：</em>不阻止，新窗口总是被激活。</li>
<li><em>低：</em>开启阻止; 如果某些窗口不支持后台规则，且 KWin 无法可靠的确定是否要激活该窗口，则会激活该窗口。这一设置可能比普通级别的效果更差或更好，这取决于具体的应用程序。</li>
<li><em>中：</em>启用阻止</li>
<li><em>高：</em>仅当没有激活窗口或新窗口属于当前应用程序时，才会激活新窗口。如果不使用鼠标聚焦策略的话，这一设置在不使用鼠标聚焦时可能不怎么常用。</li>
<li><em>极高：</em>全部窗口都必须由用户显式激活。</li>
</ul></p>
<p>标为避免获取焦点的窗口引起注意的默认方式是突出显示其任务栏项。您可以在通知控制模块中更改。</p> 激活 激活并放在后面 激活并进行点击 激活并放在前面 激活并滚动 激活、放在前面并移动 激活、放在前面并进行点击 激活、放在前面并滚动 激活窗口 活动的 活动屏幕跟随鼠标(&M) 高级(&V) Alt 自动对相似窗口进行分组 <em>双</em>击标题栏时的行为。 <em>左</em>键点击<em>活动</em>窗口的标题栏或边框时的行为。 <em>左</em>键点击<em>非活动</em>窗口的标题栏或边框时的行为。 <em>左键</em>单击最大化按钮的行为。 <em>中</em>键点击<em>活动</em>窗口的标题栏或边框时的行为。 <em>中键</em>单击最大化按钮的行为。 <em>右</em>键点击<em>活动</em>窗口的标题栏或边框时的行为。 <em>右键</em>单击最大化按钮的行为。 Bernd Wuebben 点击升起激活窗口(&L) 层叠 居中 更改不透明度 点击 关闭 Cristian Tibirna Daniel Molkentin 降低不透明度 延时(&Y)： 移动或者改变大小时显示窗口几何尺寸(&G) kde-china@kde.org 如果您要在移动或者改变大小时显示窗口几何尺寸，请启用此项。相对于屏幕左上角的窗口位置及其大小将会在移动时显示。 终极 阻止焦点抢占(&S) 处理鼠标滚轮事件 您可以自定义在按下修饰键时在窗口内滚动鼠标滑轮时 KDE 的行为。 您可以在这里设置窗口只在重叠时才将它们的边界吸引到一起。即，如果它们只是相互靠近或者靠近屏幕边沿但尚未重叠，则不相互吸引。 您可以设置屏幕边界的吸附区，即磁场的“吸引力”，它使窗口移近边界时被吸附到边界上。 您可以设置屏幕中间的吸附区，即磁场的“吸引力”，它使窗口移近中间时被吸附到中间位置。 您可以设置窗口的吸附区，即磁场的“吸引力”，它使窗口移近另外的窗口时彼此吸附到一起。 这里您可以选择按住 Meta 键或者 ALT 键时能完成下面的动作。 从任务栏隐藏不活动的窗口标签 隐藏非活动应用程序的工具窗口 高 悬停 如果悬停展开被启用，当鼠标指针在标题栏上停留一段时间后，卷起的窗口将自动展开。 在这一列里，您可以自定义鼠标点击活动窗口的标题栏或边框的窗口行为。 在该列里，您可以自定义鼠标点击非活动窗口的标题栏或边框的行为。 这一行您可以自定义鼠标滚到非活动窗口内部(标题栏和边框包围住的部分)时的行为。 这一行您可以自定义鼠标左键点击非活动窗口内部(标题栏和边框包围住的部分)的行为。 这一行您可以自定义鼠标左键点击标题栏或边框时的窗口行为。 这一行您可以自定义鼠标中键点击非活动窗口内部(标题栏和边框包围住的部分)的行为。 这一行您可以自定义鼠标中键点击标题栏或边框时的窗口行为。 这一行您可以自定义鼠标右键点击非活动窗口内部(标题栏和边框包围住的部分)的行为。 这一行您可以自定义鼠标右键点击标题栏或边框时的窗口行为。 非活动的 非活动窗口内部 提高不透明度 窗口内部、标题栏和边框 常居顶端/底端 鼠标左键(&B)： 鼠标左键： 低 推后 鼠标中键(&I)： 鼠标滑轮(&O)： Matthias Ettrich Matthias Hoelzer-Kluepfel Matthias Kalle Dalheimer 最大化 最大化(仅水平方向) 最大化(仅垂直方向) 最大化按钮 最大化/恢复 最大化 中 Meta 鼠标中键(&U)： 鼠标中键： 最小化 修饰键(&K)： 移动 移动到上个/下个窗口 多显示器行为 KDE 中国 无 无动作 在全部桌面 操作菜单 Pat Dowler 策略 提前 升起/降低 升起窗口 随机 更改大小 鼠标右键(&G)： 鼠标右键： 分隔屏幕焦点(&E) 滚动 设置鼠标指针在卷起的窗口上停留多少毫秒后窗口自动展开。 卷起 卷起/折叠 卷起 智能 吸附区 仅在重叠时才吸引窗口(&Y) 特殊窗口 开始窗口标签拖放 窗口标签向左/右切换 立即切换到自动窗口分组状态 窗口放置策略决定新窗口出现时将被放置到桌面的什么部位。<br><ul>
<li><em>智能</em>将窗口的重叠减到最少。</li>
<li><em>最大化</em> 将尝试最大化每个窗口，将其填满整个屏幕。使用窗口特定的设置来影响某些窗口的放置可能很有用。</li> 
<li><em>层叠</em> 将错开层层叠放窗口。</li>
<li><em>随机</em> 将窗口随机放置。</li>
<li><em>居中</em> 将把窗口居中放置</li>
<li><em>边角</em> 将把窗口放在左上角</li>
<li><em>鼠标下</em> 将窗口放在鼠标指针下方</li>
</ul> 当鼠标移动到窗口上后，延迟这么长时间窗口就会自动出现在最前面。 当鼠标移动到窗口上后，延迟这么长时间窗口就会自动出现在最前面。 标题栏 标题栏和边框 前后切换 鼠标下 Waldo Bastian 滚轮事件： 这个选项启用时，当鼠标光标移动到窗口上一段时间后，在后面背景里的窗口将自动出现在最前面。 这个选项启用时，强制策略会只限对活动中的 Xinerama 屏幕生效 这个选项启用时，鼠标指针所在的 Xinerama 屏幕将会是活动的屏幕 (比如新窗口出现的地方)。反之，活动的屏幕则是焦点所在的屏幕。此选项在点击获得焦点时默认被禁用，使用其他焦点策略时默认启用。 这个选项启用时，当您点击活动窗口中的内容时，该窗口将会被带到顶层。要更改非活动窗口的特性，您需要在动作标签中更改设置。 如果启用此选项，程序将会自动检测新打开的窗口是否和现有窗口相关，并将有关联的窗口放到相同的窗口分组中。 如果启用此选项，任务栏上所有非活动的标签都会被隐藏起来。 如果启用此选项，会立刻将所有新开的窗口标签自动添加到当前组内。 开启时，非活动应用程序的工具窗口(工具栏、私下的菜单)将被隐藏，而且仅当应用程序活动时才显示。请注意，要让此特性工作正常，应用程序必须将正确设定相应窗口的类型。 窗口动作(&N) 窗口行为配置模块 窗口标签 窗口 Wynn Wilkes 边角 无边界吸附区 无中间吸附区 无窗口吸附区 