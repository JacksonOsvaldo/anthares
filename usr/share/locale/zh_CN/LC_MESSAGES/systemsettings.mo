��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
         #  6   6     m     �     �  !   �  	   �     �     �          (     5     B     I     P     ]     t     {     �  '   �  	   �  	   �     �     �                    !  -   .     \     x  	   �     �     �     �     �  
   �     �  -   �  0   %  -   V     �  !   �     �  	   �     �  	   �     �     �  ?   
  ?   J  O   �     �     �  Q   �     F                                            ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/systemsettings.pot
 %1 是一个外部应用程序，已经被自动启动 (c) 2009，Ben Cooksley (c) 2017, Marco Martin <i>包含 %1 个项目</i> 没有匹配您的搜索的项目 关于 %1 关于正在使用的模块 关于活动的视图 关于系统设置 所有设置 应用设置 作者 后退 Ben Cooksley KDE 的配置中心。 配置 配置您的系统 包含 %1 个项目 决定是否使用详细的工具提示 开发者 对话框 kde-china@kde.org 自动展开第一级 常用的： 常规 帮助 图标视图 内部模块重新展示，内部模块模型 所用视图的内部名称 键盘快捷键：%1 维护者 Marco Martin Mathias Soeken 最常用的 最常用的模块编号 %1 KDE 中国 找不到视图 提供各控制模块的分类图标视图。 为控制模块提供一个分类的侧边栏。 提供各控制模块的经典树形视图。 重新启动 %1 重置修改内容到之前的值 搜索 搜索... 显示详细的工具提示 侧边栏 侧边栏视图 系统设置 系统设置找不到任何视图模式，无法进行显示。 系统设置找不到任何视图模式，无法进行配置。 当前模块的设置已经更改。
您想要应用更改还是丢弃更改？ 树形视图 视图风格 欢迎使用“系统设置”，这是用于配置您的系统的控制中心。 Will Stephenson 