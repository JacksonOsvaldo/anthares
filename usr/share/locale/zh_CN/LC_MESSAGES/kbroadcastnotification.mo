��          �      �       0     1  ,   J     w  O   �     G      _     �  =   �     �  H   �  !   @     b         �  !   �  s   �  6   I     �     �     �  3   �  
   �  '   �          8        
                                        	          (c) 2016 Kai Uwe Broulik A brief one-line summary of the notification A comma-separated list of user IDs this notification should be sent to. If omitted, the notification will be sent to all users. A tool that emits a notification for all users by sending it on the system DBus Broadcast Notifications EMAIL OF TRANSLATORSYour emails Icon for the notification Keep the notification in the history until the user closes it NAME OF TRANSLATORSYour names Name of the application that should be associated with this notification The actual notification body text Timeout for the notification Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:52+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kbroadcastnotification.pot
 (c) 2016 Kai Uwe Broulik 通知消息的一行简洁摘要 逗号分隔的用户 ID 列表，指定通知消息应该发送给谁。如果缺省，将发送给所有用户。 通过系统 DBus 广播通知给所有用户的工具 广播通知 kde-china@kde.org 通知消息的图标 将通知保存到历史中，直到用户关闭它 KDE 中国 要绑定到这个通知的应用名称 通知消息的实际正文 通知消息的超时时间 