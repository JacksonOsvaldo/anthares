��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g    p     �     �  	   �     �     �  	   �     �     �     �     �     �     �     �  G   �  "   E     h  '   x  !   �     �     �     �  	   �     	  	   	     	     &	     -	     4	  	   ;	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmkwindecoration.pot
 巨大 大 无边框 无侧边框 中 超特大 小 超大 很大 应用程序菜单 边框大小(&Z)： 按钮 关闭 双击关闭：
 如需打开菜单，按住按钮直到菜单出现。 双击菜单按钮关闭窗口(&T) 上下文帮助 在此处和标题栏之间拖拽按钮 拖拽至此处以删除按钮。 获得新窗口装饰... 保持在上方 保持在下方 最大化 菜单 最小化 在全部桌面 搜索 卷起 主题 标题栏 