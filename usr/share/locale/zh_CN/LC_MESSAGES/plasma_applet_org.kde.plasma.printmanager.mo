��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  (  �          ,     B     O     k     {     �     �     �     �     �     �     �  	             $        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2018-02-04 20:35-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kdeutils/plasma_applet_org.kde.plasma.printmanager.pot
 配置打印机(&C)... 只显示活动任务 全部任务 只显示已完成的任务 配置打印机 常规 没有活动任务 没有任务 未配置或未发现打印机 %1 个活动任务 %1 个任务 打开打印队列 打印队列为空 打印机 搜索打印机... 队列中有 %1 个打印任务 