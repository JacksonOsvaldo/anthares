��    *      l  ;   �      �     �     �     �  
   D  +   O  
   {     �     �     �      �     �     �       	           �   @  e   �  *   D  H   o     �     �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~     �  #   �     �     �    �            l   ,  	   �  )   �  	   �     �     �     �               $     7  	   >     H  �   Z  ?   �       A   6     x     �     �  
   �  	   �  %   �  7   �  	          $   /     T     p     �     �     �     �     �               )     E     R        
          *   &      #      '         $                               (            %                       	       !                            )              "                                          msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcm_kwindesktop.pot
  毫秒 桌面数量(&N)： <h1>多桌面</h1>在该模块中，您可以配置您需要多少个桌面以及怎样标记这些桌面。 动画： 指派全局快捷键“%1”给桌面 %2 桌面 %1 桌面 %1： 桌面特效动画 桌面名称 桌面切换显示 桌面切换 桌面循环切换 桌面 间隔： kde-china@kde.org 如果您希望键盘或活动桌面边框在越过桌面边缘时，自动转到对应一边的新桌面，请启用此选项。 启用此选项后，将会显示所选桌面的预览小图。 输入桌面 %1 的名字 您可以设置您的 KDE 桌面上要有多少个虚拟桌面。 KWin 开发组 布局 行数(&U)： KDE 中国 无动画 找不到适合桌面 %1 的快捷键 快捷键冲突：无法为桌面 %2 设置快捷键 %1 快捷键 显示桌面布局符 显示所有可用桌面的快捷键 将一个桌面向下切换 将一个桌面向上切换 将一个桌面切换到左边 将一个桌面切换到右边 切换到桌面 %1 切换到下一个桌面 切换到上一个桌面 切换 遍历桌面列表 遍历桌面列表 (反向) 遍历桌面 遍历桌面 (反向) 