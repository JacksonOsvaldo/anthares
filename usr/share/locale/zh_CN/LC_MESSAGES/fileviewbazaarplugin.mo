��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �    �  #   �	      
  &   5
     \
     s
     �
     �
     �
     �
     �
          (     F  #   a      �  &   �     �     �     �          !     ?     \     v     �     �     �     �     �     �     �     �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2018-02-04 20:35-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kdesdk/fileviewbazaarplugin.pot
 文件已添加到 Bazaar 仓库。 向 Bazaar 仓库添加文件... 向 Bazaar 仓库添加文件失败。 Bazaar 日志关闭。 Bazaar 更改提交失败。 Bazaar 更改已提交。 正在提交 Bazaar 更改... 拉取 Bazaar 源失败。 拉取 Bazaar 源。 拉取 Bazaar 源... 推送 Bazaar 源失败。 已经推送到 Bazaar 源。 正在推送 Bazaar 源... 文件已从 Bazaar 仓库删除。 从 Bazaar 仓库删除文件... 从 Bazaar 仓库删除文件失败。 复核更改失败。 复核更改。 复核更改... 运行 Bazaar 日志失败。 正在运行 Bazaar 日志... Bazaar 仓库更新失败。 Bazaar 仓库已更新。 正在更新 Bazaar 仓库... Bazaar 添加... Bazaar 提交... Bazaar 删除 Bazaar 日志 Bazaar 拉取 Bazaar 推送 Bazaar 更新 显示本地 Bazaar 更改 