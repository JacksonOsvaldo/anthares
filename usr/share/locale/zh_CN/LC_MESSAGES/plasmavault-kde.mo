��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �      �  #  i  �  �  f     !     !!  	   (!     2!  !   H!     j!  6   q!     �!     �!     �!     �!  $   �!  !   "     5"     K"     R"     h"     n"     ~"  	   �"     �"     �"     �"  *   �"     �"  3   �"     3#     D#     R#  �   Y#     $  i   9$  	   �$     �$     �$  	   �$     �$  	   �$     �$  '   %  	   3%  3   =%     q%  6   �%  $   �%  *   �%  0   &  !   ?&  6   a&  .   �&     �&     �&     �&  '   �&     %'     8'  6   O'  6   �'     �'     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasmavault-kde.pot
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>安全注意：</b>
                             根据 Taylor Hornby (来自 Defuse Security) 的安全审计，当前 Encfs
                             的实现是易受攻击或潜在易受攻击的。比如，具有读写访问加密数据
                             权限的攻击者可以随后附加加密数据，而不被合法用户察觉，从而降
                             低解密复杂度，或者使用时间分析来推断信息。
                             <br /><br />
                             这意味着您不应当将加密的数据同步到云存储服务，或者用在任何攻
                             击者可频繁访问加密数据的情况。
                             <br /><br />
                             查看 <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> 获取更多信息。 <b>安全提示：</b>
                             CryFS 加密您的文件，您可以安全地将他们存放在任何位置。
                             它与 Dropbox，iCloud，OneDrive 和其他与服务能协同工作。
                             <br /><br />
                             不像其他文件系统覆盖解决方案，它并不会暴露目录结构，文件
                             数量，或是文件尺寸于加密数据中。
                             <br /><br />
                             需要注意的是，尽管 CryFS 被认为是安全的，但是尚未有独立的
                             安全审计确认这一点。 创建新保险库 活动 后端： 不能创建挂载点 不能打开未知的保险库。 更改 选择您想要为此保险库使用的加密系统︰ 选择使用加密方式： 关闭保险库 配置 配置保险库... 配置的后端不能实例化︰ %1 已配置的后端不存在︰ %1 找到正确的版本 创建 创建新保险库... CryFS 设备已打开 设备未打开 对话框 不再显示此提示 EncFS 加密数据位置 创建目录失败，请检查您的权限 未能执行 未能获取使用此保险库的应用程序列表 打开 %1 失败 强行关闭  常规 如果您限制只在某些活动中使用此存储库，只当您是在这些活动中时，它才出现在小程序中。此外，当您切换到它不可用的活动时，它会自动关闭。 限制为选定的活动︰ 请注意忘记的密码将无法恢复。如果您忘记了密码，您的数据也就一并遗失了。 挂载点 未指定挂载点 挂载点： 下一个 用文件管理器打开 密码： Plasma 保险库 请输入密码以打开此保险库： 上一个 挂载点目录不是空的，拒绝打开保险库 指定后端不可用 只能在保险库关闭的时候更改它的配置。 保险库未知，无法关闭它。 保险库是未知的，不能销毁它。 此设备已被注册。不能重新创建它。 此目录已包含加密的数据 无法关闭保险库，有应用程序正在使用它 无法关闭保险库，它正在被 %1 使用 无法检测版本 无法执行该操作 未知设备 未知的错误，无法创建后端。 默认加密方式 保险库名称(&T)： 安装了错误的版本。所需的版本是 %1.%2.%3 您需要为加密存储和挂载点选择空的目录 %1：%2 