��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �    �     �	     �	     �	     
     
     *
     <
     H
     T
  %   n
     �
     �
     �
     �
  K   �
  H   $     m  3   }  (   �     �  0   �       	   .     8     N     [  '   h     �  	   �  <   �     �     �  <     -   M     {  	   �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/kcmkwinscreenedges.pot
  毫秒 % %1 - 全部桌面 %1 - 立方 %1 - 当前程序 %1 - 当前桌面 %1 - 圆柱 %1 - 球体 重新触发延迟(&R)： 在屏幕边缘时切换桌面(&S)： 触发延迟(&D)： 活动屏幕边缘 活动管理器 总是启用 在一个动作被触发后，允许触发下一个动作前的等待时间 当鼠标在屏幕边缘处驻留时，触发动作前应等待的时间 程序启动器 当鼠标光标放置在屏幕边缘时切换桌面 kde-china@kde.org, chaofeng111@gmail.com 锁定屏幕 将窗口拖拽到屏幕上边缘使其最大化 KDE 中国, Feng Chao 无动作 仅当移动窗口时 运行命令 其它设置 触发四分之一平铺在屏幕外部 显示桌面 已禁用 将窗口拖拽到屏幕左侧或右侧边缘来将其平铺 触发额外窗口切换 触发窗口切换 将鼠标指针推向屏幕边缘或者边角来出发动作 从屏幕边缘向中心滑动来触发动作 窗口管理 的部分 