��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  &  �  5   �     �     �          0     K     \     s     z     �     �     �     �     �     �  	   �       	     $   $     I     c  	   y  $   �     �     �  !   �  (   �                0     7     M     f     v     }     �     �     �  �   �  0   �     �  	   �     �               ,     E     [     q     �     �     �     �     �  
   �  .   �                         !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: kdeorg
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2018-02-12 05:47-0500
Last-Translator: guoyunhebrave <guoyunhebrave@gmail.com>
Language-Team: Chinese Simplified
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: crowdin.com
X-Crowdin-Project: kdeorg
X-Crowdin-Language: zh-CN
X-Crowdin-File: /kf5-stable/messages/kde-workspace/plasma_applet_org.kde.plasma.comic.pot
 

选择上一幅可转到最近缓存的连环画。 创建连环画归档(&C)... 连环画另存为(&S)... 连环画数字(&S)： *.cbz|连环画归档(Zip) 实际大小(&A) 保存当前位置(&P) 高级 全部 标识符 %1 发生错误。 外观 创建连环画归档失败 自动更新连环画插件： 缓存 检查新连环画： 连环画 连环画缓存： 配置... 无法在指定地点创建归档。 创建 %1 连环画归档 创建连环画归档 目标： 获取连环画失败时显示错误 下载连环画 错误处理 将文件加入到归档失败。 创建标识符为 %1 的文件失败。 从开头到... 从结尾到... 常规 获得新连环画... 获取连环画失败： 转到连环画 信息 跳转到当前连环画(&C) 跳转到第一张连环画(&F) 跳转到连环画... 手动范围 可能没有互联网连接。
可能连环画插件已失效。
还有个可能是今天/此数字/字符串没有相应的连环画， 所以另外选一个可能就行了。 中键点击连环画让其以原始大小显示 zip 文件不存在，终止。 范围： 仅悬停时显示箭头 显示连环画 URL 显示连环画作者 显示连环画标识符 显示连环画标题 连环画标识符： 归档的连环画范围。 更新 访问连环画网站 访问商店网站(&W) # %1 天 yyyy-MM-dd 跳转到下一个有新连环画的标签(&N) 从： 到： 分钟 页每个连环画 