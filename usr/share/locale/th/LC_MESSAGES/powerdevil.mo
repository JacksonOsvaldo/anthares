��          �   %   �      @     A     F  ,   L  N   y  
   �      �  	   �  �   �     �     �  e   �          )     ;  
   Q     \     c  &   t  %   �  f   �     (     8     S     j  �  �     1     ?     U  �   e  .   E  &   t     �  ;  �     �	  s   
  '  z
  0   �  B   �  B     9   Y     �     �  6   �  6   �  �   6     �  Z   �  Z   J  [   �                                   	                                                          
                                  min After Brightness level, label for the sliderLevel Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2010-12-18 14:04+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
  นาที หลังจาก ระดับ ไม่สามารถเชื่อมต่อไปยังส่วนติดต่อแบตเตอรี่ได้
กรุณาตรวจสอบการตั้งค่าของระบบ ไม่ดำเนินการใด ๆ narachai@gmail.com, donga.nb@gmail.com จำศีล ไม่สามารถเริ่มระบบจัดการพลังงานของ KDE ได้ โปรแกรมเบื้องหลังรายงานข้อผิดพลาดนี้%1
กรุณาตรวจสอบการตั้งค่าของระบบ ล็อคหน้าจอ ภูวณัฏฐ์ สาครสกลพัฒน์, ถนอมทรัพย์ นพบูรณ์ ไม่พบส่วนเสริมโปรแกรมเบื้องหลังการจัดการพลังงานที่ใช้งานได้ คุณอาจแก้ปัญหาด้วยการติดต่อใหม่อีกครั้ง เมื่อโหลดโพรไฟล์ เมื่อไม่ได้โหลดโพรไฟล์ เปิดกล่องการออกจากระบบ สั่งสคริปต์ให้ทำงาน สคริปต์ ปิดหลังจาก เสียบตัวแปลงไฟแล้ว ตัวแปลงไฟถูกถอดออก ไม่พบโพรไฟล์ "%1" ที่เลือกไว้
โปรดตรวจสอบการตั้งค่าของ PowerDevil ปิดจอภาพ วิธีการพักการทำงานที่ไม่รองรับ เมื่อมีการปิดฝาเครื่องแล็ปท็อป เมื่อกดปุ่มเปิด/ปิดบนตัวเครื่อง 