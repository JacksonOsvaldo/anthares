��          �      L      �     �  �   �  m   b  `   �     1     >     V     c     o      �     �     �     �     �     �  ?   �  Y   :  +   �  �  �  F   k  g  �      ?  	     ]
  `   y
  9   �
       k   3  %   �     �  d   �     ;  _   H  *   �  �   �  �   Y  X   "                                                             	                                
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-17 20:54+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 สงวนลิขสิทธิ์ (c) 2003-2007 Fredrik Höglund <qt>คุณแน่ใจหรือว่าต้องการจะลบชุดเคอร์เซอร์ <strong>%1</strong> นี้ออก ?<br />การทำแบบนี้จะเป็นการลบแฟ้มทั้งหมดที่ติดตั้งโดยชุดเคอร์เซอร์นี้</qt> <qt>คุณไม่สามารถลบชุดเคอร์เซอร์ที่กำลังใช้งานอยู่ได้<br />คุณต้องเปลี่ยนไปใช้งานชุดอื่นก่อน</qt> มีชุดตกแต่งชื่อ %1 อยู่แล้วในโฟลเดอร์ของชุดตกแต่งไอคอนของคุณ คุณต้องการจะแทนที่ชุดเดิมด้วยชุดตกแต่งนี้หรือไม่ ? การยืนยัน มีการเปลี่ยนแปลงค่าของเคอร์เซอร์ ชุดตกแต่งเคอร์เซอร์ รายละเอียด ให้ลากหรือพิมพ์ที่อยู่ URL ของชุดตกแต่ง donga.nb@gmail.com, drrider@gmail.com Fredrik Höglund ถนอมทรัพย์ นพบูรณ์, สหชาติ อนุกูลกิจ ชื่อ เขียนชุดตกแต่งทับของเดิมหรือไม่ ? ลบชุดตกแต่งออก แฟ้ม %1 ไม่ใช่แฟ้มจัดเก็บของชุดตกแต่งเคอร์เซอร์ ไม่สามารถดาวน์โหลดชุดตกแต่งได้ โปรดตรวจสอบว่าที่อยู่ %1 ถูกต้องหรือไม่ หาแฟ้มจัดเก็บชุดตกแต่ง %1 ไม่เจอ 