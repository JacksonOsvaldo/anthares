��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  U     "  a  �  �  �   A  3     3   Q  3   �  Z   �  H     <   ]  U   �  E   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-04-26 11:57+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: LoKalize 0.2
Plural-Forms: nplurals=1; plural=0;
  วินาที ช่วงเวลาให้แสดงว่าเริ่มทำงาน: <H1>การแจ้งให้ทราบทางแถบงาน</H1>
คุณสามารถเปิดใช้งานวิธีที่สอง ในการแจ้งให้ทราบว่ามีการเริ่มโปรแกรมแล้ว
โดยแสดงผ่านทางแถบงาน โดยปุ่มงานจะมีภาพนาฬิกาทรายหมุนปรากฎอยู่
เพื่อแสดงว่าโปรแกรมกำลังเริ่มการทำงาน ซึ่งมันอาจจะทำงานได้
แต่บางครั้ง บางโปรแกรมอาจจะไม่แจ้งให้ทราบถึงการเริ่มทำงานนี้
ในกรณีนี้ ปุ่มจะหายไป หลังจากช่วงเวลาที่กำหนดในส่วน
'ช่วงเวลาให้แสดงว่าเริ่มทำงาน' <h1>แสดงการแจ้งทางเคอร์เซอร์</h1>
KDE แนะนำให้ใช้การแจ้งทางเคอร์เซอร์ เมื่อมีการเริ่มโปรแกรม
เพื่อเปิดใช้การแสดงการแ่จ้งทางเคอร์เซอร์ ให้เลือกที่
'เปิดใช้การแจ้งให้ทราบทางเคอร์เซอร์' และหากต้องการให้เคอร์เซอร์กระพริบ
เลือกที่ 'เปิดใช้การกระพริบ' ทางด้านล่าง ซึ่งมันอาจจะทำงานได้
แต่บางครั้ง บางโปรแกรม อาจจะไม่แจ้งถึงการเริ่มทำงานนี้
ในกรณีนี้ เคอร์เซอร์จะหยุดกระพริบ หลังจากช่วงเวลาที่กำหนดในส่วน
'ช่วงเวลาให้แสดงว่าเริ่มทำงาน' <h1>เรียกทำงาน</h1> คุณสามารถปรับแต่ง การตอบกลับ การเรียกให้โปรแกรมทำงานได้ที่นี่ กระพริบเคอร์เซอร์ กระเด้งเคอร์เซอร์ แจ้งทางเคอร์เซอร์ เปิดใช้การแจ้งให้ทราบทางแถบงาน ไม่ต้องแจ้งทางเคอร์เซอร์ เคอร์เซอร์แบบไม่ว่าง ช่วงเวลาให้แสดงว่าเริ่มทำงาน: การแจ้งให้ทราบทางแถบงาน 