��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �  1   �  P   /  $   �  C   �     �               1     >  -   Z  �   �  9   0  9   j  $   �     �     �     �  B   �  B   *	  T   m	  T   �	  -   
  -   E
                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-07-07 15:11+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.1
 %1 แฟ้ม %1 โฟลเดอร์ %1 จาก %2 ถูกประมวลผล %1 จาก %2 ถูกประมวลผลที่ %3/วินาที %1 ถูกประมวลผล %1 ถูกประมวลผลที่ %2/วินาที รูปลักษณ์ พฤติกรรม ยกเลิก ล้าง ปรับแต่ง... งานที่เสร็จแล้ว รายการของการส่งถ่ายแฟ้มและงานต่าง ๆ ที่กำลังทำงานอยู่ (kuiserver) ย้ายไปที่รายการอื่น ย้ายไปที่รายการอื่น หยุดชั่วคราว ลบ ลบ ทำต่อ แสดงงานทั้งหมดในรายการ แสดงงานทั้งหมดในรายการ แสดงงานทั้งหมดในรายการต้นไม้ แสดงงานทั้งหมดในรายการต้นไม้ แสดงหน้าต่างแยก แสดงหน้าต่างแยก 