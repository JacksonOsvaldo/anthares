��          <      \       p   !   q   3   �      �   �  �   a   �  �   �  2   �                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2010-03-27 21:46+0700
Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 ค้นหากลุ่มงานของ Kate ที่เข้าคู่กับ :q: เรียกดูรายการกลุ่มงานทั้งหมดของ Kate ในบัญชีผู้ใช้ของคุณ เปิดกลุ่มงานของ Kate 