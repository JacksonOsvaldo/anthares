��          T      �       �      �      �   '   �   5     D   :       �  �     C  f   ]  �   �  �   R     �  ^   �                                         MiB Enable low disk space warning Is the free space notification enabled. Minimum free space before user starts being notified. The settings dialog main page name, as in 'general settings'General Warn when free space is below: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-22 23:25+0700
Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
  เมกะไบต์ เปิดใช้งานการเตือนพื้นที่เหลือน้อย การแจ้งเตือนพื้นที่ว่างเปิดใช้งานอยู่ใช่หรือไม่ พื้นที่ว่างขั้นต่ำก่อนผู้ใช้เริ่มจะถูกแจ้งเตือน ทั่วไป เตือนเมื่อมีพื้นที่ว่างน้อยกว่า: 