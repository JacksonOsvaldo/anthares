��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  0   �     �  )        +  %   E     k  E   {     �  N   �  �   #  �   �  `   N	  Q   �	     
     
  1    
  x   R
  d   �
     0  ,   >  D   k     �  <   �        K        e  c   r     �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-12-31 22:47+0700
Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 ทั่วไป เพิ่มสคริปต์ใหม่ เพิ่ม... ยกเลิกหรือไม่ ? หมายเหตุ: donga.nb@gmail.com, drrider@gmail.com แก้ไข แก้ไขสคริปต์ที่เลือกไว้ แก้ไข... ประมวลผลสคริปต์ที่เลือกไว้ การสร้างสคริปต์ของตัวแปลคำสั่งภาษา "%1" ล้มเหลว การตรวจหาตัวแปลคำสั่งภาษาเพื่อใช้กับแฟ้มสคริปต์ "%1" ล้มเหลว การโหลดตัวแปลคำสั่งภาษา "%1" ล้มเหลว การเปิดแฟ้มสคริปต์ "%1" ล้มเหลว แฟ้ม: ไอคอน: ตัวแปลคำสั่งภาษา: ระดับความปลอดภัยของตัวแปลคำสั่งภาษารูบี้ ถนอมทรัพย์ นพบูรณ์, สหชาติ อนุกูลกิจ ชื่อ: ไม่มีฟังก์ชัน "%1" ไม่มีตัวแปลคำสั่งภาษา "%1" เอาออก ลบสคริปต์ที่เลือกออก ประมวลผล ยังไม่มีแฟ้มสคริปต์ "%1" อยู่ หยุด หยุดการประมวลผลสคริปต์ที่เลือกไว้ ข้อความ: 