��            )         �     �     �     �  +   �      	     *  ;   @     |     �     �     �      �       9        O  3   W  	   �     �  )   �  5   �  *        2     Q     d     k     x  +   �  (   �     �  3   
  �  >  J   �  .   A     p  b   �  b   �  5   I	  �   	  .   
  E   7
  Z   }
  r   �
     K  >   _  �   �     B  �   X     �  4     V   B  �   �  Z   k  S   �  ;        V  "   c  ?   �  c   �  [   *  V   �  x   �                                               
                                                  	                                       %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2009-01-22 15:11+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 प्रसंग %1 पहिली से ही मौजूद हे चेहराचिनहा जोड़व जोड़व... चेहराचिनहा प्रसंग बनाए बर किसिम चुनव चेहराचिनहा प्रसंग इनस्टाल नइ कर सकिस नवा चेहराचिनहा बनाव कुछ पाठ अउ एक चिनहा आबंटित कर एक नवा चेहराचिनहा बनाव चेहराचिनहा मेटाव नवा चेहराचिनहा डिजाइन करव का आप मन सही मं %1 ल घलोक मिटाना चाहू? चेहराचिनहा प्रसंग यूआरएल ड्रैग या टाइप करव raviratlami@aol.in, चेहराचिनहा संपादित करव एखर चिनहा या एखर पाठ ल बदले बर चुने गे चेहराचिनहा ल संपादित करव संपादन... चेहराचिनहा प्रसंग मन ल लोकल फाइल मन से इनस्टाल करना होही. चेहराचिनहा चेहराचिनहा प्रबंधक नवा चेहराचिनहा प्रसंग के नाम भरव प्रसंग अभिलेख फाइल जऊन आप मन तिर पहिली से ही लोकल रूप से मौजूद हे ओला इनस्टाल करव चुने गे पाठ या चेहराचिनहा ल सुधारव रविसंकर सिरीवास्तव, जी. करूनाकर नवा चेहराचिनहा प्रसंग हटाव प्रसंग मेटाव चुने गे चेहराचिनहा हटाव अपन डिस्क से चुने गे चेहराचिनहा मेटाव अपन डिस्क से चुने गे प्रसंग ल मेटाव चेहराचिनहा के चारों तरफ जगह चाही ये चुने गे प्रसंग ल आप मन के डिस्क से मिटा देही. 