��          �   %   �      P     Q  L   V  /   �     �     �     �          $     1     A     W     f  I   w     �  *   �     �            !   9  "   [     ~  6   �  *   �     �     
  �       �  �   �  x   �  L   	     V  U   i  a   �     !	  ,   =	  /   j	     �	  :   �	  �   �	     �
  j   �
  H        O     _  "   o  p   �  h     �   l  �   �     �     �           	                          
                                                                                         to  <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Smallest font that is still readable well. Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2009-01-22 15:39+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
  to  <p>कुछ बदलाव जइसन कि डीपीआई सिरिफ नवा चालू करे अनुपरयोग मन मं ही लागू होही</p> एक अ-समानुपातिक फोंट (जइसन कि - टाइपराइटर फोंट). सब्बो फोंट मन ल एडजस्ट करव... (&j) बीजीआर सब्बो फोंट मन ल बदले बर किलिक करव एन्टी अलियासिंग सेटिंग कान्फिगर करव कान्फिगर... रेंज बाहिर करव: (&x) फोंट सेटिंग बदलिस %1:  फोंट डीपीआई बाध्य करव: हिन्टिंग एक काम हे जऊन कि छोटे आकार मन के फोंट मन के गुन मन ल उभारथे  आरजीबी सबसे छोटे फोंट जउन हर बने से पढ़े जा सकथे. एंटी-अलियासिंग उपयोग करव: (&n) अक्छम सक्छम तंत्र सेटिंग मेन्यू बार अउ पापअप मेन्यू से उपयोग मं आथे . विंडो सीर्सक पट्टी के साथ उपयोग में आथे. सामान्य पाठ बर उपयोग मं (जइसन कि - बटन लेबल, चीज सूची). औजार पट्टी चिनहा के बाजू से देखाय जाय वाले पाठ बर उपयोग मं आथे . खड़ा बीजीआर खड़ा आरजीबी 