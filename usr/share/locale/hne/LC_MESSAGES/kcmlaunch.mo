��          �      �       0     1     6  ^  S  P   �               #     0     M     \     p     �  �  �  
   W  R   b  �  �  �   6	  .   �	     
  *   <
  8   g
  (   �
  9   �
  R     6   V                                      
   	                  sec &Startup indication timeout: <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-24 16:13+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 सेक. स्टार्टअप इंडीकेसन टाइमआउटः (&S) व्यस्त संकेतक<h1>Busy Cursor</h1>
केडीई प्रस्तुत करथे  एक व्यस्त संकेतक- अनुपरयोग चालू करे के परिचय बताय बर.
 व्यस्त संकेतक सक्छम करे बर एक किसम के विजुअल फीडबैक
काम्बो डब्बा मं से चुनव.
अइसन हो सकथे  कि कुछ अनुपरयोग एखर विसेसता- चालू करे परिचय बताय बर- नी जानत हों
. अइसन हालत में, संकेतक टिमटिमाना बन्द कर देथे 
 'स्टार्टअप इंडीकेसन टाइमआउट' मं दे गे समय के बाद. <h1>लांच फीडबैक</h1> इहां आप मन एप्लीकेसन-लांच फीडबैक कान्फिगरेसन कर सकथो. टिमटिमावत संकेतक उछलत संकेतक व्यस्त संकेतक (&y) काम पट्टी सक्छम करव (&t) अव्यस्त संकेतक अक्रिय व्यस्त संकेतक  स्टार्टअप इंडीकेसन टाइमआउटः (&u) कामपट्टी अधिसूचना (&N) 