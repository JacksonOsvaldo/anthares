��    3      �  G   L      h     i     r     {     �     �     �  �   �  -   J  r   x  	   �  	   �     �               (  
   5     @     L     T      k     �     �     �      �     �     �  5   �     )     6     U  	   Z     d     j     r  (        �     �     �     �          
  +     3   B     v     ~     �     �  S   �  )   �     	  �  +	     �
     �
  !     *   6     a  -   y    �  V   �  �        �       A   6     x     �     �     �     �     �  Y   �     T  1   h     �     �  C   �       K     �   g  ,     S   L  	   �  #   �     �     �     �  [     )   p  O   �  R   �  L   =     �     �  f   �  u        �     �     �  %   �  �   
  Z   �  &   '     *                          "              '          !                                  (       -                   /      #                       0       &      )   ,          1   	                 3              
   $          2   %       .   +       &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-01-22 15:58+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>
Language-Team: Hindi <kde-i18n-doc@lists.kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=(n!=1);
 मात्राः (&A) प्रभावः (&E) दुसरा रंगः (&S) अर्ध-पारदर्सी (&S) प्रसंग (&T) (c) 2000-2003 गीर्त जानसन <qt>का आप मन चिनहा प्रसंग <strong>%1</strong> ल मिटाना चाहथो ?<br /><br />एखर से ये प्रसंग से इनस्टाल करे गे फाइल मन मिट जाही.</qt> <qt>इनस्टाल करत हे <strong>%1</strong> प्रसंग</qt> संस्थापन काम के समय समस्या उत्पन्न होइस, हालाकि अभिलेख के लगभग सब्बो प्रसंग इनस्टाल हो गिस विस्तृत (&v) सब्बो चिनहा एंटोनियो लारोसा जिमेनेज रंग: (&l) रंगीन पुस्टिकरन डि-सेचुरेट वर्नन डेस्कटाप प्रसंग यूआरएल खींचलाव या टाइप करव raviratlami@aol.in, प्रभाव पैरामीटर्स गामा गीर्त जानसन इंटरनेट से नवा प्रसंग लाव चिनहा चिनहा नियंत्रन पेनल माड्यूल प्रसंग अभिलेख फाइल जऊन आप मन के पास पहिली से लोकल मं हे ओला इंस्टाल करव मुख्य औजार पट्टी रविसंकर सिरीवास्तव, जी. करूनाकर नाम कोई प्रभाव नइ पेनल प्रिव्यू प्रसंग हटाव अपन डिस्क से चुने गे प्रसंग ल मेटाव प्रभाव सेट करव... सक्रिय चिनहा प्रभाव सेटअप करव डिफाल्ट चिनहा प्रभाव सेटअप करव अक्छम चिनहा प्रभाव सेटअप करव आकारः छोटे चिनहा ए फाइल एक वैध चिनहा प्रसंग अभिलेख नइ हे. ए आप मन के डिस्क से चुने गे प्रसंग ल मिटा देही. धूसरहा कालासफेद पर औजार पट्टी तार्स्तेन रान चिनहा प्रसंग अभिलेख डाउनलोड करे मं अक्छम;
किरपा करके, जांचव कि पता %1 सही हे. चिनहा प्रसंग अभिलेख %1 पाय मं अक्छम. चिनहा के उपयोग 