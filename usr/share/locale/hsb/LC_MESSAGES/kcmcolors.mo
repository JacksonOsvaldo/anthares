��    6      �  I   |      �  #   �     �     �     �  J   �     -     9     I     ^     s     �     �     �     �     �  	   �     �     �  '   �     	       !   .     P     ^     p  
   �     �  	   �     �     �     �     �     �     �     �                     ;     M     b     z     �     �     �     �     �  	   �     �     �       0     3   D  �  x  "   T
     w
     �
     �
  C   �
     �
     �
     �
          -     @     R     Y  ,   _     �  	   �     �     �  %   �     �     �     	     )     :  !   Q  
   s     ~     �     �     �     �  
   �     �     �     �     �          %     =     V     f       #   �     �     �  	   �     �     �     �            '   '  )   O            5                                   3       .          )   #   4       *   "         '   -       
   6                 1              ,          (                                        +       $       !   0             	   %   &          2      /       &Enter a name for the color scheme: (c) 2007 Matthew Woehlke 0 1 A color scheme with that name already exists.
Do you want to overwrite it? Active Text Active Titlebar Active Titlebar Text Alternate Background Button Background Button Text Color: Colors Colorset to view/modify Contrast Contrast: Error Focus Decoration Get new color schemes from the Internet Hover Decoration Import Color Scheme Import a color scheme from a file Inactive Text Inactive Titlebar Inactive Titlebar Text Intensity: Jeremy Whiting Link Text Matthew Woehlke Negative Text Neutral Text New Row Normal Background Normal Text Options Positive Text Remove Scheme Remove the selected scheme Save Color Scheme Selection Background Selection Inactive Text Selection Text Shade sorted column &in lists Tooltip Background Tooltip Text Varies View Background View Text Visited Text Window Background Window Text You do not have permission to delete that scheme You do not have permission to overwrite that scheme Project-Id-Version: kcmcolors
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2008-11-10 23:08+0100
Last-Translator: Eduard Werner <edi.werner@gmx.de>
Language-Team: en_US <kde-i18n-doc@lists.kde.org>
Language: hsb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;
 &Zapodajće mjeno za barbnu šemu: (c) 2007 Matthew Woehlke 0 1 Barbna šema z tutym mjenom hižo eksistuje.
Chceće ju přepisać? Aktiwny tekst Aktiwny titlowy pas Tekst w aktiwnym titlowym pasu Alternatiwny pozadk Pozadk tłóčatka Tekst tłóčatka Barba: Barby Barby, kiž maja so wobhladać/wobdźěłać Kontrast Kontrast: Zmylk Debjenje fokusa Nowe barbne šemy z interneta čitać Debjenje znošowanja Barbnu šemu importować Barbnu šemu z dataje začitać Njeaktiwny tekst Njeaktiwny titlowy pas Tekst w njeaktiwnym titlowym pasu Intensita: Jeremy Whiting Wotkazowy tekst Matthew Woehlke Negatiwny tekst Neutralny tekst Nowa linka Normalny pozadk Normalny tekst Opcije Pozitiwny tekst Barbnu šemu wumaznyć Wubranu šemu wumaznyć Barbnu šemu zawěsćić Pozadk wubraća Njeaktiwny wubrany tekst Wubrany tekst Sortěrowane stołpiki šatěrować Pozadk tooltipow Tekst tooltipow Wariacije Pozadk wobhladać Tekst wobhladać Wopytany tekst Pozadk wokna Tekst wokna Nimaće dowolnosć, tut šemu zničić. Nimaće dowolnosć, tutu šemu přepisać 