��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     -     1  �   O  R  6  [   �	     �	     �	     
  (   
     A
     Y
     r
     �
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: desktop_kdebase
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-11-13 21:27+0100
Last-Translator: Eduard Werner <edi.werner@gmx.de>
Language-Team: en_US <kde-i18n-doc@lists.kde.org>
Language: hsb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KAider 0.1
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;
 sek &Maksimalny čas za feedback: <H1>Zdźělenka w pasu nadawkow</H1>
Móžeće dalšu družinu zdźělenki připrawić; to je zdźělenje w pasu nadawkow,
hdźež so tłóčatko z wjerćacym pěskowym časniku jewi jako symbol za to,
zo so Waš program startuje. <h1>Zaběrany cursor</h1>
KDE poskića zaběrany cursor jako zdźělenku, zo je so program startował.
Za zaswěćenje móžeće družinu wisuelneho feedbacka z kašćika wubrać.
Je pak móžno, zo wěste programy to njepodpěruja.
W tutym padźe přestanje cursor blinkować po času, podatym we wotrězku 'Maksimalny čas za feedback'. <h1>Feedback za startowanje</h1>Tu móžeće feedback za startowanje programow připrawić. Blinkowacy cursor Skakacy cursor Zaběra&ny cursor Zdźělenku w &pasu nadawkow zaswěćić Žadyn zaběrany cursor Pasiwny zaběrany cursor &Maksimalny čas za feedback: Zdźělenka w &pasu nadawkow 