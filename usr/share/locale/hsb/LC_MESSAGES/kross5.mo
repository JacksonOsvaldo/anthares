��          �   %   �      p     q     �     �      �     �     �     �     �  ,   �  3   !     U     u     �     �     �  '   �     �     �     �          )     0     H     L     l  &   q     �  �  �     y  	   �     �     �     �     �     �     �  .     -   >  &   l      �     �  
   �     �  %   �     �               )     F     R     l  !   u     �  )   �     �                                                                                    
         	                              Add a new script. Add... Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2008-12-19 22:49+0100
Last-Translator: Eduard Werner <edi.werner@gmx.de>
Language-Team: en_US <kde-i18n-doc@lists.kde.org>
Language: hsb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;
X-Generator: KAider 0.1
 Nowy skript dodać. Dodać... Přispomnjenje: edi.werner@gmx.de Wobdźěłać Wubrany skript wobdźěłać. Wobdźěłać... Wubrany skript wuwjesć. Njemóžu skript stworić za interpreter "%1". Njemóžu interpreter za skript "%1" namakać Njemóžu interpreter "%1" startować. Njemóžu skript "%1" wočinić. Dataja: Piktogram: Interpreter: Stopjeń wěstosće Ruby-interpretera Edward Wornar Mjeno: Žana tajka funkcija '%1' Žadyn tajki interpreter: %1 Wotstronić Wubrany skript wumaznyć. Wuwjesć Skriptowa dataja %1 njeeksistuje. Stop Wuwjedźenje wubraneho skripta zastajić. Tekst: 