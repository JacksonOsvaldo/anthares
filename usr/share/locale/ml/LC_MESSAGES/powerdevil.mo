��          t      �              N     
   e      p     �  &   �  %   �  f   �     d     {  �  �     Q  8  k  +   �     �  R   �  x   5  �   �  =  4  _   r  Y   �         	                                      
              min Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. When laptop lid closed When power button pressed Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2009-12-31 01:31+0530
Last-Translator: Praveen Arimbrathodiyil <pravi.a@gmail.com>
Language-Team: Malayalam <smc-discuss@googlegroups.com>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=(n != 1);
  മിനിറ്റ് ബാറ്ററിയുടെ വിനിമയതലവുമായി ബന്ധപ്പെടാന്‍ കഴിഞ്ഞില്ല.
ദയവായി നിങ്ങളുടെ സിസ്റ്റത്തിന്റെ ക്രമീകരണം പരിശോധിയ്ക്കുക ഒന്നും ചെയ്യണ്ട pravi.a@gmail.com പ്രവീണ്‍ അരിമ്പ്രത്തൊടിയില്‍ വൈദ്യുതി അഡാപ്റ്റര്‍ കുത്തിയിരിയ്ക്കുന്നു. വൈദ്യുതി അഡാപ്റ്റര്‍ നീക്കം ചെയ്തിരിയ്ക്കുന്നു. "%1" എന്ന പ്രൊഫൈല്‍ തെരഞ്ഞെടുത്തിരിയ്ക്കുന്നു, പക്ഷേ അതു് നിലവിലില്ല.
ദയവായി പവര്‍ഡെവിളിന്റെ ക്രമീകരണം പരിശോധിയ്ക്കൂ. ലാപ്‌ടോപ്പിന്റെ അടപ്പു് അടച്ചാല്‍ വൈദ്യുതി ബട്ടന്‍ അമര്‍ത്തിയാല്‍ 