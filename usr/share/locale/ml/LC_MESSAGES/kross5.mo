��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �  !   �  T   �  !         "     B  b   _  +   �     �  .   n  �   �  �   %	    
  �   /  �   �     C     T  4   h  r   �  �          6     T   Q     �  �   �  B   K  Y   �     �  �        �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-12-29 15:32+0530
Last-Translator: 
Language-Team:  <en@li.org>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
X-Poedit-Country: INDIA
 പൊതുവായുള്ള പുതിയ സ്ക്രിപ്റ്റ് ചേര്‍ക്കുക. ചേര്‍ക്കുക... റദ്ദാക്കുക? അഭിപ്രായം: pravi.a@gmail.com, peter.ani@gmail.com, maxinbjohn@gmail.com, gnuanu@gmail.com, manusmad@gmail.com മാറ്റം വരുത്തുക തെരഞ്ഞെടുത്ത സ്ക്രിപ്റ്റില്‍ മാറ്റം വരുത്തുക. മാറ്റം വരുത്തുക... തെരഞ്ഞെടുത്ത സ്ക്രിപ്റ്റ് പ്രവര്‍ത്തിപ്പിക്കുക. "%1" എന്ന ഇന്റര്‍പ്രെട്ടറിനു് വേണ്ടിയുള്ള സ്ക്രിപ്റ്റ് സൃഷ്ടിയ്ക്കുന്നതില്‍ പരാജയപ്പെട്ടു "%1" എന്ന സ്ക്രിപ്റ്റ് ഫയലിനു് വേണ്ട ഇന്റര്‍പ്രെട്ടര്‍ ഏതെന്നു് നിശ്ചയിയ്ക്കുന്നതില്‍ പരാജയപ്പെട്ടു "%1" എന്ന ഇന്റര്‍പ്രറ്റര്‍ ലഭ്യമാക്കുന്നതില്‍ പരാജയം "%1" എന്ന സ്ക്രിപ്റ്റ്ഫയല്‍ തുറക്കുന്നതില്‍ പരാജയം ഫയല്‍: ചിഹ്നം: ഇന്റര്‍പ്രെറ്റര്‍: റൂബി ഇന്റര്‍പ്രെട്ടറിനു് വേണ്ട സുക്ഷാതലം പ്രവീണ്‍ അരിമ്പ്രത്തൊടിയില്‍, അനി പീറ്റര്‍, മാക്സിന്‍ ജോണ്‍, അനൂപ് പനവളപ്പില്‍, മനു എസ് മാധവ് പേര്: "%1" ഫംഗ്ഷന്‍ ലഭ്യമല്ല "%1" എന്നൊരു ഇന്റര്‍പ്രെറ്ററില്ല നീക്കുക തെരഞ്ഞെടുത്ത സ്ക്രിപ്റ്റ് നീക്കം ചെയ്തിരിക്കുന്നു. പ്രവര്‍ത്തിപ്പിയ്ക്കുക സ്ക്രിപ്റ്റ് ഫയല്‍ "%1" നിലവിലില്ല. നിര്‍ത്തുക തെരഞ്ഞെടുത്ത സ്ക്രിപ്റ്റിന്റെ പ്രവര്‍ത്തനം നിര്‍ത്തുക. വാചകം: 