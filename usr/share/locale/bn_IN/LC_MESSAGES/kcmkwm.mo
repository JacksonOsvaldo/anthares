��    <      �  S   �      (     )     -     5     A  )   S     }     �     �     �  	   �     �  4   �  X   �  Z   K  9   �  Z   �  ;   ;  Y   w  :   �               6     ?     P     a  0   i      �     �  .   �     	     	     	     +	     <	     V	     o	     �	     �	     �	     �	     �	     �	     �	     �	     
     #
     +
  
   ;
     F
     M
     T
     b
     h
     v
     ~
     �
  $   �
     �
     �
  �  �
     �  &   �     �  S   �  Y   +     �  H   �  H   �     3  0   F     w  w   {  �   �    �  �   �  �   �  �   }  �     �     %   �  k   �  $   $  7   I  7   �     �  �   �     �  Q   �  �        �  0   �  &     .   6  S   e  D   �  Z   �  Q   Y      �  >   �               .  !   H  l   j  1   �     	  5     "   S     v  "   �  &   �  "   �  1   �     (  7   >  @   v  W   �          "     5   :               <   "                           *              ,   8          0   $      4   (   /   1            2   6   -   	          9                   .                                      %   7         ;             
       3                        #       &       '       )   !   +        ms &Moving &Placement: &Titlebar Actions (c) 1997 - 2002 KWin and KControl Authors Activate Activate & Lower Activate & Raise Active Ad&vanced Alt Behavior on <em>double</em> click into the titlebar. Behavior on <em>left</em> click into the titlebar or frame of an <em>active</em> window. Behavior on <em>left</em> click into the titlebar or frame of an <em>inactive</em> window. Behavior on <em>left</em> click onto the maximize button. Behavior on <em>middle</em> click into the titlebar or frame of an <em>active</em> window. Behavior on <em>middle</em> click onto the maximize button. Behavior on <em>right</em> click into the titlebar or frame of an <em>active</em> window. Behavior on <em>right</em> click onto the maximize button. Bernd Wuebben C&lick raises active window Centered Cristian Tibirna Daniel Molkentin Dela&y: Display window &geometry when moving or resizing EMAIL OF TRANSLATORSYour emails Handle mouse wheel events Hide utility windows for inactive applications Inactive Keep Above/Below Left button: Matthias Ettrich Matthias Hoelzer-Kluepfel Matthias Kalle Dalheimer Maximize (horizontal only) Maximize (vertical only) Maximize Button Maximize/Restore Meta Middle button: Minimize Move Move to Previous/Next Desktop NAME OF TRANSLATORSYour names Nothing Operations Menu Pat Dowler Random Resize Right button: Shade Shade/Unshade Shading Waldo Bastian Window Actio&ns Window Behavior Configuration Module Windows Wynn Wilkes Project-Id-Version: kcmkwm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-07 16:17+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
  ms স্থানান্তরণ (&M) স্থাপনা: (&P) শিরোনামের বার সংক্রান্ত কর্ম (&T) (c) ১৯৯৭ - ২০০২ KWin ও KControl নির্মাতাবৃন্দ সক্রিয়করণ সক্রিয় করে নীচে স্থানান্তর সক্রিয় করে উপরে স্থানান্তর সক্রিয় উন্নত বৈশিষ্ট্য (&v) Alt শিরোনামের বারের উপর <em>দুইবার</em> ক্লিকের আচরণ। <em>সক্রিয়</em> উইন্ডোর শিরোনামের বার অথবা ফ্রেমের মধ্যে <em>বাঁদিকের</em> বাটন ক্লিক করার ফলে হওয়া আচরণ। <em>নিষ্ক্রিয়</em> উইন্ডোর শিরোনামের বার অথবা ফ্রেমের মধ্যে <em>বাঁদিকের</em> বাটন ক্লিক করার ফলে হওয়া আচরণ। বড় করার বাটনের উপর <em>বাঁদিকের</em> বাটন ক্লিকের ফলে আচরণ। <em>সক্রিয়</em> উইন্ডোর শিরোনামের বার অথবা ফ্রেমের মধ্যে <em>মাঝের</em> বাটন ক্লিক করার ফলে হওয়া আচরণ। বড় করার বাটনের উপর <em>মাঝের</em> বাটন ক্লিকের ফলে আচরণ। <em>সক্রিয়</em> উইন্ডোর শিরোনামের বার অথবা ফ্রেমের মধ্যে <em>ডানদিকের</em> বাটন ক্লিক করার ফলে হওয়া আচরণ। বড় করার বাটনের উপর <em>ডানদিকের</em> বাটন ক্লিকের ফলে আচরণ। বার্ন্ড উইবেন ক্লিকের ফলে সক্রিয় উইন্ডো উপরে করা হবে (&l) কেন্দ্রস্থিত ক্রিস্টিনা টিবির্না ড্যানিয়েল মোলকেনটিন বিলম্ব: (&y) উইন্ডো স্থানান্তর অথবা উইন্ডোর মাপ পরিবর্তনের সময় উইন্ডোর জ্যামিতি প্রদর্শন করা হবে (&g) runab@redhat.com মাউস উইলের ইভেন্ট ব্যবস্থাপনা নিষ্ক্রিয় অ্যাপ্লিকেশনের ক্ষেত্রে সহায়ক উইন্ডো বন্ধ করা হবে নিষ্ক্রিয় উপরে/নীচে রাখা হবে বাঁদিকের বাটন: ম্যাথায়েস এট্রিখ ম্যাথায়েস হোয়েলজার-ক্লুয়েপফেল ম্যাথায়েস কালে ডালহেইমার বড় করুন (শুধুমাত্র অনুভূমিক দিশায়) বড় করুন (শুধুমাত্র উলম্ব দিশায়) বড় করার বাটন সর্বোচ্চ/স্বাভাবিক মাপ Meta মাঝের বাটন: আড়াল করুন স্থানান্তরণ পূর্ববর্তী/পরবর্তী ডেস্কটপে স্থানান্তর রুণা ভট্টাচার্য্য কিছু না কর্ম সংক্রান্ত মেনু প্যাট ডাউলার যথেচ্ছ মাপ পরিবর্তন ডানদিকের বাটন: ছায়াবৃত করুন ছায়াবৃত/ছায়াবিহীন ছায়াবৃত ওয়াল্ডো ব্যাস্টিয়েন উইন্ডো সংক্রান্ত কর্ম (&n) উইন্ডোর আচরণ কনফিগারেশনের মডিউল উইন্ডো উইন উইলক্স 