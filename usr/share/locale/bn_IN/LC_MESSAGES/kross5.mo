��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  B   �       +   &     R     i     z  ]   �     �  ]     �   k  �   �  V   }	  2   �	     
     
  (   #
  }   L
  1   �
  
   �
  3     H   ;     �  W   �     �  b        r  p   �     �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-12-28 14:35+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali INDIA <anubad@lists.ankur.org.in>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
 সাধারণ নতুন স্ক্রিপ্ট যোগ করুন। যোগ করুন... বাতিল করা হবে কি? মন্তব্য: runabh@gmail.com সম্পাদনা নির্বাচিত স্ক্রিপ্ট সম্পাদন করুন। সম্পাদনা... নির্বাচিত স্ক্রিপ্ট সঞ্চালন করুন। ইন্টারপ্রেটার "%1"-র জন্য স্ক্রিপ্ট নির্মাণ করতে ব্যর্থ scriptfile "%1"-র জন্য ইন্টারপ্রেটার নির্ধারণ করতে ব্যর্থ ইন্টারপ্রেটার "%1" লোড করতে ব্যর্থ scriptfile "%1" খুলতে ব্যর্থ ফাইল: আইকন: ইন্টারপ্রেটার: Ruby ইন্টারপ্রেটারের ক্ষেত্রে নিরাপত্তার মাত্রা রুণা ভট্টাচার্য্য নাম: ফাংশান "%1" অনুপস্থিত ইন্টারপ্রেটাট "%1" অনুপস্থিত মুছে ফেলুন নির্বাচিত স্ক্রিপ্ট মুছে ফেলুন। সঞ্চালন "%1" স্ক্রিপ্ট-ফাইল বর্তমানে অনুপস্থিত বন্ধ করুন নির্বাচিত স্ক্রিপ্টের সঞ্চালন বন্ধ করুন। টেক্সট: 