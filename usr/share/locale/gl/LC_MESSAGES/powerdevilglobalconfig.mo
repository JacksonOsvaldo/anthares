��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8           "     3     A     [  >   o  ;   �     �     
     %     4     I     R     [  +   s     �  #   �     �  	   �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-10-03 14:28+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % Nivel &crítico: Nivel &baixo: <b>Niveis de batería</b> &No nivel crítico: A batería estará no nivel crítico en canto acade este nivel A batería estará no nivel baixo en canto acade este nivel Configurar as notificacións… Nivel crítico de batería Non facer nada mvillarino@gmail.com Activado Hibernar Nivel baixo da batería Nivel baixo para dispositivos periféricos: Marce Villarino Deter os reprodutores ao suspender: Apagar Suspender Parece que o servizo de xestión da enerxía non se está a executar.
Isto só se pode arranxar iniciando ou planificándoo en «Arrinque e apagado» 