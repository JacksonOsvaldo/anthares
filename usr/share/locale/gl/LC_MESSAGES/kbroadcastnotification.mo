��          �      �       0     1  ,   J     w  O   �     G      _     �  =   �     �  H   �  !   @     b  �         '   4  �   \  W   �     T     n       >   �     �  2   �  "        :        
                                        	          (c) 2016 Kai Uwe Broulik A brief one-line summary of the notification A comma-separated list of user IDs this notification should be sent to. If omitted, the notification will be sent to all users. A tool that emits a notification for all users by sending it on the system DBus Broadcast Notifications EMAIL OF TRANSLATORSYour emails Icon for the notification Keep the notification in the history until the user closes it NAME OF TRANSLATORSYour names Name of the application that should be associated with this notification The actual notification body text Timeout for the notification Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:52+0100
PO-Revision-Date: 2017-07-25 11:00+0100
Last-Translator: Adrián Chaves <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 © 2016 Kai Uwe Broulik Un resumo da notificación nunha liña. Unha lista separada por comas dos identificadores dos usuarios aos que enviar a notificación. Se non se indica, a notificación enviarase a todos os usuarios. Ferramenta para difundir notificacións a todos os usuarios mediante o DBus do sistema. Difusor de notificacións adrian@chaves.io Icona da notificación. Manter a notificación no historial ata que o usuario a peche. Adrian Chaves Nome do aplicativo para asociar coa notificación. O texto do corpo da notificación. Tempo máximo da notificación. 