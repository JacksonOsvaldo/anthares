��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �     n     �     �     �     �     �     �     �  
   �  
   �     �     �                    4  ?   <  H   |     �     �     �  .   �     *     1     K  \   S  O   �        "        ;     D  0   K  %   |     �     �  $   �  %   �                 ,     %   I     o  -   �  !   �     �  &   �          	       	   *  H   4  /   }     �  "   �     �  $        &  "   @     c  1   u  %   �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-01-18 22:00+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Propiedade da xanela a casar:  Enorme Grande Sen moldura Sen moldura lateral Normal Sobredimensionada Pequerrecha Descomunal Moi grande Grandes Normais Pequenos Moi grandes Brillo da xanela activa Engadir Engadir unha asa para cambiar o tamaño das xanelas sen moldura Permitir cambiar de tamaño as xanelas maximizadas desde os seus bordos. Animacións Tamaño dos b&otóns: Tamaño da moldura: Transición do botón ao pasar o rato por riba Centro Centrado (toda a anchura) Clase:  Configura o resalte entre sombreado e brillante cando a xanela cambia o estado de actividade Configura a animación de realce dos botóns da xanela ao pasar o rato por riba Opcións da decoración &Detectar as propiedades da xanela Diálogo Editar Editar a excepción — Configuración de Oxygen Activar ou desactivar esta excepción Tipo da excepción Xeral Agochar a barra de título da xanela Información sobre a xanela escollida Esquerda Baixar Subir Nova excepción — Configuración de Oxygen Pregunta — Configuración de Oxygen Expresión regular A sintaxe da expresión regular é incorrecta Expresión &regular para buscar:  Retirar Desexa retirar a excepción escollida? Dereita Sombras &Aliñamento do título: Título:  Usar as mesmas cores para a barra do título e para o contido da xanela. Usar a clase de xanela (para todo o aplicativo) Usar o título da xanela Aviso — Configuración de Oxygen Nome da clase da xanela Sombra dos despregábeis das xanelas Identificación da xanela Selección da propiedade da xanela Título da xanela Transicións de cambio do estado activo da xanela Substitucións específicas da xanela 