��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  A     �   _  r   �     j  4   q     �     �  �   �     �     �     �  	   �     �     �  
   �  *        ,     A     U      \     }  ?   �  *   �             ^   7     �  #   �     �  �   �     �     �  "   �     �  
   �  
   �     �       Z    &   a      �     �  �   �     _     |     �     �  '   �  	   �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-18 21:55+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 «Repintar toda a pantalla» pode causar problemas de rendemento. «Só cando sexa barato» só evita o efecto bandeira nos cambios que afectan a toda a pantalla, como cando se reproduce un vídeo a pantalla completa. «Reutilizar o contido da pantalla» causa graves problemas de rendemento cando se utilizan os controladores MESA. Exacto Permitir que os aplicativos bloqueen a composición. Sempre Velocidade das animacións: Os aplicativos poden definir unha pista para bloquear a composición cando se abre a xanela.
Isto mellora o rendemento, por exemplo, nos xogos.
As regras específicas de xanelas poden definir un valor distinto para esta opción. Autor: %1
Licenza: %2 Automático Accesibilidade Aparencia Adornos Foco Utilidades Animación do cambio de escritorio virtual Xestión das xanelas Configurar o filtro Rugoso mvillarino@users.sourceforge.net Activar o compositor no inicio Excluír os efectos do escritorio non admitidos polo compositor Excluír os efectos do escritorio internos Repintar toda a pantalla Obter novos efectos… Consello: para descubrir ou configurar como activar un efecto, consulte as opcións do efecto. Instantánea O equipo de desenvolvemento de KWin Manter as miniaturas da xanela: Manter sempre a miniatura da xanela interfire co estado de minimización das xanelas. Isto pode facer que as xanelas non suspendan o seu procesamento mentres están minimizadas. Marce Villarino Nunca Só para as xanelas que se mostran Só cando sexa barato OpenGL 2.0 OpenGL 3.1 EGL GLX A composición de OpenGL (a predeterminada) quebrou KWin no pasado.
Posibelmente se debese a un fallo no controlador.
Se cre que mentres tanto anovou o controlador a unha versión estábel, pode quitar esta protección pero queda avisado de que isto pode causar unha quebra inmediata.
Como alternativa pode usar no canto a infraestrutura XRender. Activar de novo a detección de OpenGL Reutilizar o contido da pantalla Infraestrutura de debuxado: Non calquera hardware é compatíbel co método de cambio de escala «Certeiro», e o método pode causar problemas de rendemento e debuxado de obxectos. Método de cambio de escala: Buscar Suave Suave (máis lento) Prevención do efecto bandeira (vsync): Moi lenta XRender 