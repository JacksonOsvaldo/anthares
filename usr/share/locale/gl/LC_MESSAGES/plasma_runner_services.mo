��          D      l       �      �   6   �   a   �      /  �  ?     =  D   I     �     �                          Applications Finds applications whose name or description match :q: Jump list search result, %1 is action (eg. open new tab), %2 is application (eg. browser)%1 - %2 System Settings Project-Id-Version: plasma_runner_services
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-14 03:09+0100
PO-Revision-Date: 2017-08-13 09:18+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Aplicativos Atopa aplicativos que teñan un nome ou descrición que case con :q: %1 - %2 Configuración do sistema 