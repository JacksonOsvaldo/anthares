��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     v     �     �     �     �     �     �     �     �               "     :     O     V     _     k     r     y  
   {     �     �     �     �     �     �     �     �               5     D  J   W  O   �     �                     %     ;     M     R     a     s     �     �     �  
   �     �     �  	   �     �     �            V   !  �   x           '     B     N     e     |     �     �  	   �     �     �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2018-01-18 22:01+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Actividades Engadir unha acción Engadir un espazador Engadir trebellos… Alt Trebellos alternativos Sempre visíbel Aplicar Aplicar a configuración Aplicar agora Autor: Agochar automaticamente Botón de retroceder Abaixo Cancelar Categorías Centro Pechar + Configurar Configurar a actividade Crear unha actividade… Ctrl En uso actualmente Eliminar Correo electrónico: Botón de avanzar Obter novos trebellos Altura Desprazamento horizontal Escribir aquí Atallos de teclado Non se pode cambiar a disposición mentres os trebellos estean bloqueados. Os cambios de disposición deben aplicarse antes de poder facer outros cambios. Disposición: Á esquerda Botón esquerdo Licenza: Bloquear os trebellos Maximizar o panel Meta Botón central Máis opcións… Accións do rato Aceptar Aliñamento do panel Retirar o panel Á dereita Botón secundario Bordo da pantalla Buscar… Maiús Deter a actividade Actividades detidas: Cambiar Cambiouse a configuración do módulo activo. Quere aplicar os cambios ou descartalos? Este atallo activará o miniaplicativo, é dicir, porá o foco de teclado no miniaplicativo e se este ten unha xanela emerxente (como o menú de inicio) esta abrirase. Arriba Desfacer a desinstalación Desinstalar Desinstalar o trebello Desprazamento vertical Visibilidade Fondo de escritorio Tipo de fondo de escritorio: Trebellos Anchura As xanelas poden cubrilo As xanelas van en baixo 