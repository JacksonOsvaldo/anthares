��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (       F  A   J     �     �     �  ?   �     �  3   	     S	  �   Y	  �   
  )   �
       (   )  #   R  &   v  ;   �     �     �  "     &   0                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-07-29 11:06+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 1 acción para este dispositivo %1 accións para este dispositivo %1 libre Estase a acceder… Todos os dispositivos Prema para acceder a este dispositivo desde outros aplicativos. Prema para expulsar este disco. Prema para retirar con seguridade este dispositivo. Xeral Na actualidade <b>non é seguro</b> retirar este dispositivo: pode haber aplicativos accedendo a el. Prema o botón de expulsar para retirar con seguranza este dispositivo. Na actualidade <b>non é seguro</b> retirar este dispositivo: pode haber aplicativos accedendo a outros volumes do dispositivo. Prema o botón de expulsar deses outros volumes para retirar con seguranza este dispositivo. Agora é seguro retirar este dispositivo. O dispositivo máis recente Non hai ningún dispositivo dispoñíbel Só os dispositivos non extraíbeis Configurar os dispositivos extraíbeis Abrir unha xanela emerxente ao conectar dispositivos novos. Só os dispositivos extraíbeis Estase a retirar… Este dispositivo está accesíbel. Este dispositivo non está accesíbel. 