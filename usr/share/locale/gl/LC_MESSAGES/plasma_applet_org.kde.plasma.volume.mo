��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �     �     �     	  	   	     $	     3	     Q	     a	     n	     t	     {	     �	     �	  	   �	     �	     �	  8   �	  2   
     R
     o
     �
     �
     �
  $   �
     �
     �
               *     2     9     >        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-01-18 22:09+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % Axustar o volume de %1. Aplicativos Son silenciado Volume do son Comportamento Dispositivos de captura Secuencias de captura Silenciar Predeterminada Baixar o volume do micrófono Baixar o volume Dispositivos Xeral Portos Subir o volume do micrófono Subir o volume Volume máximo: Silenciar Silenciar %1. Silenciar o micrófono Non hai ningún aplicativo reproducindo ou gravando son. Non se atoparon dispositivos de saída ou entrada. Dispositivos de reprodución Secuencias de reprodución  (non dispoñíbel) (desenchufado) Aumentar o volume máximo Mostrar opcións adicionais para %1. Volume Volume ao %1% Información do volume Volume por nivel: %1 (%2) %1: %2 100% %1% 