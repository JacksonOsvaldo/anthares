��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �  
   A     L     g  J     %   �     �  "   �     !        	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-10-12 11:53+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Desactivar Desactivar a área táctil Activar a área táctil Non se detectou ningún rato.
Seguro que quere desactivar a área táctil? Non se atopou ningunha área táctil. Área táctil A área táctil está desactivada. A área táctil está activada. 