��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
     �
     �
           $     +  
   :  #   E     i     �     �  +   �     �     �      �          #     2  	   R     \     d  (   p     �  	   �  5   �     �     �  	             -  /   4     d  	   ~     �     �     �  	   �     �     �     �     �  "        $     +     ;  #   C               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-18 22:22+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Volver Batería ao %1% Cambiar a disposición Teclado virtual Cancelar As maiúsculas están bloqueadas Pechar Pechar a busca Configurar Configurar os complementos de busca Sesión de escritorio: %1 Usuario distinto Disposición do teclado: %1 Sairase en 1 segundo Sairase en %1 segundos Identificación Fallou o acceso Acceder como un usuario distinto Saír Seguinte pista Non se está a reproducir nada. Non usado Aceptar Contrasinal Reproducir ou deter o contido multimedia Pista anterior Reiniciar Reiniciarase en 1 segundo Reiniciarase en %1 segundos Consultas recentes Retirar Reiniciar Mostrar os controis multimedia: Apagar Apagarase en 1 segundo Apagarase en %1 segundos Comezar unha sesión nova Suspender Cambiar Cambiar de sesión Cambiar de usuario Buscar… Buscar «%1»… Plasma, por KDE Desbloquear Fallou o desbloqueo no terminal (TTY) %1 (pantalla %2) TTY %1 Nome de usuario %1 (%2) na categoría de consultas recentes 