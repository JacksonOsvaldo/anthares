��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l  	             0     ?     F     M     Z     s  	   �  	   �     �     �     �  
   �  	   �  	   �     �     �     �     �               2     7     F     R     ^     r  $   �     �     �     �     �       (        4     K     b     p  "   }     �     �     �     �     �     �     �     �     �  	   �       &        2     7     @     H  H   Y     �     �     �     �  	   �  
                       4     F     ]     v  "   �     �  4   �      �  ,        I     Q     Y     a     m     y          �     �     �     �     �     �     �     �     �          2  I   Q     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-01-18 22:04+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Eliminar &Baleirar o lixo &Botar no lixo &Abrir &Pegar &Propiedades &Actualizar o escritorio &Actualizar a vista &Recargar &Renomear Escoller… Borrar a icona Aliñar Aparencia: Dispor en Dispor en Disposición: Volver Cancelar Columnas Configurar o escritorio Título personalizado Data Predeterminada Descendente Descrición Anular a selección Disposición do escritorio Insira aquí o título personalizado Funcionalidades: Padrón do nome do ficheiro: Tipo de ficheiro Tipos de ficheiro: Filtro Axudiñas da vista previa dos cartafoles Primeiro os cartafoles Primeiro os cartafoles Ruta completa Xa o collín Agochar os ficheiros que casen con Enorme Tamaño das iconas Iconas Grande Á esquerda Lista Lugar Lugar: Bloquear no sitio Bloqueado Mediana Máis opcións de previsualización… Nome Ningunha Aceptar Botón do panel: Premer un trebello e manter premido para movelo e mostrar as súas asas. Vistas previas dos complementos Miniaturas da vista previa Retirar Cambiar o tamaño Restaurar Á dereita Rotar Filas Buscar ficheiros do tipo… Seleccionalo todo Seleccionar o cartafol Marcadores de selección Mostrar todos os ficheiros Mostrar os ficheiros que casen con Mostrar un lugar: Mostrar os ficheiros asociados coa actividade actual Mostrar o cartafol do escritorio Mostrar a caixa de ferramentas do escritorio Tamaño Pequeno Pequena Ordenar por Ordenar por Orde: Especifique un cartafol: Liñas de texto Pequerrecha Título: Axudas Axustes Tipo Escriba aquí unha ruta ou URL Sen ordenar Usar unha icona personalizada. Xestión dos trebellos Desbloqueáronse os trebellos. Prema un trebello e manteña premido para movelo e mostrar as súas asas. Modo de vista 