��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  2        C  	   X     b     k  	   �     �     �     �     �     �     �     �     �            3   2     f     |     �  -   �     �     �     �     �          $     :     Q     U  5   ^  	   �  	   �  
   �  
   �  
   �     �     �  
   �     �                (     -     E     b  	   f     p  
   �     �     �     �     �     �     �     �     �  	                  #     (     -     >  "   \          �     �  %   �     �                      H   %     n     �  
   �     �     �     �  
   �     �     �     �  
   �     �                    )     6           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2017-10-22 13:29+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Módulo de visor de dispositivos baseado en Solid © 2010 David Hubner AMD 3DNow ATI IVEC %1 de %2 libres (%3% empregado) Baterías Tipo de batería:  Bus:  Cámara Cámaras Estado de carga:  Estase a cargar Pregalo todo Lector de Compact Flash Un dispositivo. Información do dispositivo Mostra todos os dispositivos que aparecen na lista. Visor de dispositivos Dispositivos Estase a descargar xosecalvo@gmail.com, mvillarino@kde-espana.es Cifrado Expandilo todo Sistema de ficheiros Tipo de sistema de ficheiros:  Carga completa Unidade de disco duro Conectábel en quente? IDE IEEE1394 Mostra información sobre o dispositivo seleccionado. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Teclado Teclado + Rato Etiqueta:  Velocidade máxima:  Lector de lapis de memoria Montado en:  Rato Reprodutores multimedia Xosé Calvo, Marce Villarino Non Sen carga Non se dispón de datos Desmontado Non definida Unidade óptica PDA Táboa de particións Primario Procesador %1 Número de procesadores:  Procesadores Produto:  Raid Extraíbel? SATA SCSI Lector de SD/MMC Mostrar todos os dispositivos Mostrar os dispositivos relevantes Lector de soportes intelixentes Unidades de almacenamento Controladores admitidos:  Conxuntos de instrucións admitidas:  Protocolos admitidos:  UDI:  UPS USB UUID:  Mostra o UDI do dispositivo actual (Identificador único de dispositivo) Unidade descoñecida Sen utilizar Vendedor:  Tamaño do volume: Uso do volume:  Si kcmdevinfo Descoñecido Ningún Ningunha Plataforma Descoñecido Descoñecido Descoñecido Descoñecido Descoñecido Lector de tarxetas xD 