��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �       A     '   [  K   �  K   �  D        `  
   {  Q   �     �     �  %     *   2     ]     u     �     �     �     �     �     �     �          #     9     @     I  %   U      {      �  ;   �  J   �  (   D     m     �  E   �  	   �  	   �  /   �       $   -     R     ^     g     t     �  	   �     �     �     �  .   �  $   �       )        E     ^     t     }  Z   �  6   �  	   '     1     A     I  	   \     f  A   w     �     �  	   �  	   �     �     �               5     >     T     [     z     �  ,   �     �     �     �  %   �               0     @  	   P     Z  !   m     �  0   �         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-18 22:33+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Tamén dispoñíbel en %1 %1 %1 (%2) %1 (predeterminado) <b>%1</b> por %2 <em>A %1 de %2 persoas serviulles esta crítica para decidir</em> <em>Cóntanos algo desta crítica!</em> <em>Foille útil? <a href='true'><b>Si</b></a>/<a href='false'>Non</a></em> <em>Foille útil? <a href='true'>Si</a>/<a href='false'><b>Non</b></a></em> <em>Foiche útil? <a href='true'>Si</a>/<a href='false'>Non</a></em> Obtendo as actualizacións Obtendo… Descoñécese cando foi a última vez que se comprobou se había actualizacións. Buscando actualizacións… Non hai actualizacións Non hai actualizacións dispoñíbeis Debería comprobar se hai actualizacións. O sistema está ao día Actualizacións Actualizando… Aceptar Engadir unha fonte… Complementos Aleix Pol Gonzalez Un explorador de aplicativos Aplicar os cambios Infraestruturas dispoñíbeis:
 Modos dispoñíbeis:
 Atrás Cancelar Categoría: Comprobando se hai actualizacións… O comentario é demasiado longo. O comentario é demasiado curto. Modo de visualización (automático, compacto ou completo). Non se puido pechar o aplicativo, é necesario completar algunhas tarefas. Non se puido atopar a categoría «%1». Non se puido abrir %1 Eliminar a orixe Abre directamente o aplicativo especificado polo seu nome de paquete. Descartar Descubrir Mostra unha lista de entradas cunha categoría. Extensións… Non se puido retirar a fonte «%1». Recomendado Axuda… Páxina web: Mellorar o resumo. Instalar Instalado Jonathan Thomas Iniciar Licenza: Listar todas as infraestruturas dispoñíbeis. Listar todos os modos dispoñíbeis. Cargando… O ficheiro local de paquete para instalar Converter en predefinido Máis información… Máis… Non hai actualizacións Abrir Discover nun modo dito. Os modos correspóndense cos botón da barra de ferramentas. Abrir un programa que poida xestionar o mimetype dado. Proseguir Cualificación: Retirar Recursos de «%1» Revisión Revisando «%1» Non se recomenda executar como <em>root</em>, e non é necesario. Buscar Buscar en «%1»… Buscar… Busca: %1 Busca: %1 + %2 Configuración Resumo breve… Mostrar as recensións (%1)… Tamaño: Non se atopou nada… Orixe: Especificar a nova fonte de %1 Continúa a busca… Resumo: Compatíbel co esquema de URL «appstream:» Tarefas Tarefas (%1%) %1 %2 Non foi posíbel atopar o recurso: %1 Actualizalo todo Actualizar os escollidos Actualizar (%1) Actualizacións Versión: autor descoñecido actualizacións non seleccionadas actualizacións seleccionadas © 2010-2016 Equipo de desenvolvemento de Plasma 