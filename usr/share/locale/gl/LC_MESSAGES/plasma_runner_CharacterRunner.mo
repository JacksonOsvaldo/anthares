��    
      l      �       �      �            	               .  I   3     }  	   �  �  �          �     �     �  (   �     �  \   �     F     Z               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-01-01 18:31+0100
Last-Translator: Adrian Chaves Fernandez <adriyetichaves@gmail.com>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Palabra &desencadeante: Engadir un elemento Alias Alias: Configuración do executor de caracteres Código Crea caracteres a partir de :q: se se trata dun código hexadecimal ou un alias predefinido. Eliminar o elemento Código hexadecimal: 