��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �               !     (     9     @     H     O     i     �     �     �  8   �  8        <     I     W     l     �     �  "   �     �     �     �     �               8     E     X     j     n     �     �     �  
   �     �  )   �            
   /  (   :     c     r     �     �  	   �     �     �     �     �       0     %   E  	   k     u     |     �     �     �     �     �     �  +   �  +     )   A  "   k  F   �     �     �     �     �  
        #     )     /     6     L  $   _        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-18 22:09+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Todos os escritorios &Pechar Pantalla &completa &Mover &Novo escritorio &Fixar &Sombra &%1 %2 Tamén dispoñíbel en %1 Engadir á actividade actual Todas as actividades Permitir agrupar este programa Alfabeticamente Organizar as tarefas sempre en columnas de tantas filas. Organizar as tarefas sempre en filas de tantas columnas. Disposición Comportamento Segundo a actividade Segundo o escritorio Segundo o nome do programa Pechar a xanela ou grupo Cambiar de tarefa coa roda do rato Non agrupar Non ordenar Filtros Esquecer os documentos recentes Xeral Agrupamento e ordenamento Agrupamento: Realzar as xanelas Tamaño de icona: — Manter enrib&a das outras Manter &baixo as outras Manter os iniciadores separados Grande Ma&ximizar Manualmente Marcar os aplicativos que reproducen son. Máximo de columnas: Máximo de filas: Mi&nimizar Minimizar ou restaurar a xanela ou grupo Máis accións Mover ao escritorio &actual Mover á &actividade Mover ao &escritorio Silenciar Nova instancia En %1 En todas as actividades Na actividade actual Botón central: Só agrupar cando o xestor de tarefas estea cheo Abrir os grupos en xanelas emerxentes Restaurar 9.999+ Pór en pausa Pista seguinte Pista anterior Saír Cambiar de &tamaño Soltar %1 lugar máis %1 lugares máis Mostrar só as tarefas na actividade actual Mostrar só as tarefas no escritorio actual Mostrar só as tarefas na pantalla actual Mostrar só as tarefas minimizadas Mostrar a información de progreso e de estado nos botóns de tarefas. Mostrar as axudas Pequeno Ordenación: Iniciar unha nova instancia Reproducir Deter Nada. &Fixar Agrupar ou desagrupar Dispoñíbel en %1 Dispoñíbel en todas as actividades 