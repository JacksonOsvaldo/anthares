��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  -   �       '        @     S     i     �     �     �     �     �     �  
   �     �     �               1  &   N  "   u     �     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2017-07-29 11:21+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 xanela minimizada: %1 xanelas minimizadas: %1 xanela: %1 xanelas: …e %1 xanela máis e %1 xanelas máis Nome da actividade Número da actividade Engadir un escritorio virtual Configurar os escritorios… O nome do escritorio O número do escritorio Mostrar: Non fai nada Xeral Horizontal Iconas Disposición: Ningún texto Só a pantalla actual Retirar o escritorio virtual Estase a escoller o escritorio actual: Mostrar o xestor de actividades… Mostra o escritorio Predeterminada Vertical 