��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %          .     H     _     }     �  	   �     �     �     �     �     �  L   �     5     S     r  	   y  
   �  	   �  $   �  %   �  &   �  '   
     2     O                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-08-09 22:55+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 ficheiro %1 ficheiros %1 cartafol %1 cartafoles Procesáronse %1 de %2 Procesáronse %1 de %2 a %3/s Procesouse %1 Procesouse %1 a %2/s Aparencia Comportamento Cancelar Limpar Configurar… Tarefas rematadas Lista de transferencias ou tarefas sobre ficheiros en execución (kuiserver) Movelas a unha lista distinta Movelas a unha lista distinta. Pausar Retiralas Retiralas. Continuar Mostrar todas as tarefas nunha lista Mostrar todas as tarefas nunha lista. Mostrar todas as tarefas nunha árbore Mostrar todas as tarefas nunha árbore. Mostrar en xanelas separadas Mostrar en xanelas separadas. 