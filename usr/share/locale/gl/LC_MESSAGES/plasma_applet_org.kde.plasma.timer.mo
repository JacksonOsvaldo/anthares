��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %    {     }     �  
   �     �     �  	   �     �     �     �     �  6        B     U     \     x     �     �     �     �     �     �  u   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-26 17:50+0200
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 estase a executar %1 non se está a executar &Reiniciar &Comezar Avanzado Aparencia Orde: Visualización Executar unha orde Notificacións Tempo restante: %1 segundo Tempo restante: %1 segundos Executar unha orde &Deter Mostrar unha notificación. Mostrar os segundos Mostrar o título Texto: Temporizador O temporizador rematou O temporizador está en marcha Título: Use a roda do rato para cambiar os díxitos ou escolla unha cantidade de tempo predefinida desde o menú de contexto. 