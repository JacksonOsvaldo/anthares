��          L      |       �   `   �      
          "     *  �  ;  n   9     �     �     �     �                                         Finds local directories and files, network locations and Internet sites with paths matching :q:. Go to %1 Launch with %1 Open %1 Send email to %1 Project-Id-Version: krunner_locationsrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-08-13 09:18+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Atopa directorios e ficheiros locais, lugares na rede e sitios de internet que teñan rutas que casen con :q:. Ir a %1 Iniciar con %1 Abrir %1 Enviar un correo a %1 