��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  �  :     �"     �"     �"  $   #  1   *#     \#  ?   d#  #   �#     �#  
   �#     �#  B   $  +   G$     s$     �$     �$     �$     �$     �$     �$     %     "%     (%  @   ?%     �%  J   �%     �%     �%     &  �   &  &   �&     '     �'  "   �'     �'     �'     �'      (     (  4   (     O(  Q   X(  1   �(  K   �(  6   ()  9   _)  =   �)  '   �)  B   �)  9   B*  %   |*  '   �*     �*  ;   �*  (   +     H+  E   ^+  ]   �+     ,     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-01-18 22:47+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Nota de seguranza:</b>
                             Segundo unha auditoría de seguranza por Taylor Hornby (Defuse Security),
                             a codificación actual de Encfs é vulnerábel ou potencialmente vulnerábel
                             a varios tipos de ataque.
                             Por exemplo, un atacante con acceso de lectura e escritura aos datos cifrados
                             podería reducir a complexidade do descifrado para datos cifrados posteriormente
                             sen que o usuario real se decate, ou podería usar análises de tempo para
                             deducir información.
                             <br /><br />
                             Isto significa que non debería sincronizar os datos cifrados con ningún servizo
                             de almacenamento na nube, nin usalos noutras circunstancias nas que o atacante
                             poida acceder frecuentemente os datos cifrados.
                             <br /><br />
                             Consulte <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> (en inglés) para máis información. <b>Nota de seguranza:</b>
                             CryFS cifra os ficheiros, así que pode almacenalos con seguranza
                             en calquera lugar.
                             Funciona ben en combinación con servizos na nube como Dropbox,
                             iCloud, OneDrive, etc.
                             <br /><br />
                             A diferenza dalgunhas outras solucións de capa de sistema de
                             ficheiros, non expón nin a estrutura de directorios, nin o número                              de ficheiros nin os seus tamaños a través do formato de cifraxe de
                             datos.
                             <br /><br />
                             Porén, cómpre ter en conta que aínda que CryFS se considera seguro,
                             non existe ningunha auditoría independente que poida confirmalo. Crear unha nova caixa forte Actividades Infraestrutura: Non se pode crear o punto de montaxe Non poden abrirse as caixas fortes descoñecidas. Cambiar Escolla o sistema de cifrado que quere usar para a caixa forte: Escolla o sistema de cifraxe usado: Pechar a caixa forte Configurar Configurar a caixa forte… Non se pode crear unha instancia da infraestrutura configurada: %1 A infraestrutura configurada non existe: %1 Atopouse a versión correcta. Crear Crear unha nova caixa forte… CryFS O dispositivo xa está aberto. O dispositivo non está aberto. Diálogo Non volver mostrar esta nota. EncFS Lugar de datos cifrado Non se puideron crear os directorios, comprobe os seus permisos. Non se puido executar. Non se puido obter a lista de aplicativos que están a usar a caixa forte. Non se puido abrir: %1 Pechada forzosamente Xeral Se limita a caixa forte a certas actividades, mostrarase no miniaplicativo só cando estea nesas actividades. Ademais, cando cambie a unha actividade na que non debería estar dispoñíbel, pecharase automaticamente. Limitar ás actividades seleccionadas: Teña en conta que non hai xeito de recuperar un contrasinal esquecido. Se esquece o contrasinal, non poderá acceder ao datos. Punto de montaxe Non se indicou un punto de montaxe Punto de montaxe: Seguinte Abrir co xestor de ficheiros Contrasinal: Plasma Vault Introduza o contrasinal para abrir esta caixa forte: Anterior O directorio de punto de montaxe non está baleiro, non se abrirá a caixa forte. A infraestrutura indicada non está dispoñíbel. A configuración da caixa forte só pode modificarse mentres está pechada. A caixa forte é unha descoñecida, non pode pecharse. A caixa forte é unha descoñecida, non pode destruírse. O dispositivo xa está rexistrado. Non se pode crear de novo. O directorio xa contén datos cifrados. Non foi posíbel pechar a caixa forte, hai un aplicativo usándoa. Non foi posíbel pechar a caixa forte, %1 está usándoa. Non foi posíbel detectar a versión. Non foi posíbel realizar a operación. Dispositivo descoñecido Erro descoñecido, non foi posíbel crear a infraestrutura. Usar o sistema de cifraxe predeterminado Nome da &caixa forte: A versión instalada é incorrecta. A versión necesaria é %1.%2.%3. Debe seleccionar directorios baleiros para o almacenamento cifrado e para o punto de montaxe. %1: %2 