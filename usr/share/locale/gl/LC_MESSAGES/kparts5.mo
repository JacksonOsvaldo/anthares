��          �   %   �      `     a  :   i     �     �  =   �          *     G     [     t     �  6   �     �     �  #   �           >     F     T     d     �     �  ;   �  V   �  G   %     m  �  v     s  ,   {     �     �  1   �     �     �       
             (  .   6     e     m  !   �  >   �     �     �     �  1   	     D	     M	  ;   Y	  J   �	  F   �	     '
                                                  
                                                                   	       &Search <qt>Do you want to search the Internet for <b>%1</b>?</qt> @action:inmenuOpen &with %1 @infoOpen '%1'? @info:whatsthisThis is the file name suggested by the server @label File nameName: %1 @label Type of fileType: %1 @label:button&Open @label:button&Open with @label:button&Open with %1 @label:button&Open with... @label:checkboxRemember action for files of this type Accept Close Document Do you really want to execute '%1'? EMAIL OF TRANSLATORSYour emails Execute Execute File? Internet Search NAME OF TRANSLATORSYour names Reject Save As The Download Manager (%1) could not be found in your $PATH  The document "%1" has been modified.
Do you want to save your changes or discard them? Try to reinstall it  

The integration with Konqueror will be disabled. Untitled Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2017-07-27 08:43+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde, development
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Buscar <qt>Quere buscar <b>%1</b> en internet?</qt> &Abrir con %1 Abrir «%1»? Este é o nome de ficheiro suxerido polo servidor Nome: %1 Tipo: %1 &Abrir &Abrir con &Abrir con %1 &Abrir con… Lembrar a acción para os ficheiros deste tipo Aceptar Pechar o documento Seguro que quere executar «%1»? xosecalvo@gmail.com, mvillarino@gmail.com, proxecto@trasno.gal Executar Executar o ficheiro? Busca por internet Xabier García Feal, marce villarino, Xosé Calvo Rexeitar Gardar como Non se puido atopar o Xestor de descargas (%1) no seu $PATH O documento «%1» foi modificado.
Quere gardar os cambios ou descartalos? Intente instalalo de novo

Desactivarase a integración con Konqueror. Sen título 