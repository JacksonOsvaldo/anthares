��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  4   }     �     �     �  "   �     
  J     7   j  "   �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-03-15 14:50+0100
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 O intento de facer «%1» fallou coa mensaxe «%2». Seguinte pista Pista anterior Controlador multimedia Reproducir ou deter a reprodución Deter a reprodución Falta o argumento «%1» para a acción «%2» ou non é do tipo correcto. O reprodutor «%1» non pode realizar a acción «%2». Descoñécese a operación «%1». Erro descoñecido. 