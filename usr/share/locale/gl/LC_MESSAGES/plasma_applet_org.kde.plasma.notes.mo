��          �   %   �      0     1     E     X     l          �     �     �     �  
   �     �     �                                    "     0  	   <     F     L    S     a     q     �     �     �     �     �     �     �  	   �                         %     .     6     ;     D     L  
   Y     d     k     	                                                                          
                                                A black sticky note A blue sticky note A green sticky note A pink sticky note A red sticky note A translucent sticky note A white sticky note A yellow sticky note An orange sticky note Appearance Black Blue Bold Green Italic Orange Pink Red Strikethrough Translucent Underline White Yellow Project-Id-Version: plasma_applet_notes
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-09 03:12+0200
PO-Revision-Date: 2015-08-03 07:30+0200
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Unha nota negra Unha nota azul Unha nota verde Unha nota rosa Unha nota vermella Unha nota translúcida Unha nota branca Unha nota amarela Unha nota laranxa Aparencia Negro Azul Letra grosa Verde Itálica Laranxa Rosa Vermello Riscado Translúcido Subliñado Branco Amarelo 