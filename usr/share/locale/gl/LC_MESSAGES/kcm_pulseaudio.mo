��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �     �     	  .   '	  -   V	  ,   �	     �	  
   �	     �	     �	     �	  e   �	  !   L
  ^   n
     �
     �
     �
     �
               '     A     X     `     m     s     �  2   �  ?   �     	               	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-09-10 11:02+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100% Autor © 2015 Harald Sitter Harald Sitter Ningún aplicativo emitindo son Ningún aplicativo gravando son Non hai perfís de dispositivos dispoñíbeis. Non hai dispositivos de entrada dispoñíbeis Non hai dispositivos de saída dispoñíbeis Perfil: PulseAudio Avanzado Aplicativos Dispositivos Engadir un dispositivo de saída virtual para saída simultánea por todas as tarxetas de son locais. Configuración avanzada da saída Cambiar automaticamente todas as emisións en marcha cando haxa unha nova saída dispoñíbel. Capturar Predeterminado Perfís de dispositivos adrian@chaves.io Entradas Silenciar o son Adrian Chaves (Gallaecio) Sons de notificacións Saídas Reprodución Porto  (non dispoñíbel) (desenchufado) Necesita o módulo «module-gconf» de PulseAudio. Este módulo permite configurar o subsistema de son PulseAudio. %1: %2 100% %1% 