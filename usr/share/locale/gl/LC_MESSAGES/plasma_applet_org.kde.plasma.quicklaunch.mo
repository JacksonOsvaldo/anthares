��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  O   �  	   �     �     �          /     @     F     X     m          �     �  !   �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-21 07:20+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Engadir un iniciador… Engade iniciadores arrastrándoos e deixándoos ou mediante o menú contextual. Aparencia Disposición Editar o iniciador… Activar o menú emerxente Insira o título Xeral Agochar as iconas Máximo de columnas: Máximo de filas: Quicklaunch Retirar o iniciador Mostrar as iconas agochadas Mostrar os nomes dos iniciadores: Mostrar o título Título 