��          |      �             !  *   <     g  @   �  �   �  B   Q  R   �  &   �  *     ,   9  4   f  �  �     D  6   c  "   �  G   �  �     #   �  1   �  +   �  ,   $  0   Q     �        	       
                                            Could not eject this disc. Could not mount this device as it is busy. Could not mount this device. One or more files on this device are open within an application. One or more files on this device are opened in application "%2". One or more files on this device are opened in following applications: %2. Remove is less technical for unmountCould not remove this device. Remove is less technical for unmountYou are not authorized to remove this device. This device can now be safely removed. You are not authorized to eject this disc. You are not authorized to mount this device. separator in list of apps blocking device unmount,  Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-10-12 15:18+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Non se puido expulsar o disco. Non se puido montar o dispositivo porque está en uso. Non se puido montar o dispositivo. Hai aberto polo menos un ficheiro deste dispositivo nalgún aplicativo. Hai aberto polo menos un ficheiro deste dispositivo no aplicativo «%2». Hai aberto polo menos un ficheiro deste dispositivo nos seguintes aplicativos: %2. Non se puido retirar o dispositivo. Non ten autorización para retirar o dispositivo. O dispositivo pode retirarse con seguranza. Non ten autorización para expulsar o disco. Non ten autorización para montar o dispositivo. ,  