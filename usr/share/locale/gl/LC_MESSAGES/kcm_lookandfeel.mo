��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  0     P   6  4   �     �  $   �  5   �     ,     =  >   W  (   �  	   �     �     �  4   �  �   %	     �	  �   

  )   �
  �   �
  ?   E     �                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-18 22:20+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aplicar un paquete de aparencia e comportamento. Ferramenta da liña de ordes para aplicar paquetes de aparencia e comportamento. Configurar os detalles do aspecto e do comportamento © 2017 Marco Martin Cambiouse a configuración do cursor Descargar novos paquetes de aparencia e comportamento adrian@chaves.io Obter novas aparencias… Listar os paquetes de aparencia e comportamento dispoñíbeis. Ferramenta de aparencia e comportamento. Mantedor. Marco Martin Adrián Chaves Fernández Restabelecer a disposición do escritorio de Plasma. Seleccione un tema xeral para o espazo de traballo (incluíndo o tema de Plasma, o esquema de cores, o cursor do rato, o cambiador de xanela e escritorio, a pantalla de benvida, a pantalla trancada, etc.). Mostrar a vista previa Este módulo permítelle configurar a aparencia do espazo de traballo completo, e fornece algunhas aparencias predefinidas que pode usar. Usar a disposición de escritorio do tema Aviso: a disposición do seu escritorio Plasma perderase e restaurarase a disposición predeterminada que fornece o tema seleccionado. Debe reiniciar KDE para que os cambios do cursor xurdan efecto. nomedopaquete 