��    	      d      �       �   .   �   2     )   C  -   m     �  '   �  H   �  R   *  �  }     e     {  
   �     �  #   �  0   �  O   �  \   N             	                                 Note this is a KRunner keyworddesktop console Note this is a KRunner keyworddesktop console :q: Note this is a KRunner keywordwm console Note this is a KRunner keywordwm console :q: Open KWin interactive console Open Plasma desktop interactive console Opens the KWin interactive console with a file path to a script on disk. Opens the Plasma desktop interactive console with a file path to a script on disk. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-09-28 21:57+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 consola do escritorio desktop console :q: wm console wm console :q: Abrir a consola interactiva de KWin Abrir a consola interactiva do escritorio Plasma Abre a consola interactiva de KWin cunha ruta de ficheiro a un script no disco. Abre a consola interactiva do escritorio Plasma cunha ruta de ficheiro a un script no disco. 