��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �     	     '	     0	     6	     J	     Q	     `	  $   f	     �	     �	     �	     �	     �	     �	     �	     �	     
     "
     =
     O
     j
  '   �
     �
     �
     �
     �
  %   �
               4     C     c       *   �  @   �     �  !   �          0     8     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-18 21:48+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (Wayland) … (Tamaños dispoñíbeis: %1) Escoller unha imaxe Avanzado Autor Acceso &automático Fondo: Limpar a imaxe Ordes Non se puido descomprimir o arquivo. Tema do cursor: Personalizar o tema Predeterminada Descrición Descargar novos temas de SDDM adrian@chaves.io Xeral Obter un novo tema Orde de apagar: Instalar desde un ficheiro Instalar un tema. Paquete de tema incorrecto Cargar dun ficheiro… Pantalla de identificación usando SDDM UID máximo: UID mínimo: Adrian Chaves (Gallaecio) Nome Non está dispoñíbel a vista previa Orde de reiniciar: Acceder de novo ao saír Retirar o tema Configuración de SDDM para KDE Instalador de temas de SDDM Sesión: O tema de cursores predeterminado de SDDM. O tema para instalar, debe ser un ficheiro de arquivo existente. Tema Non foi posíbel instalar o tema. Desinstalar un tema. Usuario Usuario: 