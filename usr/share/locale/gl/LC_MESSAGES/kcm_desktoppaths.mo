��            )   �      �  �   �  	   W     a     q     �     �  	   �     �  	   �     �  %   �     �               #     )     5     >  X   M  S   �  �   �    �  I   �  F     E   _  H   �  B   �  :   1  7   l  �  �  �   �
     s     �     �  
   �     �  
   �     �  	   �                     &     -     >     F     W     ^  F   o  H   �  �   �  �   �  U   �  g   �  R   S  g   �  T        c     j                                                        	                                                                
                 <h1>Paths</h1>
This module allows you to choose where in the filesystem the files on your desktop should be stored.
Use the "Whats This?" (Shift+F1) to get help on specific options. Autostart Autostart path: Confirmation Required Desktop Desktop path: Documents Documents path: Downloads Downloads path: Move files from old to new placeMove Move the directoryMove Movies Movies path: Music Music path: Pictures Pictures path: The path for '%1' has been changed.
Do you want the files to be moved from '%2' to '%3'? The path for '%1' has been changed.
Do you want to move the directory '%2' to '%3'? This folder contains all the files which you see on your desktop. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder contains applications or links to applications (shortcuts) that you want to have started automatically whenever the session starts. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder will be used by default to load or save documents from or to. This folder will be used by default to load or save movies from or to. This folder will be used by default to load or save music from or to. This folder will be used by default to load or save pictures from or to. This folder will be used by default to save your downloaded items. Use the new directory but do not move anythingDo not Move Use the new directory but do not move filesDo not Move Project-Id-Version: kcmkonq
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2018-01-18 21:49+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <h1>Rutas</h1>
Este módulo permítelle escoller en que parte do sistema de ficheiros se deberán almacenar os ficheiros situados no escritorio.
Use «Que é Isto?» (Maiús+F1) para obter axuda sobre opcións específicas. Inicio automático Ruta do inicios automáticos: Requírese confirmación Escritorio Ruta do escritorio: Documentos Ruta dos documentos: Descargas Ruta das descargas: Mover Mover Filmes Ruta dos filmes: Música Ruta da música: Imaxes Ruta das imaxes: Cambiou a ruta de «%1».
Quere mover os ficheiros de «%2» a «%3»? A ruta de «%1» foi cambiada.
Quere mover o directorio «%2» a «%3»? Este cartafol contén todos os ficheiros que pode ver no seu escritorio. Pode cambiar a localización deste cartafol se o quere, e o contido será automaticamente movido ao novo lugar. Este cartafol contén os aplicativos ou ligazóns aos aplicativos que quereexecutar automaticamente ao iniciar a sesión. Pode cambiar a ruta deste cartafol se quere, e os contidos moveranse automaticamente á nova ruta. Este cartafol empregarase de maneira predeterminada para cargar ou gardar documentos. Este cartafol empregarase de maneira predeterminada para cargar ou gardar filmes recibidos ou enviados. Este cartafol empregarase de maneira predeterminada para cargar ou gardar música. Este cartafol empregarase de maneira predeterminada para cargar ou gardar imaxes recibidas ou enviadas. Este cartafol usarase de maneira predeterminada para gardar o elementos descargados. Deixar Deixar 