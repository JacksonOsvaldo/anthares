��          �      ,      �     �  $  �     �  	   �  	   �  F   �     %  
   ,     7  
   >     I  	   R     \     a  	   {     �  �  �     �  Q  �      	  
   	     	  S   	     q	     w	     �	  
   �	  
   �	  
   �	  	   �	  +   �	     �	     �	            
                                    	                              &Select test picture: <h1>Monitor Gamma</h1> This is a tool for changing monitor gamma correction. Use the four sliders to define the gamma correction either as a single value, or separately for the red, green and blue components. You may need to correct the brightness and contrast settings of your monitor for good results. The test images help you to find proper settings.<br> You can save them system-wide to XF86Config (root access is required for that) or to your own KDE settings. On multi head systems you can correct the gamma values separately for all screens. Blue: CMY Scale Dark Gray Gamma correction is not supported by your graphics hardware or driver. Gamma: Gray Scale Green: Light Gray Mid Gray RGB Scale Red: Save settings system wide Screen %1 Sync screens Project-Id-Version: kgamma
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-18 03:02+0200
PO-Revision-Date: 2009-11-29 20:35+0100
Last-Translator: Miguel Branco <mgl.branco@gmail.com>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Galician
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Escoller a imaxe de proba: <h1>Gama do monitor</h1> É unha utilidade para cambiar a corrección de gama no monitor.Emprega os catro controis para definir a corrección de gama de todos xuntos, ou separado para o vermello, o verde e o azul. Quizais precisará corrixir luminosidade e contraste do monitor para obter uns bos resultados. A imaxes de proba axudarano a atopar a configuración adecuada.<br> Pode gardalos en XF86Config (ten que acceder como administrador para facer iso) ou na súa configuración de KDE. Nos sistemas multipantalla pode corrixir os valores da gama por separado para cada unha das pantallas. Azul: Escala CMY Gris escuro A corrección de gama non se permite polo dispositivo gráfico ou polo controlador. Gama: Escala de grises Verde: Gris suave Gris medio Escala RGB Vermello: Gardar a configuración para todo o sistema Pantalla %1 Sincronizar as pantallas 