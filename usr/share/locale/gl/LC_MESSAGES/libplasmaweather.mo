��          <      \       p      q   *   �   /   �   �  �   $   �  E     L   N                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2013-01-01 14:45+0100
Last-Translator: Adrian Chaves Fernandez <adriyetichaves@gmail.com>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Non se pode atopar «%1» usando %2. Esgotouse o tempo máximo de conexión ao servidor meteorolóxico %1. A obtención da información meteorolóxica para %1 esgotou o tempo-límite. 