��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �     \   �               #     6  	   =     G     P     W     o     �     �     �  !   �     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2011-01-11 18:56+0100
Last-Translator: Marce Villarino <mvillarino@gmail.com>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Medio escurecer a pantalla Escurecer totalmente a pantalla Lista as opcións de brillo da pantalla ou axústaa ao brillo definido por :q:, p.ex. un brillo de 50 atenuará a pantalla ao 50% do brillo máximo Lista as opcións de suspensión do sistema (p.ex. durmir, hibernar) e permítelle activalas Escurecer a pantalla hibernar brillo da pantalla durmir suspender no disco na ram escurecer a pantalla %1 brillo da pantalla %1 Trocar o brillo a %1 Suspender no disco Suspender na RAM Suspende o sistema na memoria RAM Suspende o sistema no disco 