��          ,      <       P   �   Q   �    �                        # created by KDE Plasma, %1
#
# If you do not want Plasma to override your GTK settings, select
# Colors in the System Settings and disable the checkbox
# "Apply colors to non-Qt applications"
#
#
 Project-Id-Version: krdb
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2017-09-07 22:31+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 # creado por KDE Plasma, %1
#
# Se non quere que Plasma ignore a configuración de GTK, seleccione
# Cores na Configuración do sistema e desactive a caixa para marcar
# «Aplicar as cores aos aplicativos que non usen Qt»
#
#
 