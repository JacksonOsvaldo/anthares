��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     l     }  
   �  +   �     �     �  *        /  
   D     O  *   _     �  
   �  9   �  j   �     Y  S   m     �     �     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-31 21:34+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Termos de %1: %2 © 2012, Vishesh Handa Baloo Show Identificador de dispositivo dos ficheiros. mvillarino@kde-espana.org Termos de nome de ficheiro: %1 Número de inode do ficheiro para mostrar. Información interna Mantenedor Marce Villarino Non se atopou información de indexación. Imprimir información interna. Termos: %1 O visor de datos de Baloo, unha ferramenta de depuración Non se puido abrir o índice de Baloo. Execute «%1» para ver se Baloo está activado e en funcionamento. Os URL de ficheiro. O identificador do ficheiro non é idéntico ao identificador do ficheiro de Baloo. Isto é un fallo. Vishesh Handa 