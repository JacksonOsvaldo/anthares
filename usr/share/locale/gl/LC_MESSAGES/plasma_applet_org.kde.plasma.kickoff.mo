��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �         
                  -     C  	   X     b     n  
   �  �   �       1   5  	   g     q  	   �     �     �     �     �     �     �     �     	     	     8	     P	     m	     �	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-18 22:08+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1@%2 %2@%3 (%1) Escoller… Borrar a icona Engadir aos favoritos Todos os aplicativos Aparencia Aplicativos Actualizáronse os aplicativos. Computador Arrastre as lapelas entre as caixas para mostralas ou agochalas, ou cambie arrastre as lapelas visíbeis para cambiar a súa orde. Editar os aplicativos… Incluír na busca marcadores, ficheiros e correos Favoritos Lapelas agochadas Historial Icona: Saír Botóns de menú Usados habitualmente En todas as actividades Na actividade actual Retirar dos favoritos Mostrar nos favoritos Mostrar o nome dos aplicativos Ordenar alfabeticamente Pasar á lapela baixo o rato Escriba para buscar… Lapelas visíbeis 