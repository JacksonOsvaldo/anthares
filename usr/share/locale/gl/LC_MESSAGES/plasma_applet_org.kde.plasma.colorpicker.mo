��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �    �  ,   �          1     A     W     v     |     �     �     �     �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-07-29 11:06+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Copiar a cor automaticamente no portapapeis. Limpar o historial Opcións de cor Copiar no portapapeis Formato de cor predeterminado: Xeral Abrir o diálogo de cor Escoller unha cor Escoller unha cor Mostrar o historial Ao premer o atallo de teclado: 