��    
      l      �       �      �   6   �   3   4  8   h     �      �     �     �  "   �  �         O      L   p  =   �     �           (     6  2   F            	            
                     A MIME type A command-line tool for querying the KDE trader system A constraint expressed in the trader query language A servicetype, like KParts/ReadOnlyPart or KMyApp/Plugin David Faure EMAIL OF TRANSLATORSYour emails KTraderClient NAME OF TRANSLATORSYour names Output only paths to desktop files Project-Id-Version: ktraderclient
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:51+0200
PO-Revision-Date: 2017-07-29 09:59+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Un tipo MIME Unha utilidade da liña de ordes para buscar no sistema de transaccións de KDE Unha restrición expresada na linguaxe de buscas do sistema de transaccións Un tipo de servizo, como KParts/ReadOnlyPart ou KMyApp/Plugin David Faure mvillarino@users.sourceforge.net KTraderClient Marce Villarino Imprimir só as rutas aos ficheiros de escritorio. 