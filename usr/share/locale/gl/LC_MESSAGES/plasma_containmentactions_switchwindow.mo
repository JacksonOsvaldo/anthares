��          T      �       �   "   �      �   *   �   3   '  E   [  .   �  �  �  (   �  $   �  +        9  ,   N     {                                        Display a submenu for each desktop Display all windows in one list Display only the current desktop's windows plasma_containmentactions_switchwindowAll Desktops plasma_containmentactions_switchwindowConfigure Switch Window Plugin plasma_containmentactions_switchwindowWindows Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-30 03:13+0200
PO-Revision-Date: 2015-03-15 14:49+0100
Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Mostrar un submenú para cada escritorio Mostrar todas as xanelas nunha lista Mostrar só as xanelas do escritorio actual Todos os escritorios Configurar o complemento de cambio de xanela Xanelas 