��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     �     �  6   �  J   �  D   ;  H   �  J   �  R   	  W   g	     �	     �	     �	     �	     �	     �	  	   
     
     &
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-08-13 09:18+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Trancar o contedor Expulsar o medio Atopa dispositivos que teñan un nome que case con :q: Lista todos os dispositivos e permite montalos, desmontalos ou expulsalos. Lista todos os dispositivos que se poidan expulsar e permite facelo. Lista todos os dispositivos que se poidan montar, e permite desmontalos. Lista todos os dispositivos que se poidan desmontar e permite desmontalos. Lista todos os dispositivos cifrados que se poidan bloquear, e permite bloquealos. Lista todos os dispositivos cifrados que se poidan desbloquear e permite desbloquealos. Montar o dispositivo dispositivo expulsar bloquear montar desbloquear desmontar Desatrancar o contedor Desmontar o dispositivo 