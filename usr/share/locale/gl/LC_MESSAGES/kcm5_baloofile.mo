��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]  %        ,  #   L     p     �     �  U   �        '   8     `  X   p     �  '   �                                 	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-18 21:58+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Indexar tamén o contido do ficheiro. Configurar a busca de ficheiros Copyright 2007-2010 Sebastian Trüg Non buscar nestes lugares mvillarino@kde-espana.org Activar a busca de ficheiros A busca de ficheiros permítelle dar rapidamente cos seus ficheiros segundo o contido O cartafol %1 xa está excluído O %1 pai do cartafol xa está excluído Marce Villarino Non se permite excluír o cartafol raíz, desactive a busca de ficheiros se non o quere. Sebastian Trüg Escolla o cartafol que se debe excluír Vishesh Handa 