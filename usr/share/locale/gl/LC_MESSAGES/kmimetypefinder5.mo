��          \      �       �       �   $   �              -   ?  8   m  Z   �  �    2   �      +     L  *   f  G   �  ?   �  o                                          EMAIL OF TRANSLATORSYour emails Gives the MIME type for a given file MIME Type Finder NAME OF TRANSLATORSYour names The filename to test. '-' to read from stdin. Use only the file content for determining the MIME type. Whether use the file name only for determining the MIME type. Not used if -c is specified. Project-Id-Version: kmimetypefinder
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:51+0200
PO-Revision-Date: 2017-07-29 09:54+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 mvillarino@users.sourceforge.net, adrian@chaves.io Indica o tipo MIME dun ficheiro. Descubridor de tipos MIME Marce Villarino, Adrian Chaves (Gallaecio) O nome do ficheiro que se vai probar. Empregue «-» para ler de stdin. Empregar só o contido do ficheiro para determinar o tipo MIME. Indica se debe empregar só o nome do ficheiro para determinar o tipo MIME. Non se emprega de especificarse -c. 