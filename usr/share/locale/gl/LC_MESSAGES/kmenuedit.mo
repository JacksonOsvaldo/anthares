��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �     �     �  	   �            	        &     -  "   ?     b  #   k     �  (   �  !   �     �     	  >        [  D   d     �     �     �  Y   �  0   A     r  �  �     4     :     K     \     n     �     �  
   �     �  G   �  *        H     W     _     f     �     �     �     �     �     �     �     �  !   
     ,  "   E     h     �  (   �     �     �     �  X        h  5   v            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-09-09 12:04+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  [Agochado] &Comentario: &Eliminar &Descrición: &Editar &Ficheiro &Nome: &Novo submenú… Executar como un &usuario distinto &Ordenar &Ordenar todo segundo a descrición &Ordenar todo segundo o nome &Ordenar a escolla segundo a descrición &Ordenar a escolla segundo o nome Nome de &usuario: &Ruta de traballo: © 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Avanzado Retiraranse todos os submenús de «%1». Quere realmente continuar? &Orde: Non se puido escribir en %1 Atallo de teclado act&ual: Quere restaurar o menú do sistema? Aviso: Isto retirará todos os menús personalizados. xabigf@gmx.net, mvillarino@users.sourceforge.net Activar o &aviso de inicio Seguindo a orde, pode pór varios sinais de substitución que serán substituídos cos valores reais cando o programa se execute:
%f: un nome de ficheiro
%F: unha lista de ficheiros; úseo para aplicativos que poden abrir varios ficheiros locais dunha vez
%u: un único URL
%U: unha lista de URL
%d: o cartafol do ficheiro que se vai abrir
%D: unha lista de cartafoles para abrir
%i: a icona
%m: a mini-icona
%c: o título Xeral Opcións xeerais Entrada agochada Nome do elemento: Editor do menú de KDE Editor do menú de KDE Barra de ferramentas Principal Mantenedor Matthias Elter Non se puideron gardar os cambios no menú debido ao seguinte problema: Entrada do menú que se vai preseleccionar Montel Laurent &Baixar &Subir Xabi García, Marce Villarino Novo &elemento… Novo elemento Novo &separador Novo submenú Mostrar só en KDE Autor orixinal Anterior mantenedor Raffaele Sandrini Restaurar para o menú do sistema Executar nunha &terminal Desexa gardar os cambios do menú? Mostrar as entradas agochadas Comprobación da ortografía Opcións da comprobación da ortografía Submenú a preseleccionar Nome do submenú: &Opcións da terminal: Non se puido contactar con khotkeys. Os cambios gardáronse pero non puideron activarse. Waldo Bastian Fixo cambios no menú.
Quere gardalos ou descartalos? 