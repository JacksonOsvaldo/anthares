��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	  "   �
       0     8   P     �     �     �     �     �     �     �  !        2  	   P     Z     c     g     t     �     �  $   �  )   �     �     �       '     2   C     v  6   �  B   �               8     =     J  
   N     Y                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-18 22:09+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 notificación %1 notificacións %1 de %2 %3 %1 tarefa en execución %1 tarefas en execución &Configurar as notificacións e accións para eventos… Hai 10 segundos Hai 30 segundos Mostrar os detalles Agochar os detalles Retirar as notificacións Copiar Copiar o enderezo da ligazón 1 directorio %2 de %1 directorios 1 ficheiro %2 de %1 ficheiros Historial %1 de %2 +%1 Información A tarefa fallou Rematou a tarefa Máis opcións… Non hai ningunha notificación nova. Non hai ningunha notificación nin tarefa Abrir… %1 (en pausa) Escoller todo Mostrar un historial de notificacións. Mostrar notificacións de aplicativos e do sistema %1 (%2 restantes) Seguir as transferencias de ficheiros e outras tarefas Usar unha posición personalizada para a xanela de notificacións. Hai %1 minuto Hai %1 minutos Hai %1 día Hai %1 días Onte Hai un intre %1: %1: Fallou %1: Rematado 