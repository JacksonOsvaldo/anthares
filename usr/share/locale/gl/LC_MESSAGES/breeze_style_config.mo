��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     A  )   E  #   o  )   �  )   �     �       #        3  '   L  /   t  .   �  W   �  M   +	  '   y	  2   �	  4   �	  /   	
  ,   9
  ?   f
     �
  -   �
     �
     �
     �
  	          4   &     [  !   h                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2018-01-18 21:49+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms &Visibilidade do aceleradores do teclado: &Tipo de botón da frecha superior: Agochar sempre os aceleradores do teclado Mostrar sempre os aceleradores do teclado D&uración das animacións: Animacións T&ipo de botón da frecha inferior: Configuración de Breeze Centrar as lapelas na barra de lapelas. Arrastrar as xanelas por calquera área baleira Arrastrar as xanelas só pola barra do título Arrastrar as xanelas pola barra do título, a barra do menú e as barras de ferramentas Mostrar unha liña delgada para indicar o foco nos menús e barras de menús. Debuxar o indicador do foco nas listas. Mostrar un marco arredor de cada panel ancorábel. Mostrar un marco arredor de cada título de páxina. Mostrar un marco arredor de cada panel lateral. Mostrar as marcas de valores do desprazador. Debuxar os separadores dos elementos das barras de ferramentas. Activar as animacións Activar as asas grandes de cambiar o tamaño. Marcos Xeral Sen botóns Un botón Barras de desprazamento Mostrar os aceleradores de teclado cando se precisen Dous botóns Método de arrastre das &xanelas: 