��    
      l      �       �      �                0  !   H  .   j  2   �  7   �  4     �  9     �     
     "     5  %   O  $   u  &   �  (   �  #   �     	                             
           Used a moment ago Used more than a year ago Used some time ago Walk through activities Walk through activities (Reverse) amount in daysUsed a day ago Used %1 days ago amount in hoursUsed an hour ago Used %1 hours ago amount in minutesUsed a minute ago Used %1 minutes ago amount in monthsUsed a month ago Used %1 months ago Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-07 09:26+0100
PO-Revision-Date: 2016-04-21 07:23+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Usada hai un momento Usada hai máis dun ano Usada hai un tempo Navegar polas actividades Navegar polas actividades (ao revés) Usada hai un día Usada hai %1 días Usada hai unha hora Usada hai %1 horas Usada hai un minuto Usada hai %1 minutos Usada hai un mes Usada hai %1 meses 