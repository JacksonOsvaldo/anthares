��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  T   �  }   #  $   �  b   �  !   )     K  J   _     �  H   �               "     +  @   I  >   �  A   �  *     	   6  g   @     �     �  �   �     i  @   �     �     �     �               5     >     Y  
   k  *   v     �     �     �  #   �     �     �                    .     ?  !   M     o     �     �     �  "   �  	   �     �     �       �        �  6   �  �        �  K   �  1   �  4   "  4   W      �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-01 20:38+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <proxecto@trasno.net>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Para aplicar o cambio de infraestrutura terá que saír da sesión e entrar de novo. Unha lista coas infraestruturas de Phonon que se atoparon no sistema. A orde aquí determina a orde coa que as usará Phonon. Aplicar a lista de dispositivos a… Aplicar esta lista de preferencia do dispositivo ás seguintes categorías de reprodución de son: Configuración do hardware de son Reprodución de son Preferencia do dispositivo de reprodución de son para a categoría «%1» Gravación de son Preferencia do dispositivo de gravación de son para a categoría «%1» Infraestrutura Colin Guthrie Conector Copyright 2006 Matthias Kretz Preferencia do dispositivo predeterminado de reprodución de son Preferencia do dispositivo predeterminado de gravación de son Preferencia do dispositivo predeterminado de gravación de vídeo Categoría predeterminada/non especificada Postergar Define a ordenación predeterminada dos dispositivos, pode ser sobrescrita por categorías individuais. Configuración do dispositivo Preferencia do dispositivo Atopáronse dispositivos no sistema axeitados para a categoría seleccionada. Escolla o dispositivo que desexe que usen os aplicativos. mvillarino@kde-espana.es Non se puido configurar o dispositivo de saída de son escollido Dianteiro central Dianteiro esquerdo Dianteiro esquerdo ou central Dianteiro dereito Dianteiro dereito ou central Hardware Dispositivos independentes Niveis de entrada Incorrecto Configuración do hardware de son para KDE Matthias Kretz Mono Marce Villarino Módulo de configuración de Phonon Reprodución (%1) Preferir Perfil Traseiro central Traseiro esquerdo Traseiro dereito Gravando (%1) Mostras os dispositivos avanzados Lateral esquerdo Lateral dereito Tarxeta de son Dispositivo de son Situación e probas do altofalante Subwoofer Probar Probar o dispositivo escollido Estase a probar %1 A orde determina a preferencia dos dispositivos. Se por algunha razón non se pode usar o primeiro dispositivo, Phonon intentará usar o segundo, e así en diante. Canle descoñecida Usa esta lista de dispositivos para máis categorías. Varias categorías de casos de uso dos medios. Para cada categoría pode escoller o dispositivo que prefire usar cos aplicativos Phonon. Gravación de vídeo Preferencia do dispositivo de gravación de imaxe para a categoría «%1»  Poida que a infraestrutura non permita gravar son Poida que a infraestrutura non permita gravar vídeo non se indicou a prioridade do dispositivo escollido preferir o dispositivo escollido 