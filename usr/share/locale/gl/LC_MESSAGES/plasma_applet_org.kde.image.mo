��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s     q  ,   z     �     �     �  
   �     �     �  F   �     E  #   d     �     �     �  %   �     �     �  $   �     "  +   2     ^     f     �     �  ,   �     �     �     	  
   	                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2018-01-18 22:15+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 de %2 Engadir un fondo de escritorio personalizado Engadir un cartafol… Engadir unha imaxe... Fondo: Desenfoque Centrado &Cambiar cada: Directorio que contén as imaxes para mostrar como unha presentación. Descargar fondos de escritorio Obter novos fondos de escritorio… Horas Ficheiros de imaxe Minutos Seguinte imaxe de fondo de escritorio Abrir o cartafol contedor… Abrir unha imaxe Abrir a imaxe de fondo de escritorio Posicionamento: Ficheiro de fondo de escritorio recomendado Retirar Restaurar o fondo de escritorio Cambiado de escala Cambiado de escala e recortado Cambiado de escala, mantendo as proporcións Segundos Escoller a cor do fondo Cor sólida En mosaico 