��          �            x  !   y  "   �  #   �  &   �  !   	  &   +  '   R     z     �  V   �     �     �     �            �       �     �     �  
   �  	   �     �     �     �     �  [   �     X  
   r  	   }     �     �                                     
       	                                   @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Get New Decorations... Maximize Minimize Search Shade Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-02-03 11:42+0100
Last-Translator: Samir Ribic <sribic@etf.unsa.ba>
Language-Team: Bosnian <kde-i18n-doc@kde.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Launchpad (build 17341)
X-Launchpad-Export-Date: 2015-02-16 06:42+0000
X-Associated-UI-Catalogs: kdelibs4
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Ogromne Velike Normalne Neizmjerne Sićušne Vrlo ogromne Vrlo velike Dugmad Zatvori Zatvorite na dvostruki klok:
 Da otvorite meni, držite dugme pritisnutim dok se ne pojavi. Dobavi nove dekoracije... Maksimizuj Minimizuj Traži Namotaj 