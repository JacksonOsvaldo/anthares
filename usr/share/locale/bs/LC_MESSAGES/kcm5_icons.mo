��    =        S   �      8     9     B     K     Z     l     s  �  �  �   i  -   �  )   #	  -   M	  +   {	  r   �	  	   
  	   $
     .
     F
     N
     W
  
   d
     o
     {
     �
     �
      �
     �
     �
     �
      �
     	       r   *  5   �     �     �     �          *  	   /     9     ?     G  (   T     }     �     �     �     �     �  +   �  3        K     S     a     i  "   v  S   �  )   �       �   #  �       �     �     �     �     �     �  �  �  �   �  ,        5     =     I  W   Q  	   �  	   �     �     �     �     �     �     �     �               ,     @     Q     V     b     �      �  r   �  1        L     e     v     �     �     �     �     �     �     �     �     �           (  
   I  
   T  .   _  (   �  
   �     �     �     �  %   �  N     *   ^     �  �   �     (            4                        
   7             1   ,          #   $      :   3              ;      0                     5   6       %       "       	            2      +            9   '       8                 <   /   &   .              -       *       )                 =         !        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2014-10-20 19:32+0000
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Launchpad (build 17341)
X-Launchpad-Export-Date: 2015-02-16 06:42+0000
X-Associated-UI-Catalogs: kdelibs4
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 &Količina: &Efekti: &Druga boja: &Poluprovidne &Tema © 2000-2003, Gert Jansen <h1>Ikone</h1>U ovom modulu birate izgled ikona na radnoj površini.<p>Da biste izabrali temu ikona, kliknite na njeno ime i primijenite izbor klikom na dugme "Primijeni". Ako ne želite da primijenite izbor, kliknite na dugme "Reset" i promjene će biti odbačene.</p><p>Dugmetom "Instaliraj datoteku teme..."možete instalirati temu ikona upisivanjem njene lokacije ili izborom iz dijaloga. Kliknite na dugme "U redu" da biste završili instalaciju.</p><p>Dugme "Ukloni temu" će biti aktivno samo ako izaberete temu instaliranu ovim modulom. Odavdje ne možete ukloniti globalno instalirane teme.</p><p>Takođe možete naznačiti i koji će se efekti primjenjivati nad ikonama.</p> <qt>Želite li zaista da uklonite temu ikona <strong>%1</strong>?<br/><br/>Ovo će obrisati sve datoteke instalirane ovom temom.</qt> <qt>Instaliram temu <strong>%1</strong></qt> Aktivna Isključena Osnovna Došlo je do problema u toku instalacije. Ipak, veći dio tema iz arhive je instaliran. &Napredno Sve ikone Antonio Larosa Himenez &Boja: Obojeno Potvrda Odzasićeno Opis Površ Dijalozi Prevucite ili ukucajte URL teme Caslav.ilic@gmx.net Parametri efekta Gama Gert Jansen Dobavi nove teme sa Interneta Ikone Modul kontrolnog panela za ikone Ako arhivu teme već imate lokalno, ovim dugmetom je možete raspakovati i staviti na raspolaganje KDE programima. Instaliraj arhivu teme koja je smještena lokalno Instaliram temu ikona... Jonathan Riddell Glavna traka Chusslove Illich Ime Bez efekata Panel Pregled Ukloni temu Ukloni izabranu temu sa diska Postavi efekat... Podesi efekte aktivnih ikona Podesi efekte osnovnih ikona Podesi efekte isključenih ikona Veličina: Male ikone Ovaa datoteka nije ispravna arhiva teme ikona. Ovo će ukloniti izabranu temu sa diska. Zasivljeno Monohromatsko Traka alatki Torsten Ran Ne mogu kreirati privremenu datoteku. Ne mogu da preuzmem arhivu teme ikona.
Provjerite da li je adresa %1 ispravna. Ne mogu da pronađem arhivu teme ikona %1. Upotreba ikone <html>Za ovu radnju morate biti povezani na Internet. Pojaviće se dijalog sa spiskom tema sa veb sajta <link>http://www.kde.org</link>, odakle ih možete instalirati klikom na dugme <interface>Instaliraj</interface> pokraj željene teme.</html>. 