��    .      �  =   �      �     �     �                  
        %     2     J     ]     i     p     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     )     0     6     =     B  
   V     a     s     �     �     �     �     �     �     �     �     �       �  
     �     �     �     �     �  
   �       !        4     L     \     c  	   w  
   �     �     �     �     �     �     �     �     �     �     �      	     	     &	     -	     A	     G	     O	     V	     o	     {	     �	     �	     �	  !   �	  	   �	     �	     
     
     "
     &
     D
            +             '   
                    *   %                         #             !                       $   (      &   ,   )              .   	         -                       "                          Cancel Columns Custom title Date Default Descending Deselect All Enter custom title here File name pattern: File types: Filter Folder preview popups Folders first Full path Hide Files Matching Icons Large Left Location Lock in place More Preview Options... Name None OK Preview Plugins Preview thumbnails Remove Resize Right Rotate Rows Search file type... Select All Selection markers Show All Files Show Files Matching Show a place: Show the Desktop folder Size Small Sorting: Specify a folder: Type Type a path or a URL here Unsorted Project-Id-Version: kde5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2015-02-24 20:34+0100
Last-Translator: Samir Ribić <megaribi@epn.ba>
Language-Team: Bosnian
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Otkaži Kolona Prilagođen naslov Datum Podrazumijevani Opadajući Poništi izbor Unesite ovdje prilagođen naslov: Obrazac imena datoteke: Vrste datoteka: Filtar Popup pregleda mape Mape prvo Puna staza Sakrij odgovarajuce datoteke Ikone Velika Lijeva Lokacija Zaključaj u mjestu Još opcija pregleda... Ime Ništa OK Pregledati plugine Pregledne sličice Ukloni Promijeni veličinu Desno Rotiraj Redovi Pronađi tip datoteke... Izaberi Sve Markeri za odabir Pokaži Sve Datoteke Prikaži odgovarajuće datoteke Pokaži mjesto: Prikaži direktorij radne površi Veličina Mala Sortiranje: Specificiraj mapu: Tip Unesite putanju ili URL ovdje Nesortirano 