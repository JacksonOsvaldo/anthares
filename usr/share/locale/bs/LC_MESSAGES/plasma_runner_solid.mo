��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �    �     �     �  &   �  Q     E   g  I   �  M   �  d   E	  d   �	     
      
     (
     /
     4
     =
  
   D
     O
     e
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: kdebase-workspace
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-02-04 14:13+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 06:36+0000
X-Generator: Launchpad (build 17331)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Zaključaj sadržalac Izbaci medijum Nalazi uređaje čije ime odgovara :q: Nabraja sve uređaje i omogućava njihovo montiranje, demontiranje i izbacivanje. Nabraja sve uređaje koji se mogu izbaciti i omogućava da se izbace. Nabraja sve uređaje koji se mogu montirati i omogućava da se montiraju. Nabraja sve uređaje koji se mogu demontirati i omogućava da se demontiraju. Prikazuje sve kriptografisane uređaje koji se mogu zaključati i omogućava njihovo zaključavanje. Prikazuje sve kriptografisane uređaje koji se mogu otključati i omogućava njihovo otključavanje. Montiraj uređaj uređaj izbaci lock montiraj unlock demontiraj Otključaj sadržalac Demontiraj uređaj 