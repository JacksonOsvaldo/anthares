��    -      �  =   �      �     �     �               &     3  �   B  r   �  }   <  �   �  �   Y  �   �  �   �  g   J  h   �  Y   	  Y   u	  ]   �	     -
     2
  
   ;
     F
  
   X
     c
  	   l
     v
  
   �
     �
  ]   �
  I   
     T  %   m      �     �     �     �        H        _  -   }     �     �  *   �     �  #  �          -     E     U     g     x  �   �  @     Y   R  �   �  �   C  �   �  �   �  C   N  =   �  :   �  -     9   9     s  
   y     �     �  
   �     �     �     �     �     �  $     R   1     �  &   �  )   �     �               3  G   H  &   �     �  	   �  	   �  (   �                         '                      +   )          *                           &   -   "   ,      	   #                                            (           
   !       %                               $       @title:windowChoose Image Add user account Clear Avatar Delete User Delete files Email Address: Error returned when the password is invalidThe password should be at least %1 character The password should be at least %1 characters Error returned when the password is invalidThe password should be more varied in letters, numbers and punctuation Error returned when the password is invalidThe password should contain a mixture of letters, numbers, spaces and punctuation Error returned when the password is invalidThe password should contain at least %1 lowercase letter The password should contain at least %1 lowercase letters Error returned when the password is invalidThe password should contain at least %1 number The password should contain at least %1 numbers Error returned when the password is invalidThe password should contain at least %1 special character (like punctuation) The password should contain at least %1 special characters (like punctuation) Error returned when the password is invalidThe password should contain at least %1 uppercase letter The password should contain at least %1 uppercase letters Error returned when the password is invalidThe password should not contain sequences like 1234 or abcd Error returned when the password is invalidThe password should not contain too many repeated characters Error returned when the password is invalidThis password can't be used, it is too simple Error returned when the password is invalidYour name should not be part of your password Error returned when the password is invalidYour username should not be part of your password John John Doe Keep files Load from file... MyPassword New User Password: Passwords do not match Real Name: Remove user account Returned when a more specific error message has not been foundPlease choose another password The username can contain only letters, numbers, score, underscore and dot The username is too long The username must start with a letter This e-mail address is incorrect This password is excellent This password is good This password is very good This password is weak This user is using the system right now, removing it will cause problems This username is already used Title for change password dialogNew Password Users Verify: What do you want to do after deleting %1 ? john.doe@example.com Project-Id-Version: user manager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:21+0100
PO-Revision-Date: 2015-02-04 14:59+0000
Last-Translator: Anela Darman <adarman1@etf.unsa.ba>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2015-02-05 07:15+0000
X-Generator: Launchpad (build 17331)
 Izaberite sliku Dodaj korisnički nalog Izbriši avatar Obriši korisnika Obriši datoteke Adresa e-pošte: Šifra bi trebala sadržavati barem %1 znak Šifra bi trebala sadržavati barem %1 znaka Šifra bi trebala sadržavati barem %1 znakova Šifra treba biti raznolika u slovima, brojevima i interpunkciji Šifra bi trebala sadržavati mješavinu slova, brojeva, praznih prostora i interpunkcije Šifra bi trebala sadržavati barem %1 malo slovo Šifra bi trebala sadržavati barem %1 mala slova Šifra bi trebala sadržavati barem %1 malih slova Šifra bi trebala sadržavati barem %1 broj Šifra bi trebala sadržavati barem %1 broja Šifra bi trebala sadržavati barem %1 brojeva Šifra bi trebala sadržavati barem %1 specijalni znak (kao interpunkciju) Šifra bi trebala sadržavati barem %1 specijalna znaka (kao interpunkciju) Šifra bi trebala sadržavati barem %1 specijalnih znakova (kao interpunkciju) Šifra bi trebala sadržavati barem %1 veliko slovo Šifra bi trebala sadržavati barem %1 velika slova Šifra bi trebala sadržavati barem %1 velikih slova Šifra ne bi trebala sadržavati sekvence kao što su 1234 ili abcd Šifra ne bi trebala sadržavati previše ponavljanih simbola Ova šifra  se ne može koristiti, previše je jednostavna Vaše ime ne bi trebalo biti dio vaše šifre Vaše korisničko ime ne bi trebalo biti dio vaše šifre Junuz Junuz Alic Zadrži datoteke Učitaj iz datoteke... MyPassword Novi korisnik Lozinka: Lozinke se ne poklapaju Stvarno ime: Ukloni korisnički nalog Molimo vas da odaberete drugu šifru Korisničko ime može sadržavati samo slova, brojeve, zarez, donju crtu i tačku. Korisničko ime predugo Korisničko ime mora početi sa slovom Ova adresa elektronske pošte je netačna Ova šifra je odlična Ova lozinka je dobra Ova šifra je veoma dobra Ova lozinka je slaba Ovaj korisnik trenutno koristi sistem, uklanjanje će napraviti problem Ovo korisničko ime je već u upotrebi Nova lozinka Korisnici Provjera: Šta želite uraditi nakon brisanja %1 ? junuz.alic@primjer.ba 