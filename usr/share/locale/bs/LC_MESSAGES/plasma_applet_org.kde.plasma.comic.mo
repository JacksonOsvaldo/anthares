��    /      �  C           ;        U     s     �     �  6   �  A   �     ,     5  $   9  
   ^     i  #   �     �     �     �  7   �          (     D     Q  $   `  ,   �     �     �     �     �     �               +     @     R  �   _  "        >     E  %   W     }     �     �     �  
   �  7   �     	     1	  *  I	  B   t     �     �     �     �               8     A  &   E     l      s  ,   �     �     �     �  .   �     #     ;     P     \  '   p  1   �     �     �     �      �          !     -     F     \     p  �   }  !   ;     ]     d     z  	   �     �  !   �     �  
   �      �               &   "      (                                  !             )   	          +           .      #       '   -                             /         %                            *   ,   
                  $                                  

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Check for new comic strips: Comic Comic cache: Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. No zip file is existing, aborting. Range: Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-02-04 16:15+0000
Last-Translator: Vedran Ljubovic <vljubovic@smartnet.ba>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 06:35+0000
X-Generator: Launchpad (build 17331)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 

Izaberite prethodni strip da prijeđete na posljednji keširani. &Kreiraj strip arhivu... &Snimi strip kao... &Broj stripa: *.cbz|Strip arhiva (Zip) &Stvarna veličina Spremi  tekuću &poziciju Napredno Sve Desila se greška za identifikator %1. Izgled Arhiviranje stripa nije uspjelo: Automatski ažuriraj stripovske priključke: Provjeri nove stripove: Comic Skladište stripova: Ne mogu kreirati arhivu na navedenoj lokaciji. Kreiraj %1 strip arhivu Kreiram strip arhivu Odredište: Rukovanje greškama Neuspjeklo dodavanje datoteke u arhivu. Neuspjelo kreiranje datoteke za identifikator %1. Od početkla do... Od kraja do... Opšte Dobavljanje stripa nije uspjelo: Idi na strip Informacija Skoči na &tekući strip Skoči na &prvi strip Skoči na strip ... Ručni opseg Možda ne radi veza sa Internetom.
Možda je priključak stripa pokvaren.
Takođe može biti da nema stripa za ovaj dan, broj ili niz, pa dobavljanje može uspjeti ako izaberete neki drugi. Ne postoji zip datoteka, izlazim. Opseg: Identifikator stripa: Opseg stripova za arhivu. Ažuriraj Posjeti comic web stranicu Posjeti &web stranicu za kupovinu # %1 dd.MM.gggg &Naredna kartica s novim stripom Od: Do: 