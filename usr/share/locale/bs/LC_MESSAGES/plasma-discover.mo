��            )   �      �  5   �  #   �  E   �  E   A  >   �     �     �     �             <        I     Q  *   Z     �  	   �     �     �      �     �  
   �  :   �     3     ;     B     I  	   [     e  
   n  1  y  5   �  %   �  E     E   M  >   �     �     �     	     	     	  9   %	     _	     f	  '   m	  
   �	     �	     �	     �	  )   �	  "   �	     
  6   
     U
     ]
     d
     l
  	   
  	   �
     �
            	                                                      
                                                                       <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> Aleix Pol Gonzalez Available backends:
 Available modes:
 Back Cancel Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Install Installed Jonathan Thomas Launch List all the available backends. List all the available modes. Loading... Open with a program that can deal with the given mimetype. Rating: Remove Review Search in '%1'... Search... Summary: Update All Project-Id-Version: bosnianuniversetranslation
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2015-02-04 16:18+0000
Last-Translator: Nermina Ahmić <nahmic1@etf.unsa.ba>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2015-02-05 06:39+0000
X-Generator: Launchpad (build 17331)
 <em>%1 od %2 ljudi smatra ovu recenziju korisnom</em> <em>Recite nam o ovoj recenziji!</em> <em>Korisno? <a href='true'><b>Da</b></a>/<a href='false'>Ne</a></em> <em>Korisno? <a href='true'>Da</a>/<a href='false'><b>Ne</b></a></em> <em>Korisno? <a href='true'>Da</a>/<a href='false'>Ne</a></em> Aleix Pol Gonzalez Dostupni pozadinski programi:
 Dostupni režimi:
 Nazad Otkaži Direktno otvori navedenu aplikaciju njenim imenom paketa. Odbaci Otkrij Prikaži listu elemenata s kategorijom. Instaliraj Instalirano Jonathan Thomas Pokreni Nabroji sve dostupne pozadinske programe. Nabroji sve dostupne režime rada. Učitavam... Otvori programom koji može raditi s datim MIME tipom. Ocjena: Ukloni Pregled Tražim u  '%1'... Traži... Sažetak: Ažuriraj sve 