��    -      �  =   �      �     �  ;   �  9   "  (   \  ;   �  9   �  8   �  $   4     Y     o     �  /   �      �     �  ,   �        N   0  
         �  	   �  �   �     ?     K  e   j     �     �     �  
             '     @     G  &   X  %     f   �  8   	  ;   E	     �	     �	     �	     �	  �   �	  "   j
  C   �
  v  �
     H     N     j     �     �     �  %   �     �  )   �          2  $   8  "   ]  (   �     �     �  K   �               4  �   @     �     �  {        }  #   �     �     �     �     �               %     A  M   ^  @   �  :   �     (     4     T      l  �   �       K   )                             "         #      
   !   $   	                                   -             %            ,          *          )                                 +   &                '   (                min @action:inmenu Global shortcutDecrease Keyboard Brightness @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Keyboard Brightness @action:inmenu Global shortcutIncrease Screen Brightness @action:inmenu Global shortcutToggle Keyboard Backlight @label:slider Brightness levelLevel AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Battery Critical (%1% Remaining) Battery Low (%1% Remaining) Brightness level, label for the sliderLevel Charge Complete Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Running on AC power Running on Battery Power Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Your battery is low. If you need to continue using your computer, either plug in your computer, or shut it down and then change the battery. Your battery is now fully charged. Your battery level is critical, save your work as soon as possible. Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2013-10-18 20:23+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Launchpad (build 16807)
X-Launchpad-Export-Date: 2013-10-19 05:21+0000
X-Associated-UI-Catalogs: kdelibs4
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
  min. Smanji osvjetljaj tastature Smanji osvjetljaj ekrana Hibernacija Povećaj osvjetljaj tastature Povećaj osvjetljaj ekrana Preklopi pozadinsko svjetlo tastature Nivo Uključen adapter za naizmjeničnu struju Menadžer aktivnosti Nakon Sve akcije na suspenziji su otkazane Baterija kritična (%1% preostalo) Baterija na niskom nivou (%1% preostalo) Nivo Završeno punjenje Ne mogu da se povežem sa interfejsom baterije.
Provjerite postavu sistema. ne radi ništa samir.ribic@etf.unsa.ba Hibernacija KDE sistem za upravaljanje napajenjem ne može da bude inicijalizovan. Pozadinski program je javio sljedeću grešku: %1
Molimo provjerite sistemsku konfiguraciju Zaključaj ekran Samir Ribić Nema važećih Firefox pozadinskih priključaka  za upravljanje napajanjem. Nova instalacije može da riješi ovaj problem. Pri učitavanju profila Pri izbacivanju profila iz memorije Prikaz dijaloga za odjavu Pokreni skriptu Radim na AC napajanju Radim na baterijama Skripta Isključi nakon Strujni adapter je utaknut. Strujni adapter je izvučen. Izabrani profil „%1“ ne postoji.
Provjerite postavku programa PowerDevil. Ova pravila aktivnosti sprečavaju upravljanje napajanjem ekrana Ova pravila aktivnosti sprečavaju sistem od suspendovanja Ugasi ekran Nepodržani metod suspendovanja Kada se spusti poklopac Kada se pritisne dugme napajanja Baterija vam je na niskom nivou. Ako želite nastaviti rad, uključite računar u struju, ili ga isključite i promijenite bateriju Baerije su potpuno pune Baterija je na kritičnom nivou, sačuvajte svoj rad što je prije moguće. 