��          �   %   �      0  9   1     k     �     �  7   �  	   �     �     �               ,     :     N     U     l  #   y  %   �     �  5   �          %     3  )   ;  ^  e     �     �     �  !   �       
   ,     7  	   ?     I  !   [     }     �     �     �  
   �     �     �  "   �  2   �     +     C     X     f            
                                                               	                                                    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging &Configure Power Saving... Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2015-02-15 13:01+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: bosanski <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-16 06:46+0000
X-Generator: Launchpad (build 17341)
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 %1 Preostalo (%2%) %1% baterije preostalo %1%. Punjenje &Konfiguriši uštedu energije... Nije prisutna Kapacitet: Puni se Prazni se Osvjetljaj ekrana Omogućenu upravljanje napajanjem Potpuno napunjeno Osvjetljaj tastature Model: Nema dostupnh baterija Ne puni se %1% %1% Omogućeno upravljanje napajanjem: Baterija aplet ima omogućenu sistemsku inhibiciju Vrijeme za pražnjenje: Vrijeme za punjenje: Proizvođač: %1% 