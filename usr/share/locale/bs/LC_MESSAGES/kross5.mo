��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  h  �     f     m     �  	   �  	   �  D   �     �     �            /   -  .   ]  #   �  &   �  	   �     �     �  '   �  ,   	     L	     Q	     b	     y	     �	     �	      �	     �	  '   �	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-12-21 17:26+0000
Last-Translator: Ademovic Saudin <sademovic1@etf.unsa.ba>
Language-Team: bosanski <bs@li.org>
Language: bosnian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Launchpad (build 16872)
X-Launchpad-Export-Date: 2013-12-22 05:52+0000
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Opšte Dodaj novu skriptu. Dodaj... Odustati? Komentar: samir.ribic@etf.unsa.ba,sademovic1@etf.unsa.ba,vljubovic@smartnet.ba Izmijeni Izmijeni izabranu skriptu. Izmijeni... Izvrši izabranu skriptu. Ne mogu da stvorim skriptu za interpretator %1. Ne mogu da odredim interpretator za skriptu %1 Ne mogu da učitam interpretator %1 Ne mogu da otvorim datoteku skripte %1 Datoteka: Ikona: Interpretator: Nivo bezbjednosti interpretatora Rubyja Samir Ribić,Ademovic Saudin,Vedran Ljubovic Ime: Nema funkcije %1 Nema interpretatora %1 Ukloni Ukloni izabranu skriptu. Izvrši Skriptna datoteka %1 ne postoji. Zaustavi Zaustavi izvršavanje izabrane skripte. Tekst: 