��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  #  �  %   
  (   (
  2   Q
     �
  "   �
     �
     �
  *         +     H  &   h     �     �  *   �  +   �  3        R     l     �  %   �     �  *   �          "     B     T     e     s     �     �     �      �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: bosnianuniversetranslation
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-02-04 15:36+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 06:27+0000
X-Generator: Launchpad (build 17331)
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Dodane datoteke u Bazaar repozitorij. Dodajem datoteke u Bazaar repozitorij... Dodavanje datoteka u Bazaar repozitorij neuspjelo. Bazaar dnevnik je zatvoren. Potvrda Bazaar promjena neuspjela. Potvrđene Bazaar promjene... Potvrđujem Bazaar promjene... Povlačenje Bazaar repozitorija neuspjelo. Povučen Bazaar repozitorij. Povlačim Bazaar repozitorij... Guranje Bazaar repozitorija neuspjelo. Guranut Bazaar repozitorij. Guram Bazaar repozitorij... Uklonjene datoteke iz Bazaar repozitorija. Uklanjam datoteke iz Bazaar repozitorija... Uklanjam datoteke iz Bazaar repozitorija neuspjelo. Pregled promjena neuspio. Pregledane promjene. Pregledam promjene... Pokretanje Bazaar dnevnika neuspjelo. Pokrećem Bazaar dnevnik... Ažuriranje Bazaar repozitorija neuspjelo. Ažuriran Bazaar repozitorij. Ažuriram Bazaar repozitorij... Dodaj u Bazaar... Upis u Bazaar... Briši Bazaar Bazaar dnevnik Bazaar povlačenje Bazaar slanje Bazaar nadogradnja Prikaži lokalne Bazaar promjene 