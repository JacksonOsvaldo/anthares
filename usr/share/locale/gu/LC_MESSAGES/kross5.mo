��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  6   �     �               1     I  \   \     �  M   �  o     �   �  C   	  X   W	     �	     �	     �	  H   �	     ,
  
   :
  5   E
  8   {
     �
  J   �
       W   #     {  f   �     �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-11-22 00:01+0530
Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>
Language-Team: Gujarati <team@utkarsh.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: KBabel 1.11.4
 સામાન્ય નવી સ્ક્રિપ્ટ ઉમેરો. ઉમેરો... રદ કરશો? ટિપ્પણી: kartik.mistry@gmail.com ફેરફાર પસંદ કરેલ સ્ક્રિપ્ટમાં ફેરફાર કરો. ફેરફાર... પસંદ કરેલ સ્ક્રિપ્ટ ચાલુ કરો. દૂભાષક "%1" માટે સ્ક્રિપ્ટ બનાવવામાં નિષ્ફળ સ્ક્રિપ્ટફાઇલ "%1" માટે દૂભાષક નક્કી કરવામાં નિષ્ફળ દૂભાષક "%1" લાવવામાં નિષ્ફળ સ્ક્રિપ્ટફાઇલ "%1" ખોલવામાં નિષ્ફળ ફાઈલ: ચિહ્ન: દૂભાષક: રૂબી દૂભાષકનું સલામતી સ્તર Kartik Mistry નામ: આવું કોઇ વિધેય "%1" નથી આવું કોઇ દૂભાષક "%1" નથી દૂર કરો પસંદ કરેલ સ્ક્રિપ્ટ દૂર કરો. ચલાવો સ્ક્રિપ્ટફાઇલ %1 અસ્તિત્વમાં નથી. બંધ કરો પસંદ કરેલ સ્ક્રિપ્ટ ચલાવવાનું બંધ કરો. લખાણ: 