��          D      l       �      �   
   	       *      �  K  e  �     I     e  �   �                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2008-07-22 12:17+0530
Last-Translator: Sweta Kothari <swkothar@redhat.com>
Language-Team: Gujarati
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 આ મોડ્યુલમાં <h1>ઘણાબધા ડેસ્કટોપો</h1>, તમે કેટલા વર્ચ્યુઅલ ડેસ્કટોપો ઇચ્છો છો અને કેટલા આ લેબલ થયેલ હોવા જોઇએ તે તમે રૂપરેખાંકિત કરી શકો છો. ડેસ્કટોપ %1 ડેસ્કટોપ %1: ડેસ્કટોપ %1 માટે અહિંયા તમે નામનો પ્રવેશ કરી શકો છો 