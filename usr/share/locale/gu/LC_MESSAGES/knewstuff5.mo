��    S      �  q   L        -        ?     R  �   d  �     �   �  [   Z	  A   �	     �	     
     
     +
     3
     7
  	   D
  
   N
     Y
     n
     �
     �
     �
     �
     �
     �
     �
      �
           3     C     J     N     `     s  	   {  
   �     �     �     �     �     �  1   �  
          B        Z  	   p     z  	   �  =   �     �     �     �  ?   �     7  	   L     V     ]     i     p     �     �     �     �  
   �     �     �  3   �     "     /  #   J     n  	   u          �     �     �     �     �     �  	   �     �       �    :   �  ,   �     %  d  E  {  �  R  &  �   y          "  2   ?  /   r     �     �     �     �     �  @   �  9   .     h     y     �  <   �  4   �       K   +  R   w  $   �  0   �           6  ?   :  @   z     �     �  #   �  >        G     L  2   c  ,   �  `   �     $     A  �   W  	          E   (     n  �   �  1        H     X  .   i  ;   �  #   �     �          )  &   7     ^     x  8   �  :   �     �  J     B   Y     �  #   �  =   �  Y         l      �      �      �   ,   �   H   �   H   9!  N   �!  "   �!  (   �!     "  (   4"         E   $           F   R      @          O   )   <          1   
       L   (   7   4   !             C   +          2             6              D   -   =      *                .   0       5      '               "   ;   K               A      B   9   S   ?   I              >          &           G      8   :   J   P                      3   /   M   N      ,   	                Q           H           #      %        <a href="http://opendesktop.org">Homepage</a> <br />Provider: %1 <br />Version: %1 <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> <qt>Enter passphrase for key <b>0x%1</b>, belonging to<br /><i>%2&lt;%3&gt;</i><br />:</qt> A link to the description of this Get Hot New Stuff itemHomepage All Categories All Providers Authentication error. Author: BSD Become a Fan Category: Changelog: Could not install %1 Create content on server Description: Details Details for %1 Download New Stuff... Enter search phrase here Error Fetch content link from server Fetching provider information... File not found: %1 File to upload: Finish GPL Get Hot New Stuff Get Hot New Stuff! Install Installed Installing Key used for signing: LGPL License: Loading Preview Most downloads Name of the file as it will appear on the website New Upload Newest Note: You can edit, update and delete your content on the website. Opposite to BackNext Order by: Overwrite existing file? Password: Please fill out the information about your upload in English. Preview Images Price Price: Program name followed by 'Add On Installer'%1 Add-On Installer Provider information Provider: Rating Rating: %1% Re: %1 Reason for price: Reset Search: Select Preview... Select Signing Key Server: %1 Set a price for this item Share Hot New Stuff Show the author of this item in a listBy <i>%1</i> Start Upload There was a network error. Timeout. Check Internet connection. Title: Uninstall Update Updating Upload content Upload first preview Upload second preview Upload third preview Uploading Failed Username: Version: You are now a fan. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2009-11-22 00:01+0530
Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>
Language-Team: Gujarati <team@utkarsh.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: KBabel 1.11.4
 <a href="http://opendesktop.org">ઘરપાનું</a> <br />પૂરૂં પાડનાર: %1 <br />આવૃત્તિ: %1 <qt><i>gpg</i> શરૂ કરી શકાતું નથી અને ફાઈલની ચકાસણી કરી શકતા નથી. ખાતરી કરો કે <i>gpg</i> સ્થાપિત થયેલ છે, નહીતર ડાઉનલોડ થયેલ સ્રોતોની ચકાસણી શક્ય નથી.</qt> <qt><i>gpg</i> શરૂ કરી શકાતું નથી અને ઉપલબ્ધ કળો પ્રાપ્ત કરી શકતા નથી. ખાતરી કરો કે <i>gpg</i> સ્થાપિત થયેલ છે, નહીતર ડાઉનલોડ થયેલ સ્રોતોની ચકાસણી શક્ય થશે નહી.</qt> <qt><i>gpg</i> શરૂ કરી શકતા નથી અને ફાઈલને સહી કરી શકતા નથી. ખાતરી કરો કે <i>gpg</i> સ્થાપિત થયેલ છે, નહીતર સ્રોતોનું સહી કરવાનું શક્ય રહેશે નહી.</qt> <qt>કળ <b>0x%1</b> માટે પાસફ્રેજ દાખલ કરો, જે આની છે<br /><i>%2&lt;%3&gt;</i><br />:</qt> ઘર પાનું બધાં વર્ગો બધા પૂરૂં પાડનારાઓ સત્તાધિકરણ ક્ષતિ. લેખક: BSD ચાહક બનો વર્ગ: ફેરફાર: '%1 સ્થાપિત કરી શકાતું નથી સર્વર પર માહિતી બનાવો વર્ણન: વિગતો %1 માટે વિગતો નવી વસ્તુ ડાઉનલોડ કરો... અહીં શોધ પદ દાખલ કરો ક્ષતિ માહિતી કડી સર્વરમાંથી મેળવો પૂરૂં પાડનારની માહિતી લાવે છે... ફાઇલ મળી નહી: %1 અપલોડ કરવાની ફાઇલ: સંપૂર્ણ GPL નવી ઉત્તેજક વસ્તુ મેળવો નવી ઉત્તેજક વસ્તુ મેળવો! સ્થાપન સ્થાપિત સ્થાપન કરે છે સહી કરવા માટે વપરાતી કળ: LGPL લાઈસન્સ: પૂર્વદર્શન લાવે છે સૌથી વધુ ડાઉનલોડ વેબસાઈટ પર દેખાય તે રીતે ફાઇલનું નામ નવું અપલોડ છેલ્લું નોંધ: તમે તમારી માહિતી વેબસાઈટ પર ફેરફાર, સુધારો અને દૂર કરી શકો છો. આગળ ક્રમ વડે: હાજર રહેલ ફાઈલ પર ફરી લખશો? પાસવર્ડ: મહેરબાની કરી તમારા અપલોડ વિશે માહિતી અંગ્રેજીમાં આપો.. પૂર્વદર્શન ચિત્રો કિંમત કિંમત: %1 વધારાનું સ્થાપક પૂરૂં પાડનારની માહિતી પૂરૂં પાડનાર: યોગ્યતા યોગ્યતા: %1% ફરી: %1 કિંમતનું કારણ: ફરી ગોઠવો શોધો: પૂર્વદર્શન પસંદ કરો... સહી કરવાની કળ પસંદ કરો સર્વર: %1 આ વસ્તુ માટે કિંમત નક્કી કરો નવી ઉત્તેજક વસ્તુ વહેંચો <i>%1</i> વડે અપલોડ શરૂ કરો અહીં નેટવર્ક ક્ષતિ હતી. સમયસમાપ્તિ. ઇન્ટરનેટ જોડાણ ચકાસો. શીર્ષક: અસ્થાપન સુધારો સુધારે છે માહિતી અપલોડ કરો પ્રથમ પૂર્વદર્શન અપલોડ કરો બીજું પૂર્વદર્શન અપલોડ કરો ત્રીજું પૂર્વદર્શન અપલોડ કરો અપલોડ નિષ્ફળ વપરાશકર્તાનામ: આવૃત્તિ: તમે હવે ચાહક છો. 