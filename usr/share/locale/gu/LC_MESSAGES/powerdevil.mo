��          �      |      �     �     �  N   �  
   K      V  	   w     �     �     �     �     �  
   �     �     �  &     %   .  f   T     �     �     �     �  �       �  	   �  �   �     �  ,   �     �  5   �  [   1  /   �  ,   �  B   �  +   -     Y      u  Y   �  ]   �  (  N	  )   w
  <   �
  M   �
  I   ,                                              	                                               
     min After Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate Lock screen NAME OF TRANSLATORSYour names On Profile Load On Profile Unload Prompt log out dialog Run script Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2010-06-22 16:52+0530
Last-Translator: Sweta Kothari <swkothar@redhat.com>
Language-Team: Gujarati
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
  મિનિટ પછી બેટરી ઇન્ટરફેસને જોડી શકાયુ નહી.
મહેરબાની કરીને તમારા સિસ્ટમ રૂપરેખાંકનને ચકાસો કંઇ ન કરો swkothar@redhat.com, kartik.mistry@gmail.com હાઈબરનેટ સ્ક્રીનને તાળુ મારો શ્ર્વેતા કોઠારી, કાર્તિક મિસ્ત્રી પ્રોફાઇલ લાવવા પર પ્રોફાઈલ ગયા પછી બહાર નીકળવાનો સંવાદ પૂછો સ્ક્રિપ્ટ ચલાવો સ્ક્રિપ્ટ પાવર બંધ કરો પાવર એડેપ્ટર લગાવવામાં આવ્યું છે. પાવર એડેપ્ટર કાઢી નાખવામાં આવેલ છે. રૂપરેખા "%1" ને પસંદ કરી દેવામાં આવી છે, પરંતુ તે અસ્તિત્વ ધરાવતી નથી.
મહેરબાની કરીને તમારા PowerDevil રૂપરેખાંકનને ચકાસો. સ્ક્રિન બંધ કરો ન આધારિત સસ્પેન્ડ રીતો જ્યારે લેપટોપ લીડ બંધ કરેલ છે જ્યારે પાવર બટન દબાવ્યું છે 