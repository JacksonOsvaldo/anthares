��    �     �  �  |-      �<     �<     �<     �<     �<  !   =     (=  #   5=     Y=     u=     �=  
   �=  7   �=     �=     �=  	   �=     >  &   >      F>     g>  (   �>  .   �>  /   �>     ?  -   (?     V?     k?      �?     �?  0   �?     �?     @     2@     J@     c@     �@     �@  '   �@      �@  !    A  "   "A  2   EA  ,   xA  "   �A  *   �A     �A  7   B  !   =B  '   _B  #   �B  0   �B  .   �B  4   C     @C     TC  %   iC  *   �C  ,   �C     �C      D  %   'D  -   MD     {D  #   �D  &   �D  	   �D     �D     �D  	   �D  2  E     7G     @G     MG     VG  &   ]G     �G     �G  /   �G     �G  -   �G  	   "H  %   ,H  *   RH  5   }H  4   �H     �H     �H     �H     �H     
I     I     )I     EI     SI     ZI     iI  &   �I     �I     �I     �I  	   �I  
   �I  $   �I  %   J  5   6J  4   lJ      �J     �J  +   �J     �J     K     (K  	   GK     QK     gK     }K     �K     �K     �K     �K     �K     �K     �K  	   �K     �K     L     L     L  	   0L     :L     AL     VL     ^L     qL     �L     �L  	   �L  
   �L     �L  )   �L  ,   M  )   1M  +   [M     �M     �M  /   �M  +   �M  :   N  ?   VN     �N  !   �N  9   �N  4   �N  #   4O  #   XO     |O     �O     �O     �O     �O  %   	P      /P      PP     qP     �P     �P  '   �P  '   �P  &   !Q      HQ      iQ     �Q     �Q     �Q     �Q     �Q     �Q     	R     R     R     ,R     >R  
   GR     RR     YR     eR      lR     �R  %   �R     �R     �R     �R  '   �R  -   !S  )   OS     yS  (   �S     �S     �S     �S     �S     �S     T  &   T     >T  #   PT     tT  0   �T  *   �T  #   �T     U  J   &U  �  qU  k   DW  6  �W  �   �X  I  �Y     �Z  b   [  !   w[  '   �[  
   �[  '   �[     �[  0   \  
   D\     O\      ^\  >   \  2   �\  B   �\     4]     N]     k]     �]     �]     �]      �]  .   �]     %^  #   8^  0   \^  .   �^  5   �^  A   �^  '   4_  %   \_  '   �_  %   �_  )   �_     �_  #   `  ;   <`  %   x`     �`  -   �`     �`     a     (a  '   Ga  1   oa  >   �a  6   �a  >   b  5   Vb  0   �b  !   �b     �b     �b     c  6   >c  /   uc  3   �c  #   �c  *   �c  (   (d  "   Qd  *   td  '   �d  0   �d  )   �d     "e  #   @e  #   de  #   �e  /   �e  !   �e     �e  ;   f  5   Wf      �f  (   �f  )   �f  )   g  +   +g  9   Wg  (   �g     �g     �g  8   �g     %h     =h     Zh  +   xh  J   �h     �h     �h  $   i     4i  "   Ri  &   ui  <   �i  =   �i     j  5   .j  ,   dj  )   �j  )   �j  '   �j  +   k  0   9k  ,   jk  0   �k  ,   �k  -   �k  "   #l      Fl      gl  +   �l  +   �l     �l     �l     �l     m  !   .m     Pm     fm     mm     um  
   �m     �m     �m  
   �m  L   �m     �m     n  #   n  -   =n     kn  �   tn     	o  <   o  A   Mo  
   �o  A   �o     �o  1   �o     'p     -p  (   9p  )   bp  0   �p  B   �p      q     q     q     q     ,q     <q     Lq  
   Zq      eq  5   �q     �q     �q     �q     �q  4   r  0   6r  9   gr  0   �r  /   �r  4   s     7s     >s     Fs     Ms     ^s  !   }s     �s     �s     �s  (   �s  !   t  #   't  =   Kt     �t  )   �t     �t  0   �t     �t     u     8u     Ku  "   ju     �u     �u  �   �u     �v     �v     �v  
   �v  (   �v     "w     1w     :w     Zw     xw     �w     �w     �w     �w  	   �w  !   �w     �w     x     x  $   x  &   Cx  !   jx     �x     �x  2   �x     �x  
   �x     �x     
y     &y     -y  
   6y  N   Ay  	   �y     �y     �y     �y  %   �y     �y  $   �y     z     *z     7z  Z   Gz     �z     �z     �z     �z  +   �z  +   �z     '{     3{     ;{  (   @{     i{     y{  '   �{     �{  1   �{  4   �{  	    |     *|     2|     ?|     T|  /   h|     �|     �|  
   �|      �|  )   �|     }     	}     }  "   }     A}     U}     g}  5   o}  9   �}  K   �}  E   +~  >   q~  E   �~  @   �~  E   7  F   }  G   �  C   �     P�  	   U�     _�     d�  ,   w�  '   ��  $   ̀  q   �  `   c�  1   ā     ��  *   �  ,   8�     e�     ��  "   ��  5   ��     ��     �     %�  S   E�     ��  E   ��  *   �     �      �     >�  %   ^�     ��     ��  +   ��     �     �     �     (�     6�     N�     e�     |�  �   ��  V  7�  �   ��  Q   ��  �   ��  �   щ  �  x�    �  n   �  b   ��  �   �  �   ��  �   _�  >    �  <   _�  }   ��  ~   �  }   ��  �   �     ��     ��     ��     ��     ɓ     ͓     ۓ  )   �  $   �     9�     R�     d�     z�     ��     ��     ��     ה     ۔  !   �  	   �     �     8�     H�     X�     h�     n�     ��     ��     ��     ��  J   ��  J   ��  F   J�     ��     ��     ��     ��     Ȗ     ۖ     �     ��     �     �     '�     ;�     U�  *   Y�  (   ��  :   ��     �     ��     �  �   6�  	   5�  
   ?�     J�     V�     d�     k�     p�  !   u�  &   ��     ��  O   ę  	   �     �     5�  
   K�  %   V�     |�  %   ��      ��     ܚ  +   �  
   �     (�     :�     F�     S�  �   [�  �   �  /   ��     ��     Ŝ     Ҝ  (   ��     	�     �  	   !�     +�     F�     X�     f�     �  �   ��  &   ��  0   ��  4   ٞ  !   �  (   0�     Y�     x�     ��  7   ��     �  %   ��     �     8�     S�     [�      r�     ��     ��     ��  &   Р  +   ��     #�     )�  V  6�     ��     ��  #   ��     ̢  #   �     �     !�     7�     <�     W�     f�     r�     v�  $   y�     ��  	   ��  �   ��  �   4�  $   ��     �     /�     N�     `�     t�  0   ��     ��  .   ʥ  �   ��  
  ��     ��  3   ��     ާ     ��     �  �   �  U   ��  5   �     �     %�     .�     ;�     P�  (   n�     ��     ��     ��  	   ��  ?   ʩ  :   
�  "   E�  	   h�     r�  �  v�  E   $�  >   j�      ��  [   ʬ  U   &�     |�  X   ��  8   ��  M   &�  -   t�     ��  �   ��     b�     i�  	   q�  +   {�  K   ��  5   �  ,   )�  P   V�  l   ��  i   �  /   ~�  k   ��  %   �  9   @�  9   z�  .   ��  _   �  &   C�  <   j�  &   ��  '   γ  E   ��  2   <�  /   o�  C   ��  H   �  B   ,�  I   o�  `   ��  Q   �  D   l�  U   ��     �  v   &�  D   ��  @   �  G   #�  l   k�  _   ظ  o   8�  .   ��  +   ׹  P   �  y   T�  n   κ  O   =�  L   ��  >   ڻ  V   �  M   p�  T   ��  X   �     l�     ��     ��     ��  �  ν     u�     ~�     ��     ��  T   ��  R   �  5   j�  �   ��     #�  E   +�  	   q�  A   {�  X   ��  W   �  O   n�     ��     ��     ��  (   ��     
�  ,   '�  <   T�  "   ��     ��  &   ��  G   ��  f   *�     ��     ��  K   ��      �     �  p   -�  _   ��  l   ��  p   k�  G   ��     $�  g   C�     ��  &   ��  J   ��     ;�  &   K�     r�  0   ��  -   ��  K   ��     =�     K�     P�     W�     c�     i�     �  "   ��     ��  "   ��     ��     �  9   �     O�  6   \�  A   ��  J   ��  ;    �     \�  &   r�  ,   ��  Z   ��  T   !�  ]   v�  ]   ��  C   2�  8   v�  y   ��  f   )�  �   ��  �   �     ��  J   ��  �   ��  r   ��  P   �  S   W�  T   ��  j    �  9   k�  @   ��  G   ��  q   .�  U   ��  U   ��  T   L�  _   ��  _   �  a   a�  b   ��  c   &�  K   ��  K   ��     "�  9   2�  ;   l�  '   ��  0   ��  T   �     V�     m�  "   q�     ��     ��  %   ��     ��     
�     *�  W   :�     ��  �   ��  C   J�  F   ��  $   ��  \   ��  w   W�  R   ��  #   "�  U   F�  )   ��  "   ��     ��  <   ��  5   9�     o�  j   ��  )   ��  d   �  d   �  �   ��  �   {�  ~   �  3   ��  �   ��  �  O�  �   4�    �  �  �  $  ��  :   �  �   G�  J   5�  u   ��     ��  S   �  8   Y�  d   ��     ��     �  :   �  �   T�  `   ��  �   C�     ��  0   ��  0   )�  <   Z�  %   ��  9   ��  <   ��  e   4�     ��  D   ��  K   ��  `   B�  {   ��  �   �  ]   ��  Q   �  Z   U�  G   ��  C   ��  :   <�  M   w�  �   ��  A   i�  ;   ��  `   ��  ?   H�  E   ��  N   ��  R   �  w   p�  q   ��  k   Z�  q   ��  j   8�  [   ��  A   ��  r   A�  8   ��  3   ��  W   !  e   y  �   �  9   c E   � g   � N   K j   � u    _   { 8   � 0    E   E E   � F   � {    =   � 8   � q    `   } ?   � S    ]   r L   � _    �   } X   	 $   Z	 5   	 y   �	 8   /
 6   h
 7   �
 Q   �
 �   )    � %   � c   � ^   O b   � g    �   y �   : I   � �   ( f   � f    `   � o   � r   T �   � x   L �   � w   J q   � J   4 u    u   � �   k �   � %   �    � ;   � ;   � Q   3 4   �    �    � %   �    �        $    = �   \ #   , #   P f   t u   � 	   Q h  [    � �   � �   � $   H �   m    � m       �    � f   � g    s   � �   � #   �     �     �     �  ?   �  %   ?! 9   e!    �! V   �! �   " -   �"    �"    �" B   �" s   /# n   �# �   $ {   �$ d   % w   {%    �%     &     & 1   6& D   h& ]   �&    ' 8   ' /   W' n   �' Q   �' Q   H( �   �( 1   <) �   n)     * d   * Z   i* x   �*    =+ :   P+ 7   �+ ,   �+ !   �+ m  , '   �. *   �. `   �.    4/ |   S/    �/    �/ O   0 n   W0     �0 #   �0 5   1 L   A1    �1    �1 v   �1    $2 :   D2    2 W   �2 W   �2 U   33 	   �3 )   �3 y   �3 #   74    [4 :   r4 G   �4    �4    5    5 �   /5    �5 R   �5    Q6    d6 ^   }6    �6 M   �6 ,   F7    s7     �7   �7    �8    �8 "   �8    	9 X   9 X   v9 "   �9    �9    : P   :    ^: "   x: p   �:    ; �   "; �   �;    8<    R< 8   _< M   �< K   �< M   2=    �=    �= "   �= K   �= o   )>    �> %   �> 	   �> F   �> /    ? 4   P? 	   �? �   �? �   $@ �   �@ �   �A �   �B �   nC �   6D �   �D �   �E �   �F �   QG 	   H    H 	   3H    =H d   ZH b   �H K   "I &  nI    �J �   �K <   L N   \L [   �L E   M U   MM 5   �M g   �M &   AN 7   hN B   �N �   �N 7   �O �   �O d   SP    �P O   �P ?   Q j   LQ H   �Q [    R ~   \R    �R 3   �R &   "S    IS 4   fS .   �S *   �S    �S +  T S  1U   �W �   �Y �  AZ -  \ f  5] S  �` �  �b �   �e 9  �f �   �g U  �h k   (j m   �j �   k �   �k �   {l   fm '   }n    �n    �n    �n    �n     �n *   �n {   o e   �o F   �o    Cp &   Zp *   �p :   �p $   �p 3   q    @q *   Dq .   oq    �q    �q    �q    �q    �q    �q ,   �q &   r    /r    4r '   9r �   ar �   -s �   �s    �t    �t    �t    �t (   �t "   u    *u    >u    Du A   `u ,   �u :   �u    
v w   v q   �v u   �v -   nw '   �w !   �w �  �w &   �y 3   �y !   z 1   3z    ez    {z    �z K   �z \   �z    D{ �   T{    ,| :   B| 1   }| &   �| h   �| k   ?} w   �} a   #~ B   �~ p   �~    9 "   I    l <   �    � ^  � q  1� W   �� (   ��    $� 3   D� r   x�    �    �    (� I   =� )   �� "   �� @   Ԅ    � �  %� f   ܆ E   C� E   �� R   χ X   "� F   {� 2    2   �� |   (� *   �� Y   Љ C   *� 7   n�    ��    �� K   Ŋ    � c   � )   x� �   �� i   -�    �� %   �� F  Ԍ -   �    I� H   P� I   �� K   � 0   /� I   `�    �� <   ��    �� $   � 	   ?�    I� m   V�    đ 3   ב 
  � �  � D    (   � T   0� )   �� /   �� 2   ߕ �   � 3   �� u   Ӗ f  I� -  �� O   ޚ l   .� G   �� #   �    � I  � �   g� �   B�    ֞ *   �� !   � 1   A� B   s� a   ��    �    5� (   H� %   q� �   �� �   � H   ��    � 	   �          �  �   1  �  B      �      �  �  �  |      �       -  �  �   �  �      �      {      �     *      p  _      �  �  �     R  f  �  h  �    �  �  g  Q   �       �       �           �  e       �  �   z  �     Z  �  T        Y  �      @   �  2      f   1       �          h  �  �  �  �       z   �   �       y  ,       �   �  (       �      ^   �   9   �   �      �                  v  F  �   )       �   �  )  5  j  k      �  D   �  �   c     Y  �          �        x  �  '  �  7  R          �  l  U  J  <  V  �  �         9      T  �  -        �   �   4        u           j   ^  \  �            q           ;      �   "  �     F   E  �       �  c  5       V  4  #  �  G  d      �  s       �               `  �  �    �   �   �      !   �   �  �  a  �      �  "          L  �         �                      N   �       M   N  �   _             �   �      �  �  �  w   �    �   �  �  ,  �          H  �  M  �      �  �  �            �              i   �   '      [  `      �  �  A  �  9  �  �  a   3  K  �   �   d      ]  >  �      0    �          8  �                �       �   /  �       �       R   `  �   �  y      �      �   �       q      �   ~   �      �   )  n   �  T   �      �  �    �  �      ~        �  �  �   �   �         !  �  �   �  8        �   �   ?  �  �   �            �   2  6  ~  �  �           �   �      �  X  �   P      �  ?      %  �    3   =  �        r  }  �  �  D    �   �   �           �         \           �      |  	          [   �  �      �      q    �   �       �  _   <  �  �      �  #  s  {  �       t   b    �  �   �   @  �  �      �  H       �  s  0          y       S  �  �      	        �  C  �  �  $   p   �      W  i     $          �   �  �  b                  �  �    �  >       w      �  &  <   �   �   '       �   v  J      I  :          �      o      �  �  �  F  �      E   g           X           m  �   u  V       �       �   W       Q  a      S  
   n          �         .  �         �  x  }       �  .   N      �  �              �     �      �  B  �  �       �  �  %  �  �   �       O      #           J   �  �      �  B       �      �   4   P   �   �   A  &      �         �       �  �   ,  -       �   �  ]    P  u  �      C          �  l  .  �  �           i  %   :   �  �               n  �   I   e  �      ;  �   ]   1  Z       �  v   �       d  �  �      �          �   U   �  �       �   W  �   I  �  3  A   �  �  �   ;   m   �  $  7   �   �   �    �     &   �      k  �   "   t          j  �   
  �   �     �   �   +       �  �  �               �  �  H          �   �   @  �  �       >          �   �   �                      �   �        �   t  X      �  �  �          �   6   �                  �  �     �  �   �  �         x   �  L   �  �   o  �  l   �   �  �  �  �   L  �   Z      �   �     �     r          !  m  {   �      |           /      �   �   o   �                       �  �  7  \  ^  �  (  �            O        �  K  �       �  �      �  e      �       �  �  �  c     b   G   E  Y   *   �      5     �  f  �  �              	   �      S       �       �      �  (  �  �      2         C       [          g  6      
  Q         :      �  K   0       ?       �  8   �   �  r       D            �  p      O       U      /   =   +    �            M  �   �   �       +  =     �   k   z      G        �    }  h   �   �         �  *          w         # Created by NetworkManager
 # Merged from %s

 %d (disabled) %d (enabled, prefer public IP) %d (enabled, prefer temporary IP) %d (unknown) %d. IPv4 address has invalid prefix %d. IPv4 address is invalid %d. route has invalid prefix %d. route is invalid %s Network %s.  Please use --help to see a list of valid options.
 %u MHz %u Mb/s %u Mbit/s '%d' is not a valid channel '%d' is out of valid range <128-16384> '%d' value is out of range <0-3> '%ld' is not a valid channel '%s' can only be used with '%s=%s' (WEP) '%s' connections require '%s' in this property '%s' contains invalid char(s) (use [A-Za-z._-]) '%s' is ambiguous (%s x %s) '%s' is neither an UUID nor an interface name '%s' is not a number '%s' is not a valid DCB flag '%s' is not a valid Ethernet MAC '%s' is not a valid IBoIP P_Key '%s' is not a valid IPv4 address for '%s' option '%s' is not a valid MAC '%s' is not a valid MAC address '%s' is not a valid PSK '%s' is not a valid UUID '%s' is not a valid Wi-Fi mode '%s' is not a valid band '%s' is not a valid channel '%s' is not a valid channel; use <1-13> '%s' is not a valid duplex value '%s' is not a valid hex character '%s' is not a valid interface name '%s' is not a valid interface name for '%s' option '%s' is not a valid number (or out of range) '%s' is not a valid value for '%s' '%s' is not a valid value for the property '%s' is not valid '%s' is not valid master; use ifname or connection UUID '%s' is not valid; use 0, 1, or 2 '%s' is not valid; use <option>=<value> '%s' is not valid; use [%s] or [%s] '%s' length is invalid (should be 5 or 6 digits) '%s' not a number between 0 and %u (inclusive) '%s' not a number between 0 and %u (inclusive) or %u '%s' not among [%s] '%s' option is empty '%s' option is only valid for '%s=%s' '%s' option requires '%s' option to be set '%s' security requires '%s' setting presence '%s' security requires '%s=%s' '%s' value doesn't match '%s=%s' '%s=%s' is incompatible with '%s > 0' '%s=%s' is not a valid configuration for '%s' (No custom routes) (No support for dynamic-wep yet...) (No support for wpa-enterprise yet...) (default) (none) (unknown error) (unknown) ---[ Property menu ]---
set      [<value>]               :: set new value
add      [<value>]               :: add new option to the property
change                           :: change current value
remove   [<index> | <option>]    :: delete the value
describe                         :: describe property
print    [setting | connection]  :: print property (setting/connection) value(s)
back                             :: go to upper level
help/?   [<command>]             :: print this help or command description
quit                             :: exit nmcli
 0 (NONE) 0 (disabled) 0 (none) 802.1X 802.1X supplicant configuration failed 802.1X supplicant disconnected 802.1X supplicant failed 802.1X supplicant took too long to authenticate 802.3ad ===| nmcli interactive connection editor |=== A (5 GHz) A dependency of the connection failed A password is required to connect to '%s'. A problem with the RFC 2684 Ethernet over ADSL bridge A secondary connection of the base connection failed ADSL ARP ARP targets Access Point Activate Activate a connection Activate connection details Active Backup Ad-Hoc Ad-Hoc Network Adaptive Load Balancing (alb) Adaptive Transmit Load Balancing (tlb) Add Add... Adding a new '%s' connection Addresses Aging time Allow control of network connections Allowed values for '%s' property: %s
 An http(s) address for checking internet connectivity Are you sure you want to delete the connection '%s'? Ask for this password every time Authentication Authentication required by wireless network AutoIP service error AutoIP service failed AutoIP service failed to start Automatic Automatic (DHCP-only) Automatically connect Available properties: %s
 Available settings: %s
 Available to all users B/G (2.4 GHz) BOND BRIDGE BRIDGE PORT BSSID Bluetooth Bond Bond connection %d Bridge Bridge connection %d Broadcast Cancel Carrier/link changed Channel Cloned MAC address Closing %s failed: %s
 Config directory location Config file location Connected Connecting Connecting... Connection '%s' (%s) successfully added.
 Connection '%s' (%s) successfully modified.
 Connection '%s' (%s) successfully saved.
 Connection '%s' (%s) successfully updated.
 Connection is already active Connection profile details Connection sharing via a protected WiFi network Connection sharing via an open WiFi network Connection successfully activated (D-Bus active path: %s)
 Connection with UUID '%s' created and activated on device '%s'
 Connectivity Could not activate connection: %s Could not create editor for connection '%s' of type '%s'. Could not create editor for invalid connection '%s'. Could not create temporary file: %s Could not daemonize: %s [error %u]
 Could not decode private key. Could not generate random data. Could not load file '%s'
 Could not parse arguments Could not re-read file: %s Couldn't convert password to UCS2: %d Couldn't decode PKCS#12 file: %d Couldn't decode PKCS#12 file: %s Couldn't decode PKCS#8 file: %s Couldn't decode certificate: %d Couldn't decode certificate: %s Couldn't initialize PKCS#12 decoder: %d Couldn't initialize PKCS#12 decoder: %s Couldn't initialize PKCS#8 decoder: %s Couldn't verify PKCS#12 file: %d Couldn't verify PKCS#12 file: %s Create Current nmcli configuration:
 DCB or FCoE setup failed DHCP client error DHCP client failed DHCP client failed to start DNS servers DSL DSL authentication DSL connection %d Datagram Deactivate Delete Destination Device Device '%s' has been connected.
 Device details Device disconnected by user or client Device is now managed Device is now unmanaged Disabled Do you also want to clear '%s'? [yes]:  Do you also want to set '%s' to '%s'? [yes]:  Doesn't look like a PEM private key file. Don't become a daemon Don't become a daemon, and log to stderr Don't print anything Dynamic WEP (802.1x) ETHERNET Edit '%s' value:  Edit a connection Edit... Editing existing '%s' connection: '%s' Editor failed: %s Enable STP (Spanning Tree Protocol) Enable or disable WiFi devices Enable or disable WiMAX mobile broadband devices Enable or disable mobile broadband devices Enable or disable system networking Enter '%s' value:  Enter a list of IPv4 addresses of DNS servers.

Example: 8.8.8.8, 8.8.4.4
 Enter a list of IPv6 addresses of DNS servers.  If the IPv6 configuration method is 'auto' these DNS servers are appended to those (if any) returned by automatic configuration.  DNS servers cannot be used with the 'shared' or 'link-local' IPv6 configuration methods, as there is no upstream network. In all other IPv6 configuration methods, these DNS servers are used as the only DNS servers for this connection.

Example: 2607:f0d0:1002:51::4, 2607:f0d0:1002:51::1
 Enter a list of S/390 options formatted as:
  option = <value>, option = <value>,...
Valid options are: %s
 Enter a list of bonding options formatted as:
  option = <value>, option = <value>,... 
Valid options are: %s
'mode' can be provided as a name or a number:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

Example: mode=2,miimon=120
 Enter a list of user permissions. This is a list of user names formatted as:
  [user:]<user name 1>, [user:]<user name 2>,...
The items can be separated by commas or spaces.

Example: alice bob charlie
 Enter bytes as a list of hexadecimal values.
Two formats are accepted:
(a) a string of hexadecimal digits, where each two digits represent one byte
(b) space-separated list of bytes written as hexadecimal digits (with optional 0x/0X prefix, and optional leading 0).

Examples: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 Enter connection type:  Enter the type of WEP keys. The accepted values are: 0 or unknown, 1 or key, and 2 or passphrase.
 Error in configuration file: %s.
 Error initializing certificate data: %s Error: %s
 Error: %s - no such connection profile. Error: %s argument is missing. Error: %s properties, nor it is a setting name.
 Error: %s. Error: %s: %s. Error: '%s' argument is missing. Error: '%s' is not a valid monitoring mode; use '%s' or '%s'.
 Error: '%s' is not valid argument for '%s' option. Error: '--fields' value '%s' is not valid here (allowed field: %s) Error: 'autoconnect': %s. Error: 'connection show': %s Error: 'device show': %s Error: 'device status': %s Error: 'device wifi': %s Error: 'general logging': %s Error: 'general permissions': %s Error: 'networking' command '%s' is not valid. Error: 'save': %s. Error: 'type' argument is required. Error: <setting>.<property> argument is missing. Error: Access point with bssid '%s' not found. Error: Argument '%s' was expected, but '%s' provided. Error: BSSID to connect to (%s) differs from bssid argument (%s). Error: Cannot activate connection: %s.
 Error: Connection activation failed.
 Error: Connection activation failed: %s Error: Connection deletion failed: %s Error: Device '%s' is not a Wi-Fi device. Error: Device '%s' not found. Error: Device activation failed: %s Error: Failed to add/activate new connection: Unknown error Error: NetworkManager is not running. Error: No Wi-Fi device found. Error: No access point with BSSID '%s' found. Error: No arguments provided. Error: No connection specified. Error: No interface specified. Error: No network with SSID '%s' found. Error: Option '%s' is unknown, try 'nmcli -help'. Error: Option '--pretty' is mutually exclusive with '--terse'. Error: Option '--pretty' is specified the second time. Error: Option '--terse' is mutually exclusive with '--pretty'. Error: Option '--terse' is specified the second time. Error: Parameter '%s' is neither SSID nor BSSID. Error: SSID or BSSID are missing. Error: Timeout %d sec expired. Error: Unexpected argument '%s' Error: Unknown connection '%s'. Error: bssid argument value '%s' is not a valid BSSID. Error: cannot delete unknown connection(s): %s. Error: connection is not saved. Type 'save' first.
 Error: connection is not valid: %s
 Error: connection verification failed: %s
 Error: extra argument not allowed: '%s'. Error: failed to modify %s.%s: %s. Error: failed to remove value of '%s': %s
 Error: failed to set '%s' property: %s
 Error: invalid '%s' argument: '%s' (use on/off). Error: invalid <setting>.<property> '%s'. Error: invalid argument '%s'
 Error: invalid connection type; %s
 Error: invalid connection type; %s. Error: invalid extra argument '%s'. Error: invalid or not allowed setting '%s': %s. Error: invalid property '%s': %s. Error: invalid property: %s
 Error: invalid property: %s, neither a valid setting name.
 Error: invalid setting argument '%s'; valid are [%s]
 Error: invalid setting name; %s
 Error: missing argument for '%s' option. Error: missing setting for '%s' property
 Error: no argument given; valid are [%s]
 Error: no setting selected; valid are [%s]
 Error: only one of 'id', uuid, or 'path' can be provided. Error: only these fields are allowed: %s Error: property %s
 Error: save-confirmation: %s
 Error: setting '%s' is mandatory and cannot be removed.
 Error: status-line: %s
 Error: unknown setting '%s'
 Error: unknown setting: '%s'
 Error: value for '%s' argument is required. Error: wep-key-type argument value '%s' is invalid, use 'key' or 'phrase'. Ethernet Ethernet connection %d Failed to decode PKCS#8 private key. Failed to decode certificate. Failed to decrypt the private key. Failed to decrypt the private key: %d. Failed to decrypt the private key: decrypted data too large. Failed to decrypt the private key: unexpected padding length. Failed to encrypt: %d. Failed to finalize decryption of the private key: %d. Failed to find expected PKCS#8 end tag '%s'. Failed to find expected PKCS#8 start tag. Failed to initialize the MD5 context: %d. Failed to initialize the crypto engine. Failed to initialize the crypto engine: %d. Failed to initialize the decryption cipher slot. Failed to initialize the decryption context. Failed to initialize the encryption cipher slot. Failed to initialize the encryption context. Failed to register with the requested network Failed to select the specified APN Failed to set IV for decryption. Failed to set IV for encryption. Failed to set symmetric key for decryption. Failed to set symmetric key for encryption. Forward delay GROUP GSM Modem's SIM PIN required GSM Modem's SIM PUK required GSM Modem's SIM card not inserted GSM Modem's SIM wrong GVRP,  Gateway Hairpin mode Hello time Hide Hostname INFINIBAND IP configuration could not be reserved (no available address, timeout, etc.) IPv4 CONFIGURATION IPv6 CONFIGURATION IV contains non-hexadecimal digits. IV must be an even number of bytes in length. Identity If you are creating a VPN, and the VPN connection you wish to create does not appear in the list, you may not have the correct VPN plugin installed. Ignore Ignoring unrecognized log domain(s) '%s' from config files.
 Ignoring unrecognized log domain(s) '%s' passed on command line.
 InfiniBand InfiniBand P_Key connection did not specify parent interface name InfiniBand connection %d InfiniBand device does not support connected mode Infra Interface:  Invalid IV length (must be at least %d). Invalid IV length (must be at least %zd). Invalid configuration option '%s'; allowed [%s]
 Invalid option.  Please use --help to see a list of valid options. JSON configuration Key LEAP LOOSE_BINDING,  Link down delay Link monitoring Link up delay Link-Local List of plugins separated by ',' Log domains separated by ',': any combination of [%s] Log level: one of [%s] MII (recommended) MTU Make all warnings fatal Malformed PEM file: DEK-Info was not the second tag. Malformed PEM file: Proc-Type was not first tag. Malformed PEM file: invalid format of IV in DEK-Info tag. Malformed PEM file: no IV found in DEK-Info tag. Malformed PEM file: unknown Proc-Type tag '%s'. Malformed PEM file: unknown private key cipher '%s'. Manual Max age Metric Mobile Broadband Mobile broadband connection %d Mobile broadband network password Mode Modem initialization failed ModemManager is unavailable Modify network connections for all users Modify persistent system hostname Modify personal network connections Monitoring connection activation (press any key to continue)
 Monitoring frequency Must specify a P_Key if specifying parent N/A Necessary firmware for the device may be missing Network registration denied Network registration timed out NetworkManager TUI NetworkManager active profiles NetworkManager connection profiles NetworkManager is not running. NetworkManager logging NetworkManager monitors all network connections and automatically
chooses the best connection to use.  It also allows the user to
specify wireless access points which wireless cards in the computer
should associate with. NetworkManager permissions NetworkManager status NetworkManager went to sleep Networking Never use this network for default route New Connection Next Hop No carrier could be established No custom routes are defined. No dial tone No reason given No such connection '%s' Not searching for networks OK OLPC Mesh One custom route %d custom routes Open System Opening %s failed: %s
 PCI PEM certificate had no end tag '%s'. PEM certificate had no start tag '%s'. PEM key file had no end tag '%s'. PIN PIN check failed PIN code is needed for the mobile broadband device PIN code required PPP failed PPP service disconnected PPP service failed to start Parent Password Password:  Passwords or encryption keys are required to access the wireless network '%s'. Path cost Please select an option Prefix Primary Print NetworkManager version and exit Priority Private key cipher '%s' was unknown. Private key password Profile name Property name?  Put NetworkManager to sleep or wake it up (should only be used by system power management) Quit REORDER_HEADERS,  Radio switches Remove Require IPv4 addressing for this connection Require IPv6 addressing for this connection Round-robin Routing SSID SSID length is out of range <1-32> bytes SSID or BSSID:  Search domains Secrets were required, but not provided Security Select the type of connection you wish to create. Select the type of slave connection you wish to add. Select... Service Set Hostname Set hostname to '%s' Set system hostname Setting '%s' is not present in the connection.
 Setting name?  Shared Shared Key Shared connection service failed Shared connection service failed to start Show Show password Slaves Specify the location of a PID file State file location Status of devices Success System policy prevents control of network connections System policy prevents enabling or disabling WiFi devices System policy prevents enabling or disabling WiMAX mobile broadband devices System policy prevents enabling or disabling mobile broadband devices System policy prevents enabling or disabling system networking System policy prevents modification of network settings for all users System policy prevents modification of personal network settings System policy prevents modification of the persistent system hostname System policy prevents putting NetworkManager to sleep or waking it up System policy prevents sharing connections via a protected WiFi network System policy prevents sharing connections via an open WiFi network TEAM TEAM PORT Team Team connection %d The Bluetooth connection failed or timed out The IP configuration is no longer valid The Wi-Fi network could not be found The connection profile has been removed from another client. You may type 'save' in the main menu to restore it.
 The connection profile has been removed from another client. You may type 'save' to restore it.
 The device could not be readied for configuration The device was removed The device's active connection disappeared The device's existing connection was assumed The dialing attempt failed The dialing request timed out The expected start of the response The interval between connectivity checks (in seconds) The line is busy The modem could not be found The supplicant is now available Time to wait for a connection, in seconds (without the option, default value is 30) Transport mode Type 'describe [<setting>.<prop>]' for detailed property description. Type 'help' or '?' for available commands. USB Unable to add new connection: %s Unable to delete connection: %s Unable to determine private key type. Unable to save connection: %s Unable to set hostname: %s Unexpected amount of data after encrypting. Unknown Unknown command argument: '%s'
 Unknown command: '%s'
 Unknown error Unknown log domain '%s' Unknown log level '%s' Unknown parameter: %s
 Usage Usage: nmcli connection delete { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

Delete a connection profile.
The profile is identified by its name, UUID or D-Bus path.

 Usage: nmcli connection edit { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

Edit an existing connection profile in an interactive editor.
The profile is identified by its name, UUID or D-Bus path

ARGUMENTS := [type <new connection type>] [con-name <new connection name>]

Add a new connection profile in an interactive editor.

 Usage: nmcli connection load { ARGUMENTS | help }

ARGUMENTS := <filename> [<filename>...]

Load/reload one or more connection files from disk. Use this after manually
editing a connection file to ensure that NetworkManager is aware of its latest
state.

 Usage: nmcli connection reload { help }

Reload all connection files from disk.

 Usage: nmcli device connect { ARGUMENTS | help }

ARGUMENTS := <ifname>

Connect the device.
NetworkManager will try to find a suitable connection that will be activated.
It will also consider connections that are not set to auto-connect.

 Usage: nmcli device show { ARGUMENTS | help }

ARGUMENTS := [<ifname>]

Show details of device(s).
The command lists details for all devices, or for a given device.

 Usage: nmcli device status { help }

Show status for all devices.
By default, the following columns are shown:
 DEVICE     - interface name
 TYPE       - device type
 STATE      - device state
 CONNECTION - connection activated on device (if any)
Displayed columns can be changed using '--fields' global option. 'status' is
the default command, which means 'nmcli device' calls 'nmcli device status'.

 Usage: nmcli general hostname { ARGUMENTS | help }

ARGUMENTS := [<hostname>]

Get or change persistent system hostname.
With no arguments, this prints currently configured hostname. When you pass
a hostname, NetworkManager will set it as the new persistent system hostname.

 Usage: nmcli general logging { ARGUMENTS | help }

ARGUMENTS := [level <log level>] [domains <log domains>]

Get or change NetworkManager logging level and domains.
Without any argument current logging level and domains are shown. In order to
change logging state, provide level and/or domain. Please refer to the man page
for the list of possible logging domains.

 Usage: nmcli general permissions { help }

Show caller permissions for authenticated operations.

 Usage: nmcli general status { help }

Show overall status of NetworkManager.
'status' is the default action, which means 'nmcli gen' calls 'nmcli gen status'

 Usage: nmcli general { COMMAND | help }

COMMAND := { status | hostname | permissions | logging }

  status

  hostname [<hostname>]

  permissions

  logging [level <log level>] [domains <log domains>]

 Usage: nmcli networking connectivity { ARGUMENTS | help }

ARGUMENTS := [check]

Get network connectivity state.
The optional 'check' argument makes NetworkManager re-check the connectivity.

 Usage: nmcli networking off { help }

Switch networking off.

 Usage: nmcli networking on { help }

Switch networking on.

 Usage: nmcli networking { COMMAND | help }

COMMAND := { [ on | off | connectivity ] }

  on

  off

  connectivity [check]

 Usage: nmcli radio all { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of all radio switches, or turn them on/off.

 Usage: nmcli radio wifi { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of Wi-Fi radio switch, or turn it on/off.

 Usage: nmcli radio wwan { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of mobile broadband radio switch, or turn it on/off.

 Username VLAN VLAN connection %d VLAN id VPN VPN connected VPN connecting VPN connecting (getting IP configuration) VPN connecting (need authentication) VPN connecting (prepare) VPN connection %d VPN connection failed VPN disconnected Valid connection types: %s
 Verify connection: %s
 Verify setting '%s': %s
 WEP WEP 128-bit Passphrase WEP 40/128-bit Key (Hex or ASCII) WEP index WEP key index1 (Default) WEP key index2 WEP key index3 WEP key index4 WI-FI WPA & WPA2 Enterprise WPA & WPA2 Personal WPA1 WPA2 WWAN radio switch Waits for NetworkManager to finish activating startup network connections. Warning: editing existing connection '%s'; 'con-name' argument is ignored
 Warning: editing existing connection '%s'; 'type' argument is ignored
 Wi-Fi Wi-FiAutomatic Wi-FiClient Wi-Fi connection %d Wi-Fi radio switch Wi-Fi scan list Wi-Fi securityNone WiMAX Wired Wired 802.1X authentication Wired connection %d Writing to %s failed: %s
 XOR You may edit the following properties: %s
 You may edit the following settings: %s
 [ Type: %s | Name: %s | UUID: %s | Dirty: %s | Temp: %s ]
 ['%s' setting values]
 [NM property description] [nmcli specific description] activate [<ifname>] [/<ap>|<nsp>]  :: activate the connection

Activates the connection.

Available options:
<ifname>    - device the connection will be activated on
/<ap>|<nsp> - AP (Wi-Fi) or NSP (WiMAX) (prepend with / when <ifname> is not specified)
 activated activating advertise,  agent-owned,  asleep auth auto back  :: go to upper menu level

 bandwidth percentages must total 100%% bytes change  :: change current value

Displays current value and allows editing it.
 connected connected (local only) connected (site only) connecting connecting (checking IP connectivity) connecting (configuring) connecting (getting IP configuration) connecting (need authentication) connecting (prepare) connecting (starting secondary connections) connection connection failed deactivated deactivating default describe  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 describe [<setting>.<prop>]  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 device '%s' not compatible with connection '%s' disabled disconnected disconnecting don't know how to get the property value element invalid enabled enabled,  field '%s' has to be alone flags are invalid flags invalid flags invalid - disabled full goto <setting>[.<prop>] | <prop>  :: enter setting/property for editing

This command enters into a setting or property for editing it.

Examples: nmcli> goto connection
          nmcli connection> goto secondaries
          nmcli> goto ipv4.addresses
 has to match '%s' property for PKCS#12 help/? [<command>]  :: help for nmcli commands

 help/? [<command>]  :: help for the nmcli commands

 index '%d' is not in range <0-%d> index '%d' is not in the range of <0-%d> invalid '%s' or its value '%s' invalid IPv4 address '%s' invalid IPv6 address '%s' invalid field '%s'; allowed fields: %s and %s, or %s,%s invalid option '%s' invalid option '%s' or its value '%s' invalid priority map '%s' is not a valid MAC address limited long device name%s %s mandatory option '%s' is missing millisecondsms missing name, try one of [%s] missing option must contain 8 comma-separated numbers neither a valid connection nor device given never new hostname nmcli can accepts both direct JSON configuration data and a file name containing the configuration. In the latter case the file is read and the contents is put into this property.

Examples: set team.config { "device": "team0", "runner": {"name": "roundrobin"}, "ports": {"eth1": {}, "eth2": {}} }
          set team.config /etc/my-team.conf
 nmcli tool, version %s
 no no active connection on device '%s' no active connection or device no device found for connection '%s' no item to remove no priority to remove none not a valid interface name not required,  not saved,  off on only one of '%s' and '%s' can be set portal preparing print [all]  :: print setting or connection values

Shows current property or the whole connection.

Example: nmcli ipv4> print all
 print [property|setting|connection]  :: print property (setting, connection) value(s)

Shows property value. Providing an argument you can also display values for the whole setting or connection.
 priority '%s' is not valid (<0-%ld>) property invalid property invalid (not enabled) property is empty property is invalid property is missing property is not specified and neither is '%s:%s' property missing property value '%s' is empty or too long (>64) quit  :: exit nmcli

This command exits nmcli. When the connection being edited is not saved, the user is asked to confirm the action.
 remove <setting>[.<prop>]  :: remove setting or reset property value

This command removes an entire setting from the connection, or if a property
is given, resets that property to the default value.

Examples: nmcli> remove wifi-sec
          nmcli> remove eth.mtu
 requires '%s' or '%s' setting requires presence of '%s' setting in the connection requires setting '%s' property running seconds set [<setting>.<prop> <value>]  :: set property value

This command sets property value.

Example: nmcli> set con.id My connection
 set [<value>]  :: set new value

This command sets provided <value> to this property
 setting this property requires non-zero '%s' property started starting sum not 100% teamd control failed the property can't be changed this property is not allowed for '%s=%s' unavailable unknown unknown device '%s'. unmanaged use 'goto <setting>' first, or 'describe <setting>.<property>'
 use 'goto <setting>' first, or 'set <setting>.<property>'
 value '%d' is out of range <%d-%d> willing,  yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-11-10 15:29+0100
PO-Revision-Date: 2017-04-21 05:23-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=(n!=1);
 # ને NetworkManager દ્દારા બનાવેલ છે
 # ને %s માંથી ભેગુ કરેલ છે

 %d (નિષ્ક્રિય) %d (સક્રિય, સાર્વજનિક IP પસંદ કર્યુ છે) %d (સક્રિય, કામચલાઉ IP પસંદ કર્યુ છે) %d (અજ્ઞાત) %d. IPv4 સરનામાં પાસે અયોગ્ય ઉપસર્ગ છે %d. IPv4 સરનામું અયોગ્ય છે %d. માર્ગ પાસે અયોગ્ય ઉપસર્ગ છે %d. માર્ગ અયોગ્ય છે %s નેટવર્ક %s.  મહેરબાની કરીને યોગ્ય વિકલ્પોની યાદીને જોવા માટે --help ને વાપરો.
 %u MHz %u Mb/s %u Mbit/s '%d' યોગ્ય ચેનલ નથી '%d' એ યોગ્ય <128-16384> સીમાની બહાર છે '%d' એ <0-3> સીમાની બહાર છે '%ld' યોગ્ય ચેનલ નથી '%s' ફક્ત '%s=%s' (WEP) સાથે વાપરી શકાય છે '%s' જોડાણોને આ ગુણધર્મમાં '%s' ની જરૂરિયાત છે '%s' એ અયોગ્ય અક્ષરોને સમાવે છે ([A-Za-z._-] વાપરો) '%s' એ અસ્પષ્ટ છે (%s x %s) '%s' એ ક્યાંતો કદી UUID અથવા ઇન્ટરફેસ નામ ન હતુ '%s' એ સંખ્યા નથી '%s' એ માન્ય DCB નિશાની નથી '%s' એ યોગ્ય ઇથરનેટ MAC નથી '%s' એ યોગ્ય IBoIP P_Key નથી '%s' એ '%s' વિકલ્પ માટે યોગ્ય IPv4 સરનામુ નથી '%s' એ યોગ્ય MAC નથી '%s' એ યોગ્ય MAC સરનામું નથી '%s' એ યોગ્ય PSK નથી '%s' એ યોગ્ય UUID નથી '%s' એ યોગ્ય Wi-Fi સ્થિતિમાં નથી. '%s' એ યોગ્ય બૅન્ડ નથી '%s' એ યોગ્ય ચેનલ નથી '%s' યોગ્ય ચેનલ નથી; <1-13> વાપરો '%s' એ યોગ્ય ડુપ્લેશ કિંમત નથી '%s' એ યોગ્ય હેક્સ અક્ષર નથી '%s' એ એક યોગ્ય ઇન્ટરફેસ નામ છે '%s' એ '%s' વિકલ્પ માટે યોગ્ય ઇન્ટરફેસ નથી '%s' એ યોગ્ય નંબર નથી (સીમાની બહાર) '%s' એ '%s' માટે યોગ્ય કિંમત નથી '%s' એ ગુણધર્મ માટે યોગ્ય કિંમત નથી '%s' યોગ્ય નથી '%s' એ યોગ્ય માસ્ટર નથી; ifname અથવા જોડાણ UUID ને વાપરો '%s' યોગ્ય નથી; 0, 1, અથવા 2 વાપરો '%s' યોગ્ય નથી; <option>=<value> વાપરો '%s' યોગ્ય નથી;  [%s] અથવા [%s] વાપરો '%s' લંબાઇ અયોગ્ય છે (૫ અથવા ૬ અંકો હોવા જોઇએ) '%s' એ 0 અને %u (સંકલિત) વચ્ચેની સંખ્યા નથી '%s' એ 0 અને %u (સંકલિત) અથવા %u વચ્ચેની સંખ્યા નથી '%s' એ [%s] ની વચ્ચે નથી '%s' વિકલ્પ ખાલી છે '%s' વિકલ્પ ફક્ત '%s=%s' માટે યોગ્ય છે '%s' ને સુયોજિત કરવા માટે '%s' વિકલ્પની જરૂરિયાત છે '%s' સુરક્ષાને '%s' સુયોજન હાજરીની જરૂરિયાત છે '%s' સુરક્ષાને '%s=%s' ની જરૂરિયાત છે '%s' કિંમત '%s=%s' સાથે બંધબેસતુ નથી '%s=%s' એ '%s > 0' સાથે સુસંગત નથી '%s=%s' એ '%s' માટે યોગ્ય રૂપરેખાંકન નથી (કોઇ વૈવિધ્યપૂર્ણ માર્ગો નથી) dynamic-wep માટે હજુ સુધી કોઇ આધાર નથી...) (wpa-enterprise માટે હજુ સુધી કોઇ આધાર નથી...) (મૂળભૂત) (કંઇ નહિં) (અજ્ઞાત ભૂલ) (અજ્ઞાત) ---[ ગુણધર્મ મેનુ ]---
set      [<value>]               :: નવી કિંમતને સુયોજિત કરો
add      [<value>]               :: ગુણધર્મમાં નવાં વિકલ્પને ઉમેરો
change                           :: હાલની કિંમત બદલો
remove   [<index> | <option>]    :: કિંમતને કાઢી નાંખો
describe                         :: ગુણધર્મને વર્ણવો
print    [setting | connection]  :: ગુણધર્મને છાપો (સુયોજન/જોડાણ) કિંમત(ઓ)
back                             :: ઉપકરનાં સ્તરે જાવ
help/?   [<command>]             :: આ મદદ અથવા આદેશ વર્ણનને છાપો
quit                             :: nmcli માંથી બહાર નીકળો
 0 (NONE) 0 (નિષ્ક્રિય) ૦ (કંઇ નહિં) 802.1X 802.1X સપ્લિકન્ટ રૂપરેખાંકન નિષ્ફળ 802.1X સપ્લિકન્ટનું જોડાણ તૂટી ગયુ 802.1X સપ્લિકન્ટ નિષ્ફળ 802.1X સપ્લિકન્ટ સત્તાધિકરણ કરવા માટે લાંબો સમય લીધો 802.3ad ===| nmcli પરસ્પર જોડાણ સંપાદક |=== A (5 GHz) જોડાણની નિર્ભરતા નિષ્ફળ '%s' સાથે જોડાવા માટે પાસવર્ડ જરૂરી. ADSL બ્રિજ પર RFC 2684 ઇથરનેટ સાથે સમસ્યા મૂળ જોડાણનું ગૌણ જોડાણ નિષ્ફળ ADSL ARP ARP લક્ષ્યો એક્સેસ પોંઇન્ટ સક્રિય કરો જોડાણ સક્રિય કરો જોડાણ વિગતો સક્રિય કરો ક્રિયા બૅકઅપ Ad-Hoc ઍડ-હોક નેટવર્ક ઍડેપ્ટીવ લૉડ બેલેન્સીંગ (alb) ઍડેપ્ટીવ ટ્રાન્સમીટ લૉડ બેલેન્સીંગ (tlb) ઉમેરો ઉમેરો... નવુ '%s' જોડાણને ઉમેરી રહ્યા છે સરનામાં ઊંમર સમય નેટવર્ક જોડાણોનાં નિયંત્રણની પરવાનગી આપો '%s' ગુણધર્મ માટે પરવાનગી મળેલ કિંમત: %s
 ઇન્ટરનેટ જોડાણને ચકાસવા માટે http(s) સરનામું શું તમે ખરેખર '%s' જોડાણ કાઢી નાંખવા માંગો છો? દર વખતે આ પાસવર્ડ માટે પૂછો સત્તાધિકરણ વાયરલેસ નેટવર્ક માટે સત્તાધિકરણ જરૂરી AutoIP સેવા ભૂલ AutoIP સેવા નિષ્ફળ શરૂ કરવા માટે AutoIP સેવા નિષ્ફળ આપોઆપ આપોઆપ (DHCP-માત્ર) આપોઆપ જોડાવ ઉપલબ્ધ ગુણધર્મો: %s
 ઉપલબ્ધ સુયોજનો: %s
 બધા વપરાશકર્તાઓ માટે ઉપલબ્ધ B/G (2.4 GHz) BOND BRIDGE BRIDGE PORT BSSID બ્લુટુથ બોન્ડ બોન્ડ જોડાણ %d બ્રિજ બ્રિજ જોડાણ %d બ્રોડકાસ્ટ રદ કરો કૅરિઅર/કડી બદલાયેલ છે ચેનલ ક્લોન થયેલ MAC સરનામું %s ને બંધ કરવાનું નિષ્ફળ: %s
 રૂપરેખાંકન ડિરેક્ટરી સ્થાન રૂપરેખાંકન ફાઇલ સ્થાન જોડાયેલ જોડાઇ રહ્યા છે જોડાઇ રહ્યા છીએ... જોડાણ '%s' (%s) સફળતાપૂર્વક ઉમેરાઇ ગયુ.
 જોડાણ '%s' (%s) સફળતાપૂર્વક બદલેલ છે.
 જોડાણ '%s' (%s) સફળતાપૂર્વક સંગ્રહેલ છે.
 જોડાણ '%s' (%s) સફળતાપૂર્વક સુધારાઇ ગયુ.
 જોડાણ પહેલાથી જ સક્રિય છે જોડાણો રૂપરેખા વિગતો સુરક્ષિત થયેલ WiFi નેટવર્ક મારફતે જોડાણ વહેંચણી ખુલ્લા WiFi નેટવર્ક મારફતે જોડાણ વહેંચણી જોડાણ સફળતાપૂર્વક સક્રિય થયેલ છે (D-Bus સક્રિય પાથ: %s)
 UUID '%s' સાથે જોડાણ બનાવેલ છે અને ઉપકરણ '%s' પર સક્રિય થયેલ છે
 જોડાણ જોડાણ સક્રિય કરી શક્યા નહિ: %s જોડાણ '%s' માટે સંપાદક બનાવી શક્યા નહિ કે જે '%s' પ્રકારનું છે. અયોગ્ય જોડાણ '%s' માટે સંપાદક બનાવી શક્યા નહિ. કામચલાઉ ફાઇલ બનાવી શક્યા નહિ: %s ડિમોનાઇઝ કરી શક્યા નહિં: %s [ભૂલ %u]
 ખાનગી કીને ડિકોડ કરી શકાયુ નહિં. રેન્ડમ માહિતી ને ઉત્પન્ન કરી શક્યા નહિં. ફાઇલ '%s' લાવી શક્યા નહિ
 દલીલો પસાર કરી શક્યા નહિ ફાઇલ પુનઃ-વાંચી શક્યા નહિ: %s UCS2 માં પાસવર્ડને રૂપાંતરિત કરી શકાયુ નહિં: %d PKCS#12 ફાઇલને ડિકોડ કરી શકાયુ નહિં: %d PKCS#12 ફાઇલને ડિકોડ કરી શકાયુ નહિં: %s PKCS#8 ફાઇલને ડિકોડ કરી શક્યા નહિં: %s પ્રમાણપત્રને ડિકોડ કરી શકાયુ નહિં: %d પ્રમાણપત્રને ડિકોડ કરી શકાયુ નહિં: %s PKCS#12 ડિકોડરને પ્રારંભ કરી શકાયુ નહિં: %d PKCS#12 ડિકોડરને પ્રારંભ કરી શકાયુ નહિં: %s  PKCS#8 ડિકોડરનું પ્રારંભ કરી શક્યા નહિં: %s PKCS#12 ફાઇલને ચકાસી શકાયુ નહિં: %d PKCS#12 ફાઇલને ચકાસી શકાયુ નહિં: %s બનાવો હાલનું nmcli રૂપરેખાંકન:
 DCB અથવા FCoE સુયોજન નિષ્ફળ DHCP ક્લાયન્ટ ભૂલ DHCP ક્લાયન્ટ નિષ્ફળ શરૂ કરવા માટે DHCP ક્લાયન્ટ નિષ્ફળ DNS સર્વરો DSL DSL સત્તાધિકરણ DSL જોડાણ %d ડેટાગ્રામ નિષ્ક્રિય કરો કાઢી નાંખો અંતિમ મુકામ ઉપકરણ ઉપકરણ '%s' ને જોડી દેવામાં આવ્યુ છે.
 ઉપકરણ વિગતો વપરાશકર્તા અથવા ક્લાયન્ટ દ્દારા ઉપકરણનું જોડાણ તૂટી ગયુ ઉપકરણ હવે સંચાલિત થયેલ છે ઉપકરણ હવે સંચાલિત થયેલ નથી નિષ્ક્રિયકૃત શું તમે '%s' ને સાફ કરવા માંગો છો? [હાં]:  શું તમે '%s' માં '%s' ને સુયોજિત કરવા માંગો છો? [હાં]:  PEM ખાનગી કી ફાઇલ જેવી દેખાતી નથી. ડિમન બનો નહિં ડિમન બનો નહિં, અને stderr માં પ્રવેશો કંઇપણ છાપો નહિં વૈશ્વિક WEP (802.1x) ઇથરનેટ '%s' કિંમતમાં ફેરફાર કરો:  જોડાણમાં ફેરફાર કરો ફેરફાર... હાલનાં '%s' જોડાણમાં ફેરફાર કરી રહ્યા છે: '%s' સંપાદન નિષ્ફળ: %s STP (સ્પાનીંગ ટ્રી પ્રોટોકોલ) સક્રિય કરો WiFi  ઉપકરણોને સક્રિય અથવા નિષ્ક્રિય કરો WiMAX મોબાઇલ બ્રોડબેન્ડ ઉપકરણોને સક્રિય અથવા નિષ્ક્રિય કરો મોબાઇલ બ્રોડબેન્ડ ઉપકરણોને સક્રિય અથવા નિષ્ક્રિય કરો સિસ્ટમ નેટવર્કીંગ ને સક્રિય અથવા નિષ્ક્રિય કરો '%s' કિંમતને દાખલ કરો:  DNS સર્વરનાં IPv4 સરનામાંની યાદીને દાખલ કરો.

ઉદાહરણ: 8.8.8.8, 8.8.4.4
 DNS સર્વરની IPv6 સરનામાંની યાદીને દાખલ કરો.  જો IPv6 રૂપરેખાંકન પદ્દતિ એ 'આપોઆપ' હોય તો આ DNS સર્વરો એ પેલાં માટે જોડાયેલ છે (જો કોઇપણ) આપમેળે રૂપરેખાંકન દ્દારા પાછુ મળેલ છે.  DNS સર્વરો 'વહેંચાયેલ' અથવા 'સ્થાનિક કડી' IPv6 રૂપરેખાંકન પદ્દતિઓ સાથે વાપરી શકાતી નથી , અપસ્ટ્રીમ નેટવર્ક નથી. બધી બીજી IPv6 રૂપરેખાંકન પદ્દતિઓમાં, આ DNS સર્વરો એ આ જોડાણ માટે ફક્ત DNS સર્વરો તરીકે વાપરેલ છે.

ઉદાહરણ: 2607:f0d0:1002:51::4, 2607:f0d0:1002:51::1
 નીચે પ્રમાણે ઘડાયેલા S/390 વિકલ્પોની યાદી દાખલ કરો:
  option = <value>, option = <value>,...
માન્ય વિકલ્પો છે: %s
 આ તરીકે બંધારિત થયેલ વિકલ્પોની યાદીને દાખલ કરો:
  વિકલ્પ = <value>, વિકલ્પ = <value>,... 
યોગ્ય કિંમતો આ છે: %s
'સ્થિતિ' નામ અથવા નંબર તરીકે પૂરુ પાડી શકાય છે:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

ઉદાહરણ: mode=2,miimon=120
 વપરાશકર્તા પરવાનગીઓની યાદી દાખલ કરો. આ નીચે પ્રમાણે ઘડાયેલ વપરાશકર્તા નામોની યાદી છે:
  [user:]<user name 1>, [user:]<user name 2>,...
વસ્તુઓ અલ્પવિરામ અથવા ખાલી જગ્યાઓથી અલગ કરી શકાય છે.

ઉદાહરણ: alice bob charlie
 હેક્ઝાડેસિમલ કિંમતોની યાદી તરીકે બાઇટને દાખલ કરો.
બે બંધારણો સ્વીકારેલ છે:
(a) હેક્ઝાડેસિમલ આંકડાની શબ્દમાળા, જ્યાં દરેક બે આંકડા એ એક બાઇટને રજૂ કરે છે
(b) બાઇટની જગ્યાથી અલગ થયેલ યાદી હેક્ઝાડેસિમલ આંકડા તરીકે લખાયેલ બાઇટની યાદી છે (વૈકલ્પિક 0x/0X ઉપસર્ગ સાથે, અને વૈકલ્પિક અગ્રણી 0).

ઉદાહરણો: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 જોડાણ પ્રકાર દાખલ કરો: WEP કીનાં પ્રકારને દાખલ કરો. સ્વીકારેલ કિંમતો આ છે: 0 અથવા અજ્ઞાત, 1 અથવા કી, અને 2 અથવા પાસફ્રેજ.
 રૂપરેખાંકન ફાઇલમાં ક્ષતિ: %s.
 પ્રમાણપત્ર માહિતીને પ્રારંભ કરતી વખતે ભૂલ: %s ભૂલ: %s
 ભૂલ: %s - આવી કોઈ જોડાણ રૂપરેખાનથી. ભૂલ: %s દલીલ ગેરહાજર છે. ભૂલ: %s ગુણધર્મો, અથવા તે સુયોજન નામ નથી.
 ભૂલ: %s. ભૂલ: %s: %s. ભૂલ: '%s' દલીલ ગેરહાજર છે. ભૂલ: '%s' એ યોગ્ય મોનિટરીંગ સ્થિતિ નથી; '%s' અથવા '%s' ને વાપરો.
 ભૂલ: '%s' એ '%s' વિકલ્પ માટે યોગ્ય દલીલ નથી. ભૂલ: '--fields' કિંમત '%s' અહિંયા યોગ્ય નથી (પરવાનગી મળેલ ક્ષેત્ર: %s) ભૂલ: 'autoconnect': %s. ભૂલ: 'જોડાણ બતાવો': %s ભૂલ: 'ઉપકરણ બતાવો': %s ભૂલ: 'ઉપકરણ પરિસ્થિતિ': %s ભૂલ: 'ઉપકરણ wifi': %s ભૂલ: 'સામાન્ય લૉગીંગ': %s ભૂલ: 'સમાન્ય પરવાનગીઓ': %s ભૂલ: 'નેટવર્કીંગ આદેશ' આદેશ '%s' યોગ્ય નથી. ભૂલ: 'save': %s. ભૂલ: 'પ્રકાર' દલીલ જરૂરી છે. ભૂલ: <setting>.<property> દલીલ ગુમ થયેલ છે. ભૂલ: bssid '%s' સાથે પ્રવેશ બિંદુ મળ્યુ નથી. ભૂલ: દલીલ '%s' ઇચ્છિત હતુ, પરંતુ '%s' ને પૂરુ પાડેલ છે. ભૂલ: (%s) માં BSSID સાથે જોડાવું bssid દલીલમાંથી અલગ પડે છે (%s). ભૂલ: જોડાણને સક્રિય કરી શકાતુ નથી: %s.
 ભૂલ: જોડાણ સક્રિયકરણ નિષ્ફળતા.
 ભૂલ: જોડાણ સક્રિય કરવાનું નિષ્ફળ: %s ભૂલ: જોડાણ નિરાકરણ નિષ્ફળ: %s ભૂલ: ઉપકરણ '%s' એ WiFi ઉપકરણ નથી. ભૂલ: ઉપકરણ '%s' મળ્યુ નથી. ભૂલ: ઉપકરણ સક્રિયકરણ નિષ્ફળ: %s ભૂલ: નવાં જોડાણને ઉમેરવાનું/સક્રિય કરવામાં નિષ્ફળ: અજ્ઞાત ભૂલ ભૂલ: NetworkManager ચાલી રહ્યુ નથી. ભૂલ: Wi-Fi ઉપકરણ મળ્યુ નથી. ભૂલ: BSSID '%s' સાથે પ્રવેશ બિંદુ મળ્યુ નથી. ભૂલ: દલીલો પૂરા પાડેલ છે. ભૂલ: જોડાણ સ્પષ્ટ થયેલ નથી. ભૂલ: ઇન્ટરફેસ સ્પષ્ટ થયેલ નથી. ભૂલ: SSID '%s' સાથે નેટવર્ક મળ્યુ નથી. ભૂલ: વિકલ્પ '%s' એ અજ્ઞાત છે, 'nmcli -help' નો પ્રયત્ન કરો. ભૂલ: વિકલ્પ '--pretty' એ '--terse' સાથે પરસ્પર અનોખું છે. ભૂલ: વિકલ્પ '--pretty' એ બીજી વખત સ્પષ્ટ થયેલ છે. ભૂલ: વિકલ્પ '--terse' એ '--pretty' સાથે પરસ્પર અનોખું છે. ભૂલ: વિકલ્પ '--terse' એ બીજી વખત સ્પષ્ટ થયેલ છે. ભૂલ: પરિમાણ '%s' ક્યાંતો SSID અથવા BSSID નથી. ભૂલ: SSID અથવા BSSID ગુમ થયેલ છે. ભૂલ: સમયસમાપ્તિ %d સેકંડ નો સમય સમાપ્ત થઇ ગયો. ભૂલ: અનિચ્છનીય દલીલ '%s' ભૂલ: અજ્ઞાત જોડાણ '%s'. ભૂલ: bssid દલીલ કિંમત '%s' એ માન્ય BSSID નથી. ભૂલ: અજ્ઞાત જોડાણ(ઓ) ને કાઢી શકાતુ નથી: %s. ભૂલ: જોડાણ સગ્રહેલ નથી. પહેલાં 'સગ્રહો' ને ટાઇપ કરો.
 ભૂલ: જોડાણ યોગ્ય નથી: %s
 ભૂલ: જોડાણ ચકાસણી નિષ્ફળ: %s
 ભૂલ: વધારાની દલીલની પરવાનગી મળેલ નથી: '%s'. ભૂલ: %s.%s ને બદલવામાં નિષ્ફળતા: %s. ભૂલ: '%s' ની કિંમતને દૂર કરવામાં નિષ્ફળતા: %s
 ભૂલ: '%s' ગુણધર્મને સુયોજિત કરવામાં નિષ્ફળતા: %s
 ભૂલ: અયોગ્ય '%s' દલીલ: '%s' (ચાલુ/બંધ વાપરો). ભૂલ: અયોગ્ય <setting>.<property> '%s'. ભૂલ: અયોગ્ય દલીલ '%s'
 ભૂલ: અયોગ્ય જોડાણ પ્રકાર; %s
 ભૂલ: અયોગ્ય જોડાણ પ્રકાર; %s. ભૂલ: અયોગ્ય વધારાની દલીલ '%s'. ભૂલ: અયોગ્ય અથવા સુયોજન '%s' ને પરવાનગી મળેલ નથી: %s. ભૂલ: અયોગ્ય ગુણધર્મ '%s': %s. ભૂલ: અયોગ્ય ગુણધર્મ: %s
 ભૂલ: અયોગ્ય ગુણધર્મ: %s, યોગ્ય સુયોજન નામ નથી.
 ભૂલ: અયોગ્ય સુયોજન દલીલ '%s'; યોગ્ય [%s] છે
 ભૂલ: અયોગ્ય સુયોજન નામ; %s
 ભૂલ: '%s' વિકલ્પ માટે ગેરહાજર દલીલ. ભૂલ: '%s' ગુણધર્મ માટે ગુમ થયેલ સુયોજન
 ભૂલ: દલીલ આપેલ નથી; યોગ્ય [%s] છે
 ભૂલ: સુયોજન પસંદ થયેલ નથી; યોગ્ય [%s] છે
 ભૂલ: ફક્ત 'id', uuid, અથવા 'path' માંના એકને પૂરુ પાડી શકાય છે. ક્ષતિ: માત્ર આ ક્ષેત્રો માન્ય છે: %s ભૂલ: ગુણધર્મ %s
 ભૂલ: સંગ્રહો-ખાતરી: %s
 ભૂલ: સુયોજન '%s' ફરજિયાત છે અને દૂર કરી શકાતુ નથી.
 ભૂલ: પરિસ્થિતિ-લાઇન: %s
 ભૂલ: અજ્ઞાત સુયોજન '%s'
 ભૂલ: અજ્ઞાત સુયોજન: '%s'
 ભૂલ: '%s' માટે કિંમત દલીલ જરૂરી છે. ભૂલ: wep-key-type દલીલ કિંમત '%s' અયોગ્ય છે, 'key' અથવા 'phrase' ને વાપરો. ઇથરનેટ ઇથરનેટ જોડાણ %d PKCS#8 ખાનગી કીને ડિકોડ કરવામાં નિષ્ફળતા. પ્રમાણપત્રને ડિકોડ કરવાનું નિષ્ફળ. ખાનગી કીને ડિક્રિપ્ટ કરવાનું નિષ્ફળ. ખાનગી કી ને ડિક્રિપ્ટ કરવાનું નિષ્ફળ: %d. ખાનગી કીને ડિક્રિપ્ટ કરવાનું નિષ્ફળ: ડિક્રિપ્ટ થયેલ માહિતી ઘણી લાંબી છે. ખાનગી કી ને ડિક્રિપ્ટ કરવાનું નિષ્ફળ: અનિચ્છનિય પેડિંગ લંબાઇ. એનક્રિપ્ટ કરવાનું નિષ્ફળ: %d. ખાનગી કીનાં ડિક્રિપ્શનનને આખરી રૂપ આપવાનું નિષ્ફળ: %d. ઇચ્છિત PKCS#8 અંત ટેગ '%s' ને શોધવામાં નિષ્ફળ. ઇચ્છિત PKCS#8 શરૂઆત ટૅગને શોધવામાં નિષ્ફળ. MD5 સંદર્ભને પ્રારંભ કરવાનું નિષ્ફળ: %d. ક્રિપ્ટો એંજિન ને પ્રારંભ કરવાનું નિષ્ફળ. ક્રિપ્ટો એંજિનને પ્રારંભ કરવાનું નિષ્ફળ: %d. ડિક્રિપ્શન સાઇફર સ્લોટને પ્રારંભ કરવાનું નિષ્ફળ. ડિક્રિપ્શન સંદર્ભને આખરી રૂપ આપવામાં નિષ્ફળ. એનક્રિપ્શન સાઇફર સ્લોટને પ્રારંભ કરવાનું નિષ્ફળ. એનક્રિપ્શન સંદર્ભને પ્રારંભ કરવામાં નિષ્ફળ. સૂચિત નેટવર્ક સાથે રજીસ્ટર કરવાનું નિષ્ફળ ખાસ APN ને પસંદ કરવાનું નિષ્ફળ ડિક્રિપ્શન માટે IV ને સુયોજિત કરવામાં નિષ્ફળ. એનક્રિપ્શન માટે IV ને સુયોજિત કરવામાં નિષ્ફળ. ડિક્રિપ્શન માટે સમપ્રમાણ કીને સુયોજિત કરવામાં નિષ્ફળ. એનક્રિપ્શન માટે સમપ્રમાણ કીને સુયોજિત કરવામાં નિષ્ફળ. ફોર્વર્ડ ડિલે GROUP GSM મોડેમનું SIM PIN જરૂરી છે GSM મોડેમનું SIM PUK જરૂરી છે GSM મોડેમનું SIM કાર્ડ દાખલ થયેલ છે GSM મોડેમનું SIM ખોટુ છે GVRP,  ગેટવે હેરપિન સ્થિતિ હેલો ટાઇમ છુપાવો યજમાનનામ ઈન્ફીબેન્ડ IP રૂપરેખાંકનને આરક્ષિત રાખી શક્યા નહિં (ઉપલબ્ધ સરનામું નથી, સમય સમાપ્તિ, વગેરે.) IPv4 રૂપરેખાંકન IPv6 રૂપરેખાંકન IV એ બિન-હેક્ઝાડેસિમલ આંકડાઓને સમાવે છે. IV એ લંબાઇમાં બાઇટોનું બેકી નંબર જ હોવુ જ જોઇએ. ઓળખ જો તમે VPN બનાવી રહ્યા હોય, અને જે પ્રકારનું VPN જોડાણ બનાવવા ઇચ્છો તે યાદીમાં દેખાય નહિ, તો તમારી પાસે યોગ્ય VPN પ્લગઇન સ્થાપિત થયેલ નહિ હોઇ શકે. અવગણો રૂપરેખાંકન ફાઇલોમાંના નહિં ઓળખાયેલ લૉગ ડોમેઇન(ઓ) '%s' અવગણી રહ્યા છીએ.
 આદેશ વાક્ય પર આપવામાં આવેલા નહિં ઓળખાયેલ લૉગ ડોમેઇન(ઓ) '%s' અવગણી રહ્યા છીએ.
 ઇન્ફિનિબેન્ડ InfiniBand P_Key જોડાણ એ મુખ્ય ઇન્ટરફેસ નામને સ્પષ્ટ કરતુ નથી InfiniBand જોડાણ %d InfiniBand ઉપકરણ જોડાયેલ સ્થિતિને આધાર આપતુ નથી ઇન્ફ્રા ઈન્ટરફેસ:  અયોગ્ય IV લંબાઇ (ઓછામાં ઓછી %d હોવી જ જોઇએ). અયોગ્ય IV લંબાઇ (ઓછામાં ઓછી %zd હોવી જ જોઇએ). અયોગ્ય રૂપરેખાંકન વિકલ્પ '%s'; પરવાનગી મળેલ [%s]
 અયોગ્ય વિકલ્પ.  મહેરબાની કરીને યોગ્ય વિકલ્પોની યાદી ને જોવા માટે --help ને વાપરો. JSON રૂપરેખાંકન કી LEAP LOOSE_BINDING,  કડી નીચે લાવવામાં વિલંબ કડી મોનીટરીંગ કડી ઉપર આવવામાં વિલંબ કડી-સ્થાનિય ',' દ્દારા અલગ થયેલ પ્લગઇનોની યાદી ',' દ્દારા લૉગ ડોમેઇન અલગ થયેલ છે: [%s] નું કોઇપણ સંયોજન લૉગ સ્તર: [%s] નું એક MII (આગ્રહણીય) MTU બધી ચેતવણીઓને ફેટલ બનાવો મેલફોર્મ થયેલ PEM ફાઇલ: DEK-Info એ બીજો ટેગ હતો નહિં. મેલફોર્મ થયેલ PEM ફાઇલ: Proc-Type એ પહેલો ટેગ ન હતો. મેલફોર્મ થયેલ PEM ફાઇલ: DEK-Info ટેગમાં IV નું અયોગ્ય બંધારણ. મેલફોર્મ થયેલ PEM ફાઇલ: IV એ DEK-Info ટેગમાં શોધાયુ નહિં. મેલફોર્મ થયેલ PEM ફાઇલ: અજ્ઞાત Proc-Type ટેગ '%s'. મેલફોર્મ થયેલ PEM ફાઇલ: અજ્ઞાત ખાનગી કી સાઇફર '%s'. જાતે મહત્તમ ઊંમર મેટ્રિક મોબાઇલ બ્રોડબૅન્ડ મોબાઇલ બ્રોડબૅન્ડ જોડાણ %d મોબાઇલ બ્રોડબૅન્ડ નેટવર્ક પાસવર્ડ સ્થિતિ મોડેમની શરૂઆત નિષ્ફળ ModemManager બિનઉપલબ્ધ છે બધા વપરાશકર્તાઓ માટે નેટવર્ક જોડાણો બદલો નિરંતર સિસ્ટમ યજમાનનામને બદલો વ્યક્તિગત નેટવર્ક જોડાણો બદલો મોનિટરીંગ જોડાણ સક્રિયકરણ (ચાલુ રાખવા માટે કોઇપણ કીને દબાવો)
 મોનીટરીંગ આવૃત્તિ P_Key ને સ્પષ્ટ કરવુ જ જોઇએ જો મુખ્યને સ્પષ્ટ કરી રહ્યા હોય N/A ઉપકરણ માટે જરૂરી ફર્મવેર ગુમ થઇ શકે છે નામંજૂર થયેલ નેટવર્ક રજીસ્ટ્રેશન નેટવર્ક રજીસ્ટ્રેશન કરવાનો સમય સમાપ્ત થઇ ગયો NetworkManager TUI NetworkManager સક્રિય રૂપરેખાઓ NetworkManager જોડાણ રૂપરેખાઓ NetworkManager ચાલતું નથી. NetworkManager લૉગીંગ NetworkManager બધા નેટવર્ક જોડાણોને મોનિટર કરે છે અને આપમેળે વાપરવા માટે સારામાં સારા જોડાણને પસંદ કરે છે.
તે પણ વાયરલેસ પ્રવેશ બિંદુને સ્પષ્ટ કરવા માટે વપરાશકર્તાને પરવાનગી આપે છે કે જે કમ્પ્યૂટરમાં વાયરલેસ કાર્ડ તેની સાથે સંકળાયેલ
હોવા જોઇએ. NetworkManager પરવાનગીઓ NetworkManager પરિસ્થિતિ NetworkManager નિષ્ક્રિય સ્થિતિમાં જતુ રહ્યુ નેટવર્કીંગ મૂળભૂત માર્ગ માટે આ નેટવર્ક ક્યારેય વાપરશો નહિ નવું જોડાણ આગળનો હૉપ કૅરિઅર સ્થાપિત કરી શક્યા નહિં કોઇ વૈવિધ્યપૂર્ણ માર્ગો વ્યાખ્યાયિત નથી. ડાયલ ટોન નથી કારણ આપેલ નથી આવું કોઇ જોડાણ '%s' નથી નેટવર્કો માટે શોધી રહ્યા નથી બરાબર OLPC મેશ એક વૈવિધ્યપૂર્ણ માર્ગ %d વૈવિધ્યપૂર્ણ માર્ગો સિસ્ટમ ખોલો %s ને ખોલવાનું નિષ્ફળ: %s
 PCI PEM પ્રમાણપત્ર પાસે અંત ટેગ '%s' ન હતો. PEM પ્રમાણપત્ર પાસે અંત ટેગ '%s' ન હતો. PEM કી ફાઇલ પાસે છેલ્લો ટેગ '%s' ન હતો. પિન PIN ચકાસણી નિષ્ફળ મોબાઇલ બ્રોડબૅન્ડ ઉપકરણ માટે પિન કોડ જરૂરી છે પિન કોડ જરૂરી PPP નિષ્ફળ તૂટી ગયેલ PPP સેવા જોડાણ શરૂ કરવા માટે PPP સેવા નિષ્ફળ પિતૃ પાસવર્ડ પાસવર્ડ: વાયરલેસ નેટવર્ક '%s' વાપરવા માટે પાસવર્ડો અથવા એનક્રિપ્શન કીઓ જરૂરી છે. પાથ ખર્ચ મહેરબાની કરીને વિકલ્પ પસંદ કરો પૂર્વગ પ્રાથમિક NetworkManager આવૃત્તિને છાપો અને બહાર નીકળો પ્રાધાન્ય ખાનગી કી સાઇફર '%s' એ અજ્ઞાત હતુ. ખાનગી કી પાસવર્ડ રૂપરેખા નામ ગુણધર્મ નામ? NetworkManager ને નિષ્ક્રિય સ્થિતિ અથવા સક્રિય સ્થિતિમાં મૂકો (સિસ્ટમ પાવર સંચાલન દ્દારા ફક્ત વાપરવુ જોઇએ) બહાર નીકળો REORDER_HEADERS,  રેડિયો સ્વીચ દૂર કરો આ જોડાણ માટે IPv4 સરનામાકરણ જરૂરી છે આ જોડાણ માટે IPv6 સરનામાકરણ જરૂરી છે રાઉન્ડ-રૉબિન રાઉટીંગ SSID SSID લંબાઇ  સીમા <1-32> બાઇટની બહાર છે SSID અથવા BSSID:  ડોમેઇનો શોધો ખાનગીની જરૂરિયાત હતી, પરંતુ પૂરુ પાડેલ નથી સુરક્ષા તમે જે પ્રકારનું જોડાણ બનાવવા ઇચ્છો છો તે પસંદ કરો. જે પ્રકારનું ગૌણ જોડાણ તમે ઉમેરવા ઇચ્છો છો તે પસંદ કરો. પસંદ કરો... સેવા યજમાનનામ સુયોજીત કરો યજમાનનામને '%s' માં સુયોજીત કરો સિસ્યમ યજમાનનામ સુયોજીત કરો સુયોજન '%s' એ જોડાણમાં હાજર નથી.
 સુયોજન નામ? વહેંચાયેલ વહેંચાયેલ કી વહેંચાયેલ જોડાણ સેવા નિષ્ફળ શરૂ કરતી વખતે વહેંચાયેલ જોડાણ સેવા નિષ્ફળ બતાવો પાસવર્ડ બતાવો ગૌણ PID ફાઇલનું સ્થાન સ્પષ્ટ કરો સ્થિતિ ફાઇલ સ્થાન ઉપકરણોની પરિસ્થિતિ સફળ સિસ્ટમ પોલિસી એ સિસ્ટમ સુયોજનોનાં નિયંત્રણને અટકાવે છે સિસ્ટમ પોલિસી એ WiFi ઉપકરણોને સક્રિય અથવા નિષ્ક્રિય કરવાનું અટકાવે છે સિસ્ટમ પોલિસી એ WiMAX મોબાઇલ બ્રોડબેન્ડ ઉપકરણોને સક્રિય અથવા નિષ્ક્રિય કરવાનું અટકાવે છે સિસ્ટમ પોલિસી એ મોબાઇલ બ્રોડબેન્ડ ઉપકરણોને સક્રિય અથવા નિષ્ક્રિય કરવાનું અટકાવે છે સિસ્ટમ પોલિસી એ સિસ્ટમ નેટવર્કીંગને નિષ્ક્રિય અથવા સક્રિય કરવાનું અટકાવે છે સિસ્ટમ પોલિસી એ બધા વપરાશકર્તાઓ માટે નેટવર્ક સુયોજનોનાં બદલાવને અટકાવે છે સિસ્ટમ પોલિસી એ વ્યક્તિગત નેટવર્ક સુયોજનોનાં બદલાવને અટકાવે છે સિસ્ટમ પોલિસી એ નિરંતર સિસ્ટમ યજમાનનામનાં બદલાવને અટકાવે છે સિસ્ટમ પોલિસી NetworkManager  ને નિષ્ક્રિય સ્થિતિમાં મૂકલાનું અથવા સક્રિય સ્થિતિમાં રાખવાનું અટકાવે છે સિસ્ટમ પોલિસી એ સુરક્ષિત થયેલ WiFi નેટવર્ક મારફતે વહેંચણી જોડાણોને રોકી રહ્યા છે સિસ્ટમ પોલિસી એ ખુલ્લા WiFi નેટવર્ક મારફતે વહેંચણી જોડાણોને રોકી રહ્યા છે ટીમ ટીમ પોર્ટ જૂથ જૂથ જોડાણ %d બ્લુટુથ જોડાણ નિષ્ફળ અથવા સમયસમાપ્તિ IP રૂપરેખાંકન લાંબા સમય સુધી માન્ય નથી Wi-Fi નેટવર્કને શોધી શક્યા નહિં અન્ય ક્લાયન્ટમાંથી જોડાણ રૂપરેખા દૂર કરવામાં આવી છે. તેને પુનઃસંગ્રહ કરવા માટે તમે મુખ્ય મેનુમાં 'save' લખી શકો છો.
 અન્ય ક્લાયન્ટમાંથી જોડાણ રૂપરેખા દૂર કરવામાં આવી છે. તેને પુનઃસંગ્રહ કરવા માટે તમે 'save' લખી શકો છો.
 ઉપકરણ રૂપરેખાંકન માટે પહેલેથી તૈયાર કરી શક્યુ નહિં જોડાણને દૂર કરાયેલ હતુ ઉપકરણનું સક્રિય જોડાણ અદૃશ્ય ઉપકરણનાં હાલનાં જોડાણને ધારેલ હતુ ડાયલ કરવાનો પ્રયાસ નિષ્ફળ ડાયલ કરવાની માંગણીનો સમય સમાપ્ત જવાબની ઇચ્છિત શરૂઆત જોડાણ ચકાસણી વચ્ચેનો અંતરાલ (સેકંડ માં) લાઇન વ્યસ્ત છે મોડેમ મળી શકે તેમ નથી સપ્લીકન્ટ હવે ઉપલબ્ધ નથી જોડાણ માટે રાહ જોવાનો સમય, સેકંડમાં (વિક્લપ વગર, મૂળભૂત કિંમત 30 છે) ટ્રાન્સપોર્ટ સ્થિતિ વિગત થયેલ ગુણધર્મ વર્ણન માટે '[<setting>.<prop>] વર્ણવો' ટાઇપ કરો. ઉપલબ્ધ આદેશો માટે 'મદદ' અથવા '?' ટાઇપ કરો. USB નવું જોડાણ ઉમેરવામાં અસમર્થ: %s જોડાણ કાઢવામાં અસમર્થ: %s ખાનગી કી પ્રકાર ને નક્કી કરવાનું નિષ્ફળ. જોડાણ સંગ્રહવામાં અસમર્થ: %s યજમાનનામ સુયોજીત કરવામાં અસમર્થ: %s એનક્રિપ્ટ કર્યા પછી માહિતીની અનિચ્છનિય સંખ્યા. અજ્ઞાત અજ્ઞાત આદેશ દલીલ: '%s'
 અજ્ઞાત આદેશ: '%s'
 અજ્ઞાત ભૂલ અજ્ઞાત લોગ ડોમેઇન '%s' અજ્ઞાત લોગ સ્તર '%s' અજ્ઞાત પરિમાણ: %s
 વપરાશ વપરાશ: nmcli connection delete { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

જોડાણ રૂપરેખા કાઢી નાંખો.
રૂપરેખા તેના નામ, UUID અથવા D-Bus પાથથી ઓળખવામાં આવે છે.

 વપરાશ: nmcli connection edit { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

પૂછપરછવાળા સંપાદકમાં હાલની જોડાણ રૂપરેખામાં ફેરફાર કરો.
રૂપરેખા તેના નામ, UUID અથવા D-Bus પાથ દ્વારા ઓળખવામાં આવે છે.

ARGUMENTS := [type <new connection type>] [con-name <new connection name>]

પૂછપરછવાળા સંપાદકમાં નવી જોડાણ રૂપરેખા ઉમેરો.

 વપરાશ: nmcli connection load { ARGUMENTS | help }

ARGUMENTS := <filename> [<filename>...]

ડિસ્કમાંથી એક અથવા વધુ જોડાણ ફાઇલો લાવો/પુનઃલાવો. NetworkManager એ તેની તાજેતરની પરિસ્થિતિથી પરિચિત છે
તેની ખાતરી કરવા માટે જોડાણ ફાઇલમાં જાતે ફેરફાર કર્યા પછી આનો ઉપયોગ
કરો.

 વપરાશ: nmcli connection reload { help }

ડિસ્કમાંથી બધી જોડાણ ફાઇલો પુનઃલોડ કરો.

 વપરાશ: nmcli device connect { ARGUMENTS | help }

ARGUMENTS := <ifname>

ઉપકરણ સાથે જોડાવ.
NetworkManager સુસંગત જોડાણ શોધવાનો પ્રયાસ કરશે કે જે સક્રિય થશે.
તે જોડાણોને પણ ધ્યાનમાં રાખશે કે જે આપોઆપ-જોડાવા માટે સુયોજીત નથી.

 વપરાશ: nmcli device show { ARGUMENTS | help }

ARGUMENTS := [<ifname>]

ઉપકરણ(ઓ) ની વિગતો બતાવો.
આદેશ બધા ઉપકરણોની વિગતોની યાદી આપે છે, અથવા આપેલ ઉપકરણ માટે.

 વપરાશ: nmcli device status { help }

બધા ઉપકરણો માટે સ્થિતિ બતાવો.
મૂળભૂત રીતે, નીચેના સ્તંભો બતાવવામાં આવે છે:
 DEVICE     - ઇન્ટરફેસ નામ
 TYPE       - ઉપકરણ પ્રકાર
 STATE      - ઉપકરણ સ્થિતિ
 CONNECTION - ઉપકરણ પર જોડાણ સક્રિયકૃત થયું (જો કોઇ હોય તો)
દર્શાવવામાં આવેલ સ્તંભો વૈશ્વિક વિકલ્પ '--fields' ની મદદથી બદલી શકાય છે. 'status' એ
મૂળભૂત આદેશ છે, જેનો અર્થ એ થાય કે 'nmcli device' એ 'nmcli device status' ને બોલાવે છે.

 વપરાશ: nmcli general hostname { ARGUMENTS | help }

ARGUMENTS := [<hostname>]

હાજર સિસ્ટમ યજમાનનામ મેળવો અથવા બદલો.
કોઇ દલીલો વિના, આ વર્તમાનમાં રૂપરેખાંકિત યજમાનનામ છાપે છે. જ્યારે તમે યજમાનનામ
પૂરું પાડો, ત્યારે NetworkManager તેને નવી હાજર સિસ્ટમ યજમાનનામ તરીકે સુયોજીત કરશે.

 વપરાશ: nmcli general logging { ARGUMENTS | help }

ARGUMENTS := [level <log level>] [domains <log domains>]

NetworkManager લૉગીંગ સ્તર અને ડૉમેઇનો મેળવો અથવા બદલો.
કોઇપણ દલીલ વિના વર્તમાન લૉગીંગ સ્તર અને ડૉમેઇનો બતાવવા આવેલા છે. લૉગીંગ પરિસ્થિતિ
બદલવા માટે, સ્તર અને/અથવા ડૉમેઇન પૂરું પાડો. સંભવિત લૉગીંગ ડૉમેઇનોની યાદી માટે
મહેરબાની કરીને મદદ પાનાંનો સંદર્ભ લો.

 વપરાશ: nmcli general permissions { help }

સત્તાધિકારીત પ્રક્રિયાઓ માટે બોલાવનારની પરવાનગીઓ બતાવો.

 વપરાશ: nmcli general status { help }

NetworkManager ની આખી પરિસ્થિતિ બતાવો.
'status' એ મૂળભૂત ક્રિયા છે, કે જેનો અર્થ એવો થાય કે 'nmcli gen' એ 'nmcli gen status' ને બોલાવે છે

 વપરાશ: nmcli general { COMMAND | help }

COMMAND := { status | hostname | permissions | logging }

  status

  hostname [<hostname>]

  permissions

  logging [level <log level>] [domains <log domains>]

 વપરાશ: nmcli networking connectivity { ARGUMENTS | help }

ARGUMENTS := [check]

નેટવર્ક જોડાણ પરિસ્થિતિ મેળવો.
વૈકલ્પિક 'check' દલીલ NetworkManager ને જોડાણ પુનઃ-ચકાસવા માટે સક્રિય કરે છે.

 વપરાશ: nmcli networking off { help }

નેટવર્કીંગને બંધ કરો.

 વપરાશ: nmcli networking on { help }

નેટવર્કીંગને ચાલુ કરો.

 વપરાશ: nmcli networking { COMMAND | help }

COMMAND := { [ on | off | connectivity ] }

  on

  off

  connectivity [check]

 વપરાશ: nmcli radio all { ARGUMENTS | help }

ARGUMENTS := [on | off]

બધી રેડિયો સ્વીચની પરિસ્થિતિ મેળવો, અથવા તેમને ચાલુ/બંધ કરો.

 વપરાશ: nmcli radio wifi { ARGUMENTS | help }

ARGUMENTS := [on | off]

Wi-Fi રેડિયો સ્વીચની પરિસ્થિતિ મેળવો, અથવા તેને ચાલુ/બંધ કરો.

 વપરાશ: nmcli radio wwan { ARGUMENTS | help }

ARGUMENTS := [on | off]

મોબાઇલ બ્રોડબેન્ડ રેડિયો સ્વીચની પરિસ્થિતિ મેળવો, અથવા તેને ચાલુ/બંધ કરો.

 વપરાશકર્તાનામ VLAN VLAN જોડાણ %d VLAN id VPN VPN જોડાયેલ છે VPN જોડાઇ રહ્યુ છે VPN જોડાઇ રહ્યુ છે (IP રૂપરેખાંકનને મેળવી રહ્યા છે) VPN જોડાઇ રહ્યુ છે (સત્તાધિકરણની જરૂર છે) VPN જોડાઇ રહ્યુ છે (તૈયાર કરો) VPN જોડાણ %d VPN જોડાણ નિષ્ફળ VPN જોડાણ તૂટી ગયુ યોગ્ય જોડાણ પ્રકારો: %s
 જોડાણ ચકાસો: %s
 સુયોજન '%s' ને ચકાસો: %s
 WEP WEP 128-બીટ પાસફ્રેઝ WEP 40/128-bit કી (Hex અથવા ASCII) WEP અનુક્રમ 1 (મૂળભૂત) 2 3 4 WI-FI WPA & WPA2 એન્ટરપ્રાઇઝ WPA & WPA2 વ્યક્તિગત WPA1 WPA2 WWAN રેડિયો સ્વીચ શરૂઆતનાં નેટવર્ક જોડાણને સક્રિય કરવાનું સમાપ્ત કરવા માટે NetworkManager માટે રાહ જુઓ. ચેતવણી: હાલનાં જોડાણ '%s' માં ફેરફાર કરી રહ્યા છે; 'con-name' દલીલ અવગણે છે
 ચેતવણી: હાલનાં જોડાણ '%s' માં ફેરફાર કરી રહ્યા છે; 'પ્રકાર' દલીલને અવગણેલ છે
 Wi-Fi આપોઆપ ક્લાયન્ટ Wi-Fi જોડાણ %d Wi-Fi રેડિયો સ્વીચ Wi-Fi સ્કેન યાદી કંઇ નહિ WiMAX વાયરવાળું વાયરવાળું 802.1X સત્તાધિકરણ વાયર થયેલ જોડાણ %d %s માં લખવાનું નિષ્ફળ: %s
 XOR તમે નીચેનાં ગુણધર્મોમાં ફેરફાર લાવી શકો છો: %s
 તમે નીચેનાં સુયોજનોમાં ફેરફાર કરી શકે છે: %s
 [ જોડાણ પ્રકાર: %s | નામ: %s | UUID: %s | ખરાબ: %s | કામચલાઉ: %s ]
 ['%s' સુયોજન કિંમતો]
 [NM ગુણધરમ વર્ણન] [nmcli ખાસ વર્ણન] activate [<ifname>] [/<ap>|<nsp>]  :: જોડાણ સક્રિય કરો

જોડાણને સક્રિય કરે છે.

ઉપલબ્ધ વિકલ્પો:
<ifname>    - ઉપકરણનું જોડાણ તેની પર સક્રિય થયેલ હશે
/<ap>|<nsp> - AP (Wi-Fi) અથવા NSP (WiMAX) (/ સાથે જોડાવો જ્યારે <ifname> સ્પષ્ટ થયેલ ન હોય)
 સક્રિય થયેલ છે સક્રિય કરી રહ્યા છે જાહેરાત કરો,  માલિકી થયેલ ઍજન્ટ,  સૂતેલું સત્તા સ્વયં back  :: ઉપરનાં મેનુ સ્તર પર જાવો

 બેન્ડવિથ ટકાનો સરવાળો 100%% થવો જ જોઇએ બાઇટો change  :: હાલની કિંમતને બદલો

હાલની કિંમતને દર્શાવે છે અને તેને બદલવાની પરવાનગી આપે છે.
 જોડાયેલ જોડાયેલ (ફક્ત સ્થાનિક) જોડાયેલ (ફક્ત સાઇટ) જોડાઇ રહ્યા છે જોડાઇ રહ્યા છે (IP જોડાણને ચકાસી રહ્યા છે) જોડાઇ રહ્યા છે (રૂપરેખાંકિત કરી રહ્યા છે) જોડાઇ રહ્યા છે (IP રૂપરેખાંકનને મેળવી રહ્યા છે) જોડાઇ રહ્યા છે (સત્તાધિકરણની જરૂર છે) જોડાઇ રહ્યા છે (તૈયાર કરો) જોડાઇ રહ્યા છે (ગૌણ જોડાણો શરૂ કરી રહ્યા છે) જોડાણ જોડાણ નિષ્ફળ અસક્રિય નિષ્ક્રિય કરી રહ્યા છે મૂળભૂત describe  :: ગુણધર્મને વર્ણવો

ગુણધર્મ વર્ણનને બતાવે છે. બધા NM સુયોજનો અને ગુણધર્મોને જોવા માટે તમે nm-settings(5) પુસ્તિકા પાનાંનો સંપર્ક કરી શકો છો.
 describe [<setting>.<prop>]  :: ગુણધર્મને વર્ણવો

ગુણધર્મ વર્ણનને બતાવે છે. બધા NM સુયોજનો અને ગુણધર્મોને જોવા માટે તમે nm-settings(5) પુસ્તિકા પાનાંનો સંપર્ક કરી શકો છો.
 ઉપકરણ '%s' એ જોડાણ '%s' સાથે સુસંગત નથી નિષ્ક્રિય થયેલ તૂટેલ જોડાણ જોડાઇ તૂટી રહ્યુ છે જાણતા નથી કેવી રીતે ગુણધર્મ કિંમતને મેળવવી ઘટક અયોગ્ય સક્રિય થયેલ સક્રિય,  ક્ષેત્ર '%s' એ એકલુ હોવુ જ જોઇએ ફ્લેગ અયોગ્ય છે ફ્લેગ અયોગ્ય ફ્લેગ અયોગ્ય - નિષ્ક્રિય પૂર્ણ goto <setting>[.<prop>] | <prop>  :: ફેરફાર કરવા માટે સુયોજન/ગુણધર્મને દાખલ કરો

આ આદેશને તેને બદલવા માટે સુયોજન અથવા ગુણધર્મમા દાખલ કરે છે.

ઉદાહરણો: nmcli> goto connection
          nmcli connection> goto secondaries
          nmcli> goto ipv4.addresses
 PKCS#12 માટે '%s' ગુણધર્મ બંધબેસતો હોવો જ જોઇએ help/? [<command>]  :: nmcli આદેશો માટે મદદ

 help/? [<command>]  :: nmcli આદેશો માટે મદદ

 અનુક્રમણિકા '%d' એ સીમા <0-%d> માં નથી અનુક્રમણિકા '%d' એ <0-%d> ની સીમામાં નથી અયોગ્ય '%s' અથવા તેની કિંમત '%s' અયોગ્ય IPv4 સરનામું '%s' અયોગ્ય IPv6 સરનામું '%s' અયોગ્ય ક્ષેત્ર '%s'; માન્ય ક્ષેત્રો: %s અને %s, અથવા %s,%s અયોગ્ય વિકલ્પ '%s' અયોગ્ય વિકલ્પ '%s' અથવા તેની કિંમત '%s' અયોગ્ય પ્રાધાન્ય નક્ષો '%s' આ યોગ્ય MAC સરનામું નથી મર્યાદિત %s %s ફરજિયાત વિકલ્પ '%s' ગુમ થયેલ છે ms ગુમ થયેલ નામ, [%s] માંના એકનો પ્રયત્ન કરો ગુમ થયેલ વિકલ્પ 8 અલ્પવિરામથી-અલગ પડેલી સંખ્યાઓ સમાવતું હોવું જ જોઇએ ક્યાંતો યોગ્ય જોડાણ અથવા ઉપકરણ આપેલ નથી કદી નહિં નવું યજમાનનામ nmcli એ બંને સીધી JSON રૂપરેખાંકન માહિતી અને રૂપરેખાંકન સમાવતું ફાઇલ નામ ધરાવી શકે છે. પછીના કિસ્સામાં ફાઇલ વંચાય છે અને સમાવિષ્ટો આ ગુણધર્મમાં મૂકવામાં આવે છે.

ઉદાહરણો: set team.config { "device": "team0", "runner": {"name": "roundrobin"}, "ports": {"eth1": {}, "eth2": {}} }
          set team.config /etc/my-team.conf
 nmcli સાધન, આવૃત્તિ %s
 ના ઉપકરણ '%s' પર સક્રિય જોડાણ નથી સક્રિય જોડાણ અથવા ઉપકરણ નથી જોડાણ '%s' માટે ઉપકરણ મળ્યુ નથી દૂર કરવા વસ્તુ નથી દૂર કરવા માટે પ્રાધાન્ય નથી કંઇ નહિં યોગ્ય ઇન્ટરફેસ નામ નથી જરૂરી નથી,  સંગ્રહેલ નથી,  બંધ ચાલુ ફક્ત '%s'  નું એક અને '%s' ને સુયોજિત કરી શકાય છે પોર્ટલ તૈયારી કરી રહ્યા છે print [all]  :: સુયોજન અથવા જોડાણ કિંમતને છાપો

હાલનાં ગુણધર્મ અથવા આખા જોડાણને બતાવે છે.

ઉદાહરણ: nmcli ipv4> બધુ છાપો
 print [property|setting|connection]  :: ગુણધર્મ છાપો (સુયોજન, જોડાણ) કિંમત(ઓ)

ગુણધર્મ કિંમતને બતાવે છે. દલીલને પૂરુ પાડી રહ્યા છે તમે આખા સુયોજન અથવા જોડાણ માટે કિંમતોને પણ દર્શાવી શકાય છે.
 પ્રાધાન્ય '%s' યોગ્ય નથી (<0-%ld>) ગુણધર્મ અયોગ્ય ગુણધર્મ અયોગ્ય (સક્રિય થયેલ નથી) ગુણધર્મ ખાલી છે ગુણધર્મ અયોગ્ય છે ગુણધર્મ ગેરહાજર છે ગુણધર્મ સ્પષ્ટ થયેલ નથી અને '%s:%s' પણ કડી સ્પષ્ટ થયેલ નથી ગુણધર્મ ગુમ થયેલ છે ગુણધર્મ કિંમત '%s' ખાલી છે અથવા ઘણું લાંબુ છે (>64) quit  :: nmcli માંથી બહાર નીકળો

આ આદેશ nmcli ને બહાર નીકાળે છે. જ્યારે બદલાયેલ જોડાણ એ સંગ્રહ થયેલ ન હોય, વપરાશકર્તા ક્રિયાની ખાતરી કરવા માટે પૂછે છે.
 remove <setting>[.<prop>]  :: સુયોજનને દૂર કરો અથવા ગુણધર્મ કિંમતને પુન:સુયોજિત કરો

આ આદેશ જોડાણમાંથી બધા સુયોજનને દૂર કરે છે, અથવા જો ગુણધર્મ આપેલ હોય તો, મૂળભૂત કિંમતમાં
ગુણધર્મને પુન:સુયોજિત કરે છ.

ઉદાહરણો: nmcli> remove wifi-sec
          nmcli> remove eth.mtu
 '%s' અથવા '%s' સુયોજનની જરૂરિયાત છે જોડાણમાં '%s' સુયોજનની હાજરીની જરૂરિયાત છે સુયોજન '%s' ગુણધર્મની જરૂર છે ચાલી રહ્યુ છે સેકન્ડો set [<setting>.<prop> <value>]  :: ગુણધર્મ કિંમતને સુયોજિત કરો

આ આદેશ ગુણધર્મ કિંમતને સુયોજિત કરે છે.

ઉદાહરણ: nmcli> એ con.id મારાં જોડાણને સુયોજિત કરે છે
 set [<value>]  :: નવી કિંમતને સુયોજિત કરો

આ આદેશ આ ગુણધર્મમાં પૂરી પાડેલ <value> ને સુયોજિત કરે છે
 આ ગુણધર્મ સુયોજનને શૂન્ય વગરની '%s' ગુણધર્મની જરૂરિયાત છે શરૂ થયેલ છે શરૂ કરી રહ્યા છે સરવાળો 100% નથી teamd નિયંત્રણ નિષ્ફળ ગુણધર્મને બદલી શકાતુ નથી આ ગુણધર્મને '%s=%s' માટે પરવાનગી મળેલ નથી ઉપલબ્ધ નથી અજ્ઞાત અજ્ઞાત ઉપકરણ '%s'. અસંચાલિત થયેલ પહેલાં '<setting> માં જાવ' ને વાપરો, અથવા '<setting>.<property> વર્ણવો'
 પહેલાં '<setting> માં જાવ' ને વાપરો, અથવા '<setting>.<property> સુયોજિત કરો'
 કિંમત '%d' એ સીમા <%d-%d> ની બહાર છે ઇચ્છુક,  હાં 