��            )   �      �     �  $   �  *   �     �          )     C     H     P     m  '   �     �  (   �     �  &        .  ,   J  +   w  _   �  !     )   %  J   O  c   �     �  >     A   F  	   �     �    �     �     �  (   �     �     	  #   .	     R	     W	     _	     x	  '   �	     �	  )   �	     
  *   $
  ,   O
  *   |
  *   �
  U   �
     (  3   @  I   t  \   �       :   #  J   ^  	   �     �                                                                                                 	   
                                      

KDE is unable to start.
 $HOME directory (%1) does not exist. $HOME directory (%1) is out of disk space. $HOME not set! Also allow remote connections Halt Without Confirmation Lock Log Out Log Out Without Confirmation Logout canceled by '%1' No read access to $HOME directory (%1). No read access to '%1'. No write access to $HOME directory (%1). No write access to '%1'. Plasma Workspace installation problem! Reboot Without Confirmation Restores the saved user session if available Sleeping in 1 second Sleeping in %1 seconds Starts <wm> in case no other window manager is 
participating in the session. Default is 'kwin' Starts the session in locked mode Temp directory (%1) is out of disk space. The following installation problem was detected
while trying to start KDE: The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Turn off Writing to the $HOME directory (%2) failed with the error '%1' Writing to the temp directory (%2) failed with
    the error '%1' ksmserver wm Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-15 02:50+0200
PO-Revision-Date: 2014-08-11 19:30+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 

KDE klarte ikke starte .
 $HOME-mappa (%1) finnes ikke. $HOME-mappa (%1) har ikke mer diskplass. $HOME ikke oppgitt. Godta også fjernforbindelser Stopp datamaskinen uten bekreftelse Lås Logg ut Logg ut uten bekreftelse Utlogging avbrutt av «%1» Ingen lesetilgang til $HOME-mappa (%1), Ingen lesetilgang til «%1». Ingen skrivetilgang til $HOME-mappa (%1), Ingen skrivetilgang til «%1». Installasjonsproblem med Plasma Workspace. Start datamaskinen om igjen uten bekreftelse Gjenopprett forrige økt hvis tilgjengelig Hviler om ett sekund Hviler om %1 sekunder Starter <wm> hvis ingen annen vindus-
behandler deltar i økten. Standard er «kwin» Starter økta som låst Den midlertidige mappa (%1) har ikke mer diskplass. Følgende installasjonsproblem  ble funnet mens
KDE ble forsøkt startet: En pålitelig KDE-øktbehandler som snakker standard X11R6-
øktbehandlingsprotokoll (XSMP). Slå av Skriving til $HOME-mappa (%2) mislyktes med feilen «%1». Skriving til  den midlertidige mappa (%2) mislyktes med
    feilen «%1». ksmserver wm 