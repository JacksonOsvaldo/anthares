��          �      ,      �     �     �     �  *   �     �  >   �  9   &     `     {  
   �      �  	   �     �     �  	   �  �       �     �     �     �     �     �  <   �  /        K     d     y  �   �            R   #     v  �   ~              	                                                 
               % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Hibernate Low battery level NAME OF TRANSLATORSYour names Shut down The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2015-04-28 13:59+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % &Kritisk batterinivå: &Lavt nivå: <b>Batterinivåer</b> På kri&tisk nivå: Når batteriet er på dette nivået er det på kritisk nivå Når batteriet er på dette nivået er det lavt Sett opp varslinger … Kritisk batterinivå Ikke gjør noe knut.yrvin@gmail.com,gluras@c2i.net,boerre@skolelinux.no,bjornst@skogkatt.homelinux.org,fri_programvare@bojer.no,sunny@sunbase.org Dvale Lavt batterinivå Knut Yrvin,Gunnhild Lurås,Børre Gaup,Bjørn Steensrud,Axel Bojer,Øyvind A. Holm Slå av Det ser ikke ut til at strømstyringstjenesten kjører.
Dette kan løses ved å starte den eller sette den opp i «Oppstart og avslutning» 