��          �      l      �     �     �     �     �           +  	   L     V     u  &   �     �     �     �  	     �     �   �  t   '  7   �  +   �        �                        !   5     W  
   v     �     �  (   �     �     �            �   '  �   �  m   ?	     �	  8   �	     �	                                       
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2011-10-22 09:31+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  min Bruk som Alltid Definer en spesiell oppførsel Ikke bruk spesielle innstillinger bjornst@skogkatt.homelinux.org Dvalemodus Bjørn Steensrud Slå aldri av skjermen Gå aldri i dvale eller slå av maskinen PC kjører på nettstrøm PC kjører på batteri PC kjører på lavt batteri Slå av Det ser ikke ut til at strømstyringstjenesten kjører.
Dette kan løses ved å starte den eller sette den opp i «Oppstart og avslutning» Aktivitetstjenesten kjører ikke.
Aktivitetstjenesten må kjøre for at det skal være mulig å sette opp strømstyring for hver aktivitet. Aktivitetstjenesten kjører med lav funksjonalitet.
Navn og ikoner for aktiviteter er kanskje utilgjengelige. Aktivitet «%1» Bruk atskilte innstillinger (bare for avanserte brukere) etter 