��    \      �     �      �  !   �     �  	          S   $  	   x     �     �     �     �     �     �     �     �     �     	  J   	     _	     g	      s	  	   �	  
   �	     �	     �	     �	     �	     �	     �	     �	  L   
  	   N
  	   X
  
   b
  
   m
  
   x
     �
     �
     �
     �
     �
     �
     �
     �
     �
     
  	             )     5     =     K     O     _     g     t  
   �  	   �     �  
   �     �     �     �     �     �     �     �          !     >     T     Z     ^     b  H   i     �     �     �     �     �     �  
   �  &   �     #  "   6     Y     w     �     �     �     �  	      �  
  $        *  	   ?     I     R  	   m     w     �     �     �     �     �     �     �     �     �  %   �                )     H  
   Q  	   \     f     v          �     �  	   �  (   �  	   �  	   �  
   �  
   �  
                  '     0     B     R     `     d     x     �     �     �     �  	   �     �     �     �     �     �             	         *  	   /     9     >     C     V     g     }     �     �     �     �     �     �     �     �  8   �     3     ?     F  
   T     _     k  
   n     y     �     �  	   �     �     �     �     �     �     �           A   !       '       "   #   $            Z   ?      K   L   &   5       =   S          ;   8   F   %           @      -   7   Q       6       I             >   3                   G   ,                      E          4   (   M   C       +       J             2       T   <   
       :       N   9           *               Y   .      U   X              P   V      D       0       W             \       /      O         [   H          	          B       R   1       )    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-30 22:59+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Solid-basert modul for enhetsvising © 2010 David Hubner AMD 3DNow ATI IVEC %1 ledig av %2 (%3% brukt) Batterier Batteritype:  Buss:  Kamera Kameraer Ladingsstatus:  Lader Fold sammen alle Kompakt flash-leser En enhet Enhetsinformasjon Viser alle enheter som er listet nå. Enheter Lader ut bjornst@skogkatt.homelinux.org Kryptert Utvid alle Filsystem Filsystemtype:  Fulladet Harddisk Kan kobles til påslått? IDE IEEE 1394 Viser informasjon om den valgte enheten. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Tastatur Tastatur og mus Etikett: Maks. hastighet:  Minnepenn-leser Montert på:  Mus Multimedia-spillere Bjørn Steensrud Nei Ingen lading Ingen data tilgjengelige Ikke montert Ikke satt Optisk drev PDA Partisjonstabell Primær Prosessor %1 Prosessor nummer:  Prossessorer Produkt:  RAID Flyttbar? SATA SCSI SD / MMC kortleser Vis alle enheter Vis relevante enheter Smart media-leser Lagringsdrev Støttede drivere:  Støttede instruksjonssett:  Støttede protokoller:  UDI:  UPS USB UUID:  Viser UDI for gjeldende enhet (Unique Device Identifier) Ukjent drev Ubrukt Leverandør:  Volumrom:  Volumbruk:  Ja kcmdevinfo Ukjent Ingen Ingen Plattform Ukjent Ukjent Ukjent Ukjent Ukjent xD-kortleser 