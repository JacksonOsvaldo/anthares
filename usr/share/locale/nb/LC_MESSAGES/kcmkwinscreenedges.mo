��    #      4  /   L           	            	   !     +     D     Y     g     s     �     �     �     �     �  S   �  w   C     �  M   �           ?  :   K     �  	   �     �     �  %   �     �     
  7   *  e   b  #   �     �            �  $     	     #	     %	     <	     K	     d	     �	     �	     �	     �	     �	  "   �	     
     *
  P   7
  Q   �
     �
  >   �
     (     G  6   U     �     �     �     �  &   �       	     8     \   V  %   �     �     �                                                   	           
            !                                 "                                            #            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Edge Actions Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the side of the screen To trigger an action push your mouse cursor against the edge of the screen in the action's direction. Toggle alternative window switching Toggle window switching Window Management of the screen Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-08 03:04+0200
PO-Revision-Date: 2014-08-09 12:23+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms % %1 – Alle skrivebord %1 – Terning %1 – Gjeldende program %1 – Gjeldende skrivebord %1 – Sylinder %1 – Kule Forsinkelse for &reaktivering: &Bytt skrivebord ved kant: &Forsinkelse for aktivering: Handlinger på aktive skjermkanter Aktivitetsbehandler Alltid aktiv Hvor lang tid må gå etter en utløst handling før neste handling kan utløses Hvor lenge musepekeren må skyves mot kanten av skjermen før handlingen utløses Programstarter Bytt skrivebord når musepekeren skyves mot kanten av skjermen bjornst@skogkatt.homelinux.org Lås skjermen Maksimer vinduer ved å dra dem til toppen av skjermen Bjørn Steensrud Ingen handling Bare ved flytting av vinduer Andre innstillinger Kvadrantflislegging utløst i den ytre Vis skrivebord Slått av Fliselegg vinduer ved å dra dem til en side av skjermen En handling utløses ved å skyve musepekeren mot kanten av skjermen, i handlingens retning. Slå på/av alternativ vindusveksling Slå på/av vindusbytting Vindusbehandling av skjermen 