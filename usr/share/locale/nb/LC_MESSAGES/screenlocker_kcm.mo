��          �      �       H  
   I     T     `  .   f     �     �     �      �  *   �                :  0   G  �  x  
   s  
   ~     �  %   �     �     �  	   �     �  :   �     6     E  
   b  (   m     
                                                        	    Activation Clear Image Error Failed to successfully test the screen locker. Immediately Load from file... Lock Session Lock screen automatically after: Lock screen when waking up from suspension Lock screen: Re&quire password after locking: Select image The global keyboard shortcut to lock the screen. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-11 03:59+0100
PO-Revision-Date: 2015-04-28 13:51+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Aktivering Tøm bilde Feil Klarte ikke å prøve skjermlåseren. Straks Last inn fra fil … Lås økt Lås skjermen automatisk etter: Låser skjermen når systemet vekkes fra hvile eller dvale Lås skjermen: &Krev passord etter låsing: Velg bilde Global snarveistast som låser skjermen. 