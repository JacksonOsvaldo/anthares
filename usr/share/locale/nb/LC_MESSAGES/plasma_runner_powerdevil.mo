��          �      �           	       8   /  �   h  T   �  )   S  (   }  ,   �  0   �  $     &   )  &   P  %   w  ?   �  B   �  F         g     |     �     �     �     �    �     �       2   )  �   \  E   �     D	  
   W	     b	     o	  
   �	  
   �	     �	  	   �	     �	     �	     �	     �	     �	     
     )
  "   ?
  "   b
             	                                
                                                           Dim screen by half Dim screen totally Lists all power profiles and allows them to be activated Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordpower profile Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterpower profile %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Set Profile to '%1' Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: krunner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-23 11:32+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Halver lysstyrke på skjermen Mørklegg skjermen helt Lister opp alle strømprofiler og kan skru på dem Lister lysstyrkevalg for skjermen eller setter den til lysstyrke definert ved :q:; f.eks. skjermlysstyrke 50 vil formørke skjermen til 50% av maksimal lysstyrke Lister valg for systempause (f.eks. hvile, dvale) og kan skru på dem mørklegg skjermen dvalemodus strømprofil lysstyrke på skjerm dvalemodus hvilemodus til disk til minne demp skjermen %1 strømprofil %1 lysstyrke på skjermen %1 Sett lysstyrke til %1 Sett profil til «%1» Dvalemodus til disk Hvilemodus til minnet Legger systemet til hvile i minnet Legger systemet i dvale til disken 