��    2      �  C   <      H     I     a     w     �     �  A   �  9   �  .   5  F   d  :   �  D   �  $   +  W   P  7   �  3   �  (     4   =  .   r  C   �  3   �  k        �     �     �     �  #   �     	     5	     N	  !   n	     �	  "   �	     �	     �	     �	     
     -
     C
     _
     t
  (   �
  +   �
  5   �
  3     7   P  1   �  1   �     �     �  �                      !     %     *  -   1  *   _  E   �  %   �  3   �     *  O   D  %   �  $   �     �  0   �      +  6   L  #   �     �     �     �     �  
   �     �     �  	   �     �                    %     +     4     =     D     M     a     f     w     |  '   �  '   �  +   �  "   �  #   !     E     R     )             0         -                #              2      ,                    %      .   /   
                        1   	          &   *       '                                                "   +             $   (      !       @action:buttonCheckout @action:buttonCommit @action:buttonCreate Tag @action:buttonPull @action:buttonPush @action:button Add Signed-Off line to the message widgetSign off @info:tooltipA branch with the name '%1' already exists. @info:tooltipA tag named '%1' already exists. @info:tooltipAdd Signed-off-by line at the end of the commit message. @info:tooltipBranch names may not contain any whitespace. @info:tooltipCreate a new branch based on a selected branch or tag. @info:tooltipDiscard local changes. @info:tooltipProceed even if the remote branch is not an ancestor of the local branch. @info:tooltipTag names may not contain any whitespace. @info:tooltipThere are no tags in this repository. @info:tooltipThere is nothing to amend. @info:tooltipYou must enter a commit message first. @info:tooltipYou must enter a tag name first. @info:tooltipYou must enter a valid name for the new branch first. @info:tooltipYou must select a valid branch first. @item:intext Prepended to the current branch name to get the default name for a newly created branchbranch @label:listboxBranch: @label:listboxLocal Branch: @label:listboxRemote Branch: @label:listboxRemote branch: @label:listbox a git remoteRemote: @label:textboxTag Message: @label:textboxTag Name: @option:checkAmend last commit @option:checkCreate New Branch:  @option:checkForce @option:radio Git CheckoutBranch: @option:radio Git CheckoutTag: @title:groupAttach to @title:groupBranch Base @title:groupBranches @title:groupCheckout @title:groupCommit message @title:groupOptions @title:groupTag Information @title:group The remote hostDestination @title:group The source to pull fromSource @title:window<application>Git</application> Checkout @title:window<application>Git</application> Commit @title:window<application>Git</application> Create Tag @title:window<application>Git</application> Pull @title:window<application>Git</application> Push Dialog height Dialog width Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2012-08-24 16:26+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Sjekk ut Innlever Opprett tagg Dra Skyv Signer Det finnes fra før en gren som heter «%1». En tagg med navnet «%1» finnes fra før. Legg til «Signed-off-by»-linje til slutten av innleveringsmeldinga. Det kan ikke være tomrom i grennavn. Lag en ny gren basert på en valgt gren eller tagg. Forkast lokale endringer. Fortsett selv om den fjerne grenen ikke er en forgjenger til den lokale grenen. Det kan ikke være tomrom i taggnavn. Det er ingen tagger i dette lageret. Det er ingenting å endre. Du må skrive inn en innleveringsmelding først. Du må først oppgi et taggnavn. Du må først oppgi et gyldig navn for den nye grenen. Du må først velge en gyldig gren. gren Gren: Lokal gren: Fjern gren: Fjerngren: Fjern: Tagg-meldng: Taggnavn: Endre siste innlevering Lag ny gren:  Tving Gren: Tagg: Fest til Grenbase Grener Sjekk ut Innleveringsmelding Valg Tagg-informasjon Mål Kilde <application>Git</application> sjekk ut <application>Git</application> innlever <application>Git</application> Opprett tagg <application>Git</application> dra <application>Git</application> skyv Dialoghøyde Dialogbredde 