��    >        S   �      H     I     R     [     j     |     �  �  �  �   y  -   	  )   3	  -   ]	  +   �	  r   �	  	   *
  	   4
     >
     L
     d
     l
     u
  
   �
     �
     �
     �
     �
      �
     �
     �
     �
           '     -  r   H  5   �     �               )     H  	   M     W     ]     e  (   r     �     �     �     �     �     �  +   	  3   5     i     q          �  "   �  S   �  )        5  �   A         "     +     4     B     U     [  �  z  �   >  /   �                 a     	   v     �     �     �     �  	   �     �  	   �     �  
   �     �  +   �     &     5     F     L     Y     w     ~  {   �  ,        @     \     m     �     �     �     �     �  
   �  "   �     �  %   �  *     ,   I     v     �  &   �  3   �     �     �            "     D   ?      �     �  �   �     )            5       !                 
   8             2   -          $   %      ;   4              <      1                     6   7   9   &       #       	            3      ,            :   (                        =   0   '   /              .       +       *                 >         "        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Animate icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-08 04:00+0100
PO-Revision-Date: 2014-04-25 18:41+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Mengde: &Effekt: &Andre farge: &Halvgjennomsiktig &Tema © 2000––2003 Geert Jansen <h1>Ikoner</h1>Med denne modulen kan du velge ikonene for skrivebordet.<p>For å velge et ikontema, trykk på navnet og bekreft valget ved å trykke på «Bruk»-knappen nedenfor. Hvis du ikke vil bruke dette valget likevel, kan du trykke «Tilbakestill» for å fjerne endringene.</p><p>Når du trykker på «Installer nytt tema …» installerer du et nytt ikontema ved å skrive inn plasseringa i boksen eller lete deg fram til plasseringa. Trykk «OK» for å fullføre installasjonen.</p><p>«Fjern tema» er bare i bruk hvis du velger et tema som du har installert med denne modulen. Temaer som er installert på hele systemet kan du ikke fjerne her.</p><p>Du kan også angi effekter for ikonene.</p> <qt>Er du sikker på at du vil fjerne ikontemaet <strong>%1</strong>?<br /> <br />Dette vil slette de filene som dette temaet har installert.</qt> <qt>Installerer temaet <strong>%1</strong></qt> I bruk Av Standard Det oppsto et problem under installasjonen, men de fleste temaene i arkivet er likevel installert &Avansert Alle ikoner Animer ikoner Antonio Larrosa Jimenez Fa&rge: Fargelegg Bekreftelse Avmetting Beskrivelse Skrivebord Dialoger Dra hit eller skriv nettadressen til temaet gluras@c2i.net Effektparametere Gamma Geert Jansen Hent nye temaer fra Internett Ikoner Kontrollmodul for ikoner Hvis du allerede har et temaarkiv lokalt, så vil denne knappen pakke det opp og gjøre det tilgjengelig for KDE-programmer Installer en tema-arkivfil som finnes lokalt Installerer ikontemaer … Jonathan Riddell Hovedverktøylinje Gunnhild Lurås Navn Ingen effekt Panel Forhåndsvisning Fjern tema Fjern det valgte temaet fra disken Velg effekt … Oppsett av effekter for ikoner i bruk Oppsett av virkemåten til standardikonene Tilpass effekten for ikoner som er slått av Størrelse: Små ikoner Fila er ikke et gyldig ikontema-arkiv. Dette sletter det valgte temaet fra harddisken din. Til grå Til svarthvitt Verktøylinje Torsten Rahn Kan ikke opprette midlertidig fil. Kan ikke laste ned ikontema-arkivet,
sjekk at adressen %1 er riktig. Finner ikke ikontema-arkivet %1. Ikonbruk Du må være tilkoplet Internett for å bruke denne handlinga. En dialog vil vise en liste over temaer fra nettstedet http://www.kde.org. Trykker du på installer-knappen tilknyttet et tema, så b lir den installert lokalt. 