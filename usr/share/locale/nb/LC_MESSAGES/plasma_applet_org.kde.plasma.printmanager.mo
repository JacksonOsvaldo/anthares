��          T      �       �      �      �      �   .   �        H   +  �  t     o     �     �  '   �     �  A   �                                        Active jobs only All jobs Completed jobs only No printers have been configured or discovered Print queue is empty There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2013-08-11 17:25+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Bare aktive jobber Alle jobber Bare fullførte jobber Ingen skrivere er funnet eller satt opp Skrivekøen er tom Det er en utskriftsjobb i køen Det er %1 utskriftsjobber i køen 