��    ;      �  O   �           	          )     H     h     u  
   ~     �     �     �     �  	   �     �     �     �  ,   �  	   +     5  
   T     _     w     �     �     �     �  	   �     �     �     �                       	   +  
   5     @     G  
   W     b     i     }     �     �     �     �     �     �  	   �     �     �  ,        9     A     Q     ]     d     v  #   �  �  �     �
     �
     �
     �
  
     
     
     
   &  	   1     ;     N  
   _     j          �  +   �  
   �     �     �               +     8     F     O     W     ]     f     t     z          �     �  	   �  
   �     �     �  
   �     �     �     �          !     6     J     V     [  	   k     u     z  +   �  
   �     �     �     �     �     �  "            9   /   .             !               	                1         "         :       ,          %   2                  $       )   3          5   4                  -      (   0       '   #                  
   7   ;                    6   +              &      8       *            Add to Desktop Add to Favorites Align search results to bottom App name (Generic name)%1 (%2) Applications Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Documents Forget Application Forget Document General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Open with: Places Power / Session Properties Reboot Recent Applications Recent Documents Recently used Remove from Favorites Run Command... Save Session Search Search results Search... Session Show applications as: Start a parallel session as a different user Suspend Suspend to disk Switch User System Turn off computer Unhide Applications in '%1' Unhide Applications in this Submenu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-01 03:10+0200
PO-Revision-Date: 2014-10-04 10:12+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Legg til på skrivebord Legg til Favoritter Innrett søkeresultater nederst %1 (%2) Programmer Oppførsel Kategorier Datamaskin Kontakter Beskrivelse (Navn) Bare beskrivelse Dokumenter Rediger program … Rediger programmer … Avslutt økt Utvid søk til bokmerker. filer og e-poster Favoritter Flat ut menyen til ett nivå Glem alt Glem alle programmer Glem alle dokumenter Glem program Glem dokument Generelt %1 (%2) Dvale Skjul %1 Skjul program Ikon: Lås Lås skjermen Logg ut Navn (beskrivelse) Bare navn Åpne med: Steder Strøm / Økt Egenskaper Omstart Nylig brukte programmer Nylig brukte dokumenter Nylig brukt Fjern fra Favoritter Kjør kommando … Lagre økta Søk Søkeresultater Søk … Økt Vis program som: Start en parallell økt som en annen bruker Hvilemodus Suspender til disk Bytt bruker System Slå av maskinen Vis programmer i «%1» Vis programmer i denne undermenyen 