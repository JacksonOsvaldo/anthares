��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �      �	  (   �	  /   #
     S
  (   g
  !   �
  !   �
     �
     �
          !     A     X  !   q  %   �  /   �  !   �          "  "   =     `  #   y     �     �     �     �     �     
          "     .     A                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-09-04 22:25+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 La til filer til Bazaar-lageret. Legger til filer til Bazaar-lageret … Klarte ikke legge til filer til Bazaar-lageret. Bazaar logg lukket. Klarte ikke levere inn Bazaar-endringer. Leverte inn Bazaar-endringer … Leverer inn Bazaar-endringer … Klarte ikke hente Bazaar-lager. Hentet Bazaar-lager Henter Bazaar-lager … Klarte ikke skyve Bazaar-lager. Skjøvet Bazaar-lager. Skyver Bazaar-lager … Fjernet filer fra Bazaar-lageret. Fjerner filer fra Bazaar-lageret … Klarte ikke å fjerne filer fra Bazaar-lageret. Klarte ikke gjennomgå endringer. Gjennomgikk endringer. Gjennomgår endringer … Klarte ikke å kjøre Bazaar-logg. Kjører Bazaar-logg … Klarte ikke oppdatere Bazaar-lager. Oppdaterte Bazaar-lager. Oppdaterer Bazaar-lager … Bazaar legg til … Bazaar innlever … Bazaar slett Bazaar logg Bazaar hent Bazaar skyv Bazaar oppdatering Vis lokale Bazaar-endringer 