��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �         %     )     9     L     _     |     �     �  
   �     �  	   �     �     �  :   �          <     Z  	   `  
   j     u     ~     �     �     �     �                                  
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-29 20:43+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 fil %1 filer %1 mappe %1 mapper %1 av %2 behandlet %1 av %2 behandlet ved  %3/s %1 behandlet %1 behandlet ved %2/s Utseende Oppførsel Avbryt Nullstill Sett opp … Avsluttede jobber Liste over filoverføringer/jobber som pågår (kuiserver) Flytt dem til en annen liste Flytt dem til en annen liste. Pause Fjern dem Fjern dem. Fortsett Vis alle jobbene i en liste Vis alle jobber i en liste. Vis alle jobbene i et tre Vis alle jobbene i et tre. Vis adskilte vinduer Vis adskilte vinduer. 