��    
      l      �       �      �                2     :     W  Q   l     �     �  �  �     �     �     �               6     V     ]     q     	                      
                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-11-24 09:54+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.1
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 K&rev utløserord U&tløserord: Sjekk skrivemåten for :q:. iktig Kunne ikke finne en ordbok. Innstillinger for stavekontroll %1 :q: Foreslåtte ord: %1 stav 