��          �      �       H     I     K  	   Z  $   d     �     �  	   �  
   �     �     �  "   �  	     '     �  4     /     1  
   ?  /   J     z     �     �     �     �     �  #   �     �  (   �                   
                    	                          / Authentication Base Dir: Could not create the new request:
%1 Could not upload the patch Destination Password: Repository Request Error: %1 Server: User name in the specified service Username: Where this project was checked out from Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2011-12-08 17:59+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 / Autentisering Basemappe: Klarte ikke opprette den nye forespørselen:
%1 Klarte ikke laste opp lappen Mål Passord: Lager Forespørselfeil: %1 Tjener: Brukernavn i den oppgitte tjenesten Brukernavn: Hvor dette prosjektet ble sjekket ut fra 