��    <      �  S   �      (     )     =     L     Z     n     {     �     �     �     �     �     �     �     �               -     :     J     Z     j     |     �     �     �  -   �     �     	          $     1     >     K     g     ~     �      �     �     �          0     H  #   g     �     �     �  "   �     	  $   ,	     Q	     n	     �	     �	     �	     �	     �	  &   
  !   ?
  !   a
  �  �
     ~     �     �     �     �  	   �     �     �  	   �  	   �     �     �     �                    #  
   )     4     ;     J     W     d  	   l     v  /   �     �     �     �     �     �     �     �     �     �     �     
          <     Q  	   ]     g     w     �     �     �     �     �  &   �          %     3     D     U     f     l  
   t       
   �            7               ;                 0                  #          )   1      $      (               "   6                      !                 4   ,                 9           .       5   &   -   2      *      8   /       %         3   	      '      :             <             +         
    @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDuration @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSoftware used to Generate the document @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label music albumAlbum @label music genreGenre @label music track numberTrack Number @label number of linesLine Count @label number of wordsWord Count Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-16 06:51+0200
PO-Revision-Date: 2014-11-05 13:42+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Album artist Arkiv Artist Bredde/høyde-forhold Lyd Forfatter Bitrate Kanaler Kommentar Komponist Opphavsrett Opprettelsesdato Dokument Varighet Rutefrekvens Høyde Bilde Nøkkelord Språk Tekstforfatter Antall sider Presentasjon Utgiver Slipp-år Samplingsrate Program som er brukt til å generere dokumentet Regneark Emne Tekst Tittel Video Bredde Dato og klokkeslett Bilde Modell Bilderetning Foto blenderåpning Foto eksponeringskompensasjon Foto eksponeringstid Foto F-tall Fotoblits Foto brennvidde Foto brennvidde 35mm GPS-høyde for bildet GPS-bredde for bildet GPS-lengde for bildet Foto ISO-hastighet Foto målemodus Opprinnelig dato og tidspunkt for foto Foto metning Foto skarphet Foto hvitbalanse Foto x-dimensjon Foto y-dimensjon Album Sjanger Spornummer Antall linjer Antall ord 