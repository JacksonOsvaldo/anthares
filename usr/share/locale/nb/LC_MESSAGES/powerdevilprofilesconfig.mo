��    	      d      �       �       �           !  
   -     8     G  �   `  �   (    �  �   �  R   6     �     �     �     �  �   �  �   �     	                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names On AC Power On Battery On Low Battery Restore Default Profiles The KDE Power Management System will now generate a set of defaults based on your computer's capabilities. This will also erase all existing modifications you made. Are you sure you want to continue? The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2011-10-20 11:42+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 knut.yrvin@gmail.com,gluras@c2i.net,boerre@skolelinux.no,bjornst@skogkatt.homelinux.org,fri_programvare@bojer.no,sunny@sunbase.org Knut Yrvin,Gunnhild Lurås,Børre Gaup,Bjørn Steensrud,Axel Bojer,Øyvind A. Holm På nettstrøm På batteri På lavt batteri Gjenopprett standardprofiler KDEs  strømstyringssystem vil nå lage et sett standarder basert på din datamaskins muligheter. Dette vil også slette alle endringer du har gjort. Er du sikker på at du vil fortsette? Det ser ikke ut til at strømstyringstjenesten kjører.
Dette kan løses ved å starte den eller sette den opp i «Oppstart og avslutning» 