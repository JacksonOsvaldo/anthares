��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  &   7  -   ^  :   �  '   �  -   �  *        H     h  '   t         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-21 13:08+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Klarte ikke å finne mimetype for fila Klarte ikke finne alle nødvendige funksjoner Klarte ikke å finne leverandøren med det oppgitte målet Feil ved forsøk på å kjøre skriptet Ugyldig sti for den etterspurte leverandøren Det var ikke mulig å lese den valgte fila Tjenesten var ikke tilgjengelig Ukjent feil Du må oppgi en URL for denne tjenesten 