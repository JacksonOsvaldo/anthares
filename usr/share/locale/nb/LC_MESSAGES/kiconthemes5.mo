��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     �     �  =   �  
     
     
   '     2     :  
   C  	   N  
   X     c     r     y  1   �  	   �     �                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2012-12-30 20:55+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Bla gjennom … &Søk: *.png *.xpm *.svg *.svgz|Ikonfiler (*.png *.xpm *.svg *.svgz) Handlinger Programmer Kategorier Enheter Emblemer Emotikoner Ikonkilde Mime-typer A&ndre ikoner: Steder &Systemikoner: Søk interaktivt etter ikon-navn (f.eks. mapper). Velg ikon Status 