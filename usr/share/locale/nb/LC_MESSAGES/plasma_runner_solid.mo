��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     �     �  *   �  E   �  ;   9  9   u  =   �  ?   �  G   -	     u	     �	     �	     �	     �	     �	     �	     �	     �	                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-15 21:41+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Lås beholderen Løs ut mediet Finner enheter med navn som passer med :q: Lister alle enheter og kan montere, avmontere eller løse ut enheten. Lister alle enheter som kan løses ut, og kan løse dem ut. Lister alle enheter som kan monteres, og kan montere dem. Lister alle enheter som kan avmonteres, og kan avmontere dem. Lister alle krypterte enheter som kan låses, og kan låse dem. Lister alle krypterte enheter som kan låses opp, og kan låse dem opp. Monter enheten enhet løs ut lås monter lås opp avmonter Lås opp beholderen Avmonter enheten 