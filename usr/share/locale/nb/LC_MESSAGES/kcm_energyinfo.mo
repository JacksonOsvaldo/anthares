��    5      �  G   l      �     �     �     �     �     �     �     �     �     �                     ,      8     Y     `     s     �     �     �     �     �     �     �     �            	     	   %     /     <     B     a     d     q     y     �     �     �     �     �     �  @   �               4     ;     B     J     g     u     y  �  �     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     '
     .
     <
     W
     ^
     m
     z
     �
     �
     �
     �
     �
     �
  	   �
  
   �
  	   �
                 
   "     -     5  	   =  	   G     Q     ]     _  
   f  A   q     �     �     �     �     �     �          	                 #          /      	   '                    1   &   3   *                                         $   +   
   ,                          !                    (   2   0       -      5              %      "          4   )            .               % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) Watt-hoursWh Yes literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-30 21:53+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % %1 %2 %1: Energiforbruk for program Batteri Kapasitet Prosent ladet Ladingstilstand Lader Strøm °C Detaljer: %1 Lader ut bjornst@skogkatt.homelinux.org Energi Energiforbruk Statistikk over energibruk Miljø Full utforming Ferdig ladet Har strømforsyning Kai Uwe Broulik Siste 12 timer Siste 2 timer Siste 24 timer Siste 48 timer Siste 7 dager Full last Siste time Produsent Modell Bjørn Steensrud Nei Lader ikke PID: %1 Sti: %1 Oppladbar Frisk opp Serienummer W System Temperatur Denne sorten historie er nå ikke tilgjengelig for denne enheten. Tidsrom Vis data for tidsrom Leverandør V Spenning Vekkinger per sekund: %1 (%2%) Wh Ja % 