��          <      \       p   m   q      �   R   �   �  D  ]   ?     �  D   �                   Calculates the value of :q: when :q: is made up of numbers and mathematical symbols such as +, -, /, * and ^. Copy to Clipboard The exchange rates could not be updated. The following error has been reported: %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-28 05:09+0200
PO-Revision-Date: 2009-08-26 09:22+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Beregner verdien av :q: når :q: består av tall og matematikksymboler slik som +,-,/,* og ^. Kopier til utklippstavla Klarte ikke å oppdatere valutakursene. Følgende feil ble meldt: %1 