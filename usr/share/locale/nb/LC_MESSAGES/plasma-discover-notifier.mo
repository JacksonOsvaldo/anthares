��          T      �       �   )   �      �      �           &     >  �  P  1   K      }  %   �     �     �     �                                        1 package to update %1 packages to update No packages to update Security updates available System up to date System update available Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-06-13 07:40+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 1 pakke skal oppdateres %1 pakker skal oppdateres Ingen pakker trenger oppdatering Sikkerhetsoppdateringer tilgjengelige Systemet er oppdatert Systemoppdatering tilgjengelig Oppdateringer tilgjengelige 