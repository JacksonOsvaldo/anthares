��          �      |      �     �  �     m   �  �      )   �  `   �          ,     D     Q     ]      t     �  '   �     �     �     �       ?     Y   P  +   �    �     �  �   �  b   �     �     	  Z   %	     �	     �	  	   �	     �	  *   �	     �	     
  #   
  
   9
     D
     I
  
   c
  A   n
  I   �
  '   �
                                	   
                                                              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-04-30 23:00+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2003–2007 Fredrik Höglund <qt> Er du sikker på at du vil fjerne pekertemaet <strong>%1</strong>? <br /> Dette vil slette alle filer som dette temaet har installert.</qt> <qt>Du kan ikke slette det temaet som du bruker. <br />Du må bytte til et annet tema først.</qt> (Tilgjengelige størrelser: %1) Avhengig av oppløsning Det finnes allerede et tema som heter %1 i ikondrakt-mappa. Vil du erstatte det med dette? Bekreftelse Pekerinnstillingene er endret Pekertema Beskrivelse Dra eller skriv inn nettadresse til temaet fri_programvare@bojer.no Fredrik Höglund Hent nye fargeoppsett fra Internett Axel Bojer Navn Vil du overskrive temaet? Fjern tema Fila %1 ser ikke ut til å være et gyldig arkiv for pekertemaer. Klarte ikke å laste ned pekertema-arkivet, sjekk at adressen %1 stemmer. Klarte ikke finne pekertema-arkivet %1. 