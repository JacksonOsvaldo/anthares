��    *      l  ;   �      �     �     �     �  
   D  +   O  
   {     �     �     �      �     �     �       	           �   @  e   �  *   D  H   o     �     �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~     �  #   �     �     �  	  �          
  u     
   �  1   �     �     �     �       !        =  "   N  
   q  
   |     �  �   �  }   A  $   �  U   �     :     M     U     d     u  -   �  B   �  	   �  (      (   )     R     j     �     �     �     �     �            '   1     Y  !   p        
          *   &      #      '         $                               (            %                       	       !                            )              "                                          msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-08-09 08:38+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms A&ntall skrivebord: <h1>Flere skrivebord</h1>I denne modulen kan du velge hvor mange virtuelle skrivebord du vil ha, og hva de skal hete. Animasjon: Tilordnet global snarvei «%1» til skrivebord %2 Skrivebord %1 Skrivebord %1: Animerte skrivebordseffekter Skrivebordsnavn Skjermvisning av skrivebordsbytte Skrivebordsbytte Skrivebordsnavigasjon ruller rundt Skrivebord Varighet:  bjornst@skogkatt.homelinux.org Hvis dette valget er på, vil tastatur- eller musenavigasjon bortenfor kanten av et skrivebord bringe fram skrivebordet på den andre siden av den kanten. Hvis dette er slått på blir det vist en liten forhåndsvisning av skrivebordsutformingen som viser det valgte skrivebordet. Her kan du gi navn til skrivebord %1 Her kan du velge hvor mange virtuelle skrivebord du vil ha på KDE-skrivebordet ditt. KWin utviklingslag Oppsett A&ntall rader: Bjørn Steensrud Ingen animasjon Fant ingen passende snarvei til skrivebord %1 Snarveiskonflikt: Kunne ikke tilordne snarvei %1 til skrivebord %2 Snarveier Vis indikatorer for skrivebordsutforming Vis snarveier for alle mulige skrivebord Bytt ett skrivebord ned Bytt ett skrivebord opp Bytt ett skrivebord mot venstre Bytt ett skrivebord mot høyre Bytt til skrivebord %1 Bytt til neste skrivebord Bytt til forrige skrivebord Bytte Bla gjennom skrivebordsliste Bla gjennom skrivebordsliste (baklengs) Bla gjennom skrivebord Bla gjennom skrivebord (baklengs) 