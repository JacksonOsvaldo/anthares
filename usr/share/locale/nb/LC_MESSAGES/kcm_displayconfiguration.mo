��    1      �  C   ,      8     9     ?     T     a          �  !   �  '   �     �     �  -        =     B     X     _     h     �     �     �      �     �  	   �     �     �       
             :     L  A   e     �     �     �     �     �     �     �  3     �   5     �     �  	   �  ;   �     -     =     K  %   W  b   }  �  �     �
     �
     �
          &     2     >     T     j     w  1   �     �     �     �  	   �     �               "     *  
   I  
   T     _     t     �     �     �     �  '   �  I   �     :     A     N  
   [     f     x     �  1   �  �   �     ]     d     k  A   x     �     �     �  '   �  h   
           	       .      1               (   )       ,   
   -                                      0      $          "       !                        +   '         &                                       #             %         *   /       %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scaling changes will come into effect after restart Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 TextLabel Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-18 03:25+0200
PO-Revision-Date: 2015-08-27 16:59+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 Hz &Slå av alle utganger &Sett opp på nytt © 2012–2013 Dan Vrátil 90° medurs 90° moturs Slå av alle utganger Ikke støttet oppsett Aktiv profil Avanserte innstillinger Er du sikker på at du vil slå av alle utganger? Auto Bryt opp forente utganger Knapp Komboboks Oppsett for skjermer Daniel Vrátil Skjermoppsett Skjerm: bjornst@skogkatt.homelinux.org Slått på Gruppeboks Identifiser utganger KCM Testprogram Skjerm på bærbar Vedlikeholder Bjørn Steensrud Ingen hovedutgang Ingen tilgjengelige skjermoppløsninger Fant ingen bakgrunnsmotor for kscreen. Kontroller kscreen-installasjonen. Normal Orientering: Hovedskjerm: Radioknapp Oppfriskingsrate: Oppløsning: Skaler visning Endringer i skalering får virkning etter omstart Oppsettet ditt kunne ikke tas i bruk.

Vanlige feil er at skjermstørrelsen er satt for stor, eller du slo på flere skjermer enn det GPU-en støtter. Fane 1 Fane 2 Tekstetikett Tip: Hold Ctrl mens du drar et skjermbilde for å slå av festing Forente utganger Foren utganger Opp ned Advarsel: Det er ingen aktive utganger. Systemet ditt støtter bare høyst %1 aktiv skjerm Systemet ditt støtter bare høyst %1 aktive skjermer 