��    :      �  O   �      �  
   �  
     
             )     -     A     P     V     e  	   m     w     ~  
   �     �     �     �     �     �     �     �     �     �  
   �     �     
               #     ,     9     H     M     [     l     z     }     �     �     �     �  	   �     �     �     �  b   �  �   G     �  	   �     �  
     	             )     1     7     I  �  Z     U
     a
     s
     �
     �
     �
     �
     �
     �
  
   �
                  
      	   +     5     :     Q     V     \     d     w     ~     �     �  
   �     �     �     �     �     �     �     �               -     0     @     N     U  
   b  	   m     w     }     �  `   �  �   �     �     �     �  	   �     �     �  	   �     �     �     �     $         7   
            0   6                8      :   )                -   !      4      	   '   2               "      *                9           .          1   /      &      (      +               5       #                              ,                       %          3    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Author: Auto Hide Bottom Cancel Categories Center Close Create activity... Ctrl Delete Email: Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Uninstall Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-01 03:19+0200
PO-Revision-Date: 2015-06-13 07:34+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Aktiviteter Legg til handling Legg til avstandsholder Legg til skjermelementer … Alt Alternative skjermelementer Alltid synlig Bruk Ta i bruk innstillingene Forfatter: Skjul automatisk Nederst Avbryt Kategorier Midtstilt Lukk Opprett aktivitet … Ctrl Slett E-post: Hent nye elementer Høyde Vannrett rulling Inndata her Tastatursnarveier Utforming: Venstre Venstre knapp Lisens: Lås elementer Maksimer panel Meta Midtknappen Flere innstillinger … Musehandlinger OK Panelinnretting Fjern panelet Høyre Høyre knapp Skjermkant Søk … Shift Stoppede aktiviteter: Bytt Innstillingene i denne modulen er blitt endret. Vil du ta endringene i bruk, eller forkaste dem? Denne snarveien vil starte miniprogrammet, gi det fokus for tastaturet, og hvis miniprogrammet har et oppsprett (f.eks. en startmeny), så blir det åpnet. Øverst Avinstaller Loddrett rulling Synlighet Tapet Tapet-type: Elementer Bredde Vinduer kan dekke over Vinduer går under 