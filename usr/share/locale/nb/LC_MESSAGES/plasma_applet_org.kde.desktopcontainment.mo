��    K      t  e   �      `     a     i     z     �     �     �     �     �     �     �     �     �  
   �  
   �     �                                -     2  
   :     E     R     j     s     �     �     �     �     �  	   �     �     �  	   �     �     �          	                '     ?     D     I     L     \     o     v     |     �     �  
   �     �     �     �     �     �     �     	     	     	     !	     )	     1	     :	     L	     X	     ]	     c	     l	     q	     �	    �	     �     �     �     �     �     �     �     	          .     <  	   E     O     X     `     i     q     y     �     �     �     �     �     �     �  
   �     �  	   �       $        2     @     N     W     r     x     �     �     �     �     �     �  "   �     �     �     �     �          $     5     <     B     H  	   `  
   j     u     �     �     �     �  
   �     �     �     �  	   �  	               
   +     6     >     K     P     k     I   .       2   ,                                   1   3   H   "   *      A   6         :      #   ;   +      7         8   <      E   -   9   4   &             '           J   =       C         G           )               %         K          @       (   
           0             >      /      5   B       !   F      	   D                  $                    ?        &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Align Align: Appearance Arrange In Arrange in: Arrangement Back Cancel Columns Custom title Date Default Descending Deselect All Enter custom title here Features File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Hide Files Matching Huge Icon Size Icons Large Left Location Lock in place Locked More Preview Options... Name None OK Preview Plugins Preview thumbnails Resize Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show the Desktop folder Size Size: Small Sort By Sorting Sorting: Specify a folder: Text lines: Tiny Title Tooltips Type Type a path or a URL here Unsorted Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-31 03:07+0200
PO-Revision-Date: 2014-09-04 11:49+0200
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Slett &Tøm papirkurven Flytt til &papirkurven Å&pne &Lim inn &Egenskaper F&risk opp Skrivebord &Oppdater visning Last &inn på nytt Gi &nytt navn Rett inn Rett inn: Utseende Ordne i Ordne i: Ordning Tilbake Avbryt Kolonner Egendefinert tittel Dato Standard Synkende Fravelg alle Oppgi en selvvalgt tittel Egenskaper Mønster for filnavn: Filtyper: Filter Oppsprett for mappe-forhåndsvisning Mapper først Mapper først Full sti Skjul filer som passer med Enorm Ikonstørrelse Ikoner Stor Venstre Sted Lås på plass: Låst Flere  valg for forhåndsvisninger Navn Ingen OK Tillegg for forhåndsvisning Minibilder for forhåndsvisning Endre størrelse Høyre Roter Rader Søk etter filtype … Velg alle Velg mappe Utvalgsmarkører Vis alle filer Vis filer som passer med Vis et Sted: Vis skrivebordsmappa Størrelse Størrelse: Liten Sorter etter Sortering Sortering Oppgi en mappe: Tekstlinjer: Bitteliten Tittel: Verktøytips Type Oppgi en sti eller URL her Usortert 