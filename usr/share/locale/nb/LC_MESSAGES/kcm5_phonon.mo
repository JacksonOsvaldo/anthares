��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �    �  N   �  v   1     �  T   �          .  3   <  	   p  /   z  	   �     �  
   �     �  ,   �  (     +   A     m     �  Y   �     �     �  s        �  0   �     �     �     �             
   <     G     Z     j     r     �     �     �     �     �  	   �     �     �     �               (     >     T     i     q  $   z  	   �     �     �  	   �  �   �     r  8        �     8  2   D  4   w  7   �  (   �          $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-03-10 17:00+0100
Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 For å ta i bruk endring av bakgrunnsmotor må du logge ut og logge inn igjen. En liste over bakstykker for Phonon som finnes på systemet. Rekkefølgen her viser rekkefølgen Phonon vil bruke dem. Bruk enhetslista på … Bruk den preferanselista som vises nå på følgende andre lydavspillingskategorier: Oppsett for lydenheter Lydavspilling Foretrukket lydavspillingsenhet for kategori «%1» Lydopptak Foretrukket lydopptaksenhet for kategori «%1» Bakstykke Colin Guthrie Tilkobling Copyright 2006 Matthias Kretz Innstilling for standard lydavspillingsenhet Innstilling for standard lydopptaksenhet Innstilling for standard video-opptaksenhet Standard/uspesifisert kategori Utsett Definerer standard rekkefølge for enheter som kan overstyres av individuelle kategorier. Enhetsoppsett Enhetsinnstilling Enheter som passer den valgte kategorien funnet på systemet ditt. Velg enhet som du vil at programmene skal bruke. bjornst@skogkatt.homelinux.org Klarte ikke å angi den valgte lydutgangsenheten Foran i midten Foran til venstre Foran til venstre for midten Foran til høyre Foran til høyre for midten Maskinvare Uavhengige enheter Inngangsnivåer Ugyldig KDE-oppsett for lyd-maskinvare Matthias Kretz Mono Bjørn Steensrud Oppsettsmodul for Phonon Avspilling (%1) Foretrekk Profil Bak i midten Bak til venstre Bak til høyre Opptak (%1) Vis avanserte enheter På siden til venstre På siden til høyre Lydkort Lydenhet Plassering og testing av høyttalere Subwoofer Test Test den valgte enheten Tester %1 Rekkefølgen bestemmer innstillinga for enhetene. Hvis den første enheten av en eller annen grunn ikke kan brukes, så forsøker Phonon å bruke den neste, osv. Ukjent kanal Bruk den enhetslista som vises nå for flere kategorier. Forskjellige kategorier media-bruk. For hver kategori kan det velges hvilken enhet du foretrekker at Phonon-programmene bruker. Videoopptak Foretrukket videoopptaksenhet for kategori «%1»  Bakgrunnsmotoren din støtter kanskje ikke lydopptak Bakgrunnsmotoren din støtter kanskje ikke video-opptak ingen innstilling for den valgte enheten foretrekk den valgte enheten 