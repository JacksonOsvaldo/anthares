��          �      �           	  �     �   �  �   |                    *     2     8     <     D     Z     _  	   h     r     x  
   �     �     �  	   �     �  �  �     f  �   x  �   C  �        �     �  	   �     �     �     �  
   �     	     	      	     ,	     :	  #   B	  
   f	  	   q	  	   {	     �	     �	                                                        
                                        	        <br />Version: %1 <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> Author: BSD Description: Details Error GPL Install Key used for signing: LGPL License: Password: Reset Select Signing Key Server: %1 Title: Update Username: Version: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2008-05-30 17:29+0200
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
 <br />Versiya: %1 <qt><i>Gpg</i> yordamida faylning haqiqiyligini tekshirib boʻlmadi. <i>Gpg</i> dasturi oʻrnatilganligiga ishonch hosil qiling aks holda, yozib olingan faylning haqiqiyligini tekshirib boʻlmaydi.</qt> <qt><i>Gpg</i> yordamida mavjud kalitlarning roʻyxatini aniqlab boʻlmadi. <i>Gpg</i> dasturi oʻrnatilganligiga ishonch hosil qiling aks holda, yozib olingan faylning haqiqiyligini tekshirib boʻlmaydi.</qt> <qt>Faylga imzo qoʻyish uchun <i>gpg</i> dasturini ishga tushirib boʻlmadi. <i>Gpg</i> dasturi oʻrnatilganligiga ishonch hosil qiling aks holda, imzolashni iloji boʻlmaydi.</qt> Muallif: BSD Taʼrifi: Tafsilotlar Xato GPL Oʻrnatish Imzolash uchun kalitlar: LGPL Litsenziya: Maxfiy soʻz: Tiklash Imzo qoʻyish uchun kalitni tanlang Server: %1 Sarlavha: Yangilash Foydalanuvchi: Versiya: 