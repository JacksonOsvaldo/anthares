��          �      |      �  &   �  �        �     �     �     �     �     �      �  c        h     y     �     �     �     �     �  	   �  C   �  j   !     �  �  �  G   Q  �   �     '     -     >     P     \     i     v  S   �     �     �               -     =     C  
   I  >   T  S   �     �                                                       
                 	                         (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2006-03-05 12:04+0100
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: nplurals=1; plural=0;
 (c) 2002 Karol Ssved (Karol Szwed), Daniel Molkentin (Daniel Molkentin) <h1>Uslub</h1>Ushbu modul yordamida vidjet uslubi va effektlarga oʻxshagan foydalanuvchi interfeysining tashqi koʻrinishini moslash mumkin. Tugma Belgilash katagi Tanlash roʻyxati &Moslash... %1 moslamasi Taʼrifi: %1 kmashrab@uni-bremen.de Agar belgilansa, KDE dasturlari baʼzi muhim tugmalarda nishonchalarni koʻrsatadi. KDE uslub moduli Mashrab Quvatov Hech qanday taʼrif yoʻq. Koʻrib chiqish Tanlash tugmasi Tab 1 Tab 2 Faqat matn Ushbu uslub uchun moslash oynasini yuklashda xato roʻy berdi. Bu yerda tanlangan ulubni qoʻllamasdan uning tashqi koʻrinishini koʻrish mumkin. Oynani yuklab boʻlmadi 