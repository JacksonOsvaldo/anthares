��    (      \  5   �      p     q     z     �     �     �     �  -   �  r   �  	   g  	   q     {     �     �     �     �      �     �     �     �     �          5  	   :     D     J     R     _     m     �     �     �     �  +   �     �            S     )   k     �  �  �  	   P     Z     c     s     �  (   �  @   �  e   �     Y	     f	     y	     �	  
   �	     �	  "   �	  (   �	     �	     �	     
  #   
  $   5
     Z
  	   _
     i
     o
     
     �
  !   �
  "   �
  &   �
  
          *   1     \     d     m  f   }  -   �                                 "         #             %              &                    
                   '                          $   !   (         	                                &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Installing <strong>%1</strong> theme</qt> A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Co&lor: Colorize Confirmation Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Icons Icons Control Panel Module NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. To Gray To Monochrome Toolbar Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2006-12-16 15:28+0000
Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>
Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
 &Miqdori: &Effekt: &Ikkinchi rang: Yarim sha&ffof Ma&vzu (c) 2000-2003 Gert Yansen (Geert Jansen) <qt><strong>%1</strong> nishonchalar mavzusi oʻrnatilmoqda</qt> Oʻrnatish davomida xato roʻy berdi. Shunday boʻlsa ham, arxivdagi mavzulardan koʻpisi oʻrnatildi Qoʻshimch&a Hamma nishonchalar &Rangi: Boʻyash Tasdiqlash Taʼrifi Mavzuning manzilini (URL) kiriting kmashrab@uni-bremen.de, mavnur@gmail.com Effekt parametrlari Gamma Nishonchalar Nishonchalar uchun boshqaruv moduli Mashrab Quvatov, Nurali Abdurahmonov Nomi Effektsiz Panel Koʻrib chiqish Mavzuni olib tashlash Effektni moslash Aktiv nishoncha effektini moslash Andoza nishoncha effektini moslash Aktiv emas nishoncha effektini moslash Oʻlchami: Kichik nishonchalar Fayl nishonchalar mavzusining arxivi emas. Oq-qora Monoxrom Asboblar paneli Nishonchalar mavzusining arxivini yozib olib boʻlmadi.
Iltimos %1 manzili toʻgʻriligini tekshiring. Nishonchalar mavzusining %1 arxivi topilmadi. Nishonchadan foydalanish 