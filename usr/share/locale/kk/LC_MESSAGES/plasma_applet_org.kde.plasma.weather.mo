��          �      |      �     �  '     "   0     S     m     �  ?   �     �  &   �        %   ?     e  >        �     �  '   �  !        >     ^     }  3   �  �  �     i     l     |     �     �     �     �     �     �  (   	     2  &   L  "   s     �     �     �  &   �  $   �                <                                                                       	   
                       Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2010-04-17 07:29+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.0
 ° %1 тәулік Мин.: %2  Макс.: %1 Жоғарғысы: %1 Төмені: %1 - Баптау керек-ау Жел жоқ Ылғалдылығы: %1%2 Көрініс қашықтығы: %1 %2 Шық нүктесі: %1 Салыс. ылғалдылығы: %1 Ауа қысым үрдісі: %1 Ауа қысымы: %1 %2 %1%2 Көрінісі: %1 Жасалған ескертулер: Бақылау әрекеттері: %1 %2 %3 Күн суықтығы: %1 Жел ұйтқуы: %1 %2  