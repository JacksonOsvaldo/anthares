��    /      �  C           ;        U     s     �     �  6   �  A   �     ,     5  $   9  
   ^     i  #   �     �     �     �  7   �          (     D     Q  $   `  ,   �     �     �     �     �     �               +     @     R  �   _  "        >     E  %   W     }     �     �     �  
   �  7   �     	     1	  �  I	  g   �
  ;   K  ,   �  !   �  8   �       2   $     W     f  :   u     �  2   �  S   �  @   F     �     �  K   �  E   �  7   =     u  $   �  6   �  D   �     "     A  
   `  @   k     �     �  #   �  -   �  "   )     L  +  d  (   �     �  0   �  F   �     ?  5   N  /   �     �  
   �  9   �                &   "      (                                  !             )   	          +           .      #       '   -                             /         %                            *   ,   
                  $                                  

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Check for new comic strips: Comic Comic cache: Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. No zip file is existing, aborting. Range: Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2011-06-30 05:13+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 

Кештегі соңғы комикске ауысу үшін алдыңғысына барыңыз. Комикс кітабінің архивін құ&ру... Комиксті былай &сақтау... &Тізбектің нөмірі: *.cbz|Комикс кітабінің архиві (Zip) &Шын өлшемі Көріп жатқан орының &жаттау Қосымша Бүкілін %1 идентификаторымен қате болды. Көрінсі Комиксті архивтеу жаңылысы Комикс плагиндерін автоматты түрде жаңартуы: Жаңа комикс тізбегі бар ма тексеру: Комикс Комикс кэші: Келтірілген орында архивті құру болмады. %1 деген комикс кітабінің архивін құру Комикс кітабінің архивін құру Қайда: Қатеге тап болғанда Файлды архивке қосу жаңылысы. Идентификаторы %1 файлын құру қатесі.. Басынан бастап... Соңынан бастап... Жалпы Комикс тізбегін қабылдау жаңылысы: Тізбегіне өту Мәліметі Қ&арап жатқанға өту &Бірінші тізбегіне ауысу Ауысатын тізбегі... Өзгеше ауқым Интернет қосылымы болмауы мүмкін.
Комикс плагині істемеуі мүмкін.
Тіпті, мынау күн/нөмір/атауда комикс мүлдем жоқ та шығар, басқасын таңдасаңыз мүмкін істеп кетер. Zip файл жоқ, доғарылды. Ауқымы: Тізбектің идентификаторы: Архивтейтін комикс тізбегінің ауқымы. Жаңарту Комикстің веб-сайтына жолығу Шеберханасының в&еб-сайты № %1 dd.mm.yyyy Жаңа тізбег бар &келесі қойынды Мынадан: Мынаған дейін: 