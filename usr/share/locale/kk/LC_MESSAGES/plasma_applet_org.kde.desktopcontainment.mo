��    !      $  /   ,      �     �     �                         *     ;     I     Q     Y  
   _     j     w          �     �     �     �     �     �     �     �  
                   .     <     T     Z     b     t  �  �     A  3   M  *   �     �     �     �     �      �           9     N     ]     l     �  0   �  =   �          "  ;   ;     w  
   �  0   �     �     �  .   �  =   	     Z	  *   r	  
   �	     �	     �	  8   �	                                                                                        
                                        	                           !    &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Align Arrange In Custom title Default Deselect All Enter custom title here File name pattern: File types: Hide Files Matching Icons Large More Preview Options... None Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Sort By Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2013-05-28 03:59+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;

X-Generator: Lokalize 1.2
 Ө&шіру 'Өшіргендер' шелегін &босату Өшірілгендерге &тастау &Ашу &Орналастыру Қасиетте&р Үстелді &жаңарту Көрінсін &жаңартү Қ&айта жүктеу Қ&айта атау Туралау Тәртібі Қалаған атауы Әдетті Барлығын таңдаудан шығару Қалаған атауды осында келтіріңіз Файл атау өрнегі: Файл түрлері: Сәйкес келетін файлдарды жасыру Таңбашалар Үлкен Қосымша көру параметрлері Жоқ Барлығын таңдау Барлық файлдарды көрсету Сәйкес келетін файлдарды көрсету Көрсететіні: Үстел қапшығын көрсету Шағын Реті Қапшығы: Мұнда жолын не URL-ін келтіріңіз 