��          �   %   �      0  U   1     �     �  
   �     �     �     �     �     �     �  	   �                     .  )   4  (   ^  '   �  "   �     �     �  9   �  J   #  �  n  1        :  %   V     |  .   �     �     �     �  
     *        8  '   N     v  #   �     �  h   �  \   "  \     R   �  #   /	     S	     c	  A   |	                                                                            
                           	                        Activities a window is currently on (apart from the current one)Also available on %1 Alphabetically By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum rows: On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2013-09-30 01:00+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 %1 дегенде белсенділігі бар Әліппе бойынша Белсенділік бойынша Үстелі бойынша Бағдарлама атауы бойынша Топтастырмау Реттемеу Сүзгілер Жалпы Топтастыру және реттеу Таптастыру: Терезелерді белгілеу Қолмен Жолдар санның шегі: %1-үстелде Назардағы белсенділіктің тапсырмалары ғана көрсетілсін Назардағы үстелдегі тапсырмалар ғана көрсетілсін Назардағы экрандагы тапсырмалар ғана көрсетілсін Түйіп қойылған ғана тапсырмалар көрсетілсін Ішараларын көрсету Реттеуі: %1 дегенде бар Бүкіл белсенділіктерде қол жетімді 