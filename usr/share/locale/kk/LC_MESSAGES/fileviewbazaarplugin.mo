��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  8   x	  8   �	  G   �	  %   2
  :   X
  6   �
  6   �
  4     )   6  %   `  4   �  +   �  %   �  <     <   J  K   �  1   �  3     "   9  8   \  )   �  8   �  +   �  )   $     N     f     �     �     �     �     �  2   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-12-22 04:35+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 Bazaar қоймасына файлдар қосылды. Bazaar қоймасына файлдарды қосу... Bazaar қоймасына файлдарды қосу жаңылысы. Bazaar журналы жабылды. Bazaar қоймасына тапсыру жаңылысы. Өзгерістер Bazaar-ға тапсырылды. Bazaar-ге өзгерістерді тапсыру... Bazaar қоймасынан алу жаңылысы. Bazaar қоймасынан алынды. Bazaar қоймасынан алу... Bazaar қоймасына салу жаңылысы. Bazaar қоймасынан салынды. Bazaar қоймасына салу... Bazaar қоймасынан файлдар өшірілді. Bazaar қоймасынан файлдарды өшіру... Bazaar қоймасынан файлдарды өшіру жаңылысы. Өзгерістері шолу жаңылысы. Өзгерістер қарап шығарылды. Өзгерістері шолу... Bazaar журналын жүргізу жаңылысы. Bazaar журналын жүргізу... Bazaar қоймасын жаңарту жаңылысы. Bazaar қоймасы жаңартылды. Bazaar қоймасын жаңарту... Bazaar-ға қосу... Bazaar-ға тапсыру... Bazaar-дан өшіру Bazaar журналы Bazaar-дан алу Bazaar-ға салу Bazaar-ды жаңарту Жергілікті Bazaar өзгерістері 