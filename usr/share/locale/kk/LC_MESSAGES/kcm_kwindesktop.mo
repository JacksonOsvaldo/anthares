��    '      T  5   �      `     a     g  
   �  +   �  
        )     5     N      \     }     �     �  	   �      �  �   �  e   �  *   �  H        [     b     �  )   �  ;   �  	   �     �  (        F     ^     t     �     �     �     �  	   �       #        B     X  �  x  	   "
  �   ,
     �
  ]   �
     [     i  ,   x     �  ;   �  $   �  ,   "     O     `     r  �   �  �   u  M     �   g     �             E   5  i   {     �  K   �  f   H  /   �  -   �  '     %   5     [  &   z  (   �  
   �  A   �  L     !   d  .   �           	                 "                     %   
                                          #            &               !                             '         $                   msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2013-09-15 06:47+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;

  мсек <h1>Бірнеше Үстел</h1>Қанша виртуалды үстел керек және олар қалай белгіленетінің осы модулінде анықталады. Анимация Жалпы жүйелік "%1" тіркесімі  %2-үстелге тағайындалды %1-үстел %1-үстел: Үстел эффект анимациясы Үстел атаулары Экрандағы үстелара ауысу панелі Басқа үстелге ауысу Тұйық үстел навигациясы Үстелдер Ұзақтығы: sairan@computer.org Бұл параметрді таңдағанда, пернетақта не незардағы үстелдің шегінің сыртындағы навигациясы жаңа үстелдің қарсы шетіне шығарады. Бұл белгіні қойсаңыз, таңдалған үстелдің көрінісін нобайлайтын шағын сурет көрсетіледі. Мұнда %1-үстел қалай аталатынын келтіріңіз Мұнда KDE үстеліңізде қанша виртуалды үстел керек ететіңізді анықтай аласыз. Құрамы Сайран Киккарин Анимациясыз %1-үстелге лайық тіркесім табылған жоқ Тіркесім қайшылығы: %1 тіркесімі %2-үстелге тағайындалмады Тіркесімдер Үстелдегінін орналасу индикаторы болсын Бар және болуға мүмкін үстелдерінің перне тіркесімдері Астыңғы жақ үстелге ауысу Үстңгі жақ үстелге ауысу Сол жақ үстелге ауысу Оң жақ үстелге ауысу %1-үстеліне ауысу Келесі үстелге ауысу Алдыңғы үстелге ауысу Ауысу Үстелдерінің тізімі бойынша аралау Үстелдерінің тізімі бойынша (кері) аралау Үстелдерді аралау Үстелідерді (кері) аралау 