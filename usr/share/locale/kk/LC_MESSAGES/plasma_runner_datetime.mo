��          \      �       �      �   -   �        -   +  #   Y  #   }     �  �  �  ,   N  X   {  0   �  \        b     g     l                                       Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-05-31 05:49+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 Бүгінгі күнді көрсетеді Келтірілген белдеудегі бүгінгі күнін көрсетеді Қазіргі уақытты көрсетеді Келтірілген белдеудегі қазіргі уақытын көрсетеді date time Бүгін %1 