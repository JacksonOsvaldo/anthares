��          �      \      �  7   �  	   	               (     ;     S     a     i     }     �  #   �  %   �     �  5   �     .     =     K  )   S  �  }     2     9     G  (   g  '   �  6   �     �  
     '        8  !   F     h     l  @   p  S   �  %     %   +     Q     i                                                  	                       
                          Battery is currently not present in the bayNot present Capacity: Charging Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2013-06-09 05:41+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;






 Жоқ Көлемі: Толтырылып жатыр Толуын жоғалтып жатыр Дисплейдің жарықтығы Қуаттандыруды басқаруын қосу Әбден толды Жалпы Пернетақта жарықтығы Моделі: Толтырылмай жатыр %1% %1% Қуаттандыруды басқару бұғатталған Батарея апплеті жалпы жүйелік тежеуді қосқан Бітуге қалған уақыт: Толуға қалған уақыт: Жабдықтаушы: %1% 