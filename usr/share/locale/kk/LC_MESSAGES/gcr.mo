��    S      �  q   L                    %   A  	   g     q     ~     �     �     �     �     �     �     �     �     �     �     �  *   �          "     0     8  	   K     U     t  
   {  
   �  
   �     �     �     �     �     �     �     �     �     �     �  
   �  	   	     	     	      	  	   9	     C	     [	     _	     f	     k	     r	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     
     
     
  	   
     
  
   $
     /
     5
     =
     J
  	   N
     X
     `
     g
     p
     t
     }
     �
     �
     �
     �
     �
     �
  �  �
  	   �  J   �  <   .     k     z     �     �  +   �  +   �          +     9     N     S     d     y     }  L   �     �     �     �  -        <  E   K     �     �     �     �     �     �       '   !     I     b     k  %   t     �     �     �     �     �     �  4   �     +  <   9     v     z     �     �     �  0   �     �     �                 +   -     Y     o     {     �     �     �     �     �     �     �     �     �     �       	        (     8     >     R     `     s  %   �     �  
   �     �         (       3       N       I           /   .       ?             A   Q                 '          0         B   8   $      )   4   !      E   9       %              ,   K      &   	         G   
                      @       5   L   M                    7              F   P       ;   *   =   J   #   6             C   H   +       R   D   2   :   >                          -   S   <                 1   "       O       %u bit %u bits - View certificate and key files A file already exists with this name. Attribute Authenticate Cancel Certificate Certificate Viewer Certificate files Comment Confirm: Continue Country Created Critical DNS DSA Do you want to replace it with a new file? Email Email Address Encrypt Export certificate Extension GCR Certificate and Key Viewer Gender Given Name IP Address Identifier Identity Import Import failed Import settings Imported Invalid Key Key ID Label: Organization Other Name PEM files PGP Key Password Password cannot be blank Password: Passwords do not match. RSA Reason SHA1 SHA256 Serial Number Show the application's version Sign Signature Size Street Strength The operation was cancelled. The password was incorrect Token: Type URI Unknown Unlimited Unlock Unlock: %s Value Version X400 Address Yes [file...] _Cancel _Close _Details _OK _Replace _Save capabilityDisabled columnIssued By columnKey ID columnName minutes ownertrustDisabled Project-Id-Version: gcr master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-keyring&keywords=I18N+L10N&component=gcr
POT-Creation-Date: 2015-09-16 05:07+0000
PO-Revision-Date: 2015-09-16 15:22+0500
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
X-Generator: Poedit 1.8.4
 %u бит - Сертификаттар және кілт файларын қарау Олай аталатын файл бар болып тұр. Атрибут Аутентификация Бас тарту Сертификат Сертификаттар шолушысы Сертификаттар файлдары Түсіндірме Растау: Жалғастыру Ел Жасалған Критикалық DNS DSA Оны жаңа файлмен алмастыруды қалайсыз ба? Эл. пошта Электронды пошта Шифрлеу Сертификатты экспорттау Кеңейту GCR сертификаттар және кілдер шолушысы Жынысы Әкесінің аты IP адресі Анықтағыш Идентификация Импорт Импорттау сәтсіз Импорттау баптаулары Импортталған Қате Кілт Кілт идентификаторы Белгісі: Ұйым Басқа аты PEM файлдары PGP кілті Пароль Пароль бос болуы мүмкін емес Пароль: Парольдер өзара сәйкес келмейді. RSA Себебі SHA1 SHA256 Сериялық нөмірі Қолданба нұсқасын көрсету Қолтаңба Қолтаңба Өлшемі Көше Мықтылығы Әрекеттен бас тартылды. Пароль қате Токен: Түрі URI Белгісіз Шектелмеген Босату Босату: %s Мәні Нұсқасы X400 адресі Иә [файл...] Ба_с тарту _Жабу Көбір_ек О_К А_лмастыру _Сақтау Сөндірулі Кім берген Кілт идентификаторы Аты минут Сөндірулі 