��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �  
   �  #   �     �     �     �       
   $  /   /     _  4   m  b   �  d     A   j  6   �  	   �     �     	  M   	     m	     �	     �	  (   �	     �	  /   �	     
  "   +
     N
  G   ]
     �
                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-11-08 01:24+0600
Last-Translator: Sairan Kikkarin <sairan@computer.org>
Language-Team: Kazakh <kde-i18n-doc@kde.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;





 Жалпы Жаңа скриптті қосу. Қосу... Қайттық па? Түсініктемесі: sairan@computer.org Өңдеу Таңдалған скриптті өңдеу. Өңдеу... Таңдалған скрипттің орындау "%1" интерпретаторы үшін скриптті құру кездегі жаңылыс "%1" скрипт файлы үшін лайық интерпретаторы анықталмады "%1" интерпретаторын жүктеу жаңылысы "%1" скрипт файлын ашу жаңылысы Файл: Таңбашасы: Интерпретаторы: Ruby интерпретаторының қауіпсіздік деңгейі Сайран Киккарин Атауы: "%1" функциясы жоқ "%1" интерпретаторы жоқ Кетіру Таңдалған скриптті өшіру. Орындау %1 скрипт файлы жоқ. Тоқтату Таңдалған скрипттің орындауын тоқтату Мәтіні: 