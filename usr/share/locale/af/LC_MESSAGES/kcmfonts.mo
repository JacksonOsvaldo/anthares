��          �      ,      �     �  /   �     �     �     �     �            I   ,     v  "   z     �  6   �  *   �          )  �  6     �  <   �     '     B  $   F  %   k     �     �  b   �       6     '   P  A   x  5   �     �     �                                                           
      	              to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts stable
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2005-06-29 11:28+0200
Last-Translator: JUANITA FRANZ <JUANITA.FRANZ@VR-WEB.DE>
Language-Team: AFRIKAANS <translate-discuss-af@lists.sourceforge.net>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
  na  'n Nie-proporsionele skriftipe (i.e. tikmasjien skrif tipe). Verstel Alle Skriftipes... BGR Kliek na verandering alle skriftipes Konfigureer Anti-Aliasing Instellings Konfigureer... Sluit uit omvang: Leiding is 'n proses wat gebruik word om die kwaliteit van skriftipes in klein grotes te verbeter. RGB Gebruik word deur kieslys stawe en opspring kieslyste. Gebruik word deur die venster titelbar. Gebruik word vir normale teks (e.g. knoppie etikette, lys iteme). Gebruik word na vertoon teks behalwe nutsbalk ikoone. Vertikaal BGR Vertikaal RBG 