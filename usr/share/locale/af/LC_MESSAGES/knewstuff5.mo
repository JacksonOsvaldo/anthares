��          �      l      �  �   �  �   �  �   B     �     �     �     �     �     �          
           %  	   .     8     >     Q     e     l     s  �  |  �   +  �   �  �   �     a     i     m     y     �     �  
   �  +   �     �  	   �  	   �     �     �     �     	     	  	   	                                    
                	                                              <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> Author: BSD Description: Details Error GPL Install Key used for signing: LGPL License: Password: Reset Select Signing Key Share Hot New Stuff Title: Update Version: Project-Id-Version: kdelibs4 stable
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2006-01-12 16:33+0200
Last-Translator: JUANITA FRANZ <JUANITA.FRANZ@VR-WEB.DE>
Language-Team: AFRIKAANS <translate-discuss-af@lists.sourceforge.net>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 <qt>Kan nie <i>gpg</i> begin om geldigheid van 'n lêer te toets nie. Maak seker dat <i>gpg</i> geïnstalleer is, anders sal die verifiëring van afgelaaide hulpbronne nie moontlik wees nie.</qt> <qt>Kan nie <i>gpg</i> begin om beskikbare sleutels aan te vra nie. Maak seker dat <i>gpg</i> geïnstalleer is, anders sal die verifiëring van afgelaaide hulpbronne nie moontlik wees nie.</qt> <qt>Kan nie <i>gpg</i> begin om 'n lêer te onderteken nie. Maak seker dat <i>gpg</i> geïnstalleer is, anders sal die ondertekening van hulpbronne nie moontlik wees nie.</qt> Outeur: BSD Beskrywing: Details Fout GPL Installeer Sleutel wat vir ondertekening gebruik word: LGPL Lisensie: Wagwoord: Herstel Kies ondertekening sleutel Deel Nuwe Goed Titel: Update Weergawe: 