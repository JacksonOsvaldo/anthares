��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �        �     �  �  S   Y
     �
     �
     �
     �
          -      J     k                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch stable
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-06-10 16:33+0200
Last-Translator: Juanita Franz <juanita.franz@vr-web.de>
Language-Team: AFRIKAANS <translate-discuss-af@lists.sourceforge.net>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
  sek Opstart indikasie tydverstreke: <H1>Taakbalk Inkenningsstelling</H1>
U kan 'n tweede metode van selflaai kennisgewing ontsper wat
gebruik word deur die taakbalk waar 'n knoppie met 'n roterende uurglas verskyn,
dit simboliseer dat u beginde aanwending besig is om te laai.
Dit mag voorkom, dat sommige aanwendings is nie bewus van hierdie selflaai
kennisgewing. In hierdie geval, verdwyn die knoppie na die
gegee in die seksie 'Selflaai indikasie tyduit' <h1>Besige Plekaanduier</h1>
Kde bied 'n besige Plekaanduier vir aansoek selflaai inkennisstelling.
Om die besige Plekaanduier te aktiveer, kies een soort virtuele terugvoer
van die comboboks.
Dit dalk mag gebeur, dat sommige programme nie bewus is van hierdie selflaai
inkennisstelling. In hierdie geval, stop die Plekaanduier om te flikker na die tyd
gegewe in die seksie 'selflaai indikasie tyduit' <h1>Lanseer terugvoer</h1> U kan die aanwending-Lanseer terugvoer hier konfigureer. Flikkerende Plekaanduier Terugbonsende Plekaanduier Besige Plekaanduier Aktiveer taakbalk kennisgewing Geen Besige Plekaanduier Passiewe Besige Plekaanduier Begin&op indikasie tydverstreke: Taakbalk Inkennisstelling 