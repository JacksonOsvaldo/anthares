��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l  
   K     V     j     �     �     �     �     �  
   �  	   �     �     �                    (     4     C     J     R     [     p     �     �     �     �     �     �  $   �     �          *     :     O     V     v     �     �     �  "   �     �     �     �     �                         -     L     X     `     |     �     �     �  d   �          !  	   5     ?  	   N     X     _     m      t     �     �     �     �  $   �       3   4     h  %   �     �     �     �  	   �  	   �     �     �     �  	     	     
     
   $     /  '   4  
   \  "   g  %   �  %   �  q   �     H     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-01-12 18:30+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Supprimer &Vider la corbeille &Déplacer vers la corbeille &Ouvrir &Coller &Propriétés &Rafraîchir le bureau &Rafraîchir la vue &Recharger &Renommer Sélectionner… Effacer l'icône Aligner Apparence : Disposer en Disposer en Disposition : Retour Annuler Colonnes Configurer le bureau Titre personnalisé Date Défaut Décroissant Description Tout désélectionner Disposition du bureau Saisissez un titre personnalisé ici Fonctionnalités : motif de nom de fichier : Type du fichier Types de fichiers : Filtre Aperçus flottants des dossiers Dossiers en premier Dossiers en premier Emplacement complet Pigé cacher les fichiers correspondants Énorme Taille des icônes Icônes Grande Gauche Liste Emplacement Emplacement : Verrouiller à cet emplacement Verrouillé Moyenne Plus d'options d'aperçu… Nom Aucun Ok Bouton du panneau : Restez appuyé sur les composants graphiques pour les déplacer et faire apparaître leurs poignées Modules d'extension d'aperçu Vignettes d'aperçu Supprimer Redimensionner Rétablir Droite Faire pivoter Lignes Rechercher le type de fichier… Tout sélectionner Sélectionner le dossier Marqueurs de sélection Afficher tous les fichiers Afficher les fichiers correspondants Afficher un emplacement : Afficher les fichiers liés à l'activité actuelle Afficher le dossier du bureau Afficher la boîte à outil du bureau Taille Petite Petite Trier par Trier par Tri : Spécifier un dossier : Lignes de texte Minuscule Titre :  Infobulles Bidouilles Type Saisissez un emplacement ou une URL ici Non triés Utiliser une icône personnalisée Manipulation de composants graphiques Composants graphiques déverrouillés Vous pouvez rester appuyé sur les composants graphiques pour les déplacer et faire apparaître leurs poignées. Mode d'affichage 