��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s  	   h  &   r     �     �     �     �     �     �  B   �      6  '   W          �     �     �     �     �     �     	  $        @     [     v     ~  "   �     �      �     �  	   �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2018-01-12 18:29+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 par %2 Ajouter un fond d'écran personnalisé Ajouter un dossier... Ajouter une image... Arrière-plan : Flou Centré Changer chaque : Dossier contenant les images à afficher en tant que fond d'écran Télécharger des fonds d'écran Obtention de nouveaux fonds d'écran... Heures Fichiers images Minutes Image de fond d'écran suivante Ouvrir le dossier contenant Ouvrir l'image Ouvrir l'image de fond d'écran Positionnement : Fichier de fond d'écran recommandé Supprimer le fond d'écran Restaurer le fond d'écran Adapté Adapté et rogné Adapté, conserver les proportions Secondes Sélectionner la couleur de fond Couleur unie Mosaïque 