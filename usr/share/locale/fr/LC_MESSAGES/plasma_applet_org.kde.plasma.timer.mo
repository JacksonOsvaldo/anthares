��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     r     �     �  
   �     �  	   �     �  	   �     �       6        K  	   b     l     �     �     �     �     �  %   �     �  �                           	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-05-17 14:58+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 est en cours d'exécution %1 pas en cours d'exécution &Réinitialiser &Démarrer Avancé Apparence Commande : Affichage Exécuter une commande Notifications Temps restant : %1 seconde Temps restant : %1 secondes Exécuter une commande Arrê&ter Afficher une notification Afficher les secondes Afficher le titre Texte : Minuteur Décompte terminé Le minuteur est en cours d'exécution Titre : Utilisez la molette de la souris pour changer les chiffres ou choisissez depuis des durées prédéfinies dans le menu contextuel 