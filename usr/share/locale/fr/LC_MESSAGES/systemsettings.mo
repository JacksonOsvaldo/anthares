��    5      �  G   l      �  A   �     �     �  /   �  8   )     b     k          �     �     �     �     �     �  $   �  	          !   !  3   C  	   w     �      �  $   �     �  *   �     
  	     5        O     o  
   �     �     �     �     �  5   �  3     6   D     {  ,   �  /   �  	   �     �     	     	  O   "	  Z   r	  b   �	  	   0
  
   :
  P   E
     �
  �  �
  6   �     �     �  9   �  0   .     _     o      �  )   �     �     �                 (      
   I     T  +   n  B   �     �     �  2   �  .   0     _  	   y     �     �  =   �  %   �       
        $     1  !   @     b  F   z  H   �  M   
     X  F   d  
   �     �  '   �     �               �  h        �     �  q   �     %        3   *             +      5       #       1             &      .           )                   ,             $                                      %      "   -       !   2            
       	   4      '       /                       0          (    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2017-09-21 09:25+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 est une application externe lancée automatiquement (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contient 1 élément</i> <i>Contient %1 éléments</i> Aucun élément correspondant à votre recherche À propos de %1 À propos du module courant À propos de l'affichage courant À propos de la configuration du système Tous les paramètres Appliquer les paramètres Auteur Retour Ben Cooksley Centre de configuration central par KDE. Configurer Configurer votre système Contient 1 élément Contient %1 éléments Détermine si des infobulles détaillées doivent être utilisées Développeur Boîte de dialogue aminesay@yahoo.fr,johan.claudebreuninger@gmail.com Développer automatiquement le premier niveau  Utilisés fréquemment : Général Aide Affichage en icônes Représentation interne du module, modèle interne du module  Nom interne pour l'affichage utilisé Raccourci clavier : %1 Mainteneur Marco Martin Mathias Soeken Amine Say,Johan Claude-Breuninger Aucun affichage trouvé Fournit un affichage des modules de contrôle par catégorie d'icônes Fournit un panneau latéral catégorisé pour les modules de contrôles. Fournit un affichage classique en arborescence pour les modules de contrôle. Relancer %1 Réinitialiser toutes les modifications avec les valeurs précédentes Rechercher Chercher… Afficher les bulles détaillées d'aide Vue avec panneau latéral Configuration du système Impossible pour le module de configuration du système de trouver des affichages. Par conséquent, il n'y a rien à configurer. Impossible pour le module de configuration du système de trouver des affichages. Par conséquent, il n'y a rien à configurer. Les paramètres du module courant ont changé.
Voulez-vous appliquer ces modifications ou les annuler ? Affichage en arborescence Style d'affichage Bienvenue dans le module « Configuration du système », l'endroit essentiel pour configurer votre ordinateur. Will Stephenson 