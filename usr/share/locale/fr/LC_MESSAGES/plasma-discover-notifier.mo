��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     �  7   �  ;   �  7        =  Q   \  '   �     �     �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2015-11-18 21:41+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1, %2 1 paquet à mettre à jour %1 paquets à mettre à jour 1 mise à jour de sécurité %1 mises à jour de sécurité 1 paquet à mettre à jour %1 paquets à mettre à jour Aucun paquet à mettre à jour avec la présence d'une mise à jour de sécurité %1 mises à jour de sécurité Mises à jour de sécurité disponibles Système à jour Mettre à jour Mises à jour disponibles 