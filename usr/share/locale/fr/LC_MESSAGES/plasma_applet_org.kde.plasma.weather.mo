��    N      �  k   �      �     �  ^   �  H     
   V     a     t     �     �     �  '   �     �  "   �     
     $     ;  
   D     O     h     x  	   �     �     �     �     �     �     �  "   �     	     	  	   (	     2	  !   9	     [	  ?   y	     �	     �	     �	     �	     �	     �	     

  ;   
  &   R
      y
  %   �
     �
     �
     �
       >   &     e     �  '   �  +   �  !   �          1     B     U     h     y     �     �     �     �     �     �     �               0     A     T     g     �     �  3   �  �  �     �     �     �  	   �     �          &     )     2     I     [     l     |     �     �     �     �     �  	   �     �     �     �               3     B  /   F  	   v     �     �     �  :   �     �     �     �     �                (     8     >     Q     Z     l     �     �     �  
   �     �     �     �     �     �               '     =     ?     C     G     I     L     P     T     W     Y     \     `     d     g     j     l     p     t     }  !   �     �     .       >       #         J   L   !   (      ?                                 1   D       &               0          5              N   ;      G   I      7       6      =       <   /   )          -      +   9      M   C   F   B   2   	       *   3                E          $   @                   4       %   :              8   
           ,                      '   "   K      A   H             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-12 18:26+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  min %1 %2 %1 (%2) Apparence Échelle de Beaufort bft Degrés Celsius °C ° Détails Degrés Fahrenheit °F 1 jour %1 jours Hectopascals hPa M : %1 m : %2 Maxima : %1 Pouces de mercure inHg Kelvin K Kilomètres Kilomètres par heure km/h Kilopascals kPa Nœuds kt Lieu : Minima : %1 Mètres par seconde m/s Milles Milles par heure mph Millibars mbar N/D Aucune station météo trouvée pour « %1 » Remarques % Pression : Chercher Sélectionner un fournisseur de services météorologiques - Veuillez configurer Température : Unités Actualiser toutes les : Visibilité : Station météo Calme Vitesse du vent : %1 (%2%) Humidité : %1%2 Visibilité : %1 %2 Point de rosée : %1 Humidex : %1 décroissante croissante stable Tendance barométrique : %1 Pression : %1 %2 %1 %2 Visibilité : %1 %1 (%2) Alertes émises : Vigilances émises : E ENE ESE N NE NNE NNO NO S SE SSE SSO SO VR O ONO OSO %1 %2 %3 Calme Refroidissement dû au vent : %1 Rafale de vent : %1 %2 