��    	      d      �       �      �   -   �   *   %      P  '   q  
   �     �     �  �  �     �  2   �  5   �  %   *  0   P  
   �     �  "   �                   	                           (c) 2009 Marco Martin Display informational tooltips on mouse hover Display visual feedback for status changes EMAIL OF TRANSLATORSYour emails Global options for the Plasma Workspace Maintainer Marco Martin NAME OF TRANSLATORSYour names Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-12-15 18:03+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 (c) 2009 Marco Martin Affiche des infobulles lors du survol de la souris Affiche un retour visuel lors des changements d'état xavier.besnard@neuf.fr, annma@kde.org Options globales pour l'espace de travail Plasma Mainteneur Marco Martin Xavier Besnard, Anne-Marie Mahfouf 