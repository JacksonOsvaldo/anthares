��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �            A   #  8   e  G   �  G   �  @   .  "   o     �  D   �     �            (   :     c     {     �     �     �     �     �     �               4     J     V     ^     m     �     �  *   �  O   �  .   3     b     y  B   �  
   �  
   �  ;   �     "  -   0     ^     r     z     �  	   �     �     �     �  
   �  %   �  !        &  $   4     Y     p     �     �  ^   �  ?     	   G     Q  	   Y     c     ~     �  Q   �  
             +     7     F     Z     f  !   x  
   �  #   �  	   �  %   �     �       "   $     G     O     _  (   e     �     �     �     �  
   �     �  !        #  2   A         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-02-08 22:56+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Également disponible dans %1 %1 %1 (%2) %1 (par défaut) <b>%1</b> par %2 <em>%1 personnes sur %2 ont trouvé cette évaluation utile</em> <em>Informez-nous à propos de cette évaluation !</em> <em>Utile ? <a href='true'><b>Oui</b></a>/<a href='false'>Non</a></em> <em>Utile ? <a href='true'>Oui</a>/<a href='false'><b>Non</b></a></em> <em>Utile ? <a href='true'>Oui</a>/<a href='false'>Non</a></em> Téléchargement des mises à jour Téléchargement... La date de la dernière vérification des mises à jour est inconnue Recherche de mises à jour Aucune mise à jour Aucune mise à jour disponible. Vous devriez vérifier les mises à jour Le système est à jour Mises à jour Mise à jour… Accepter Ajouter une source... Modules complémentaires Aleix Pol Gonzalez Un explorateur d'applications Appliquer les changements Moteurs disponibles :
 Modes disponibles :
 Précédent Annuler Catégorie :  Recherche de mises à jour... Commentaire trop long Commentaire trop court Mode compact (auto / compact / complet). Impossible de fermer l'application, car certaines tâches sont encore en cours. Impossible de trouver la catégorie « %1 » Impossible d'ouvrir %1 Supprimer l'origine Ouvre directement l'application spécifiée par son nom de paquet. Abandonner Découvrir Affiche une liste des éléments comportant une catégorie. Extensions… Impossible de supprimer la source « %1 ». Applications phares Aide… Page personnelle : Améliorer le résumé Installer Installé(s) Jonathan Thomas Lancer Licence : Liste toutes les moteurs disponibles. Liste tous les modes disponibles. Chargement... Fichier de paquet local à installer Définir comme défaut Plus d'informations... Davantage… Aucune mise à jour Ouvre Discover dans le mode indiqué. Les modes correspondent aux boutons des barres d'outils. Ouvre avec un programme pouvant se charger du type MIME donné. Continuer Note : Supprimer Ressources pour « %1 » Soumettre une évaluation Évaluation de « %1 » L'exécution en tant que <em>superutilisateur</em> est inutile et déconseillée. Rechercher Chercher dans « %1 »... Chercher... Chercher : %1 Chercher : %1 + %2 Paramètres Court résumé… Afficher les évaluations (%1)... Taille :  Désolé, rien n'a été trouvé... Source : Spécifier la nouvelle source pour %1 Recherche toujours en cours… Résumé : Prend en charge les URL appstream: Tâches Tâches (%1 %) %1 %2 Impossible de trouver la ressource : %1 Tout mettre à jour Mises à jour sélectionnées Mettre à jour (%1) Mises à jour Version : évaluateur inconnu Mises à jour non sélectionnées Mises à jour sélectionnées © 2010-2016 l'équipe de développement de Plasma 