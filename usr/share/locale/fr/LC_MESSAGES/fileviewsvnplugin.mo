��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     y  /   �  ,   �  .   �  /   	     =	  ,   Z	  1   �	  2   �	  5   �	  <   "
  9   _
  <   �
  m   �
  *   D     o  '   �  	   �     �     �  1   �       #        C     b     q     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2017-05-11 13:29+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Valider Les fichiers ont été ajoutés au dépôt SVN. Ajout de fichiers au dépôt SVN en cours... L'ajout au dépôt SVN de fichiers a échoué. La validation des modifications SVN a échoué. Modifications SVN validées. Validation des modifications SVN en cours... Les fichiers ont été supprimés du dépôt SVN. Suppression de fichiers du dépôt SVN en cours... La suppression du dépôt SVN de fichiers a échoué. Le retour des contenu des fichiers du dépôt SVN a réussi. Retour du contenu des fichiers du dépôt SVN en cours... Le retour du contenu des fichiers du dépôt SVN a échoué. La mise à jour de l'état SVN a échoué. Désactivation de l'option « Afficher les mises à jour SVN ». La mise à jour du dépôt SVN a échoué. Dépôt SVN mis à jour. Mise à jour du dépôt SVN en cours... Ajout SVN Validation SVN... Suppression SVN Retour du contenu vers une version donnée de SVN Mise à jour SVN Afficher les changements locaux SVN Afficher les mises à jour SVN Description : Validation SVN Afficher les mises à jour 