��    	      d      �       �   	   �      �   <   �   5   0     f     �  4   �     �  �  �     �  	   �     �     �  3   �     '  	   7     A                                          	    Ends at 5 General Show the number of the day (eg. 31) in the iconDay in month Show the week number (eg. 50) in the iconWeek number Show week numbers in Calendar Starts at 9 What information is shown in the calendar iconIcon: Working Day Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-09-05 22:35+0100
Last-Translator: Thomas Vergnaud <thomas.vergnaud@gmx.fr>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Termine à 17 h Général Jour du mois Numéro de la semaine Afficher les numéros de semaine dans le calendrier Commence à 9 h Icône : Jour travaillé 