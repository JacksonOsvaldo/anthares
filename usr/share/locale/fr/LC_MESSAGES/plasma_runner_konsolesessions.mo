��          4      L       `   $   a   /   �   �  �   5   �  =   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-07-02 13:35+0200
Last-Translator: Joëlle Cornavin <jcornavi@club-internet.fr>
Language-Team: Français <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Cherche les sessions de Konsole correspondant à :q:. Répertorie toutes les sessions de Konsole dans votre compte. 