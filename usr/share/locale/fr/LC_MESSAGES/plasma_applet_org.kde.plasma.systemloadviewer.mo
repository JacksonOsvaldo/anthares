��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �                 $     A  
   G  
   R     ]     k     �     �     �     �  6   �     
       
   ,     7     @     O  	   a      k     �     �     �     �     �  "   �     	     	     3	     P	     p	     |	     �	     �	     �	  #   �	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-07-03 17:56+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 s Application : Fréquence moyenne : %1 MHz Barre Tampons : Processeur Processeur %1 Moniteur de processeur Processeur %1 : %2 % @ %3 Mhz Processeur : %1% séparer les processeurs Cache Cache dirty, en écriture différée : %1 MiB, %2 MiB Moniteur de cache Mis en cache : Circulaire Couleurs Barre compacte Mémoire dirty : Général Attente d'Entrées / Sorties : Mémoire Moniteur de mémoire Mémoire : %1/%2 Mio Type de moniteur : Niveau de priorité : Définir les couleurs manuellement Afficher : Espace d'échange (swap) Moniteur d'espace d'échange Espace d'échange : %1/%2 Mio Système : Charge système Intervalle de mise à jour : Espace d'échange utilisée : Utilisateur : Mémoire en écriture différée : 