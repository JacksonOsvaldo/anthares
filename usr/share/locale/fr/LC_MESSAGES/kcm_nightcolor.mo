��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     m     p     x     �     �     �  H   �  ,   "  -   O     }     �  	   �  1   �     �  "   �       
        !     =     [     b  
   w                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: kcm_nightcolor
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-15 17:51+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  K (HH:MM) (En minutes - min. 1, max. 600) Activer la couleur de nuit Automatique Détecter la localisation geoffray.levasseurbrandin@numericable.fr, kde@macolu.org, vpinon@kde.org Erreur : le matin ne précède pas le soir. Erreur : le temps de transition se recouvre. Latitude Emplacement Longitude Geoffray Levasseur, Matthieu Robin, Vincent Pinon Couleur de nuit Température de couleur de nuit : Mode d'opération : Roman Gilg Le lever de soleil commence Le coucher de soleil commence Heures Durée de transition et termine 