��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  7   �          .     C  #   a  	   �  (   �     �     �     �  )        
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-01-15 07:45+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Copier automatiquement la couleur vers le presse-papier Effacer l'historique Options des couleurs Copier dans le presse-papiers Format par défaut de la couleur : Général Ouvrir la boîte de dialogue de couleurs Sélectionner une couleur Sélectionner une couleur Afficher l'historique Lorsque le raccourci clavier est composé 