��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  2   �	  /   �	  1   .
      `
  7   �
  2   �
  4   �
  *   !     L  (   d  ,   �     �  )   �  ;   �  <   8  ?   u  *   �  '   �  '     -   0  +   ^  -   �  *   �  *   �          #     8     P     b     w     �  ,   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-07-02 10:49+0200
Last-Translator: xavier <xavier.besnard@neuf.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Des fichiers ont été ajoutés au dépôt Bazaar. Ajout de fichiers au dépôt Bazaar en cours... L'ajout de fichiers au dépôt Bazaar a échoué. Le journal de Bazaar est fermé. La validation des modifications dans Bazaar a échoué. Les modifications dans Bazaar ont été validées. Validation des modifications dans Bazaar en cours... L'extraction du dépôt Bazaar a échoué. Dépôt Bazaar extrait. Extraction du dépôt Bazaar en cours... La publication du dépôt Bazaar a échoué. Dépôt Bazaar publié. Publication du dépôt Bazaar en cours... Des fichiers ont été supprimés depuis le dépôt Bazaar. Suppression de fichiers depuis le dépôt Bazaar en cours... La suppression de fichiers depuis le dépôt Bazaar a échoué. La révision des modifications a échoué. Les modifications ont été révisées. Révision des modifications en cours... L'exécution du journal de Bazaar a échoué. Exécution du journal de Bazaar en cours... La mise à jour du dépôt Bazaar a échoué. Mise à jour du dépôt Bazaar effectuée. Mise à jour du dépôt Bazaar en cours... Ajout dans Bazaar... Validation Bazaar... Suppression dans Bazaar Journal de Bazaar Extraction de Bazaar Publication dans Bazaar Mise à jour de Bazaar Afficher les modifications locales de Bazaar 