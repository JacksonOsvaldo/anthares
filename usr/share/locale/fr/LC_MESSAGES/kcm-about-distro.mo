��          �      L      �     �  .   �       5     K   U     �      �     �     �     �          !     )     H     Q     h     t     }  �  �     s  !   z     �     �     �     �  [   �  	   =     G     d     }     �  C   �  "   �          !  	   2     <                                                                    
                      	          @info:creditAuthor @info:creditCopyright 2012-2014 Harald Sitter @info:creditHarald Sitter @label %1 is the CPU bit width (e.g. 32 or 64)%1-bit @label %1 is the formatted amount of system memory (e.g. 7,7 GiB)%1 of RAM @titleAbout Distribution EMAIL OF TRANSLATORSYour emails Hardware KDE Frameworks Version: KDE Plasma Version: Kernel Version: Memory: NAME OF TRANSLATORSYour names OS Type: Processor: Processors: Qt Version: Software Unknown amount of RAMUnknown Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-11-23 14:43+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Auteur Copyright 2012-2014 Harald Sitter Harald Sitter %1-bit %1 de mémoire vive A propos de la distribution geoffray.levasseurbrandin@numericable.fr, jcorn@free.fr, mcorteel@gmail.com, vpinon@kde.org Matériel Version de KDE Frameworks : Version de KDE Plasma : Version de noyau : Mémoire : Geoffray Levasseur, Joëlle Cornavin, Maxime Corteel, Vincent Pinon Type de système d'exploitation : Processeur : Processeurs : Version de Qt : Logiciels Inconnu 