��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �       !        A  	   [     e     z  6   �  $   �     �     �          4     @  U   ]        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-03-10 23:21+0100
Last-Translator: Thomas Vergnaud <thomas.vergnaud@gmx.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Configurer les imprimantes… Uniquement les tâches actives Toutes les tâches Uniquement les tâches terminées Configurer une imprimante Général Aucune tâche active Aucune tâche Aucune imprimante n'a été configurée ou découverte Une tâche active %1 tâches actives Une tâche %1 tâches Ouvrir la file d'impression La file d'impression est vide Imprimantes Rechercher une imprimante… Il y a une tâche d'impression dans la file Il a %1 tâches d'impression dans la file 