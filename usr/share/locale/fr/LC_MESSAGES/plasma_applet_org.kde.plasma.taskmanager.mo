��    P      �  k         �     �     �     �     �     �     �       .   	  U   8     �     �      �     �  /   �  /        E     Q     Z  
   f     q     �  $   �     �     �     �     �     �     �  	   	     	  
   .	  7   9	     q	     �	     �	     �	  	   �	     �	  !   �	     �	     �	  	   	
      
     4
     A
     Z
     l
     }
     �
     �
     �
     �
     �
  (   �
     �
  =        M     b  "   }     �     �  J   �  1     )   @  (   j  '   �  "   �  4   �          !     '     0     C     W     j  U   �  N   �  9   %  J   _  �  �     �     �     �  
   �     �  	   �  	   �     �     �          <  )   R     |  =   �  =   �               *  
   8     C     X  =   x     �     �     �     �  	   �            $   '     L     b     f      �      �     �  
   �     �  -   �          3  
   O  ,   Z     �  !   �     �     �     �     �               /     H  @   d  /   �  	   �     �     �     �            
     3   *  A   ^  :   �  <   �  +     P   D     �     �     �     �     �     �     �  	   �     �       $   $     &   O         A   B       ?   ,   #       -       G   0   C   P           :      <      !                      H          4          %         @                        3   M   +      "      I   >                   =              (           L   ;       .            /   J   K   7   D           N       2          $   1      5       8       )   	   
           9               E   6   *   '   F    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-08 22:58+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Tous les bureaux &Fermer &Plein écran &Déplacer &Nouveau bureau &Attacher &Enrouler &%1 %2 Également disponible sur %1 Ajouter à l'activité courante Toutes les activités Autoriser ce programme à être regroupé Par ordre alphabétique Toujours organiser les tâches en colonnes d'autant de lignes Toujours organiser les tâches en lignes d'autant de colonnes Organisation Comportement Par activité Par bureau Par nom de programme Fermer la fenêtre ou le groupe Passer d'une tâche à une autre avec la molette de la souris Ne pas grouper Ne pas trier Filtres Oublier les documents récents Général Regroupement et tri Regroupement : Mettre les fenêtres en surbrillance Taille des icônes : — Conserver &au-dessus des autres Conserver en-des&sous des autres Conserver des lanceurs séparés Grande Ma&ximiser Manuellement Marquer les applications qui émettent du son Nombre maximal de colonnes : Nombre maximal de lignes : Mi&nimiser Minimiser/rétablir la fenêtre ou le groupe Plus d'actions Déplacer vers le bureau &courant Déplacer vers l'&activité Déplacer vers le &bureau Couper le son Nouvelle instance Sur %1 Sur toutes les activités Sur l'activité courante Action du bouton central : Regrouper seulement lorsque le gestionnaire de tâches est plein Ouvrir les groupes dans des fenêtre flottantes Restaurer Pause Piste suivante Piste précédente Quitter Re&dimensionner &Détacher %1 place supplémentaire %1 places supplémentaires Afficher uniquement les tâches provenant de l'activité actuelle Afficher uniquement les tâches provenant du bureau actuel Afficher uniquement les tâches provenant de l'écran actuel Afficher uniquement les tâches minimisées Afficher la progression et les informations d'état dans les boutons des tâches Afficher les infobulles Petite Tri : Démarrer une nouvelle instance Lecture Arrêter Aucun &Attacher Grouper / dégrouper Disponible sur %1 Disponible sur toutes les activités 