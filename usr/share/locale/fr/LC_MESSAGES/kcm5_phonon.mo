��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  \   �  n     +   �  �   �  '   <     d  N   r     �  T   �     +     2  
   @     K  <   i  B   �  C   �  )   -  	   W  m   a     �     �  �     H   �  =   	     G     T     a     x     �  	   �     �     �     �  .   �            5   #  !   Y     {     �     �     �     �     �     �  %   �     �       	        (  #   =     a     s  $   z  
   �  �   �     l  U   z  �   �     �  U   �  A   �  B   5  8   x  &   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-12 10:33+0200
Last-Translator: xavier <ktranslator31@yahoo.fr>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Pour appliquer le changement de moteur, vous devrez vous déconnecter puis vous reconnecter. Une liste des moteurs pour Phonon trouvés sur votre système. Phonon les utilisera dans l'ordre indiqué ici. Appliquer la liste de périphériques à... Appliquer la liste actuellement affichée de préférences de périphériques aux autres catégories de lecture audio ci-dessous : Configuration des périphériques audio Lecture audio Préférences du périphérique de lecture audio pour la catégorie « %1 » Enregistrement audio Préférences du périphérique d'enregistrement audio pour la catégorie « %1 » Moteur Colin Guthrie Connecteur Copyright 2006 Matthias Kretz Préférences du périphérique de lecture audio par défaut Préférences du périphérique d'enregistrement audio par défaut Préférences du périphérique d'enregistrement vidéo par défaut Catégorie par défaut / non spécifiée Descendre Définit l'ordre par défaut des périphériques pouvant être outrepassé par des catégories individuelles. Configuration du périphérique Préférences de périphérique Périphériques trouvés sur votre système, utilisables pour la catégorie sélectionnée. Choisissez le périphérique que vous souhaitez être utilisé par les applications. xavier.besnard@neuf.fr, mrjay01@users.sourceforge.net, guill.p@gmail.com Impossible de sélectionner le périphérique de sortie audio Avant centre Avant gauche Avant gauche du centre Avant droit Avant droit du centre Matériel Périphériques indépendants Niveaux d'entrée Non valable Configuration des périphériques audio de KDE Matthias Kretz Mono Xavier Besnard, Jean-Jacques Finazzi, Guillaume Pujol Module de configuration de Phonon Lecture (%1) Monter Profil Arrière centre Arrière gauche Arrière droit Enregistrement (%1) Afficher les périphériques avancés Latéral gauche Latéral droit Carte son Périphérique audio Emplacement et test du haut-parleur Caisson de basses Tester Test du périphérique sélectionné %1 en test L'ordre détermine la précédence des périphériques. Si, pour une raison quelconque, le premier périphérique ne peut être utilisé, Phonon essaiera d'utiliser le second et ainsi de suite. Canal inconnu Utiliser la liste actuellement affichée de périphériques pour plus de catégories. Différentes catégories pour des usages de média. Pour chaque catégorie, vous pouvez choisir le périphérique que vous préférez pour être utilisé par les applications Phonon. Enregistrement vidéo Préférences du périphérique d'enregistrement vidéo pour la catégorie « %1 » Votre moteur peut ne pas prendre en charge l'enregistrement audio Votre moteur peut ne pas prendre en charge l'enregistrement vidéo pas de préférence pour le périphérique sélectionné monter le périphérique sélectionné 