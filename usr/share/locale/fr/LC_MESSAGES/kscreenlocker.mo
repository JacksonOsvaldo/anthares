��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "  �  $  =        J     a      x     �  1   �     �  E   �  F   :  1  �                      
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: kscreenlocker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-06-28 21:53+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 S'assure que l'écran est verrouillé avant la mise en veille Verrouiller la session Écran de verrouillage Verrouillage de l'écran activé Écran verrouillé Temps d'attente pour démarrer l'écran de veille Écran déverrouillé Définit le nombre de minutes après lequel l'écran est verrouillé. Définir si l'écran sera verrouillé après le laps de temps défini. Un problème est survenu avec le verrouillage d'écran et il n'est plus possible de déverrouiller.
Pour déverrouiller, passez dans un terminal virtuel (par exemple Ctrl+Alt+F2),
connectez-vous et exécutez la commande :

loginctl unlock-session %1

Revenez ensuite à la session actuelle (Ctrl+Alt+F%2). 