��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	     �	  #   �	  	   �	     �	     
     
     
     3
     Q
  
   g
     r
     �
  ,   �
  .   �
     �
          4  f   C     �     �     �     �  "     	   0     :     A     X  -   _     �     �  ;   �  $   �  X      /   Y  
   �     �     �     �     �  	   �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-07-03 18:48+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Ne pas se souvenir Parcourir les activités Parcourir les activités (inversé) Appliquer Annuler Modifier… Créer Paramètres des activités Créer une nouvelle activité Supprimer l'activité Activités Informations de l'activité Basculer l'activité Voulez-vous vraiment supprimer « %1 » ? Mettre toutes ces applications sur liste noire Effacer l'historique récent Créer une activité... Description : Erreur lors du chargement des fichiers QML. Veuillez vérifier votre installation.
Fichier %1 manquant Pour toutes les applications Effacer la dernière journée Effacer tout Effacer la dernière heure Effacer les deux dernières heures Général Icône Conserver l'historique Nom : Uniquement pour des applications spécifiques Autre Vie privée Privé - ne pas conserver d'historique pour cette activité Se souvenir des documents ouverts :  Se souvenir du bureau virtuel courant pour chaque activité (nécessite un redémarrage) Raccourci pour basculer vers cette activité :  Raccourcis Basculer Fond d'écran pour  mois  mois Illimité 