��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �     �     �     �  0     *   5     `     x     �     �     �     �  
   �     �     �     �  	             '     C  8   W     �  !   �     �     �     �               2     E     Y  	   x     �     �  	   �     �  	   �     �     �     �     �     �  "        3     R     f     �     �  #   �     �     �     �               %     7     J     _     t     �     �     �  2   �     �     
          1     =     U  '   ]     �  "   �  /   �  *   �  +     #   I     m     �     �  	   �     �  :   �          ,     =     S     i     r  	   �     �  3   �  5   �              Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-14 17:57+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Gérer « %1 »… Sélectionner… Effacer l'icône Ajouter au bureau Ajouter aux favoris Ajouter au tableau de bord (composant graphique) Aligner les résultats de recherche en bas Toutes les applications %1 (%2) Applications Applications et documents Comportement Catégories Ordinateur Contacts Description (Nom) Seulement les descriptions Documents Éditer les applications… Éditer les applications… Terminer la session Étendre la recherche aux signets, fichiers et courriels Favoris Aplatir le menu en un seul niveau Tout oublier Oublier toutes les applications Oublier tous les contacts Oublier tous les documents Oublier l'application Oublier le contact Oublier le document Oublier les documents récents Général %1 (%2) Hiberner Cacher %1 Masquer l'application Icône : Verrouiller Verrouiller l'écran Déconnexion Nom (Description) Seulement les noms Applications utilisées couramment Documents utilisés couramment Utilisé couramment Sur toutes les activités Sur l'activité courante Ouvrir avec : Attacher au gestionnaire de tâches Emplacements État d'allumage / session Propriétés Redémarrer Applications récentes Contacts récents Documents récents Utilisés récemment Utilisés récemment Lecteur amovible Supprimer des favoris Redémarrer Exécuter la commande… Exécute une commande ou une requête de recherche Enregistrer la session Chercher… Résultats de la recherche Chercher… Recherche de « %1 » Session Afficher les informations du contact… Afficher dans les favoris Afficher les applications comme : Afficher les applications utilisées couramment Afficher les contacts utilisés couramment Afficher les documents utilisés couramment Afficher les applications récentes Afficher les contacts récents Afficher les documents récents Afficher : Éteindre Trier par ordre alphabétique Démarrer une session parallèle avec un autre utilisateur Mettre en veille Mettre en veille Mettre en hibernation Changer d'utilisateur Système Actions du système Éteindre Tapez pour rechercher. Afficher à nouveau les application dans « %1 » Afficher à nouveau les application dans ce sous-menu Composants graphiques 