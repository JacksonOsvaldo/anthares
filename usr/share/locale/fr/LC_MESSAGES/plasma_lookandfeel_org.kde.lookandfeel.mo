��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
          $     4  #   <     `     g  
   {  ,   �     �     �     �  9   �  	   9     C  +   Z     �     �     �  
   �     �     �     �     �       7        H  	   \     f  &   r  	   �  5   �     �  	   �          
          3     A     \     l     {     �     �     �     �  &   �               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-12 18:25+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 % Retour Batterie à %1 % Changer de disposition Clavier virtuel Annuler Verrouillage des majuscules activé Fermer Fermer la recherche Configurer Configurer les modules externes de recherche Session de bureau : %1 Utilisateur différent Disposition du clavier : %1 Déconnexion dans 1 seconde Déconnexion dans %1 secondes Connexion Échec de la connexion Connexion en tant qu'utilisateur différent Déconnexion Piste suivante Aucun média en lecture Inutilisé Ok Mot de passe Lecture / pause du média Piste précédente Redémarrer Redémarrer dans 1 seconde Redémarrer dans %1 secondes Requêtes récentes Supprimer Redémarrer Afficher les contrôles multimédia : Éteindre Extinction dans 1 seconde Extinction dans %1 secondes Démarrer une nouvelle session Suspendre Changer Changer de session Changer d'utilisateur Rechercher… Recherche de « %1 »… Plasma, par KDE Déverrouiller Échec du déverrouillage sur TTY %1 (affichage %2) TTY %1 Nom d'utilisateur %1 (%2) requêtes récentes dans la catégorie 