��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8               4     G     e  [   �  G   �     $     D     `     n     �     �     �  &   �     �  E     	   M     W  �   h                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-06-28 21:49+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % Niveau &critique :  Niveau &faible :  <b>Niveaux de la batterie</b> &Au niveau critique à :  Le niveau de la batterie sera considéré comme critique lorsqu'elle aura atteint ce niveau La batterie sera considérée faible lorsqu'elle aura atteint ce niveau Configurer les notifications... Niveau de batterie critique Ne rien faire jcorn@free.fr, vpinon@kde.org Activé Veille prolongée Niveau de batterie faible Niveau bas pour les périphériques : Joëlle Cornavin, Vincent Pinon Mettre en pause les lecteurs multimédia lors de la mise en veille : Éteindre Mettre en veille Le service de gestion de l'alimentation ne semble pas être en cours de fonctionnement.
Ce problème peut être résolu en le démarrant ou en le programmant dans « Démarrage et arrêt » 