��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  	   �	     �	     
     %
     3
     @
      \
     }
  !   �
     �
     �
  u   �
  �   U     �  W   �  2   D     w  R   �  /   �       ,        J     a  3   t     �     �  o   �  2   7  '   j  x   �  \        h     ~               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-18 19:54+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms  %  %1 - Tous les bureaux %1 - Cube %1 - Application actuelle %1 - Bureau actuel %1 - Cylindre %1 - Sphère Délai de &réactivation : Changer d'écran &sur le bord : &Délai d'activation : Bords et coins actifs de l'écran Gestion des activités Toujours activé Durée nécessaire après le déclenchement d'une action jusqu'à ce que le prochain déclenchement puisse avoir lieu Durée nécessaire pour que le pointeur de la souris soit poussé contre le bord de l'écran avant le déclenchement de l'action Lanceur d'application Changer de bureau quand le pointeur de la souris est poussé contre le bord de l'écran renard@kde.org, vpinon@kde.org, sdepiets@gmail.com Verrouiller l'écran Maximiser les fenêtres en les faisant glisser vers le bord supérieur de l'écran Sébastien Renard, Vincent Pinon, Simon Depiets Aucune action Seulement lors du déplacement des fenêtres Exécuter une commande Autres paramètres Quadrillage par quart déclenché dans l'extérieur Afficher le bureau Désactivé Disposer les fenêtres en mosaïque en les faisant glisser vers le côté droit ou le côté gauche de l'écran (Dés)Activer le changement de fenêtre alternatif (Dés)Activer le changement de fenêtre Déclenchez une action en poussant le pointeur de votre souris contre le bord de l'écran dans la direction de l'action. Déclencher une action en glissant votre curseur depuis un côté de l'écran vers le centre Gestion des fenêtres de l'écran 