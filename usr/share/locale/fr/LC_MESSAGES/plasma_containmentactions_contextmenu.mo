��          \      �       �   F   �   .     1   ?  4   q  7   �  5   �  1       F  /   K  
   {     �     �  $   �     �     �                                       plasma_containmentactions_contextmenuConfigure Contextual Menu Plugin plasma_containmentactions_contextmenuLeave... plasma_containmentactions_contextmenuLock Screen plasma_containmentactions_contextmenuRun Command... plasma_containmentactions_contextmenuWallpaper Actions plasma_containmentactions_contextmenu[Other Actions] plasma_containmentactions_contextmenu[Separator] Project-Id-Version: plasma_containmentactions_contextmenu
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-05-26 18:01+0100
Last-Translator: Maxime Corteel <mcorteel@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 Configurer le module externe de menu contextuel Quitter... Verrouiller l'écran Exécuter une commande... Actions relatives aux fonds d'écran [Autres actions] [Séparateur] 