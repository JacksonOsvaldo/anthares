��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  S   �     �     �     �  %        8  Q   L  L   �  %   �                                   	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-05-17 15:03+0100
Last-Translator: Yoann Laissus <yoann.laissus@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 La tentative d'effectuer l'action « %1 » a échoué avec le message « %2 ». Média suivant Média précédent Contrôleur de média Lancer / Mettre en pause la lecture Arrêter la lecture L'argument « %1 » pour l'action « %2 » est absent ou d'un type incorrect. Le lecteur multimédia « %1 » ne peut pas effectuer l'action « %2 ». L'opération « %1 » est inconnue. Erreur inconnue. 