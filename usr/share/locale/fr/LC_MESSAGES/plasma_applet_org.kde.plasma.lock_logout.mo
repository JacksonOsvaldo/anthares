��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  -   5  =   c  	   �     �     �  /   �     �  
   �            3   )     ]     a  A   y     �     �     �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-09-06 11:35+0100
Last-Translator: Thomas Vergnaud <thomas.vergnaud@gmx.fr>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Voulez-vous passer en veille (mode veille) ? Voulez-vous passer en veille prolongée (mode hibernation) ? Général Actions Veille prolongée Veille prolongée (mettre en veille prolongée) Quitter Quitter... Verrouiller Verrouiller l'écran Déconnecter, éteindre ou redémarrer l'ordinateur Non Veille (mise en veille) Démarre une session parallèle en tant qu'utilisateur différent Passer en veille Changer d'utilisateur Changer d'utilisateur Oui 