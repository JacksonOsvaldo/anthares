��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    4   �  
   %  	   0     :  .   A     p          �  .   �  .   �  6   	  	   @     J  9   P     �                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-06 20:52+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Verrouiller l'écran lors de la sortie de veille : Activation Apparence Erreur Impossible de tester l'écran de verrouillage. Immédiatement Raccourci clavier : Verrouiller la session Verrouiller l'écran automatiquement après : Verrouiller l'écran à la sortie de la veille &Demander un mot de passe après un verrouillage de :  min  min  s  s Le raccourci de clavier global pour verrouiller l'écran. &Type de fond d'écran : 