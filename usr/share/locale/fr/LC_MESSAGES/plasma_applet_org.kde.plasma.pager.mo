��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  1   �     �  2         3     F     ]     w     �     �     �     �  	   �  
   �     �     �     �            "   ;  (   ^     �     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2017-07-03 17:57+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 fenêtre minimisée: %1 fenêtres minimisées: %1 fenêtre: %1 fenêtres: … et %1 autre fenêtre … et %1 autre fenêtres Nom de l'activité Numéro de l'activité Ajouter un bureau virtuel Configurer les bureaux… Nom du bureau Numéro du bureau Affichage : Ne fait rien Général Horizontal Icônes Disposition : aucun texte Uniquement l'écran courant Supprimer un bureau virtuel La sélection du bureau courant : Afficher le gestionnaire d'activités… Affiche le bureau Défaut Vertical 