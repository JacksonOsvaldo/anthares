��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     	  8   #	  >   \	  )   �	  )   �	  )   �	     
  
   "
     -
     5
     B
  `   R
  "   �
  \   �
     3     <     H  0   d     �     �  &   �     �     �     �     �     �       4     >   Q     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-07-03 18:48+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 100 % Auteur Copyright 2015 Harald Sitter Harald Sitter Aucune application ne lit actuellement du contenu sonore Aucune application n'enregistre actuellement du contenu sonore Aucun profil de périphérique disponible Aucun périphérique d'entrée disponible Aucun périphérique de sortie disponible Profil : PulseAudio Avancé Applications Périphériques Ajouter un périphérique de lecture virtuel pour lire simultanément sur toutes les cartes sons Configuration avancée des sorties Commuter automatiquement tous les flux en cours quand une nouvelle sortie devient disponible Capturer Par défaut Profils de périphérique : vpinon@kde.org, johan.claudebreuninger@gmail.com Entrées Couper le son Vincent Pinon, Johan Claude-Breuninger Sons de notification Sorties Lecture Port  (indisponible)  (débranché) Nécessite le module PulseAudio « module-gconf » Ce module permet de configurer le mécanisme de son PulseAudio %1: %2 100 % %1% 