��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �               .     B  	   S     ]     j  
   �  q   �       @         a     i  
   y  	   �     �     �     �     �     �     �     	  !   	     ?	     ]	     x	     �	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-14 17:32+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1@%2 %2@%3 (%1) Sélectionner… Réinitialiser l'icône Ajouter aux favoris Les Applications Apparence Applications Applications mises à jour. Ordinateur Déplacer les onglets entre les boîtes pour les afficher ou les cacher, ou changer l'ordre des onglets visibles. Modifier les applications… Étendre la recherche aux signets, aux fichiers et aux courriels Favoris Onglets cachés Historique Icône : Quitter Boutons de menu Utilisé couramment Sur toutes les activités Sur l'activité courante Supprimer des favoris Afficher dans les favoris Afficher les applications par nom Trier par ordre alphabétique Changer d'onglet au survol Tapez pour rechercher… Onglets visibles 