��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \     ;  J   [  #   �     �  !   �  -   	  <   7     t  *   �     �  
   �     �  .   �  ,   %	  �   R	     1
  r   E
  +   �
  �   �
  S   {     �                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-06 20:39+0800
Last-Translator: Simon Depiets <sdepiets@gmail.com>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Appliquer un paquet d'apparence Outil en ligne de commande permettant d'appliquer les paquets d'apparence. Configurer les détails d'apparence Copyright 2017, Marco Martin Paramètres de curseurs modifiés Télécharger de nouveaux paquets d'apparence maxence.ledore@gmail.com, vpinon@kde.org, sdepiets@gmail.com Obtenir d'autres apparences… Lister les paquets d'apparence disponibles Outil de gestion de l'apparence Mainteneur Marco Martin Maxence Le Doré, Vincent Pinon, Simon Depiets Réinitialiser l'agencement du bureau Plasma Sélectionner un thème global pour votre espace de travail (dont le thème plasma, le schéma de couleurs, le pointeur de souris, le sélecteur de fenêtre et de bureau, l'écran d'accueil, l'écran de verrouillage, etc.) Afficher un aperçu Ce module vous permet de configurer l'apparence de tout votre espace de travail avec des préréglages tout faits. Utiliser la disposition de bureau du thème Avertissement : votre disposition de bureau Plasma sera perdue et ré-initialisée à la disposition par défaut fournie par le thème sélectionné. Vous devez redémarrer KDE pour que les changements sur les curseurs prennent effet nom_du_paquet 