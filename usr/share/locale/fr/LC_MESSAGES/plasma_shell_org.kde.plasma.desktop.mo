��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
  
   i     t      �  $   �     �  !   �     �  	             (  	   =     G     ^     l     p     x     �     �     �  
   �     �     �     �     �  	   �     �       )        H     P  
   g     r  Y   �  c   �     D     R     Y  
   g  %   r     �     �     �     �     �     �     �          4     ;     H     Y     g     k     �     �  h   �  �   	     �     �     �  $   �     �               *     C     Y     a      �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-14 17:56+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Activités Ajouter une action Ajouter un élément séparateur Ajouter des composants graphiques... Alt Composants graphiques alternatifs Toujours visible Appliquer Appliquer les paramètres Appliquer maintenant Auteur : Cacher automatiquement Bouton retour Bas Annuler Catégories Centre Fermer + Configurer Configurer l'activité Créer une activité... Ctrl En train d'être utilisé Supprimer Adresse électronique : Bouton avance Obtenir de nouveaux composants graphiques Hauteur Défilement horizontal Saisir ici Raccourcis claviers Impossible de changer la disposition tant que les composants graphiques sont verrouillés Les changements de disposition doivent être appliqués avant de pouvoir faire d'autres changements Disposition : Gauche Bouton gauche Licence : Verrouiller les composants graphiques Maximiser le tableau de bord Méta Bouton central Plus de paramètres... Actions de la souris Ok Alignement du tableau de bord Supprimer le tableau de bord Droite Bouton droit Bord de l'écran Rechercher... Maj Arrêter l'activité Activités arrêtées : Changer Les paramètres du module actuel ont changé. Voulez-vous appliquer les changements ou les abandonner ? Ce raccourci va activer l'applet : Cela placera le focus sur celui-ci, et, si l'applet possède un popup (comme le menu de démarrage), le popup sera ouvert. Haut Annuler la désinstallation Désinstaller Désinstaller le composant graphique Défilement vertical Visibilité Fond d'écran Type de fond d'écran : Composants graphiques Largeur Les fenêtres peuvent recouvrir Les fenêtres se placent dessous 