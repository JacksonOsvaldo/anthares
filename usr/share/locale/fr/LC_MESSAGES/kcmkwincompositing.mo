��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  Y     �   j  x   �     o  4   x     �     �  �   �     �     �     �  	   �  	         
       )        A     W     l  %   t  $   �  N   �  %     "   4     W  w   u     �  #   �  (     �   G            '   #     K  
   j  
   u     �     �  �  �  '     #   @     d  �   w  !   +  
   M     X     e  .   ~     �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-15 09:04+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 « Rafraîchissement de tout l'écran » peut provoquer des problèmes de performances. « Seulement lorsque peu coûteux » prévient le déchirement uniquement pour les changements de l'écran complet comme dans une vidéo. « Ré-utiliser le contenu de l'écran » peut provoquer de sérieux problèmes de performances avec les pilotes MESA. Précise Permettre aux applications de bloquer la composition Toujours Vitesse d'animation : Les applications peuvent utiliser une astuce pour empêcher la composition quand la fenêtre est ouvert.
Ceci améliore les performances par exemple dans les jeux.
Ce réglage peut être forcé par des règles spécifiques à chaque fenêtre. Auteur : %1
Licence : %2 Automatique Accessibilité Apparence Ornements Focus Outils Animation de changement de bureau virtuel Gestion des fenêtres Configurer le filtre Directe bruno.patri@gmail.com, vpinon@kde.org Activer le compositeur au démarrage Exclure les effets de bureau qui ne sont pas pris en charge par le compositeur Exclure les effets de bureau internes Rafraîchissement de tout l'écran Obtenir de nouveaux effets... Remarque : pour trouver ou pour savoir comment configurer un effet, veuillez regarder dans les paramètres de l'effet. Ultra-rapide L'équipe de développement de KWin Conserver les vignettes des fenêtres : Conserver les vignettes des fenêtres interfère toujours avec l'état minimisé des fenêtres. Ceci peut mener les fenêtres à ne pas arrêter leur travail même à l'état minimisé. Bruno Patri, Vincent Pinon Jamais Seulement pour les fenêtres affichées Seulement lorsque peu coûteux OpenGL 2.0 OpenGL 3.1 EGL GLX Le compositeur OpenGL (par défaut) a rendu KWin instable par le passé.
Ceci était très probablement dû à un bogue dans le pilote graphique.
Si vous pensez avoir entre temps effectué une mise à jour vers un pilote stable,
vous pouvez réinitialiser cette protection, mais soyez conscient que cela peut aboutir à un plantage immédiat !
Sinon, vous pouvez utiliser plutôt le moteur Xrender. Activer à nouveau la détection OpenGL Ré-utiliser le contenu de l'écran Moteur de rendu : La méthode de mise à l'échelle « précise » n'est pas prise en charge par tous les matériels et peut provoquer des dégradations de performances et des artefacts de rendu. Méthode de mise à l'échelle : Rechercher Avec lissage Avec lissage (plus lent) Prévention de déchirement (« vsync ») : Très lente XRender 