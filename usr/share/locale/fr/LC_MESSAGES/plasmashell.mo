��    &      L  5   |      P     Q     p  	   ~  0   �  .   �     �  7         ?     `     i  \   �  t   �  9   Z  -   �  $   �     �  [   �     P  E   o  G   �  >   �     <     C     Z     g     �     �     
     )  <   .  :   k  s   �  o   	  .   �	  ,   �	  .   �	  ,   
  �  B
  /        M     [     n     �  :   �  ?   �          (  #   0  v   T  r   �  "   >     a  8   ~     �  c   �     -     =     E     W     d     k     �  �   �     >     Q  0   o     �     �     �     �     �          '     =     N                 &   %                     !   	      
                       #       "                     $                                                                             Activate Task Manager Entry %1 Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Enable QML Javascript debugger Enables test mode and specifies the layout javascript file to set up the testing environment Fatal error message bodyAll shell packages missing.
This is an installation issue, please contact your distribution Fatal error message bodyShell package %1 cannot be found Fatal error message titlePlasma Cannot Start Force loading the given shell plugin Hide Desktop Load plasmashell as a standalone application, needs the shell-plugin option to be specified NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Plasma is unable to start as it could not correctly use OpenGL 2.
 Please check that your graphic drivers are set up correctly. Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2017-07-03 21:07+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Activer l'entrée du gestionnaire de tâches %1 Activités… Ajouter un panneau Bluetooth désactivé Bluetooth activé Configurer le module externe pour les actions de la souris Ne pas relancer plasma-shell automatiquement après un plantage thomas.vergnaud@gmx.fr %1 vide Activer le débugger JavaScript QML Active le mode de test et spécifie le fichier Javascript de disposition pour la mise en place l'environnement de test Un paquet du Shell est manquant.
Il s'agit d'un problème d'installation ; veuillez contacter votre distribution. Le paquet shell %1 est introuvable Plasma ne peut pas démarrer Forcer le chargement d'un module externe donné du shell Masquer le bureau Charger plasmashell comme une application autonome ; l'option « shell-plugin » est nécessaire Thomas Vergnaud %1 muet Microphone coupé Audio coupé Plasma Plasma ne peut pas démarrer Shell Plasma Plasma ne peut pas démarrer car il ne peut pas utiliser OpenGL 2 correctement.
Veuillez vous assurer que les pilotes de carte graphique sont configurés correctement. Afficher le bureau Arrêter l'activité actuelle Impossible de charger le fichier de script : %1 fichier Internet mobile désactivé Internet mobile activé Clavier visuel désactivé Clavier visuel activé Pave tactile désactivé Pavé tactile activé Wifi désactivé Wifi activé 