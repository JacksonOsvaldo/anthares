��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  '     =   �  *   �  )     8   :  )   s  -   �  @   �     	     !	  "   .	  7   Q	      �	  J   �	  A   �	     7
  `   R
  %   �
  r   �
  ;   L     �     �  ?   �  W   �                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-07-03 17:51+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Impossible de lancer « Initramfs ». Impossible de lancer la commande « update-alternatives ». Configurer l'écran de démarrage Plymouth Obtenir de nouveaux écrans de démarrage geoffray.levasseurbrandin@numericable.fr, kde@macolu.org Obtenir de nouveaux écrans de démarrage La lancement de « Initramfs » a échoué. « Initramfs » s'est terminé avec une condition d'erreur %1. Installer un thème. Marco Martin Geoffray Levasseur, Matthieu Robin Aucun thème spécifié dans les paramètres du module. Installateur de thèmes Plymouth Sélectionner de manière globale un écran de démarrage pour le système Le thème à installer, doit être un fichier d'archive existant. Le thème %1 n'existe pas. Thème corrompu : impossible de trouver le fichier « .plymouth » à l'intérieur du thème. Le dossier de thème %1 n'existe pas. Ce module vous permet de configurer l'apparence de tout votre espace de travail avec des préréglages tout faits. Impossible d'authentifier / d'exécuter l'action : %1, %2 Désinstaller Désinstaller un thème. Échec du lancement de la commande « update-alternatives ». La commande « update-alternatives » s'est terminée avec une condition d'erreur %1. 