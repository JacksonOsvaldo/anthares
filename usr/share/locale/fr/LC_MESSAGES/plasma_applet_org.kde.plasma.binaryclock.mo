��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �  	   t     ~     �     �      �  ;   �  2     =   C                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-06-28 18:19+0100
Last-Translator: Vincent Pinon <vpinon@kde.org>
Language-Team: French <kde-francophone@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Apparence Couleurs : Afficher les secondes Afficher la grille Afficher les diodes inactives : Utiliser une couleur personnalisée pour les diodes actives Utiliser une couleur personnalisée pour la grille Utiliser une couleur personnalisée pour les diodes inactives 