��    3      �  G   L      h  #   i     �  J   �     �     �          "     7     I     U     \     c     {  	   �     �     �      �     �     �  '   �       !   (     J     X     j  
   �     �  	   �     �     �     �     �     �     �               +     =     R     j     y     �     �     �  	   �     �     �     �  0   �  3   -    a     e
     �
  G   �
     �
     �
           ,  
   :     E     R     Y     `     w  	   �     �     �     �     �     �  )   �       #   *     N     _  #   z     �     �     �     �     �  
   �     �     �                     9     R     d     �  *   �     �     �     �     �     �       
     %      &   F            '              "                /   1       $                .   !                    *   	       &             (                                ,   %         3   2          
       +   #   )          -                             0              &Enter a name for the color scheme: (c) 2007 Matthew Woehlke A color scheme with that name already exists.
Do you want to overwrite it? Active Text Active Titlebar Active Titlebar Text Alternate Background Button Background Button Text Color: Colors Colorset to view/modify Contrast Contrast: Disabled color Disabled contrast type EMAIL OF TRANSLATORSYour emails Error Focus Decoration Get new color schemes from the Internet Import Color Scheme Import a color scheme from a file Inactive Text Inactive Titlebar Inactive Titlebar Text Intensity: Jeremy Whiting Link Text Matthew Woehlke NAME OF TRANSLATORSYour names New Row Normal Background Normal Text Options Remove Scheme Remove the selected scheme Save Color Scheme Selection Background Selection Inactive Text Selection Text Shade sorted column &in lists Tooltip Background Tooltip Text View Background View Text Visited Text Window Background Window Text You do not have permission to delete that scheme You do not have permission to overwrite that scheme Project-Id-Version: kcmcolors
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2008-11-04 17:27+0200
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <i18n@mova.org>
Language: be@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Upišy nazvu schiemy koleraŭ: © 2007 Matthew Woehlke Schiema koleraŭ z takoj nazvaju ŭžo jość.
Chočaš jaje zamianić? Dziejny tekst Dziejny zahałovak akna Tekst dziejnaha zahałoŭka akna Dadatkovy fon Fon knopki Tekst knopki Koler: Kolery Patrebny zbor koleraŭ Kantrast Kantrast: Vyklučany koler Vyklučany typ kantrastu ihar.hrachyshka@gmail.com Pamyłka Azdoba fokusu Vaźmi novyja schiemy koleraŭ ź sieciva Zimpartuj schiemu koleraŭ Zimpartuj schiemu koleraŭ z fajła Niadziejny tekst Niadziejny zahałovak akna Tekst niadziejnaha zahałoŭka akna Nasyčanaść: Jeremy Whiting Łuč Matthew Woehlke Ihar Hračyška Novy radok Zvyčajny fon Zvyčajny tekst Opcyi Vydali schiemu Vydali vybranuju schiemu Zapišy schiemu koleraŭ Fon vyłučeńnia Niadziejny tekst vyłučeńnia Tekst vyłučeńnia Zacianiaj uparadkavany słupok &u śpisach Fon padkazki Tekst padkazki Fon prahladu Tekst prahladu Vykarystany łuč Fon akna Tekst akna Tabie nielha vydalić hetuju schiemu. Tabie nielha zamianić hetuju schiemu. 