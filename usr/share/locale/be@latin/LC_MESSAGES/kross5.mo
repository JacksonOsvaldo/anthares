��            )   �      �  &   �     �     �     �      �     �                  ,   ;  3   h     �     �     �     �     �  '   �          ;     A     W     p     w     �     �     �  &   �     �    �     �     �       	             4     <  
   T     _  ;   w  H   �  <   �  2   9     l     s     z  ,   �     �     �     �  &   �     	     	     1	  $   9	     ^	  $   d	     �	                                       	                                              
                                                     @title:group Script propertiesGeneral Add a new script. Add... Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2008-08-30 01:10+0300
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <i18n@mova.org>
Language: be@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: KBabel 1.11.4
 Hałoŭnaje Dadaj novy skrypt. Dadaj... Kamentar: ihar.hrachyshka@gmail.com Źmiani Redahuj vybrany skrypt. Redahuj... Vykanaj vybrany skrypt. Nie ŭdałosia stvaryć skrypt dla interpretatara „%1”. Nie ŭdałosia vyznačyć interpretatar dla skryptavaha fajła „%1”. Nie ŭdałosia źmiaścić interpretatar „%1” u pamiać. Nie ŭdałosia adčynić skryptavy fajł „%1”. Fajł: Ikona: Interpretatar: Rovień biaśpiečnaści interpretatara Ruby Ihar Hračyška Nazva: Niama takoj funkcyi: „%1”. Niama takoha interpretatara: „%1”. Vydali Vydali vybrany skrypt. Vykanaj Skryptavy fajł „%1” nie isnuje. Spyni Spyni vykanańnie vybranaha skrypta. Tekst: 