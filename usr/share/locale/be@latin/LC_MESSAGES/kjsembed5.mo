��    '      T  5   �      `  *   a     �     �     �     �     �            9   )     c     k      �     �     �     �  ;   �  -   1  )   _     �     �     �     �     �       !        @     _     w     �     �     �     �     �       (   #     L  &   _  &   �    �  9   �	      �	  .   
  .   :
  0   i
  =   �
     �
     �
  D   �
     A  (   M  &   v  (   �  ,   �  )   �  M     @   k  >   �  3   �  @     /   `  /   �  4   �     �  0     $   E     j     �  !   �     �     �     �          :  9   Q     �  7   �  8   �                 
              '                                         	   #      !       $   &                                             "            %                             %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Alert Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Error encountered while processing include '%1' line %2: %3 Exception calling '%1' function from %2:%3:%4 Exception calling '%1' slot from %2:%3:%4 Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. There was an error reading the file '%1' Wrong object type. include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2008-08-30 01:10+0300
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <i18n@mova.org>
Language: be@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: KBabel 1.11.4
 %1 nia jość funkcyjaj, jakuju možna było b vyklikać. %1 nia jość typu „Object”. „%1” nia jość sapraŭdnym „QLayout”. „%1” nia jość sapraŭdnym „QWidget”. Dziejańnie („Action”) pryjmaje 2 arhumenty. Hrupa dziejańniaŭ („ActionGroup”) pryjmaje 2 arhumenty. Tryvoha Niaŭdały vyklik „%1”. Niaŭdały vyklik „%1”. Nie ŭdałosia atrymać arhumenty %2: %3 Paćvierdź Nie ŭdałosia skanstrujavać vartaść. Nie ŭdałosia stvaryć časovy fajł. Nie ŭdałosia adčynić fajł „%1”. Nie ŭdałosia adčynić fajł „%1”: %2. Nie ŭdałosia pračytać fajł „%1”. Uźnikła pamyłka padčas razrableńnia zahałoŭka „%1” u radku %2: %3. Praz vyklik funkcyi „%1” z %2:%3:%4 uźnikła vyklučeńnie. Praz vyklik slota „%1” z %2:%3:%4 uźnikła vyklučeńnie. Nie ŭdałosia stvaryć dziejańnie („Action”). Nie ŭdałosia stvaryć hrupu dziejańniaŭ („ActionGroup”). Nie ŭdałosia stvaryć vykład („Layout”). Nie ŭdałosia stvaryć widžet („Widget”). Nie ŭdałosia źmiaścić u pamiać fajł „%1”. Fajł „%1” nia znojdzieny. Pieršy arhument musić być typu „QObject”. Niapravilnaja kolkaść arhumentaŭ. Treba padać nazvu fajła. Treba padać nazvu vykładu. Treba padać sapraŭdnaha prodka. Treba padać nazvu widžeta. Nazva klasy nie aznačanaja. Nazva klasy nie aznačanaja. Niama takoha metadu: „%1”. Nie staje arhumentaŭ. Padčas pračytańnia fajła „%1” uźnikła pamyłka. Niapravilny typ abjekta. Zahałovak (include) pryjmaje adzin arhument, a nie %1. Biblijateka (library) pryjmaje adzin arhument, a nie %1. 