��    H      \  a   �            !  	   .     8     E     S     Y     `     q     w     �     �  	   �     �     �  
   �     �     �  
   �     �     �     �  	          
             +     :     @     O     \     b     f  
   o     z  
   �     �     �     �     �  	   �      �          !     8  
   J     U     d     x     �  
   �     �     �     �     �     �     �     �     	     	  C   $	     h	     x	     �	     �	     �	  	   �	     �	     �	     �	     �	     
    
          -     5     H     Y     `     i  	   y     �     �     �     �     �     �     �               #     8     D     K     c     p     x     �     �     �     �     �     �     �     �     �  
   �            #   ,  !   P     r     y     �     �     �     �     �     �          -     L     \     j          �     �     �     �     �     �     �  8   �     8     M     h     ~     �     �     �  	   �     �  	   �  
   �             D   ?   8   .          -   @         F          "   !   A                  3   C      #       2                  :         9   =   7             (                   /           ,   >   )             H              0   G       ;       
                E       5   	   &      %   6          $       *   <                 1   '      4      B   +       %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Find... &First Page &Fit to Page &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... About &KDE C&lear Clear List Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Cu&t Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width NAME OF TRANSLATORSYour names No Entries Open &Recent Print Previe&w Quit application Re&do Re&vert Save &As... Select &All Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Switch Application &Language... Tip of the &Day What's &This? Zoom &In Zoom &Out go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2008-08-30 01:10+0300
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <i18n@mova.org>
Language: be@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: KBabel 1.11.4
 &Padručnik pa %1 &Pra %1 &Sapraŭdny pamier &Dadaj zakładku &Nazad &Začyni &Naładź %1... &Skapijuj &Znajdzi... &Pieršaja staronka &Układzi ŭ staronku &Pierajdzi... &Pierajdzi da radka... &Pierajdzi da staronki... &Apošniaja staronka &Vyšli list... &Novy &Nastupnaja staronka &Adčyni... &Uklej &Papiaredniaja staronka &Vydrukuj... &Vyjdzi &Pakažy znoŭ &Zamiani... &Paviedam pra chibu... &Zapišy &Zapišy nałady &Pravapis... &Anuluj &Vyšej &Maštabuj... Pra &KDE &Vyčyści Vyčyści śpis Naładź &nahadvańni... Naładź &klavijaturnyja skaroty... Naładź &paneli pryładździa... &Vytni &Anuluj vyłučeńnie ihar.hrachyshka@gmail.com Aŭtamatyčna vyznačanaje Zmoŭčanaje &Na ŭvieś ekran Znajdzi &nastupnaje Znajdzi &papiaredniaje Układzi ŭ &vyšyniu staronki Układzi ŭ &šyryniu staronki Ihar Hračyška Ničoha niama Adčyni &niadaŭnaje &Vydrukuj padhlad Vyjdzi z aplikacyi &Uznavi &Viarni Zapišy &jak... Vyłučy &ŭsio Pakažy &menu Pakažy &panel pryładździa Pakažy menu<p>Pakazvaje menu, kali jano schavanaje.</p> Pakažy panel &stanu Źmiani &movu aplikacyi... Štodzionnyja &parady Što &heta? &Nabliź &Addali &Nazad Na&pierad &Dadomu &Dapamoha biaz nazvy 