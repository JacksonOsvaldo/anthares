��    \      �     �      �  C   �  /     -   M     {     �     �     �     �     �     	     	     	  
   #	     .	     7	     @	     S	  	   d	     n	     �	     �	  ,   �	  	   �	     �	  
   �	     
     
     0
     E
     X
     g
     w
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
  	             -  
   B  
   M     X     l     s  
   �     �     �     �     �     �     �     �     �               .     N     [     b  	   q     {     �     �     �     �     �     �          1     F     \  	   b     l  ,   �     �     �     �     �     �     �     �            #   4     X  �  `  #   �     #  )   5  =   _  *   �  J   �  b         v     �     �  #   �     �     �               &     F     b  ,   q  ,   �     �  z   �     d  F   w     �  1   �  2     /   B  (   r  #   �  &   �  <   �     #     0  
   8     C  #   W     {     �     �     �      �     �  E     A   N  +   �     �  E   �       #   ,     P     c  %   |     �     �  /   �  1        D  /   d  -   �  "   �  ^   �  '   D     l  -        �     �     �  7   �  )   +  V   U  P   �  T   �  6   R  0   �  4   �     �       )     l   B     �  "   �  $   �          &  '   5  ,   ]  7   �  B   �  [     !   a         N         V   3           %               G   !                  *       >   S   2   X       ?   (   
       R   @   Z   [       $   \         '   <       B   H      -                  =      ,   L   F       O   6   4   T   0   Y   ;   U   C   #       K   A       Q   E   J          1   "         7             +          P          D           9   8                    &   :   I          M         5          .       /                             )            	   W       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-08 16:23+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Διαχείριση του '%1'... Επιλογή... Καθαρισμός εικονιδίου Προσθήκη στην επιφάνεια εργασίας Προσθήκη στα αγαπημένα Προσθήκη στον πίνακα (γραφικό συστατικό) Στοίχιση των αποτελεσμάτων αναζήτησης στο κάτω μέρος Όλες οι εφαρμογές %1 (%2) Εφαρμογές Εφαρμογές & έγγραφα Συμπεριφορά Κατηγορίες Υπολογιστής Επαφές Περιγραφή (όνομα) Μόνο περιγραφή Έγγραφα Επεξεργασία εφαρμογής... Επεξεργασία εφαρμογών... Τέλος συνεδρίας Επέκταση της αναζήτησης σε σελιδοδείκτες, αρχεία και αλληλογραφία Αγαπημένα Εμφάνιση του μενού σε ένα μόνο επίπεδο Να ξεχαστούν όλα Αγνόηση όλων των εφαρμογών Να ξεχαστούν όλες οι επαφές Αγνόηση όλων των εγγράφων Αγνόηση της εφαρμογής Να ξεχαστεί η επαφή Αγνόησή του εγγράφου Να ξεχαστούν τα πρόσφατα έγγραφα Γενικά %1 (%2) Νάρκη Απόκρυψη %1 Απόκρυψη εφαρμογής Εικονίδιο: Κλείδωμα Κλείδωμα οθόνης Αποσύνδεση Όνομα  (Περιγραφή) Μόνο όνομα Εφαρμογές που χρησιμοποιούνται συχνά Έγγραφα που χρησιμοποιούνται συχνά Χρησιμοποιούνται συχνά Άνοιγμα με: Καρφίτσωμα στον διαχειριστή εργασιών Τοποθεσίες Ενέργεια / Συνεδρία Ιδιότητες Επανεκκίνηση Πρόσφατες εφαρμογές Πρόσφατες επαφές Πρόσφατα έγγραφα Χρησιμοποιημένα πρόσφατα Χρησιμοποιήθηκαν πρόσφατα Αφαιρούμενο μέσο Αφαίρεση από τα αγαπημένα Επανεκκίνηση υπολογιστή Εκτέλεση εντολής... Εκτέλεση μιας εντολής ή ενός ερωτήματος αναζήτησης Αποθήκευση συνεδρίας Αναζήτηση Αποτελέσματα αναζήτησης Αναζήτηση... Αναζήτηση για '%1' Συνεδρία Εμφάνιση πληροφοριών επαφής... Εμφάνιση εφαρμογών ως: Εμφάνιση εφαρμογών που χρησιμοποιούνται συχνά Εμφάνιση επαφών που χρησιμοποιούνται συχνά Εμφάνιση εγγράφων που χρησιμοποιούνται συχνά Εμφάνιση πρόσφατων εφαρμογών Εμφάνιση πρόσφατων επαφών Εμφάνιση πρόσφατων εγγράφων Εμφάνιση: Τερματισμός Ταξινόμηση αλφαβητικά Έναρξη μιας παράλληλης συνεδρίας σαν διαφορετικός χρήστης Αναστολή Αναστολή στη μνήμη Αναστολή στον δίσκο Εναλλαγή χρήστη Σύστημα Ενέργειες συστήματος Κλείσιμο του υπολογιστή Πληκτρολογήστε για αναζήτηση. Αναίρεση απόκρυψης εφαρμογών στο '%1' Αναίρεση απόκρυψης εφαρμογών σε αυτό το υπο-μενού Γραφικά συστατικά 