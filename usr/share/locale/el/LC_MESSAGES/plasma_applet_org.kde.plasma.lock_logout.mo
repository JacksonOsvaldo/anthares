��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  B   �  L   2          �  
   �  /   �     �     �       $     ^   9     �  3   �  l   �     @     Q     o     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-11-18 16:29+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Επιθυμείτε αναστολή στη RAM (κοίμηση); Επιθυμείτε την αναστολή στο δίσκο (νάρκη); Γενικά Ενέργειες Νάρκη Νάρκη (αναστολή στο δίσκο) Αποχώρηση Αποχώρηση... Κλείδωμα Κλείδωμα της οθόνης Αποσύνδεση, κλείσιμο ή επανεκκίνηση του υπολογιστή Όχι Κοίμηση (αναστολή στη μνήμη) Έναρξη μιας παράλληλης συνεδρίας σαν διαφορετικός χρήστης Αναστολή Εναλλαγή χρήστη Εναλλαγή χρήστη Ναι 