��          �   %   �      P     Q  
   W  /   b  -   �     �     �  
   �     �     �  W        [  ,   p  	   �     �     �     �     �     �  
   �     �     �          #  1   8     j  �  w       
        '  )   9  *   c      �     �     �     �  �   �  ,   �  z   �     U  #   h     �     �     �     �  +   �  /   	  7   4	  )   l	  E   �	  9   �	     
                                                                                               	          
                    %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used Remove from Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-05-22 17:24+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Επιλογή... Καθαρισμός εικονιδίου Προσθήκη στα αγαπημένα Όλες οι εφαρμογές Εμφάνιση Εφαρμογές Υπολογιστής Σύρετε τις καρτέλες μεταξύ των κουτιών για να τις εμφανίσετε/κρύψετε, ή αναδιατάξτε τις ορατές με σύρσιμο. Επεξεργασία εφαρμογών... Επέκταση της αναζήτησης σε σελιδοδείκτες, αρχεία και αλληλογραφία Αγαπημένα Κρυμμένες καρτέλες Ιστορικό Εικονίδιο: Έξοδος Κουμπιά μενού Χρησιμοποιούνται συχνά Αφαίρεση από τα αγαπημένα Εμφάνιση εφαρμογών κατά όνομα Ταξινόμηση αλφαβητικά Εναλλαγή καρτελών με πέρασμα από πάνω Πληκτρολογήστε για αναζήτηση... Ορατές καρτέλες 