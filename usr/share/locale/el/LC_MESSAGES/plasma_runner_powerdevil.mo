��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �  9     (   �  1  �  �        �  
   	  !   	     /	     >	     O	     a	     s	  $   �	  -   �	  "   �	  "   
  0   +
  7   \
                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2013-04-07 11:13+0200
Last-Translator: Stelios <sstavra@gmail.com>
Language-Team: Greek <kde-i18n-doc@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Σβήσιμο κατά το μισό της οθόνης Πλήρες σβήσιμο οθόνης Εμφανίζει τις επιλογές λαμπρότητας οθόνης ή ορίζει τη λαμπρότητα καθορισμένη ως :q:; π.χ. η λαμπρότητα οθόνης 50 θα φωτίσει την οθόνη στο 50% της μέγιστης φωτεινότητάς της Εμφανίζει όλες τις επιλογές αναστολής του συστήματος (π.χ. αδράνεια, αναστολή) και επιτρέπει την ενεργοποίησή τους σβήσιμο οθόνης νάρκη λαμπρότητα οθόνης κοίμηση αναστολή στο δίσκο στη μνήμη σβήσιμο οθόνης %1 λαμπρότητα οθόνης %1 Ρύθμιση λαμπρότητας σε %1 Αναστολή στο δίσκο Αναστολή στη μνήμη Αναστολή συστήματος στη RAM Αναστολή συστήματος στο δίσκο 