��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  :   �	     �	     �	     
  #   $
     H
     W
     h
     l
     �
     �
     �
  '   �
  >   �
     (     =  !   ]  .        �      �     �      �         #   A     e     �     �     �  %   �     �     �                /     P  !   a     �     �     �  �   �     M  J   m     �     �     �  <   �                    #     8     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-09-16 13:58+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Κατανάλωση ενέργειας εφαρμογής Μπαταρία Χωρητικότητα Ποσοστό φόρτισης Κατάσταση φόρτισης Φόρτιση Τρέχουσα °C Λεπτομέρειες: %1 Αποφόρτιση dimkard@gmail.com Ενέργεια Κατανάλωση ενέργειας Στατιστικά κατανάλωσης ενέργειας Περιβάλλον Πλήρης σχεδιαστή Πλήρως φορτισμένη Υπάρχει παροχή ενέργειας Kai Uwe Broulik Τελευταίες 12 ώρες Τελευταίες 2 ώρες Τελευταίες 24 ώρες Τελευταίες 48 ώρες Τελευταίες 7 ημέρες Τελευταία πλήρης Τελευταία ώρα Κατασκευαστής Μοντέλο Δημήτρης Καρδαράκος Όχι Εκτός φόρτισης PID: %1 Διαδρομή: %1 Επαναφορτιζόμενη Ανανέωση Σειριακός αριθμός W Σύστημα Θερμοκρασία Αυτού του τύπου η ιστορικότητα δεν είναι διαθέσιμη αυτήν τη στιγμή για αυτήν τη συσκευή. Χρονικό διάστημα Χρονικό διάστημα για εμφάνιση δεδομένων Προμηθευτής V Τάση Ξυπνήματα ανά δευτερόλεπτο: %1 (%2%) W Wh Ναι Κατανάλωση % 