��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  �   7  I   �  (     '   E  :   m  $   �  3   �  !        #  L   *     w  6   �  B   �       7   "  
   Z  +   e     �  r   �  H     E   e     �  [   �       '   1  Q   Y  Z   �  !     !   (     J     W  -   w  &   �     �  6   �  4     +   O  !   {  �  �  n   _  9   �       \        q  .   �  8   �  (   �  2   "  O   U     �  I   �  B        E     J  
   W  5   b     �     �  
   �     �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-11-23 16:27+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 

Επιλογή προηγούμενου αποκόμματος για να πάει στο τελευταίο αποθηκευμένο απόκομμα. &Δημιουργία αρχειοθήκης βιβλίου κόμικ... &Αποθήκευση κόμικ ως... &Αριθμός αποκόμματος: *.cbz|Αρχειοθήκη βιβλίου κόμικ (Zip) &Πραγματικό μέγεθος Αποθήκευση τρέχουσας &θέσης Για προχωρημένους Όλα Συνέβη ένα σφάλμα για το αναγνωριστικό  %1. Εμφάνιση Αποτυχία αρχειοθέτησης κόμικ Αυτόματη ενημέρωση πρόσθετων κόμικ: Λανθάνουσα μνήμη Έλεγχος για νέες σειρές κόμικ: Κόμικ Λανθάνουσα μνήμη κόμικ: Διαμόρφωση... Αδυναμία δημιουργίας αρχειοθήκης στην καθορισμένη τοποθεσία. Δημιουργία %1 αρχειοθήκης βιβλίου κόμικ Δημιουργία αρχειοθήκης βιβλίου κόμικ Προορισμός: Εμφάνιση σφάλματος όταν αποτύχει η λήψη του κόμικ Λήψη κόμικ Διαχείριση σφαλμάτων Αποτυχία προσθήκης αρχείου στην αρχειοθήκη. Αποτυχία δημιουργίας αρχείου με αναγνωριστικό %1. Από την αρχή έως ... Από το τέλος έως ... Γενικά Λήψη νέων κόμικ... Απέτυχε η λήψη του κόμικ: Μετάβαση σε απόκομμα Πληροφορίες &Μετάβαση στο τρέχον απόκομμα &Μετάβαση στο πρώτο απόκομμα Μετάβαση στο απόκομμα... Χειροκίνητο εύρος Ενδέχεται να μην υπάρχει σύνδεση στο Internet.
Ενδέχεται το πρόσθετο 'κόμικ' να είναι κατεστραμμένο.
Ένας ακόμα λόγος ενδέχεται να είναι ότι δεν υπάρχει κόμικ γι'αυτή τη μέρα/αριθμό/συμβολοσειρά, οπότε ίσως το πρόβλημα διορθωθεί αν ξαναπροσπαθήσετε.  Εμφάνιση σε κανονικό μέγεθος κάνοντας μεσαίο κλικ στο κόμικ Δεν υπάρχει αρχείο zip, ματαίωση. Εύρος: Εμφάνιση βελών μόνο όταν το ποντίκι είναι από πάνω Εμφάνιση URL κόμικ Εμφάνιση συγγραφέα κόμικ Εμφάνιση αναγνωριστικού κόμικ Εμφάνιση τίτλου κόμικ Αναγνωριστικό αποκόμματος: Το εύρος των σειρών κόμικ για αρχειοθέτηση. Ενημέρωση Επισκεφτείτε το δικτυακό χώρο του κόμικ Επισκεφθείτε το &δικτυακό κατάστημα # %1 ημέρες dd.MM.yyyy &Επόμενη καρτέλα με νέα σειρά Από: Έως: λεπτά  σειρές ανά κόμικ 