��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �  &   �  
     E     A   W     �  
   �     �  F   �  h     C   �  F   �  S   	     e	     |	     �	  k  �
  
       %  _   E     �                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-07-29 19:49+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  λεπ Λειτουργία όπως όταν Πάντα Καθορισμός μιας ειδικής συμπεριφοράς Να μη γίνει χρήση ειδικών ρυθμίσεων capoiosct@gmail.com Νάρκη Αντώνης Γέραλης Να μη γίνεται ποτέ κλείσιμο της οθόνης Να μη γίνεται ποτέ αναστολή ή τερματισμός του υπολογιστή Το σύστημα τροφοδοτείται από ρεύμα AC Το σύστημα τροφοδοτείται από μπαταρία Το σύστημα τροφοδοτείται από χαμηλή μπαταρία Τερματισμός Αναστολή Η υπηρεσία διαχείρισης ενέργειας δεν φαίνεται να εκτελείται.
Αυτό μπορεί να επιλυθεί με την έναρξη ή τον προγραμματισμό της από το "Εκκίνηση και τερματισμός" Η υπηρεσία δραστηριοτήτων δεν εκτελείται.
Είναι απαραίτητο να εκτελείται ο διαχειριστής δραστηριοτήτων για να είναι δυνατή η διαμόρφωση της ανά-δραστηριότητας συμπεριφοράς διαχείρισης ενέργειας. Η υπηρεσία δραστηριοτήτων εκτελείται με στοιχειώδεις λειτουργίες.
Τα ονόματα και τα εικονίδια των δραστηριοτήτων μπορεί να μην είναι διαθέσιμα. Δραστηριότητα "%1" Χρήση ξεχωριστών ρυθμίσεων (μόνο για προχωρημένους) μετά από 