��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  _   �  _   <  x   �  E     N   [  ]   �  8        A  [   ]         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-08-05 18:04+0200
Last-Translator: Dimitrios Glentadakis <dglent@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Δεν ήταν δυνατή η ανίχνευση του τύπου mime του αρχείου Αδυναμία εύρεσης όλων των απαιτούμενων συναρτήσεων Δεν ήταν δυνατή η εύρεση του παρόχου με τον καθορισμένο προορισμό Σφάλμα κατά την εκτέλεση του σεναρίου Μη έγκυρη διαδρομή για το ζητούμενο πάροχο Δεν ήταν δυνατή η ανάγνωση του επιλεγμένου αρχείου Η υπηρεσία δεν είναι διαθέσιμη Άγνωστο σφάλμα Πρέπει να καθορίσετε ένα URL για αυτήν την υπηρεσία 