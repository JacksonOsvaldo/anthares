��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  T   �  %     z   E  /   �     �     �  0        B        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-12-18 19:58+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Μερίδιο δίσκου Δεν εντοπίστηκαν περιορισμοί μεριδίων δίσκου Εγκαταστήστε το 'quota' Το εργαλείο μεριδίων δίσκου δεν εντοπίστηκε.

Εγκαταστήστε το 'quota'. Η εκτέλεση του quota απέτυχε %1 από %2 %1 ελεύθερα Μερίδιο δίσκου: %1% σε χρήση %1: %2% σε χρήση 