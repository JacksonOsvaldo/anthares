��          t      �                 '     6     R     Z     w  Q   �     �      �       �    7   �  %     1   .  
   `  /   k  8   �     �  +   �          	     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-09-20 12:17+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Απαίτηση λέξης ενεργοποίησης &Λέξη ενεργοποίησης: Έλεγχος ορθογραφίας του :q:. Σωστό Αδυναμία εύρεσης λεξικού. Ρυθμίσεις ελέγχου ορθογραφίας %1:q: Προτεινόμενες λέξεις: %1 ,  ορθογραφία 