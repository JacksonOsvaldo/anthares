��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     -  #   J     n     �  !   �     �     �     �     �       �   !     �     �  '   �  +        3     Q     a  +   v  .   �     �  �   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-11-26 15:37+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Το %1 εκτελείται Το %1 δεν εκτελείται &Μηδενισμός &Εκκίνηση Για προχωρημένους Εμφάνιση Εντολή: Εμφάνιση Εκτέλεση εντολής Ειδοποιήσεις Υπολειπόμενος χρόνος: %1 δευτερόλεπτο Υπολειπόμενος χρόνος: %1 δευτερόλεπτα Εκτέλεση εντολής Σ&ταμάτημα Εμφάνιση ειδοποίησης Εμφάνιση δευτερολέπτων Εμφάνιση τίτλου Κείμενο: Χρονόμετρο Ολοκλήρωση χρονομέτρου Το χρονόμετρο εκτελείται Τίτλος: Χρησιμοποιήστε τη ροδέλα του ποντικιού για να αλλάξετε τα ψηφία ή επιλέξτε από τα προκαθορισμένα χρονόμετρα στο βοηθητικό μενού 