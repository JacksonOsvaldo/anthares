��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  S   �  '   �  !   $  *   F  9   q     �  0   �     �       #   !  M   E     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-16 16:43+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Αυτόματη αντιγραφή του χρώματος στο πρόχειρο Καθαρισμός ιστορικού Επιλογές χρώματος Αντιγραφή στο πρόχειρο Προκαθορισμένη μορφή χρώματος: Γενικά Άνοιγμα διαλόγου χρωμάτων Επιλέξτε χρώμα Επιλέξτε χρώμα Εμφάνιση ιστορικού Όταν πιέζεται η συντόμευση πληκτρολογίου: 