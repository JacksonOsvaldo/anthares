��    *      l  ;   �      �     �  �   �     A     J     Q     ]     i     u     ~     �     �     �     �     �      �                    +     =     N     d     v     �     �     �     �     �     �     �               %     :      C  7   d     �     �     �     �     �  �  �     w  %   {  !   �     �      �     �  #   	     )	  @   8	     y	  #   �	     �	     �	  %   �	     

     
      )
  $   J
  *   o
  &   �
  -   �
  %   �
  (        >     R  %   h  
   �  2   �  (   �  2   �     (     H  ?   f     �  C   �  s   �     p  8   y  *   �     �     �                      $   )   *          !         
                                                   	                                              (      %      "      &         '                    #       ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-01-24 16:07+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ... (Διαθέσιμα μεγέθη: %1) Για προχωρημένους Συγγραφέας Αυτόματη &σύνδεση Φόντο: Καθαρισμός εικόνας Εντολές Αδυναμία αποσυμπίεσης αρχειοθήκης Θέμα δρομέα: Προσαρμογή θέματος Προκαθορισμένο Περιγραφή Λήψη νέων θεμάτων SDDM dimkard@gmail.com Γενικά Λήψη νέου θέματος Εντολή τερματισμού: Εγκατάσταση από αρχείο Εγκατάσταση θέματος. Μη έγκυρο πακέτο θέματος Φόρτωση από αρχείο... Οθόνη εισόδου με το SDDM Μέγιστο UID: Ελάχιστο UID: Δημήτρης Καρδαράκος Όνομα Μη διαθέσιμη προεπισκόπηση Εντολή επανεκκίνησης: Σύνδεση ξανά μετά την έξοδο Αφαίρεση θέματος Διαμόρφωση SDDM KDE Πρόγραμμα εγκατάστασης θεμάτων SDDM Συνεδρία: Το προκαθορισμένο θέμα δρομέα του SDDM Για εγκατάσταση θέματος, απαιτείται η ύπαρξη μιας αρχειοθήκης. Θέμα Αδυναμία εγκατάστασης θέματος Απεγκατάσταση θέματος. Χρήστης Χρήστης: 