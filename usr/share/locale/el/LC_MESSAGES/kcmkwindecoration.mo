��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p           1  #   >  4   b     �     �     �     �     �     �  +        I     X  �   i  \   	     |	  s   �	  >   
  @   O
  "   �
  "   �
     �
  
   �
     �
  :        T     g     v                                                                               
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-11-18 16:23+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Τεράστιο Μεγάλο Χωρίς περιγράμματα Χωρίς πλευρικά περιγράμματα Κανονικό Υπερφυσικό Μικροσκοπικό Τερατώδες Πολύ μεγάλο Μενού εφαρμογών Μέγεθος &περιγράμματος: Κουμπιά Κλείσιμο Κλείσιμο με διπλό κλικ  :
 Για να ανοίξετε το μενού, κρατήστε το κουμπί πατημένο μέχρι να εμφανιστεί. Κλείσιμο παραθύρων με διπλό &κλικ στο κουμπί μενού Πλαίσιο βοήθειας Σύρσιμο κουμπιών μεταξύ της θέσης αυτής και της γραμμής τίτλου Απόθεση εδώ για αφαίρεση κουμπιού Λήψη νέων διακοσμήσεων παραθύρου... Διατήρηση από πάνω Διατήρηση από κάτω Μεγιστοποίηση Μενού Ελαχιστοποίηση Σε όλες τις επιφάνειες εργασίας Αναζήτηση Τύλιγμα Θέμα Γραμμή τίτλου 