��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �  +   �  (   �     �  4   �  %   4     Z  &   g     �  ^   �  E     )   M  ?   w  2   �     �  &   �  �   $        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-03-03 16:47+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Διαμόρφωση εκτυπωτών... Μόνο ενεργές εργασίες Όλες οι εργασίες Μόνο ολοκληρωμένες εργασίες Διαμόρφωση εκτυπωτή Γενικά Καμία ενεργή εργασία Καμία εργασία Δεν έχουν διαμορφωθεί εκτυπωτές ή δεν εντοπίστηκαν Μία ενεργή εργασία %1 ενεργές εργασίες Μία εργασία %1 εργασίες Άνοιγμα λίστας αναμονής εκτύπωσης Η λίστα αναμονής είναι κενή Εκτυπωτές Αναζήτηση εκτυπωτή... Μια εργασία βρίσκεται στη λίστα αναμονής %1 εργασίες βρίσκονται στη λίστα αναμονής 