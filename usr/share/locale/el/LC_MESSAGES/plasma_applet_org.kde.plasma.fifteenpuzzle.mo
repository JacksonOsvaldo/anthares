��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     g     x     �     �  C   �       F   )     p     �     �     �  2   �  *        .  6   ?     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-19 19:36+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Εμφάνιση Περιήγηση... Επιλογή εικόνας Παζλ 15 κομματιών Αρχεία εικόνας (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Χρώμα αριθμού Διαδρομή για την προσαρμοσμένη εικόνα Χρώμα κομματιού Εμφάνιση αριθμών Ανακάτεμα Μέγεθος Επίλυση με διάταξη σε σειρά Λύθηκε! Δοκιμάστε ξανά. Χρόνος: %1 Χρήση προσαρμοσμένης εικόνας 