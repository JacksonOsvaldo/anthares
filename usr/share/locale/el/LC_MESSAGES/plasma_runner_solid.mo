��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �  (   N      w  U   �  �   �  �   �  �   B	  �   �	  �   �
  �   z  ,   O     |     �     �     �     �     �  ,   �  2                                   
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-09-10 17:26+0200
Last-Translator: Dimitrios Glentadakis <dglent@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Κλείδωμα του υποδοχέα Εξαγωγή του μέσου Εύρεση συσκευών που το όνομα ταιριάζει με το :q: Εμφανίζει όλες τις συσκευές και επιτρέπει την προσάρτηση τους, την αποπροσάρτηση ή την εξαγωγή τους. Εμφανίζει όλες τις συσκευές που μπορούν να εξαχθούν τους, και επιτρέπει την εξαγωγή. Εμφανίζει όλες τις συσκευές που μπορούν να προσαρτηθούν τους, και επιτρέπει την προσάρτηση. Εμφανίζει όλες τις συσκευές που μπορούν να αποπροσαρτηθούν τους, και επιτρέπει την αποπροσάρτηση. Εμφανίζει όλες τις κρυπτογραφημένες συσκευές που μπορούν να κλειδωθούν τους, και τους επιτρέπει να είναι κλειδωμένες. Εμφανίζει όλες τις κρυπτογραφημένες συσκευές που μπορούν να ξεκλειδωθούν, και τους επιτρέπει να είναι ξεκλείδωτες. Προσάρτηση της συσκευής συσκευή εξαγωγή κλείδωμα προσάρτηση ξεκλείδωμα αποπροσάρτηση Ξεκλείδωμα του υποδοχέα Αποπροσάρτηση της συσκευής 