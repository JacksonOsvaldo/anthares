��          4      L       `   �   a   L   �   �  E  m  �     g                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: krunner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-03-07 13:30+0200
Last-Translator: Antonis Geralis <capoiosct@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Μετατρέπει την αξία του :q: όταν το :q: είναι δημιουργημένο από "μονάδα αξίας [>, σε, ως] αξίας". Μπορείτε να χρησιμοποιήσετε την μικροεφαρμογή μετατροπής μονάδων για να βρείτε όλες τις διαθέσιμες μονάδες. in;σε;ως 