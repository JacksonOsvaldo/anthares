��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     r     �  U   �     �     	          #     8     I     \     c     �     �     �  )   �  k   �  #   Z     ~                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-12-08 10:24+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: Greek <kde-i18n-el@kde.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Ε&ξερεύνηση... &Αναζήτηση: *.png *.xpm *.svg *.svgz|Αρχεία εικονιδίων (*.png *.xpm *.svg *.svgz) Ενέργειες Όλα Εφαρμογές Κατηγορίες Συσκευές Εμβλήματα Emotes Πηγή εικονιδίων Τύποι mime Ά&λλα εικονίδια: Τοποθεσίες Εικονίδια &συστήματος: Διαδραστική αναζήτηση για ονόματα εικονιδίων (π.χ. φάκελο). Επιλογή εικονιδίου Κατάσταση 