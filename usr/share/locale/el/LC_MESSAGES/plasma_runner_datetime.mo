��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �  (   �  ?   �  z   �  1   p  l   �          $  6   +                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-02-08 12:00+0200
Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>
Language-Team: American English <kde-i18n-el@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Η τρέχουσα ώρα είναι %1 Εμφανίζει την τρέχουσα ημερομηνία Εμφανίζει την τρέχουσα ημερομηνία σε μια δεδομένη ωρολογιακή ζώνη Εμφανίζει την τρέχουσα ώρα Εμφανίζει την τρέχουσα ώρα σε μια δεδομένη ωρολογιακή ζώνη ημερομηνία ώρα Η σημερινή ημερομηνία είναι %1 