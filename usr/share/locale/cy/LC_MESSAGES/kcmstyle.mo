��          �      �        &   	  �   0     �     �     �     �     �     �      �  �     c   �     I     Z     y     �     �     �     �  	   �  C   �  j        m  �  �  )      �   J     �     �     �     �               *  �   ?  a   	     z	     �	     �	     �	     �	     �	     �	     �	  C   �	  q   
     �
               
                                                                      	                   (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2004-07-04 17:23+0100
Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>
Language-Team: Cymraeg <cy@li.org>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 (h)(c) 2002 Karol Szwed, Daniel Molkentin <h1>Arddull</h1>Mae'r modiwl hwn yn galluogi chi i newid golwg elfennau y rhyngwyneb defnyddiwr, er enghraifft ardulliau ac effeithiau y celfigion. Botwm Blwch brith Blwch Cyfunol Ffurf&weddu... Ffurfweddu %1 Disgrifiad: %1 kyfieithu@dotmon.com Fan hyn gallwch chi ddewis oddiar restr o gelfigion rhagosodedig (e.e. sut mae'r botymau yn cael eu darlunio) sydd yn gallu neu beidio cael ei gyfuno gyda chynllun (gyda gwybodaeth arall fel gwead marmor neu raddliw) Os alluogwch y dewisiad yma, dangosa Cymhwysiadau KDE eiconau bach wrth ochr rhai botymau pwysig. Modwl Arddull KDE KD Nid oes disgrifiad ar gael. Rhagolwg Botwm radio Tab 1 Tab 2 Testun yn Unig Roedd gwall wrth lwytho'r ymgom ffurfweddu ar gyfer yr arddull yma. Mae'r ardal hon yn dangos rhagolwg o'r arddull dewisiedig heb fod rhaid gweithredu'r arddull ar y penbwrdd cyfan. Methu Llwytho'r Ymgom 