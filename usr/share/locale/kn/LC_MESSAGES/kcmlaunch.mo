��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  \        ]  �  f  �   L  .   '  .   V  8   �  f   �  V   %  S   |  \   �  8   -                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-12-26 11:17+0530
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: Kannada <en@li.org>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  ಸೆಕೆಂಡು ಆರಂಭಗೊಳಿಕಾ ಸೂಚನೆಯ ಕಾಲಾವಧಿಯ ಮಿತಿ(&S): <H1>ಕಾರ್ಯಪಟ್ಟಿಕೆ ಸೂಚನೆ</H1>
ನೀವು ಆರಂಭಿಸಿದ ಅನ್ವಯವು ಲೋಡ್‌ ಆಗುತ್ತಿದೆ ಎಂದು ಸೂಚಿಸುವ, ಕಾರ್ಯಪಟ್ಟಿಕೆಯು ಬಳಸುವಂತಹ 
ತಿರುಗುವ ಮರಳು ಗಡಿಯಾರ (hourglass) ಅನ್ನು ಹೊಂದಿರುವ ಗುಂಡಿಯು ಕಾಣಿಸಿಕೊಳ್ಳುವಂತಹ ಆರಂಭಿಸುವ 
ಇನ್ನೊಂದು ಕ್ರಮವನ್ನು ಶಕ್ತಗೊಳಿಸಬಹುದಾಗಿದೆ. ಕೆಲವೊಂದು ಅನ್ವಯಗಳಿಗೆ ಈ ಆರಂಭ ಸೂಚನೆಯ ಬಗ್ಗೆ ತಿಳಿದಿರುವುದಿಲ್ಲ.
ಇಂತಹ ಸಂದರ್ಭಗಳಲ್ಲಿ, 'ಆರಂಭಗೊಳಿಕಾ ಸೂಚನೆಯ ಕಾಲಾವಧಿ ಮಿತಿ' ವಿಭಾಗದಲ್ಲಿ 
ನೀಡಲಾದ ಸಮಯವು ಮುಗಿದ ನಂತರ ಗುಂಡಿಯು ಮರೆಯಾಗುತ್ತದೆ. <h1>ಕಾರ್ಯನಿರತ ತೆರೆಸೂಚಕ</h1>
KDE ಯು ಅನ್ವಯದ ಆರಂಭದ ಸೂಚನೆಗಾಗಿ ಒಂದು ಕಾರ್ಯನಿರತ ತೆರೆಸೂಚಕವನ್ನು ಒದಗಿಸುತ್ತದೆ.
ಕಾರ್ಯನಿರತ ತೆರೆಸೂಚಕವನ್ನು ಶಕ್ತಗೊಳಿಸಲು, ಕಾಂಬೋಬಾಕ್ಸಿನಿಂದ ಒಂದು ಬಗೆಯ ಅಭಿಪ್ರಾಯವನ್ನು 
ಆಯ್ಕೆ ಮಾಡಿ
ಕೆಲವೊಂದು ಅನ್ವಯಗಳಿಗೆ ಈ ಆರಂಭ ಸೂಚನೆಯ ಬಗ್ಗೆ ತಿಳಿದಿರುವುದಿಲ್ಲ.
ಇಂತಹ ಸಂದರ್ಭಗಳಲ್ಲಿ, 'ಆರಂಭಗೊಳಿಕಾ ಸೂಚನೆಯ ಕಾಲಾವಧಿ ಮಿತಿ' ವಿಭಾಗದಲ್ಲಿ 
ನೀಡಲಾದ ಸಮಯವು ಮುಗಿದ ನಂತರ ತೆರೆಸೂಚಕವು ಮಿನುಗುವುದನ್ನು ನಿಲ್ಲಿಸುತ್ತದೆ. <h1>ಆರಂಭಗೊಳಿಕೆಯ ಅಭಿಪ್ರಾಯ</h1> ಅನ್ವಯ ಆರಂಭಗೊಳಿಕಾ ಅಭಿಪ್ರಾಯವನ್ನು ಇಲ್ಲಿ ನೀವು ಸಂರಚಿಸಬಹುದು. ಮಿನುಗುವ ತೆರೆಸೂಚಕ ಪುಟಿಯುವ ತೆರೆಸೂಚಕ ಕಾರ್ಯನಿರತ ತೆರೆಸೂಚಕ(&y) ಕಾರ್ಯಪಟ್ಟಿಕೆ ಸೂಚನೆಯನ್ನು ಶಕ್ತಗೊಳಿಸು(&t) ಯಾವುದೆ ಕಾರ್ಯನಿರತ ತೆರೆಸೂಚಕವಿಲ್ಲ ಜಡಗೊಂಡಿರುವ ಕಾರ್ಯನಿರತ ತೆರೆಸೂಚಕ ಆರಂಭಗೊಳಿಕಾ ಸೂಚನೆಯ ಕಾಲಾವಧಿಯ ಮಿತಿ(&u): ಕಾರ್ಯಪಟ್ಟಿಕೆ ಸೂಚನೆ(&N) 