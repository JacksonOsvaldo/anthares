��            )   �      �  &   �  �   �     ?     W     ^     g     p     ~     �     �      �  	   �  �   �  c   �               !     @     Z     b     o     {     �  	   �  C   �  j   �     @     V  �  d  &     ,  (  >   U	     �	     �	     �	     �	  (   �	     $
     5
     L
     `
  &  z
    �  )   �     �  5   �  B   ,     o  "   �     �     �     �     �  �   �  '  �  `   �      7            	                                                                                 
                                             (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2008-12-29 16:57+0530
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: Kannada <en@li.org>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>ಶೈಲಿ</h1>ಈ ಮಾಡ್ಯೂಲ್ ಬಳಕೆದಾರ ಸಂಪರ್ಕಸಾಧನ ಘಟಕಗಳ ವಿಜೆಟ್ ಶೈಲಿ ಹಾಗು ಪರಿಣಾಮಗಳಂತಹ ಗೋಚರಿಕೆಯನ್ನು ಬದಲಾಯಿಸಲು ನೆರವಾಗುತ್ತದೆ. ಸೂಕ್ಷ್ಮ ಸರಿಪಡಿಸುವಿಕೆ(&F) ಗುಂಡಿ ಪರೀಕ್ಷಕ ಚೌಕ ಕಾಂಬೋ ಚೌಕ ಸಂರಚಿಸು(&f)... %1 ಅನ್ನು ಸಂರಚಿಸು Daniel Molkentin ವಿವರಣೆ: %1 svenkate@redhat.com ಗುಂಪು ಚೌಕ ಮೊದಲೆ ನಿರ್ಧರಿಸಲಾದ ವಿಜೆಟ್ ಶೈಲಿಗಳ (ಉದಾ., ಗುಂಡಿಗಳು ಹೇಗೆ ಬರೆಯಲ್ಪಡುತ್ತವೆ) ಪಟ್ಟಿಯಿಂದ ಆಯ್ಕೆ ಮಾಡಬಹುದಾಗಿದ್ದು, ಇದರೊಂದಿಗೆ ಥೀಮ್ ಅನ್ನೂ (ಅಮೃತಶಿಲೆಯ ಮೇಲ್ಮೈರಚನೆ ಅಥವ ಒಂದು ಇಳಿಜಾರು) ಸಹ ಸೇರ್ಪಡಿಸಿರಬಹುದು ಅಥವ ಸೇರ್ಪಡಿಸದೆ ಇರಬಹುದು. ಈ ಆಯ್ಕೆಯನ್ನು ಶಕ್ತಗೊಳಿಸಿದಲ್ಲಿ, KDE ಅನ್ವಯಗಳು ಕೆಲವೊಂದು ಪ್ರಮುಖ ಗುಂಡಿಗಳ ಪಕ್ಕದಲ್ಲಿ ಸಣ್ಣ ಚಿಹ್ನೆಯನ್ನು ತೋರಿಸುತ್ತದೆ. KDE ಶೈಲಿ ಮಾಡ್ಯೂಲ್ Karol Szwed ಶಂಕರ ಪ್ರಸಾದ್‌ ಎಂ. ವಿ. ಯಾವುದೆ ವಿವರಣೆ ಲಭ್ಯವಿಲ್ಲ. ಮುನ್ನೋಟ ರೇಡಿಯೋ ಗುಂಡಿ Ralf Nolden ಟ್ಯಾಬ್ ೧ ಟ್ಯಾಬ್ ೨ ಪಠ್ಯ ಮಾತ್ರ ಈ ಶೈಲಿಗಾಗಿ ಸಂರಚನಾ ಸಂವಾದವನ್ನು ಲೋಡ್ ಮಾಡುವಲ್ಲಿ ಒಂದು ದೋಷ ಕಂಡುಬಂದಿದೆ. ಈ ಜಾಗದಲ್ಲಿ ಪ್ರಸಕ್ತ ಆಯ್ಕೆ ಮಾಡಲಾದ ಶೈಲಿಯನ್ನು ಸಂಪೂರ್ಣ ಗಣಕತೆರೆಗೆ ಅನ್ವಯಿಸದೆ ಕೇವಲ ಅದರ ಮುನ್ನೋಟವನ್ನು ತೋರಿಸಲಾಗುತ್ತದೆ. ಸಂವಾದವನ್ನು ಲೋಡ್ ಮಾಡಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ ವಿಜೆಟ್ ಶೈಲಿ: 