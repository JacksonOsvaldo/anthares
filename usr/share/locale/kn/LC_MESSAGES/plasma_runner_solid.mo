��          �            h  m   i     �  "   �  F     F   N  F   �  J   �     '  %   8  $   ^  $   �  &   �  �   �     W  �  j  8   �  :   ,  �   g       �     �   �  �   �	  1   �
     �
               1  D   J  4   �           	                                                             
        Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordmount Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-29 14:15+0530
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: kn-IN <>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 ಕಂಟೈನರನ್ನು ಲಾಕ್ ಮಾಡು ಮಾಧ್ಯಮವನ್ನು ಹೊರತಳ್ಳು :q:ಗೆ ತಾಳೆಯಾಗುವ ಹೆಸರನ್ನು ಹೊಂದಿರುವ ಸಾಧನಗಳನ್ನು ಹುಡುಕುತ್ತದೆ ಎಲ್ಲಾ ಸಾಧನಗಳನ್ನು ಪಟ್ಟಿ ಮಾಡಿ ನಂತರ ಅವುಗಳನ್ನು ಆರೋಹಿಸಲು, ಅವರೋಹಿಸಲು, ಅಥವಾ ಹೊರತಳ್ಳಲು ಅನುವು ಮಾಡಿಕೊಡುತ್ತದೆ. ಹೊರತಳ್ಳಬಹುದಾದ ಸಾಧನಗಳನ್ನು ಪಟ್ಟಿ ಮಾಡುತ್ತದೆ ಹಾಗೂ ಅವುಗಳನ್ನು ಹೊರಳ್ಳಲು ಅನುಮಾಡಿಕೊಡುತ್ತದೆ. ಆರೋಹಿಸಬಹುದಾದ ಸಾಧನಗಳನ್ನು ಪಟ್ಟಿ ಮಾಡುತ್ತದೆ ಹಾಗೂ ಅವುಗಳನ್ನು ಆರೋಹಿಸಲು ಅನುಮಾಡಿಕೊಡುತ್ತದೆ. ಅವರೋಹಿಸಬಹುದಾದ ಸಾಧನಗಳನ್ನು ಪಟ್ಟಿ ಮಾಡುತ್ತದೆ ಹಾಗೂ ಅವುಗಳನ್ನು ಅವರೋಹಿಸಲು ಅನುಮಾಡಿಕೊಡುತ್ತದೆ. ಸಾಧನವನ್ನು ಆರೋಹಿಸು ಸಾಧನ ಹೊರತಳ್ಳು ಆರೋಹಿಸು ಅವರೋಹಿಸು ಕಂಟೈನರನ್ನು ಅನ್‌ಲಾಕ್ ಮಾಡು ಸಾಧನವನ್ನು ಅವರೋಹಿಸು 