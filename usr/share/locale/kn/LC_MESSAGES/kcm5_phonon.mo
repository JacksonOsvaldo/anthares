��    A      $  Y   ,      �  i   �  m   �     i  b   �     �     �  6        ?  7   O     �     �  	   �     �  (   �  )   �  )        B     _  Y   e     �     �  �   �      i	     �	  
   �	     �	     �	     �	     �	     �	     �	     
     
     $
     3
     8
     W
     s
     �
     �
     �
  	   �
  
   �
     �
     �
  	   �
  
   �
  
   �
     �
       	   !     +     0  
   I  �   T     �  8   �  �   2     �  8   �  ,     ,   0  %   ]     �  �  �  �   -  j  �  `   V  	  �  A   �  1     V   5  "   �  i   �          2     @     M  W   k  j   �  m   .  T   �     �           "   4  A  W  (   �      �  "   �  >     #   E  >   i     �  .   �  2   �     #  B   6     y     �  @   �  8   �     	     '     G  %   `  "   �  "   �     �  G   �     ,     =  "   N     q  Z   �     �       D   !  :   f  �  �  "   G  �   j       %   %!  m   K!  �   �!  �   \"  ^   #  E   a#     #       $          :   +          A                     "                 6   8       *              (   >   ,   ;         5                                   )   1       =         7       0       ?       -   4   2   9   @         %      .                3              /      !             <   '   &         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-08 13:27+0530
Last-Translator: s
Language-Team: American English <kde-i18n-doc@kde.org>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 ಹಿನ್ನೆಲೆ ತಂತ್ರಾಂಶಗಳನ್ನು ಬದಲಾಯಿಸಲು ನೀವು ಲಾಗೌಟ್ ಆಗಿ ಪುನಃ ಲಾಗಿನ್ ಆಗಬೇಕು. ನಿಮ್ಮ ಗಣಕ ವ್ಯವಸ್ಥೆಯಲ್ಲಿ ಸಿಕ್ಕಿದ ಫೋನಾನ್ ಹಿನ್ನೆಲೆಗಳ ಪಟ್ಟಿ. ಈ ಪಟ್ಟಿಯ ಹಿನ್ನೆಲೆಗಳ ಕ್ರಮವು ಫೋನಾನ್ ಅವುಗಳನ್ನು ಬಳಸುವ ಕ್ರಮವನ್ನು ನಿರ್ಧರಿಸುತ್ತದೆ. ಸಾಧನದ ಪಟ್ಟಿಯನ್ನು ಇಲ್ಲಿಗೆ ಅನ್ವಯಿಸು... ಪ್ರಸಕ್ತ ತೋರಿಸಲಾಗುತ್ತಿರುವ ಸಾಧನ ಆದ್ಯತೆ ಪಟ್ಟಿಯನ್ನು ಈ ಕೆಳಗಿನ ಇತರೆ ಆಡಿಯೋ ಔಟ್‌ಪುಟ್ ವರ್ಗಗಳಿಗೆ ಅನ್ವಯಿಸು:  ಶ್ರವಣ ಯಂತ್ರಾಂಶ ವ್ಯವಸ್ಥೆ ಆಡಿಯೋ ಚಲಾಯಿಸುವಿಕೆ '%1' ವರ್ಗಕ್ಕಾಗಿ ಶ್ರವಣ ಸಾಧನದ ಆದ್ಯತೆ ಆಡಿಯೊ ಮುದ್ರಣ '%1' ವರ್ಗಕ್ಕಾಗಿ ಧ್ವನಿ ಮುದ್ರಣ ಸಾಧನದ ಆದ್ಯತೆ ಹಿನ್ನೆಲೆ Colin Guthrie ಜೋಡಕ Copyright 2006 Matthias Kretz ಪೂರ್ವನಿಯೋಜಿತ ಶ್ರವಣ ಸಾಧನದ ಆದ್ಯತೆ ಪೂರ್ವನಿಯೋಜಿತ ಶ್ರವಣ ಮುದ್ರಣ ಸಾಧನದ ಆದ್ಯತೆ ಪೂರ್ವನಿಯೋಜಿತ ವೀಡಿಯೊ ಮುದ್ರಣ ಸಾಧನದ ಆದ್ಯತೆ ಪೂರ್ವನಿಯೋಜಿತ/ಸೂಚಿಸದೆ ಇರುವ ವರ್ಗ ಮುಂದೂಡು ಸಾಧನಗಳ ಪೂರ್ವನಿಯೋಜಿತ ಜೋಡಣಾ ಕ್ರಮವನ್ನು ಸೂಚಿಸುತ್ತದೆ ಹಾಗು ಇದನ್ನು ಪ್ರತ್ಯೇಕ ವರ್ಗಗಳಿಂದ ಬದಲಾಯಿಸಬಹುದಾಗಿದೆ. ಸಾಧನ ಸಂರಚನೆ ಸಾಧನದ ಆದ್ಯತೆ ಆಯ್ಕೆ ಮಾಡಿದ ಪಂಗಡಕ್ಕೆ ಸರಿಹೊಂದುವ ನಿಮ್ಮ ಗಣಕ ವ್ಯವಸ್ಥೆಯಲ್ಲಿ ಸಿಕ್ಕಿದ ಸಾಧನಗಳು. ಅನ್ವಯಗಳು ಯಾವ ಸಾಧನವನ್ನು ಬಳಸಬೇಕೆಂಬುದನ್ನು ಆರಿಸಿ. svenkate@redhat.com,adarsharao@gmail.com ಮಧ್ಯ ಮುಂಭಾಗ  ಮುಂದಿನ ಎಡಭಾಗ ಮುಂಭಾಗ ಮಧ್ಯದಿಂದ ಎಡಕ್ಕೆ ಮುಂದಿನ ಬಲ ಬದಿ ಮುಂಭಾಗ ಮಧ್ಯದಿಂದ ಬಲಕ್ಕೆ ಯಂತ್ರಾಂಶ ಸ್ವತಂತ್ರ ಸಾಧನಗಳು ಆದಾನ (ಇನ್ ಪುಟ್) ಮಟ್ಟ ಅಮಾನ್ಯ KDE ಶ್ರವಣ ಯಂತ್ರಾಂಶ ಸಿದ್ಧತೆ Matthias Kretz ಏಕ ಶಂಕರ ಪ್ರಸಾದ್, ಆದರ್ಶ ರಾವ್ ಫೋನಾನ್ ಸಂರಚನೆಯ ಉಪಘಟಕ ಅವತರಣಿಕೆ (%1) ಆದ್ಯತೆ ನೀಡು ಪ್ರೊಫೈಲ್ ಹಿಂದಿನ ನಡುಭಾಗ ಹಿಂದಿನ ಎಡಬದಿ ಹಿಂದಿನ ಬಲಬದಿ ಮುದ್ರಣ (%1) ಸುಧಾರಿತ ಸಾಧನಗಳನ್ನು ತೋರಿಸು ಎಡ ಬದಿ ಬಲ ಬದಿ ಧ್ವನಿ ಬಿಲ್ಲೆ ಧ್ವನಿ ಸಾಧನ ಧ್ವನಿವರ್ಧಕ ಸ್ಥಾಪನೆ ಮತ್ತು ಪರೀಕ್ಷೆ ಸಬ್‌ವೂಫರ್ ಪ್ರಾಯೋಗಿಕ ಆಯ್ದ ಸಾಧನವನ್ನು ಪರೀಕ್ಷಿಸು ಪರೀಕ್ಷಿಸಲಾಗುತ್ತಿದೆ, %1 ಈ ಕ್ರಮವು ಉತ್ಪನ್ನ ಸಾಧನಗಳ ಆದ್ಯತೆಯ ಕ್ರಮವನ್ನು ನಿರ್ಧರಿಸುತ್ತದೆ. ಯಾವುದೇ ಕಾರಣಕ್ಕೆ ಮೊದಲ ಸಾಧನವನ್ನು ಬಳಸಲಾಗದಿದ್ದಲ್ಲಿ ಫೋನಾನ್ ಎರಡನೇ ಸಾಧನವನ್ನು ಪ್ರಯತ್ನಿಸುತ್ತದೆ, ಇತ್ಯಾದಿ. ಅಜ್ಞಾತ ಚಾನಲ್ ಪ್ರಸಕ್ತ ತೋರಿಸಲಾಗುತ್ತಿರುವ ಸಾಧನ ಪಟ್ಟಿಯನ್ನು ಇತರೆ ಪಂಗಡಗಳಿಗೆ ಬಳಸು. ಮಾಧ್ಯಮ ಬಳಕೆಯ ವಿವಿಧ ಪಂಗಡಗಳು. ಪ್ರತಿ ಪಂಗಡಕ್ಕೂ  ಫೋನಾನ್ ಅನ್ವಯಗಳು ಯಾವ ಸಾಧನವನ್ನು  ಬಳಸಬೇಕೆಂದು ನೀವು ಆರಿಸಬಹುದು.  ವೀಡಿಯೊ ಮುದ್ರಣ '%1' ವರ್ಗಕ್ಕಾಗಿ ವೀಡಿಯೊ ಮುದ್ರಣ ಸಾಧನದ ಆದ್ಯತೆ  ನಿಮ್ಮ ಹಿನ್ನೆಲೆ ಯಂತ್ರಾಂಶ ಆಡಿಯೊ ಮುದ್ರಣವನ್ನು ಬೆಂಬಲಿಸುವುದಿಲ್ಲ. ನಿಮ್ಮ ಹಿನ್ನೆಲೆ ಯಂತ್ರಾಂಶ ವಿಡಿಯೊ ಮುದ್ರಣವನ್ನು ಬೆಂಬಲಿಸುವುದಿಲ್ಲ. ಆಯ್ದ ಸಾಧನಕ್ಕೆ ಯಾವುದೇ ಆದ್ಯತೆಗಳಿಲ್ಲ. ಆಯ್ದ ಸಾಧನಕ್ಕೆ ಆದ್ಯತೆ ಕೊಡು 