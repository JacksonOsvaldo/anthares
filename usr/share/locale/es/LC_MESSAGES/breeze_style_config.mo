��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     E  ,   I  '   v  +   �  +   �     �       &   !     H  0   ^  /   �  1   �  \   �  V   N	  '   �	  >   �	  5   
  2   B
  "   u
  >   �
     �
  ,   �
               '  	   3     =  8   V     �     �                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-22 12:08+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms &Visibilidad de los aceleradores de teclado: &Tipo de botón para «flecha arriba»: Ocultar siempre los aceleradores de teclado Mostrar siempre los aceleradores de teclado D&uración de las animaciones: Animaciones T&ipo de botón para «flecha abajo»: Preferencias de Brisa Centrar las pestañas de las barras de pestañas Arrastrar ventanas desde cualquier área vacía Arrastrar ventanas solo desde la barra de título Arrastrar ventanas desde la barra de título, la barra de menú y las barras de herramientas Dibujar una línea delgada para indicar el foco en los menús y en las barras de menú Dibujar el indicador del foco en listas Dibujar un marco alrededor de los paneles que se pueden anclar Dibujar un marco alrededor de los títulos de página Dibujar un marco alrededor de los paneles adosados Dibujar marcas en los deslizadores Dibujar separadores de elementos en las barras de herramientas Activar animaciones Activar asas extendidas de cambio de tamaño Marcos General Sin botones Un botón Barras de desplazamiento Mostrar los aceleradores de teclado cuando sea necesario Dos botones Mo&do de arrastre de ventanas: 