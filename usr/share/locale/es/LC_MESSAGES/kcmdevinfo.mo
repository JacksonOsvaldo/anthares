��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  2   �     �  	   
            	   =     G     Z     `     h     q     �     �     �     �     �  (   �     
           -     9     L     T     b     v     �  
   �      �     �     �  2   �  	     	     
   '  
   2  
   =     H     P  
   a     l     ~     �     �     �     �     �  	   �     �  
   �  	   �     	          !     6     ?     M     e  
   r     }     �     �     �     �     �     �     �             !   6     X     p     v     z     ~  K   �     �     �     �     �          (  
   ,     7     C     K  
   S     ^     j     v     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-10 11:34+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 
Módulo del visor de dispositivos basado en Solid © 2010 David Hubner AMD 3DNow ATI IVEC %1 libres de %2 (%3% utilizado) Baterías Tipo de batería:  Bus:  Cámara Cámaras Estado de la carga:  En carga Contraer todo Lector de Compact Flash Un dispositivo Información del dispositivo Muestra todos los dispositivos listados. Visor de dispositivos Dispositivos En descarga ecuadra@eloihr.net Cifrado Expandir todo Sistema de archivos Tipo de sistema de archivos:  Totalmente cargada Disco duro ¿Se puede conectar en caliente? IDE IEEE1394 Muestra información del dispositivo seleccionado. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Teclado Teclado + Ratón Etiqueta:  Velocidad máx.:  Lector de Memory Stick Montado en:  Ratón Reproductores multimedia Eloy Cuadra No Sin carga No hay datos disponibles No montado Sin fijar Dispositivo óptico PDA Tabla de particiones Primaria Procesador %1 Número de procesador:  Procesadores Producto:  Raid ¿Extraíble? SATA SCSI Lector de SD/MMC Mostrar todos los dispositivos Mostrar dispositivos relevantes Lector de Smart Media Unidades de almacenamiento Controladores permitidos:  Juego de instrucciones empleado:  Protocolos permitidos:  UDI:  SAI USB UUID:  Muestra el UDI (Identificador Único de Dispositivo) del dispositivo actual Dispositivo desconocido No usado Fabricante:  Espacio en el volumen: Uso del volumen:  Sí kcmdevinfo Desconocido Ninguno Ninguno Plataforma Desconocido Desconocido Desconocido Desconocido Desconocido Lector de xD 