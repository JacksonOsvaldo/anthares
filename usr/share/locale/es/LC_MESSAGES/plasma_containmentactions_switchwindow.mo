��          T      �       �   "   �      �   *   �   3   '  E   [  .   �  �  �  (   �  '   �  /        I  .   _     �                                        Display a submenu for each desktop Display all windows in one list Display only the current desktop's windows plasma_containmentactions_switchwindowAll Desktops plasma_containmentactions_switchwindowConfigure Switch Window Plugin plasma_containmentactions_switchwindowWindows Project-Id-Version: plasma_containmentactions_switchwindow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-30 03:13+0200
PO-Revision-Date: 2014-06-14 21:37+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
com>
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Mostrar un submenú para cada escritorio Mostrar todas las ventanas en una lista Mostrar solo las ventanas del escritorio actual Todos los escritorios Configurar el complemento de cambio de ventana Ventanas 