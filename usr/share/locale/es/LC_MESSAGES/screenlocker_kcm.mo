��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    #   �     �     �     �  ?         @     O     j  +   {  4   �  $   �  
     
     >        V                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-13 18:32+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 B&loquear la pantalla al continuar: Activación Aspecto visual Error No se ha podido probar con éxito el bloqueador de la pantalla. Inmediatamente Acceso rápido de teclado: Bloquear sesión Bloquear la pantalla automáticamente tras: Bloquear la pantalla al despertar de una suspensión Necesi&ta contraseña tras bloquear:  min  mins  seg  segs El acceso rápido de teclado global para bloquear la pantalla. &Tipo de fondo del escritorio: 