��          <      \       p      q   *   �   /   �   �  �   .   �  M   �  S                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: plasma_applet_weatherstation
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2017-04-20 09:53+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 No ha sido posible encontrar «%1» usando %2. La conexión con el servidor meteorológico %1 ha superado el tiempo límite. Se ha sobrepasado el tiempo límite para obtener información meteorológica de %1. 