��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     y     �  3   �     �  2   �     $     *     /                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-02-01 12:36+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 La hora actual es %1 Muestra la fecha actual Muestra la fecha actual en la zona horaria indicada Muestra la hora actual Muestra la hora actual en la zona horaria indicada fecha hora La fecha de hoy es %1 