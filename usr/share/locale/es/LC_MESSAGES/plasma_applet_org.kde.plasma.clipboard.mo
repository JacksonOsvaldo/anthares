��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �     %   �     �     �  +        ;  
   X  
   c     n  -   �     �     �     �     �  
   �     �          +     2                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-17 21:40+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Cambiar el tipo del código de barras Borrar el historial Contenido del portapapeles El historial del portapapeles está vacío. El portapapeles está vacío Código 39 Código 93 Configurar el Portapapeles... La creación del código de barras ha fallado Matriz de datos Editar el contenido +%1 Invocar acción Código QR Eliminar del historial Volver al portapapeles Buscar Mostrar código de barras 