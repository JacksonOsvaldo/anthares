��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     #  &   *  )   Q  <   {  .   �     �      	  (   	  *   D	  >   o	  (   �	  +   �	  >   
  l   B
  6   �
     �
  "        &     5     F     W     h          �     �     �     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-11 22:42+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Enviar Archivos añadidos al repositorio SVN. Añadiendo archivos al repositorio SVN... Ha ocurrido un error al añadir archivos al repositorio SVN. Ha ocurrido un error al enviar cambios al SVN. Cambios enviados al SVN. Enviando cambios al SVN... Archivos eliminados del repositorio SVN. Eliminando archivos del repositorio SVN... Ha ocurrido un error al eliminar archivos del repositorio SVN. Archivos revertidos del repositorio SVN. Revirtiendo archivos del repositorio SVN... Ha ocurrido un error al revertir archivos del repositorio SVN. Ha fallado la actualización del estado de SVN. Se desactiva la opción «Mostrar actualizaciones del SVN». Ha ocurrido un error al actualizar el repositorio SVN. Repositorio SVN actualizado. Actualizando el repositorio SVN... Añadir al SVN Enviar al SVN... Eliminar del SVN Revertir del SVN Actualización del SVN Mostrar cambios locales del SVN Mostrar actualizaciones del SVN Descripción: Enviar al SVN Mostrar actualizaciones 