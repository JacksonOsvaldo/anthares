��    O      �  k         �  "   �     �     �  8     9   =  ,   w      �  2   �  4   �  4   -  8   b  ?   �  0   �  #   	  /   0	  0   `	  #   �	  2   �	  /   �	     
     $
     3
     D
     J
     X
  
   p
     {
     �
     �
     �
     �
     �
     �
  }   �
     Y     ^     e      q     �     �     �     �     �     �     �  	   �     �               *     7     Q     W     e     �     �     �     �     �     �     �     �     �  	   �  	   �  G   �     0     <     H     W     _     t     |     �     �     �     �     �  �  �  "   �     �     �  ;     >   M  +   �      �  ?   �  6     ?   P  .   �  9   �  5   �  $   /  0   T  4   �  #   �  3   �  5        H     T     \     l     r      �     �     �     �     �     �     �     �       �        �     �     �     �     �                    3      F     g     v          �  &   �     �     �     �  
   �     �          ,     9  	   A     K     Q     X     `     o  	   t  	   ~  L   �     �     �     �               (     0     I  "   h  !   �     �     �         /   6          A   -   >      B                  5             M              )      %   E   L         4             3   +             H   ;      $   1   J      &          '   D   0           	   #   !       C      *   <          ,           2   I          @      
       .   O                         G          F             =                 K   8      9   7   :   ?   N           "   (    (c) 2001 Matthias Hoelzer-Kluepfel <b>Manufacturer:</b>  <b>Serial #:</b>  <tr><td><i>Attached Devicenodes</i></td><td>%1</td></tr> <tr><td><i>Bandwidth</i></td><td>%1 of %2 (%3%)</td></tr> <tr><td><i>Channels</i></td><td>%1</td></tr> <tr><td><i>Class</i></td>%1</tr> <tr><td><i>Intr. requests</i></td><td>%1</td></tr> <tr><td><i>Isochr. requests</i></td><td>%1</td></tr> <tr><td><i>Max. Packet Size</i></td><td>%1</td></tr> <tr><td><i>Power Consumption</i></td><td>%1 mA</td></tr> <tr><td><i>Power Consumption</i></td><td>self powered</td></tr> <tr><td><i>Product ID</i></td><td>0x%1</td></tr> <tr><td><i>Protocol</i></td>%1</tr> <tr><td><i>Revision</i></td><td>%1.%2</td></tr> <tr><td><i>Speed</i></td><td>%1 Mbit/s</td></tr> <tr><td><i>Subclass</i></td>%1</tr> <tr><td><i>USB Version</i></td><td>%1.%2</td></tr> <tr><td><i>Vendor ID</i></td><td>0x%1</td></tr> AT-commands ATM Networking Abstract (modem) Audio Bidirectional Boot Interface Subclass Bulk (Zip) CAPI 2.0 CAPI Control CDC PUF Communications Control Device Control/Bulk Control/Bulk/Interrupt Could not open one or more USB controller. Make sure, you have read access to all USB controllers that should be listed here. Data Device Direct Line EMAIL OF TRANSLATORSYour emails Ethernet Networking Floppy HDLC Host Based Driver Hub Human Interface Devices I.430 ISDN BRI Interface Keyboard Leo Savernik Live Monitoring of USB Bus Mass Storage Matthias Hoelzer-Kluepfel Mouse Multi-Channel NAME OF TRANSLATORSYour names No Subclass Non Streaming None Printer Q.921 Q.921M Q.921TM Q.932 EuroISDN SCSI Streaming Telephone This module allows you to see the devices attached to your USB bus(es). Transparent USB Devices Unidirectional Unknown V.120 V.24 rate ISDN V.42bis Vendor Specific Vendor Specific Class Vendor Specific Protocol Vendor Specific Subclass Vendor specific kcmusb Project-Id-Version: kcmusb
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-03-10 11:34+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
First-Translator: Pablo de Vicente <pvicentea@nexo.es>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2001 Matthias Hoelzer-Kluepfel <b>Fabricante:</b>  <b>Nº de serie #:</b>  <tr><td><i>Dispositivos conectados</i></td><td>%1</td></tr> <tr><td><i>Ancho de banda</i></td><td>%1 of %2 (%3%)</td></tr> <tr><td><i>Canales</i></td><td>%1</td></tr> <tr><td><i>Clase</i></td>%1</tr> <tr><td><i>Peticiones de interrupción</i></td><td>%1</td></tr> <tr><td><i>Peticiones Isochr.</i></td><td>%1</td></tr> <tr><td><i>Tamaño máximo del paquete</i></td><td>%1</td></tr> <tr><td><i>Consumo</i></td><td>%1 mA</td></tr> <tr><td><i>Consumo</i></td><td>auto alimentados</td></tr> <tr><td><i>ID del producto</i></td><td>0x%1</td></tr> <tr><td><i>Protocolo</i></td>%1</tr> <tr><td><i>Revisión</i></td><td>%1.%2</td></tr> <tr><td><i>Velocidad</i></td><td>%1 Mbit/s</td></tr> <tr><td><i>Subclase</i></td>%1</tr> <tr><td><i>Versión USB</i></td><td>%1.%2</td></tr> <tr><td><i>ID del vendedor</i></td><td>0x%1</td></tr> Órdenes AT Red ATM Resumen (modem) Audio Bidireccional Subclase de interfaz de arranque Grande (Zip) CAPI 2.0 Control CAPI PUF CDC Comunicaciones Dispositivo de control Control/Grande Control/Grande/Interrupción No fue posible abrir uno o más controladores de USB. Asegúrese de que tiene permiso de lectura a todos los controladores de USB que puedan estar listados. Datos Dispositivo Línea directa jaime@kde.org,vicente@oan.es Red Ethernet Disquete HDLC Controlador basado en máquina Hub (Concentrador) Dispositivos de interfaz humanos BRI RDSI I.430 Interfaz Teclado Leo Savernik Monitorización en directo del bus USB Almacenamiento masivo Matthias Hoelzer-Kluepfel Ratón Multicanal Jaime Robles,Pablo de Vicente Ninguna subclase No streaming Ninguno Impresora Q.921 Q.921M Q.921TM Q.932 EuroRDSI SCSI Streaming Teléfono Este módulo le permite ver los dispositivos conectados a su(s) bus(es) USB. Transparente Dispositivos USB Unidireccional Desconocido Velocidad RDSI V.120 V.24 V.42bis Específico del vendedor Clase específica del vendedor Protocolo específico del vendedor Subclase específica del vendedor Específico del vendedor kcmusb 