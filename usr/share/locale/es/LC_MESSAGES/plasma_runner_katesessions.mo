��          <      \       p   !   q   3   �      �   �  �   -   �  6   �                        Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-05-22 13:26+0200
Last-Translator: Enrique Matias Sanchez (aka Quique) <cronopios@gmail.com>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
 Busca sesiones de Kate que coincidan con :q:. Lista todas las sesiones del editor Kate de su cuenta. Abrir sesión de Kate 