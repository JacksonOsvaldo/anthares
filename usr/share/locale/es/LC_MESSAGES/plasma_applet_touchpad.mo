��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �  
   H     S     o  S   �  )   �       "        7        	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: plasma_applet_touchpad
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-01-12 02:14+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Desactivar Desactivar el panel táctil Activar el panel táctil No se ha detectado ningún ratón.
¿Seguro que quiere desactivar el panel táctil? No se ha encontrado ningún panel táctil Panel táctil El panel táctil está desactivado El panel táctil está activado 