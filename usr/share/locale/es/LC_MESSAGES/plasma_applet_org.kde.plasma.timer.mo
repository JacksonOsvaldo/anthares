��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     (     ?  
   Y     d     m     v     ~     �     �     �  8   �     �     �     �          #     3     :     G  #   ^     �  �   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-27 12:49+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 está en ejecución %1 no está en ejecución &Reiniciar &Iniciar Avanzado Aspecto Orden: Mostrar Ejecutar orden Notificaciones Tiempo restante: %1 segundo Tiempo restante: %1 segundos Ejecutar orden De&tener Mostrar notificación Mostrar segundos Mostrar título Texto: Temporizador Temporizador terminado El temporizador está en ejecución Título: Use la rueda del ratón para cambiar los dígitos o para escoger entre los temporizadores predeterminados en el menú de contexto 