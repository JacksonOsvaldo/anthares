��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  $   �      	  (   3	  )   \	  (   �	     �	  
   �	     �	     �	     �	  h   �	  !   N
  Z   p
     �
     �
     �
     �
               '     3     L     T     b     i     z  /   �  F   �                    	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-04 19:38+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100% Autor Copyright 2015 Harald Sitter Harald Sitter Ninguna aplicación reproduce sonido Ninguna aplicación graba sonido Ningún perfil de dispositivo disponible Ningún dispositivo de entrada disponible Ningún dispositivo de salida disponible Perfil: PulseAudio Avanzado Aplicaciones Dispositivos Añadir dispositivo de salida virtual para la salida simultánea en todas las tarjetas de sonido locales Configuración avanzada de salida Cambiar automáticamente todos los canales en uso cuando esté disponible una nueva salida Capturar Por omisión Perfiles de dispositivos ecuadra@eloihr.net Entradas Silenciar sonido Eloy Cuadra Sonidos de notificación Salidas Reproducción Puerto  (no disponible)  (sin conectar) Necesita el mulo «module-gconf» de PulseAudio Este módulo le permite configurar el subsistema de sonido PulseAudio. %1: %2 100% %1% 