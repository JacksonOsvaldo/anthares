��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     S	     l	     �	  6  �	    �
  (   �  �   �     �     �     �  |   �  �   9     �       '   #     K      e                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-08-15 11:13+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
com>
First-Translator: Pablo de Vicente <pvicentea@nexo.es>
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 &Terminar sesión actual &Reiniciar el equipo A&pagar el equipo <h1>Gestor de sesiones</h1> Aquí puede configurar el gestor de sesiones, incluyendo opciones como si al cerrar sesión (terminar) se debe solicitar confirmación, si se debe restaurar la sesión previa al acceder a la cuenta y si, por omisión, el equipo debe apagarse automáticamente tras cerrar la sesión. <ul>
<li><b>Restaurar sesión anterior:</b> Guarda todas las aplicaciones al salir y las restaura en el próximo inicio.</li>
<li><b>Restaurar sesión guardada manualmente: </b> Le permite que la sesión sea guardada en cualquier momento vía «Guardar sesión» en el menú K. Esto significa que las aplicaciones en ejecución en ese momento reaparecerán en el próximo inicio.</li>
<li><b>Iniciar con una sesión vacía:</b> No guarda nada. Arrancará con un escritorio vacío en el siguiente inicio.</li>
</ul> Aplicaciones a e&xcluir de las sesiones: Seleccione esta opción si desea que el gestor de sesiones muestre un cuadro de diálogo solicitando confirmación para cerrar la sesión. Conf&irmar terminar Opción de salida por omisión General Aquí puede elegir lo que debería ocurrir por omisión cuando termine. Esto solo tiene sentido, si accede a través de KDM. Puede añadir una lista de aplicaciones separadas por comas que no se guardarán en las sesiones y que no serán iniciadas cuando se restaure la sesión. Por ejemplo «xterm,xconsole». O&frecer opciones de apagado Al iniciar sesión Restaurar sesión &guardada manualmente Restaurar sesión &previa Comenzar con una sesión &vacía 