��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     .     C     W     ]  "   d     �  Q   �     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2012-09-27 19:08+0200
Last-Translator: Javier Viñal <fjvinal@gmail.com>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Palabra ac&tivadora: Añadir un elemento Alias Alias: Configuración de Character Runner Código Crea caracteres a partir de :q: si es un código hexadecimal o un alias definido. Borrar el elemento Código hexadecimal: 