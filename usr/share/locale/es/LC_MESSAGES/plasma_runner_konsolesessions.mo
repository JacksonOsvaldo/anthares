��          4      L       `   $   a   /   �   �  �   0   �  1   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-05-25 09:52+0200
Last-Translator: Enrique Matias Sanchez (aka Quique) <cronopios@gmail.com>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
 Busca sesiones de Konsole que coincidan con :q:. Lista todas las sesiones de Konsole de su cuenta. 