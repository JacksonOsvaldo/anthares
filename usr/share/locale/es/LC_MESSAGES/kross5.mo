��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     U     [     z     �     �     �  
   �     �     �     �     �     �  	   	      '	  9   H	  C   �	  %   �	  *   �	     
      
     '
  '   4
     \
     h
     p
  &   �
     �
      �
     �
  &   �
       .        E  A   L     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-04 00:33+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
First-Translator: Boris Wesslowski <Boris@Wesslowski.com>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer Es script a ejecutar. General Añadir un nuevo script. Añadir... ¿Cancelar? Comentario: ecuadra@eloihr.net Editar Editar el script seleccionado. Editar... Ejecutar el script seleccionado. No fue posible crear un script para el intérprete «%1» Fallo al determinar el intérprete para el archivo de script «%1» Fallo al cargar el intérprete «%1» Fallo al abrir el archivo de script «%1» Archivo: Icono: Intérprete: Nivel de seguridad del intérprete Ruby Eloy Cuadra Nombre: No existe la función «%1» No se encuentra ese intérprete «%1» Eliminar Eliminar el script seleccionado. Ejecutar El archivo de script «%1» no existe. Detener Detener la ejecución del script seleccionado. Texto: Utilidad de la línea de órdenes para ejecutar scripts de Kross. Kross 