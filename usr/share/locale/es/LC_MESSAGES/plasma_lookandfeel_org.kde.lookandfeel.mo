��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
     �
        (   	     2     9  
   J  (   U     ~     �     �  ?   �            !   -     O     ^  $   n     �     �     �     �     �  	   �  -   �          *  	   3     =     [  +   b     �  	   �     �     �     �  	   �     �     �               '     ?     F     X  '   `               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: plasma_lookandfeel_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-13 18:31+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Atrás Batería al %1% Cambiar distribución Teclado virtual Cancelar El bloqueo de mayúsculas está activado Cerrar Cerrar búsqueda Configurar Configurar los complementos de búsqueda Sesión de escritorio: %1 Otro usuario Disposición de teclado: %1 Cierre de sesión en 1 segundo Cierre de sesión en %1 segundos Inicio de sesión Inicio de sesión fallido Iniciar sesión como otro usuario Cerrar sesión Pista siguiente No se está reproduciendo multimedia No usado Aceptar Contraseña Reproducir o pausar multimedia Pista anterior Reiniciar Reinicio en 1 segundo Reinicio en %1 segundos Consultas recientes Eliminar Reiniciar Mostrar controles multimedia: Apagar Apagado en 1 segundo Apagado en %1 segundos Iniciar nueva sesión Suspender Cambiar Cambiar sesión Cambiar usuario Buscar... Buscar «%1»... Plasma, creado por KDE Desbloquear Desbloqueo fallido en TTY %1 (pantalla %2) TTY %1 Nombre de usuario %1 (%2) en la categoría de consultas recientes 