��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "  �  $  8   �     �          "     ?  #   R     v  8   �  ?   �  '                        
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-06-04 13:04+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Asegurando que la pantalla se bloquea antes de suspender Bloquear la sesión Bloqueador de pantalla Bloqueo de pantalla activado Pantalla bloqueada Tiempo de espera del salvapantallas Pantalla desbloqueada Fija los minutos tras los que se bloqueará la pantalla. Establece si la pantalla se bloqueará tras el tiempo indicado. El bloqueador de pantalla está dañado y ya no se puede desbloquear.
Para desbloquear, cambie a un terminal virtual (por ejemplo, Ctrl+Alt+F2),
inicie sesión allí y ejecute la orden:

loginctl unlock-session %1

A continuación, vuelva a cambiar a la sesión que estaba usando (Ctrl+Alt+F%2). 