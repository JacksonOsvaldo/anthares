��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  J   (  "   s  !   �     �  -   �  #   �  I   !  D   k  $   �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-22 19:48+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 El intento de realizar la acción «%1» ha fallado con el mensaje «%2». Siguiente reproducción multimedia Anterior reproducción multimedia Controlador multimedia Reproducir/pausar la reproducción multimedia Detener la reproducción multimedia El argumento «%1» para la acción «%2» falta o es de tipo incorrecto. El reproductor de medios «%1» no puede realizar la acción «%2». La operación «%1» es desconocida. Error desconocido. 