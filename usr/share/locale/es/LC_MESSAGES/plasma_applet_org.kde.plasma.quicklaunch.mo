��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     o  G   �     �     �     �     �          $     ,     ;     X     r     �     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-19 21:11+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Añadir lanzador... Añada lanzadores arrastrando y soltando o usando el menú de contexto. Aspecto Organización Editar lanzador... Activar ventana emergente Introduzca el título General Ocultar iconos Número máximo de columnas: Número máximo de filas: Lanzamiento rápido Eliminar lanzador Mostrar iconos ocultos Mostrar nombres de lanzadores Mostrar el título Título 