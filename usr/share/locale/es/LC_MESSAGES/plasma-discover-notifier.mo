��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     M  /   T  =   �  /   �  %   �  ^     ,   w     �  
   �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: muon-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-15 19:49+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1, %2 1 paquete a actualizar %1 paquetes a actualizar 1 actualización de seguridad %1 actualizaciones de seguridad 1 paquete a actualizar %1 paquetes a actualizar No hay que actualizar ningún paquete de los que 1 es una actualización de seguridad de los que %1 son actualizaciones de seguridad Hay actualizaciones de seguridad disponibles Sistema actualizado Actualizar Hay actualizaciones disponibles 