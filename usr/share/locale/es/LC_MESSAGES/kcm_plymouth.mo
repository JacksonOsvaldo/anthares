��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  "   >  ,   a  .   �  &   �     �  '   �  *     9   J     �     �     �  =   �     �  ;   	  =   I	     �	  K   �	  !   �	  m   
  6   y
     �
     �
  4   �
  C                       
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-06 14:24+0100
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 No se puede iniciar «initramfs». No se puede iniciar «update-alternatives». Configurar la pantalla de arranque de Plymouth Descargar nuevas pantallas de arranque ecuadra@eloihr.net Obtener nuevas pantallas de arranque... La ejecución de «initramfs» ha fallado. «initramfs» ha terminado con la condición de error %1. Instalar un tema. Marco Martin Eloy Cuadra No se ha indicado ningún tema en los parámetros auxiliares. Instalador de temas de Plymouth Seleccionar una pantalla de arranque global para el sistema El tema a instalar. Debe ser un archivo comprimido existente. El tema %1 no existe. Tema dañado: el archivo «.plymouth» no se ha encontrado dentro del tema. La carpeta del tema %1 no existe. Este módulo le permite configurar el aspecto del espacio de trabajo con algunos preajustes listos para usar. No se ha podido autenticar/ejecutar la acción: %1, %2 Desinstalar Desinstalar un tema. La ejecución de «update-alternatives» ha fallado. «update-alternatives» ha terminado con la condición de error %1. 