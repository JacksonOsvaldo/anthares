��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �          &     4     I     Q     `     i     p     ~  6   �     �     �                 	   (  '   2  (   Z  '   �  (   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-18 13:13+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 archivo %1 archivos %1 carpeta %1 carpetas %1 de %2 procesados %1 de %2 procesados a %3/s %1 procesados %1 procesados a %2/s Aspecto Comportamiento Cancelar Borrar Configurar... Trabajos terminados Lista de transferencias/trabajos en marcha (kuiserver) Moverlos a una lista diferente Moverlos a una lista diferente. Pausar Eliminarlos Eliminarlos. Continuar Mostrar todos los trabajos en una lista Mostrar todos los trabajos en una lista. Mostrar todos los trabajos en un árbol Mostrar todos los trabajos en un árbol. Mostrar ventanas separadas Mostrar ventanas separadas. 