��    �      t  �   �      �  
   �  
   �     �     �     �     �     �                         ,     ?     K     O     ]     k     r     w     }     �     �     �     �     �     �     �     �     �     �          &     :     X     `     o     ~     �     �     �     �     �     �     �     �     �     �     �  	   �     �  
   �     �  	          	   $     .     :     K  
   R     ]     m     {     �  
   �     �     �     �  	   �     �     �     �     �     �     �     �     
     !     '  	   -     7     D     J     Z     h     u     |     �     �     �     �  
   �     �     �     �  
   �     �       	        &     .  
   3     >  	   K  
   U     `     e     j     p     u     �     �     �     �     �     �     �     �     �     �     	  	        $     )     :     F     K     S     c     u     |     �     �     �     �  	   �     �     �     �  �  �  	   �     �     �     �     �     �     �     �  	   �                    1     =     A     M     [     b     g     m     u     �  	   �     �     �     �     �     �                     6  !   J     l     t     �     �     �     �     �     �     �     �     �     �     �     �     �     �               (     1     9     L     ]     q  
   �     �     �     �     �     �  
   �     �            
             =     C     G     P     Y     `  !   u     �     �     �     �     �     �     �     �               '     6     D     J  	   ]     g     |  
   �  	   �     �     �  
   �     �     �     �  
     
     
        )     .     3     9     A     M     T     m     �     �     �     �     �     �     �               .     3     H     Y     ^     j          �     �     �  
   �     �  	   �  
   �     �     �     	        ^           	   +                y   A       2       n   i       /       �   :       b   �   g       w             B   j   �   1   L   6   <   �   �   8       K   �   ]   E   0   Q   s              D       V      Y      %   G      !   R       X       l   3   '   >          $               ;   O       m   �   q       P   u       |   �       {          @   W   .       f   a                  T   c       (   ?       4       ~      *       [   r   �          I   )      \   d   J   7   p                  -   N            F   `           �   h   H      "   M   C       5   ,       
          k   }   e           =   x   9   z             &              �   _   �   Z       S   o   t   U                           v       #        Accessible Appendable Audio Available Content BD BD-R BD-RE Battery Blank Block Blu Ray Recordable Blu Ray Rewritable Blu Ray Rom Bus CD Recordable CD Rewritable CD Rom CD-R CD-RW Camera Camera Battery Can Change Frequency Capacity Cdrom Drive Charge Percent Charge State Charging Compact Flash DVD DVD Plus Recordable DVD Plus Recordable Duallayer DVD Plus Rewritable DVD Plus Rewritable Duallayer DVD Ram DVD Recordable DVD Rewritable DVD Rom DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Data Description Device Device Types Disc Type Discharging Drive Type Emblems Encrypted Encrypted Container File Path File System File System Type Floppy Free Space Free Space Text Fully Charged HD DVD Recordable HD DVD Rewritable HD DVD Rom HDDVD HDDVD-R HDDVD-RW Hard Disk Hotpluggable Icon Ide Ieee1394 Ignored In Use Keyboard Battery Keyboard Mouse Battery Label Major Max Speed Memory Stick Minor Monitor Battery Mouse Battery Not Charging Number Operation result Optical Drive OpticalDisc Other PDA Battery Parent UDI Partition Table Phone Battery Platform Plugged In Portable Media Player Primary Battery Processor Product Raid Read Speed Rechargeable Removable Rewritable Sata Scsi SdMmc Size Smart Media State Storage Access Storage Drive Storage Volume Super Video CD Supported Drivers Supported Media Supported Protocols Tape Temperature Temperature Unit Timestamp Type Type Description UPS Battery UUID Unknown Unknown Battery Unknown Disc Type Unused Usage Usb Vendor Video Blu Ray Video CD Video DVD Write Speed Write Speeds Xd Project-Id-Version: plasma_engine_soliddevice
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-04-04 19:24+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Accesible Permite añadir Audio Contenido disponible BD BD-R BD-RE Batería En blanco Bloque Blu Ray grabable Blu Ray regrabable Blu Ray ROM Bus CD Grabable CD Regrabable CD ROM CD-R CD-RW Cámara Batería de la cámara Puede cambiar la frecuencia Capacidad Unidad de CDROM Porcentaje de carga Estado de la carga Cargando Compact Flash DVD DVD Plus grabable DVD Plus grabable de doble capa DVD Plus regrabable DVD Plus regrabable de doble capa DVD RAM DVD Grabable DVD Regrabable DVD ROM DVD+DL DVD+DLRW DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Datos Descripción Dispositivo Tipos de dispositivos Tipo de disco Descargando Tipo de unidad Emblemas Cifrado Contenedor cifrado Ruta del archivo Sistema de archivos Tipo de sistema de archivos Disquetera Espacio libre Texto de espacio libre Totalmente cargada HD DVD grabable HD DVD regrabable HD DVD ROM HDDVD HDDVD-R HDDVD-RW Disco duro Se puede conectar en caliente Icono IDE IEEE1394 Ignorado En uso Batería del teclado Batería del ratón y del teclado Etiqueta Mayor Velocidad máx. Memory Stick Menor Batería del monitor Batería del ratón No está en carga Número Resultado de la operación Unidad óptica Disco óptico Otros Batería de la PDA UDI padre Tabla de particiones Batería del teléfono Plataforma Conectado Reproductor de medios portátil Batería primaria Procesador Producto Raid Velocidad de lectura Recargable Extraíble Regrabable SATA SCSI SdMmc Tamaño Smart Media Estado Acceso de almacenamiento Dispositivo de almacenamiento Volumen de almacenamiento Super Vídeo CD Controladores permitidos Medios soportados Protocolos permitidos Cinta Temperatura Unidad de temperatura Marca de tiempo Tipo Descripción de tipo Batería del SAI UUID Desconocido Batería desconocida Tipo de disco desconocido No usado Uso USB Fabricante Vídeo Blu Ray Vídeo CD Vídeo DVD Velocidad de escritura Velocidades de escritura XD 