��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  k     V
     o
  8   �
  .   �
     �
     �
          !  *   2  f   ]     �     �     �     �               %     4  	   B  	   L     V     ]     m     u                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-07 12:24+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 <p>Ha elegido abrir otra sesión de escritorio.<br />La sesión actual se ocultará y se mostrará una nueva pantalla de identificación.<br />Se asignará una tecla de función a cada sesión. F%1 suele asignarse a la primera sesión, F%2 a la segunda, y así sucesivamente. Puede cambiar entre sesiones pulsando simultáneamente las teclas Ctrl, Alt y la tecla de función adecuada. Además, dispone de acciones para cambiar de sesión en los menús del panel y del escritorio de KDE.</p> Lista todas las sesiones Bloquear la pantalla Bloquea las sesiones actuales e inicia el salvapantallas Sale, cerrando la sesión de escritorio actual Nueva sesión Reinicia el equipo Reiniciar el equipo Apagar el equipo Inicia una nueva sesión como otro usuario Cambia a la sesión activa para el usuario :q:, o lista todas las sesiones activas si no se indica :q: Apaga el equipo sesiones Advertencia: nueva sesión bloquear cerrar la sesión Cerrar la sesión cerrar sesión nueva sesión reiniciar reiniciar apagar cambiar usuario cambiar cambiar :q: 