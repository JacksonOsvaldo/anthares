��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  1   �  :     B   C  4   �  ,   �  /   �           9  *   K         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-04-20 10:39+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 No se ha podido detectar el tipo MIME del archivo No se han podido encontrar todas las funciones solicitadas No se ha podido encontrar un proveedor con el destino especificado Ha ocurrido un error al tratar de ejecutar el script Ruta no válida para el proveedor solicitado No ha sido posible leer el archivo seleccionado El servicio no estaba disponible Error desconocido Debe especificar un URL para este servicio 