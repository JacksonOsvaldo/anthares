��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     �     �  2   �  M   �  J   :  F   �  L   �  J   	  P   d	     �	     �	     �	     �	     �	     �	  	   �	     
      
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-07 12:21+0200
Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>
Language-Team: Spanish <kde-l10n-es@kde.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
com>
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Bloquear el contenedor Expulsar medio Busca dispositivos cuyos nombres coinciden con :q: Lista todos los dispositivos y permite montarlos, desmontarlos o expulsarlos. Lista todos los dispositivos que se pueden expulsar y permite expulsarlos. Lista todos los dispositivos que se pueden montar y permite montarlos. Lista todos los dispositivos que se pueden desmontar y permite desmontarlos. Lista todos los dispositivos que se pueden bloquear y permite bloquearlos. Lista todos los dispositivos que se pueden desbloquear y permite desbloquearlos. Montar el dispositivo dispositivo expulsar bloquear montar desbloquear desmontar Desbloquear el contenedor Desmontar el dispositivo 