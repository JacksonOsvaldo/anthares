��          �      l      �  �   �  �   �  �   B     �     �     �     �     �     �          
           %  	   .     8     >     Q     e     l     s  �  |  �   .  �   �  �   �     �     �     �     �     �     �     �  !   �     �     �  
   �     	  &   	     7	     I	     P	  	   `	                                    
                	                                              <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> Author: BSD Description: Details Error GPL Install Key used for signing: LGPL License: Password: Reset Select Signing Key Share Hot New Stuff Title: Update Version: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2006-06-22 16:29+0200
Last-Translator: Michel Ludwig <michel.ludwig@kdemail.net>
Language-Team: Luxembourgish <kde-i18n-lb@kde.org>
Language: lb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.2
Plural-Forms: nplurals=2; plural=n != 1;
 <qt>Kann de <i>gpg</i> net luede fir d'Validitéit vun der Datei ze préifen. Kuckt w.e.g. no, ob den <i>gpg</i> installéiert ass, well soss kënnen déi erofgeluede Ressourcen net verifizéiert ginn.</qt> <qt>Kann de <i>gpg</i> net luede fir d'verfügbar Schlësselen nozefroën. Kuckt w.e.g. no, ob den <i>gpg</i> installéiert ass, well soss kënnen déi erofgeluede Ressourcen net verifizéiert ginn.</qt> <qt>Kann de <i>gpg</i> net luede fir d'Datei z'ënnerschreiwen. Kuckt w.e.g. no, ob den <i>gpg</i> installéiert ass, well soss kënnen déi erofgeluede Ressourcen net ënnerschriwwe ginn.</qt> Auteur: BSD Beschreiwung: Detailer Feeler GPL Installéieren Schlëssel fir z'ënnerschreiwen: LGPL Lizenz: Passwuert: Zerécksetzen Schlëssel fir d'Ënnerschrëft wielen Nei Saache sharen Titel: Aktualiséieren Versioun: 