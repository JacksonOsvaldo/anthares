��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     Q  *   U      �  *   �  )   �     �       !        @     X  6   u  7   �  N   �  C   3	  '   w	  4   �	  5   �	  1   

  &   <
  8   c
     �
  .   �
     �
     �
     �
  	   �
       1        P  "   ]                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2016-11-01 15:58+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: ms
Plural-Forms: nplurals=2; plural=n != 1;
  ms &Visibilidade das combinações de teclas: &Tipo do botão da seta de topo: Esconder Sempre as Combinações de Teclas Mostrar Sempre as Combinações de Teclas Duração das &animações: Animações T&ipo do botão da seta do fundo: Configuração da Brisa Centrar as páginas na barra Arrastar as janelas a partir de todas as áreas vazias Arrastar as janelas apenas a partir da barra de título Arrastar as janelas a partir da barra de título, menu e barras de ferramentas Desenhar uma linha fina a indicar o foco nos menus e barras de menu Desenhar o indicador de foco nas listas Desenhar contornos à volta dos painéis acopláveis Desenhar contornos à volta dos títulos das páginas Desenhar contornos à volta dos painéis laterais Desenhar as marcas da barra deslizante Desenhar o separador dos itens das barras de ferramentas Activar as animações Activar as pegas de dimensionamento extendidas Molduras Geral Sem Botões Um Botão Barras de deslocamento Mostrar as Combinações de Teclas se Necessário Dois Botões Modo de arrastamento das &janelas: 