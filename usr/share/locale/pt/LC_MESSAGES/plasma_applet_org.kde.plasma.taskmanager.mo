��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �     �     �     �     �          
     $     C     X     w  A   �  A   �               &  	   5     ?     U  )   n     �     �     �     �     �     �     �               0     4     P      m     �  
   �     �  ,   �     �     �  
      !        -     ;     V     o  	   �     �     �     �     �     �  ,   �          .     4     ;     A     P     _     d     q     x  .   �  8   �  )   �  %   '  D   M     �     �     �     �     �     �     �     �     �     �  #           L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-16 16:18+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: &Pin
Plural-Forms: nplurals=2; plural=n != 1;
 To&dos os Ecrãs Fe&char &Ecrã Completo &Mover &Novo Ecrã &Fixar Enro&lar &%1 - %2 Também disponível em %1 Adicionar à Actividade Actual Todas as Actividades Permitir agrupar este programa Alfabeticamente Organizar sempre as tarefas em colunas com este número de linhas Organizar sempre as tarefas em linhas com este número de colunas Disposição Comportamento Por Actividade Por Ecrã Pelo Nome do Programa Fechar a Janela ou Grupo Circular pelas tarefas com a roda do rato Não Agrupar Não Ordenar Filtros Esquecer os Documentos Recentes Geral Agrupamento e Ordenamento Agrupamento: Realçar as janelas Tamanho dos ícones: — Manter por Cim&a das Outras Manter por &Baixo das Outras Manter os lançamentos separados Grande Ma&ximizar Manualmente Marcar as aplicações que reproduzem áudio Máximo de colunas: Máximo de linhas: Mi&nimizar Minimizar/Repor a Janela ou Grupo Mais Acções Mover para o Ecrã Ac&tual Mover para a &Actividade Mover Para o &Ecrã Silenciar Nova Instância Em %1 Em Todas as Actividades À Actividade Actual Com o botão do meio: Agrupar apenas com o gestor de tarefas cheio Abrir os grupos em janelas Repor 9 999+ Pausa Faixa Seguinte Faixa Anterior Sair &Dimensionar Soltar Mais %1 local Mais %1 locais Mostrar apenas as tarefas da actividade actual Mostrar apenas as tarefas do ambiente de trabalho actual Mostrar apenas as tarefas do ecrã actual Mostrar apenas as tarefas minimizadas Mostrar a informação de progresso e estado nos botões das tarefas Mostrar as dicas Pequeno Ordenação: Iniciar uma Nova Instância Tocar Parar Nada &Fixar Agrupar/Desagrupar Disponível em %1 Disponível em todas as actividades 