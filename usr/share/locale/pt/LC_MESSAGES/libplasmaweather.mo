��          <      \       p      q   *   �   /   �   �  �   0   �  D   �  I   D                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2010-05-09 14:23+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: inHg mph Fahrenheit kPa Hecto bft hPa mbar Beaufort
 Não é possível encontrar o '%1'; a usar o %2. A ligação com o servidor meteorológico %1 expirou o tempo-limite. A obtenção da informação meteorológica de %1 expirou o tempo-limite. 