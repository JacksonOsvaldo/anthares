��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  O   0  "   �  "   �     �     �  !   �  C     E   b  "   �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-18 12:09+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 A tentativa de execução da acção '%1' foi mal-sucedida com a mensagem '%2'. Reprodução do conteúdo seguinte Reprodução do conteúdo anterior Controlador Multimédia Reproduzir/pausar o conteúdo Parar a reprodução do conteúdo Falta o argumento '%1' para a acção '%2' ou é de um tipo errado. Não é possível ao leitor multimédia '%1' efectuar a acção '%2'. A operação '%1' é desconhecida. O erro é desconhecido. 