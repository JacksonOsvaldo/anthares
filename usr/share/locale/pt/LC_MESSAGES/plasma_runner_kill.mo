��          �            h     i     r     �  	   �  *   �     �  "   �                 ;   +     g     z       �  �     L     Y     r     �  1   �  .   �  1   �     1     C     U  S   c     �     �     �     	                 
                                                          &Sort by &Trigger word: &Use trigger word CPU usage It is not sure, that this will take effect Kill Applications Config Process ID: %1
Running as user: %2 Send SIGKILL Send SIGTERM Terminate %1 Terminate running applications whose names match the query. inverted CPU usage kill nothing Project-Id-Version: plasma_runner_kill
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-10-29 12:28+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: SIGTERM SIGKILL
Plural-Forms: nplurals=2; plural=n != 1;
 &Ordenar por Palavra de ac&tivação: &Usar a palavra de activação Carga do CPU Se não tiver a certeza, então isto fará efeito Configuração do Encerramento de Aplicações ID do Processo: %1
Executado com o utilizador: %2 Enviar um SIGKILL Enviar um SIGTERM Terminar o %1 Terminar as aplicações em execução que corresponderem ao critério de pesquisa. carga do CPU invertida matar nada 