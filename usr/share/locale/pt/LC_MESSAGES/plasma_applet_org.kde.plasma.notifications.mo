��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	  "   �
     �
  0   	  5   :     p     y     �     �     �     �     �     �       
   !     ,     5     9     F     Y     j     {     �     �     �     �  (   �  5        ;  7   I  ?   �     �     �     �     �        
                          "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-12-23 17:11+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: KiB MiB GiB TiB min
 %1 notificação %1 notificações %1 em %2 %3 %1 tarefa em execução %1 tarefas em execução &Configurar as Notificações e Acções do Evento... Há 10 s Há 30 s Mostrar os Detalhes Esconder os Detalhes Limpar as Notificações Copiar Copiar o Endereço da Ligação 1 pasta %2 de %1 pastas 1 ficheiro %2 de %1 ficheiros Histórico %1 em %2 +%1 Informação Tarefa sem Sucesso Tarefa Terminada Mais Opções... Sem notificações novas. Sem notificações nem tarefas Abrir... %1 (Em Pausa) Seleccionar Tudo Mostrar um histórico das notificações Mostrar as notificações da aplicação e do sistema %1 (falta %2) Seguir as transferências de ficheiros e outras tarefas Usar uma posição personalizada para a área de notificações Há %1 min Há %1 min Há %1 dia Há %1 dias Ontem Agora mesmo %1: %1: Falhou %1 Terminada 