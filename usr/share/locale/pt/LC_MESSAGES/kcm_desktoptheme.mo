��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �  )   2     \     h     z     �     �     �     �  -   �  (     !   /  "   Q  C   t         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-17 10:22+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Configurar o Tema do Ambiente de Trabalho David Rosca zepires@gmail.com Obter Temas Novos... Instalar de um Ficheiro... José Nuno Pires Abrir um Tema Remover o Tema Ficheiros de Temas (*.zip *.tar.gz *.tar.bz2) A instalação do tema foi mal-sucedida. O tema foi instalado com sucesso. Não foi possível remover o tema. Este módulo permite-lhe configurar o tema do ambiente de trabalho. 