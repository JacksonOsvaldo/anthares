��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  !   �	     �	  
   �	     �	     
  
   
     )
     0
     4
     A
     O
     a
     i
  #   |
     �
     �
     �
     �
     �
     �
               )     ;     K     b  
   o     z     �     �     �     �     �     �  
   �     �     �     �     �  P        W     i  
   �     �     �  "   �     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-17 11:44+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Kai Wh Uwe Broulik
Plural-Forms: nplurals=2; plural=n != 1;
 % %1 %2 %1: Consumo de Energia da Aplicação Bateria Capacidade Percentagem da Carga Estado da carga A Carregar Actual °C Detalhes: %1 A Descarregar zepires@gmail.com Energia Consumo de Energia Estatísticas de Consumo de Energia Ambiente Desenho completo Carga completa Tem fonte de alimentação Kai Uwe Broulik Últimas 12 horas Últimas 2 horas Últimas 24 horas Últimas 48 horas Últimos 7 dias Última carga completa Última hora Fabricante Modelo José Nuno Pires Não Não está em carga PID: %1 Localização: %1 Recarregável Actualizar Número de Série W Sistema Temperatura Este tipo de histórico não está disponível de momento para este dispositivo. Período de tempo O período de tempo a mostrar Fabricante V Tensão Activações por segundo: %1 (%2%) W Wh Sim Consumo % 