��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �     �     �      	  	   	     !	     0	     O	     a	     n	     t	     {	     �	     �	  	   �	     �	     �	  6   �	  8   "
     [
     x
     �
     �
     �
  $   �
     �
     �
               *     2     9     >        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-20 11:40+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 % Ajustar o volume de %1 Aplicações Áudio Silenciado Volume do Áudio Comportamento Dispositivos de Captura Sequências de Captura Silenciar Predefinição Diminuir o Volume do Microfone Diminuir o Volume Dispositivos Geral Portas Aumentar o Volume do Microfone Aumentar o Volume Volume máximo: Silenciar Silenciar o %1 Silenciar o Microfone Não estão aplicações a reproduzir ou gravar áudio Não foram encontrados dispositivos de entrada ou saída Dispositivos de Reprodução Sequências de Reprodução  (indisponível)  (desligada) Aumentar o volume máximo Mostrar as opções adicionais do %1 Volume Volume a %1% Reacção ao volume Passo do volume: %1 (%2) %1: %2 100% %1% 