��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     K     N  !   V     x     �     �     �  )   �  *   �          $  	   2     <     M     Z     x  
   �     �     �     �     �     �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: kcm_nightcolor
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-12 10:27+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: mín Roman Gilg máx HH
Plural-Forms: nplurals=2; plural=n != 1;
  K (HH:MM) (Em minutos - mín. 1, máx. 600) Activar a Cor Nocturna Automática Detectar a localização zepires@gmail.com Erro: A manhã não é anterior à noite. Erro: O tempo de transição sobrepõe-se. Latitude Localização Longitude José Nuno Pires Cor Nocturna Temperatura da Cor Nocturna:  Modo de operação: Roman Gilg O Sol nasce às O Sol põe-se às Vezes Duração da transição e termina às 