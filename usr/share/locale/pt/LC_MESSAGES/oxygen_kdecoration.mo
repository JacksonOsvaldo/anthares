��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  *   �     �     �     �     �     �     �  
   �     �               $     +     3     @  	   X  ;   b  D   �     �     �       +        F     M     d  y   m  Q   �     9  "   Q     t     {  .   �  "   �     �     �  %   �  *        >     G     N  *   T  #        �  0   �  $   �       )        >     F     N  	   g  ;   q  -   �     �  $   �          2     F  #   `     �  3   �  &   �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-13 10:40+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: ms Oxygen
Plural-Forms: nplurals=2; plural=n != 1;
 Corresponde&nte à propriedade da janela:  Enorme Grande Sem Contorno Sem Contornos Laterais Normal Ainda Maior Minúsculo Mais do que Enorme Muito Grande Grande Normal Pequeno Muito Grande Brilho da Janela Activa Adicionar Adicionar uma pega para dimensionar as janelas sem contorno Permitir dimensionar as janelas maximizadas nos extremos das janelas Animações Tamanho dos &botões: Tamanho do contorno: Transição à passagem do rato nos botões Centro Centro (Largura Total) Classe:  Configurar a transição desvanecida entre a sombra e o brilho de uma janela, quando muda o estado de actividade da mesma Configurar a animação de realce à passagem do cursor para os botões da janela Opções de Decoração Detectar as Propriedades da Janela Janela Editar Editar a Excepção - Configuração do Oxygen Activar/desactivar esta excepção Tipo de Excepção Geral Esconder a barra de título da janela Informação Acerca da Janela Seleccionada Esquerda Descer Subir Nova Excepção - Configuração do Oxygen Pergunta - Configuração do Oxygen Expressão Regular A sintaxe da expressão regular está incorrecta Expressão regular a &corresponder:  Remover Deseja remover a excepção seleccionada? Direita Sombras A&linhamento do título: Título:  Usar as mesmas cores para o título e o conteúdo da janela Usar a classe da janela (aplicação inteira) Usar o título da janela Atenção - Configuração do Oxygen Nome da Classe da Janela Sombreado da Janela Identificação da Janela Selecção da Propriedade da Janela Título da Janela Transições de mudança de estado da janela activa Substituições Específicas da Janela 