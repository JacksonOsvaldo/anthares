��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  
   l     w     �     �  <   �     �  %   �          (     <     E     M     l  	   �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2014-11-13 11:37+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: svgz jpg
 Aparência Escolher... Escolher uma imagem Puzzle dos Quinze Ficheiros de Imagens (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Cor do número Localização da imagem personalizada Cor da peça Mostrar os números Baralhar Tamanho Resolva ao organizar por ordem Resolvido! Tente de novo. Tempo: %1 Usar uma imagem personalizada 