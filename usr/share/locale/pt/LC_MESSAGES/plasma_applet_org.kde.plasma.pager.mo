��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �    �  -   �     %  (   <     e     x     �     �     �     �     �     �     �  
   �     
       	         *     A     Y  "   x     �     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-25 11:43+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: pre wrap white space buttonGroupAction
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: buttonGroupText
X-POFile-IgnoreConsistency: Display:
 %1 Janela Minimizada: %1 Janelas Minimizadas: %1 Janela: %1 Janelas: ...e mais %1 janela ...e mais %1 janelas Nome da actividade Número da actividade Adicionar um Ecrã Virtual Configurar os Ecrãs... Nome do ecrã Número do ecrã Mostrar: Não fazer nada Geral Horizontal Ícones Disposição: Sem texto Apenas no ecrã actual Remover o Ecrã Virtual Ao seleccionar o ecrã actual: Mostrar o Gestor de Actividades... Mostra o ecrã Predefinida Vertical 