��    -      �  =   �      �  Z   �  *   <     g     �     �     �     �     �  >   �  B   .     q  9   �     �     �      �               9  ;   R  -   �  )   �     �     �          6     O  -   h     �  !   �     �     �               9     T     k     �     �  >   �  (   �     	  >   )	  &   h	  &   �	  �  �	     �"  3   �"     �"  "   #  "   '#     J#  (   i#     �#  Q   �#  W   �#     C$  U   a$     �$  $   �$  0   �$  *   %  .   F%  &   u%  =   �%  =   �%  ;   &  %   T&  .   z&  *   �&  *   �&  +   �&  <   +'  "   h'  ,   �'  )   �'  +   �'  '   (  $   6(  )   [(     �(  $   �(     �(  !   �(  8   )  &   @)     g)  =   �)  ,   �)  -   �)            	          '   
                      "                          (          -   %       *                     +                        $               !      &                 #   ,          )              %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not a function and cannot be called. %1 is not an Object type '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Alert Bad event handler: Object %1 Identifier %2 Method %3 Type: %4. Bad slot handler: Object %1 Identifier %2 Method %3 Signature: %4. Call to '%1' failed. Call to method '%1' failed, unable to get argument %2: %3 Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Error encountered while processing include '%1' line %2: %3 Exception calling '%1' function from %2:%3:%4 Exception calling '%1' slot from %2:%3:%4 Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' Failure to cast to %1 value from Type %2 (%3) File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. No such method '%1'. Not enough arguments. The slot asked for %1 argument The slot asked for %1 arguments There was an error reading the file '%1' Wrong object type. but there is only %1 available but there are only %1 available include only takes 1 argument, not %1. library only takes 1 argument, not %1. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-10-15 10:38+0100
Last-Translator: José Nuno Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: &Discard
X-POFile-IgnoreConsistency: Enter
X-POFile-IgnoreConsistency: Author
X-POFile-IgnoreConsistency: Open &File
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-Allow: 2
X-POFile-IgnoreConsistency: Far
X-POFile-SpellExtra: Hspell Koenig KHTML Thaana Telugu KScript Sycoca
X-POFile-SpellExtra: artsmessage KDEInit SOCKS kcmkresources
X-POFile-SpellExtra: ChavePrivadaFalhou KIOTest Han Thaani Jumaada Hangul
X-POFile-SpellExtra: Cherokee Meh KSpell chaveSessao TestWritevCard
X-POFile-SpellExtra: Thulatha Javascript Tagbanwa End Tagalog LTR Oriya
X-POFile-IgnoreConsistency: H:
X-POFile-IgnoreConsistency: Untrusted
X-POFile-IgnoreConsistency: Export
X-POFile-SpellExtra: Hiragana Backspace Print PgUp Ins
X-POFile-SpellExtra: ModificadoresEspaço Yi Lao Return gpg Caps Lock kab
X-POFile-SpellExtra: aRts tags Buhid Insert Gurmukhi Malayalam Scroll
X-POFile-SpellExtra: Delete Ogham PgDn Kannada Tab Home Katakana SysReq
X-POFile-SpellExtra: KConvertTest Khmer OutraOpção Bopomofo
X-POFile-SpellExtra: MarcasCombinatórias Enter UmaOpção Devanagari
X-POFile-SpellExtra: Hanunoo Sinhala JanelaAutoExemplo Lars Ian help
X-POFile-IgnoreConsistency: Try Different
X-POFile-IgnoreConsistency: Delete
X-POFile-IgnoreConsistency: Comment
X-POFile-IgnoreConsistency: &Restore
X-POFile-IgnoreConsistency: Reset
X-POFile-IgnoreConsistency: 0.1
X-POFile-IgnoreConsistency: Forward
X-POFile-SpellExtra: Kanbun CTRL Klash Syloti JS Jan TETest QObject
X-POFile-SpellExtra: Sebastian Geiser Far kdeinit Weis Mordad Yau Hausmann
X-POFile-SpellExtra: execprefix autostart Dirk Nov Elul shanbe Farvardin
X-POFile-SpellExtra: KApplication bin tagcloudtest displayname IFrame yo
X-POFile-SpellExtra: Aza Adar Sáb dah XIM Sha Sonnet testregression Jamo
X-POFile-SpellExtra: Shawwal Bah KConf IPA Hijri Sab Testkhtml Jeroen
X-POFile-SpellExtra: QWidget dumps KJSEmbed Arb qttest stderr Kho ban Kha
X-POFile-SpellExtra: PathLengthExceeded Tai shn Ago KrossTest Ithnain
X-POFile-SpellExtra: klauncher tempfile Aban frame ThreadWeaver Kun yeyo
X-POFile-SpellExtra: Buginese Lue Kislev Khamees home Jumma XDG Khordad
X-POFile-SpellExtra: Zemberek KAboutData Wijnhout Sivan Saami Method Qua
X-POFile-SpellExtra: Molkentin PTY Koivisto onthespot Ord Shvat Jom
X-POFile-SpellExtra: KMultiPart Ahad CJK Aspell Tifinagh NoCARoot Tishrey
X-POFile-SpellExtra: Up KDXSView ModRunner subtexto aifamily Panj path
X-POFile-SpellExtra: NumLock keramik GHNS TestRegressionGui Yek Iyar Ahd
X-POFile-SpellExtra: khtmltests Torben QApplication overthespot caption
X-POFile-SpellExtra: khtml desktop Ispell QWS create Faure Object Limbu
X-POFile-SpellExtra: KLauncher Sauer Hijjah Myanmar NEC BCC Fev Kelly
X-POFile-SpellExtra: Jumee Stephan TestRegression Knoll frames HOME Jum
X-POFile-SpellExtra: DISPLAY KNewStuff Awal Rajab pt plastik InvalidHost
X-POFile-SpellExtra: kdemain STDOUT Jun Jul Kulow Yaum pa Chahar widgets
X-POFile-SpellExtra: man KUnitTest pm KDEPIM TAB Waldo CL CC Balinês
X-POFile-SpellExtra: Nagri Kangxi QLayout qtplugininstance regression
X-POFile-SpellExtra: multipart Jalali Phags Set servname nograb
X-POFile-SpellExtra: International Frame CGIs Stylesheet Library Sex Seg
X-POFile-SpellExtra: KDontChangeTheHostName SO toner Yijing Peter Out
X-POFile-SpellExtra: InvalidCA Le Khmeres Tevet Ordibehesht Anton am al
X-POFile-SpellExtra: Tir Tuebingen Esf Abr ini KLocale KiB WMNET Dingbats
X-POFile-SpellExtra: InvalidPurpose kdehelp id Glagolitic factory Esfand
X-POFile-SpellExtra: Nisan kjs ErrorReadingRoot MiB Copta Shanbe Xvfb
X-POFile-SpellExtra: client Mai Bastian document config TiB Jones AC
X-POFile-SpellExtra: KBuildSycoca Bahman offthespot Mueller Tang ye Thu
X-POFile-SpellExtra: Sabt NKo aisocktype mixed Carriage Thl aiflags
X-POFile-SpellExtra: Muharram Reinhart Kontact Cantonês Page icon
X-POFile-SpellExtra: makekdewidgets ManyColor Heshvan Kross Ith bind Antti
X-POFile-SpellExtra: DXS Tamuz Shahrivar sessionId sh KJSCmd Av KLibLoader
X-POFile-SpellExtra: Mehr GiB Arbi dograb AssinaturaFalhou prefix
X-POFile-SpellExtra: Hexagramas ize AutoAssinado NãoConfiável Qi Down
X-POFile-SpellExtra: directory Índicas ise Oxygen info shared share usr
X-POFile-IgnoreConsistency: Separate Folders
X-POFile-SpellExtra: XDGDATADIRS KTTS Control PrtScr Hyper Sys Win Screen
X-POFile-SpellExtra: Req Break AltGr ReadOnly SHM EOF Re abc ABC QPL Kate
X-POFile-SpellExtra: Serif Sans KFormula URIs raster opengl favicons Solid
X-POFile-SpellExtra: Harald Fernengel KTTSD baseline Resource writeall
X-POFile-SpellExtra: Trüg YiB PiB YB ZB EB PB EiB ZiB GB TB KIdleTest
X-POFile-SpellExtra: Freddi KIdleTime Cha Āshwin Budhavãra Suk Paush
X-POFile-SpellExtra: Shrāvana Somavãra Phālgun Raviãra Phā Māg
X-POFile-SpellExtra: Bhādrapad Chaitra Māgh Sukravãra Āshādha
X-POFile-SpellExtra: Agrahayana Bud Shr Guruvãra Mañ Gur Vaishākh Jya
X-POFile-SpellExtra: Kārtik Agr Jyaishtha Kār Āsh Bhā Rav milisegundo
X-POFile-SpellExtra: Āsw Mañgalvã Sanivãra Mayek Ol Saurashtra
X-POFile-SpellExtra: Sundanês Viet Lisu Kayah Chiki Lepcha Meetei Cham
X-POFile-SpellExtra: Rejang Tham Bamum Pshoment Pas Ptiou Neh Genbot Hamus
X-POFile-SpellExtra: Pag Paope Pesnau Pes Hed Magabit Ehu Tahsas Yak Mag
X-POFile-SpellExtra: Tob Pef Kou Pam Pao Tequemt Paremhotep Psh nabot
X-POFile-SpellExtra: Hathor Pso Kiahk Hat Meo Psa Sene Psabbaton Miy
X-POFile-SpellExtra: Pashons Mes LarguraxAltura Kia Qedame Ham Gen Hedar
X-POFile-SpellExtra: Parmoute Teq Mesore Nehase Kouji Yakatit Maksegno
X-POFile-SpellExtra: Paone Sen Meshir Pagumen Thoout Hamle Epe Mak
X-POFile-SpellExtra: Tkyriakē Ehud Tho Qed Pti Psoou Segno Tah Rob
X-POFile-SpellExtra: Miyazya Meskerem Tobe Peftoou Epep Tky pastabase Fã
X-POFile-SpellExtra: KVTML USD Colaborativos Hunspell Jovie AM PM mails
X-POFile-SpellExtra: mbuttonGroup Blog blog np cp nc UTC Mandaico Batak
X-POFile-SpellExtra: DQTDECLARATIVEDEBUG QML slot pedro mantê Pocinhas
X-POFile-SpellExtra: Reconstrói ii Del iii querê
X-POFile-IgnoreConsistency: Update
 %1, %2. O %1 não é uma função e não pode ser invocado. O %1 não é um tipo Object O '%1' não é um QLayout válido. O '%1' não é um QWidget válido. A acção recebe 2 argumentos. O grupo de acções recebe 2 argumentos. Alerta Tratamento de eventos inválido: Objecto %1 Identificador %2 Método %3 Tipo: %4. Tratamento do 'slot' inválido: Objecto: %1 Identificador %2 Método %3 Assinatura: %4. A invocação de '%1' falhou. A invocação do método '%1' falhou, por não ser possível obter o argumento %2: %3 Confirmação Não foi possível construir o valor Não foi possível criar o ficheiro temporário. Não foi possível aceder ao ficheiro '%1' Não foi possível aceder ao ficheiro '%1': %2 Não foi possível ler o ficheiro '%1' Ocorreu um erro ao processar a inclusão '%1' na linha %2: %3 Ocorreu uma excepção ao invocar a função '%1' em %2:%3:%4 Ocorreu uma excepção ao invocar o 'slot' '%1' em %2:%3:%4 Não foi possível criar uma acção. Não foi possível criar um grupo de acções. Não foi possível criar uma disposição. Não foi possível criar um item gráfico. Não foi possível carregar o ficheiro '%1' Não foi possível converter para %1 o valor do tipo %2 (%3) O ficheiro %1 não foi encontrado. O primeiro argumento deverá ser um QObject. O número de argumentos está incorrecto. É necessário indicar um nome de ficheiro. Tem de indicar um nome de disposição. Deverá indicar um item-pai válido. Deverá indicar um nome de item gráfico. Não indicou um nome da classe Não foi indicado um nome de classe. Não existe o método '%1'. Os argumentos são insuficientes. O 'slot' pediu %1 argumento O 'slot' pediu %1 argumentos Ocorreu um erro ao ler o ficheiro '%1' O tipo do objecto é inválido. mas só existe %1 disponível mas só existem %1 disponíveis a inclusão só recebe 1 argumento, não %1. a biblioteca só recebe 1 argumento, não %1. 