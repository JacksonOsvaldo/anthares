��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     		     	      7	  '   X	     �	     �	     �	  
   �	  	   �	     �	     �	  e   �	  "   T
  c   w
     �
     �
     �
               #     6     G     _     g     t     z     �  /   �  C   �                    	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-03 11:37+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Sitter Harald PulseAudio gconf
Plural-Forms: nplurals=2; plural=n != 1;
 100% Autoria Copyright 2015 Harald Sitter Harald Sitter Sem Aplicações a Tocar Áudio Sem Aplicações a Gravar Áudio Sem Perfis de Dispositivos Disponíveis Sem Dispositivos de Entrada Sem Dispositivos de Saída Perfil: PulseAudio Avançado Aplicações Dispositivos Adicionar um dispositivo de saída virtual para a saída simultânea em todas as placas de som locais Configuração Avançada da Saída Mudar automaticamente todas as transmissões em execução quando ficar disponível uma saída nova Captura Predefinição Perfis do Dispositivo zepires@gmail.com Entradas Silenciar o áudio José Nuno Pires Sons das Notificações Saídas Reprodução Porta  (indisponível)  (desligada) Precisa do módulo do PulseAudio 'module-gconf' Este módulo permite configurar o sub-sistema de áudio PulseAudio. %1: %2 100% %1% 