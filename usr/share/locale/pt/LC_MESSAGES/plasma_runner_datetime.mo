��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  /   �     �  /   �     !     &     +                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-30 11:29+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 A hora actual é %1 Mostra a data actual Mostra a data actual para um dado fuso-horário Mostra a hora actual Mostra a hora actual para um dado fuso-horário data hora A data de hoje é %1 