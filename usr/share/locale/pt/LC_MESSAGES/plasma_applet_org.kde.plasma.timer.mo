��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {          3  
   Q     \  	   e  
   o     z     �     �     �  6   �     �                !     5     G     N     Z  !   q     �  e   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-22 10:41+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 O %1 está em execução O %1 não está em execução &Reiniciar &Iniciar Avançado Aparência Comando: Visualização Executar um comando Notificações Tempo restante: %1 segundo Tempo restante: %1 segundos Executar um comando &Parar Mostrar uma notificação Mostrar os segundos Mostrar o título Texto: Cronómetro O cronómetro terminou O cronómetro está em execução Título: Use a roda do rato para mudar os dígitos ou escolha os cronómetros predefinidos no menu de contexto 