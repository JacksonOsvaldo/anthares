��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  )   �               &     .     ;     K  @   d         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-09 10:21+0100
Last-Translator: Pedro Morais <morais@kde.org>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Spell-Extra: KDE KNotify kde org
X-POFile-SpellExtra: KNotify Samuels Goffart Pfeiffer Carsten Olivier
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002-2006 A Equipa do KDE <h1>Notificações do Sistema</h1>O Plasma permite ao utilizador controlar o modo como é notificado quando certos eventos ocorrem. Há várias opções para o modo como o utilizador será notificado:<ul><li>Do modo como a aplicação foi desenhada.</li><li>Com a campainha ou outro som.</li><li>Através de uma caixa de diálogo com informação adicional.</li><li>Escrevendo o evento num ficheiro de registo sem qualquer alerta visual ou auditivo.</li></ul> Carsten Pfeiffer Charles Samuels Desactivar os sons de todos esses eventos morais@kde.org Origem do evento: KNotify Pedro Morais Olivier Goffart Implementação original Notificações do Sistema - Um módulo para o painel de controlo 