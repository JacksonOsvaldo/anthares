��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  T   �  �     $   �  k   �  &   &     M  K   d     �  I   �            	   -  "   7  B   Z  @   �  @   �           @  n   H     �     �  �   �      �  L   �     �               0     @  
   ]     h     �  	   �  -   �     �     �     �                .     7     >     O     a     r  "   �     �     �     �     �  "   �  	   	       !        <  �   J     �  M      �   N     �  I   �  B   G  B   �  ,   �  #   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-03-01 11:04+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-doc@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: Phonon Kretz Matthias Subwoofer Colin Guthrie
 Para aplicar a mudança de infra-estrutura, terá de encerrar e reiniciar a sessão. Uma lista com as Infra-Estruturas do Phonon existentes no seu sistema. A ordem aqui determina a ordem pela qual o Phonon as irá usar. Aplicar a Lista de Dispositivos a... Aplica a lista de preferências apresentada de momento para as outras categorias de reprodução de áudio: Configuração do 'Hardware' de Áudio Reprodução de Áudio Preferência do Dispositivo de Reprodução de Áudio para a Categoria '%1' Gravação do Áudio Preferência do Dispositivo de Gravação de Áudio para a Categoria '%1' Infra-estrutura Colin Guthrie Ligação 'Copyright' 2006 de Matthias Kretz Preferência por Omissão do Dispositivo de Reprodução de Áudio Preferência por Omissão do Dispositivo de Gravação de Áudio Preferência por Omissão do Dispositivo de Gravação de Vídeo Categoria Predefinida/Indefinida Ignorar Define a ordenação predefinida dos dispositivos que poderão ser substituídos pelas categorias individuais. Configuração do Dispositivo Preferências do Dispositivo Foram encontrados os dispositivos no seu sistema, adequados à categoria seleccionada. Escolha o dispositivo que deseja usar pelas aplicações. zepires@gmail.com,morais@kde.org Não foi possível configurar o dispositivo de saída de áudio seleccionado Frontal Central Frontal Esquerdo Frontal à Esquerda do Centro Frontal Direito Frontal à Direita do Centro 'Hardware' Dispositivos Independentes Níveis de Entrada Inválido Configuração do 'Hardware' de Áudio do KDE Matthias Kretz Mono José Nuno Pires,Pedro Morais Ficheiros de Configuração Reprodução (%1) Preferir Perfil Traseiro Central Traseiro Esquerdo Traseiro Direito Gravação (%1) Mostrar os dispositivos avançados Lateral Esquerdo Lateral Direito Placa de Som Dispositivo de Som Colocação e Teste do Altifalante Subwoofer Testar Testar o dispositivo seleccionado A testar o %1 A ordem define a preferência nos dispositivos. Se, por alguma razão, não puder usar o primeiro dispositivo, o Phonon irá tentar o segundo, e assim por diante. Canal Desconhecido Usar a lista de dispositivos apresentada de momento para ver mais categorias. As diversas categorias de casos de uso multimédia. Para cada categoria, poderá escolher qual o dispositivo que prefere usar nas aplicações do Phonon. Gravação do Vídeo Preferência do Dispositivo de Gravação de Vídeo para a Categoria '%1' A sua infra-estrutura poderá não suportar a gravação de áudio A sua infra-estrutura poderá não suportar a gravação de vídeo sem preferência do dispositivo seleccionado preferir o dispositivo seleccionado 