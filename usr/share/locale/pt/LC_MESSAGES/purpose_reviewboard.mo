��          �      \      �     �     �  	   �  $   �          ,     C     ^     j  	   y  
   �     �     �     �     �     �  "   �  	   �  '   �  �  &     �     �     �  *   �  -   (  *   V  )   �     �     �     �     �     �     �  	   �            '   0     X      d                      
            	                                                                 / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Repository: Request Error: %1 Server: Update Review: Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: kdevreviewboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-09-27 12:23+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: JSON
Plural-Forms: nplurals=2; plural=n != 1;
 / Autenticação Pasta de Base: Não foi possível criar o novo pedido:
%1 Não foi possível obter a lista de revisões Não foi possível modificar os meta-dados Não foi possível enviar a modificação Destino Erro de JSON: %1 Senha: Repositório Repositório: Erro do Pedido: %1 Servidor: Actualizar a Revisão: Actualizar a revisão Nome do utilizador no serviço indicado Utilizador: De onde foi obtido este projecto 