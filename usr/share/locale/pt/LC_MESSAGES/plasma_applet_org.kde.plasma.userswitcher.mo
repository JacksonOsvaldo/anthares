��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     2     D     J     W     h     u     �     �  (   �     �     �     �  1     
   G     R  $   Y      ~                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-09 11:41+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: TTY avatar
Plural-Forms: nplurals=2; plural=n != 1;
 Utilizador actual Geral Disposição Bloquear o Ecrã Nova Sessão Não utilizado Sair... Mostrar o avatar e o nome Mostrar o nome completo (se disponível) Mostrar o nome da conta Mostrar apenas o avatar Mostrar apenas o nome Mostrar a informação técnica sobre as sessões em %1 (%2) TTY %1 Apresentação do nome do utilizador Está autenticado como <b>%1</b> 