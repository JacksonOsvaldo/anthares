��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �             	   '     1     5     <     K     _     h     z  .   �     �     �     �     �     �     �          	               2     F     W     c     �     �     �     �     �     �     �     
	      	     ,	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-02-24 10:15+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: virt MiB
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-IgnoreConsistency: Bar
 s Aplicação: Relógio (médio): %1 MHz Barras Tampões: CPU CPU %1 Monitor do CPU CPU%1: %2% @ %3 MHz CPU: %1% CPU's em separado 'Cache' 'Cache' Modificada, Por Gravar: %1 MiB, %2 MiB Monitor da 'cache' Em 'cache': Circular Cores Barra Compacta Memória modificada: Geral Espera E/S: Memória Monitor da memória Memória: %1/%2 MiB Tipo de monitor: Prioridade: Atribuir as cores manualmente Mostrar: Memória virtual Monitor de memória virtual Memória Virtual: %1/%2 MiB Sistema: Carga do sistema Intervalo de actualização: Memória virt. usada: Utilizador: Memória para gravar: 