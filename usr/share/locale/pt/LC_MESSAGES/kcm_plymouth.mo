��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  (   u  2   �  &   �     �       *   &  *   Q  3   |     �     �     �  9   �     	  2   9	  @   l	     �	  K   �	      
  y   1
  7   �
     �
     �
  4     =   8                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-06 12:08+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: Plymouth plymouth alternatives update initramfs
Plural-Forms: nplurals=2; plural=n != 1;
 Não é possível iniciar o 'initramfs'. Não é possível iniciar o 'update-alternatives'. Configurar o Ecrã Inicial do Plymouth Obter Novos Ecrãs Iniciais zepires@gmail.com Obter Novos Ecrãs Iniciais de Arranque... Não foi possível executar o 'initramfs'. O 'initramfs' terminou com a condição de erro %1. Instala um tema. Marco Martin José Nuno Pires Não foi indicado nenhum tema nos parâmetros auxiliares. Instalador de temas do Plymouth Seleccionar um ecrã inicial global para o sistema O tema a instalar - deverá ser um ficheiro de pacote existente. O tema %1 não existe. Tema danificado: o ficheiro '.plymouth' não foi encontrado dentro do tema. A pasta de temas %1 não existe. Este módulo permite-lhe configurar a aparência de toda a área de trabalho, com algumas predefinições prontas a usar. Não é possível autenticar/executar a acção: %1, %2 Desinstalar Desinstala um tema. Não foi possível executar o 'update-alternatives'. O 'update-alternatives' terminou com a condição de erro %1. 