��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  7   �  N         c     �     �     �     �     �     �     �     �     �       %   ,     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 15:20+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 <a href='%1'>%1</a> Fechar Copiar Automaticamente: Não mostrar mais esta janela e copiar automaticamente. Largue algum texto ou uma imagem aqui para a enviar para um serviço 'online'. Ocorreu um erro durante o envio. Geral Tamanho do Histórico: Colar Espere por favor Tente de novo, por favor. A enviar... Partilha Partilhas de '%1' Enviado com sucesso O URL acabou de ser partilhado Enviar o %1 para um serviço 'online' 