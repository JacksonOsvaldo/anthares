��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     c  E   }  
   �     �     �     �                    2     F     Y     m     �      �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-20 10:20+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Adicionar um Lançador... Adicionar lançamentos por arrastamento ou usando o menu de contexto. Aparência Disposição Editar o Lançador... Activar a mensagem Introduza o título Geral Esconder os ícones Máximo de colunas: Máximo de linhas: Lançamento Rápido Remover o Lançador Mostrar os ícones escondidos Mostrar os nomes dos lançadores Mostrar o título Título 