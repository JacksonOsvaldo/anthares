��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  3   �  <     @   N  -   �  0   �  .   �  "        @  (   R         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-02 09:52+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Não foi possível detectar o tipo MIME do ficheiro Não foi possível encontrar todas as funções necessárias Não foi possível encontrar o fornecedor com o destino indicado Ocorreu um erro ao tentar executar o programa Localização inválida para o fornecedor pedido Não foi possível ler o ficheiro seleccionado O serviço não estava disponível Erro Desconhecido Tem de indicar um URL para este serviço 