��    �        �   �	      H     I  -   a     �     �  �   �  �   f  �     [   �  )     ,   0  ,   ]     �     �  0   �     �     	  \   '  A   �  v   �  R   =     �     �     �     �     �     �     �  	   �  
          5     #   T     x     �  %   �  '   �     �  8   �  )   7     a     z     �     �     �     �     �  "   �  2   �  3   *     ^     w     }     �  +   �  $   �  $   
      /  2   P  +   �     �     �     �  T   �     .     2     D  �   W     '     7     D     L  	   Z  
   d     o     }     �     �     �     �     �  )   �  '        +     H     U     b     p  	        �     �     �  1   �     �  
          B        W  	   m     w  	   �  =   �     �     �            ?     =   O     �  	   �     �     �     �     �     �  )   �  -     $   G     l     r     z     �     �  
   �     �     �  3   �  2   !  3   T     �  �   �  &   4  �   [       f   !  #   �     �  ?   �  8   �  	   ,   2   6      i      p      y      �      �      �      �      �      �      �   	   �      !     !  ;   #!  !   _!  w   �!  5   �!    /"  #   1;  0   U;     �;     �;  �   �;  �   }<  �   H=  b   �=  $   b>  !   �>     �>     �>     �>  1   �>     
?     ?     0?     C?  @   O?     �?     �?     �?     �?  "   �?     @     @     @  
   *@     5@     N@  C   e@  /   �@     �@     �@  6   A  C   9A      }A  P   �A  >   �A     .B     LB     YB     bB     qB     �B     �B  3   �B  [   �B  F   BC      �C     �C  ,   �C  *   �C  B   D  5   JD  4   �D  '   �D  C   �D  7   !E  "   YE     |E     �E  _   �E     �E     �E     F    #F     5G     NG     \G     eG  	   vG  
   �G     �G     �G     �G  	   �G     �G     �G  !   �G  ?   H  3   [H  '   �H     �H     �H     �H     �H     I     
I     *I     EI  6   KI     �I  
   �I     �I  I   �I     �I     J  '   J     6J  G   =J  /   �J     �J     �J     �J     �J     �J     
K     %K     1K     AK     VK     ]K     sK     �K     �K  
   �K  	   �K  	   �K     �K     �K     �K     L      %L  "   FL     iL     vL     �L     �L  �   �L  +   iM  �   �M     1N  y   JN  9   �N     �N  _   O     gO     �O  A   �O  
   �O     �O     �O     P     P     5P     RP     nP     �P     �P     �P     �P     �P  C   �P     Q  )   'Q     QQ        F              	   e   �   :       f          "      �      p   �       R   �   r       �           x       +   a   D   i   G      .   �      �      /   J      )   ,       �   �   *   O       6      s   �           c   �   H   _               �          �   �   n       j   Y                g   b   3   l          �   <   m   u      L          0   -   o       9   �   �       �   (   C           �   �           ;   �   q      z   �       8      &   @   A   v   w   ]       ^      #   '       E   �       4           [   �       2   N       ?       S   �      =   $      ~               k   U   t          W                  `   X   B   
   �       K   M       y   �   h          �   I          �   1   T   }      Z          %   5   V   7   |           d   Q   {       �          !   P           \   >   �    1 download %1 downloads <a href="http://opendesktop.org">Homepage</a> <br />Provider: %1 <br />Version: %1 <qt>Cannot start <i>gpg</i> and check the validity of the file. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and retrieve the available keys. Make sure that <i>gpg</i> is installed, otherwise verification of downloaded resources will not be possible.</qt> <qt>Cannot start <i>gpg</i> and sign the file. Make sure that <i>gpg</i> is installed, otherwise signing of the resources will not be possible.</qt> <qt>Enter passphrase for key <b>0x%1</b>, belonging to<br /><i>%2&lt;%3&gt;</i><br />:</qt> @action:inmenu$GenericName - all devices @action:inmenu$GenericName - current device @action:inmenu$GenericName - current folder @action:inmenuConfigure... @action:inmenuMore @action:inmenuNo further information available. @action:inmenuNot installed: @action:inmenuVisit homepage A link to make a donation for a Get Hot New Stuff item (opens a web browser)Make a donation A link to the description of this Get Hot New Stuff itemHomepage A link to the knowledgebase (like a forum) (opens a web browser)Knowledgebase (no entries) Knowledgebase (%1 entries) A link to the website where the get hot new stuff upload can be seenVisit website All Categories All Providers All categories are missing Authentication error. Author: BSD Become a Fan Category: Changelog: Checking login... Configuration file exists, but cannot be opened: "%1" Configuration file is invalid: "%1" Configure menu Configure menu - %1 Could not fetch provider information. Could not install "%1": file not found. Could not install %1 Could not load get hot new stuff providers from file: %1 Could not verify login, please try again. Create content on server Description: Details Details for %1 Details view mode Download File Download New Stuff... Download of "%1" failed, error: %2 Download of item failed: no download URL for "%1". Downloaded file was a HTML file. Opened in browser. Enter search phrase here Error Error initializing provider. Fetch content link from server Fetching content data from server finished. Fetching content data from server... Fetching license data from server... Fetching provider information... Fetching your previously updated content finished. Fetching your previously updated content... File not found: %1 File to upload: Finish For this menu there are some more tools suggested which are currently not installed: GPL Get Hot New Stuff Get Hot New Stuff! I ensure that this content does not violate any existing copyright, law or trademark. I agree for my IP address to be logged. (Distributing content without the permission of the copyright holder is illegal.) Icons view mode Initializing Install Install Again Installed Installing Invalid item. Key used for signing: LGPL License: Loading Preview Loading data Loading data from provider Loading of providers from file: %1 failed Loading one preview Loading %1 previews Loading provider information Main section More section More tools... Most downloads Move down Move to Main section Move to More section Move up Name of the file as it will appear on the website Network error %1: %2 New Upload Newest Note: You can edit, update and delete your content on the website. Opposite to BackNext Order by: Overwrite existing file? Password: Please fill out the information about your upload in English. Possibly bad download link Preview Images Price Price: Program name followed by 'Add On Installer'%1 Add-On Installer Program name followed by 'Add On Uploader'%1 Add-On Uploader Provider information Provider: Rating Rating: %1% Re: %1 Reason for price: Register a new account Request installation of this itemInstall Request uninstallation of this itemUninstall Request updating of this itemUpdate Reset Search: Select Preview... Select Signing Key Select preview image Server: %1 Set a price for this item Share Hot New Stuff Show the author of this item in a listBy <i>%1</i> Show the size of the file in a list<br />Size: %1 Show the size of the file in a list<p>Size: %1</p> Start Upload The downloaded file is a html file. This indicates a link to a website instead of the actual download. Would you like to open the site with a browser instead? The selected category "%1" is invalid. The server does not recognize the category %2 to which you are trying to upload. The server does not recognize any of the categories to which you are trying to upload: %2 There was a network error. This should clearly describe the file content. It can be the same text as the title of the kvtml file. Timeout. Check Internet connection. Title: Too many requests to server. Please try again in a few minutes. Tooltip for a link in a dialogOpens in a browser window Uninstall Unknown Open Collaboration Service API error. (%1) Update Updating Upload Failed Upload content Upload failed: %1 Upload first preview Upload second preview Upload third preview Uploading Failed Use Username: Version: You are now a fan. Your account balance is too low:
Your balance: %1
Price: %2 fan as in supporter1 fan %1 fans the price of a download item, parameter 1 is the currency, 2 is the priceThis item costs %1 %2.
Do you want to buy it? voting for an item (good/bad)Your vote was recorded. Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:00+0100
PO-Revision-Date: 2017-06-24 09:48+0100
Last-Translator: José Nuno Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-IgnoreConsistency: &Discard
X-POFile-IgnoreConsistency: Enter
X-POFile-IgnoreConsistency: Author
X-POFile-IgnoreConsistency: Open &File
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-Allow: 2
X-POFile-IgnoreConsistency: Far
X-POFile-SpellExtra: Hspell Koenig KHTML Thaana Telugu KScript Sycoca
X-POFile-SpellExtra: artsmessage KDEInit SOCKS kcmkresources
X-POFile-SpellExtra: ChavePrivadaFalhou KIOTest Han Thaani Jumaada Hangul
X-POFile-SpellExtra: Cherokee Meh KSpell chaveSessao TestWritevCard
X-POFile-SpellExtra: Thulatha Javascript Tagbanwa End Tagalog LTR Oriya
X-POFile-IgnoreConsistency: H:
X-POFile-IgnoreConsistency: Untrusted
X-POFile-IgnoreConsistency: Export
X-POFile-SpellExtra: Hiragana Backspace Print PgUp Ins
X-POFile-SpellExtra: ModificadoresEspaço Yi Lao Return gpg Caps Lock kab
X-POFile-SpellExtra: aRts tags Buhid Insert Gurmukhi Malayalam Scroll
X-POFile-SpellExtra: Delete Ogham PgDn Kannada Tab Home Katakana SysReq
X-POFile-SpellExtra: KConvertTest Khmer OutraOpção Bopomofo
X-POFile-SpellExtra: MarcasCombinatórias Enter UmaOpção Devanagari
X-POFile-SpellExtra: Hanunoo Sinhala JanelaAutoExemplo Lars Ian help
X-POFile-IgnoreConsistency: Try Different
X-POFile-IgnoreConsistency: Delete
X-POFile-IgnoreConsistency: Comment
X-POFile-IgnoreConsistency: &Restore
X-POFile-IgnoreConsistency: Reset
X-POFile-IgnoreConsistency: 0.1
X-POFile-IgnoreConsistency: Forward
X-POFile-SpellExtra: Kanbun CTRL Klash Syloti JS Jan TETest QObject
X-POFile-SpellExtra: Sebastian Geiser Far kdeinit Weis Mordad Yau Hausmann
X-POFile-SpellExtra: execprefix autostart Dirk Nov Elul shanbe Farvardin
X-POFile-SpellExtra: KApplication bin tagcloudtest displayname IFrame yo
X-POFile-SpellExtra: Aza Adar Sáb dah XIM Sha Sonnet testregression Jamo
X-POFile-SpellExtra: Shawwal Bah KConf IPA Hijri Sab Testkhtml Jeroen
X-POFile-SpellExtra: QWidget dumps KJSEmbed Arb qttest stderr Kho ban Kha
X-POFile-SpellExtra: PathLengthExceeded Tai shn Ago KrossTest Ithnain
X-POFile-SpellExtra: klauncher tempfile Aban frame ThreadWeaver Kun yeyo
X-POFile-SpellExtra: Buginese Lue Kislev Khamees home Jumma XDG Khordad
X-POFile-SpellExtra: Zemberek KAboutData Wijnhout Sivan Saami Method Qua
X-POFile-SpellExtra: Molkentin PTY Koivisto onthespot Ord Shvat Jom
X-POFile-SpellExtra: KMultiPart Ahad CJK Aspell Tifinagh NoCARoot Tishrey
X-POFile-SpellExtra: Up KDXSView ModRunner subtexto aifamily Panj path
X-POFile-SpellExtra: NumLock keramik GHNS TestRegressionGui Yek Iyar Ahd
X-POFile-SpellExtra: khtmltests Torben QApplication overthespot caption
X-POFile-SpellExtra: khtml desktop Ispell QWS create Faure Object Limbu
X-POFile-SpellExtra: KLauncher Sauer Hijjah Myanmar NEC BCC Fev Kelly
X-POFile-SpellExtra: Jumee Stephan TestRegression Knoll frames HOME Jum
X-POFile-SpellExtra: DISPLAY KNewStuff Awal Rajab pt plastik InvalidHost
X-POFile-SpellExtra: kdemain STDOUT Jun Jul Kulow Yaum pa Chahar widgets
X-POFile-SpellExtra: man KUnitTest pm KDEPIM TAB Waldo CL CC Balinês
X-POFile-SpellExtra: Nagri Kangxi QLayout qtplugininstance regression
X-POFile-SpellExtra: multipart Jalali Phags Set servname nograb
X-POFile-SpellExtra: International Frame CGIs Stylesheet Library Sex Seg
X-POFile-SpellExtra: KDontChangeTheHostName SO toner Yijing Peter Out
X-POFile-SpellExtra: InvalidCA Le Khmeres Tevet Ordibehesht Anton am al
X-POFile-SpellExtra: Tir Tuebingen Esf Abr ini KLocale KiB WMNET Dingbats
X-POFile-SpellExtra: InvalidPurpose kdehelp id Glagolitic factory Esfand
X-POFile-SpellExtra: Nisan kjs ErrorReadingRoot MiB Copta Shanbe Xvfb
X-POFile-SpellExtra: client Mai Bastian document config TiB Jones AC
X-POFile-SpellExtra: KBuildSycoca Bahman offthespot Mueller Tang ye Thu
X-POFile-SpellExtra: Sabt NKo aisocktype mixed Carriage Thl aiflags
X-POFile-SpellExtra: Muharram Reinhart Kontact Cantonês Page icon
X-POFile-SpellExtra: makekdewidgets ManyColor Heshvan Kross Ith bind Antti
X-POFile-SpellExtra: DXS Tamuz Shahrivar sessionId sh KJSCmd Av KLibLoader
X-POFile-SpellExtra: Mehr GiB Arbi dograb AssinaturaFalhou prefix
X-POFile-SpellExtra: Hexagramas ize AutoAssinado NãoConfiável Qi Down
X-POFile-SpellExtra: directory Índicas ise Oxygen info shared share usr
X-POFile-IgnoreConsistency: Separate Folders
X-POFile-SpellExtra: XDGDATADIRS KTTS Control PrtScr Hyper Sys Win Screen
X-POFile-SpellExtra: Req Break AltGr ReadOnly SHM EOF Re abc ABC QPL Kate
X-POFile-SpellExtra: Serif Sans KFormula URIs raster opengl favicons Solid
X-POFile-SpellExtra: Harald Fernengel KTTSD baseline Resource writeall
X-POFile-SpellExtra: Trüg YiB PiB YB ZB EB PB EiB ZiB GB TB KIdleTest
X-POFile-SpellExtra: Freddi KIdleTime Cha Āshwin Budhavãra Suk Paush
X-POFile-SpellExtra: Shrāvana Somavãra Phālgun Raviãra Phā Māg
X-POFile-SpellExtra: Bhādrapad Chaitra Māgh Sukravãra Āshādha
X-POFile-SpellExtra: Agrahayana Bud Shr Guruvãra Mañ Gur Vaishākh Jya
X-POFile-SpellExtra: Kārtik Agr Jyaishtha Kār Āsh Bhā Rav milisegundo
X-POFile-SpellExtra: Āsw Mañgalvã Sanivãra Mayek Ol Saurashtra
X-POFile-SpellExtra: Sundanês Viet Lisu Kayah Chiki Lepcha Meetei Cham
X-POFile-SpellExtra: Rejang Tham Bamum Pshoment Pas Ptiou Neh Genbot Hamus
X-POFile-SpellExtra: Pag Paope Pesnau Pes Hed Magabit Ehu Tahsas Yak Mag
X-POFile-SpellExtra: Tob Pef Kou Pam Pao Tequemt Paremhotep Psh nabot
X-POFile-SpellExtra: Hathor Pso Kiahk Hat Meo Psa Sene Psabbaton Miy
X-POFile-SpellExtra: Pashons Mes LarguraxAltura Kia Qedame Ham Gen Hedar
X-POFile-SpellExtra: Parmoute Teq Mesore Nehase Kouji Yakatit Maksegno
X-POFile-SpellExtra: Paone Sen Meshir Pagumen Thoout Hamle Epe Mak
X-POFile-SpellExtra: Tkyriakē Ehud Tho Qed Pti Psoou Segno Tah Rob
X-POFile-SpellExtra: Miyazya Meskerem Tobe Peftoou Epep Tky pastabase Fã
X-POFile-SpellExtra: KVTML USD Colaborativos Hunspell Jovie AM PM mails
X-POFile-SpellExtra: mbuttonGroup Blog blog np cp nc UTC Mandaico Batak
X-POFile-SpellExtra: DQTDECLARATIVEDEBUG QML slot pedro mantê Pocinhas
X-POFile-SpellExtra: Reconstrói ii Del iii querê GenericName
X-POFile-IgnoreConsistency: Update
 1 transferência %1 transferências <a href="http://opendesktop.org">Página Web</a> <br />Fornecedor: %1 <br />Versão: %1 <qt>Não foi possível iniciar o <i>gpg</i> e verificar a validade do ficheiro. Certifique-se que o <i>gpg</i> está instalado, caso contrário a verificação dos recursos obtidos não será possível.</qt> <qt>Não é possível iniciar o <i>gpg</i> e obter as chaves disponíveis. Certifique-se que o <i>gpg</i> está instalado, caso contrário a verificação dos recursos obtidos não será possível.</qt> <qt>Não foi possível iniciar o <i>gpg</i> e assinar o ficheiro. Certifique-se que o <i>gpg</i> está instalado, caso contrário a assinatura dos recursos não será possível.</qt> <qt>Indique a frase-senha da chave <b>0x%1</b>, que pertence a <br/><i>%2&lt;%3&gt;</i><br/>:</qt> $GenericName - todos os dispositivos $GenericName - dispositivo actual $GenericName - pasta actual Configurar... Mais Não está disponível mais nenhuma informação. Não instalado: Visitar a página Web Fazer uma doação Página Web Base de conhecimento (sem itens) Base de conhecimento (%1 itens) Visitar a página Web Todas as Categorias Todos os Fornecedores Faltam todas as categorias Ocorreu um erro na autenticação. Autor: BSD Tornar-se um Fã Categoria: Registo de alterações: A verificar a conta... O ficheiro de configuração existe, mas não pode ser aberto: "%1" O ficheiro de configuração é inválido: "%1" Configurar o menu Configurar o menu - %1 Não foi possível obter a informação do fornecedor. Não foi possível instalar o "%1": o ficheiro não foi encontrado. Não foi possível instalar o %1 Não foi possível obter os fornecedores de itens novos a partir do ficheiro: %1 Não foi possível verificar a conta, por favor tente de novo. Criar o conteúdo no servidor Descrição: Detalhes Detalhes de %1 Modo de vista em detalhes Obter o Ficheiro Obter Coisas Novas... A transferência do "%1" foi mal-sucedida, erro: %2 A transferência do item foi mal-sucedida: não existe o URL de transferência para o "%1". O ficheiro transferido foi um documento HTML. Foi aberto no navegador. Indique aqui a frase de pesquisa Erro Ocorreu um erro ao inicializar o fornecedor. Obter a ligação ao conteúdo do servidor A obtenção dos dados do conteúdo a partir do servidor terminou. A obter os dados do conteúdo a partir do servidor... A obter os dados da licença a partir do servidor... A obter a informação do fornecedor... A obtenção do seu conteúdo, actualizado anteriormente, terminou. A obter o conteúdo actualizado anteriormente por si... O ficheiro não foi encontrado: %1 Ficheiro a enviar: Terminar Para este menu, existem mais algumas ferramentas sugeridas que não são instaladas de momento: GPL Obter Coisas Novas Obter Coisas Novas! Garanto-lhe que este conteúdo não viola nenhum direito de cópia, lei ou marca registada existente. Concordo em que o meu endereço IP fique registado. (A distribuição de conteúdo para o qual não tem permissões por parte do detentor de direitos de cópia é ilegal.) Modo de vista em ícones A inicializar Instalar Instalar de Novo Instalado A Instalar O item é inválido. Chave utilizada para assinar: LGPL Licença: A Carregar a Antevisão A carregar os dados A carregar os dados do fornecedor O carregamento de fornecedores do ficheiro: %1 foi mal-sucedido A carregar uma antevisão A carregar %1 antevisões A carregar a informação do fornecedor Secção principal Secção 'Mais' Mais ferramentas... Os mais transferidos Descer Mover para a secção Principal Mover para a secção Mais Subir O nome do ficheiro, tal como aparecerá na página Web Erro de rede %1: %2 Novo Envio Mais Recente Nota: Poderá editar, actualizar e apagar o seu conteúdo na página Web. Seguinte Ordenar por: Deseja substituir o ficheiro existente? Senha: Preencha por favor alguma informação acerca do seu envio, em Inglês. Possível endereço de transferência inválido Imagens de Antevisão Preço Preço: %1 - Instalador Adicional Envio de Extras do %1 Informação do fornecedor Fornecedor: Classificação Classificação: %1% Re: %1 Razão para o preço: Registar uma nova conta Instalar Desinstalar Actualizar Reiniciar Procurar: Seleccionar a Antevisão... Escolha a Chave de Assinatura Escolha a imagem de antevisão Servidor: %1 Definir um preço para este item Partilhar as Funcionalidades Novas De <i>%1</i> <br />Tamanho: %1 <p>Tamanho: %1</p> Iniciar o Envio O ficheiro transferido é um ficheiro HTML. Isto corresponde a uma ligação a uma página Web, em vez de ser a transferência em si. Deseja abrir a página num navegador, como alternativa? A categoria seleccionada "%1" é inválida. O servidor não conhece a categoria %2 para onde está a tentar enviar. O servidor não conhece nenhuma das categorias para onde está a tentar enviar: %2. Ocorreu um erro de rede. Isto deverá descrever de forma clara o conteúdo do ficheiro. Poderá ser o mesmo texto que o título do ficheiro KVTML. Acabou o tempo-limite. Verifique a ligação à Internet. Título: Foram emitidos demasiados pedidos ao servidor. Por favor, tente de novo daqui a alguns minutos. Abrir numa janela de navegador Desinstalar Erro desconhecido da API de Serviços Colaborativos abertos. (%1) Actualizar A Actualizar O Envio Foi Mal-Sucedido Enviar o conteúdo O envio foi mal-sucedido: %1 Enviar a primeira antevisão Enviar a segunda antevisão Enviar a terceira antevisão O Envio Foi Mal-Sucedido Usar Utilizador: Versão: Você é agora um fã. O saldo da sua conta é demasiado baixo:
O seu saldo: %1
Preço: %2 1 fã %1 fãs Este item custa %2 %1.
Deseja comprá-lo? O seu voto foi registado. 