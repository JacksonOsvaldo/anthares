��          �   %   �      @     A     Z     g          �     �      �     �     �     �            W   2  #   �     �     �     �     �  $        +     J     \     p     �  <  �     �     �     �          4     H     U     g     �     �     �     �  (   �     �     	     	     "	     3	     M	     ^	     o	     �	     �	     �	                                           	                                                           
                        @title:menuMain Toolbar Clear Search Collapse All Categories Copyright 2009-2018 KDE Current Maintainer David Hubner EMAIL OF TRANSLATORSYour emails Expand All Categories Helge Deller Help button labelHelp Info Center Information Modules Information about current module located in about menuAbout Current Information Module Kaction search labelSearch Modules Main window titleKInfocenter Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Module help button labelModule Help NAME OF TRANSLATORSYour names Nicolas Ternisien Previous Maintainer Search Bar Click MessageSearch Waldo Bastian Project-Id-Version: kcm_infobase
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-08 05:56+0100
PO-Revision-Date: 2017-10-22 12:40+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: sbin LSB mount ALeastSignificant Alib MiB
X-POFile-SpellExtra: AMostSignificant dev FPU PA sndstat camcontrol
X-POFile-SpellExtra: ALSBFirst AMSBFirst MSB backing LSBFirst BPP MSBFirst
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: Bitmap Padding mm unders save store Elter Helge
X-POFile-SpellExtra: Kinfocenter Ettrich Waldo Matthias Hoelzer Deller
X-POFile-SpellExtra: Kluepfel Ternisien SC Bastian SF KInfocenter Hubner
 Barra Principal Limpar a Procura Fechar Todas as Categorias 'Copyright' 2009-2018 do KDE Manutenção Actual David Hubner zepires@gmail.com Expandir Todas as Categorias Helge Deller Ajuda Centro de Informações Módulos de Informação Acerca do Módulo de Informação Actual Procurar nos Módulos KInfocenter Matthias Elter Matthias Ettrich Matthias Hoelzer-Kluepfel Ajuda do Módulo José Nuno Pires Nicolas Ternisien Manutenção Anterior Procurar Waldo Bastian 