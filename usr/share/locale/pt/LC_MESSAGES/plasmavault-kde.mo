��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  &    �  6  �    �  �     �#     �#     �#  +   �#  .    $  	   /$  C   9$     }$     �$  
   �$     �$  >   �$  -   %     2%     N%     T%     k%     q%     �%     �%     �%     �%  #   �%  A   &     C&  D   _&     �&     �&     �&    �&  &   �'  �   (     �(  %   �(  
   �(     �(     �(     �(     )  1   )     E)  Y   N)  1   �)  N   �)  6   )*  8   `*  I   �*  (   �*  I   +  >   V+  %   �+  (   �+     �+  I   �+     G,     a,  C   q,  [   �,     -     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-10 15:35+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: pre EncFS iCloud space OneDrive CryFS Hornby Dropbox
X-POFile-SpellExtra: wrap Encfs white Defuse Security Cloud Vault
Plural-Forms: nplurals=2; plural=n != 1;
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Aviso de segurança:</b>
                             De acordo com uma auditoria de segurança de Taylor Hornby (Defuse Security),
                             a implementação actual do Encfs é vulnerável ou potencialmente vulnerável
                             a diversos tipos de ataques.
                             Por exemplo, um atacante com acesso de leitura/escrita
                             aos dados encriptados poderá reduzir a complexidade da descodificação
                             para os dados encriptados subsequentemente sem que isto seja detectado por um utilizador legítimo,
                             ou poderá usar a análise temporal para deduzir a informação.
                             <br /><br />
                             Isto significa que não deverá sincronizar
                             os dados encriptados com um serviço de armazenamento na Cloud,
                             ou usá-lo noutras circunstâncias em que o atacante
                             possa aceder frequentemente aos dados encriptados.
                             <br /><br />
                             Veja o <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> para obter mais informações. <b>Aviso de segurança:</b>
                             O CryFS encripta os seus ficheiros, para que os possa guardar em qualquer lado.
                             Ele funciona bem em conjunto com os serviços na Cloud, como o Dropbox, iCloud, OneDrive, entre outros.
                             <br /><br />
                             Ao contrário de outras soluções de sobreposição de sistemas de ficheiros,
                             não expõe a estrutura das pastas,
                             o número dos ficheiros nem os tamanhos
                             dos mesmos no formato de dados encriptado.
                             <br /><br />
                             Algo de importante a notar é que,
                             embora o CryFS seja considerado seguro,
                             não existe nenhuma auditoria de segurança
                             independente que o confirme. Criar um Novo Cofre Actividades Infra-estrutura: Não é possível criar o ponto de montagem Não é possível abrir um cofre desconhecido. Modificar Escolha o sistema de encriptação que deseja usar para este cofre: Escolher a cifra usada: Fechar o cofre Configurar Configurar o Cofre... Não é possível instanciar a infra-estrutura configurada: %1 A infra-estrutura configurada não existe: %1 Versão correcta encontrada Criar Criar um Novo Cofre... CryFS O dispositivo já está aberto O dispositivo não está aberto Janela Não mostrar este aviso de novo EncFS Localização dos dados encriptados Não foi possível criar as pastas; verifique as suas permissões Não foi possível executar Não foi possível obter a lista de aplicações que usam este cofre Não foi possível abrir: %1 Fechar à força  Geral Se limitar este cofre apenas a certas actividades, ele será apresentado na 'applet' apenas quando estiver nessas actividades. Para além disso, quando mudar para uma actividade em que não deva estar disponível, o mesmo cofre será fechado automaticamente. Limitar às actividades seleccionadas: Lembre-se que não existe nenhuma forma de recuperar uma senha esquecida. Se esquecer a sua senha, pode considerar os seus dados como perdidos. Ponto de montagem O ponto de montagem não foi definido Montar em: Seguinte Abrir com o Gestor de Ficheiros Senha: Plasma Vault Por favor, indique a senha para abrir este cofre: Anterior A pasta do ponto de montagem não está vazia; o pedido de abertura do cofre foi recusado A infra-estrutura indicada não está disponível A configuração do cofre só poderá ser modificada enquanto estiver fechada. O cofre é desconhecido - não é possível fechá-lo. O cofre é desconhecido - não é possível destruí-lo. Este dispositivo já está registado. Não é possível criá-lo de novo. Esta pasta já contém dados encriptados Não foi possível fechar o cofre, porque uma aplicação está a usá-lo Não foi possível fechar o cofre, porque está em uso pelo %1 Não foi possível detectar a versão Não foi possível efectuar a operação Dispositivo desconhecido Ocorreu um erro desconhecido - não é possível criar a infra-estrutura. Usar a cifra por omissão Nome do &cofre: Foi instalada a versão errada. A versão necessária é a %1.%2.%3 Tem de seleccionar pastas vazias para o armazenamento encriptado e para o ponto de montagem %1: %2 