��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     )	     D	     \	  =  s	  	  �
  %   �  f   �     H  #   \     �  �   �  �   +     '     F  (   R     {     �                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-02-09 14:29+0000
Last-Translator: José Nuno Pires <zepires@gmail.com>
Language-Team: pt <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: KDM xterm xconsole KWin
Plural-Forms: nplurals=2; plural=n != 1;
 T&erminar a sessão actual &Reiniciar o computador Desligar o compu&tador <h1>Gestor de Sessões</h1> O utilizador pode configurar aqui o gestor de sessões. Esta configuração inclui algumas opções, como por exemplo se o fim da sessão deve ou não ser confirmado, se a sessão deve ser gravada quando termina e se o computador deve, por omissão, ser desligado quando a sessão termina. <ul>
<li><b>Recuperar a sessão anterior:</b> Irá gravar todas as aplicações em execução ao sair e recuperá-las no próximo arranque</li>
<li><b>Recuperar manualmente a sessão gravada: </b> Permite à sessão ser gravada em qualquer altura através da opção "Gravar a Sessão" no menu K. Isto significa que as aplicações iniciadas no momento irão reaparecer no seu próximo arranque.</li>
<li><b>Iniciar com uma sessão vazia:</b> Não grava nada. Irá aparecer um ecrã vazio no próximo arranque.</li>
</ul> Aplicações a e&xcluir das sessões: Assinale esta opção se quiser que o gestor de sessões mostre uma janela de confirmação da saída. &Confirmar a saída Opção de Encerramento Predefinida Geral Aqui pode escolher o que deve acontecer, por omissão, quando termina uma sessão. Esta opção só tem significado se tiver utilizado o KDM para iniciar a sessão. Aqui você poderá indicar uma lista separada por vírgulas ou dois-pontos com as aplicações que não devem ser gravadas nas sessões e que, por isso, não serão iniciadas ao recuperar uma sessão. Por exemplo, 'xterm,xconsole' ou 'xterm:xconsole'. O&ferecer opções de desligar No Arranque Recuperar &manualmente a sessão gravada Recu&perar a sessão anterior Iniciar com uma &sessão vazia 