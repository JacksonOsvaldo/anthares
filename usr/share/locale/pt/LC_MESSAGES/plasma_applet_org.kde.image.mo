��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s     4  *   =     h          �     �     �     �  8   �     �           .     4     H     P     i     �  !   �     �  '   �     �               &     ;     V     _  
   z     �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-18 17:10+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: svgz xcf jpg
Plural-Forms: nplurals=2; plural=n != 1;
 %1 de %2 Adicionar um Papel de Parede Personalizado Adicionar uma Pasta... Adicionar uma Imagem... Fundo: Borrar Centrado Mudar a cada: A pasta com o papel de parede de onde mostrar as imagens Obter Papéis de Parede Obter Novos Papéis de Parede... Horas Ficheiros de Imagem Minutos Papel de Parede Seguinte Abrir a Pasta Respectiva Abrir a Imagem Abrir a Imagem do Papel de Parede Posicionamento: Ficheiro recomendado de papel de parede Remover o papel de parede Repor o papel de parede Escalado Escalado e Recortado Escalado Proporcionalmente Segundos Seleccionar a Cor de Fundo Cor única Mosaico 