��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �       �     �  
   �     �  :        ?     M     `  +   s  +   �  "   �  	   �     �  1   �     0                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-11-07 10:19+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: min
Plural-Forms: nplurals=2; plural=n != 1;
 Blo&quear o ecrã ao reactivar: Activação Aparência Erro Não foi possível testar o bloqueio do ecrã com sucesso. Imediatamente Atalho de teclado: Bloquear a Sessão Bloquear automaticamente o ecrã ao fim de: Bloquear o ecrã ao recuperar da suspensão Pe&dir uma senha após o bloqueio:  min  min  s  s O atalho de teclado global para bloquear o ecrã. &Tipo de Papel de Parede: 