��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     A  6   H  2     A   �  5   �  *   *	  &   U	  4   |	  0   �	  B   �	  5   %
  1   [
  B   �
  o   �
  9   @  &   z  %   �     �     �     �     �       %   #  !   I     k     x     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-08 16:29+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: SVN
Plural-Forms: nplurals=2; plural=n != 1;
 Enviar Os ficheiros foram adicionados ao repositório de SVN. A adicionar os ficheiros ao repositório de SVN... A adição dos ficheiros ao repositório de SVN foi mal-sucedida. O envio das alterações para o SVN foi mal-sucedido. As alterações foram enviadas para o SVN. A enviar as alterações para o SVN... Os ficheiros foram removidos do repositório de SVN. A remover os ficheiros do repositório de SVN... A remoção dos ficheiros do repositório de SVN foi mal-sucedida. Os ficheiros foram revertidos do repositório de SVN. A reverter os ficheiros do repositório de SVN... A reversão dos ficheiros do repositório de SVN foi mal-sucedida. A actualização do estado do SVN foi mal-sucedida. A desactivar a opção "Mostrar as Actualizações do SVN". A actualização do repositório de SVN foi mal-sucedida. O repositório de SVN foi actualizado. A actualizar o repositório de SVN... Adicionar ao SVN Enviar para o SVN... Remover do SVN Reversão do SVN Actualização do SVN Mostrar as Alterações Locais do SVN Mostrar as Actualizações do SVN Descrição: Envio para o SVN Mostrar as actualizações 