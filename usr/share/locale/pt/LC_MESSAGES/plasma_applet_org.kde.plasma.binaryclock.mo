��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �  
   6     A     H     \     n  0   �  (   �  2   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-02 12:33+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: LED
 Aparência Cores: Mostrar os segundos Desenhar a grelha Mostrar os LED's inactivos: Usar uma cor personalizada para os LED's activos Usar uma cor personalizada para a grelha Usar uma cor personalizada para os LED's inactivos 