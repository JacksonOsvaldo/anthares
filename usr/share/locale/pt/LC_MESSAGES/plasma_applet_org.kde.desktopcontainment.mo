��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l               $     8     ?     F     T     h     }     �     �     �     �     �     �     �     �     �     �          	          2     7     F     R     ^     q  %   �     �     �     �     �          	     (     8     H     _  '   b     �     �     �     �     �     �     �     �     �  	   �     �          "     '     ,     /  H   A     �     �     �     �     �     �     �     �      �               0     I  &   d     �  4   �     �  '   �               #     2     >     J     W     j  
   z     �     �     �     �     �     �     �     �       p        �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-02 11:14+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 &Apagar &Esvaziar o Lixo E&nviar para o Lixo &Abrir Co&lar &Propriedades Actualiza&r o Ecrã Actualiza&r a Janela &Recarregar Muda&r o Nome Escolher... Limpar o Ícone Alinhar Aparência: Organizar Por Organizar por Disposição: Recuar Cancelar Colunas Configurar o Ecrã Título personalizado Data Predefinição Descendente Descrição Deseleccionar Tudo Disposição do Ecrã Indique aqui um título personalizado Funcionalidades: Padrão de nomes de ficheiros: Tipo de ficheiro Tipos de ficheiros: Filtro Janelas de antevisão da pasta Pastas Primeiro Pastas primeiro Localização completa Ok Esconder os Ficheiros Correspondentes a Enorme Tamanho dos Ícones Ícones Grande Esquerda Lista Localização Localização: Bloquear no local Bloqueado Médio Mais Opções de Antevisão... Nome Nada OK Botão do painel: Carregue e mantenha pressionados os elementos para revelar as suas pegas 'Plugins' de Antevisão Miniaturas de antevisão Remover Dimensionar Repor Direita Rodar Linhas Procurar por tipo de ficheiro... Seleccionar Tudo Seleccionar a Pasta Marcadores da selecção Mostrar Todos os Ficheiros Mostrar os Ficheiros Correspondentes a Mostrar um local: Mostrar os ficheiros associados à actividade actual Mostrar a pasta do ecrã Mostrar a área de ferramentas do ecrã Tamanho Pequeno Pequeno-Médio Ordenar Por Ordenar por Ordenação: Indique uma pasta: Linhas de texto Minúsculo Título: Dicas Ajustes Tipo Indique aqui um local ou um URL Não Ordenado Usar um ícone personalizado Tratamento do Elemento Elementos desbloqueados Poderá carregar e manter pressionado o botão do rato sobre os elementos para os mover e revelar as suas pegas. Modo de visualização 