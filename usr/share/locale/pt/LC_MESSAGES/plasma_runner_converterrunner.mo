��          4      L       `   �   a   L   �   z  E  �   �     w                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: krunner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-12-03 14:20+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: parsecs cp nanómetro pints ml cúb pol gal jd parsec
X-POFile-SpellExtra: jardas fl pint mach machs náuticas mph nmi
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: microgramas centigramas decigramas ua carate carates
 Converte o valor de :q: quando o :q: for composto por "valor unidade [>, para, como, em] unidade". Poderá usar o conversor de unidades para descobrir todas as unidades disponíveis. em;para;como 