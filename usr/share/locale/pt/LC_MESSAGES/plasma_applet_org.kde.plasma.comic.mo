��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  M   A  )   �  !   �     �  (   �     !     1  	   M     W  (   \  
   �  -   �  =   �     �  (        -     =     [  G   i  +   �  &   �       8        F     a  4   u  ;   �     �     �             1   ;     m     �  %   �  '   �      �           E   $  /   j  
   �  +   �      �  "   �  *     %   @  !   f  ,   �     �  (   �     �            
         !     B     F     L     T     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-04-17 14:20+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: cbz Zip
Plural-Forms: nplurals=2; plural=n != 1;
 

Escolha a banda desenhada anterior para ir para a última banda em 'cache'. &Criar um Arquivo de Bandas Desenhadas... Gravar a Banda De&senhada Como... Número da Banda De&senhada: *.cbz|Arquivo de Bandas Desenhadas (Zip) Tamanho &Actual Guardar a &posição actual Avançado Tudo Ocorreu um erro para o identificador %1. Aparência O arquivo da banda desenhada foi mal-sucedido Actualizar automaticamente os 'plugins' de bandas desenhadas: 'Cache' Procurar bandas desenhadas novos a cada: Banda Desenhada 'Cache' de bandas desenhadas: Configurar... Não foi possível criar o pacote do arquivo na localização indicada. Criar um Arquivo de Bandas Desenhadas do %1 A Criar o Arquivo de Bandas Desenhadas Destino: Mostrar um erro se não obter o campo da banda desenhada Obter as Bandas Desenhadas Tratamento de Erros Não foi possível adicionar um ficheiro ao arquivo. Não foi possível criar o ficheiro com o identificador %1. Desde o início até ... Desde o fim até ... Geral Obter Bandas Desenhadas Novas... A obtenção da banda desenhada foi mal-sucedida: Ir para a Banda Desenhada Informação Saltar para a banda-desenhada a&ctual &Saltar para a primeira banda-desenhada Saltar para a Banda-Desenhada... Intervalo manual Talvez não esteja disponível nenhuma ligação à Internet.
Talvez o 'plugin' de banda desenhada esteja com problemas.
Outra razão possível será que não exista nenhuma banda desenhada para este dia/número/texto, pelo que poderá resultar se escolher uma diferente. Botão do meio sobre a banda desenhada para a ver no tamanho original Não existe nenhum ficheiro ZIP, a interromper. Intervalo: Mostrar as setas apenas à passagem do rato Mostrar o URL da banda desenhada Mostrar o autor da banda desenhada Mostrar o identificador da banda desenhada Mostrar o título da banda desenhada: Identificador da banda desenhada: O intervalo de bandas desenhadas a arquivar. Actualização Visitar a página Web da banda desenhada Visitar a página &Web da loja # %1 dias dd.MM.yyyy Pági&na com Banda Nova Seguinte De: Até: minutos bandas desenhadas por servidor 