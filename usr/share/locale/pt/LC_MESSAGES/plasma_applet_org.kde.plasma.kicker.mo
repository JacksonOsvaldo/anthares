��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �     �     �     �     �  *        3     I     Q     ^     x  
   �  
   �  	   �     �     �  
   �     �     �       :     	   Y      c     �     �     �     �     �               *     J     P     X     a     o     �     �     �     �     �     �     �     �                .  
   C     N     i     p     �  	   �     �     �     �     �     �     �          -     D     [     {     �     �     �     �     �  %   �     �       "   -  $   P  %   u      �     �     �     �            9   #  	   ]     g     |     �     �     �     �     �     �  &     	   4         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_kicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-10 00:55+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: mails svgz jpg
Plural-Forms: nplurals=2; plural=n != 1;
 Gerir o '%1'... Escolher... Limpar o Ícone Adicionar ao Ecrã Adicionar aos Favoritos Adicionar ao Painel (Elemento) Alinhar os resultados da pesquisa ao fundo Todas as Aplicações %1 (%2) Aplicações Aplicações & Documentos Comportamento Categorias Computador Contactos Descrição (Nome) Apenas a descrição Documentos Editar a Aplicação... Editar as Aplicações... Terminar a sessão Expandir a pesquisa para os favoritos, ficheiros e e-mails Favoritos Alisar o menu a um único nível Esquecer Tudo Esquecer Todas as Aplicações Esquecer Todos os Contactos Esquecer Todos os Documentos Esquecer a Aplicação Esquecer o Contacto Esquecer o Documento Esquecer os Documentos Recentes Geral %1 (%2) Hibernar Esconder o %1 Esconder a Aplicação Ícone: Bloquear Bloquear o ecrã Encerrar Nome (Descrição) Apenas o nome Aplicações Frequentes Documentos Frequentes Usadas frequentemente Em Todas as Actividades À Actividade Actual Abrir com: Fixar ao Gestor de Tarefas Locais Energia / Sessão Propriedades Reiniciar Aplicações Recentes Contactos Recentes Documentos Recentes Usados Recentemente Usadas recentemente Armazenamento Removível Remover dos Favoritos Reiniciar o computador Executar um Comando... Executar um comando ou pesquisa Gravar a Sessão Procurar Resultados da procura Procurar... A procurar por '%1' Sessão Mostrar a Informação do Contacto... Mostrar nos Favoritos Mostrar as aplicações como: Mostrar as aplicações frequentes Mostrar os contactos mais frequentes Mostrar os documentos mais frequentes Mostrar as aplicações recentes Mostrar os contactos recentes Mostrar os documentos recentes Mostrar: Desligar Ordenar alfabeticamente Iniciar uma sessão paralela como um utilizador diferente Suspender Suspender para a RAM Suspender para o disco Mudar de Utilizador Sistema Acções do sistema Desligar o computador O tipo a pesquisar. Mostrar as Aplicações em '%1' Mostrar as Aplicações neste Sub-Menu Elementos 