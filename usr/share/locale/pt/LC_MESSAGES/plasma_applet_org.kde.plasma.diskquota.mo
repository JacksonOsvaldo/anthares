��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  -   �     �  F     #   N     r  	   {     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-20 12:29+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Quota do Disco Não foram encontradas restrições de quota. Por favor instale o 'quota' A ferramenta Quota não foi encontrada.

Por favor, instale o 'quota'. Não foi possível executar o Quota %1 de %2 %1 livres Quota: %1% em uso %1: %2% usado 