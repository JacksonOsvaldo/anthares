��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w  �  �       
   �  2   �  -   �     
     $     4     M     f                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: plasma_applet_org
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-07-10 09:51+0100
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-POFile-SpellExtra: KScreen
 Foi detectado um novo ecrã %1 Desactivar Não foi possível contactar o serviço do KScreen Não foi possível carregar o objecto de raiz Copiar a Saída Primária Nenhuma Acção Configuração Avançada Estender para a Esquerda Estender para a Direita 