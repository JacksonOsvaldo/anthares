��    D      <  a   \      �     �     �     �  !     "   %  &   H  ,   o  #   �  &   �  !   �  &   	  '   0  "   X  #   {  !   �  "   �  '   �       +     2   <     o  
   �     �     �     �     �     �     �     �     �     	  !   	  +   *	     V	     v	      {	     �	     �	     �	     �	     �	  !   �	     
  	    
     *
     2
     R
     m
  &   �
     �
     �
     �
     �
     �
     �
     �
            $        A     R     l     ~     �     �     �  >   �  �       �     �  *   �                          7     >  
   J     U     h     u     |     �     �     �  	   �  ;   �  D   �     *     F     R     h     }     �     �     �     �  "   �     �  0   �  9     (   V       -   �     �  "   �     �       %     *   -     X     a     h  )   n  "   �     �  0   �  $   �     $  )   ,     V     ^  	   f  
   p     {  	   �  -   �     �  #   �     	     "  #   <     `  &   r     �     D   8      1                 )                  .               3   @   4       &      /   >       ?      <   ;             7       (          
                 $                  0   5       A   :   #   2         C   +                        =   '             *                 	          ,   !                             B   9   6       "       %   -     ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Medium @item:inlistbox Button size:None @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-01-09 10:23+0000
Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>
Language-Team: Portuguese <kde-i18n-pt@kde.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POFile-SpellExtra: ms Oxygen px
Plural-Forms: nplurals=2; plural=n != 1;
  ms % Corresponde&nte à propriedade da janela:  Enorme Grande Sem Contorno Sem Contornos Laterais Normal Ainda Maior Minúsculo Mais do que Enorme Muito Grande Grande Médio Nenhum Pequeno Muito Grande Adicionar Adicionar uma pega para dimensionar as janelas sem contorno Permitir dimensionar as janelas maximizadas nos extremos das janelas Duração das &animações: Animações Tamanho dos &botões: Tamanho do contorno: Centro Centro (Largura Total) Classe:  Cor: Opções de Decoração Detectar as Propriedades da Janela Janela Desenhar um círculo em torno do botão de fecho Desenhar um Separador entre a Barra do Título e a Janela Desenhar um gradiente no fundo da janela Editar Editar a Excepção - Configuração da Brisa Activar as animações Activar/desactivar esta excepção Tipo de Excepção Geral Esconder a barra de título da janela Informação Acerca da Janela Seleccionada Esquerda Descer Subir Nova Excepção - Configuração da Brisa Pergunta - Configuração da Brisa Expressão Regular A sintaxe da expressão regular está incorrecta Expressão regular a &corresponder:  Remover Deseja remover a excepção seleccionada? Direita Sombras &Tamanho: Minúsculo A&linhamento do título: Título:  Usar a classe da janela (aplicação inteira) Usar o título da janela Atenção - Configuração da Brisa Nome da Classe da Janela Identificação da Janela Selecção da Propriedade da Janela Título da Janela Substituições Específicas da Janela Po&tência: 