��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     =     D     c     s     �     �  	   �     �  	   �     �     �     �  	   �     	  "   $	  +   G	     s	     �	     �	  	   �	     �	     �	     �	  	   �	     	
     
     2
     9
     R
     Y
     s
  !   z
  	   �
  (   �
     �
                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-22 10:35+0800
Last-Translator: Franklin Weng <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <kde-tw@googlegroups.com>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 作者 Copyright 2006 Sebastian Sauer Sebastian Sauer 要執行的文稿。 一般 新增文稿。 新增... 要取消嗎？ 註解： franklin@goodhorse.idv.tw 編輯 編輯選取的文稿。 編輯... 執行所選取的文稿。 無法建立直譯器 %1 的文稿 無法決定文稿檔 %1 使用的直譯器 無法載入直譯器 %1 無法開啟文稿檔 %1 檔案: 圖示： 直譯器： Ruby 直譯器的安全等級 Frank Weng (a.k.a. Franklin) 名稱： 找不到函式 %1 找不到直譯器 %1 移除 移除所選的文稿。 執行 文稿檔案 %1 不存在 停止 停止執行所選取的文稿。 文字： 執行 Kross 文稿的命令列工具。 Kross 