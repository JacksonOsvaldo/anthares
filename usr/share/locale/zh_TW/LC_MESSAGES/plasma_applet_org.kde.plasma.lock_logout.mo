��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  9   �  6   "     Y     `     g  $   n     �  	   �     �     �  $   �     �  '   �  0   	     :     A     Q     a        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-11-03 19:22+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 您要暫停並儲存到記憶體嗎（睡眠模式）？ 您要暫停並儲存到磁碟嗎（冬眠模式）？ 一般 動作 睡眠 冬眠（暫停並儲存到磁碟） 離開 離開... 鎖定 鎖定螢幕 登出，關閉或重新啟動電腦 否 睡眠（暫停並儲存到記憶體） 以另一個使用者開始平行的作業階段 暫停 切換使用者 切換使用者 是 