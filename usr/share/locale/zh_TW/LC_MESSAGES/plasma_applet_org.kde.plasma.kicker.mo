��    Y      �     �      �  C   �     �     �          #     B     S     s     �     �  
   �     �     �     �     �  	   �     �     �     		  ,   	  	   B	     L	  
   k	     v	     �	     �	     �	     �	     �	     �	     
     	
  	   )
     3
     ;
     L
     Q
     ]
     d
  	   w
     �
     �
  
   �
  
   �
     �
     �
     �
  
   �
     �
               %     6     D     R     d     z     �     �     �     �     �  	   �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     d     s     �     �     �     �     �     �     �     	            	        (     ;     H     O     e     {  0   �     �     �     �     �               +     >     N     [     q     x     �  	   �     �     �     �     �     �     �     �     �               .     G     N     d     k     x     �     �     �     �     �     �          "  !   2     T     g     n  	   {     �     �     �     �  !   �     �       !   9     [     z  	   �     �     �  0   �     �     �          $     4     ;     H     U     h     �     �         K   
      S   0           #               D                     '       ;   P   /   U       <   %          O   =   W           "   Y         ,   9       ?   E      *                  :      )   I   C       L   3   1   Q   -   V   8   R   @   !       H   >       N   B   G          .             4             (          M   X       A           6   5                    $   7   F          J          2          +                   	                 &               T       @action opens a software center with the applicationManage '%1'... Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-06-29 21:12+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 管理 '%1'... 新增到桌面 新增到我的最愛 新增到面板（元件） 將搜尋結果置於底部 所有應用程式 %1 (%2) 應用程式 Apps 文件(&D) 行為 類別 電腦 聯絡人 描述（名稱） 只有描述 文件 編輯應用程式... 編輯應用程式... 工作階段結束 將搜尋擴展到書籤、檔案與電子郵件 我的最愛 將選單攤平 全部遺忘 遺忘所有應用程式 遺忘所有聯絡人 遺忘所有文件 遺忘應用程式 遺忘聯絡人 遺忘文件 忘掉最近的文件 一般 %1 (%2) 冬眠 隱藏 %1 隱藏應用程式 鎖定 鎖定螢幕 登出 名稱（描述） 只有名稱 經常使用的應用程式 經常使用的文件 經常使用 開啟方式： 釘選到工作管理員 書籤 電源 / 工作階段 內容 重新開機 最近使用的應用程式 最近使用的聯絡人 最近使用的文件 最近使用 最近使用 可移除的儲存裝置 從我的最愛中移除 重新啟動電腦 執行指令... 執行指令或輸入搜尋字串 儲存工作階段 搜尋 搜尋結果 搜尋... 正在搜尋「%1」 工作階段 顯示聯絡人資訊... 顯示應用程式方式： 顯示經常使用的應用程式 顯示經常使用的聯絡人 顯示經常使用的文件 顯示最近使用的應用程式 顯示最近使用的聯絡人 顯示最近使用的文件 顯示： 關機 依字母排序 以另一個使用者開始平行的作業階段 暫停 暫停並儲存到記憶體 暫停並儲存到磁碟 切換使用者 系統 系統動作 關閉電腦 輸入以搜尋。 在 '%1' 中取消隱藏 在此子選單中取消隱藏 元件 