��    R      �  m   <      �     �     �     
               &     2     C     Q     Y  /   a  -   �     �  
   �     �     �     �     �     �            
             (     7     O     b     n     u     �     �  	   �     �     �     �  	   �     �     �     �     �     �     �     	     	     	     .	     3	     8	  <   ;	     x	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     $
     8
  )   F
     p
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                  E   1  �  w  
             4  
   N  
   Y  
   d     o     �     �     �  	   �     �     �  	   �     �     �     �     �     �                         /     <     U     k  	   {     �     �     �     �     �     �     �     �     �          
                 	   /     9     =     S     Z     ^  <   e     �     �     �     �     �     �     �     �     �               "     /     B     X  $   h     �     �     �     �     �     �  	   �     �     �     �          
  	   )     3     @  F   V                J   :   R   .      E       M      B       O   ?                3   #       7       8          L   )         9   I   F   ;   *      2   5              N          C       "   K   
      +            Q      %                    	       4                 ,   H   '   G           >   <   =          (   -           1      D   /       @   0   &   A          !   $   6           P                     &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-01-24 15:41+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 刪除(&D) 清空資源回收桶(&E) 移到資源回收筒(&M) 開啟(&O) 貼上(&P) 屬性(&P) 重整桌面(&R) 更新檢視(&R) 重新載入(&R) 重新命名(&R) 選擇... 清除圖示 對齊 安排於 返回 取消 欄 設定桌面 自訂標題 日期 預設 遞減 全部取消選取 桌面佈局 在此輸入自訂標題 檔案名稱樣式： 檔案型態： 過濾器 資料夾預覽彈出視窗 資料夾優先 資料夾優先 完整路徑 好了 隱藏符合的檔案 巨大 圖示大小 圖示 大 置左 清單 位置 鎖定於書籤 已鎖定 中 更多預覽選項... 名稱 無 確定 按住元件以移動它們，並重新顯示它們的把手 預覽外掛程式 預覽縮圖 移除 改變大小 回復 置右 旋轉 列 搜尋檔案型態... 全部選取 選擇資料夾 選擇標記 顯示所有檔案 顯示符合的檔案 顯示書籤： 顯示連結到目前活動的檔案 顯示桌面資料夾 顯示桌面工具盒 大小 小 中小 排序依據 排序： 指定資料夾： 極小 調整 類型 請在此輸入路徑或網址 不排序 元件處理 元件已解除鎖定 您可以按住元件以移動它們，並重新顯示它們的把手. 