Þ          ¬      <      °     ±     Æ     Ø  7  ë  Ð  #  +   ô  ^                   ¤  x   ¬  Ô   %     ú               ;     U  Ò  r     E	     e	     }	  Ü   	  Þ  l
  ,   K  R   x     Ë     Ý     ð     ÷  Ö   x     O     f  &   s        (   »                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-02-10 12:18+0800
Last-Translator: Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 0.3
 çµæç®åçå·¥ä½éæ®µ(&E) éæ°ååé»è¦ (&R) ééé»è¦ (&T) <h1>å·¥ä½éæ®µç®¡çå¡</h1>æ¨å¯ä»¥å¨ééçµæå·¥ä½éæ®µç®¡çå¡ã éåå«äºç»åºææ¯å¦è¦ç¶éç¢ºèªãæ¯å¦è¦å¨ç»åºæå²å­å·¥ä½éæ®µï¼ä»¥åæ¯å¦è¦å¨å·¥ä½éæ®µçµæå¾èªåéæ©ã <ul>
<li><b>æ¢å¾©åä¸åå·¥ä½éæ®µï¼</b> æå¨é¢éæå²å­ææå·è¡çæç¨ç¨å¼ä¸¦å¨ä¸æ¬¡ååæå°ä»åæ¢å¾©</li>
<li><b>æ¢å¾©æåå²å­çå·¥ä½éæ®µï¼</b> å¯ä»¥å¨ä»»ä½æåéé K-é¸å®ä¸­çãå²å­å·¥ä½éæ®µãä¾å²å­å·¥ä½éæ®µãéè¡¨ç¤ºç®åååçæç¨ç¨å¼æå¨ä¸æ¬¡ååææ¢å¾©ã</li>
<li><b>ä»¥ç©ºçå·¥ä½éæ®µååï¼</b> ä¸è¦å²å­ä»»ä½æ±è¥¿ãä¸æ¬¡ååææçå°çç©ºç¡ä¸ç©çæ¡é¢ã</li>
</ul> è¦èªå·¥ä½éæ®µæé¤çæç¨ç¨å¼(&X): é¸åéåé¸é å¦ææ¨å¸æå·¥ä½éæ®µç®¡çå¡é¡¯ç¤ºçåºç¢ºèªå°è©±çª. ç¢ºèªç»åº (&i) é è¨­é¢éé¸é  ä¸è¬ å¨éè£¡æ¨å¯ä»¥é¸æå¨æ¨ç»åºææè¦å·è¡çåä½ãä¸ééåªæå¨æ¨ä½¿ç¨ KDM ä¾ç»å¥ç³»çµ±æææçæã æ¨å¯ä»¥å¨éè£¡è¼¸å¥ä¸æ³å²å­å¨å·¥ä½éæ®µä¸­çæç¨ç¨å¼æ¸å®(ä»¥éèæåèåé)ï¼éæ¨£ä¸æ¬¡æ¢å¾©å·¥ä½éæ®µæå®åå°±ä¸æè¢«ååãä¾å¦ãxterm,konsoleãæãxterm:konsoleãã æä¾éæ©é¸é (&F) æ­£å¨ç»å¥ åå¾©æåå­å¥çå·¥ä½éæ®µ (&M) åå¾©ååçå·¥ä½éæ®µ (&P) ä½¿ç¨ç©ºç½çå·¥ä½éæ®µä¾åå(&S) 