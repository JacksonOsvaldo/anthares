��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �               &  $   3     X     q     ~     �  	   �     �  "   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-25 08:18+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 設定印表機(&C)... 只顯示進行中的工作 所有工作 只顯示已完成的工作 設定印表機 一般 沒有進行中的工作 沒有工作 沒有設定或發現任何印表機 %1 個進行中的工作 %1 個工作 開啟列印佇列 列印佇列是空的 印表機 搜尋印表機... 有 %1 個列印工作在佇列中 