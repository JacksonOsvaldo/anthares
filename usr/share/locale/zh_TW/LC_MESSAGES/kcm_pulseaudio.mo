��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  '   �  !   "	     D	     c	     	     �	  
   �	     �	     �	     �	  B   �	     
  9   $
     ^
     e
     l
  -   |
     �
     �
     �
     �
     �
     �
  	   �
     �
     
  )     3   D     x     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-06-29 21:03+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: Traditional Chinese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 100% 作者 Copyright 2015 Harald Sitter Harald Sitter 沒有正在播放音效的應用程式 沒有正在錄音的應用程式 沒有可用的裝置設定檔 沒有可用的輸入裝置 沒有可用的輸出裝置 設定檔： PulseAudio 進階 應用程式 裝置 在所有本機音效卡上加入虛擬輸出裝置以同步輸出 進階輸出設定 當新輸出可用時自動切換所有執行中的串流 擷取 預設 裝置設定檔 franklin@goodhorse.idv.tw, s8321414@gmail.com 輸入 靜音 Franklin Weng, Jeff Huang 通知音效 輸出 播放 連接埠 （無法使用） （未插入） 需要「module-gconf」PulseAudio 模組 此模組可以設定 Pulseaudio 音效子系統。 %1：%2 100% %1% 