��            )   �      �  �   �  	   G     Q     a     w       	   �     �  	   �     �  %   �     �     �                    %     .  X   =  S   �  �   �  I   �  F   �  E   >  H   �  B   �  :     7   K  �  �  �   R	      
     
     #
     0
     7
     G
     N
     ^
     e
     u
     |
     �
     �
     �
     �
     �
     �
  U   �
  R     �   q  3     3   E  3   y  3   �  0   �                                                                  	                                                                 
                 <h1>Paths</h1>
This module allows you to choose where in the filesystem the files on your desktop should be stored.
Use the "Whats This?" (Shift+F1) to get help on specific options. Autostart Autostart path: Confirmation Required Desktop Desktop path: Documents Documents path: Downloads Downloads path: Move files from old to new placeMove Move the directoryMove Movies Movies path: Music Music path: Pictures Pictures path: The path for '%1' has been changed.
Do you want the files to be moved from '%2' to '%3'? The path for '%1' has been changed.
Do you want to move the directory '%2' to '%3'? This folder contains all the files which you see on your desktop. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder will be used by default to load or save documents from or to. This folder will be used by default to load or save movies from or to. This folder will be used by default to load or save music from or to. This folder will be used by default to load or save pictures from or to. This folder will be used by default to save your downloaded items. Use the new directory but do not move anythingDo not Move Use the new directory but do not move filesDo not Move Project-Id-Version: kcmkonq
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2009-12-02 18:20+0800
Last-Translator: Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 <h1>路徑</h1>
這個模組允許您選擇您桌面上的檔案要儲存在檔案系統的何處。
使用「這是什麼？」 (Shift+F1) 以獲取指定選項的說明。 自動啟動 自動啟動路徑： 要求確認 桌面 桌面路徑： 文件 文件路徑： 下載 下載路徑： 移動 移動 影片 影片路徑： 音樂 音樂路徑： 圖片 圖片路徑： 「%1」的路徑已被更改；
您確定要將檔案從「%2」移至「%3」嗎？ 「%1」的路徑已被更改；
您確定要將目錄「%2」移至「%3」嗎？ 這個目錄包含所有在您桌面上看到的檔案。如果您想要的話，您可以改變目錄位置，而內容將會自動地搬移至新的位置。 此目錄是預設用來讀取或儲存文件的。 此目錄是預設用來儲存或載入影片的。 此目錄是預設用來儲存或載入音樂的。 此目錄是預設用來儲存或載入圖片的。 此目錄是預設用來儲存下載檔案的。 不要移動 不要移動 