��    @        Y         �     �     �     �  !   �  "   �  &   �  ,     #   <  &   `  !   �  &   �  '   �  "   �  "     '   >     f  +   j     �  
   �     �     �     �     �     �     �     �          (  !   /     Q     q      v     �     �     �     �     �  !   �     	  	   	     %	     -	     M	     h	  &   {	     �	     �	     �	     �	     �	     �	     �	     �	     
  $   
     <
     M
     g
     y
     �
     �
     �
  >   �
  �       �     �     �     �     �     �     �     �                  	   !     +     /  	   3     =  *   D     o     �     �     �     �     �  	   �  	   �     �     �  	     !        .     G     N     k     x     �     �     �     �     �     �     �     �               +  "   J     m     t     �     �     �     �     �  	   �  *   �     �          '     :     G     Z     g     z     %       +      4      6         2   "       #   :   *      @                         <       !              	   >      ;   5                              )      
   $                  .   8      9           &                  0                      ?       1   '      7          (         /       -          ,   =   3         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2017-11-11 20:27+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 毫秒 % 比對視窗屬性(&M)： 巨大 大 沒有邊界 沒有邊框 一般 超過大小 極小 非常巨大 非常大 大 小 非常大 新增 新增把手來調整沒有邊框的視窗 動畫期間(&T)： 動畫 按鍵大小(&U)： 邊界大小： 置中 置中（完整寬度） 類別： 顏色： 裝飾選項 偵測視窗屬性 對話框 在關閉按鈕外面畫一個圈 繪製視窗背景漸層 編輯 編輯例外 - Breeze 設定 開啟動畫 開啟/關閉此例外 例外型態 一般 隱藏視窗標題列 選取的視窗的資訊 置左 下移 上移 新增例外 - Breeze 設定 問題 - Breeze 設定 正規表示式 正規表示式語法不正確 要比對的正規表示式(&T)： 移除 要移除選取的例外嗎？ 置右 陰影 大小(&Z): 極小 標題對齊(&L)： 標題： 使用視窗類別（整個應用程式） 使用視窗標題 警告 - Breeze 設定 視窗類別名稱 視窗識別 視窗屬性選擇 視窗標題 視窗指定覆寫 強度(&T)： 