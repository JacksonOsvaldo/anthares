��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �  	   �     �  
   �     �  
   �  
   �     �       %     
   C     N     b     v     �     �     �  ?   �       9   $     ^     j     }  N   �     �     �  h       {     �     �     �     �     �     �  	   �     �  0   �     ,     H  
   W  
   b  
   m     x     �     �     �     �     �     �     �     �          1     M     c     p     �     �     �  B   �       L               3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-09-14 15:56+0800
Last-Translator: Franklin Weng <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <kde-tw@googlegroups.com>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
  [隱藏] 註解(&C): 刪除(&D) 描述(&D): 編輯(&E) 檔案(&F) 名稱(&N): 新增子選單(&N)... 用不同使用者身分來執行(&R) 排序(&S) 依描述排序(&D) 依名稱排序(&S) 依描述排序選取項目(&S) 依名稱排序選取項目(&S) 使用者名稱(&U): 工作路徑(&W): (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter 進階 %1 的所有子選單都將被移除。您要繼續嗎？ 指令(&M): 無法寫入至 %1 目前的捷徑鍵(&K): 您要回復系統選單嗎？警告：您將會移除所有的自定選單。 ericc@shinewave.com.tw 啟用執行回饋(&L) 下列指令中，您可以使用許多在程式實際執行時會被置換為真實數值的佔位符：
%f - 單一的檔案名稱
%F - 一組檔案清單；用於能一次開啟多個本地端檔案的應用程式
%u - 單一的 URL
%U - 一組 URL 清單
%d - 要開啟檔案的資料夾
%D - 一組資料夾清單
%i - 圖示
%m - 迷你圖示
%c - 說明 一般 一般選項 隱藏的項目 項目名稱: KDE 選單編輯器 KDE 選單編輯器 主工具列 維護者 Matthias Elter 選單的變更由於下列問題無法儲存： 預先選擇的選單項目 Montel Laurent 下移(&D) 上移(&U) Eric Cheng 新增項目(&I)... 新增項目 新增分隔(&E) 新增子選單 只在 KDE 顯示 原始作者 之前的維護者 Raffaele Sandrini 回復為系統選單 在終端機視窗執行(&T) 是否儲存選單變更？ 顯示隱藏的項目 拼字檢查 拼字檢查選項 預先選擇的子選單 子選單名稱: 終端機選項(&O): 無法聯繫 khotkeys。您的變更已儲存，但無法啟動。 Waldo Bastian 您已經對選單做了變更。
您想要儲存變更或是放棄它們？ 