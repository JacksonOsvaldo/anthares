��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {          (  
   5  
   @     K     R  	   Y     c     j     w     ~     �  
   �     �  	   �     �  	   �  	   �     �     �  	     H                          	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-06-09 17:29+0800
Last-Translator: Franklin
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 %1 執行中 %1 未執行 重置(&R) 開始(&S) 進階 外觀 指令： 顯示 執行指令 通知 剩餘時間：%1 秒 執行指令 停止(&T) 顯示通知 顯示秒 顯示標題 文字： 計時器 時間到！ 計時器執行中 標題： 使用滑鼠滾輪來改變數字，或是選擇預先定好的計時器 