��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j               #     '     4     A     H     U     \  	   i     s     w     ~  	   �     �     �  *   �     �     �     �     �            	   8  N   B  9   �     �     �  	   �     �     �          0     =     D     Z     s     z     �     �     �     �     �  "   �               4     ;     B  	   V  *   `  *   �     �     �     �     �               ,     9     X         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2016-11-03 19:14+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 比對視窗屬性(&M)： 巨大 大 沒有邊界 沒有邊框 一般 超過大小 極小 非常巨大 非常大 大 一般 小 非常大 作用中視窗發光 新增 新增把手來調整沒有邊框的視窗 動畫 按鍵大小(&U)： 邊界大小： 滑鼠覆蓋按鈕轉變 置中 置中（完整寬度） 類別： 設定視窗陰影間的淡化效果，及視窗狀態改變時的閃爍效果 設定視窗按鈕在滑鼠覆蓋其上時的突顯動畫 裝飾選項 偵測視窗屬性 對話框 編輯 編輯例外 - Oxygen 設定 開啟/關閉此例外 例外型態 一般 隱藏視窗標題列 選取的視窗的資訊 置左 下移 上移 新增例外 - Oxygen 設定 問題 - Oxygen 設定 正規表示式 正規表示式語法不正確 要比對的正規表示式(&T)： 移除 要移除選取的例外嗎？ 置右 陰影 標題對齊(&L)： 標題： 標題列與視窗內容使用相同顏色 使用視窗類別（整個應用程式） 使用視窗標題 警告 - Oxygen 設定 視窗類別名稱 視窗下拉時的陰影 視窗識別 視窗屬性選擇 視窗標題 視窗狀態變更時的轉變 視窗指定覆寫 