��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  o  K     �     �  1  �  J    L   W	     �	     �	     �	     �	     �	     �	     
     +
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2003-12-16 15:58+0800
Last-Translator: 
Language-Team:  <en@li.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.2
Plural-Forms: nplurals=1; plural=0;
  秒 啟動作業逾時(&S)： <H1>工作列提示</H1>您也可以啟動第二種通知您應用程式啟動的方法
當程式啟動時, 工作列上將會有一個旋轉的磁碟圖案
某些應用程式可能不認識此種啟動方式
注意。在這種情況下，按鈕將會在'啟動作業逾時'所設定的秒數之後
消失 <h1>忙碌指標</h1>
在應用程式啟動時, KDE 會提供一個忙碌的滑鼠指標來通知您.
要啟用忙碌指標的服務, 從選項中選擇一種視覺回饋某些應用程式可能不認識此種啟動方式
注意。在這種情況下，滑鼠游標將會在'啟動作業逾時'所設定的秒數之後
停止閃動 <h1>啟動回饋</h1> 您可以在這裡設定應用程式啟動時的回饋 閃爍指標 彈跳指標 忙碌指標(&Y) 啟動工作列提示通知 沒有忙碌指標 被動忙碌指標 啟動作業逾時(&U)： 工作列提示通知(&N) 