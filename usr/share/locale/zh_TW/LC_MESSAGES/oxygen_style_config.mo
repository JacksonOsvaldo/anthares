��    =        S   �      8     9  !   V  !   x     �  
   �     �     �  (   �  I     :   X  <   �  2   �  .     1   2  (   d  1   �  o   �     /  !   6     X  0   x     �     �     �     �     	     :	     ?	  ,   N	     {	     �	     �	     �	  !   �	     �	     �	     �	     
  	   
  
   
  
   '
     2
     B
     Y
     u
  
   �
  &   �
     �
     �
     �
     �
     
          &     D     J     _     b     w     �  �  �  %   8     ^     z     �     �     �     �  $   �  9     0   >  3   o  *   �  '   �  '   �       *   7  [   b  	   �  $   �     �  3   	  !   =     _     {     �  !   �     �     �  *   �               1     8     ?     [     h     u     �     �     �     �     �     �     �     �  	             0     =     S     c          �  %   �     �     �     �     �     �     �     1   "   :   ;                                  	   -       
   3   $   /                 8          =   4   6          0      %   )          (   *      2              9              7       +                       !                          5      #   &   ,                     <             .   '    &Use selection color (plain) Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Animation type: Animations Bottom arrow button type: Combo box transitions Configure fading transition between tabs Configure fading transition when a combo box's selected choice is changed Configure fading transition when a label's text is changed Configure fading transition when an editor's text is changed Configure menu bars' mouseover highlight animation Configure menus' mouseover highlight animation Configure progress bars' busy indicator animation Configure progress bars' steps animation Configure toolbars' mouseover highlight animation Configure widgets' focus and mouseover highlight animation, as well as widget enabled/disabled state transition Dialog Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw focus indicator in lists Draw toolbar item separators Draw tree branch lines Draw window background gradient Enable extended resize handles Fade Fade duration: Focus, mouseover and widget state transition Follow Mouse Follow mouse duration: Frame General Keyboard accelerators visibility: Label transitions Menu Highlight Menu bar highlight Menu highlight No button No buttons One button Oxygen Settings Progress bar animation Progress bar busy indicator Scrollbar wi&dth: Scrollbars Show Keyboard Accelerators When Needed Tab transitions Text editor transitions Toolbar highlight Top arrow button type: Two buttons Use dar&k color Use selec&tion color (subtle) Views Windows' &drag mode: px triangle sizeNormal triangle sizeSmall triangle sizeTiny Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2015-02-15 19:51+0800
Last-Translator: Franklin
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 使用選取區顏色（樸素）(&U) 總是隱藏鍵盤加速器 總是顯示鍵盤加速器 動畫型態： 動畫 底端箭頭按鍵型態： 下拉式選單轉變 設定分頁間的淡入淡出轉變 設定下拉式選單選取變更時的淡入淡出轉變 設定標籤文字變更時的淡入淡出轉變 設定編輯器文字變更時的淡入淡出轉變 設定選單列的滑鼠覆蓋突顯動畫 設定選單的滑鼠覆蓋突顯動畫 設定進度列的忙碌指示器動畫 設定進度列的動畫 設定工具列的滑鼠覆蓋突顯動畫 設定元件的焦點、滑鼠覆蓋突顯動畫，以及元件開啟/關閉狀態的轉變 對話框 可從所有空白區域拖曳視窗 只從標題列拖曳視窗 可從標題列、選單列與工具列拖曳視窗 在清單中繪製焦點指示器 畫工具列元件分隔線 繪製樹狀分支線 繪製視窗背景漸層 開啟延伸的調整大小元件 淡出 淡出期間： 焦點、滑鼠覆蓋與元件狀態轉變 跟隨滑鼠 跟隨滑鼠期間： 框架 一般 鍵盤加速器可見度： 標籤轉變 選單突顯 選單列突顯 選單突顯 沒有按鍵 沒有按鍵 一個按鍵 Oxygen 設定 進度列動畫 進度列忙碌指示器 捲軸列寬度(&D)： 捲軸列 需要時顯示鍵盤加速器 分頁轉變 文字編輯器轉變 工具列突顯 頂端箭頭按鍵型態： 兩個按鍵 使用深色(&K) 使用選取區顏色（精確）(&T) 檢視 視窗的拖曳模式(&D)： 像素 一般 小 極小 