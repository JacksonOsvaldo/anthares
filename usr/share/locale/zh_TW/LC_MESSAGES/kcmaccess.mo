��    C      4  Y   L      �     �     �     �  
   �     �     �     �  +      *   ,  +   W  #   �     �  J   �                    #  	   /  �   9  H   �           0     Q  �   _  �   	  M   �	     
     
  �   !
    �
  `   �     !     8     E     _     ~  Z   �     �  1   �  !   !  .   C  0   r      �  :   �  #   �     #     1  O   G  
   �     �     �     �  N   �  H     =   \     �     �     �  M   �  X        x  5   �  D   �  D     Q   I  .   �  �  �     s     {  
   �     �     �     �     �  +   �  +     +   <  #   h     �  ?   �     �     �     �     �  	     l     I   z     �  @   �       �   *  {   �  :   1     l     r  �   �  �     ]   �     @     T     a     {     �  Q   �     �  -   �     )  -   H  -   v     �  <   �                /  6   E     |     �  	   �     �  B   �  D   �  5   =     s     �     �  G   �  ;         <  *   S  6   ~  3   �  9   �  '   #        9      B       5   "   +   *      ;                           1         &       =          ?       >   %                        #       .           ,         A            $   2      0   )   4         :   6   /              	      <   3   !   -   (   C       7                  @                
   8   '                    min  msec &Bell &Duration: &Keyboard Filters &Lock sticky keys &Use slow keys &Use system bell whenever a key is accepted &Use system bell whenever a key is pressed &Use system bell whenever a key is rejected (c) 2000, Matthias Hoelzer-Kluepfel Activation Gestures All screen colors will be inverted for the amount of time specified below. AltGraph Audible Bell Author Bounce Keys Browse... Check this option if you want to use a customized bell, playing a sound file. If you do this, you will probably want to turn off the system bell. Click here to choose the color used for the "flash screen" visible bell. Configure &Notifications... EMAIL OF TRANSLATORSYour emails F&lash screen Here you can activate keyboard gestures that turn on the following features: 
Mouse Keys: %1
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can activate keyboard gestures that turn on the following features: 
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can customize the duration of the "visible bell" effect being shown. Hyper I&nvert screen If the option "Use customized bell" is enabled, you can choose a sound file here. Click "Browse..." to choose a sound file using the file dialog. If this option is checked, KDE will show a confirmation dialog whenever a keyboard accessibility feature is turned on or off.
Ensure you know what you are doing if you uncheck it, as the keyboard accessibility settings will then always be applied without confirmation. If this option is checked, the default system bell will be used. Usually, this is just a "beep". KDE Accessibility Tool Locking Keys Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names Notification Please note that you may have to log out once to allow the screen reader to work properly. Press %1 Press %1 while CapsLock and ScrollLock are active Press %1 while CapsLock is active Press %1 while NumLock and CapsLock are active Press %1 while NumLock and ScrollLock are active Press %1 while NumLock is active Press %1 while NumLock, CapsLock and ScrollLock are active Press %1 while ScrollLock is active Screen Reader Screen reader enabled Show a confirmation dialog whenever a keyboard accessibility feature is toggled Slo&w Keys Sound &to play: Sticky Keys Super The screen will turn to a custom color for the amount of time specified below. Turn sticky keys and slow keys off after a certain period of inactivity. Turn sticky keys off when two keys are pressed simultaneously Us&e customized bell Use &sticky keys Use &system bell Use Plasma's notification mechanism for modifier or locking key state changes Use Plasma's notification mechanism whenever a keyboard accessibility feature is toggled Use bou&nce keys Use gestures for activating sticky keys and slow keys Use system bell whenever a locking key gets activated or deactivated Use system bell whenever a modifier gets latched, locked or unlocked Use the system bell whenever a gesture is used to toggle an accessibility feature Use the system bell whenever a key is rejected Project-Id-Version: kcmaccess
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-10 03:17+0100
PO-Revision-Date: 2016-11-03 16:14+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
dot tw>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
  分鐘  毫秒 響鈴(&B) 持續時間(&D)： 鍵盤過濾器(&K) 鎖定相黏按鍵(&L) 使用慢速按鍵(&U) 當按鍵被接受時使用系統鈴聲(&U) 當按鍵被按下時使用系統鈴聲(&U) 當按鍵被拒絕時使用系統鈴聲(&U) (c) 2000, Matthias Hoelzer-Kluepfel 作用手勢 畫面所有的顏色將會在下面指定的時間內反白。 AltGraph 有聲響鈴 作者 彈回按鍵 瀏覽... 勾選此選項可以改為播放一個聲音檔。如果您選擇此選項，可能要關閉系統鈴聲。 按下這邊以選擇 "閃爍螢幕" 視覺響鈴所要使用的顏色。 設定通知(&N)... shyue@sonoma.com.tw,gentamicin5@yahoo.com.tw, s8321414@gmail.com 閃爍螢幕(&L) 您可以在這裡以鍵盤手勢開啟下列功能：
滑鼠鍵：%1
相黏鍵：連續按 Shift 鍵 5 次
放慢鍵：按住 Shift 8 秒 您可以在這裡以鍵盤手勢開啟下列功能：
相黏鍵：連續按 Shift 鍵 5 次
放慢鍵：按住 Shift 8 秒 您可以在這邊自訂 "視覺響鈴" 的持續時間。 Hyper 反白螢幕(&N) 如果選項"使用自訂響鈴"有啟動，您可以在這裡選擇一個聲音檔。點擊"瀏覽..."可用對話框選擇選擇一個聲音檔。 如果核取此選項，當開啟或關閉鍵盤無障礙功能時 KDE 會顯示確認對話盒。
請注意了解您在做什麼，如果您核消它則鍵盤無障礙功能將設為不使用確認。 如果勾選此選項，會使用預設的系統鈴聲。通常只是個「嗶」聲而已。 KDE 無障礙工具 鎖定按鍵 Matthias Hoelzer-Kluepfel 薛景中,王勝弘, Jeff Huang 通知 請注意，您可能需要登出一次，才能讓螢幕閱讀器正確運作。 按 %1 當大寫鎖定和捲軸鎖定作用時按 %1 當大寫鎖定作用時按 %1 當數字鎖定和大寫鎖定作用時按 %1 當數字鎖定和捲軸鎖定作用時按 %1 當數字鎖定作用時按 %1 當數字鎖定、大寫鎖定和捲軸鎖定作用時按 %1 當捲軸鎖定作用時按 %1 螢幕閱讀器 開啟螢幕閱讀器 當切換鍵盤無障礙功能時顯示確認對話盒 慢速按鍵(&W) 要播放的聲音(&T): 相黏鍵 Super 畫面將會在下面指定的時間內轉變成自定的顏色。 在一段時間不使用後關閉相黏鍵(sticky)與放慢(slow)鍵 當同時按下兩個按鍵時關閉相黏(sticky)鍵 使用自訂響鈴(&E) 使用相黏按鍵(&S) 使用系統響鈴(&S) 當輔助鍵或鎖定鍵改變其狀態時使用 Plasma 的通知機制 當切換鍵盤無障礙功能時使用 Plasma 通知機制 使用彈回按鍵(&N) 使用手勢來作用相黏鍵與放慢鍵 當鎖定鍵作用或取消作用時使用系統鈴聲 當輔助鍵被鎖定或解鎖時使用系統鈴聲 當使用手勢切換無障礙功能時使用系統鈴聲 當按鍵被拒絕時使用系統鈴聲 