��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     '  m   F  `   �     	     0	  R   C	     �	     �	     �	     �	     �	  -   �	     
     %
  !   5
     W
     g
     �
     �
     �
  4   �
  D   �
  !   %  G   G                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-11-03 16:27+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>您確定要移除 <i>%1</i> 主題嗎？<br/>這樣會刪除這個主題所安裝的所有檔案。</qt> <qt>您不能刪除目前正在使用的主題。<br />您必須先切換到其他主題。</qt> （可用的大小：%1） 解析度相依性 名為 %1 的主題已存在於您的圖示主題資料夾。您要取代它嗎？ 確認 游標設定已變更 游標主題 描述 拖放或輸入主題的網址 franklin@goodhorse.idv.tw, s8321414@gmail.com Fredrik Höglund 取得新主題 從網際網路上取得新主題 從檔案安裝 Franklin Weng, Jeff Huang 名稱 要覆寫主題嗎？ 移除主題 檔案 %1 似乎不是合法的游標主題歸檔。 無法下載此游標主題集。
請檢查位址 %1 是否正確。 無法找到游標主題集 %1。 您必須重新啟動 Plasma 作業階段才能讓這些改變生效。 