��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (     �     �  	   �     �  "        )     0     @     M     T     [  	   k     u     �     �     �  $   �     �     �  	   �  -   �  	        $     1     >  	   T  	   ^     h     {  	     -   �  	   �  	   �  
   �  
   �  
   �     �     �  	                  .     ;     B     U     o  	   s     }  	   �  	   �  	   �     �     �     �     �     �  	   �  	   �                               0     C     Y     r          �     �     �     �     �     �  L   �     $  	   4  	   >     H  	   X     b  
   f     q     x     |     �     �     �     �     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-11-03 16:18+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 
實體裝置檢視器模組 (c) 2010 David Hubner AMD 3DNow ATI IVEC 剩餘 %1，共 %2 (%3% 已使用) 電池 電池型態： 匯流排： 相機 相機 充電狀態： 充電中 全部折疊 CF 卡讀取器 裝置 裝置資訊 顯示所有目前列出的裝置。 裝置檢視器 裝置 放電中 franklin@goodhorse.idv.tw, s8321414@gmail.com 已加密 全部展開 檔案系統 檔案系統型態： 已充飽 硬碟機 可熱插拔嗎？ IDE IEEE 1394 顯示關於目前選取的裝置的資訊。 Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 鍵盤 鍵盤+滑鼠 標籤： 最大速率： 記憶卡讀取器 掛載於： 滑鼠 多媒體播放器 Franklin Weng, Jeff Huang 否 未充電 沒有可用的資料 未掛載 未設定 光碟機 PDA 磁碟分割表 主要 處理器 %1 處理器編號： 處理器 產品： Raid 可移除嗎？ SATA SCSI SD/MMC 讀取器 顯示所有裝置 顯示相關的裝置 智慧型媒體讀取器 儲存裝置 支援的驅動程式： 支援的指令集： 支援的協定： UDI:  UPS USB UUID： 顯示目前裝置的 UDI（Unique Device Identifier，唯一裝置識別） 未知的裝置 未使用 廠商： 容量大小： 用量： 是 kcmdevinfo 未知 無 無 平台 未知 未知 未知 未知 未知 xD 讀取器 