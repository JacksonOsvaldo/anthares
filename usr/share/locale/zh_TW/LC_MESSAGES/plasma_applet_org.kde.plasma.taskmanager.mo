��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b       
   !     ,  
   :     E  
   V  
   a  
   l     w     �     �     �     �     �     �            	     	   '     1     A  $   W     |  	   �  	   �     �     �     �  	   �     �     �     �          1     J     N     \  !   c     �     �     �     �     �     �     �                    *     1     D     Z  $   p  !   �     �     �  	   �  	   �     �     �     �     �  !     !   3  !   U     w  -   �     �     �  	   �     �     �     �       
   
          +     <     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-03-27 09:34+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 所有的桌面(&A) 關閉(&C) 全螢幕(&F) 移動(&M) 新增桌面(&N) 釘選(&P) 隱藏(&S) %1 %2(&%1) 也可在 %1 上使用 新增到目前活動 所有的活動 允許此程式被歸類 依字母順序 總是以欄安排事件 總是以列安排事件 安排 行為 依活動 依桌面 依程式名稱 關閉視窗或群組 使用滑鼠滾輪循環切換工作 不要分組 不排序 過濾器 忘掉最近的文件 一般 群組與排序 群組： 突顯視窗 圖示大小： 維持在其它項目之上(&A) 維持在其它項目之下(&B) 讓啟動器保持獨立 大 最大化(&X) 手動 標記播放音訊的應用程式 最大欄數： 最大列數： 最小化(&N) 最小化/復原視窗 更多動作 移到目前桌面(&T) 移動到活動(&A) 移動到桌面(&D) 靜音 新的實體 於 %1 在所有活動上 在目前的活動上 點擊滑鼠中鍵： 只在工作管理員已滿時分組 在彈出式視窗中開啟群組 回復 暫停 下一軌 前一軌 離開 調整大小(&S) 取消釘選 %1 個更多位置 只顯示目前活動裡的工作 只顯示目前桌面上的工作 只顯示目前螢幕上的工作 只顯示縮到最小的工作 在工具按鍵裡顯示進度與狀態資訊 顯示工具提示 小 排序： 開啟新的實體 播放 停止 無 釘選(&P) 分組／取消分組 於 %1 上使用 所有活動都可使用 