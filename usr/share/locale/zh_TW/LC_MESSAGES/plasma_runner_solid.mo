��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     a     n      {  B   �  <   �  <     <   Y  K   �  K   �     .	     ;	     B	     I	     P	     W	     d	     k	     ~	                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-09 08:30+0800
Last-Translator: Franklin Weng <franklin@mail.everfocus.com.tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 鎖定容器 退出媒體 尋找名稱符合 :q: 的裝置 列出所有裝置，並允許它們被掛載、卸載或退出。 列出所有可退出的裝置，並允許它們被退出。 列出所有可掛載的裝置，並允許它們被掛載。 列出所有可卸載的裝置，並允許它們被卸載。 列出所有可以被鎖定的已加密裝置，並允許它們被鎖定。 列出所有可以被解鎖的已加密裝置，並允許它們被解鎖。 掛載裝置 裝置 退出 鎖定 掛載 解除鎖定 卸載 解除鎖定容器 卸載裝置 