��    3      �  G   L      h     i     o     �     �     �     �  !   �  '   �          -  -   ?     m     r     �     �     �     �     �     �     �      �       	             .     ;  
   I     T     s     �  A   �     �     �     �                     ,     :  3   A     u  �   �     *     0  ;   6     r     �     �  %   �  b   �  �  %	     �
     �
     �
     �
               1     D     Z     p  '   }     �     �     �     �     �     �     �     �  	     -     	   9  	   C     M     Z     k  	   �     �     �     �  B   �       	        %     2     >     N     [  	   h  -   r     �  �   �     E     N  6   W     �     �     �  !   �  4   �             *      &      1           .   #                 (      -      3       +                                       '   0   !           2             "          %                         ,   )                    /                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2016-11-03 16:05+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 Hz 關閉所有的輸出(&D) 重新設定(&R) (c), 2012-2013 Daniel Vrátil 順時針轉 90°  逆時針轉 90° 關閉所有輸出 無法支援的設定 作用中的設定檔 進階設定 您確定要關閉所有的輸出嗎？ 自動 中斷識別輸出 按鈕 CheckBox ComboBox 設定顯示 Daniel Vrátil 顯示設定 顯示： franklin@goodhorse.idv.tw, s8321414@gmail.com 已開啟 群組盒 識別輸出 KCM 測試程式 筆記型電腦螢幕 維護者 Franklin Weng, Jeff Huang 沒有主要輸出 沒有可用的解析度 找不到 kscreen 後端介面。請檢查您的 kscreen 安裝。 一般 方向： 主顯示： RadioButton 刷新頻率： 解析度： 調整顯示 比例： 變更的調整會在重新啟動後才生效 螢幕縮放 抱歉，您的設定無法套用。

通常的原因會是整體的螢幕大小過大，或是您開啟了過多的螢幕，您的 GPU 無法支援。 分頁 1 分頁 2 提示：拖曳顯示時按住 Ctrl 可以關閉閃爍 識別輸出 識別輸出 上下相反 警告：沒有可用的輸出！ 您的系統只支援最多 %1 個作用中的螢幕 