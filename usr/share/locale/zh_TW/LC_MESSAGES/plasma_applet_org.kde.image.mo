��          �   %   �      @     A     J     _     h  0   v     �     �     �     �     �     �        
        "     7     D     _     p     �     �     �     �     �     �  �  �     �     �     �     �     �     �     �       	                  4     J     W  	   j     t     �     �     �     �     �     �     �     �                                                       	                                                            
           %1 by %2 Add Custom Wallpaper Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-07-03 20:25+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 %1，由：%2 新增自訂桌布 置中 變更圖片頻率： 桌布輪播之圖片目錄 下載新桌布 取得新桌布... 小時 影像檔 分鐘 下一個桌布影像 開啟存放資料夾 開啟影像 開啟桌布影像 位置： 推薦的桌布檔 移除桌布 回復桌布 調整大小 調整大小並切割 調整大小，並保持比例 秒 設定背景顏色 鋪排 