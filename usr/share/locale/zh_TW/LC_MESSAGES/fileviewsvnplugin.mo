��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     !  #   (      L  &   m     �     �     �  #   �      	  &   '	  #   N	      r	  &   �	  E   �	      
     
     6
  
   N
     Y
  
   g
  
   r
  
   }
     �
     �
  	   �
  
   �
     �
                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2016-11-15 21:25+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 提交 已新增檔案到 SVN 主目錄。 新增檔案到 SVN 主目錄... 新增檔案到 SVN 主目錄失敗。 提交 SVN 的變更失敗 已提交 SVN 的變更。 提交 SVN 的變更... 已從 SVN 主目錄移除檔案。 從 SVN 主目錄移除檔案... 從 SVN 主目錄移除檔案失敗。 已從 SVN 主目錄回復檔案。 從 SVN 主目錄回復檔案... 從 SVN 主目錄回復檔案失敗。 SVN 狀態更新失敗。正在關閉「顯示 SVN 更新」選項。 更新 SVN 主目錄失敗 已更新 SVN 主目錄。 更新 SVN 主目錄... SVN 新增 SVN 提交... SVN 刪除 SVN 回復 SVN 更新 顯示本地端的 SVN 變更 顯示 SVN 更新 描述： SVN 提交 顯示更新 