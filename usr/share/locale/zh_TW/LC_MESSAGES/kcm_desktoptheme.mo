��          |      �             !     9      E     f  
   �     �  &   �     �     �     �  1     �  E     �       -        A     [     h  $   u     �     �     �  -   �                             	      
                        Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2016-11-03 16:04+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=1; plural=0;
 設定桌面主題 David Rosca franklin@goodhorse.idv.tw, s8321414@gmail.com Franklin Weng, Jeff Huang 開啟主題 移除主題 主題檔 (*.zip *.tar.gz *.tar.bz2) 主題安裝失敗。 主題已成功安裝。 主題移除失敗。 這個模組讓您可以設定桌面主題。 