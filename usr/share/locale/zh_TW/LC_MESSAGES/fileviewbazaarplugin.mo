��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  )   �	  &   �	  )   �	     
     !
     ?
     Z
  &   u
  #   �
  #   �
  &   �
  #     #   /  &   S  )   z  )   �     �     �     �     
     (      F     g     �     �     �     �     �     �     �           +                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-09-16 10:50+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 已新增檔案到 Bazaar 主目錄中。 新增檔案到 Bazaar 主目錄中... 新增檔案到 Bazaar 主目錄失敗。 Bazaar 紀錄已關閉。 提交 Bazaar 變更失敗。 已提交 Bazaar 變更。 提交 Bazaar 變更中... 下載合併 Bazaar 主目錄失敗。 已下載合併 Bazaar 主目錄。 下載合併 Bazaar 主目錄中... 上傳合併 Bazaar 主目錄失敗。 已上傳合併 Bazaar 主目錄。 上傳合併 Bazaar 主目錄中... 已從 Bazaar 主目錄移除檔案。 正在從 Bazaar 主目錄移除檔案... 從 Bazaar 主目錄移除檔案失敗。 檢視變更失敗。 已檢視變更。 檢視變更中... 取得 Bazaar 紀錄失敗。 正在取得 Bazaar 紀錄... 更新 Bazaar 主目錄失敗。 已更新 Bazaar 主目錄。 更新 Bazaar 主目錄中... Bazaar 新增... Bazaar 提交... Bazaar 刪除 Bazaar 紀錄 Bazaar 下載合併（Pull） Bazaar 上傳合併（Push） Bazaar 更新 顯示本地端的 Bazaar 變更 