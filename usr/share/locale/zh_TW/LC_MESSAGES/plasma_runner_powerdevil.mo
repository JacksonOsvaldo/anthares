��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  m   �  Z   M     �     �     �     �     �     �     �     �                3     L     h     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-01-13 08:03+0800
Last-Translator: Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=1; plural=0;
 將螢幕變暗一半 將螢幕整個變暗 列出螢幕亮度選項，或設定為 :q: 所定義的亮度，例如螢幕亮度 50 會將螢幕變暗 50% 列出系統暫停（例如睡眠模式或冬眠模式）選項，並允許它們被啟動 將螢幕變暗 冬眠 螢幕亮度 睡眠 暫停 儲存到磁碟 儲存到記憶體 將螢幕變暗 %1 螢幕亮度 %1 設定亮度為 %1 暫停並儲存到磁碟 暫停並儲存到記憶體 暫停並儲存到記憶體 暫停並儲存到磁碟 