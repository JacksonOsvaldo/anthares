��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  *   �  ?   �     -     F     M     c  	   j     t     �     �     �     �     �     �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-06-09 17:31+0800
Last-Translator: Franklin
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> 關閉 自動複製： 不要顯示此對話框，自動複製。 將文字或影像拖曳到這裡以便上傳到線上服務。 上傳時發生錯誤。 一般 歷史紀錄大小： 貼上 請稍候 請再試一次。 傳送中... 分享 %1 的分享 已成功上傳 此網址已被分享 上傳 %1 到線上服務 