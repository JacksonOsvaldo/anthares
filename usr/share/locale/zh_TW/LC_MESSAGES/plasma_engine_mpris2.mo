��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  1   <     n     ~     �     �     �  @   �  -        4     J                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-20 22:23+0800
Last-Translator: Franklin Weng <franklin@goodhorse.idv.tw>
Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=1; plural=0;
 試著執行動作 %1 時失敗，訊息為 %2。 播放下一首 播放前一首 媒體控制器 播放/暫停媒體 停止媒體播放 遺失了動作 %2 的參數 %1，或是參數型態不正確。 媒體播放器 '%1' 無法執行動作 %2。 未知的操作 %1。 未知的錯誤。 