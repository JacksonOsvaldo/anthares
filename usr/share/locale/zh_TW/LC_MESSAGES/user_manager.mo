��    0      �  C         (     )     H     c     t     �     �     �     �  �   �  r   G  }   �  �   8  �   �  �   b  �   )  g   �  h   0	  Y   �	  Y   �	  ]   M
     �
     �
  
   �
     �
  
   �
     �
  	   �
     �
  
          ]   *     �  I   �     �  %   �      $     E     `     v     �  H   �     �  -        <     B  *   J     u  �  �     "     5     B     X     n     {     �     �     �  3   �  3   �  "   3     V  1   s  "   �  6   �  *   �  !   *  $   L  '   q     �     �     �     �     �     �  	   �     �     �               4  ?   P     �  $   �     �     �     �          #  9   6     p  	   �  	   �  	   �     �     �     "                                                     .              -   	                 (   '   )      *              &                   
         $                 !      0   ,                             %   /             #   +    @title:windowChange your Face @title:windowChoose Image Add user account Choose from Gallery... Clear Avatar Delete User Delete files Email Address: Error returned when the password is invalidThe password should be at least %1 character The password should be at least %1 characters Error returned when the password is invalidThe password should be more varied in letters, numbers and punctuation Error returned when the password is invalidThe password should contain a mixture of letters, numbers, spaces and punctuation Error returned when the password is invalidThe password should contain at least %1 lowercase letter The password should contain at least %1 lowercase letters Error returned when the password is invalidThe password should contain at least %1 number The password should contain at least %1 numbers Error returned when the password is invalidThe password should contain at least %1 special character (like punctuation) The password should contain at least %1 special characters (like punctuation) Error returned when the password is invalidThe password should contain at least %1 uppercase letter The password should contain at least %1 uppercase letters Error returned when the password is invalidThe password should not contain sequences like 1234 or abcd Error returned when the password is invalidThe password should not contain too many repeated characters Error returned when the password is invalidThis password can't be used, it is too simple Error returned when the password is invalidYour name should not be part of your password Error returned when the password is invalidYour username should not be part of your password John John Doe Keep files Load from file... MyPassword New User Password: Passwords do not match Real Name: Remove user account Returned when a more specific error message has not been foundPlease choose another password Select a new face: The username can contain only letters, numbers, score, underscore and dot The username is too long The username must start with a letter This e-mail address is incorrect This password is excellent This password is good This password is very good This password is weak This user is using the system right now, removing it will cause problems This username is already used Title for change password dialogNew Password Users Verify: What do you want to do after deleting %1 ? john.doe@example.com Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:21+0100
PO-Revision-Date: 2016-09-21 21:39+0800
Last-Translator: Jeff Huang <s8321414@gmail.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 改變您的頭像 選擇影像 新增使用者帳號 從清單中選取... 清除頭像 刪除使用者 刪除檔案 電子郵件地址： 密碼至少要 %1 個字元 密碼應該要在字母、數字與標點間變化 密碼必須混合字母、數字、空白與標點 密碼至少要 %1 個小寫字元 密碼至少要 %1 個數字 密碼至少要 %1 個特殊字元（如標點） 密碼至少要 %1 個大寫字元 密碼不應包含像是 1234 或 abcd 這樣的序列 密碼不應該包含太多重複的字元 不能用這密碼。太弱了啦 密碼中不能有部份您的姓名 密碼中不能有部份使用者名稱 John John Doe 保存檔案 從檔案載入... 我的密碼 新使用者 密碼： 密碼不符 真實姓名： 移除使用者帳號 請選擇其他密碼 選擇一個新的圖像： 使用者名稱只能包含字母、數字、線、底線與點 使用者名稱太長 使用者名稱必須以字母開頭 電子郵件地址不正確 密碼強度很給力 密碼強度不錯 密碼強度極好 密碼強度太弱 此使用者正在使用系統。移除它會造成問題 這個名稱已經被使用 新密碼 使用者 驗證： 刪除 %1 後要做什麼？ john.doe@example.com 