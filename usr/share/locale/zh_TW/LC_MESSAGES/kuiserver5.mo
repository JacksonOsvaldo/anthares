��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �             0   4     e      v     �     �     �     �  	   �     �  4   �  !     $   '     L     S     c     v     }  !   �     �  !   �     �                                 
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-30 11:00+0800
Last-Translator: Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
Language-Team: Chinese Traditional <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.0
 %1 個檔案 %1 個資料夾 已處理 %1 個（共 %2 個） 已處理 %1 個（共 %2 個），速度 %3/秒 已處理 %1 個 已處理 %1 個，速度 %2/秒 外觀 行為 取消 清除 設定... 已完成的工作 執行中的檔案傳輸/工作清單（kuiserver） 將它們移到不同的清單上 將它們移到不同的清單上。 暫停 將它們移除 將它們移除。 恢復 顯示清單中的所有工作 顯示清單中的所有工作。 以樹狀圖顯示所有工作 以樹狀圖顯示所有工作。 以不同的視窗顯示 在不同的視窗顯示。 