��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �  
   9  +   D  .   p  >   �  +   �  #   
	  &   .	  +   U	  .   �	  @   �	  <   �	  ?   .
  O   n
  f   �
  6   %  "   \           �     �     �     �     �     �          <     J     [                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-12-03 03:45+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Einspielen Dateien wurden zum SVN-Archiv hinzugefügt. Dateien werden zum SVN-Archiv hinzugefügt ... Das Hinzufügen von Dateien zum SVN-Archiv ist fehlgeschlagen. Fehler beim Einspielen der SVN-Änderungen. SVN-Änderungen wurden eingespielt. SVN-Änderungen werden eingespielt ... Dateien wurden aus dem SVN-Archiv entfernt. Dateien werden aus dem SVN-Archiv entfernt ... Das Entfernen von Dateien aus dem SVN-Archiv ist fehlgeschlagen. Dateien wurden auf den Stand des SVN-Archivs zurückgesetzt. Dateien werden auf den Stand des SVN-Archivs zurückgesetzt ... Das Zurücksetzen von Dateien auf den Stand des SVN-Archivs ist fehlgeschlagen. Die SVN-Statusaktualisierung ist fehlgeschlagen. „SVN-Aktualisierungen anzeigen“ wird deaktiviert. Die Aktualisierung des SVN-Archivs ist fehlgeschlagen. Das SVN-Archiv wurde aktualisiert. SVN-Archiv wird aktualisiert ... Hinzufügen (SVN) Einspielen (SVN) ... Löschen (SVN) Zurücksetzen (SVN) Aktualisieren (SVN) Lokale SVN-Änderungen anzeigen SVN-Aktualisierungen anzeigen Beschreibung: Einspielen (SVN) Aktualisierungen anzeigen 