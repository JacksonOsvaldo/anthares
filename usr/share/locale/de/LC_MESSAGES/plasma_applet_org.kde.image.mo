��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s  	     /        J     a     v     �  	   �     �  B   �     �  (        1     9     E     M      g     �     �     �     �     �      �            #   6     Z     c  	     	   �                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-12-04 17:35+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 von %2 Benutzerdefiniertes Hintergrundbild hinzufügen Ordner hinzufügen ... Bild hinzufügen ... Hintergrund: Unscharf Zentriert Wechseln alle: Ordner mit dem Hintergrundbild, von aus eine Diaschau gezeigt wird Hintergrundbilder herunterladen Neue Hintergrundbilder herunterladen ... Stunden Bilddateien Minuten Nächstes Hintergrundbild Ordner mit diesem Inhalt öffnen Bild öffnen Hintergrundbild öffnen Positionierung: Empfohlenes Hintergrundbild Hintergrundbild entfernen Hintergrundbild wiederherstellen Skaliert Skaliert und beschnitten Skaliert (Proportionen beibehalten) Sekunden Hintergrundfarbe auswählen Einfarbig Gekachelt 