��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  3   �  =   '  >   e  8   �  0   �  1         @     a  5   t         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-08 10:41+0200
Last-Translator: Johannes Obermayr <johannesobermayr@gmx.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Der MIME-Typ der Datei kann nicht ermittelt werden. Es können nicht alle benötigten Funktionen gefunden werden. Der Anbieter kann beim angegebenen Ziel nicht gefunden werden. Beim Ausführen des Skriptes ist ein Fehler aufgetreten. Der Pfad zum abgefragten Anbieter ist ungültig. Die ausgewählte Datei kann nicht gelesen werden. Der Dienst ist nicht verfügbar. Unbekannter Fehler Für diesen Dienst muss eine Adresse angegeben werden 