��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]     �       #   ,     P     m     �  [   �  (   �  7        T  �   c     �  J   �     B                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-12-04 17:24+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Auch Dateiinhalt indizieren Dateisuche einrichten Copyright 2007-2010 Sebastian Trüg Nicht in diesen Orten suchen lueck@hube-lueck.de Dateisuche aktivieren Die Dateisuche hilft Ihnen, alle Ihre Dateien auf der Grundlage des Inhalts zu lokalisieren Der Ordner %1 ist bereits ausgeschlossen Der übergeordnete Ordner %1 ist bereits ausgeschlossen Burkhard Lück Es ist nicht möglich, den Basisordner auszuschließen. Bitte deaktivieren Sie die Dateisuche, wenn Sie sie nicht benutzen möchten Sebastian Trüg Wählen Sie den Ordner, der von der Indizierung ausgeschlossen werden soll Vishesh Handa 