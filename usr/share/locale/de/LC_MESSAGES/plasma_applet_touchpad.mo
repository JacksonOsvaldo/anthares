��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �  
   5     @     T  K   h     �     �     �     �        	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-14 06:04+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Abschalten Touchpad abschalten Touchpad aktivieren Es wurde keine Maus erkannt.
Möchten Sie das Touchpad wirklich abschalten? Es wurde kein Touchpad gefunden Touchpad Das Touchpad ist deaktiviert Das Touchpad ist eingeschaltet 