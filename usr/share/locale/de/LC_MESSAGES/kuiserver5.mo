��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �          !     0     H  	   Y  	   c     m     t     �  B   �  &   �  '        -     6     F  
   W  %   b  &   �  +   �  ,   �          "                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-28 17:25+0200
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 Datei %1 Dateien %1 Ordner %1 Ordner %1 von %2 verarbeitet %1 von %2 verarbeitet bei %3/s %1 verarbeitet %1 verarbeitet bei %2/s Erscheinungsbild Verhalten Abbrechen Leeren Einrichten ... Abgeschlossene Aufgaben Auflistung der laufenden Dateiübertragungen/-aufgaben (kuiserver) Diese in eine andere Liste verschieben Diese in eine andere Liste verschieben. Anhalten Diese entfernen Diese entfernen. Fortsetzen Alle Aufgaben in einer Liste anzeigen Alle Aufgaben in einer Liste anzeigen. Alle Aufgaben in einer Baumansicht anzeigen Alle Aufgaben in einer Baumansicht anzeigen. Einzelne Fenster anzeigen Einzelne Fenster anzeigen. 