��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     5     9     S     p     �     �     �     �     �  "   �  ;     2   X  I   �  I   �  3   	  +   S	     	      �	  )   �	  )   �	     
  1   *
     \
  	   c
     m
  	   {
     �
  $   �
     �
     �
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-12-04 17:13+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms Anzeige der &Kurzbefehle: Knopf&typ für oberen Pfeil: Kurzbefehle immer ausblenden Kurzbefehle immer anzeigen Anima&tionsdauer: Animationen Knopft&yp für unteren Pfeil: Breeze-Einstellungen Reiter auf Karteikarten zentrieren Fenster sind über alle ihre leeren Flächen zu verschieben Fenster sind nur über ihre Titelleiste zu bewegen Fenster sind über ihre Titel-, Menü- und Werkzeugleisten zu verschieben Dünne Linie zeichnen, um den Fokus in Menüs und Menüleisten anzuzeigen Fokus von Bedienelementen in Listen sichtbar machen Rahmen um andockbare Seitenleisten zeichnen Rahmen um Seitentitel zeichnen Rahmen um Seitenleisten zeichnen Skalenstriche auf Schiebereglern anzeigen Symbol-Trenner in Werkzeugleiste anzeigen Animationen aktivieren Erweitere Griffe zur Größenänderung aktivieren Rahmen Allgemein Keine Knöpfe Ein Knopf Bildlaufleisten Kurzbefehle anzeigen, wenn benötigt Zwei Knöpfe Versch&ieben von Fenstern: 