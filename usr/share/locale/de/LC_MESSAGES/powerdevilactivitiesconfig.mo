��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �     �  (        9     K     W     j  B   �     �     �  /         0     ?  �   M  �   	  �   �	     5
  F   E
     �
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-05 13:54+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  min Verhalten wie Immer Besonderes Verhalten definieren Keine besonderen Einstellungen verwenden schwarzer@kde.org Ruhezustand Frederik Schwarzer Den Bildschirm nie ausschalten Den Rechner nie in den Standby-Modus versetzen oder herunterfahren Rechner läuft mit Netzteil Rechner läuft mit Akku Rechner läuft mit Akku auf niedrigem Ladestand Herunterfahren Standby-Modus Der Energieverwaltungsdienst scheint nicht zu laufen.
Das können Sie ändern, indem Sie ihn manuell starten oder unter „Starten und Beenden“ den automatischen Start einrichten. Der Aktivitäten-Dienst läuft nicht.
Es ist erforderlich, dass er Aktivitäten-Dienst läuft, um Aktivitäten-spezifisches Verhalten der Energieverwaltung einzurichten. Der Aktivitäten-Dienst läuft mit minimaler Funktionalität.
Namen und Symbole der Aktivitäten sind möglicherweise nicht verfügbar. Aktivität "%1" Unterschiedliche Einstellungen verwenden (nur für erfahrene Benutzer) nach 