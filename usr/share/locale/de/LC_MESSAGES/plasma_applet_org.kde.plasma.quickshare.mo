��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �  
   �     �  @   �  P        m  	   �     �  	   �     �     �     �  	   �     �     	  *   !  #   L     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-27 22:47+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Schließen Automatisch kopieren Diesen Dialog nicht mehr anzeigen, sondern automatisch kopieren. Legen Sie hier Text oder ein Bild ab, um es auf einen Online-Dienst hochzuladen. Fehler beim Hochladen. Allgemein Größe des Verlaufs: Einfügen Bitte warten Bitte versuchen Sie es erneut. Wird gesendet ... Freigeben Freigaben für %1: Erfolgreich hochgeladen Die Adresse (URL) wurde gerade freigegeben %1 zu einem Online-Dienst hochladen 