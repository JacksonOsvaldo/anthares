��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �     �     �  #   �     "     :     B     J  -   h     �     �     �     �     �     �     �                                                            
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-05-29 21:21+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Ändert den Barcode-Typ Verlauf leeren Inhalt der Zwischenablage Verlauf der Zwischenablage ist leer Zwischenablage ist leer Code 39 Code 93 Zwischenablage einrichten ... Das Erstellen des Barcodes ist fehlgeschlagen Datenmatrix Inhalte bearbeiten +%1 Aktion aufrufen QR-Code Aus Verlauf löschen Zurück zur Zwischenablage Suchen Strichcode anzeigen 