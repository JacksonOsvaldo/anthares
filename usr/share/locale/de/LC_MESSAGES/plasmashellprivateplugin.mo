��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .     �     �     �     �           8     ?     ]  /   d     �  2   �     �     �     �          
     !     1     G     b     ~  :   �  G   �     	     $	     )	  3   0	     d	  	   t	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-09-10 14:38+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Ausführen Alle Miniprogramme Kategorien: Arbeitsumgebungs-Skriptkonsole Neue Miniprogramme herunterladen Editor Skript wird ausgeführt an %1 Filter Miniprogramm aus lokaler Datei installieren ... Installationsfehler Die Installation des Pakets %1 ist fehlgeschlagen. Laden Neue Sitzung Skriptdatei öffnen Ausgabe Laufende Miniprogramme Laufzeit: %1 ms Skriptdatei speichern Bildschirmsperre aktiviert Bildschirmschoner-Startzeit Plasmoid-Datei auswählen Legt die Zeit fest, nach der der Bildschirm gesperrt wird. Legt fest, ob der Bildschirm nach einer angegebenen Zeit gesperrt wird. Vorlagen KWin Plasma Die Skriptdatei <b>%1</b> kann nicht geladen werden Deinstallierbar Verwenden 