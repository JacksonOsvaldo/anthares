��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �  	   �     �     �  	   �     �            $   *     O  %   a     �  :   �  :   �  	   
  	             .     B     T  6   s     �     �     �  &   �  	   �               )     =     L     P     f     |     �     �     �  (   �     �     �       /        D  (   U     ~     �     �     �     �     �       (   %  '   N  #   v     �     �     �     �     �     �     �     �      �  -     1   J  .   |     �  J   �          )     /     ;  
   P     [     d  	   j     t     �      �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-11 22:06+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Alle Arbeitsflächen &Schließen V&ollbild &Verschieben &Neue Arbeitsfläche &Anheften &Fensterheber &%1 %2 Auch verfügbar in %1 Zur aktuellen Aktivität hinzufügen Alle Aktivitäten Gruppierung dieser Anwendung zulassen Alphabetisch Fenster immer in Spalten mit dieser Anzahl Zeilen anordnen Fenster immer in Zeilen mit dieser Anzahl Spalten anordnen Anordnung Verhalten Nach Aktivität Nach Arbeitsfläche Nach Programmname Fenster oder Gruppe schließen Zwischen laufenden Programmen mit dem Mausrad wechseln Nicht gruppieren Nicht sortieren Filter Zuletzt geöffnete Dokumente vergessen Allgemein Gruppieren und Sortieren Gruppierung: Fenster hervorheben Symbolgröße: — Immer im &Vordergrund Immer im &Hintergrund Starter getrennt halten Groß Ma&ximieren Manuell Anwendung mit Audio-Wiedergabe markieren Maximale Anzahl Spalten: Maximale Anzahl Zeilen: Mi&nimieren Fenster oder Gruppe minimieren/wiederherstellen Weitere Aktionen Auf a&ktuelle Arbeitsfläche verschieben Zur &Aktivität verschieben Auf Arbeitsfläche &verschieben Stummschalten Neue Instanz Auf Arbeitsfläche „%1“ Auf allen Aktivitäten Auf der aktuellen Aktivität Bei Klicken mit der mittleren Maustaste: Gruppieren nur bei voller Fensterleiste Gruppen in Aufklappfenstern öffnen Wiederherstellen 9.999+ Pause Nächstes Stück Vorheriges Stück Beenden &Größe ändern Lösen Ein weiterer Ort %1 weitere Orte Nur Fenster der aktuellen Aktivität anzeigen Nur Fenster der aktuellen Arbeitsfläche anzeigen Nur Fenster des aktuellen Bildschirms anzeigen Nur minimierte Fenster anzeigen Informationen zum Fortschritt und Status im Kontrollleistensymbol anzeigen Kurzinfos anzeigen Klein Sortierung: Neue Instanz starten Wiedergabe Anhalten Keine &Anheften Gruppieren/Gruppierung aufheben Verfügbar in %1 In allen Aktivitäten verfügbar 