��    1      �  C   ,      8     9     =     B  .   Q  5   �     �     �     �     �  	   �     �               &  1   :     l     r          �  
   �     �  '   �     �     �     �            '        @     O     V     ^  5   g     �     �     �     �     �  $   �  A   �  �   @     &     -  C   >  '   �     �     �     �  �  �     �
     �
     �
     �
     �
  	   �
     �
  
   �
     �
  
   	          *     F     W  .   l  	   �     �     �     �     �  $   �  	        %     (      1     R  
   d  -   o     �  	   �     �     �  9   �          1     ?     H     Y  
   k     v     �  
   �  "   �     �     �     �     �  0                  #   /       -                      $                +         (   1          .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-12-06 15:45+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Zurück Akku bei %1 % Belegung wechseln Virtuelle Tastatur Abbrechen Feststelltaste ist aktiviert Schließen Suche schließen Einrichten Suchmodule einrichten Arbeitsflächen-Sitzung: %1 Anderer Benutzer Tastaturbelegung: %1 Abmelden in 1 Sekunde Abmeldung in %1 Sekunden Anmeldung Anmeldung fehlgeschlagen Als anderer Benutzer anmelden Abmelden Nächstes Stück Es werden keine Medien wiedergegeben Unbenutzt OK Passwort Medien wiedergeben oder anhalten Vorheriges Stück Neustarten Neustart in 1 Sekunde Neustart in %1 Sekunden Zuletzt durchgeführte Abfragen Entfernen Neu starten Herunterfahren Herunterfahren in 1 Sekunde Herunterfahren in %1 Sekunden Neue Sitzung starten Standby-Modus Wechseln Sitzung wechseln Benutzer wechseln Suchen ... „%1“ durchsuchen ... Plasma von KDE Entsperren Aufheben der Sperre fehlgeschlagen Auf TTY %1 (Bildschirm %2) TTY %1 Benutzername %1 (%2) In der Kategorie zuletzt durchgeführte Abfragen 