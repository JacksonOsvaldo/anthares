��          <      \       p      q   *   �   /   �   �  �   +   �  N   �  T                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2010-05-23 21:18+0200
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 „%1“ kann mit %2 nicht gefunden werden. Bei der Verbindung mit dem Wetterserver %1 wurde ein Zeitlimit überschritten. Das Abholen der Wetterinformationen für %1 hat eine Zeitüberschreitung ausgelöst. 