��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     r     �  B   �     �     �  	   �  
   �     �     �  	   �     	  
        '     :     ?  2   P     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-22 20:52+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Auswählen ... &Suchen: *.png *.xpm *.svg *.svgz|Symbol-Dateien (*.png *.xpm *.svg *.svgz) Aktionen Alle Programme Kategorien Geräte Logos Emoticons Quelle für Symbol MIME-Typen S&onstige Symbole: Orte &System-Symbole: Interaktiv nach Symbolnamen suchen (z. B. Ordner). Symbol auswählen Status 