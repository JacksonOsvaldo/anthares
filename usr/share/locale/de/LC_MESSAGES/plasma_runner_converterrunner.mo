��          4      L       `   �   a   L   �   �  E  �     	   �                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: plasma_runner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-12-01 19:29+0100
Last-Translator: Panagiotis Papadopoulos <pano_90@gmx.net>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Den Wert von :q: umrechnen, wenn :q: als „Wert Einheit [>, in, als] Einheit“. Verwenden Sie das Miniprogramm „Einheiten-Umrechner“, um alle verfügbaren Einheiten zu sehen. in;als;zu 