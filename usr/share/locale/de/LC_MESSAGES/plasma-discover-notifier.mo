��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  �         7   %  N   ]  7   �  +   �  A     5   R  $   �     �            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2015-11-06 02:49+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1, %2 1 Paket wird aktualisiert %1 Pakete werden aktualisiert 1 Sicherheitsrelevante Aktualisierung %1 Sicherheitsrelevante Aktualisierungen 1 Paket wird aktualisiert %1 Pakete werden aktualisiert Keine Pakete für Aktualisierung verfügbar 1 davon ist sicherheitskritisch %1 davon sind sicherheitskritisch Sicherheitskritische Aktualisierungen sind verfügbar Das System ist auf dem neusten Stand Verfügbare Aktualisierungen 