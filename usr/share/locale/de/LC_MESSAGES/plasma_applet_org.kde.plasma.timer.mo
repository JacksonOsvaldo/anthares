��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {  	   -     7     G     V  	   _     i     z     �     �     �  <   �     �  	   �          "     4     C     I     Q     a     t  f   {                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-01-23 09:47+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 läuft %1 läuft nicht &Zurücksetzen &Starten Erweitert Erscheinungsbild Befehl: Anzeige Befehl ausführen Benachrichtigungen Verbleibende Zeit: %1 Sekunde Verbleibende Zeit: %1 Sekunden Befehl ausführen An&halten Benachrichtigung anzeigen Sekunden anzeigen Titel anzeigen Text: Eieruhr Zeit abgelaufen Die Eieruhr läuft Titel: Sie können mit dem Mausrad die Ziffern ändern oder vordefinierte Zeiten aus dem Kontextmenü wählen 