��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  0   �     �     �     �       	   ,     6     I     Z     q     �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-19 15:38+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Farbe automatisch in die Zwischenablage kopieren Verlauf leeren Farbeinstellungen In die Zwischenablage kopieren Standard-Farbformat: Allgemein Farbdialog öffnen Farbe auswählen Wählen Sie eine Farbe Verlauf anzeigen Beim Drücken des Kurzbefehls: 