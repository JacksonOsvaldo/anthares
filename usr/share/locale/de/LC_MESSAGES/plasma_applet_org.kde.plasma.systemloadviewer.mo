��    "      ,  /   <      �     �          !     7     ;     D     H     O     [     o     x     �     �     �     �     �     �     �     �     �     �     �     �     �                    *     :     ?     K  
   \     g  �  m     
  
     "        :     A  	   I     S     Z     l     �     �     �     �  	   �     �     �  	   �     �     �     �                ,     8  	   R     \     q     �     �  
   �     �  !   �  	   �        	           !                                
                                  "                                                                         Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cached: Circular Colors Compact Bar General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-04-18 15:17+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 s Anwendung: Durchschnittliche Taktrate: %1 MHz Balken Puffer: Prozessor CPU %1 Prozessor-Anzeige CPU%1: %2 % @ %3 Mhz CPU: %1% CPUs getrennt Zwischenspeicher Zwischengespeichert: Kreisring Farben Kompakter Balken Allgemein IOWait: Speicher Speicher-Anzeige Speicher: %1/%2 MiB Anzeigeart: Priorität: Farben manuell einstellen Anzeigen: Auslagerungsspeicher Auslagerungsspeicher-Anzeige Auslagerungsspeicher: %1/%2 MiB System: Systemlast Aktualisierungsintervall: Verwendeter Auslagerungsspeicher: Benutzer: 