��          �            h     i  
   �     �  .   �     �     �     �      �  *         9  ,   Z  ,   �  0   �     �  �  �  )   �     �     �  (   �     �             $   %  9   J  +   �     �     �  3   �     �                      
   	                                                       &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-12-04 17:18+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Bilds&chirm nach dem Ruhezustand sperren: Aktivierung Fehler Fehler beim Testen der Bildschirmsperre. Sofort Tastatur-Kurzbefehl: Sitzung sperren Bildschirm automatisch sperren nach: Bildschirm nach der Rückkehr aus dem Ruhezustand sperren &Passwort nach der Sperrung anfordern nach:  Min.  Min.  Sek.  Sek. Der globale Kurzbefehl zum Sperren des Bildschirms. Hintergrundbild-&Typ: 