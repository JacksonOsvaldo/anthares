��    B      ,  Y   <      �     �     �     �  !   �  "   �  &     ,   /  #   \  &   �  !   �  &   �  '   �  "     "   ;  '   ^     �  +   �  2   �     �  
   �     
          %     ,     @     H     O     b     {  !   �  +   �     �     �      �     	     (	     F	     U	     ]	  !   s	     �	  	   �	     �	     �	     �	     �	  &   �	     !
     @
     G
     b
     h
     p
     w
     |
     �
  $   �
     �
     �
     �
     �
          (     5  >   O  �  �     +     /     1     Q     X     ^     n     �  
   �     �     �  
   �     �     �  
   �     �  @   �  K        j     |     �     �  	   �     �     �     �     �     �       *     2   I  (   |  
   �  *   �     �  &   �       	   &     0  '   G     o  
   u  	   �  $   �     �     �  5   �        	   7      A     b     i  	   r     |     �     �  +   �     �     �                &     F      S  	   t     '             6      8             $       %   <   ,      B                     "   >       #              	   @      =   7   3                          +      
   &   !               0   :      ;           (   4              2                      A       -   )      9          *         1       /          .   ?   5         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-01-03 20:16+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms % &Passende Fenster-Eigenschaft:  Riesig Groß Keine Umrandung Keine seitliche Umrandung Normal Übergroß Winzig Noch riesiger Sehr groß Groß Klein Sehr groß Hinzufügen Griff zur Größenänderung von Fenstern ohne Rahmen hinzufügen Größenänderung von maximierten Fenstern über die Fensterkanten zulassen &Animationsdauer: Animationen &Knopfgröße: Umrandungsgröße: Zentriert Zentriert (volle Breite) Klasse:  Farbe: Dekorations-Einstellungen Fenster-Eigenschaften ermitteln Dialog Einen Kreis um den Schließ-Knopf zeichnen Trennung zwischen Titelleiste und Fenster anzeigen Hintergrundverlauf des Fensters zeichnen Bearbeiten Ausnahme bearbeiten - Breeze-Einstellungen Animationen aktivieren Diese Ausnahme aktivieren/deaktivieren Ausnahme-Typ Allgemein Titelleiste ausblenden Informationen zum ausgewählten Fenster Links Nach unten Nach oben Neue Ausnahme - Breeze-Einstellungen Frage - Breeze-Einstellungen Regulärer Ausdruck Die Syntax des regulären Ausdrucks ist nicht korrekt Passender &regulärer Ausdruck:  Entfernen Ausgewählte Ausnahme entfernen? Rechts Schatten &Größe: Winzig Tite&lausrichtung: Titel:  Fensterklasse verwenden (gesamte Anwendung) Fenstertitel verwenden Warnung - Breeze-Einstellungen Name der Fensterklasse Fensterkennung Auswahl der Fenster-Eigenschaft Fenstertitel Fensterspezifische Einstellungen S&tärke: 