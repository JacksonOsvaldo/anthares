��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  -   5  7   c  #   �  #   �     �     �  4     0   L     }     �     �  H   �  +   �  7   &	  C   ^	     �	  G   �	  #   	
  z   -
  N   �
     �
       A     :   a                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-09-18 06:35+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 „initramfs“  kann nicht gestartet werden. „update-alternatives“  kann nicht gestartet werden. Plymouth-Startbildschirm einrichten Neue Startbildschirme herunterladen lueck@hube-lueck.de Neue Startbildschirme holen ... Die Ausführung von „Initramfs ist fehlgeschlagen. „Initramfs“ wurde mit dem Fehler %1 beendet. Installiert ein Design. Marco Martin Burkhard Lück Es wurde kein Design in den Parametern für das Hilfsprogramm angegeben. Installationsprogramm für Plymouth-Designs Auswahl eines globalen Startbildschirms für das System Das zu installierende Design muss eine vorhandene Archivdatei sein. Das Design %1 existiert nicht. Design defekt: Es wurde keine Datei „.plymouth“ im Design gefunden. Der Designordner %1 existiert nicht In diesem Modul können Sie das Erscheinungsbild der gesamten Arbeitsfläche mit einigen mitgelieferten Vorlagen anpassen. Authentifizierung nicht möglich, Aktion kann nicht ausgeführt werden: %1, %2 Deinstallieren Deinstalliert ein Design Die Ausführung von „update-alternatives“ ist fehlgeschlagen. „update-alternatives“ wurde mit dem Fehler %1 beendet. 