��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     �     �               3  T   N  S   �      �          0     C  	   W     a     m  0   �     �  (   �     �     �  �                                 
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilglobalconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-12-06 15:23+0100
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  % &Kritischer Ladestand: &Niedriger Ladestand: <b>Akku-Ladestände</b> Bei kritischem &Ladestand: Der Ladestand des Akkus wird als kritisch angesehen, wenn er diese Schwelle erreicht Der Ladestand des Akkus wird als niedrig angesehen, wenn er diese Schwelle erreicht Benachrichtigungen festlegen ... Akku-Ladestand kritisch Nichts unternehmen tr@erdfunkstelle.de Aktiviert Ruhezustand Akku-Ladestand niedrig Niedriger Ladestand für angeschlossene Geräte: Thomas Reitelbach Medienspieler im Standby-Modus anhalten: Herunterfahren Standby-Modus Der Energieverwaltungsdienst scheint nicht zu laufen.
Das können Sie ändern, indem Sie ihn starten oder unter "Starten und Beenden" einrichten. 