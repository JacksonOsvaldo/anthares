��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     O     \     p     v     }     �  W   �     �                    
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-12-26 19:35+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Schlagwort: Eintrag hinzufügen Alias Alias: Character-Runner-Einrichtung Wert Erstellt Zeichen aus :q:, falls es ein Hexadezimalwert oder ein festgelegter Alias ist. Eintrag löschen Hexadezimalwert: 