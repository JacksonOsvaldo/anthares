��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �       
             ,     3     G     Z     r     �  B   �  	   �  !   �     	  .   !	  	   P	     Z	     f	     �	     �	     �	     �	     �	     �	     �	  �   
  U   �
     �
          +                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-07-26 22:16+0200
Last-Translator: Burkhard Lück <lueck@hube-lueck.de>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Aktivitäten Alle anderen Aktivitäten Alle anderen Arbeitsflächen Alle anderen Bildschirme Alle Fenster Alternativ Arbeitsfläche 1 Inhalt Aktuelle Aktivität Aktuelle Anwendung Aktuelle Arbeitsfläche Aktueller Bildschirm Fenster filtern nach Fokus-Richtlinien schränken die Funktion des Fensterwechsels ein. Vorwärts Neues Fensterwechsel-Layout holen Ausgeblendete Fenster Symbol „Arbeitsfläche anzeigen“ einfügen Allgemein Minimierung Nur ein Fenster pro Anwendung Kürzlich verwendete Rückwärts Bildschirme Kurzbefehle Ausgewähltes Fenster anzeigen Sortierreihenfolge: Stapel-Reihenfolge Das aktuell ausgewählte Fenster wird hervorgehoben, indem alle anderen Fenster ausgeblendet werden. Diese Einstellung erfordert aktivierte Arbeitsflächen-Effekte. Dieser Effekt ersetzt die normale Liste, wenn Arbeitsflächen-Effekte aktiviert sind. Virtuellen Arbeitsflächen Sichtbare Fenster Darstellung 