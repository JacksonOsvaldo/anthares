��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �      �  "   �  Z   
  '   e  	   �     �  $   �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-22 20:33+0100
Last-Translator: Frederik Schwarzer <schwarzer@kde.org>
Language-Team: German <kde-i18n-de@kde.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Speicherplatzkontingent Kein Speicherplatzlimit gesetzt. Bitte installieren Sie „quota“ Werkzeug für Speicherplatzkontingent nicht gefunden.

Bitte installieren Sie „quota“. „quota“ kann nicht gestartet werden %1 von %2 %1 frei Speicherplatzkontingent: %1 % belegt %1: %2 % belegt 