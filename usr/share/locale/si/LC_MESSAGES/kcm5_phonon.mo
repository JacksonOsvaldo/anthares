��    @        Y         �  m   �     �  b        j       6   �     �  7   �            	   #     -  (   K  )   t  )   �     �     �  Y   �     E     Z  �   l      �     	  
   	     (	     =	     I	     _	     h	     |	     �	     �	     �	     �	     �	     �	     �	     
     
     
  	   "
  
   ,
     7
     F
  	   \
  
   f
  
   q
     |
     �
  	   �
     �
     �
  
   �
  �   �
     o  8     �   �     @  8   P  ,   �  ,   �  %   �     	  �  $  �   �  X   �  �     A   �  :   7  �   r  7   �  �   4     �     �     �     �  g     j   �  d   �  Q   Q  %   �  �   �  "   �  +   �    �     �  "        )  ,   @     m  2   �     �  "   �     �       E   #     i     x     �  A   �     �     �       "        7     N  #   k  5   �     �     �     �       g   3     �     �  G   �  $     �  @  1   �  �   �    �  1   �  �   �  �   q  �      W   �   K   �      "      #          9   *          @                     )                 5   7                      '   =   +   :         4                                   (   0       <         6       /       >       ,   3   1   8   ?         $      -         !       2       
       .                    ;   &   %            	           A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-26 23:17+0530
Last-Translator: Danishka Navin <danishka@gmail.com>
Language-Team: Sinhala <info@hanthana.org>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.1
 ඔබේ පද්ධතිය මත හමුවූ Phonon පසුඉම් ලැයිස්තුවක්. Phonon විසින් ඒවා භාවිතකරන පිළිවෙළට පෙලගස්වා ඇත. වෙත උපකරණ ලැයිස්තුව ඇතුළත් කරයි... පහත දැක්වෙන වෙනත් ශ්‍රව්‍ය වාදන ප්‍රවර්ග සඳහා දැනට පෙනෙන උපකරණ අභිප්‍රේත ඇතුළත් කරයි: ශ්‍රව්‍ය දෘඩාංග ස්ථාපනය ශ්‍රව්‍ය ප්‍රතිවාදනය '%1' ප්‍රවර්ගය සඳහා වන ශ්‍රව්‍ය වාදන උපකරණ අභිප්‍රේතය ශ්‍රව්‍ය පටිගතකිරීම '%1' ප්‍රවර්ගය සඳහා වන ශ්‍රව්‍ය පටිගත උපකරණ අභිප්‍රේතය පසුඉම Colin Guthrie සම්බන්ධකය Copyright 2006 Matthias Kretz පෙරනිමි ශ්‍රව්‍ය වාදන උපකරණ අභිප්‍රේත පෙරනිමි ශ්‍රව්‍ය පටිගත උපකරණ අභිප්‍රේත පෙරනිමි දෘශ්‍ය පටිගත උපකරණ අභිප්‍රේත පෙරනිමි/ඇතුළත් නොකල ප්‍රවර්ගය නිර්දේශ නොකරන තනි තනි ප්‍රවර්ග මගින් අතික්‍රමණය කල හැකි පෙරනිමි උපකරණ පෙළගැසම දක්වයි උපකරණ සැකසීම උපකරණ අභිප්‍රේත තෝරාගත් ප්‍රවර්ගයට ගැලපෙන උපකරණ ඔබේ පද්ධතියෙන් හමුවිය. ඔබට යෙදුම් මගින් භාවිත කරවීමට ඇවැසි උපකරණය තෝරන්න. danishka@gmail.com ඉදිරි මධ්‍යය ඉදිරි වම මධ්‍යයේ ඉදිරි වම ඉදිරි දකුණ මධ්‍යයේ ඉදිරි දකුණ දෘඩාංග නිදහස් උපකරණ ආදාන මට්ටම් වැරදි KDE ශ්‍රව්‍ය දෘඩාංග ස්ථාපනය Matthias Kretz මොනෝ Danishka Navin පෝනොන් සැකසුම් මොඩියුලය වාදනය (%1) නිර්දේශ පැතිකඩ පසුපස මධ්‍යය පසුපස වම පසුපස දකුණ පටිගතකිරීම (%1) උසස් උපකරණ පෙන්වන්න පැති වම පැති දකුණ ශබ්ද කාඩ්පත ශබ්ද උපකරණය ශබ්ද විකාශක ස්ථානගතකිරීම හා පිරික්සීම සබ්වූෆරය පිරික්සන්න තෝරාගත් උපකරණය පිරික්සන්න පිරික්සමින් %1 පෙළගැස්ම උපකරණයන්ගේ අභිප්‍රේත දක්වයි. කිසියම් හේතුවක් මත ප්‍රථම උපකරණය Phonon මගින් භාවිත කරවීමට නොහැකි වුවහොත් දෙවැන්න භාවිත කරනු ඇත. එලෙස ඉදිරියට යයි. නොදන්නා නාලිකාවක් තවත් ප්‍රවර්ග සඳහා දැනට පෙන්වන උපකරණ ලැයිස්තුව භාවිත කරන්න. මාධ්‍ය භාවිතයේ විවිධ ප්‍රවර්ග ඇත. එක් එක් ප්‍රවර්ගය සඳහා, ඔබට Phonon මගින් භාවිත කල යුතු උපකරණය නිර්දේශ කල හැක. දෘශ්‍ය පටිගතකිරීම '%1' ප්‍රවර්ගය සඳහා වන දෘශ්‍ය පටිගත උපකරණ අභිප්‍රේතය  ඔබේ පසුඉම ශ්‍රව්‍ය පටිගත කිරීම සඳහා සහාය නොදක්වනවා විය හැක ඔබේ පසුඉම දෘශ්‍ය පටිගත කිරීම සඳහා සහාය නොදක්වනවා විය හැක තෝරාගත් උපකරණයට අභිප්‍රේත නොමැත තෝරාගත් උපකරණය නිර්දේශ කරයි 