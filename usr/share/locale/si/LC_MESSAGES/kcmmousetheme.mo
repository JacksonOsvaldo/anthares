��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  �   �  �   �    �  "   �  >   �     	  P   1	     �	  "   �	     �	  3   �	  /   �	  o   #
  �   �
  u   >        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-12-31 01:24+0530
Last-Translator: Danishka Navin <danishka@gmail.com>
Language-Team: Sinhala <danishka@gmail.com>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 0.3
 <qt>ඔබට <i>%1</i>කර්සර තේමාව ඉවත් කිරීමට ඇවැසිද?<br />මෙය මෙම තේමාවට අදාල සියළු ගොනු මකාදමනු ඇත.</qt> <qt>ඔබට දැනට භාවිත කරන තේමාව මකාදැමිය නොහැක.<br />ඔබට ඒ සඳහා වෙනත් තේමාවකට මාරුවීමට සිදුවේ.</qt> ඔබේ අයිකන තේමා බහාළුම තුළ %1 නමැති අයිකන තේමාව දැනටමත් පවතී. ඔබට ඇත්තෙන්ම එය ප්‍රතිපිහිටුවීමට අවශ්‍යද? තහවුරු කිරීම කර්සර සැකසුම් වෙනස්විය විස්තරය තේමා URL ලිපිනය අදින්න හෝ ලියන්න danishka@gmail.com ඩනිෂ්ක නවින් නම තේමාව නැවත ලියන්නද? තේමාව ඉවත් කරන්න... %1 ගොනුව වලංගු කර්සර තේමා සංරක්‍ෂකයක් නොවේ. කර්සර තේමාව බාගත නොහැකි විය; කරුණාකත %1 ලිපිනය නිවැරදි දැයි බලන්න. %1 කර්සර තේමා සංරක්‍ෂක සොයාගැනීමට නොහැකි විය. 