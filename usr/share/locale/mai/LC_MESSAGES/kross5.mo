��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  9   �     �     
          4     I  L   \     �  ;   �  r   �  �   n  U   �  Q   S	     �	     �	  "   �	  S   �	  %   >
  
   d
  8   o
  D   �
     �
  ;   �
     9  b   I     �  M   �  
                                          
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-09-24 15:44+0530
Last-Translator: Rajesh Ranjan <rranjan@redhat.com>
Language-Team: Hindi <fedora-trans-hi@redhat.com>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: KBabel 1.11.4
 सामान्य नवीन स्क्रिप्ट जोड़ू. जोड़ू... रद करू टिप्पणीः sangeeta09@gmail.com संपादन चयनित स्क्रिप्ट संपादित करू. संपादन... चयनित स्क्रिप्ट चलाबू इंटरप्रेटर "%1" क' लेल स्क्रिप्ट बनाबै मे असफल स्क्रिप्ट फाइल "%1" क' लेल इंटरप्रेटर क' पता लगाने मे असफल इंटरप्रेटर "%1" केँ लोड करब मे असफल स्क्रिप्ट फाइल "%1" खोलबा मे असफल फाइल: प्रतीक: इन्टरप्रेटर: रूबी इंटरप्रेटर क' सुरक्षा स्तर संगीता कुमारी नाम: एहन कोनो फंक्शन नहि "%1" एहन कोनो इंटरप्रेटर नहि "%1" हटाबू चयनित स्क्रिप्ट हटाबू चलाबू स्क्रिप्ट फाइल "%1" अस्तित्व मे नहि अछि. रोकू चयनित स्क्रिप्ट क' चलाना रोकू पाठ: 