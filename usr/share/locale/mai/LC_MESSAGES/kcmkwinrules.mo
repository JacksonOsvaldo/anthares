��    c      4  �   L      p  
   q     |     �     �     �  
   �     �  	   �     �     �  "   �     	     )	     7	     D	     `	  	   p	     z	     �	     �	     �	  +   �	     �	     �	     �	     �	     �	     �	      
     &
     +
     9
     W
  O   _
     �
     �
     �
     �
     �
     �
  !   �
           >     C     W     c     o     s       
   �     �     �     �     �     �  
   �  
                  5     B     I     W     f     t     {     �  -   �     �     �     �     �  &   �          !     -     3     A     T  :   d     �     �     �     �     �     �     �  *   �          #  .   2  B   a     �     �     �     �  -   �  !     �  6  +   �       F   +  0   r  .   �     �     �     �  '        ?  P   Q     �  *   �     �  C   �  *   B      m     �     �     �     �  d   �     K     ^     q     �  ,   �     �     �       5     [   U     �  �   �  "   �  	   �     �  @   �       V     V   u  N   �       <   ,  !   i  '   �     �     �  $   �     �  '        B  Z   R  2   �  2   �  :     $   N     s  %   �  #   �     �  %   �  &     "   ?     b  G   x     �  �   �     `  "   t     �     �  o   �  *   /     Z     y  +   �  0   �  4   �  {   !     �     �  /   �  !         "  ,   6     c  q   |  (   �  (      {   @   �   �   '   C!  (   k!  7   �!  (   �!  s   �!     i"     0       X   >   @      8   K       9             ;          \          /           Z   O   P   ]              1   *   %   S   4   J       5   _   H   E                  L   c   B       =             2              [       7   :           G   3      Q      `       I   M   &   -   '   
   .   )                                   (   b         C           a   "   Y          V       #       N      A   +       <      ?                  	             !   T          D      6      R   F   ^   W          ,   U   $        &Closeable &Desktop &Focus stealing prevention &Fullscreen &Machine (hostname): &Modify... &New... &Position &Single Shortcut &Size (c) 2004 KWin and KControl Authors 0123456789-+,xX: Accept &focus All Desktops Application settings for %1 Apply Initially Apply Now C&lear Cascade Centered Class: Consult the documentation for more details. Default Delete Desktop Dialog Window Do Not Affect Dock (panel) EMAIL OF TRANSLATORSYour emails Edit Edit Shortcut Edit Window-Specific Settings Edit... Enable this checkbox to alter this window property for the specified window(s). Exact Match Extreme Force Force Temporarily High Ignore requested &geometry Information About Selected Window Internal setting for remembering KWin KWin helper utility Keep &above Keep &below Low Lubos Lunak M&aximum size M&inimized M&inimum size Machine: Match w&hole window class Maximized &horizontally Maximized &vertically Maximizing Move &Down Move &Up NAME OF TRANSLATORSYour names No Placement Normal Normal Window On Main Window Override Type Random Regular Expression Remember Remember settings separately for every window Role: Settings for %1 Sh&aded Shortcut Show internal settings for remembering Skip &taskbar Skip pa&ger Smart Splash Screen Standalone Menubar Substring Match This helper utility is not supposed to be called directly. Title: Toolbar Top-Left Corner Torn-Off Menu Type: Under Mouse Unimportant Unknown - will be treated as Normal Window Unnamed entry Utility Window WId of the window for special window settings. Whether the settings should affect all windows of the application. Window &type Window &types: Window settings for %1 Window t&itle: Window-Specific Settings Configuration Module no focus stealing preventionNone Project-Id-Version: kcmkwinrules
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2010-01-29 13:59+0530
Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>
Language-Team: Maithili <maithili.sf.net>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);



 बन्न करब योग्य (&C) डेस्कटाप (&D) फोकस स्टीलिंग प्रिवेंशन (&F) संपूर्ण स्क्रीन (&F) मशीन (होस्ट-नाम): (&M) सुधारू... (&M) नया... (&N) स्थिति (&P) एकल शार्टकट... (&S) आकार (&S) (c) 2004 के-विन आओर के-कन्ट्रोल लेखक 0123456789-+,xX: फोकस स्वीकारू (&f) सभ डेस्कटाप %1 क' लेल अनुप्रयोग बिन्यास आरंभ मे लागू करू अखन लागू करू साफ करू (&l) प्रपाती केंद्रित वर्गः बाइली विवरणसभ क' लेल दस्ताबेज केँ देखू मूलभूत मेटाबू डेस्कटाप संवाद विंडो प्रभावित नहि करू डाक करू (फलक) sangeeta09@gmail.com संपादन शार्टकट संपादित करू विंडो-विशिष्ट बिन्यास संपादित करू संपादन... उल्लेखित विंडो केर विंडो-गुणसभ केँ बदलब क' लेल एहि चेक-बक्सा केँ सक्षम करू. सटीक जोडीदार चरम बाधित करू अस्थायी रूप सँ बाध्य करू बेसी निवेदित ज्यामितिक उपेक्षा करू (&g) चयनित विंडो केर संबंध मे जानकारी याद रखबा क' लेल आंतरिक बिन्यास के-विन के-विन मद्दति यूटिलिटी उप्पर राखू (&A) नीच्चाँ राखू (&B) कम लुबास लुनाक अधिकतम आकार (&a) न्यूनतम (&i) न्यूनतम आकार (&i) मशीनः संपूर्ण विंडो वर्गक जोडी मिलाबू (&h) आडा मे अधिकतम करू (&h) खडा मे अधिकतम करू (&v) अधिकतम कएल जाए रहल अछि नीच्चाँ जाउ (&D) उप्पर जाउ (&U) संगीता कुमारी कोनो स्थल नहि सामान्य सामान्य विंडो मुख्य विंडो पर ओवरराइड टाइप क्रमहीन नियमित (रेगुलर) एक्सप्रेशन. याद राखू प्रत्येक विंडो क' लेल बिन्याससभ केँ अलग-अलग याद राखू भूमिका: %1 लेल बिन्यास छाया (&a) शार्टकट याद रखबाक लेल आंतरिक बिन्याससभ केँ देखाबू काजपट्टी छोडू (&t) पेजर छोडू (&g) स्मार्ट स्प्लैश स्क्रीन अलग-थलग मेनू-पट्टी सबस्ट्रिंग जोडीदार ई मद्दति यूटिलिटी सीधे बुलाएल नहि जाए सकैत अछि. शीर्षक: अओजारपट्टी ऊप्पर बम्माँ कोना टार्न-आफ मेनt प्रकार: माउस केर नीच्चाँ महत्वहीन अज्ञात - सामान्य विंडो केर रूपेँ मानल जएताह अनाम प्रविष्टि यूटिलिटी विंडो विशिष्ट विंडो सेटिंग क' लेल विंडो क' डबल्यूआईडी सेटिंग की अनुप्रयोग केर सबहि विंडो पर प्रभाव डालैछ विंडो प्रकार (&t) विंडो प्रकार (&t): %1 क' लेल विंडो बिन्यास विंडो शीर्षक (&i): विंडो-विशिष्ट बिन्यास कान्फिगरेशन मोड्यूल किछु नहि 