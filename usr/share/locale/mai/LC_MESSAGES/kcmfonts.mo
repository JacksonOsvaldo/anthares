��          �   %   �      @     A  L   F  /   �     �     �     �     �          !     1     G     V  I   g     �     �     �     �  !   �  "         C  6   `  *   �     �     �  �  �     �  �   �  ~   W  M   �     $  u   7  d   �     	  )   .	  A   X	     �	  C   �	  �   �	     �
  P   �
            %   #     I  e   �  �   /  �   �     r     �           	                          
                                                                                          to  <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2009-01-18 15:28+0530
Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>
Language-Team: Maithili <maithili.sf.net>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);


  प्रति  <p>किछु परिवर्तन जे डीपीआई केवल नवीन प्रारंभ कएल अनुप्रयोगेसभ लागू हाएत</p> एकटा अ-समानुपातिक फोन्ट (जहिना टाइपराइटर फान्ट). सभटा फान्टसभ समायोजित करू... (&j) बीजीआर सभटा फान्ट केँ परिवर्तित करब क' लेल क्लिक करू एन्टी अलियासिंग बिन्यास कान्फिगर करू कान्फिगर... रेंज बाहर करू: (&x) फान्ट बिन्यास परिवर्तित %1:  फान्टसभ डीपीआइ बाध्य करू: हिन्टिंग एकटा प्रक्रिया अछि जे छोट आकारसभ केर फान्ट केर गुणसभ केँ उभारैत अछि RGB एंटी-अलियासिंग इस्तेमाल करू (&n) अक्षम सक्षम तंत्र बिन्यास मेनू बार तथा पापअप मेनू द्वारा उपयोग मे आबैत अछि. विंडो शीर्षक पट्टी केर द्वारा उपयोगमे सामान्य पाठ क' लेल उपयोग मे (जहिना बटन लेबल, वस्तु सूची). अओजार पट्टी प्रतीक केर बाजू सँ देखाओल जाएबला पाठ क' लेल उपयोग मे आबैछ. खडा बीजीआर खडा आरजीबी 