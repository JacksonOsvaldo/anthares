��    0      �  C         (  "   )     L     c     �     �  #   �     �     �  &     +   -     Y  :   w  #   �  &   �  (   �     &  @   A     �  #   �     �  *   �  /   
     :  /   R     �  2   �  .   �  9     �   ;  /   �  2   �  2   %	  2   X	     �	  &   �	  (   �	     �	     
  !   
     ?
  $   ]
  4   �
  '   �
  &   �
           '     F  �  \  "     %   6  *   \     �     �  2   �  &   �  &     <   )  C   f     �     �  ?   �  D     b   _     �  :   �               0  ?   C  P   �     �  X   �     K  ~   b  D   �  g   &     �     �     �     �  )   �       N   !  X   p  	   �     �  9   �       (   4  S   ]  V   �  :        C  %   X  2   ~        %       	                     "       )   &              '                         /       !              (       ,      
   $   *       .   -         #                +                0                                                (C) 2000-2009, The DrKonqi Authors @action:button&Reload @action:button&Save to File... @action:buttonCancel @action:buttonLogin @action:buttonOpen selected report @action:buttonRetry search @action:buttonRetry... @action:buttonSearch for more reports @action:buttonSho&w Contents of the Report @action:buttonStop searching @action:button remove the selected item from a listRemove @infoError fetching the bug report @infoInvalid bug list: corrupted data @infoThis report is considered helpful. @info bug resolutionFixed @info/rich crash situation examplewidgets that you were running @info:creditA. L. Spehr @info:creditDario Andres Rodriguez @info:creditGeorge Kiagiadakis @info:statusError fetching the bug report @info:statusError fetching the bug report list @info:statusLoading... @info:statusSearch Finished. No reports found. @info:statusSearch stopped. @info:statusSending crash report... (please wait) @info:tooltipStarts the bug report assistant. @info:tooltipUse this button to stop the current search. @label clicking/hovering this, the user will get examples about application specific details s/he can provide<a href="#">Examples</a> @label:listbox KDE distribution methodMac OS X @label:listbox KDE distribution methodUnspecified @label:textbox bugzilla account passwordPassword: @label:textbox bugzilla account usernameUsername: @titleConclusions @titleInformation about bug reporting @titleWhat do you know about the crash? @title:columnBug ID @title:columnDescription @title:tab&Developer Information @title:windowBug Description @title:windowContents of the Report @title:windowCrash Information is not useful enough @title:windowCrash Reporting Assistant @title:windowWe need more information EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names The KDE Crash Handler Project-Id-Version: drkonqi
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-15 03:28+0200
PO-Revision-Date: 2010-09-24 21:31+0530
Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>
Language-Team: Maithili <bhashaghar@googlegroups.com>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);








 (C) 2000-2009, The DrKonqi Authors फिनु लोड करू (&R) फाइलमे सहेजू (&S)... रद करू लॉगिन चयनित रिपोर्ट खोलू खोजकेँ फेर करू फेर कोशिश करू... बेसी रिपोर्टक लेल खोजू रिपोर्टक सामग्री देखाउ (&w) खोजनाइ रोकू हटाबू बग रिपोट लाबैबला त्रुटि अमान्य बग सूची: अधलाह डाटा ई रिपोर्ट मद्दतिकएनिहार प्रतीत होइछ. स्थिर विजेट जे अहाँ चलाबै छी A. L. Spehr Dario Andres Rodriguez George Kiagiadakis बग रिपोट लाबैबला त्रुटि ई बग रिपोर्ट सूची आनैमे त्रुटि लोड कए रहल... खोज संपन्न. कोनो रिपोर्ट नहि भेटल. खोज रूकल क्रैश रिपोर्ट भेज रहल अछि... (कृपया प्रतीक्षा करू) बग रिपोर्ट मद्दतिकएनिहार मोजुदा खोज रोकबाक लेल ई बटनक प्रयोग करू <a href="#">Examples</a> Mac OS X अवर्गीकृत कूटशब्द:  प्रयोक्ताक नाम: परिणाम बग रिपोर्टिंगक संबंधमे सूचना अहाँ ई क्रैशक संबंधमे की जानैत छी? बग ID विवरण विकासकएनिहार सूचना (&D) बग विवरण रिपोटक सामग्री क्रैश सूचना बेसी उपयोगी नहि अछि क्रैश रिपोर्टिंग मद्दतिकएनिहार हम बेसी सूचना चाहैत छी sangeeta09@gmail.com संगीता कुमारी केडीई क्रेश हैंडलर 