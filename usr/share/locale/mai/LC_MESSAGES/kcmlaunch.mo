��          �      �       0     1     6  ^  S  P   �               #     0     M     \     p     �  �  �     N  M   S    �  �   �	  1   k
  "   �
  %   �
  3   �
  (     9   C  M   }  1   �                                      
   	                  sec &Startup indication timeout: <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-18 15:31+0530
Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>
Language-Team: Maithili <maithili.sf.net>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);



  sec स्टार्टअप इंडीकेशन टाइमआउटः व्यस्त संकेतक<h1>Busy Cursor</h1>
केडीई प्रस्तुत करैत अछि एकटा व्यस्त संकेतक- अनुप्रयोग प्रारंभ करब केर संबंधमे बताबै क'लेल.
 व्यस्त संकेतक सक्षम करब क' लेल एकटा प्रकार क' विजुअल फीडबैक
काम्बो बक्सामे सँ चुनू.
एहन भ' सकैत अछि जे किछु अनुप्रयोग एहि विशेषता- प्रारंभ करए केर संबंधमे बताबै क' लेल- अनभिज्ञ हुए
. एहन परिस्थितिमे, कर्सर टिमटिमओनाइ बन्न कए देत अछि
 'स्टार्टअप इंडीकेशन टाइमआउट' मे देल गेल समय केर उपरांत. <h1>लाँच फीडबैक</h1> एतय अहाँ एप्लीकेशन-लांच फीडबैक कान्फिगरेशन कए सकैत छी. टिमटिमाबैत संकेतक उछलैत संकेतक व्यस्त संकेतक काज पट्टी सक्षम करू अव्यस्त संकेतक अक्रिय व्यस्त संकेतक  स्टार्टअप इंडीकेशन टाइमआउटः काजपट्टी अधिसूचना 