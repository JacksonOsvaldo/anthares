��            )   �      �     �     �     �  +   �      �       ;   0     l     |     �     �      �     �  9        ?  3   G  	   {     �  5   �  *   �     �          *     1     >  +   [  (   �     �  3   �  �    F   �  1   �     +  k   ;  b   �  >   
	  �   I	  4   �	  K   %
  [   q
  r   �
     @  A   U  �   �     W  �   m       7   -  �   e     D  %   �  A   �     ,  "   <  A   _  e   �  `     X   h  r   �                                               
                                                  	                                        %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2009-01-18 15:27+0530
Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>
Language-Team: Maithili <maithili.sf.net>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);


 प्रसंग %1 पहिने सँ मोजुद अछि मनोभावचिह्न जोड़ू जोडू... मनोभावचिह्न प्रसंग बनाबै लेल किस्म चुनू इमोटिकान प्रसंग संस्थापित नहि कए सकल नवीन मनोभावचिह्न बनाबू किछु पाठ तथा एकटा प्रतीक आबंटित कए एकटा नवीन मनोभावचिह्न बनाबू मनोभावचिह्न मेटाबू नवीन मनोभावचिह्न डिजाइन करू की अहाँ सच्चे %1 केँ मेटाएब चाहैत छी? इमोटिकान प्रसंग यूआरएल ड्रैग अथवा टाइप करू sangeeta09@gmail.com मनोभावचिह्न संपादित करू एकर प्रतीक अथवा एकर पाठ केँ बदलब क' लेल चयनित मनोभावचिह्न केँ संपादित करू संपादन... इमोटिकान प्रसंगसभ केँ स्थानीय फाइलस सँ संस्थापित कएनाइ हाएत. भाव-प्रतीक मनोभावचिह्न प्रबंधक प्रसंग अभिलेख फाइल जे अहाँक पास पहिने सँ स्थानीय रूप सँ मोजुद अछि ओकरा संस्थापित करू चुनल गेल पाठ अथवा मनोभावचिह्न केँ परिवर्धित करू संगीता कुमारी नवीन मनोभावचिह्न प्रसंग हटाबू प्रसंग हटाबू चयनित मनोभावचिह्न हटाबू अपन डिस्क सँ चयनित मनोभावचिह्न मेटाबू अपन डिस्क सँ चयनित प्रसंग केँ मेटाबू मनोभावचिह्न केर चारोकात जगह चाही ई चयनित प्रसंग केँ अहाँक डिस्क सँ मेटाए देत. 