��          �   %   �      p  &   q     �     �     �     �      �     �     �     �       ,   #     P     o     u     {  '   �     �     �     �     �               #     '     G  &   L     s  �  y  
   )  1   4     f     v       O   �     �  0   �       .   )  G   X  G   �     �     �  	          }   ,     �     �  !   �     �  *   �     &	  3   /	     c	  7   l	     �	                                                 	                                       
                                   @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-06-28 16:40+0430
Last-Translator: Mohammad Reza Mirdamadi <mohi@linuxshop.ir>
Language-Team: Farsi (Persian) <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=1; plural=0;
 عمومی افزودن دست‌نوشته‌ای جدید. افزودن... لغو؟ توضیح: mohi@linuxshop.ir , kazemi@itland.ir , mebrahim@gmail.com , s.taghavi@gmail.com ویرایش ویرایش دست‌نوشته برگزیده. ویرایش... اجرای دست‌نوشته برگزیده. خرابی در ایجاد دست‌نوشته برای مفسر »%1« خرابی در باز کردن پرونده دست‌نوشته »%1« پرونده: شمایل: مفسر: سطح امن مفسر رابی محمدرضا میردامادی , نازنین کاظمی , محمد ابراهیم محمدی پناه , سعید تقوی نام: بدون چنین مفسر "%1" بدون چنین مفسر »%1« حذف حذف دست‌نوشته برگزیده. اجرا پرونده اسکریپت %1 موجود نیست. ایست توقف اجرای دست‌نوشته برگزیده. متن: 