��    M      �  g   �      �     �     �     �     �  
   �  #   �     �     �     �  
              6     W  /   j     �     �     �  p   �     7     J     ]     v     �     �     �     �  	   �  
   �     �     �     	      	     =	  	   X	     b	     r	     ~	     �	     �	     �	     �	     
     &
     5
     M
     f
  9   s
     �
     �
     �
     �
     �
          +     K     b     x  :   �     �     �  #        &     <     \     q     �     �     �     �     �          +     A    \  �   q  S   O  �  �     >     \     i          �  2   �     �     �     �       <        U  #   k  /   �     �     �  2   �  �     )   �  %     D   7  @   |  =   �     �           0  !   ?     a  %   w     �  "   �  +   �  -   �     +      E     f     z  <   �  >   �  :     <   O  %   �  #   �     �  #   �       i   7  "   �  '   �  %   �  ,     *   ?  &   j  *   �  %   �  !   �  %     s   *  B   �  .   �  ;     '   L  4   t  (   �  5   �  ,     *   5  &   `  *   �  &   �  %   �  !   �  %   !  �  G  d  "  �   �      =   I   A   .             9   K   B   H   ;              /   0   %   &       L   ?            !   5      
      >                @                   -         6   )   7   *       C   E           #   J                        1      ,   8                              +   	              4          "   M   (   <         '             3   G      2          :   $      D   F           &All Desktops &Close &Fullscreen &Move &No Border Activate Window Demanding Attention Close Window Cristian Tibirna Daniel M. Duley Desktop %1 Disable configuration options EMAIL OF TRANSLATORSYour emails Hide Window Border Indicate that KWin has recently crashed n times KDE window manager KWin KWin helper utility KWin is unstable.
It seems to have crashed several times in a row.
You can select another window manager to run: Keep &Above Others Keep &Below Others Keep Window Above Others Keep Window Below Others Keep Window on All Desktops Kill Window Lower Window Luboš Luňák Ma&ximize Maintainer Make Window Fullscreen Matthias Ettrich Maximize Window Maximize Window Horizontally Maximize Window Vertically Mi&nimize Minimize Window Move Window NAME OF TRANSLATORSYour names Pack Grow Window Horizontally Pack Grow Window Vertically Pack Shrink Window Horizontally Pack Shrink Window Vertically Pack Window Down Pack Window Up Pack Window to the Left Pack Window to the Right Raise Window Replace already-running ICCCM2.0-compliant window manager Resize Window Setup Window Shortcut Shade Window Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Next Desktop Switch to Next Screen Switch to Previous Desktop This helper utility is not supposed to be called directly. Toggle Window Raise/Lower Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Walk Through Windows Walk Through Windows (Reverse) Window One Desktop Down Window One Desktop Up Window One Desktop to the Left Window One Desktop to the Right Window Operations Menu Window to Next Desktop Window to Next Screen Window to Previous Desktop You have selected to show a window in fullscreen mode.
If the application itself does not have an option to turn the fullscreen mode off you will not be able to disable it again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. You have selected to show a window without its border.
Without the border, you will not be able to enable the border again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. kwin: unable to claim manager selection, another wm running? (try using --replace)
 Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:16+0100
PO-Revision-Date: 2007-07-31 19:27+0330
Last-Translator: Nazanin Kazemi <kazemi@itland.ir>
Language-Team: Persian <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
 &تمام رومیزیها‌ &بستن‌ &تمام پرده‌ &حرکت‌ &بدون لبه‌ فعال کردن پنجره تقاضای توجه بستن پنجره Cristian Tibirna Daniel M. Duley رومیزی %1 غیرفعال کردن گزینه‌های پیکربندی daniarzadeh@itland.ir مخفی کردن لبه پنجره Indicate that KWin has recently crashed n times مدیر پنجره KDE KWin برنامه سودمند کمک‌کننده KWin KWin ناپایدار است.
به نظر می‌رسد چند بار پشت سر هم دچار سانحه شده است.
می‌توانید مدیر پنجرهٔ دیگری برای اجرا انتخاب کنید: نگه داشتن &بالای بقیه‌ نگه داشتن &زیر بقیه‌ نگه داشتن پنجره بالای پنجره‌های دیگر نگه داشتن پنجره زیر پنجره‌های دیگر نگه داشتن پنجره روی تمام رومیزیها کشتن پنجره پایین آوردن پنجره Luboš Luňák &بیشینه‌‌‌سازی‌ نگه‌دارنده تمام پرده کردن پنجره Matthias Ettrich بیشینه‌سازی پنجره بیشینه‌سازی افقی پنجره بیشینه‌سازی عمودی پنجره &کمینه‌سازی‌ کمینه‌سازی پنجره حرکت پنجره نسیم دانیارزاده فشردن پنجره بزرگ‌شده به طور افقی فشردن پنجره بزرگ‌شده به طور عمودی فشردن پنجره جمع‌شده به طور افقی فشردن پنجره جمع‌شده به طور عمودی فشردن پنجره به پایین فشردن پنجره به بالا فشردن پنجره به چپ فشردن پنجره به راست بالا بردن پنجره جایگزینی مدیر پنجره تابع ICCCM2.0 که در حال حاضر در حال اجراست تغییر اندازه پنجره برپایی میان‌بر پنجره سایه‌دار کردن پنجره سودهی یک رومیزی به پایین سودهی یک رومیزی به بالا سودهی یک رومیزی به چپ سودهی یک رومیزی به راست سودهی به رومیزی بعدی سودهی به پرده بعدی سودهی به رومیزی قبلی این برنامه سودمند کمک‌کننده برای فراخوانی مستقیم فرض نمی‌شود. زدن ضامن بالا بردن/پایین آوردن پنجره گردش در میان فهرست رومیزی گردش در میان فهرست رومیزی )معکوس( گردش در میان رومیزیها گردش در میان رومیزیها )معکوس( گردش در میان پنجره‌ها گردش در میان پنجره‌ها )معکوس( پنجره یک رومیزی به پایین پنجره یک رومیزی به بالا پنجره یک رومیزی به چپ پنجره یک رومیزی به راست گزینگان عملیات پنجره پنجره در رومیزی بعدی پنجره در پرده بعدی پنجره در رومیزی قبلی نمایش یک پنجره در حالت تمام پرده را برگزیده‌اید.
اگر خودکاربرد گزینه‌ای برای خاموش کردن حالت تمام پرده نداشته باشد، دوباره قادر به غیرفعال‌سازی آن با استفاده از موشی نمی‌باشید: به جای استفاده فعال از میان‌بر %1 صفحه کلید، از گزینگان عملیات پنجره استفاده کنید. نمایش یک پنجره بدون لبه‌اش را برگزیده‌اید. 
بدون لبه، دوباره قادر به فعال‌سازی لبه با استفاده از موشی نمی‌باشید: به جای استفاده فعال از میان‌بر %1 صفحه کلید، از گزینگان عملیات پنجره استفاده کنید. kwin:ناتوانی در درخواست گزینش مدیر، wm دیگری در حال اجراست؟ )استفاده از --replace را امتحان کنید(
 