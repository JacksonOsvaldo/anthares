��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     %     5     C     \     r  #     �   �  ,   y     �     �     �  �   �     f     y     �     �     �  
   �     �  
   �     �     �  3   	  $   =     b     �     �  .   �     �  3   �  �     n   �     U  ;   r     �     �  
   �     �     �      �       )   .  0   X  /   �     �     �  F   �  `   (     �     �     �     �  �   �  H   c     �  ]  �               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2011-12-15 00:29+0330
Last-Translator: Mohammad Reza Mirdamadi <mohi@linuxshop.ir>
Language-Team: Farsi (Persian) <>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 &مقدار:‌ &جلوه:‌ رنگ &ثانویه:‌ &نیمه شفاف‌ &چهره‌ (ح) ۲۰۰۳-۲۰۰۰ Geert Jansen <qt>مطمئن هستید که می‌خواهید چهره شمایل <strong>%1</strong> را حذف کنید؟ <br /><br />پرونده‌های نصب‌شده توسط چهره را حذف می‌کند.</qt> <qt>نصب چهره <strong>%1</strong></qt> فعال غیرفعال پیش‌فرض هنگام فرآیند نصب مسأله‌ای رخ داد؛ به هرحال، بیشتر چهره‌ها در بایگانی نصب شده‌اند. &پیشرفته‌ همه شمایلها Antonio Larrosa Jimenez &رنگ:‌ رنگ کردن تأیید بدون اشباع توصیف رومیزی گفتگوها کشیدن یا تحریر نشانی وب چهره mohi@linuxshop.ir , kazemi@itland.ir پارامترهای جلوه گاما Geert Jansen دریافت تم جدید از اینترنت شمایلها پیمانه تابلوی کنترل شمایلها اگر شما درحال‌حاظر یک آرشیو چهره دارید، این کلید آنرا از آرشیو خارج و آنرا برای برنامه‌های کی‌دی‌ای در دسترس قرار میدهد نصب پرونده آرشیو شده‌ی چهره‌ای که شما همین حالا موجود دارید میله ابزار اصلی محمدرضا میردامادی , نازنین کاظمی نام بدون جلوه تابلو پیش‌نمایش حذف چهره حذف تم از دیسک شما تنظیم جلوه... برپایی جلوه شمایل فعال برپایی جلوه شمایل پیش‌فرض برپایی جلوه شمایل غیرفعال اندازه: شمایلهای کوچک پرونده، بایگانی چهره شمایل معتبر نیست. این کار چهره‌های انتخاب شده را از دیسک شما حذف میکند. به خاکستری به تک‌رنگ میله ابزار Torsten Rahn قادر به بارگیری بایگانی چهره شمایل نیست؛
لطفاً، بررسی کنید که نشانی %1 درست باشد. قادر به یافتن بایگانی چهره شمایل %1 نیست. استفاده از شمایل برای استفاده از این عمل، شما باید به اینترنت متصل باشید. پنجره‌ی محاوره‌ای لیستی از چهره‌ها (تم) را از سایت http://www.kde.org نمایش میدهد. با کلیک روی کلید نصب مربوط به هر چهره، میتوانید آنرا نصب کنید. 