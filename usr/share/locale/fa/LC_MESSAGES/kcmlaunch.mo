��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  7   �  �  7  |  �	  �   P  #   �       "   1  6   T  '   �  -   �  7   �  "                                             
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2006-12-09 13:42+0330
Last-Translator: Nasim Daniarzadeh <daniarzadeh@itland.ir>
Language-Team: Persian <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=1; plural=0;
  ثانیه &راه‌اندازی اتمام وقت نشانه:‌ <H1>اخطار میله تکلیف</H1>
می‌توانید روش دوم اخطار راه‌اندازی که توسط میله تکلیفی که
یک دکمه با یک ساعت شنی چرخان در آن ظاهر می‌شود، استفاده می‌گردد را فعال کنید،
و نماد این است که کاربرد آغازشده شما در حال بار شدن است.
ممکن است برخی از کاربردها از این اخطار راه‌اندازی
آگاه نباشند. در این حالت، دکمه پس از زمان
داده‌شده در بخش »راه‌اندازی اتمام وقت نشانه« ناپدید می‌شود <h1>مکان‌نمای مشغول</h1>
KDE مکان‌نمای مشغول را برای اخطار راه‌اندازی کاربرد پیشنهاد می‌کند.
برای فعال‌سازی مکان‌نمای مشغول، یک نوع بازخورد تصویری را
 از جعبه ترکیب انتخاب کنید.
شاید اتفاق بیفتد که برخی کاربردها از این اخطار راه‌اندازی آگاه نباشند. در این صورت، مکان‌نما پس از زمان داده‌شده در بخش »اتمام وقت نشانه راه‌اندازی« از چشمک‌زنی می‌ایستد <h1>بازخورد راه‌انداز</h1> می‌توانید بازخورد راه‌انداز کاربرد را در اینجا پیکربندی کنید. مکان‌نمای چشمک‌زن واگشت مکان‌نما مکان‌نمای &مشغول‌ فعال‌سازی اخطار &میله تکلیف‌ بدون مکان‌نمای مشغول مکان‌نمای مشغول غیرفعال &راه‌اندازی اتمام وقت نشانه:‌ &اخطار میله تکلیف‌ 