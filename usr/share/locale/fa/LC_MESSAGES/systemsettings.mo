��          �   %   �      p  /   q     �     �     �     �     �     �  	   �     �  	              &  *   G     r  	   w     �  
   �     �     �     �  ,   �  /        <  b   L  	   �  P   �     
  �       �     �  #   �  (        B     \     e     r  2   �     �     �  &   �  
               $   (     M     c  Q   r  !   �  G   �  
   .	     9	  �   U	     �	  q   

     |
              
      	                                                                                                       <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Developer Dialog EMAIL OF TRANSLATORSYour emails General config for System SettingsGeneral Help Icon View Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Reset all current changes to previous values Search through a list of control modulesSearch System Settings The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2015-08-28 11:08+0430
Last-Translator: Mohi Mirdamadi <mohi@ubuntu.ir>
Language-Team: Farsi (Persian) <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 <i>شامل %1 مورد</i> درباره‌ی %1 درباره‌ی نمای جاری درباره تنظیمات سامانه اعمال تنظیمات مولف Ben Cooksley پیکربندی سامانه خود را پیکربندی کنید توسعه‌دهنده محاوره mohi@linuxshop.ir , mebrahim@gmail.com عمومی کمک نمای شمایل میانبر صفحه‌کلید: %1 نگه‌دارنده Mathias Soeken محمدرضا میردامادی , محمد ابراهیم محمدی‌پناه هیچ نمایی یافت نشد بازگرداندن تنظیمات جاری به مقادیر قبلی جستجو تنظیمات سامانه تنظیمات ماژول فعلی تغییر پیدا کرده.
آیا می‌خواهید تغییرات را اعمال کنید یا دور بریزید؟ نمایش درختی به «تنطیمات سامانه»  مکان مرکزی پیکربندی رابانه خود خوش آمدید. Will Stephenson 