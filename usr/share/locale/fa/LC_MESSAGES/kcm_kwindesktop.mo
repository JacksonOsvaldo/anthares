��    #      4  /   L           	       
   �  +   �  
   �     �     �     �           %     7     W  	   `      j  �   �  e   )  *   �  H   �          
     )  )   6  ;   `  	   �     �  (   �     �               ;     [     p     �  	   �  �  �     d	  �   z	     I
  N   ]
     �
     �
  1   �
     �
  ;        V  6   r     �     �  "   �     �  �   �  L   �  �   �     �  3   �     �  B   �  r   +     �  8   �  K   �  2   4  0   g  3   �  7   �       &   $  '   K  
   s        !                	                                                                                   
                #                    "               msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2012-02-28 15:37+0330
Last-Translator: Mohammad Reza Mirdamadi <mohi@linuxshop.ir>
Language-Team: American English <kde-i18n-fa@kde.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.4
 میلی‌ثانیه <h1>چند رومیزی</h1>در این پیمانه، می‌توانید تعداد رومیزی مجازی دلخواه و چگونگی برچسب‌گذاری آن‌ها را پیکربندی کنید. پویانمایی: میانبر عمومی «%1» به میزکار %2 اختصاص داده شد رومیزی %1 رومیزی %1: پویانمایی جلوه‌های میزکار نام رومیزی‌ها نمایش انتقال میزکار بر روی تصویر سودهی میزکارها ناوبری میزکار دور خودش میچرخد رومیزی‌ها مدت: ali@sarchami.com,mohi@linuxshop.ir اگر می‌خواهید امکان هدایت از لبه میزکار به لبه دیگر میزکار جدید از طریق صفحه کلید یا حاشیه میزکار فعال داشته باشید، این گزینه را انتخاب کنید. فعال‌سازی این گزینه پیش‌نمایشی کوچک از طرح‌بندی میزکار نشان می‌دهد که نشانگر میزکار انتخابی است. اینجا می‌توانید نام رومیزی %1 را وارد کنید اینجا می‌توانید تنظیم کنید که چند رومیزی مجازی روی میزکار کی‌دی‌ای خود می‌خواهید. طرح‌بندی علی سرچمی,محمدرضا میردامادی بدون پویانمایی میانبر مناسبی برای میزکار %1 پیدا نشد ناسازگاری میانبر: نمی‌توان میانبر %1 را برای میزکار %2 تنظیم کرد میانبرها نمایش شاخصهای طرح‌بندی میزکار نمایش میانبرها برای تمامی میزکارهای ممکن سودهی به یک میز کار در پایین سودهی به یک میز کار در بالا سودهی به بک میز کار در سمت چپ سودهی یک به میز کار در سمت راست سودهی به رومیزی %1 سو دهی به میزکار بعدی سو دهی به میز کار قبلی سودهی 