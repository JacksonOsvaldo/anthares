��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s  +        7     >  -   N     |     �     �  =   �  J   �  C   C  6   �  0   �  b   �     R     g  <   n  =   �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-06-09 17:15-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <kde-i18n-doc@kde.org>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ਮੌਜੂਦਾ ਵਰਤੋਂਕਾਰ ਆਮ ਲੇਆਉਟ ਸਕਰੀਨ ਨੂੰ ਲਾਕ ਕਰੋ ਨਵਾਂ ਸ਼ੈਸ਼ਨ ਨਾ-ਵਰਤੇ ...ਛੱਡੋ ਅਵਤਾਰ ਅਤੇ ਨਾਂ ਨੂੰ ਦਿਖਾਓ ਪੂਰੇ ਨਾਂ ਨੂੰ ਦਿਖਾਓ (ਜੇ ਹੈ ਤਾਂ) ਲਾਗਇਨ ਵਰਤੋਂ-ਨਾਂ ਨੂੰ ਦਿਖਾਓ ਕੇਵਲ ਅਵਤਾਰ ਨੂੰ ਦਿਖਾਓ ਕੇਵਲ ਨਾਂ ਨੂੰ ਦਿਖਾਓ ਸ਼ੈਸ਼ਨਾਂ ਬਾਰੇ ਤਕਨੀਕੀ ਜਾਣਕਾਰੀ ਨੂੰ ਦਿਖਾਓ %1 (%2) ਉੱਤੇ TTY %1 ਵਰਤੋਂਕਾਰ ਨਾਂ ਨੂੰ ਦਿਖਾਓ ਤੁਸੀਂ <b>%1</b> ਵਜੋਂ ਲਾਗਇਨ ਹੋ 