��    ?        Y         p  i   q  m   �     I  b   a     �     �  6   �       7   /     g     o  	   }     �  (   �  )   �  )   �     "     ?     E     Z  �   l      �  .   	     ?	  
   L	     W	     l	     x	     �	     �	     �	     �	     �	     �	     �	     �	     
     (
     6
     =
     E
  	   Q
  
   [
     f
     u
  	   �
  
   �
  
   �
     �
     �
  	   �
     �
     �
  
   �
     	  8        R  8   b  ,   �  ,   �  %   �       �  6  �   �    �  3   �  �   �  8   �  "     ^   5  (   �  d   �     "     6     V  ,   l  O   �  U   �  X   ?  ?   �     �     �       .       N  {   a     �     �  0        E  6   _     �     �     �  	   �  <   �  %   3     Y  ,   f  5   �     �     �     �          '     D      a  ,   �     �     �     �        <        Z     q  0   ~     �     �  |   �  +   b  g   �  y   �  |   p  D   �  -   2     #       $          9   +          ?                 7   "                 6           *              (   <   ,   >         5                                   )   1       ;                0       =       -   4   2   8             %      .                3              /      !             :   '   &         	   
            @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 Unknown Channel Use the currently shown device list for more categories. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-17 22:21+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 ਬੈਕਐਂਡ ਵਿੱਚ ਬਦਲਾਅ ਨੂੰ ਲਾਗੂ ਕਰਨ ਲਈ ਤੁਹਾਨੂੰ ਲਾਗ ਆਉਟ ਕਰਕੇ ਫੇਰ ਲਾਗਇਨ ਕਰਨਾ ਪਵੇਗਾ। ਤੁਹਾਡੇ ਸਿਸਟਮ ਉੱਤੇ ਫੋਨੋਨ ਬੈਕਐਂਡ ਦੀ ਲਿਸਟ ਮਿਲੀ ਹੈ।  ਇੱਥੇ ਦਿੱਤੀ ਲੜੀ ਇਹ ਦੱਸੇਗੀ ਕਿ ਫੋਨੋਨ ਵਿੱਚ ਕਿਵੇਂ ਵਰਤੇ ਜਾਣੇਗੇ। ਜੰਤਰ ਲਿਸਟ ਲਾਗੂ ਕਰੋ... ਮੌਜੂਦਾ ਵੇਖਾਈ ਜੰਤਰ ਪਸੰਦ ਲਿਸਟ ਨੂੰ ਅੱਗੇ ਦਿੱਤੀਆਂ ਹੋਰ ਆਡੀਓ ਪਲੇਅਬੈਕ ਕੈਟਾਗਰੀਆਂ ਲਈ ਲਾਗੂ ਕਰੋ: ਆਡੀਓ ਹਾਰਡਵੇਅਰ ਸੈਟਅੱਪ ਆਡੀਓ ਪਲੇਅਬੈਕ '%1' ਕੈਟਾਗਰੀ ਲਈ ਆਡੀਓ ਪਲੇਅਬੈਕ ਜੰਤਰ ਪਸੰਦ ਆਡੀਓ ਰਿਕਾਰਡਿੰਗ '%1' ਕੈਟਾਗਰੀ ਲਈ ਆਡੀਓ ਰਿਕਾਰਡਿੰਗ ਜੰਤਰ ਪਸੰਦ ਬੈਕ-ਐਂਡ ਕੋਲਿਨ ਗੁਥਰੀ ਕੁਨੈਕਟਰ ਕਾਪੀਰਾਈਟ 2006 Matthias Kretz ਡਿਫਾਲਟ ਆਡੀਓ ਪਲੇਅਬੈਕ ਜੰਤਰ ਪਸੰਦ ਡਿਫਾਲਟ ਆਡੀਓ ਰਿਕਾਰਡਿੰਗ ਜੰਤਰ ਪਸੰਦ ਡਿਫਾਲਟ ਵਿਡੀਓ ਰਿਕਾਰਡਿੰਗ ਜੰਤਰ ਪਸੰਦ ਡਿਫਾਲਟ/ਅਣ-ਦਿੱਤੀ ਕੈਟਾਗਰੀ ਅੰਤਰ ਜੰਤਰ ਸੰਰਚਨਾ ਜੰਤਰ ਪਸੰਦ ਤੁਹਾਡੇ ਸਿਸਟਮ ਉੱਤੇ ਜੰਤਰ ਮਿਲੇ ਹਨ, ਚੁਣੀ ਕੈਟਾਗਰੀ ਲਈ ਢੁੱਕਵੇਂ। ਜੰਤਰ ਦੀ ਚੋਣ ਕਰੋ, ਜੋ ਕਿ ਤੁਸੀਂ ਐਪਲੀਕੇਸ਼ਨ ਲਈ ਵਰਤਣਾ ਚਾਹੁੰਦੇ ਹੋ। aalam@users.sf.net ਚੁਣੇ ਆਡੀਓ ਆਉਟਪੁੱਟ ਜੰਤਰ ਨੂੰ ਸੈੱਟ ਕਰਨ ਥਈ ਫੇਲ੍ਹ ਹੈ ਅੱਗੇ ਸੈਂਟਰ ਅੱਗੇ ਖੱਬਾ ਸੈਂਟਰ ਦੇ ਅੱਗੇ ਖੱਬਾ ਅੱਗੇ ਸੱਜਾ ਸੈਂਟਰ ਵਿੱਚ ਅੱਗੇ ਸੱਜਾ ਹਾਰਡਵੇਅਰ ਆਜ਼ਾਦ ਜੰਤਰ ਇੰਪੁੱਟ ਲੈਵਲ ਗਲਤ KDE ਆਡੀਓ ਹਾਰਡਵੇਅਰ ਸੈਟਅੱਪ ਮੈਥਿਜ਼ ਕਰੀਟਜ਼ ਮੋਨੋ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ਫੋਨੋਨ ਸੰਰਚਨਾ ਮੋਡੀਊਲ ਪਲੇਅਬੈਕ (%1) ਪਸੰਦ ਪਰੋਫਾਈਲ ਪਿੱਛੇ ਸੈਂਟਰ ਪਿੱਛੇ ਖੱਬਾ ਪਿੱਛੇ ਸੱਜਾ ਰਿਕਾਰਡਿੰਗ (%1) ਤਕਨੀਕੀ ਜੰਤਰ ਵੇਖੋ ਖੱਬਾ ਪਾਸਾ ਸੱਜਾ ਪਾਸਾ ਸਾਊਂਡ ਕਾਰਡ ਸਾਊਂਡ ਜੰਤਰ ਸਪੀਕਰ ਸਥਿਤੀ ਤੇ ਟੈਸਟਿੰਗ ਸਬ-ਵੂਫ਼ਰ ਟੈਸਟ ਚੁਣੇ ਜੰਤਰ ਟੈਸਟ ਕਰੋ %1 ਟੈਸਟਿੰਗ ਅਣਜਾਣ ਚੈਨਲ ਹੋਰ ਕੈਟਾਗਰੀਆਂ ਲਈ ਮੌਜੂਦਾ ਵੇਖਾਈ ਜੰਤਰ ਲਿਸਟ ਵਰਤੋਂ। ਵਿਡੀਓ ਰਿਕਾਰਡਿੰਗ '%1' ਕੈਟਾਗਰੀ ਲਈ ਵਿਡੀਓ ਰਿਕਾਰਡਿੰਗ ਜੰਤਰ ਪਸੰਦ ਤੁਹਾਡਾ ਬੈਕਐਂਡ ਆਡੀਓ ਰਿਕਾਰਡਿੰਗ ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੈ ਤੁਹਾਡਾ ਬੈਕਐਂਡ ਵਿਡੀਓ ਰਿਕਾਰਡਿੰਗ ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੈ ਚੁਣੇ ਜੰਤਰ ਲਈ ਕੋਈ ਪਸੰਦ ਨਹੀਂ ਚੁਣੇ ਜੰਤਰ ਲਈ ਪਸੰਦ 