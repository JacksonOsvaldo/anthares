��    m      �  �   �      @	     A	  	   N	     X	     e	     s	     y	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	  
   �	     
     
  
   
     !
     *
     1
  	   @
     J
  
   P
     [
     g
     v
     |
     �
     �
     �
     �
  
   �
     �
     �
  
   �
     �
     �
          &     =     Y     m     r  	   �      �     �     �     �  
   �               )     <     P     g     x     �     �     �     �  
   �     �  )   �          -     E     T     c     t     z     �     �  '   �     �     �     �               ,     B     P  C   ^     �  s   �      &     G     \     s     �     �     �      �     �  -   �  /   &     V  	   _  !   i     �      �     �     �     �     �     	  �       �     �      �  ,        G     [     s     �  )   �     �  #   �  $        0     A  +   R  *   ~     �     �     �     �               /      M     n     �     �  -   �     �  -   �     +     F     W     h  #   |     �  F   �  #   �  ,   #  ;   P  -   �  ,   �  S   �  2   ;     n  I        �     �     �       *        F     a  4     1   �  ?   �  <   &  0   c  -   �  -   �  0   �     !  O   8  &   �  #   �  n   �  >   B  ;   �     �  2   �  /        >     R  2   f  G   �  `   �  #   B  +   f  V   �     �  #     ?   %  *   e  #   �  �   �  *   d   �   �   I   n!  @   �!  @   �!  9   :"  ?   t"  !   �"  3   �"  a   
#     l#  b   �#  �   �#     x$     �$  b   �$  b   %  e   q%     �%     �%  
   �%     &     &                G   ;   P   I      0   [   1   *       >   _   :          8                   5   "   \   `                   k          c   ?   R   	   ^   F          <   K                      C         7       m   i      Y       A   3   W      f      Q       S   E   =   ,   d       H   l   +   6       )   X          U      4             O       M          '          h   $          @   e       2   .               -   /   Z   N   B   9       (   &   g   J                %       L          
   #   V           b      !   ]      a   T          j      D              %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2014-03-16 23:21-0500
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 ਹੈਂਡਬੁੱਕ(&H) %1 ਬਾਰੇ (&A) ਅਸਲੀ ਸਾਈਜ਼(&A) ਬੁੱਕਮਾਰਕ ਸ਼ਾਮਲ(&A) ਪਿੱਛੇ(&B) ਬੰਦ ਕਰੋ(&C) %1 ਸੰਰਚਨਾ(&C).... ਕਾਪੀ ਕਰੋ(&C) ਬੁੱਕਮਾਰਕ ਸੋਧ(&E)... ਖੋਜ(&F)... ਪਹਿਲਾਂ ਪੇਜ਼(&F) ਪੇਜ਼ ਲਈ ਫਿੱਟ(&F) ਅੱਗੇ(&F) ਜਾਓ(&G)... ਲਾਈਨ ਉੱਤੇ ਜਾਓ(&G).... ਪੇਜ਼ ਉੱਤੇ ਜਾਓ(&G)... ਆਖਰੀ ਪੇਜ਼(&L) ਮੇਲ(&M)... ਨਵਾਂ(&N) ਅੱਗੇ ਸਫ਼ਾ(&N) ਖੋਲ੍ਹੋ(&O)... ਚੇਪੋ(&P) ਪਿੱਛੇ ਸਫ਼ਾ(&P) ਪਰਿੰਟ ਕਰੋ(&P)... ਬੰਦ ਕਰੋ(&Q) ਮੁੜ ਵੇਖੋ(&R) ਤਬਦੀਲ(&R)... ਬੱਗ ਰਿਪੋਰਟ ਦਿਓ(&R)... ਸੰਭਾਲੋ(&S) ਸੈਟਿੰਗ ਸੰਭਾਲੋ(&S).... ਸ਼ਬਦ-ਜੋੜ(&S)... ਵਾਪਸ(&U) ਉੱਤੇ(&U) ਜ਼ੂਮ(&Z)... ਕੇਡੀਈ (&KDE) ਬਾਰੇ ਸਾਫ਼ ਕਰੋ(&l) ਡੌਕੂਮੈਂਟ ਵਿੱਚ ਸ਼ਬਦ ਜੋੜ ਜਾਂਚ ਲਿਸਟ ਸਾਫ਼ ਕਰੋ ਡੌਕੂਮੈਂਟ ਬੰਦ ਕਰੋ ਨੋਟੀਫਿਕੇਸ਼ਨ ਸੰਰਚਨਾ(&N)... ਸ਼ਾਰਟਕਟ ਸੰਰਚਨਾ(&h).... ਟੂਲਬਾਰ ਸੰਰਚਨਾ(&b)... ਚੋਣ ਨੂੰ ਕਲਿੱਪਬੋਰਡ ਵਿੱਚ ਕਾਪੀ ਕਰੋ ਨਵਾਂ ਡੌਕੂਮੈਂਟ ਬਣਾਓ ਕੱਟੋ(&t) ਚੋਣ ਨੂੰ ਕਲਿੱਪਬੋਰਡ ਵਿੱਚ ਕੱਟੋ ਅਣ-ਚੁਣੇ(&l) aalam@users.sf.net ਆਟੋ-ਖੋਜ ਡਿਫਾਲਟ ਪੂਰੀ ਸਕਰੀਨ ਮੋਡ(&u) ਅੱਗੇ ਖੋਜ(&N) ਪਿੱਛੇ ਖੋਜ(&v) ਪੇਜ਼ ਚੌੜਾਈ ਲਈ ਫਿੱਟ(&H) ਪੇਜ਼ ਉਚਾਈ ਲਈ ਫਿੱਟ(&W) ਡੌਕੂਮੈਂਟ ਵਿੱਚ ਪਿੱਛੇ ਜਾਓ ਡੌਕੂਮੈਂਟ ਵਿੱਚ ਅੱਗੇ ਜਾਓ ਪਹਿਲੇ ਪੇਜ਼ ਉੱਤੇ ਜਾਓ ਆਖਰੀ ਪੇਜ਼ ਉੱਤੇ ਜਾਓ ਅਗਲੇ ਪੇਜ਼ ਉੱਤੇ ਜਾਓ ਪਿਛਲੇ ਪੇਜ਼ ਉੱਤੇ ਜਾਓ ਉੱਤੇ ਜਾਓ ਅ.ਸ.ਆਲਮ. ੨੦੦੪-੨੦੧੪
http://code.google.com/p/gurmukhi/ ਕੋਈ ਐਂਟਰੀ ਨਹੀਂ ਤਾਜ਼ਾ ਖੋਲ੍ਹੇ(&R) ਡੌਕੂਮੈਂਟ ਖੋਲ੍ਹੋ, ਜੋ ਹੁਣੇ ਹੀ ਖੋਲ੍ਹਿਆ ਗਿਆ ਸੀ ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਖੋਲ੍ਹੋ ਕਲਿੱਪਬੋਰਡ ਸਮੱਗਰੀ ਚੇਪੋ ਪਰਿੰਟ ਝਲਕ(&w) ਡੌਕੂਮੈਂਟ ਪਰਿੰਟ ਕਰੋ ਐਪਲੀਕੇਸ਼ਨ ਬੰਦ ਕਰੋ ਪਰਤਾਓ(&d) ਰੀਵਰਟ(&v) ਡੌਕੂਮੈਂਟ ਮੁੜ-ਵੇਖਾਓ ਆਖਰੀ ਵਾਪਸ ਲਈ ਕਾਰਵਾਈ ਫੇਰ ਕਰੋ ਦਸਤਾਵੇਜ਼ ਲਈ ਨਾ-ਸੰਭਾਲੇ ਬਦਲਾਅ ਵਾਪਿਸ ਲਵੋ ਇੰਝ ਸੰਭਾਲੋ(&A)... ਡੌਕੂਮੈਂਟ ਸੰਭਾਲੋ ਡੌਕੂਮੈਂਟ ਨੂੰ ਨਵੇਂ ਨਾਂ ਹੇਠ ਸੰਭਾਲੋ ਸਭ ਚੁਣੋ(&A) ਜ਼ੂਮ ਲੈਵਲ ਚੁਣੋ ਡੌਕੂਮੈਂਟ ਮੇਲ ਰਾਹੀਂ ਭੇਜੋ ਮੇਨੂ-ਪੱਟੀ ਵੇਖੋ(&M) ਟੂਲਬਾਰ ਵੇਖੋ(&T) ਮੇਨੂ-ਪੱਟੀ ਵੇਖੋ<p>ਜੇਕਰ ਮੇਨੂ-ਪੱਟੀ ਓਹਲੇ ਹੋ ਗਈ ਹੋਵੇ ਤਾਂ ਇਸਨੂੰ ਮੁੜ ਵੇਖੋ।</p> ਹਾਲਤ-ਪੱਟੀ ਵੇਖੋ(&a) ਹਾਲਤ-ਪੱਟੀ ਵੇਖੋ<p>ਹਾਲਤ-ਪੱਟੀ ਵੇਖੋ, ਜੋ ਕਿ ਵਿੰਡੋ ਦੇ ਥੱਲੇ ਹਾਲਤ ਬਾਰੇ ਜਾਣਕਾਰੀ ਦੇਣ ਲਈ ਪੱਟੀ ਹੈ।</p> ਡੌਕੂਮੈਂਟ ਦੀ ਪਰਿੰਟ ਝਲਕ ਵੇਖਾਓ ਮੇਨੂ-ਪੱਟੀ ਵੇਖਾਓ ਜਾਂ ਓਹਲੇ ਹਾਲਤ-ਪੱਟੀ ਵੇਖਾਓ ਜਾਂ ਓਹਲੇ ਟੂਲਬਾਰ ਵੇਖਾਓ ਜਾਂ ਓਹਲੇ ਐਪਲੀਕੇਸ਼ਨ ਭਾਸ਼ਾ ਬਦਲੋ(&L)... ਅੱਜ ਦਾ ਟਿੱਪ(&D) ਆਖਰੀ ਕਾਰਵਾਈ ਫੇਰ ਕਰੋ ਡੌਕੂਮੈਂਟ ਨੂੰ ਇਸ ਦੇ ਅਸਲ ਆਕਾਰ ਵਿੱਚ ਵੇਖੋ ਇਹ ਕੀ ਹੈ?(&T) ਤੁਹਾਨੂੰ ਸੰਰਚਨਾ ਸੰਭਾਲਣ ਦੀ ਮਨਜ਼ੂਰ ਨਹੀਂ ਤੁਹਾਨੂੰ ਸੰਭਾਲਣ ਤੋਂ ਪਹਿਲਾਂ ਪਰਮਾਣਿਕਤਾ ਲਈ ਪੁੱਛਿਆ ਜਾਵੇਗਾ ਜ਼ੂਮ ਇਨ(&I) ਜ਼ੂਮ ਆਉਟ(&O) ਵਿੰਡੋ ਵਿੱਚ ਪੇਜ਼ ਉਚਾਈ ਲਈ ਫਿੱਟ ਕਰਨ ਲਈ ਜ਼ੂਮ ਵਿੰਡੋ ਵਿੱਚ ਪੇਜ਼ ਨੂੰ ਫਿੱਟ ਕਰਨ ਲਈ ਜ਼ੂਮ ਕਰੋ ਵਿੰਡੋ ਵਿੱਚ ਪੇਜ਼ ਚੌੜਾਈ ਲਈ ਫਿੱਟ ਕਰਨ ਲਈ ਜ਼ੂਮ ਪਿੱਛੇ(&B) ਅੱਗੇ(&F) ਘਰ(&H) ਮੱਦਦ(&H) ਬਿਨਾਂ ਨਾਂ 