��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  �   �  �   �     �	  ,   �	     �	     �	  4   
     A
     T
  P   e
  ,   �
  	   �
  .   �
       q   3  �   �  P   e                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-15 20:04+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2003-2007 Fredrik Höglund <qt>ਕੀ ਤੁਸੀਂ ਕਰਸਰ ਥੀਮ <i>%1</i> ਨੂੰ ਹਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ?<br />ਇਹ ਥੀਮ ਰਾਹੀਂ ਇੰਸਟਾਲ ਸਭ ਫਾਈਲਾਂ ਨੂੰ ਹਟਾ ਦੇਵੇਗਾ।</qt> <qt>ਤੁਸੀਂ ਇਸ ਸਮੇਂ ਵਰਤ ਰਹੇ ਥੀਮ ਨੂੰ ਹਟਾ ਨਹੀਂ ਸਕਦੇ ਹੋ।<br />ਤੁਹਾਨੂੰ ਪਹਿਲਾਂ ਹੋਰ ਥੀਮ ਲਈ ਸਵਿੱਚ ਕਰਨਾ ਪਵੇਗਾ।</qt> ਤੁਹਾਡੇ ਆਈਕਾਨ ਥੀਮ ਫੋਲਡਰ ਵਿੱਚ ਥੀਮ %1 ਪਹਿਲਾਂ ਹੀ ਮੌਜੂਦ ਹੈ। ਕੀ ਤੁਸੀਂ ਇਸ ਨੂੰ ਤਬਦੀਲ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ? ਪੁਸ਼ਟੀ ਕਰਸਰ ਸੈਟਿੰਗ ਬਦਲੋ ਕਰਸਰ ਥੀਮ ਵੇਰਵਾ ਸੁੱਟੋ ਜਾਂ ਟਾਇਪ ਥੀਮ URL aalam@users.sf.net Fredrik Höglund ਇੰਟਰਨੈੱਟ ਤੋਂ ਨਵੀਂ ਰੰਗ ਸਕੀਮ ਲਵੋ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ਨਾਂ ਥੀਮ ਉੱਤੇ ਲਿਖਣਾ ਹੈ? ਥੀਮ ਹਟਾਓ ਫਾਈਲ %1 ਇੱਕ ਠੀਕ ਕਰਸਰ ਥੀਮ ਅਕਾਇਵ ਨਹੀਂ ਜਾਪਦੀ ਹੈ। ਕਰਸਰ ਥੀਮ ਅਕਾਇਵ ਡਾਊਨਲੋਡ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ ਹੈ। ਚੈੱਕ ਕਰੋ ਕਿ ਐਡਰੈੱਸ %1 ਸਹੀਂ ਹੈ। ਕਰਸਰ ਥੀਮ ਅਕਾਇਵ %1 ਲੱਭਣ ਤੋਂ ਅਸਫਲ। 