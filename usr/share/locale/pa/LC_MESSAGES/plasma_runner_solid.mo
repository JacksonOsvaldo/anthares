��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �  &   T  ,   {  P   �  �   �  �   �  �   �	  �   O
  �   "       #        ;     H  	   b     l     |     �  /   �  -   �                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-01 11:41+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 ਕੰਨਟੇਨਰ ਲਾਕ ਰੋ ਮੀਡਿਅਮ ਬਾਹਰ ਕੱਢੋ ਜੰਤਰ ਲੱਭੋ, ਜਿਸ ਦਾ ਨਾਂ ਮਿਲਦਾ ਹੈ :q: ਸਭ ਜੰਤਰਾਂ ਦੀ ਲਿਸਟ ਅਤੇ ਉਹਨਾਂ ਨੂੰ ਮਾਊਂਟ, ਅਣ-ਮਾਊਂਟ ਕਰਨ ਜਾਂ ਬਾਹਰ ਕੱਢਣਾ ਮਨਜ਼ੂਰ। ਸਭ ਜੰਟਰਾਂ ਦੀ ਲਿਸਟ, ਜੋ ਕਿ ਬਾਹਰ ਕੱਢੇ ਜਾ ਸਕਦੇ ਹਨ ਅਤੇ ਉਹਨਾਂ ਨੂੰ ਬਾਹਰ ਕੱਢਣ ਦੀ ਇਜ਼ਾਜ਼ਤ। ਸਭ ਜੰਤਰਾਂ ਦੀ ਲਿਸਟ, ਜੋ ਮਾਊਂਟ ਕੀਤੇ ਜਾ ਸਕਦੇ ਹਨ ਅਤੇ ਉਹਨਾਂ ਨੂੰ ਮਾਊਂਟ ਕਰਨਾ ਮਨਜ਼ੂਰ। ਸਭ ਜੰਤਰਾਂ ਦੀ ਲਿਸਟ, ਜੋ ਅਣ-ਮਾਊਂਟ ਕੀਤੇ ਜਾ ਸਕਦੇ ਹਨ ਅਤੇ ਉਹਨਾਂ ਨੂੰ ਅਣ-ਮਾਊਂਟ ਕਰਨਾ ਮਨਜ਼ੂਰ। ਸਭ ਇੰਕ੍ਰਿਪਟ ਕੀਤੇ ਜੰਤਰਾਂ ਦੀ ਲਿਸਟ ਹੈ, ਜੋ ਕਿ ਲਾਕ ਕੀਤੇ ਜਾ ਸਕਦੇ ਹਨ ਅਤੇ ਉਹਨਾਂ ਨੂੰ ਲਾਕ ਕਰਨ ਦੀ ਮਨਜ਼ੂਰੀ। ਸਭ ਇੰਕ੍ਰਿਪਟ ਕੀਤੇ ਜੰਤਰਾਂ ਦੀ ਲਿਸਟ ਹੈ, ਜੋ ਕਿ ਅਣਲਾਕ ਕੀਤੇ ਜਾ ਸਕਦੇ ਹਨ ਅਤੇ ਉਹਨਾਂ ਨੂੰ ਅਣ-ਲਾਕ ਕਰਨ ਦੀ ਮਨਜ਼ੂਰੀ। ਜੰਤਰ ਮਾਊਟ ਕਰੋ ਜੰਤਰ ਬਾਹਰ ਕੱਢੋ ਲਾਕ ਮਾਊਂਟ ਅਣ-ਲਾਕ ਅਣ-ਮਾਊਂਟ ਕੰਨਟੇਨਰ ਅਣਲਾਕ ਕਰੋ ਜੰਤਰ ਅਣ-ਮਾਊਂਟ ਕਰੋ 