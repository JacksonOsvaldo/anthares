��    $      <  5   \      0  &   1  �   X     �     �                     )     7     D     U      e  	   �  \   �  c   �     Q     b     n     �     �     �     �     �     �     �     �     �       	     C     j   b  E   �               2  �  @  &   �    	     #
  &   C
  	   j
     t
     �
     �
     �
     �
     �
     �
       �   *  �   �  &   x     �  ,   �     �  =   �  	   3     =     W  /   c     �     �  ,   �  &   �     �  �     �   �  �   x       =   *  &   h                                 
                  #                           "            	                            !                                       $                       (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box If you enable this option, KDE Applications will show small icons alongside most menu items. If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Preview Radio button Ralf Nolden Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Toolbars Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2014-03-29 18:55-0500
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>ਸਟਾਈਲ</h1>ਇਹ ਮੋਡੀਊਲ ਤਹਾਨੂੰ ਯੂਜ਼ਰ ਇੰਟਰਫੇਸ ਇਕਾਈਆਂ, ਜਿਵੇਂ ਕਿ ਵਿਦਜੈੱਟ ਸਟਾਈਲ ਅਤੇ ਪਰਭਾਵ ਆਦਿ ਸੋਧਣ ਲਈ ਸਹਾਇਕ ਹੈ। ਐਪਲੀਕੇਸ਼ਨ(&A) ਫਾਈਨ ਟਿਊਨਿੰਗ(&F) ਬਟਨ ਚੋਣ ਬਕਸਾ ਕੰਬੋ ਬਕਸਾ ਸੰਰਚਨਾ(&f)... ਸੰਰਚਨਾ %1 Daniel Molkentin ਵੇਰਵਾ: %1 aalam@users.sf.net ਗਰੁੱਪ ਬਾਕਸ ਜੇ ਇਹ ਚੋਣ ਯੋਗ ਕੀਤੀ ਤਾਂ KDE ਕਾਰਜ ਬਹੁਤੇ ਮੇਨੂ ਆਈਟਮਾਂ ਨਾਲ ਛੋਟੇ ਵੇਖਾਉਣਗੇ। ਜੇ ਇਹ ਚੋਣ ਯੋਗ ਕੀਤੀ ਤਾਂ KDE ਕਾਰਜ ਕੁਝ ਖਾਸ ਬਟਨਾਂ ਨਾਲ ਛੋਟੇ ਵੇਖਾਉਣਗੇ। KDE ਸਟਾਇਲ ਮੋਡੀਊਲ Karol Szwed ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ਟੈਕਸਟ ਨਹੀਂ ਕੋਈ ਵੇਰਵਾ ਉਪਲੱਬਧ ਨਹੀ ਹੈ ਝਲਕ ਰੇਡੀਓ ਬਟਨ Ralf Nolden ਮੇਨੂ 'ਚ ਆਈਕਾਨ ਵੇਖੋ: ਟੈਬ 1 ਟੈਬ 2 ਆਈਕਾਨ ਹੇਠਾਂ ਅੱਖਰ ਆਈਕਾਨ ਨਾਲ ਅੱਖਰ ਕੇਵਲ ਅੱਖਰ ਇਸ ਸਟਾਇਲ ਲਈ ਸੰਰਚਨਾ ਡਾਈਲਾਗ ਲੋਡ ਕਰਨ ਦੌਰਾਨ ਇੱਕ ਗਲਤੀ ਆਈ ਹੈ। ਇਹ ਏਰੀਆ ਮੌਜੂਦਾ ਚੁਣੇ ਸਟਾਈਥ ਦੀ ਝਲਕ ਬਿਨਾਂ ਪੂਰੇ ਡੈਸਕਟਾਪ ਉੱਤੇ ਲਾਗੂ ਕੀਤੇ ਵੇਖਾਉਦਾ ਹੈ। ਇਹ ਪੇਜ਼ ਤੁਹਾਨੂੰ ਵਿਦਜੈੱਟ ਸਟਾਈਲ ਚੋਣ ਵੇਰਵੇ ਸਮੇਤ ਕਰਨ ਲਈ ਸਹਾਇਕ ਹੈ ਟੂਲਬਾਰ ਡਾਈਲਾਗ ਲੋਡ ਕਰਨ ਤੋਂ ਅਸਫਲ ਵਿਦਜੈੱਟ ਸਟਾਇਲ: 