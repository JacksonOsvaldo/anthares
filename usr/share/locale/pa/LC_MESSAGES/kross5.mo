��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     '     4     S  /   c     �  I   �     �     �     	     #	  	   6	  /   @	     p	  2   }	  e   �	  `   
  K   w
  9   �
     �
            R   <  O   �  
   �  5   �  >         _  2   l     �  6   �     �  S   �     D  m   U     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-07-08 14:39-0500
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 ਲੇਖਕ Copyright 2006 Sebastian Sauer Sebastian Sauer ਚਲਾਉਣ ਲਈ ਸਕ੍ਰਿਪਟ। ਆਮ ਇੱਕ ਨਵੀਂ ਸਕ੍ਰਿਪਟ ਸ਼ਾਮਲ ਕਰੋ। ਸ਼ਾਮਲ... ਰੱਦ ਕਰਨਾ? ਟਿੱਪਣੀ: aalam@users.sf.net ਸੋਧ ਚੁਣੀ ਸਕ੍ਰਿਪਟ ਸੋਧ। ਸੋਧ... ਚੁਣੀ ਸਕ੍ਰਿਪਟ ਚਲਾਓ। ਇੰਟਰਪਰੇਟਰ  "%1" ਲਈ ਸਕ੍ਰਿਪਟ ਬਣਾਉਣ ਲਈ ਫੇਲ੍ਹ scriptfile "%1" ਲਈ ਇੰਟਰਪਰੇਟਰ ਖੋਜਣ ਲਈ ਫੇਲ੍ਹ ਹੈ। ਇੰਟਰਪਰੇਟਰ "%1" ਲੋਡ ਕਰਨ ਲਈ ਫੇਲ੍ਹ scriptfile "%1" ਲੱਭਣ ਵਿੱਚ ਫੇਲ੍ਹ ਫਾਈਲ: ਆਈਕਾਨ: ਇੰਟਰਪ੍ਰੇਟਰ: ਰੂਬੀ ਇੰਟਰਪਰੇਟਰ ਦਾ ਸੁਰੱਖਿਆ ਲੈਵਲ ਅ.ਸ.ਆਲਮ. ੨੦੦੪-੨੦੧੪
http://code.google.com/p/gurmukhi/ ਨਾਂ: ਕੋਈ "%1" ਫੰਕਸ਼ਨ ਨਹੀਂ ਹੈ ਕੋਈ "%1" ਇੰਟਰਪਰੇਟਰ ਨਹੀਂ ਹੈ ਹਟਾਓ ਚੁਣੀ ਸਕ੍ਰਿਪਟ ਹਟਾਓ। ਚਲਾਓ Scriptfile "%1" ਮੌਜੂਦ ਨਹੀਂ ਹੈ। ਰੋਕੋ ਚੁਣੀ ਸਕ੍ਰਿਪਟ ਨੂੰ ਚੱਲਣ ਤੋਂ ਰੋਕੋ। ਟੈਕਸਟ: ਕੇ-ਰੋਸ ਸਕ੍ਰਿਪਟਾਂ ਚਲਾਉਣ ਲਈ KDE ਐਪਲੀਕੇਸ਼ਨ ਹੈ। ਕੇ-ਰੋਸ 