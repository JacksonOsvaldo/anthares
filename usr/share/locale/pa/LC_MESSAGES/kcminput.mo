��    )      d  ;   �      �     �     �     �  
   �     �     �     �  /   �  '         ?  �   `     :     Q  9   e     �     �  
   �     �     �     �     �  E   �     =     T     i      z     �     �     �     �     �     �     �     
           3     E     Q     k     y  �  �  !   3	     U	  !   [	      }	     �	  -   �	     �	  n   �	  r   R
  )   �
  �  �
  3   �  *   �  �        �     �     �          &     =     Z  �   j  -   '  *   U  *   �     �     �     �  (   �       7     ,   W     �  /   �  2   �  $   �       -   8     f     �             	                      )         !         '                                               $               "   &   %                              (               #                      
            line  lines  msec  pixel  pixels  pixel/sec  x &Acceleration delay: &General &Move pointer with keyboard (using the num pad) &Single-click to open files and folders (c) 1997 - 2005 Mouse developers <h1>Mouse</h1> This module allows you to choose various options for the way in which your pointing device works. Your pointing device may be a mouse, trackball, or some other hardware that performs a similar function. Acceleration &profile: Acceleration &time: Activates and opens a file or folder with a single click. Advanced Bernd Gehrmann Brad Hards Brad Hughes Button Order David Faure Dirk A. Mueller Dou&ble-click to open files and folders (select icons on first click) Double click interval: Drag start distance: Drag start time: EMAIL OF TRANSLATORSYour emails Icons Le&ft handed Ma&ximum speed: Mouse Mouse wheel scrolls by: NAME OF TRANSLATORSYour names Patrick Dowler Pointer acceleration: Pointer threshold: R&epeat interval: Ralf Nolden Re&verse scroll direction Righ&t handed Rik Hemsley Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-05 03:17+0100
PO-Revision-Date: 2012-01-15 20:04+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
  ਲਾਈਨ  ਲਾਈਨਾਂ  msec  ਪਿਕਸਲ  ਪਿਕਸਲ  ਪਿਕਸਲ/ਸਕਿੰਟ  x ਐਕਸਲੇਸ਼ਨ ਡਿਲੇਅ(&A): ਸਧਾਰਨ(&G) ਕੀਬੋਰਡ ਨਾਲ ਮਾਊਸ ਏਧਰ-ਓਧਰ (ਅੰਕੀ ਖਾਕਾ ਵਰਤ ਕੇ)(&M) ਫਾਈਲਾਂ ਅਤੇ ਫੋਲਡਰ ਖੋਲਣ ਲਈ ਇੱਕ ਵਾਰ ਕਲਿੱਕ ਕਰੋ(&S) (c) 1997 - 2005 ਮਾਊਸ ਖੋਜੀ <h1>ਮਾਊਸ</h1> ਇਹ ਮੋਡੀਊਲ ਤੁਹਾਨੂੰ ਆਪਣੇ ਪੁਆਇੰਟਿੰਗ ਜੰਤਰ ਲਈ ਚੋਣਾਂ ਸੈੱਟ ਕਰਨ ਲਈ ਮੱਦਦਗਾਰ ਹੈ। ਤੁਹਾਡਾ ਪੁਆਇੰਟਿੰਗ ਜੰਤਰ ਟਰੈਕਬਾਲ, ਜਾਂ ਕੋਈ ਹੋਰ ਹਾਰਡਵੇਅਰ ਹੋ ਸਕਦਾ ਹੈ, ਜੋ ਕਿ ਇੰਝ ਦਾ ਕੰਮ ਕਰ ਸਕਦਾ ਹੈ। ਐਕਸਲੇਸ਼ਨ ਪਰੋਫਾਈਲ(&p): ਐਕਸਲੇਸ਼ਨ ਟਾਈਮ(&t): ਇੱਕ ਫਾਈਲ ਜਾਂ ਫੋਲਡਰ ਨੂੰ ਇੱਕ ਵਾਰ ਕਲਿੱਕ ਕਰਨ ਨਾਲ ਐਕਟੀਵੇਟ ਕਰੋ ਅਤੇ ਖੋਲ੍ਹੋ। ਤਕਨੀਕੀ ਬਰੰਡ ਜਿਹਰਮਨ ਬਰੈਡ ਹਾਰਡਸ ਬਰਡ ਹੱਗਸ ਬਟਨ ਕ੍ਰਮ ਡੇਵਿਡ ਫੂਰੀ Dirk A. Mueller ਫਾਈਲਾਂ ਅਤੇ ਫੋਲਡਰ ਖੋਲਣ ਲਈ ਦੋ-ਵਾਰ ਕਲਿੱਕ ਕਰੋ (ਇੱਕ ਵਾਰ ਕਲਿੱਕ ਨਾਲ ਆਈਕਾਨ ਚੁਣੋ)(&b) ਡਬਲ ਕਲਿੱਕ ਇੰਟਰਵਲ: ਸੁੱਟਣ ਸ਼ੁਰੂ ਦੂਰੀ: ਚੁੱਕਣ ਸ਼ੁਰੂ ਸਮਾਂ: aalam@users.sf.net ਆਈਕਾਨ ਖੱਬੇ ਹੱਥ(&f) ਵੱਧੋ-ਵੱਧ ਸਪੀਡ(&x): ਮਾਊਸ ਮਾਊਸ ਪਹੀਆ ਸਰਕੋਲ ਹੋਵੇ: ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ Patrick Dowler ਪੁਆਇੰਟਰ ਐਕਸਲੇਸ਼ਨ: ਪੁਆਇੰਟਰ ਥਰੈਸ਼ਹੋਲਡ: ਰਪੀਟ ਇੰਟਰਵਲ(&e): ਰਲਫ ਨੋਲਡਿਨ ਉਲੱਟ ਸਕਰੋਲ ਦਿਸ਼ਾ(&v) ਸੱਜੇ ਹੱਥ(&t) ਰਿਕ ਹਿਮਸਲੇ 