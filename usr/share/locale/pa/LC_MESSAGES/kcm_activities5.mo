��    )      d  ;   �      �     �     �  )   �     �     	          8     N  #   n     �  
   �     �     �  %   �  +   	     5     J     ]     j     �     �     �     �     �     �     �     �     �            .        K  F   f  (   �  	   �  	   �  	   �  '   �  7     "   T  �  w  !   	  /   7	  ;   g	     �	     �	     �	     �	  +   �	  6   
  )   N
     x
  /   �
  )   �
  J   �
  �   6  7   �  ,   �     (  =   9  -   w  *   �  6   �  C        K     R  #   b  
   �  @   �  	   �     �  s   �  V   i  �   �  N   y     �     �     �       !        -                   #                                 $                       )                                               	   "   (   !             %           
                        &            '        &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-08-08 14:48-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ਯਾਦ ਨਾ ਰੱਖੋ(&D) ਸਰਗਰਮੀਆਂ ਨੂੰ ਵੇਖੋ ਸਰਗਰਮੀਆਂ ਨੂੰ ਵੇਖੋ (ਉਲਟ) ਲਾਗੂ ਕਰੋ ਰੱਦ ਕਰੋ ...ਬਦਲੋ ਬਣਾਓ ਸਰਗਰਮੀ ਸੈਟਿੰਗਾਂ ਨਵੀਂ ਸਰਗਰਮੀ ਨੂੰ ਬਣਾਓ ਸਰਗਰਮੀ ਨੂੰ ਹਟਾਓ ਸਰਗਰਮੀਆਂ ਸਰਗਰਮੀ ਦੀ ਜਾਣਕਾਰੀ ਸਰਗਰਮੀ ਨੂੰ ਬਦਲੋ ਕੀ ਤੁਸੀਂ %1 ਹਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ? ਇਸ ਸੂਚੀ ਤੋਂ ਬਿਨਾਂ ਸਾਰੀਆਂ ਐਪਲੀਕੇਸ਼ਨਾਂ ਉੱਤੇ ਪਾਬੰਦੀ ਲਗਾਓ ਤਾਜ਼ਾ ਅਤੀਤ ਨੂੰ ਸਾਫ਼ ਕਰੋ ...ਸਰਗਰਮੀ ਨੂੰ ਬਣਾਓ ਵੇਰਵਾ: ਸਾਰੀਆਂ ਐਪਲੀਕੇਸ਼ਨਾਂ ਲਈ(&l)  ਇੱਕ ਦਿਨ ਨੂੰ ਭੁੱਲੋ ਹਰ ਚੀਜ਼ ਨੂੰ ਭੁਲਾਓ ਪਿਛਲੇ ਘੰਟੇ ਨੂੰ ਭੁੱਲੋ ਪਿਛਲੇ ਦੋ ਘੰਟਿਆਂ ਨੂੰ ਭੁੱਲੋ ਆਮ ਆਈਕਾਨ ਅਤੀਤ ਨੂੰ ਰੱਖੋ ਨਾਂ: ਕੇਵਲ ਖਾਸ ਐਪਲੀਕੇਸ਼ਨਾਂ ਹੀ(&n) ਹੋਰ ਪਰਦੇਦਾਰੀ ਪ੍ਰਾਈਵੇਟ - ਇਸ ਸਰਗਰਮੀ ਲਈ ਵਰਤੋਂ ਨੂੰ ਯਾਦ ਨਾ ਰੱਖੋ ਖੁੱਲ੍ਹੇ ਡੌਕੂਮੈਂਟਾਂ ਨੂੰ ਯਾਦ ਰੱਖੋ: ਹਰ ਸਰਗਰਮੀ ਲਈ ਮੌਜੂਦਾ ਵਰਚੁਅਲ ਡੈਸਕਟਾਪ ਨੂੰ ਯਾਦ ਰੱਖੋ (ਮੁੜ-ਚਾਲੂ ਕਰਨ ਦੀ ਲੋੜ ਹੈ) ਇਸ ਸਰਗਰਮੀ ਉੱਤੇ ਜਾਣ ਲਈ ਸ਼ਾਰਟਕੱਟ: ਸ਼ਾਰਟਕੱਟ ਬਦਲਣਾ ਵਾਲਪੇਪਰ ਲਈ  ਮਹੀਨਾ  ਮਹੀਨੇ ਹਮੇਸ਼ਾ 