��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %  %   �  %   �  /      G   P  ,   �  1   �     �               (     ?     U  Z   s  S   �  X   "     {     �     �     �  D   �  <   	  A   U	  <   �	  /   �	  2   
                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-27 08:16+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.1
 %1 ਫਾਈਲ %1 ਫਾਈਲਾਂ %1 ਫੋਲਡਰ %1 ਫੋਲਡਰ %2 ਵਿੱਚ %1 ਕਾਰਵਾਈ ਹੋਈ %2 ਵਿੱਚੋਂ %1 ਕਾਰਵਾਈ %3/s ਵਿੱਚ ਹੋਈ %1 ਉੱਤੇ ਕਾਰਵਾਈ ਹੋਈ %1 ਕਾਰਵਾਈ %2/s ਉੱਤੇ ਹੋਈ ਦਿੱਖ ਰਵੱਈਆ ਰੱਦ ਕਰੋ ਸਾਫ਼ ਕਰੋ ਸੰਰਚਨਾ... ਖਤਮ ਹੋਏ ਜਾਬ ਜਾਰੀ ਫਾਈਲ ਟਰਾਂਸਫਰ/ਜਾਬ ਦੀ ਲਿਸਟ ( kuiserver) ਇਨ੍ਹਾਂ ਨੂੰ ਵੱਖਰੀ ਲਿਸਟ ਵਿੱਚ ਭੇਜੋ ਉਨ੍ਹਾਂ ਨੂੰ ਇੱਕ ਵੱਖਰੀ ਲਿਸਟ 'ਚ ਭੇਜੋ। ਵਿਰਾਮ ਉਹ ਹਟਾਓ ਉਹ ਹਟਾਓ। ਮੁੜ-ਪ੍ਰਾਪਤ ਇੱਕ ਲਿਸਟ ਵਿੱਚ ਸਭ ਜਾਬ ਵੇਖਾਓ ਸਭ ਜਾਬ ਇੱਕ ਲੜੀ 'ਚ ਵੇਖਾਓ। ਇੱਕ ਟਰੀ ਵਿੱਚ ਸਭ ਜਾਬ ਵੇਖਾਓ ਸਭ ਜਾਬ ਇੱਕ ਟਰੀ 'ਚ ਵੇਖਾਓ। ਵੱਖਰੇ ਝਰੋਖੇ ਵੇਖਾਓ ਵੱਖਰੇ ਝਰੋਖੇ ਵੇਖਾਓ। 