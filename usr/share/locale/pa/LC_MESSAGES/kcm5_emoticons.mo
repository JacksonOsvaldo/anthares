��          �   %   �      p     q     �     �  +   �      �     �                ,     J      j     �     �  3   �  	   �     �  )   �  %     5   A     w     �     �     �     �  +   �  (     3   /  �  c  =     "   T     w  T   �  ]   �  ,   =     j  2   �  [   �  D   	     ^	     q	     �	  �   �	     
  %   2
  E   X
  S   �
  �   �
  7   �  )   �            ,   '  Z   T  J   �  z   �                                                                            
                        	                       %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet Install a theme archive file you already have locally NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2013-06-18 10:21-0500
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 ਥੀਮ ਪਹਿਲਾਂ ਹੀ ਮੌਜੂਦ ਹੈ ਈਮੋਸ਼ਨ ਸ਼ਾਮਲ ਸ਼ਾਮਲ... ਬਣਾਉਣ ਲਈ ਈਮੋਸ਼ਨ ਥੀਮ ਦੀ ਕਿਸਮ ਚੁਣੋ ਈਮੋਸ਼ਨ ਥੀਮ ਇੰਸਟਾਲ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ ਨਵਾਂ ਈਮੋਸ਼ਨ ਬਣਾਓ ਈਮੋਸ਼ਨ ਹਟਾਓ ਨਵਾਂ ਈਮੋਸ਼ਨ ਡਿਜ਼ਾਇਨ ਕੀ ਤੁਸੀਂ %1 ਨੂੰ ਵੀ ਹਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ? ਸੁੱਟ ਜਾਂ ਈਮੋਸ਼ਨ ਥੀਮ URL ਲਿਖੋ aalam@users.sf.net ਈਮੋਸ਼ਨ ਸੋਧ ਸੋਧ... ਈਮੋਸ਼ਨ ਥੀਮ ਨੂੰ ਲੋਕਲ ਫਾਈਲਾਂ ਤੋਂ ਇੰਸਟਾਲ ਕਰਨਾ ਪਵੇਗਾ। ਈਮੋਸ਼ਨ ਈਮੋਸ਼ਨ ਮੈਨੇਜਰ ਨਵੇਂ ਈਮੋਸ਼ਨ ਥੀਮ ਲਈ ਨਾਂ ਦਿਓ: ਇੰਟਰਨੈੱਟ ਤੋਂ ਨਵਾਂ ਆਈਕਾਨ ਥੀਮ ਲਵੋ ਇੱਕ ਥੀਮ ਅਕਾਇਵ ਫਾਈਲ, ਜੋ ਤੁਹਾਡੇ ਕੋਲ ਪਹਿਲਾਂ ਹੀ ਲੋਕਲੀ ਹੈ, ਤੋਂ ਇੰਸਟਾਲ ਕਰੋ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ (A S Alam) ਨਵਾਂ ਈਮੋਸ਼ਨ ਥੀਮ ਹਟਾਓ ਥੀਮ ਹਟਾਓ ਚੁਣੇ ਈਮੋਸ਼ਨ ਹਟਾਓ ਚੁਣੇ ਈਮੋਸ਼ਨ ਨੂੰ ਆਪਣੀ ਡਿਸਕ ਤੋਂ ਹਟਾਓ ਚੁਣਿਆ ਥੀਮ ਆਪਣੀ ਡਿਸਕ ਤੋਂ ਹਟਾਓ ਇਸ ਨਾਲ ਤੁਹਾਡੀ ਡਿਸਕ ਤੋਂ ਚੁਣਿਆ ਥੀਮ ਹਟਾਇਆ ਜਾਵੇਗਾ। 