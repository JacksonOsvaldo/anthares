��    K      t  e   �      `     a     p     �     �     �     �     �  
   �     �     �            	   (     2     F     [  ,   g  	   �     �  
   �     �     �     �     	          +     ;     S     [  	   {     �     �     �     �     �     �  	   �  
   �     �     �  
   �      	     	     	     +	     <	     J	     \	     r	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     
     ,
     E
     Z
  	   p
  ,   z
     �
     �
     �
     �
     �
     �
     �
            #   .  �  R  /   �  &      W   G  "   �     �     �     �     �          +     ;     W     t  2   �  2   �      �  d        v  7   �      �  K   �  <   1  B   n  2   �  )   �  /     B   >     �     �  "   �     �  /   �  	      -   
     8     L     h  $        �     �     �      �  %        6  "   S     v  +   �  ,   �  @   �  )   *  J   T     �  	   �      �     �  )   �     !  B   .  6   q  ?   �  <   �  B   %     h  l   |     �  3     6   :  /   q     �  (   �  3   �  #     Q   2  f   �         >                  *   %       H   B       $      ?   D   J          (         "               
   <       9           5         F           )      !       2                       @      '   ,   ;   +   7            0              4   :             1              .   	   6   &      3   E   K   /             #      8   C   =       -      A   I             G           Add to Desktop Add to Favorites Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Lock screen Logout Name (Description) Name only Open with: Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show applications as: Show recent applications Show recent contacts Show recent documents Shut Down Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2016-08-08 15:20-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ਡੈਸਕਟਾਪ ਵਿੱਚ ਜੋੜੋ ਪਸੰਦ ਵਿੱਚ ਜੋੜੋ ਖੋਜ ਨਤੀਜਿਆਂ ਨੂੰ ਤਲ ਉੱਤੇ ਇਕਸਾਰ ਕਰੋ ਸਭ ਐਪਲੀਕੇਸ਼ਨ %1 (%2) ਐਪਲੀਕੇਸ਼ਨਾਂ ਰਵੱਈਆ ਕੈਟਾਗਰੀਆਂ ਕੰਪਿਊਟਰ ਸੰਪਰਕ ਵੇਰਵਾ (ਨਾਂ) ਕੇਵਲ ਵੇਰਵਾ ਦਸਤਾਵੇਜ਼ ...ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਸੋਧੋ ...ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਸੋਧੋ ਸ਼ੈਸ਼ਨ ਖਤਮ ਕਰੋ ਬੁੱਕਮਾਰਕ, ਫਾਇਲਾਂ ਅਤੇ ਈਮੇਲਾਂ ਲਈ ਖੋਜ ਕਰੋ ਪਸੰਦੀਦਾ ਇੱਕ ਪੱਧਰ ਲਈ ਸਮਤਲ ਮੀਨੂ ਸਭ ਨੂੰ ਭੁਲਾਓ ਸਾਰੀਆਂ ਐਪਲੀਕੇਸ਼ਨਾਂ ਨੂੰ ਭੁਲਾਓ ਸਾਰੇ ਸੰਪਰਕਾਂ ਨੂੰ ਭੁਲਾਓ ਤਾਜ਼ਾ ਦਸਤਾਵੇਜ਼ਾਂ ਨੂੰ ਭੁਲਾਓ ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਭੁਲਾਓ ਸੰਪਰਕ ਨੂੰ ਭੁਲਾਓ ਦਸਤਾਵੇਜ਼ ਨੂੰ ਭੁਲਾਓ ਤਾਜ਼ਾ ਦਸਤਾਵੇਜ਼ਾਂ ਨੂੰ ਭੁਲਾਓ ਆਮ %1 (%2) ਹਾਈਬਰਨੇਟ ਕਰੋ %1 ਨੂੰ ਲੁਕਾਓ ਐਪਲੀਕੇਸ਼ਨ ਓਹਲੇ ਕਰੋ ਲਾਕ ਸਕਰੀਨ ਨੂੰ ਲਾਕ ਕਰੋ ਲਾਗ ਆਉਟ ਨਾਂ (ਵੇਰਵਾ) ਕੇਵਲ ਨਾਂ ਇਸ ਨਾਲ ਖੋਲ੍ਹੋ: ਥਾਵਾਂ ਪਾਵਰ / ਸ਼ੈਸ਼ਨ ਵਿਸ਼ੇਸ਼ਤਾਵਾਂ ਮੁੜ-ਚਾਲੂ ਕਰੋ ਤਾਜ਼ਾ ਐਪਲੀਕੇਸ਼ਨ ਤਾਜ਼ਾ ਸੰਪਰਕ ਤਾਜ਼ਾ ਦਸਤਾਵੇਜ਼ ਤਾਜ਼ਾ ਵਰਤੇ ਹਟਾਉਣਯੋਗ ਸਟੋਰੇਜ਼ ਪਸੰਦ ਵਿੱਚੋਂ ਹਟਾਓ ਕੰਪਿਊਟਰ ਨੂੰ ਮੁੜ ਚਾਲੂ ਕਰੋ ...ਕਮਾਂਡ ਨੂੰ ਚਲਾਓ ਕਮਾਂਡ ਜਾਂ ਖੋਜ ਕਿਊਰੀ ਨੂੰ ਚਲਾਓ ਸ਼ੈਸ਼ਨ ਸੰਭਾਲੋ ਖੋਜ ਖੋਜ ਦੇ ਨਤੀਜੇ ...ਖੋਜ '%1' ਲਈ ਖੋਜ ਜਾਰੀ ਹੈ ਸ਼ੈਸ਼ਨ ...ਸੰਪਰਕ ਜਾਣਕਾਰੀ ਨੂੰ ਵੇਖਾਓ ਐਪਲੀਕੇਸ਼ਨ ਵਜੋਂ ਵੇਖਾਓ: ਤਾਜ਼ਾ ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਵੇਖਾਓ ਤਾਜ਼ਾ ਸੰਪਰਕਾਂ ਨੂੰ ਵੇਖਾਓ ਤਾਜ਼ਾ ਦਸਤਾਵੇਜ਼ਾਂ ਨੂੰ ਵੇਖਾਓ ਬੰਦ ਕਰੋ ਵੱਖਰੇ ਵਰਤੋਂਕਾਰ ਵਜੋਂ ਸਮਾਂਤਰ ਸ਼ੈਸ਼ਨ ਸ਼ੁਰੂ ਕਰੋ ਸਸਪੈਂਡ ਕਰੋ ਰੈਮ ਉੱਤੇ ਸਸਪੈਂਡ ਕਰੋ ਡਿਸਕ ਉੱਤੇ ਸਸਪੈਂਡ ਕਰੋ ਵਰਤੋਂਕਾਰ ਨੂੰ ਬਦਲੋ ਸਿਸਟਮ ਸਿਸਟਮ ਕਾਰਵਾਈਆਂ ਕੰਪਿਊਟਰ ਨੂੰ ਬੰਦ ਕਰੋ ਖੋਜਣ ਲਈ ਲਿਖੋ। '%1' ਵਿੱਚ ਐਪਲੀਕੇਸਨਾਂ ਨੂੰ ਨਾ-ਲੁਕਾਓ ਇਸ ਅਧੀਨ-ਮੇਨੂ ਵਿੱਚ ਐਪਲੀਕੇਸ਼ਨਾਂ ਨੂੰ ਲੁਕਾਓ 