��            )   �      �      �     �     �  -   �  #     #   ?  ?   c  1   �     �  H   �  L   #  V   p     �     �     �     �       2     
   J  )   U  8     #   �  -   �  D   
  6   O  0   �  A   �  =   �  9   7  �  q  I   	     a	  9   }	  \   �	  "   
  "   7
     Z
     z
     �
  .   �
  :   �
          4      J  =   k  I   �     �     	     %  g   9     �  W   �  E     ?   T     �     �     �     �     �        	                    
                                                                                                             %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Information Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2016-04-02 09:49UTC-0600
Last-Translator: A S Alam <aalam@fedoraproject.org>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 ਨੋਟੀਫਿਕੇਸ਼ਨ %1 ਨੋਟੀਫਿਕੇਸ਼ਨ %2 %3 ਵਿੱਚੋਂ %1 %1 ਚੱਲਦੀ ਜਾਬ %1 ਚੱਲਦੇ ਜਾਬ ...ਈਵੈਂਟ ਸੂਚਨਾਵਾਂ ਤੇ ਕਾਰਵਾਈ ਸੰਰਚਨਾ(&C) 10 ਸਕਿੰ ਪਹਿਲਾਂ 30 ਸਕਿੰ ਪਹਿਲਾਂ ਵੇਰਵੇ ਦਿਖਾਓ ਵੇਰਵੇ ਲੁਕਾਓ ਕਾਪੀ ਕਰੋ 1 ਡਾਇ %2 ਵਿੱਚੋਂ %1 ਡਾਇ 1 ਫਾਈਲ %2 ਵਿੱਚੋਂ %1 ਫਾਈਲਾਂ %2 ਵਿੱਚੋਂ %1 ਜਾਣਕਾਰੀ ਜਾਬ ਪੂਰੀ ਹੋਈ ਕੋਈ ਨਵੀਂ ਸੂਚਨਾ ਨਹੀਂ ਹੈ। ਕੋਈ ਨੋਟੀਫਿਕੇਸ਼ਨ ਤੇ ਜਾਬ ਨਹੀਂ ...ਖੋਲ੍ਹੋ %1 (ਵਿਰਾਮ ਹੈ) ਸਭ ਚੁਣੋ ਐਪਲੀਕੇਸ਼ਨ ਅਤੇ ਸਿਸਟਮ ਨੋਟੀਫਿਕੇਸ਼ਨ ਵੇਖਾਓ %1 (%2 ਬਾਕੀ) ਫਾਈਲ ਟਰਾਂਸਫਰ ਅਤੇ ਹੋਰ ਜਾਬ ਟਰੈਕ ਕਰੋ %1 ਮਿੰਟ ਪਹਿਲਾਂ %1 ਮਿੰਟ ਪਹਿਲਾਂ %1 ਦਿਨ ਪਹਿਲਾਂ %1 ਦਿਨ ਪਹਿਲਾਂ ਕੱਲ੍ਹ ਹੁਣੇ ਹੀ %1: %1: ਫੇਲ੍ਹ ਹੈ %1: ਮੁਕੰਮਲ 