��    &      L  5   |      P     Q     W  
   �  +   �  
             %     >      L     m          �  	   �      �  e   �  *   9  H   d     �     �     �  )   �  ;   
  	   F     P  (   o     �     �     �     �               1  	   L     V  #   p     �     �  �  �     	  6  �	     �
  Z   �
     1     J  >   d     �  I   �  %     Q   3     �     �     �  �   �  @   �  �   �     �  ,   �  %   �  u   �  �   `     �  K     V   c  9   �  6   �  6   +  6   b  /   �  9   �  <        @  9   P  E   �  @   �  L              	                 !                     $   
                                          "            %                                             &         #                   msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2014-03-29 18:53-0500
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
  msec <h1>ਮਲਟੀਪਲ ਡੈਸਕਟਾਪ</h1>ਇਹ ਮੋਡੀਊਲ ਵਿੱਚ, ਤੁਸੀਂ ਕਿੰਨੇ ਵਰਚੁਅਲ ਡੈਸਕਟਾਪ ਦੇਖਣੇ ਹਨ ਅਤੇ ਉਨ੍ਹਾਂ ਦੇ ਲੇਬਲ ਕੀ ਹੋਣ, ਦੀ ਸੰਰਚਨਾ ਕਰਨ ਸਕਦੇ ਹੋ। ਐਨੀਮੇਸ਼ਨ: ਗਲੋਬਲ ਸ਼ਾਰਟਕੱਟ "%1" ਡੈਸਕਟਾਪ %2 ਨੂੰ ਦਿਓ ਡੈਸਕਟਾਪ %1 ਡੈਸਕਟਾਪ %1: ਡੈਸਕਟਾਪ ਪਰਭਾਵ ਐਨੀਮੇਸ਼ਨ ਡੈਸਕਟਾਪ ਨਾਂ ਡੈਸਕਟਾਪ ਬਦਲਣਾ ਆਨ-ਸਕਰੀਨ ਵੇਖੋ ਡੈਸਕਟਾਪ ਬਦਲਣਾ ਡੈਸਕਟਾਪ ਨੇਵੀਗੇਸ਼ਨ ਪਾਸਿਓ ਸਮੇਟੋ ਡੈਸਕਟਾਪ ਅੰਤਰਾਲ: aalam@users.sf.net ਇਹ ਚੋਣ ਚਾਲੂ ਕਰਨ ਨਾਲ ਚੁਣੇ ਡੈਸਕਟਾਪ ਨੂੰ ਦਰਸਾਉਂਦੇ ਡੈਸਕਟਾਪ ਲੇਆਉਟ ਦੀ ਝਲਕ ਵੇਖਾਈ ਜਾਵੇਗੀ। ਇੱਥੇ ਡੈਸਕਟਾਪ %1 ਦਾ ਨਾਂ ਦਿਓ ਆਪਣੇ KDE ਡੈਸਕਟਾਪ ਵਿੱਚ ਵੇਖੇ ਜਾਣ ਵੇਲੇ ਵੁਰਚੁਅਲ ਡੈਸਕਟਾਪ ਸੈੱਟ ਕਰ ਸਕਦੇ ਹੋ। ਲੇਆਉਟ ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ਐਨੀਮੇਸ਼ਨ ਨਹੀਂ ਡੈਸਕਟਾਪ %1 ਲਈ ਕੋਈ ਢੁੱਕਵਾਂ ਸ਼ਾਰਟਕੱਟ ਨਹੀਂ ਲੱਭਿਆ ਸ਼ਾਰਟਕੱਟ ਟਕਰਾ: %2 ਡੈਸਕਟਾਪ ਲਈ %1 ਸ਼ਾਰਟਕੱਟ ਸੈੱਟ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ ਸ਼ਾਰਟਕੱਟ ਡੈਸਕਟਾਪ ਲੇਆਉਟ ਇੰਡੀਕੇਟਰ ਵੇਖੋ ਸਭ ਸੰਭਵ ਡੈਸਕਟਾਪ ਲਈ ਸ਼ਾਰਟਕੱਟ ਵੇਖੋ ਇੱਕ ਡੈਸਕਟਾਪ ਹੇਠਾਂ ਜਾਉ ਇੱਕ ਡੈਸਕਟਾਪ ਉੱਤੇ ਜਾਉ ਇੱਕ ਡੈਸਕਟਾਪ ਖੱਬੇ ਜਾਉ ਇੱਕ ਡੈਸਕਟਾਪ ਸੱਜੇ ਜਾਉ ਡੈਸਕਟਾਪ %1 ਉੱਤੇ ਜਾਓ ਅਗਲੇ ਡੈਸਕਟਾਪ ਉੱਤੇ ਜਾਉ ਪਿਛਲੇ ਡੈਸਕਟਾਪ ਉੱਤੇ ਜਾਉ ਬਦਲਣਾ ਡੈਸਕਟਾਪ ਲਿਸਟ ਵਿੱਚ ਜਾਉ ਡੈਸਕਟਾਪ ਲਿਸਟ ਵਿੱਚ ਜਾਉ (ਉਲਟ) ਵੱਖ-ਵੱਖ ਡੈਸਕਟਾਪ ਵਿੱਚ ਜਾਓ ਵੱਖ-ਵੱਖ ਡੈਸਕਟਾਪ ਵਿੱਚ ਜਾਓ (ਉਲਟ) 