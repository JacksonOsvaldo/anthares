��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  z   �  �   T     �     �     �  Q        i     v  	   �  -   �  }   �     <  <   I  v   �     �          -  	   J        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-08-08 15:42-0600
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi <punjabi-users@lists.sf.net>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 ਕੀ ਤੁਸੀਂ ਰੈਮ ਉੱਤੇ ਸਸਪੈਂਡ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ (ਸਲੀਪ)? ਕੀ ਤੁਸੀਂ ਡਿਸਕ ਉੱਤੇ ਸਸਪੈਂਡ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ (ਹਾਈਬਰਨੇਟ)? ਆਮ ਕਾਰਵਾਈਆਂ ਹਾਈਬਰਨੇਟ ਹਾਈਬਰਨੇਟ (ਡਿਸਕ ਉੱਤੇ ਸਸਪੈਂਡ ਕਰੋ) ਛੱਡੋ ...ਛੱਡੋ ਲਾਕ ਸਕਰੀਨ ਨੂੰ ਲਾਕ ਕਰੋ ਕੰਪਿਊਟਰ ਨੂੰ ਲਾਗ-ਆਉਟ ਕਰੋ, ਬੰਦ ਕਰੋ ਜਾਂ ਮੁੜ-ਚਾਲੂ ਕਰੋ ਨਹੀਂ ਸਲਪੀ (RAM ਉੱਤੇ ਸਸਪੈਂਡ ਕਰੋ) ਇੱਕ ਵੱਖਰੇ ਯੂਜ਼ਰ ਵਾਂਗ ਸਮਾਂਤਰ ਸ਼ੈਸ਼ਨ ਸ਼ੁਰੂ ਕਰੋ ਸਸਪੈਂਡ ਯੂਜ਼ਰ ਬਦਲੋ ਯੂਜ਼ਰ ਬਦਲੋ ਹਾਂ 