��    !      $  /   ,      �     �            +         A     b  ;   x     �     �     �     �           ?  9   M     �  3   �  	   �     �  )   �  %   	  |   /  [   �  5     *   >     i     �     �     �     �  (   �     �  3     �  I     	     	  	   %	  '   /	  "   W	     z	  ?   �	     �	     �	  $   �	  !   
     <
     V
  9   g
  
   �
  4   �
     �
     �
  &   �
  %   #  ~   I  c   �  1   ,  -   ^     �     �     �  
   �     �  #   �  '     0   ,                          !       
                                                       	                                                                       %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet If you already have an emoticon theme archive locally, this button will unpack it and make it available for KDE applications Insert the string for the emoticon.  If you want multiple strings, separate them by spaces. Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected theme from your disk Require spaces around emoticons This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2011-02-15 21:30+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=1;
 Tema %1 telah wujud Tambah Emotikon Tambah... Pilih jenis tema emotikon untuk dicipta Tidak Dapat Memasang Tema Emotikon Cipta emotikon baru Cipta emotikon baru dengan memberikannya ikon dan beberapa teks Padam emotikon Reka tema emotikon baru Adakah anda hendak membuang %1 juga? Seret atau Tapi URL Tema Emotikon sharuzzaman@myrealbox.com Sunting Emotikon Sunting emotikon dipilih untuk mengubah ikon atau teksnya Sunting... Tema emotikon mesti dipasang daripada fail tempatan. Emotikon Pengurus Emotikon Masukkan name bagi tema emotikon baru: Dapatkan ikon tema baru dari Internet Jika anda telah mempunyai arkib tema emotikon tempatan, butang ini akan mengeluarkannya dan menyediakannya untuk aplikasi KDE. Masukkan rentetan untuk emotikon. Jika anda hendak pelbagai rentetan, asingkan mereka dengan ruang. Pasang arkib fail tema yang anda telah ada disini Ubahsuai ikon emoticon atau teks yang dipilih Sharuzzaman Ahmat Raslan Tema Emotikon Baru Buang Buang Tema Buang emotikon dipilih Buang tema dipilih dari cakera anda Memerlukan ruang di sekeliling emotikon Ini akan membuang tema dipilih dari cakera anda. 