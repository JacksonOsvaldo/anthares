��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �  	   �     �     �     �     �     �  
        !  +   8  5   d     �  #   �     �     �     �  '   �          7     =     X     x     ~     �     �     �      �     �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2011-07-16 00:57+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=1;
 Umum Tambah skrip baru. Tambah... Batal? Komen: sharuzzaman@myrealbox.com Edit Sunting skrip dipilih. Sunting... Laksana skrip dipilih. Gagal mencipta skrip untuk penterjemah "%1" Gagal untuk menentukan pentafsir untuk failskrip "%1" Gagal memuatkan pentafsir "%1" Tidak dapat membuka fail skrip "%1" Fail: Ikon: Penterjemah: Tahap keselamatan bagi penterjemah Ruby Sharuzzaman Ahmat Raslan Nama: Tiada fungsi "%1" sebegitu Tiada penterjemah sebegitu "%1" Buang Buang skrip dipilih. Laksana Failskrip "%1" tidak wujud. Henti Henti pelaksanaan skrip dipilih. Teks: 