��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K          	  `  &  V  �  [   �	     :
     J
     Z
  #   h
     �
     �
     �
     �
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2008-03-03 20:04+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=1;
  saat Had ma&sa penanda permulaan: <H1>Papan Tugas Pemberitahuan</H1>
Anda boleh hidupkan kaedah kedua untuk pemberitahuan permulaan
iaitu dengan jam kaca berputar,
sebagai simbol aplikasi anda sedang dimuatkan.
Kadang-kadang berkemungkinan aplikasi tidak menyedari
fungsi ini. Dalam hal ini, kursor berhenti berkelip selepas masa
yang diberi untuk seksyen 'Penunujuk had masa permulaan' <h1>Kursor Sibuk</h1>
Kursor Sibuk KDE untuk pemberitahuan memulakan aplikasi.
Untuk membolehkan kursor sibuk, pilih satu jenis maklumbalas visual
dari kekotak kombo.
Kadang-kadang berkemungkinan aplikasi tidak menyedari
fungsi ini. Dalam hal ini, kursor berhenti berkelip selepas masa
yang diberi untuk seksyen 'Penunujuk had masa permulaan' <h1>Maklumbalas Pelancaran </h1> Anda boleh selaraskan maklumbalas lancar-aplikasi di sini. Kursor Berkelip Kursor Melantun Kursor S&ibuk Hidupkan pemberi&tahuan papan tugas Tiada Kursor Sibuk Kursor Sibuk Pasif Had masa penanda perm&ulaan: Papan Tugas Pemberitahua&n 