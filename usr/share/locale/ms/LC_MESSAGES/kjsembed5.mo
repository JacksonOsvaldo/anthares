��          �      L      �  Z   �          5     ;     P     X      r     �     �     �     �     �          2     K     d     w  (   �  �  �     h     p     �     �     �     �  $   �     �  !        0     N  &   m     �     �     �     �       '   $                     
                                                                   	            %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not an Object type Alert Call to '%1' failed. Confirm Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. No such method '%1'. There was an error reading the file '%1' Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2011-07-16 00:57+0800
Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>
Language-Team: Malay <kedidiemas@yahoogroups.com>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=1;
 %1, %2. %1 adalah bukan jenis Objek Waspada Panggilan ke '%1' gagal. Sah Tidak dapat membina nilai Tidak dapat mencipta fail sementara. Tidak dapat membuka fail '%1' Tidak dapat membuka fail '%1': %2 Tidak dapat membaca fail '%1' Gagal untuk mencipta Tindakan. Gagal untuk mencipta KumpulanTindakan. Gagal untuk mencipta Susunatur. Gagal untuk mencipta Widget. Gagal untuk memuatkan fail '%1' Fail %1 tidak ditemui. Tiada kaedah sebegitu '%1'. Terdapat ralat semasa membaca fail '%1' 