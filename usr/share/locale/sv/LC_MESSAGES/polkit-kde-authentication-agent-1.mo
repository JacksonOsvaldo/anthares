��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     X     `     d  	   z  }   �  Q        T     ]  0   y     �     �     �     �     	     $	     4	  
   B	     M	     _	     k	  $   ~	  #   �	  5   �	     �	  
   
     $
     9
     J
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-09 16:43+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 (%2) %1: © 2009 Red Hat, Inc. Åtgärd: Ett program försöker utföra en åtgärd som kräver rättigheter. Behörighetskontroll krävs för att utföra åtgärden. En annan klient håller redan på med behörighetskontroll. Försök igen senare. Program: Behörighetskontroll krävs Behörighetskontroll misslyckades Försök igen. Klicka för att redigera %1 Klicka för att öppna %1 Detaljinformation stefan.asserhall@bredband.net Tidigare underhåll Jaroslav Reznik Lukáš Tinkl Underhåll Stefan Asserhäll &Lösenord: Lösenord för %1: Lösenord för systemadministratör: Lösenord eller dra finger för %1: Lösenord eller dra finger för systemadministratör: Lösenord eller dra finger: Lösenord: PolicyKit1 KDE-modul Välj användare Tillverkare: 