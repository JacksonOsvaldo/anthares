��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  .   �     �  3   �          #     4     S     h     w     �     �     �     �     �     �  
   �     �     �     �          5     G     P        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-25 17:25+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 minimerat fönster: %1 minimerade fönster: %1 fönster: %1 fönster: ... och %1 annat fönster ... och %1 andra fönster Aktivitetsnamn Aktivitetsnummer Lägg till virtuellt skrivbord Anpassa skrivbord... Skrivbordsnamn Skrivbordsnummer Skärm: Gör ingenting Allmänt Horisontell Ikoner Layout: Ingen text Bara aktuell skärm Ta bort virtuellt skrivbord Val av aktuellt skrivbord: Visa aktivitetshanterare... Visar skrivbordet Standard Vertikal 