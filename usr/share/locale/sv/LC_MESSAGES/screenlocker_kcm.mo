��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �       �  
   �     �     �  6   �     +     2     @      M  ,   n      �  	   �  	   �  .   �     �                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-09 18:55+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Lås skärmen vid återgång: Aktivering Utseende Fel Misslyckades testa skärmlåsning med lyckat resultat. Genast Snabbtangent: Lås session Lås skärmen automatiskt efter: Låser skärmen vid uppvaknande ur viloläge &Kräv lösenord efter låsning:  min  min  sek  sek Den globala genvägen för att låsa skärmen. &Typ av skrivbordsunderlägg: 