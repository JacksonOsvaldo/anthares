��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  *   �  ,        ?  
   H     S     Y  
   t          �     �  *   �     �     �  3   �          /     >     M        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-05 12:34+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Vill du gå till viloläge i minne (vila)? Vill du gå till viloläge på disk (dvala)? Allmänt Åtgärder Dvala Dvala (viloläge på disk) Gå ifrån Gå ifrån... Lås Lås skärmen Logga ut, stäng av eller starta om datorn Nej Vila (viloläge i minne) Starta en parallell session som en annan användare Gå till viloläge Byt användare Byt användare Ja 