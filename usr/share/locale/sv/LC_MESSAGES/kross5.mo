��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y  
   -     8     W     g     {     �     �     �  
   �  n   �     0	     9	     S	     _	  +   u	  1   �	     �	  %   �	     
     
     #
     )
  M   H
     �
  *   �
  &   �
     �
     �
               1  #   8     \  0   b     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-02 16:14+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Upphovsman Copyright 2006 Sebastian Sauer Sebastian Sauer Skriptet att köra. Allmänt Lägg till ett nytt skript. Lägg till... Avbryt? Kommentar: d96reftl@dtek.chalmers.se,awl@hem.passagen.se,pelinstr@algonet.se,newzella@linux.nu,stefan.asserhall@telia.com Redigera Redigera markerat skript. Redigera... Kör markerat skript. Misslyckades skapa skript för tolken "%1". Misslyckades bestämma tolk för skriptfilen "%1" Misslyckades ladda tolken "%1" Misslyckades öppna skriptfilen "%1". Fil: Ikon: Tolk: Säkerhetsnivå för Ruby-tolk Magnus Reftel,Anders Widell,Per Lindström,Mattias Newzella,Stefan Asserhäll Namn: Det finns inte någon sådan funktion "%1" Det finns inte någon sådan tolk "%1" Ta bort Ta bort markerat skript. Kör Skriptfilen "%1" finns inte. Stoppa Stoppa körning av markerat skript. Text: Kommandoradsverktyg för att köra Kross-skript. Kross 