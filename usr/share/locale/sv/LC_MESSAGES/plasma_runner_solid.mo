��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     Y     f  ,   u  G   �  >   �  >   )  B   h  E   �  M   �     ?	     M	     S	     [	     `	     h	  	   q	     {	     �	                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-06 22:22+0100
Last-Translator: Stefan Asserhall <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Lås volymen Mata ut medium Söker efter enheter vars namn motsvarar :q: Listar alla enheter och låter dem monteras, avmonteras eller matas ut. Listar alla enheter som kan matas ut, och låter dem matas ut. Listar alla enheter som kan monteras, och låter dem monteras. Listar alla enheter som kan avmonteras, och låter dem avmonteras. Listar alla krypterade enheter som kan låsas, och låter dem låsas. Listar alla krypterade enheter som kan låsas upp, och låter dem låsas upp. Montera enhet enhet mata ut lås montera lås upp avmontera Lås upp volymen Avmontera enhet 