��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �      �  &        *     H     N     `  5   z     �     �  *   �  	     	     �   %  �   �  y   I	     �	  <   �	     
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-05 21:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  min Fungera som Alltid Definiera ett särskilt beteende Använd inte särskilda inställningar stefan.asserhall@bredband.net Dvala Stefan Asserhäll Stäng aldrig av skärmen Placera aldrig datorn i viloläge eller stäng av den Datorn använder nätadapter Datorn använder batteri Datorn använder batteri med låg laddning Stäng av Viloläge Tjänsten för strömsparhantering verkar inte köra.
Det kan lösas genom att starta eller schemalägga den i "Start och avslutning". Aktivitetstjänsten kör inte.
Det är nödvändigt att aktivitetshanteraren kör för att ställa in aktivitetsspecifikt beteende för strmsparhanteringen. Aktivitetstjänsten kör med grundläggande funktioner.
Namn och ikoner för aktiviteterna kanske inte är tillgängliga. Aktivitet "%1" Använd separata inställningar (bara avancerade användare) efter 