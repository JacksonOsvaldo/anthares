��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �  	   �     �     �     �     �  	     .        ?     ^     o     �     �     �  D   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 18:39+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 A&npassa skrivare... Bara aktiva jobb Alla jobb Bara färdiga jobb Anpassa skrivare Allmänt Inga aktiva jobb Inga jobb Inga skrivare har ställts in eller upptäckts Ett aktivt jobb %1 aktiva jobb Ett jobb %1 jobb Öppna utskriftskö Utskriftskön är tom Skrivare Sök efter en skrivare... Det finns ett utskriftsjobb i kön Det finns %1 utskriftsjobb i kön 