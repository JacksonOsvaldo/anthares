��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
  
   �
     �
     �
     �
               '     /     K     a     r  .   �     �     �     �     �                     (  	   +     5     M  	   `  6   j     �     �  	   �     �  	   �  2   �          (     ;     ?     K     Z     b     u     �     �     �     �     �     �     �               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-09 18:56+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 % Tillbaka Batteri %1 % Byt layout Virtuellt tangentbord Avbryt Caps Lock är på Stäng Stäng sökning Anpassa Anpassa sökinsticksprogram Skrivbordssession: %1 Annan användare Tangentbordslayout: %1 Loggar ut om 1 sekund Loggar ut om %1 sekunder Logga in Inloggning misslyckades Logga in som annan användare Logga ut Nästa spår Inga media spelar Oanvänd Ok Lösenord Spela eller pausa media Föregående spår Starta om Startar om efter 1 sekund Startar om efter %1 sekunder Senaste frågor Ta bort Starta om Visa mediakontroller: Stäng av Stänger av om 1 sekund Stänger av om %1 sekunder Starta ny session Gå till viloläge Byt Byt session Byt användare Sök... Sök efter '%1'... Plasma tillverkat av KDE Lås upp Upplåsning misslyckades på TTY %1 (bildskärm %2) TTY %1 Användarnamn %1 (%2) i kategorin senaste frågor 