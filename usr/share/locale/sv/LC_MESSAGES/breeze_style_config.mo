��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     D     H     d     �     �     �     �     �     �       &   *      Q  ;   r  =   �     �  &   		     0	     N	  (   k	     �	     �	  (   �	     �	     �	      
     
     
     &
     D
     Q
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-18 21:51+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms Snabb&tangenters synlighet: Typ av knapp för &uppåtpil: Dölj alltid snabbtangenter Visa alltid snabbtangenter Animerings&längd: Animeringar T&yp av knapp för neråtpil: Breeze-inställningar Centrera flikar på flikraden Dra fönster från alla tomma områden Dra bara fönster med namnlisten Dra fönster från namnlisten, menyraden och verktygsrader  Rita en tunn linje för att ange fokus i menyer och menyrader Rita fokusindikator i listor Rita ram omkring dockningsbara paneler Rita ram omkring sidorubriker Rita ram omkring sidopaneler Rita streckmarkeringar för skjutreglage Rita avdelare av verktygsrader Aktivera animeringar Aktivera utökade storleksändringsgrepp Ramar Allmänt Inga knappar En knapp Rullningslister Visa snabbtangenter vid behov Två knappar Dragmetod för &fönster: 