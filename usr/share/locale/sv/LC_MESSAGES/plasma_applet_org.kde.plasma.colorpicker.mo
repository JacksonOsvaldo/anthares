��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  9   �     �     �     �          "     +     @     M     ]     k     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-14 18:52+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Kopiera automatiskt den markerade texten till klippbordet Rensa historik Färgalternativ Kopiera till klippbordet Standardfärgformat: Allmänt Visa färgdialogruta Hämta färg Hämta en färg Visa historik När snabbtangenten trycks ner: 