��    H      \  a   �            !     5     D     R     f     s     �     �     �     �     �     �     �     �          "     0     B     P     ]     m     }     �     �     �     �     �     �     �               &     3     @     \     s     �      �     �     �     
	     %	     =	  #   \	     �	     �	     �	  "   �	     
  $   !
     F
     c
     
     �
     �
  3   �
       $   (     M  &   f  <   �  !   �  8   �  0   %  !   V  C   x  X   �  P     R   f      �  +   �  �       �     �     �  
   �     �  
   �     �     �  	   �       	             (     1     G     N     V     c     i  	   n     x          �     �     �     �     �  	   �     �     �     �     �     �     �  
     
        %     5     Q     p     �  	   �     �     �     �     �     �          '  $   =     b     z     �     �     �  
   �     �  
   �     �     �     �               5     F     O     _     y     �  
   �     �        -      0   G   C   4                      +   ,                 "      '   	          <             ;   9   3              *   A   2   %      )   :   =                           @   5                   H   &   B   $             /   
      ?       E       D   6                 F          #   !   1                  8       7       .      (       >       @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDocument Generated By @labelDuration @labelFolder @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music disc numberDisc Number @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label the URL a file was originally downloded fromDownloaded From @label the message ID of an email this file was attached toE-Mail Attachment Message ID @label the sender of an email this file was attached toE-Mail Attachment Sender @label the subject of an email this file was attached toE-Mail Attachment Subject @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2017-10-18 21:49+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Albumartist Arkiv Artist Proportion Ljud Upphovsman Bithastighet Kanaler Kommentar Tonsättare Copyright Skapad datum Dokument Dokument genererat av Längd Katalog Bildfrekvens Höjd Bild Nyckelord Språk Textförfattare Sidantal Presentation Förläggare Utgivningsår Samplingsfrekvens Kalkylark Ämne Text Titel Video Bredd Bildens datum och tid Bildmärke Bildmodell Bildorientering Bländarangivelse för foto Exponeringsavvikelse för foto Exponeringstid för foto Bländartal foto Fotoblixt Fokallängd för foto Fokallängd 35mm för foto Fotots GPS-höjd Fotots GPS-latitud Fotots GPS-longitud ISO-känslighet för foto Mätarläge för foto Ursprungligt datum och tid för foto Färgmättnad för foto Skärpa för foto Vitbalans för foto X-dimension för foto Y-dimension för foto Skapa mall Album Skivnummer Genre Spårnummer Översättningsutkast Radantal Översättningsbara enheter Översättningar Ordantal Nerladdad från Brev-id för e-postbilaga Avsändare av e-postbilaga Rubrik för e-postbilaga Upphovsman Senaste uppdatering 