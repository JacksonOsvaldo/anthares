��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �     �	  $   �	  .   �	     �	  /   
     A
      a
  '   �
     �
     �
  !   �
     �
       #   *  $   N  5   s      �     �     �     �       )   +     U     o     �     �     �     �     �     �     �     �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-09-03 11:28+0200
Last-Translator: Stefan Asserhall <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Filer tillagda i Bazaar-arkiv. Lägger till filer i Bazaar-arkiv... Tillägg av filer i Bazaar-arkiv misslyckades. Bazaar-logg stängd. Arkivering i Bazaar av ändringar misslyckades. Arkiverade ändringar i Bazaar. Arkiverar ändringar i Bazaar... Hämtning av Bazaar-arkiv misslyckades. Hämtade Bazaar-arkiv. Hämtar Bazaar-arkiv... Misslyckades skicka Bazaar-arkiv. Skickade Bazaar-arkiv. Skickar Bazaar-arkiv... Filer borttagna från Bazaar-arkiv. Tar bort filer från Bazaar-arkiv... Borttagning av filer från Bazaar-arkiv misslyckades. Misslyckades granska ändringar. Granskade ändringar. Granskar ändringar... Misslyckades köra Bazaar-logg. Kör Bazaar-logg... Uppdatering av Bazaar-arkiv misslyckades. Uppdaterade Bazaar-arkiv. Uppdaterar Bazaar-arkiv... Bazaar lägg till... Bazaar arkivera... Bazaar ta bort Bazaar logg Bazaar hämta Bazaar skicka Bazaar uppdatera Visa lokala ändringar i Bazaar 