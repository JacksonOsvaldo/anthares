��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �  
   �     �     �     �     	  !   ,	     N	     k	     �	  
   �	  	   �	     �	     �	  L   �	     
  N   !
  
   p
     {
     �
     �
  	   �
  
   �
     �
     �
  	   �
     �
                  *   %  D   P     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-03 17:30+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100 % Upphovsman Copyright 2015 Harald Sitter Harald Sitter Inga program spelar ljud Inga program spelar in ljud Inga enhetsprofiler tillgängliga Inga inenheter tillgängliga Inga utenheter tillgängliga Profil: Pulseaudio Avancerat Program Enheter Lägg till virtuell utenhet för samtidig utmatning på alla lokala ljudkort Avancerad utgångsinställning Byt automatiskt alla pågående strömmar när en ny utgång blir tillgänglig Inspelning Förval Enhetsprofiler stefan.asserhall@bredband.net Ingångar Tysta ljud Stefan Asserhäll Underrättelseljud Utgångar Uppspelning Port  (ej tillgänglig)  (urkopplad) Kräver PulseAudio-modulen 'module-gconf'  Moduler gör det möjligt att ställa in Pulseaudio ljuddelsystemet. %1: %2 100 % %1 % 