��          <      \       p   !   q   3   �      �   �  �   (   �  4   �     �                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: plasma_runner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-03-31 21:40+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <sv@li.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Hittar Kate-sessioner som motsvarar :q:. Listar alla Kates redigeringssessioner i ditt konto. Öppna Kate-session 