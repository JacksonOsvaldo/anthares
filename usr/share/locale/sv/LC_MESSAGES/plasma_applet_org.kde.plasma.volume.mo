��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �  	   �     �     �     �     	     	     	     %	     2	     :	     C	     J	  
   ]	     h	     w	     |	     �	  ,   �	  !   �	     �	     �	     
     
     +
  #   >
     b
  
   h
     s
  
   �
     �
     �
     �
     �
        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-20 17:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % Justera volym för %1 Program Ljud tystat Ljudvolym Beteende Inspelningsenheter Spela in strömmar Tyst Förval Minska mikrofonvolym Minska volym Enheter Allmänt Portar Öka mikrofonvolym Öka volym Maximal volym: Tyst Tysta %1 Tysta mikrofon Inga program spelar upp eller spelar in ljud Inga ut- eller inenheter hittades Uppspelningsenheter Spela upp strömmar  (ej tillgänglig)  (urkopplad) Höj maximal volym Visa ytterligare alternativ för %1 Volym Volym %1 % Volymåtermatning Volymsteg: %1 (%2) %1: %2 100 % %1 % 