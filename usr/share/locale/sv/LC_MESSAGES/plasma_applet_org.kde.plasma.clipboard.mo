��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �     �     �     
     )     >     E     L     c  
   �     �     �     �     �     �     �     �     �                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-17 17:28+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Ändra typ av streckkod Rensa historik Klippbordets innehåll Klippbordets historik är tom. Klippbordet är tomt Kod 39 Kod 93 Anpassa klippbordet... Misslyckades skapa streckkod Datamatris Redigera innehåll +%1 Utför åtgärd  QR-kod Ta bort från historik Återgå till klippbordet Sök Visa streckkod 