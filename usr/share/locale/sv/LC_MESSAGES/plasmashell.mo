��    &      L  5   |      P     Q     p  	   ~  0   �  .   �     �  7         ?     `     i  \   �  t   �  9   Z  -   �  $   �     �  [   �     P  E   o  G   �  >   �     <     C     Z     g     �     �     
     )  <   .  :   k  s   �  o   	  .   �	  ,   �	  .   �	  ,   
  �  B
  '   �          %     6     B  *   O  8   z     �     �      �  T   �  U   N     �     �  ,   �       p        �     �     �  	   �     �     �     �  �   �     n     }  !   �     �     �     �  $   �  "   
     -     <     L     T                 &   %                     !   	      
                       #       "                     $                                                                             Activate Task Manager Entry %1 Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Enable QML Javascript debugger Enables test mode and specifies the layout javascript file to set up the testing environment Fatal error message bodyAll shell packages missing.
This is an installation issue, please contact your distribution Fatal error message bodyShell package %1 cannot be found Fatal error message titlePlasma Cannot Start Force loading the given shell plugin Hide Desktop Load plasmashell as a standalone application, needs the shell-plugin option to be specified NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Plasma is unable to start as it could not correctly use OpenGL 2.
 Please check that your graphic drivers are set up correctly. Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2016-12-14 18:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aktivera post %1 i aktivitetshanteraren Aktiviteter... Lägg till panel Blåtand av Blåtand på Anpassa insticksprogram för musåtgärder Starta inte om Plasma-skalet automatiskt efter en krasch stefan.asserhall@bredband.net Tom %1 Aktivera QML Javaskript-avlusare Aktiverar testläge och anger Javascript-layoutfilen för att ställa in testmiljön Alla skalpaket saknas.
Detta är ett installationsproblem, kontakta din distribution. Skalpaket %1 kan inte hittas Plasma kan inte starta Tvinga laddning av givet skalinsticksprogram Dölj skrivbord Ladda Plasma-skalet som ett fristående program. Kräver att alternativet skal-insticksprogram är specificerat. Stefan Asserhäll %1 tyst Mikrofon tystad Ljud tyst Plasma Plasma misslyckades starta Plasma-skal Plasma kan inte starta eftersom det inte kunde använda OpenGL 2 korrekt.
Kontrollera att grafikdrivrutinerna är riktigt inställda. Visa skrivbord Stoppa nuvarande aktivitet Kunde inte läsa in skriptfil: %1 Fil Mobilt Internet av Mobilt Internet på Tangentbord på skärmen inaktiverat Tangentbord på skärmen aktiverat Tryckplatta av Tryckplatta på WIFI av WIFI på 