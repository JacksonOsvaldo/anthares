��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  �  �  U  !     w"     �"     �"      �"      �"     �"  9   �"     '#     >#     L#     T#  /   d#  $   �#     �#     �#     �#     �#     �#     $  
   &$     1$     N$     T$  6   h$     �$  K   �$     �$     %     *%  �   3%  !   &  �   )&     �&  "   �&     �&     �&     '  
   '     $'  (   0'     Y'  B   f'  0   �'  9   �'  (   (  +   =(  5   i(  *   �(  2   �(  +   �(     ))     F)     b)  +   o)     �)  
   �)  ?   �)  R   �)     Q*     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-12-10 21:47+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Säkerhetsanmärkning:</b>
                             Enligt en säkerhetsrevision av Taylor Hornby (Defuse Security),
                             är nuvarande implementering av Encfs sårbar eller potentiellt sårbar
                             för flera olika typer av attacker.
                             Exempelvis kan en angripare med läs- och skriv
                             åtkomst till krypterad data sänka krypteringskomplexiteten
                             för senare krypterad data utan att det märks av en legitim användare,
                             eller använda tidsanalys för att sluta sig till information.
                             <br /><br />
                             Det betyder att man inte ska synkronisera
                             krypterad data med en molnlagringstjänst,
                             eller använda det under andra omständigheter där en angripare
                             ofta kan komma åt krypterad data.
                             <br /><br />
                             Se <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> för mer information. <b>Säkerhetsanmärkning:</b>
                             CryFS krypterar filer så att de säkert kan lagras var som helst.
                             Det fungerar bra tillsammans med molntjänstersom Dropbox, iCloud, OneDrive med flera.
                             <br /><br />
                             I motsats till andra filsystemlösningar med överlagring,
                             exponerar det inte katalogstrukturen,
                             antal filer eller filstorlekarna
                             via det krypterade dataformatet.
                             <br /><br />
                             En viktig sak att notera är att även
                             om CryFS anses vara säkert, finns
                             det ingen oberoende säkerhetsrevision
                             som bekräftar det. Skapa ett nytt valv Aktiviteter Gränssnitt: Kan inte skapa monteringsplatsen Kan inte öppna ett okänt valv. Ändra Välj krypteringssystem som du vill använda för valvet: Välj använt chiffer: Stäng valvet Anpassa Anpassa valv... Inställt gränssnitt kan inte instansieras: %1 Inställt gränssnitt finns inte: %1 Korrekt version hittades Skapa Skapa ett nytt valv... CryFS Enheten är redan öppen Enheten är inte öppen Dialogruta Visa inte anmärkningen igen EncFS Krypterad dataplats Misslyckades skapa kataloger. Kontrollera rättigheter Misslyckades köra Misslyckades hämta listan över program med användning av det här valvet Misslyckades öppna: %1 Stäng ovillkorligt Allmänt Om valvet begränsas till vissa aktiviteter, visas det bara i miniprogrammet när de aktiviteterna används. Dessutom, om du byter till en aktivitet där det inte ska vara tillgängligt, stängs det automatiskt. Begränsa till valda aktiviteter: Var medveten om att det inte finns något sätt att återskapa ett glömt lösenord. Om du glömmer lösenordet är data så gott som borta. Monteringsplats Monteringsplatsen är inte angiven Monteringsplats: Nästa Öppna med filhanterare Lösenord: Plasma valv Ange lösenordet för att öppna valvet: Föregående Monteringsplatsens katalog är inte tom, vägrar att öppna valvet Det angivna gränssnittet är inte tillgängligt Valvinställningen kan bara ändras när det är stängt. Valvet är okänt, kan inte stänga det. Valvet är okänt, kan inte förstöra det. Enheten är redan registrerad. Kan inte skapa om den. Katalogen innehåller redan krypterad data Kan inte stänga valvet, ett program använder det Kan inte stänga valvet, det används av %1 Kan inte detektera versionen Kan inte utföra åtgärden Okänd enhet Okänt fel, kunde inte skapa gränssnittet. Använd standardchiffer &Valvnamn: Felaktig version installerad. Versionen som krävs är %1.%2.%3 Du måste välja tomma kataloger för krypterad lagring och för monteringsplatsen %1: %2 