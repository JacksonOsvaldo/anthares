��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     p  d   �     �     �            
   7     B     K     X     q  
   �     �     �     �  
   �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-02-07 11:01+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Lägg till startprogram... Lägg till startprogram med drag och släpp, eller genom att använda den sammanhangsberoende menyn. Utseende Arrangemang Redigera startprogram... Aktivera meddelanderutor Ange titel Allmänt Dölj ikoner Maximalt antal kolumner: Maximalt antal rader: Snabbstart Ta bort startprogram Visa dolda ikoner Visa startprogramnamn Visa titel Titel 