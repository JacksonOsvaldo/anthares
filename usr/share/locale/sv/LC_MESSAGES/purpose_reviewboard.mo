��          �      \      �     �     �  	   �  $   �          ,     C     ^     j  	   y  
   �     �     �     �     �     �  "   �  	   �  '   �  �  &     �     �     �      �  "        9  !   X     z       
   �     �     �     �     �     �     �  "   �                                 
            	                                                                 / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Repository: Request Error: %1 Server: Update Review: Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-10-04 11:27+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 / Behörighetskontroll Baskatalog: Kunde inte skapa ny begäran:
%1 Kunde inte hämta granskningslista Kunde inte ställa in metadata Kunde inte ladda upp programfixen Mål JSON-fel: %1 Lösenord: Arkiv Arkiv: Fel vid begäran: %1 Server: Uppdatera granskning: Uppdatera granskning Användarnamn för angiven tjänst Användarnamn: Varifrån projektet checkades ut 