��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  +   �        
              )  	   H     R     c     j     q     y     �     �     �  	   �     �  /   �            	        "  	   @     J  	   Y     c  	   v     �     �     �  	   �  B   �  
   �  
   �                    *     6  	   J     T     h     �     �     �     �     �     �     �     �     �       	             )     1     >     Q  	   ]     g  	   l     v     {     �     �     �     �     �     �  '   �          4     :     L     P  <   W     �     �     �     �     �     �     �     �     �     �  	                       $     +  
   2           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-11-20 10:42+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 
Modul för enhetsvisning baserad på Solid © 2010 David Hubner AMD 3DNow  ATI IVEC %1 ledigt av %2 (%3 % använt) Batterier Typ av batteri:  Buss:  Kamera Kameror Laddningsstatus:  Laddar Dra ihop alla Compact Flash-läsare En enhet. Enhetsinformation Visar alla enheter som för närvarande listas. Enhetsvisning Enheter Laddar ur stefan.asserhall@bredband.net Krypterad Expandera alla Filsystem Typ av filsystem:  Fulladdat Hårddiskenhet Inkopplingsbar? IDE IEEE 1394 Visar information om enheterna som för närvarande är markerade. Intel MMX  Intel SSE  Intel SSE2  Intel SSE3  Intel SSE4  Tangentbord Tangentbord och mus Etikett:  Maximal hastighet:  Läsare för minnesstickor Monterad som:   Mus Multimediaspelare Stefan Asserhäll Nej Ingen laddning Ingen data tillgänglig Inte monterad Inte angiven Optisk enhet Handdator Partitionstabell Primär Processor %1 Processor nummer:  Processorer Produkt:  RAID Flyttbar? SATA SCSI SD/MMC-läsare Visa alla enheter Visa relevanta enheter Smart Media-läsare Lagringsenheter Drivrutiner som stöds:  Instruktionsuppsättningar som stöds:  Protokoll som stöds:  UDI:  Avbrottsfri kraft USB UUID:  Visar de aktuella enheternas UDI (unika enhetsidentifierare) Okänd enhet Oanvänd Tillverkare:  Volymutrymme:  Volymanvändning:  Ja IM enhetsinformation Okänt Ingen Ingen Plattform Okänd Okänd Okänd Okänd Okänd xD-läsare 