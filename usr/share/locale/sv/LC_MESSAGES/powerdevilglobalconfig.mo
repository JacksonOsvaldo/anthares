��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     �     �     �  *        /  D   C  A   �     �     �     �     	  	   '     1     7      I     j  /   |  	   �  	   �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-05-11 16:04+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  % &Kritisk nivå: &Låg nivå: <b>Batterinivåer                     </b> &Vid kritisk nivå: Batteriet anses ha nått kritisk nivå när det når den här nivån Batteriet anses ha nått låg nivå när det når den här nivån Anpassa underrättelser... Kritisk batterinivå Gör ingenting stefan.asserhall@bredband.net Aktiverad Dvala Låg batterinivå Låg nivå för periferienheter: Stefan Asserhäll Pausa mediaspelare vid aktivering av viloläge: Stäng av Viloläge Tjänsten för strömsparhantering verkar inte köra.
Det kan lösas genom att starta eller schemalägga den i "Start och avslutning". 