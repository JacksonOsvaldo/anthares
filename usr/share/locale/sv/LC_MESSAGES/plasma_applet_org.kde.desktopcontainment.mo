��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l          !     3     L     T     `     l     �  
   �  	   �     �  
   �     �  	   �     �     �     �     �     �     �               $     *     2     ;     G     V     g     �     �     �  	   �     �  5   �     �     �          %     3     K     Q     ]     d     i     r     x     ~     �     �     �     �     �     �     �     �  N   �  (   (     Q     q     y     �     �     �     �     �     �     �     �     �     �       ,        I     b     �     �     �     �     �  
   �     �  	   �     �     �     �     �       *     	   6     @  !   U     w  X   �     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-19 21:07+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Ta bort &Töm papperskorg &Flytta till papperskorg Ö&ppna K&listra in E&genskaper Uppdate&ra skrivbord Uppdate&ra vy Uppdate&ra &Byt namn Välj... Rensa ikon Justera Utseende: Arrangera i Arrangera i Arrangemang: Bakåt Avbryt Kolumner Anpassa skrivbord Egen rubrik Datum Förval Fallande Beskrivning Avmarkera alla Skrivbordslayout Skriv in egen rubrik här Funktioner: Filnamnsmönster: Filtyp Filtyper: Filter Meddelanderutor för förhandsgranskning av kataloger Kataloger först Kataloger först Fullständig sökväg Jag förstår Dölj filer som matchar Enorm Ikonstorlek Ikoner Stor Vänster Lista Plats Plats: Lås på plats Låst Normal Fler granskningsalternativ... Namn Ingen Ok Panelknapp: Tryck och håll nere grafiska komponenter för flytta dem och visa deras grepp Insticksprogram för förhandsgranskning Förhandsgranska miniatyrbilder Ta bort Ändra storlek Återställ Höger Rotera Rader Sök filtyp... Markera alla Markera katalog Markeringsverktyg Visa alla filer Visa filer som matchar Visa en plats: Visa filer länkade till nuvarande aktivitet Visa skrivbordskatalogen Visa skrivbordets verktygslåda Storlek Liten Liten Sortera enligt Sortera enligt Sortering: Ange en katalog: Textrader Mycket liten Titel: Verktygstips Justeringar Typ Skriv in en sökväg eller webbadress här Osorterad Använd en egen ikon Hantering av grafiska komponenter Grafiska komponenter upplåsta Du kan trycka och hålla nere grafiska komponenter för flytta dem och visa deras grepp. Visningsläge 