��          �            h     i     r     �  	   �  *   �     �  "   �                 ;   +     g     z       �  �     3     C     T     l  4   �     �  0   �            
   #  =   .     l     �  	   �     	                 
                                                          &Sort by &Trigger word: &Use trigger word CPU usage It is not sure, that this will take effect Kill Applications Config Process ID: %1
Running as user: %2 Send SIGKILL Send SIGTERM Terminate %1 Terminate running applications whose names match the query. inverted CPU usage kill nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-10-29 21:48+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Sortera &enligt U&tlösande ord: An&vänd utlösande ord Processoranvändning Det är inte säkert att det här får någon effekt Inställning av döda program Processidentifierare: %1
Kör som användare: %2 Skicka SIGKILL Skicka SIGTERM Avsluta %1 Avsluta program som kör och vars namn motsvarar förfrågan. inverterad processoranvändning döda ingenting 