��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  %   �     �  $   �                                                  	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-29 20:42+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Aktuell tid är %1 Visar dagens datum Visar dagens datum i en given tidszon Visar aktuell tid Visar aktuell tid i en given tidszon datum tid Dagens datum är %1 