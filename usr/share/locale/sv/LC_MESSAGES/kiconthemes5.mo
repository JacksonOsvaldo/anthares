��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �          �  >   �  
   �     �     �  
   �     �     �       
     
        #     4     <  /   K  
   {     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-19 09:17+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 B&läddra... &Sök: *.png *.xpm *.svg *.svgz|Icon-filer (*.png *.xpm *.svg *.svgz) Åtgärder Alla Program Kategorier Enheter Emblem Smilisar Ikonkälla Mime-typer &Övriga ikoner: Platser S&ystemikoner: Sök interaktivt efter ikonnamn (t.ex. folder). Välj ikon Status 