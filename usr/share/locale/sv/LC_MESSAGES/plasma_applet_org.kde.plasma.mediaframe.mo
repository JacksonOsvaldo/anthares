��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �               &     7     C  	   W     a  4   j     �  
   �     �     �     �     �          !  ,   )     V     n  G   �  8   �  B   	  B   O	     �	     �	     �	     �	                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-19 21:07+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Lägg till filer... Lägg till katalog... Bakgrundsram Ändra bild efter Välj en katalog Välj filer Anpassa Plasmoid... Fylläge: Allmänt Vänsterklick på bilden öppnar den i extern visare Centrera Sökvägar Sökvägar: Paus när musen hålls över Bevara proportion och beskär Bevara proportion Slumpmässiga objekt Sträck Bilden dupliceras horisontellt och vertikalt Bilden förändras inte Bilden skalas för att passa Bilden skalas likformigt för att passa, med beskärning om det behövs Bilden skalas likformigt för att passa utan beskärning Bilden sträcks ut horisontellt och läggs sida vid sida vertikalt Bilden sträcks ut vertikalt och läggs sida vid sida horisontellt Sida vid sida Sida vid sida horisontellt Sida vid sida vertikalt s 