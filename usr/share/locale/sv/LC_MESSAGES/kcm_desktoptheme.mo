��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     W     n     z     �     �     �     �     �  $   �  "     %   7  !   ]  4            	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-18 21:51+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Anpassa skrivbordstema David Rosca stefan.asserhall@bredband.net Hämta nya teman... Installera från fil... Stefan Asserhäll Öppna Tema Ta bort tema Temafiler (*.zip *.tar.gz *.tar.bz2) Installation av tema misslyckades. Tema installerat med lyckat resultat. Borttagning av tema misslyckades. Den här modulen låter dig anpassa skrivbordstemat. 