��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �  
   	  	   "	  
   ,	     7	  	   N	  
   X	  	   c	     m	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     A
     T
  &   i
     �
     �
     �
     �
  &   �
     �
          -     :     P     j     s  8   �     �     �     �  
             !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-18 21:51+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Tillgängliga storlekar: %1) Välj bild Avancerat Upphovsman Automatisk in&loggning Bakgrund: Rensa bild Kommandon Kunde inte packa upp arkiv Muspekartema: Anpassa tema Förval Beskrivning Ladda ner nya SDDM-teman stefan.asserhall@bredband.net Allmänt Hämta nytt tema Stoppkommando: Installera från fil Installera ett tema. Ogiltigt temapaket Läs in från fil... Inloggningsfönster som använder SDDM Maximalt användar-id: Minimalt användar-id: Stefan Asserhäll Namn Ingen förhandsgranskning tillgänglig Omstartskommando: Logga in igen efter avslutning Ta bort tema SDDM KDE-inställning Installation av SDDM-tema Session: Det förvalda pekartemat i SDDM Temat att installera, måste vara en befintlig arkivfil. Tema Kan inte installera tema Avinstallerar ett tema. Användare Användare: 