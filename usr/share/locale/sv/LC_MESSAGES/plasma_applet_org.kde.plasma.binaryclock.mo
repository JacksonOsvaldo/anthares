��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     E     N     W     e     r  '   �     �  +   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-03 17:31+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Utseende Färger: Visa sekunder Rita rutnät Visa avstängda lysdioder: Använd egen färg på aktiva lysdioder Använd egen färg på rutnät Använd egen färg på avstängda lysdioder 