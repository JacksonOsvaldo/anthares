��          T      �       �   "   �      �   *   �   3   '  E   [  .   �  �  �  &   |     �  *   �     �  )   �     %                                        Display a submenu for each desktop Display all windows in one list Display only the current desktop's windows plasma_containmentactions_switchwindowAll Desktops plasma_containmentactions_switchwindowConfigure Switch Window Plugin plasma_containmentactions_switchwindowWindows Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-30 03:13+0200
PO-Revision-Date: 2014-06-14 13:59+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Visa en undermeny för varje skrivbord Visa alla fönster i en lista Visa bara fönster på nuvarande skrivbord Alla skrivbord Anpassa insticksprogram för fönsterbyte Fönster 