��    &      L  5   |      P     Q     l     y     �     �     �     �     �     �     �     �     �  &   �               #     ,     3     ?     M     U     ]     d     s     �     �     �     �     �     �     �     �     �     �  
   �            �       �     �     �     �     �  	   �               (     F     V     j  0   p     �     �     �     �     �     �     �     �               %     9  
   K     V     q     w     �     �     �  
   �     �     �     �     	                           	      #      $                                                    &                                          !          %                            "   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache Dirty, Writeback: %1 MiB, %2 MiB Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Writeback memory: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-02-24 19:46+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 s Program: Medelklockfrekvens: %1 MHz Stapel Buffrar: Processor Processor %1 Processorövervakare Processor %1: %2 % vid %3 MHz Processor: %1 % Processorer separat Cache Cache modifierad, återskrivning: %1 MiB, %2 MiB Cacheövervakare Lagrade i cache: Cirkel Färger Kompakt stapel Modifierat minne: Allmänt Väntar på I/O: Minne Minnesövervakare Minne: %1/%2 Mibyte Övervakningstyp: Prioritet: Ställ in färger manuellt Visa: Växlingsutrymme Växlingsutrymme Växlingsutrymme: %1/%2 Mibyte Sys: Systemlast Uppdateringsintervall: Använt växlingsutrymme: Användare: Återskrivningsminne: 