��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �     0   �  n   �     E  M   ^     �     �     �  �   �  q   {  0   �     	     >	  	   V	                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-03-05 21:29+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Klicka på knappen, och skriv därefter in genvägen som du skulle göra i programmet.
Exempel för Ctrl+A: Håll nere Ctrl-tangenten och tryck på A. Konflikt med standardsnabbtangenter för program d96reftl@dtek.chalmers.se,awl@hem.passagen.se,pelinstr@algonet.se,newzella@linux.nu,stefan.asserhall@telia.com KPackage QML-programskal Magnus Reftel,Anders Widell,Per Lindström,Mattias Newzella,Stefan Asserhäll Ingen Ändra tilldelning Reserverad genväg Tangentkombinationen '%1' används också för standardåtgärden "%2" som vissa program använder.
Vill du verkligen också använda den som global snabbtangent? Tangenten F12 är reserverad på Windows, och kan inte användas som global snabbtangent.
Välj en annan tangent. Tangenten du just tryckte ner stöds inte av Qt. Programmets unika namn (krävs) Tangent som inte stöds Inmatning 