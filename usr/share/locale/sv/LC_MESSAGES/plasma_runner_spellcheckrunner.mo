��          t      �                 '     6     R     Z     w  Q   �     �      �       �       �     �     
     (     /  #   M     q     w     �     �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: plasma_runner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-10 08:11+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Kräv utlösningsord &Utlösningsord: Kontrollerar stavning av :q:. Rätta Kunde inte hitta en ordlista. Inställningar av stavningskontroll %1:q: Föreslagna ord: %1 ,  stavning 