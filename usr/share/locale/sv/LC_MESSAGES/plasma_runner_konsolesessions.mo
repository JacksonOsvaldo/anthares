��          4      L       `   $   a   /   �   �  �   ,   u  ,   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-04-01 21:33+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <sv@li.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Hittar terminal-sessioner som motsvarar :q:. Listar alla terminal-sessioner i ditt konto. 