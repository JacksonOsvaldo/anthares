��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     	     8	     J	  %  \	  �  �
  *   `  i   �     �  '        3  z   <  �   �     �     �  $   �  !   �                               
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-02-09 18:52+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <sv@li.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 &Avsluta aktuell session Sta&rta om datorn S&täng av datorn <h1>Sessionshanteraren</h1> Här kan du anpassa sessionshanteraren. Detta omfattar alternativ som till exempel om sessionsavslutningen (utloggningen) ska bekräftas, om sessionen ska återställas igen vid inloggning och om datorn normalt ska stängas av automatiskt efter sessionen avslutats. <ul>
<li><b>Återställ föregående session:</b> Sparar alla program som kör vid avslutning och återställer dem vid nästa start.</li>
<li><b>Återställ manuellt sparad session:</b> Tillåter att sessionen sparas när som helst med "Spara session" i K-menyn. Det här betyder att program som för tillfället har startats återfås vid nästa start.</li>
<li><b>Starta med tom session:</b> Spara inte någonting. Börjar med ett tomt skrivbord vid nästa start.</li>
</ul> Program som ska &undantas från sessioner: Markera det här alternativet om du vill att sessionshanteraren ska visa en bekräftande utloggningsruta. Bekräfta &utloggning Förvalt alternativ när datorn lämnas Allmänt Här kan du välja vad som normalt ska ske när du loggar ut. Det här har bara någon betydelse om du loggade in via KDM. Här kan du skriva in en lista av program åtskilda med kolon eller kommatecken, som inte ska sparas i sessioner, och därför inte startas igen när en session återställs. Till exempel 'xterm:xconsole' eller 'xterm,xconsole'. &Erbjud avstängningsalternativ Vid inloggning Återställ &manuellt sparad session Återställ före&gående session Starta med en tom &session 