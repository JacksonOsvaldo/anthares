��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  G   1     y     �     �     �     �     �  	   �     �  '        )     2  +   N     z  $   �     �     �  
   �  1   �     �          $  '   *     R     c  *   p  /   �     �     �     �     �          /     >     J     b     z     �  �   �  I   �  "   �  
   �  (        .     F     _     z     �  !   �  	   �     �     �            
             9     @     F     N     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-04-17 19:28+0200
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 

Välj föregående serie för att gå till den senast lagrade serien. S&kapa seriearkiv... &Spara serie som... &Serienummer: *.cbz|Seriearkiv (zip) &Verklig storlek Lagra nuvarande &position Avancerat Alla Ett fel uppstod för identifieraren %1. Utseende Misslyckades arkivera serie Uppdatera serieinsticksprogram automatiskt: Lagring Kontrollera om det finns nya serier: Serie Lokal serielagring: Anpassa... Kunde inte skapa arkivet på den angivna platsen. Skapa %1 seriearkiv Skapar seriearkiv Mål: Visa fel när serien inte kunde hämtas Ladda ner serier Felhantering Misslyckades lägga till en fil i arkivet. Misslyckades skapa filen med identifieraren %1. Från början till... Från slutet till... Allmänt Hämta nya serier... Misslyckades hämta serie: Gå till serie Information Gå till &aktuell serie Gå till &första serie Gå till serie... Manuellt intervall Kanske finns det ingen anslutning till Internet.
Kanske är det ett fel i serieinsticksprogrammet.
En annan orsak kan vara att det inte finns någon serier för den här dagen, numret eller strängen, så att välja en annan kan fungera. Klicka med mittenknappen på serien för att visa den med originalstorlek Det finns ingen zip-fil, avbryter. Intervall: Visa bara pilar när musen hålls stilla Visa seriens webbadress Visa seriens författare Visa seriens identifierare Visa seriens namn Seriens identifierare: Intervall av serier att arkivera. Uppdatera Besök seriens webbplats Besök affärens &webbplats Nr. %1 dagar yyyy-MM-dd &Nästa flik med en ny serie Från: Till: minuter serier per serietyp 