��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �     �  
   �     �                         4  o   :     �  4   �  	   �     �     
       
        $     0     >     T     k     �     �     �  &   �     �     �                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-09 14:53+0100
Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Välj... Rensa ikon Lägg till i favoriter Alla program Utseende Program Program uppdaterade. Dator Dra flikar mellan rutorna för att visa eller dölja dem, eller ordna om de synliga flikarna genom att dra dem. Redigera program... Expandera sökning till bokmärken, filer och e-post Favoriter Dolda flikar Historik Ikon: Gå ifrån Menyknappar Ofta använda För alla aktiviteter För aktuell aktivitet Ta bort från favoriter Visa i favoriter Visa program med namn Sortera alfabetiskt Byt flikar när musen hålls över dem Skriv för att söka... Synliga flikar 