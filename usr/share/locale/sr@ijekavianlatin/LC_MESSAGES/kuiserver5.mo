��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       1  %  #   W  /   {     �     �     �     �     �  
               	         *  '   <     d     �     �  	   �     �     �     �     �     �          .     A                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-06-30 14:15+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1 fajl %1 fajla %1 fajlova %1 fajl %1 fascikla %1 fascikle %1 fascikli %1 fascikla Obrađeno %1 od %2 %1 od %2 obrađeno pri %3/s Obrađeno %1 Obrađeno %1 pri %2/s Izgled Ponašanje Odustani Očisti Podesi... Završeni poslovi Spisak prenosa fajlova i poslova u toku Premjesti ih u drugi spisak Premiještanje u drugi spisak. Pauziraj Ukloni ih Uklanjanje. Nastavi Svi poslovi u spisku Prikaži sve poslove u spisku. Svi poslovi u stablu Prikaži sve poslove u stablu. Razdvojeni prozori Prikaži razdvojene prozore. 