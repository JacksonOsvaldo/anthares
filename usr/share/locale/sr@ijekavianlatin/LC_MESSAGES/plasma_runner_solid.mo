��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  ;  �     �        %     Q   5  E   �  I   �  M   	  X   e	  X   �	     
     (
     0
  
   7
     B
  
   K
  
   V
     a
     w
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-05 01:21+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Zaključaj sadržalac Izbaci medijum Nalazi uređaje čije ime poklapa :q: Nabraja sve uređaje i omogućava njihovo montiranje, demontiranje i izbacivanje. Nabraja sve uređaje koji se mogu izbaciti i omogućava da se izbace. Nabraja sve uređaje koji se mogu montirati i omogućava da se montiraju. Nabraja sve uređaje koji se mogu demontirati i omogućava da se demontiraju. Nabraja sve šifrovane uređaje koji se mogu zaključati i omogućava da se zaključaju. Nabraja sve šifrovane uređaje koji se mogu otključati i omogućava da se otključaju. Montiraj uređaj uređaj izbaci zaključaj montiraj otključaj demontiraj Otključaj sadržalac Demontiraj uređaj 