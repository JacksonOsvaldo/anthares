��    B      ,  Y   <      �     �     �     �  !   �  "   �  &     ,   /  #   \  &   �  !   �  &   �  '   �  "     "   ;  '   ^     �  +   �  2   �     �  
   �     
          %     ,     @     H     O     b     {  !   �  +   �     �     �      �     	     (	     F	     U	     ]	  !   s	     �	  	   �	     �	     �	     �	     �	  &   �	     !
     @
     G
     b
     h
     p
     w
     |
     �
  $   �
     �
     �
     �
     �
          (     5  >   O  8  �     �     �  !   �     �     �  	   �               "  	   .     8     E     Q     X     ]     i  -   o  @   �     �  	   �     �          "     *     A     I     O     a     y     �  +   �     �     �  +   �  	         "     C     P     W     r     �     �     �  $   �     �     �            (     I     P     l     r     x  	   �     �     �     �     �  !   �                )     @     O  	   o     '             6      8             $       %   <   ,      B                     "   >       #              	   @      =   7   3                          +      
   &   !               0   :      ;           (   4              2                      A       -   )      9          *         1       /          .   ?   5         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2017-12-17 18:00+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
  ms % &Svojstvo prozora za poklapanje:  ogromne velike bez ivice bez bočne ivice normalne neizmijerne sićušne vrlo ogromne vrlo velike velika mala vrlo velika Dodaj Ručka za promenu veličine prozora bez ivica Maksimizovanim prozorima može da se menja veličina preko ivica &Trajanje animacija: Animacije Veličina &dugmadi: Veličina ivica: sredina sredina (pune širine) Klasa:  Boja: Opcije dekoracije Otkrij svojstva prozora Dijalog Krug oko dugmeta za zatvaranje Razdvajač između naslovne trake i prozora Preliv u pozadini prozora Uredi Uređivanje izuzetka — postavke Povetarca Animacije Uključi/isključi ovaj izuzetak tip izuzetka Opšte Bez naslovne trake prozora Podaci o izabranom prozoru lijevo Pomjeri nadolje Pomjeri nagore Novi izuzetak — postavke Povetarca Pitanje — postavke Povetarca regularni izraz Loša sintaksa regularnog izraza &Regularni izraz za poklapanje:  Ukloni Ukloniti izabrani izuzetak? desno Senke &Veličina: sićušna &Poravnanje naslova: Naslov:  Po klasi prozora (cio program) Po naslovu prozora Upozorenje — postavke Povetarca ime klase prozora Identifikacija prozora Izbor svojstva prozora naslov prozora Potiskivanja posebna po prozoru &Jačina: 