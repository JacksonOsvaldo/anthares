��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     P  b     �     �     �  	   �     �  	   �     �  
     4        G     a  "   p     �  3   �  3   �       
          
   &     1     C  %   \     �     �     �     �     �     �     �     �     �          "     7     S     Z     f  #   m     �     �  
   �  !   �     �     �               2     :     H     [     o     �  ,   �     �     �     �     �     �               +  7   3  !   k      �     �      �  /   �  
        *  	   /     9     O     U     ^  	   e     o  $   �     �     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-04-07 00:51+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavianlatin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &sve površi &Zatvori Preko &celog ekrana &Premesti &nova površ &Prikači &Namotaj &%1 — %2 Takođe dostupan u %1|/|Takođe dostupan u $[lok %1] Dodaj u tekuću aktivnost Sve aktivnosti Dozvoli grupisanje za ovaj program abecedno Uvek rasporedi zadatke po kolonama sa ovoliko vrsta Uvek rasporedi zadatke po vrstama sa ovoliko kolona Raspored Ponašanje po aktivnosti po površi po imenu programa zatvori prozor ili grupu Kruži kroz zadatke na točkić miša bez grupisanja bez ređanja Filteri Zaboravi nedavne dokumente Opšte Grupisanje i ređanje Grupisanje: Istakni prozore Veličina ikonica: Drži iz&nad ostalih Drži is&pod ostalih Drži pokretače razdvojeno velike Ma&ksimizuj ručno Označi programe koji puštaju zvuk Najviše kolona: Najviše vrsta: Mi&nimizuj minimizuj/obnovi prozor ili grupu Više radnji Premesti na &tekuću površ Premesti u &aktivnost Premesti na &površ Utišaj novi primerak U %1|/|U $[lok %1] U svim aktivnostima U tekućoj aktivnosti Na srednji klik: Grupiši samo kada je menadžer zadataka pun Otvaraj grupe u iskakačima Obnovi Pauziraj Sledeća numera Prethodna numera Napusti Promeni &veličinu Otkači Još %1 mesto Još %1 mesta Još %1 mesta Još %1 mesto Samo zadaci iz tekuće aktivnosti Samo zadaci na trenutnoj površi Samo zadaci na trenutnom ekranu Samo zadaci koji su minimizovani Podaci o napretku i stanju u dugmadima zadataka Oblačići male Ređanje: Pokreni novi primerak Pusti Zaustavi ništa &Prikači grupiši/razgrupiši Dostupan u %1|/|Dostupan u $[lok %1] Dostupan u svim aktivnostima 