��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  A  {     �     �     �     �     �     �     �                 o   )     �  	   �     �     �     �     �     �               .  i   6                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-24 13:57+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1 u pogonu %1 nije u pogonu &Resetuj &Pokreni Napredno Izgled Naredba: Prikaz Izvrši naredbu Obaveštenja Preostalo vreme: %1 sekunda Preostalo vreme: %1 sekunde Preostalo vreme: %1 sekundi Preostalo vreme: %1 sekunda Izvršavanje naredbe &Zaustavi Prikaži obaveštenje Prikaži sekunde Prikaži naslov Tekst: Odbrojavač Odbrojavanje završeno Odbrojavač u pogonu Naslov: Menjajte cifre točkićem miša, ili izaberite neki od predefinisanih odbrojavača iz kontekstnog menija. 