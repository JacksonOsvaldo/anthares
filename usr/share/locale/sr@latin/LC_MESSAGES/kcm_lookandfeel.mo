��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  .  \      �  <   �  #   �          #  (   B     k       *   �     �     �     �     �     	  �   #	     �	  l   �	     1
  m   M
  ?   �
     �
                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-24 21:47+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Primeni paket izgleda i osećaja Alatka komandne linije za primenu paketa izgleda i osećaja. Podesite detalje izgleda i osećaja © 2017, Marko Martin Izmenjene postavke pokazivača Preuzmite nove pakete izgleda i osećaja caslav.ilic@gmx.net Dobavi nove izglede... Nabroji dostupne pakete izgleda i osećaja Alatka za izgled i osećaj Održavalac Marko Martin Časlav Ilić Resetuj raspored plasma površi Izaberite temu za radni prostor (uključujući temu Plasme, šemu boja, pokazivač miša, menjač prozora i površi, uvodni ekran, zabravni ekran, itd.) Pregled Ovaj modul vam omogućava podešavanje izgleda celog radnog prostora, uz nekoliko spremnih pretpodešavanja. Raspored površi prema temi Upozorenje: vaš raspored plasma površi biće izgubljen i resetovan na podrazumevani raspored izabrane teme. Ponovo pokrenite KDE da bi izmene pokazivača stupile na snagu. ime‑paketa 