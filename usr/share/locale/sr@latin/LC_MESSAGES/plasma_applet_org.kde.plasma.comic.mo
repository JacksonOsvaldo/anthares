��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  2  �  ?   �     �          )     7     Q     d     ~     �  )   �     �     �  *   �               (     .  	   =  /   G     w     �     �  -   �     �     �  "     /   2     b     t     �     �     �     �     �     �     �            �   &  4   �          7  !   >  
   `     k     x     �     �  (   �     �     �     �             
   %     0     M     Q     U     \     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-14 17:59+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 

Izaberite prethodni strip da pređete na poslednji keširani. &Napravi arhivu stripa... &Sačuvaj strip kao... Broj &stripa: *.cbz|arhiva stripa (ZIP) &Stvarna veličina Zapamti tekući &položaj Napredno svi Došlo je do greške za identifikator %1. Izgled Arhiviranje stripa nije uspelo Automatski ažuriraj stripske priključke: Keš Traži nove kaiševe stripa: Strip Keš stripova: Podesi... Ne mogu da napravim arhivu na zadatoj lokaciji. Stvaranje arhive stripa %1 Stvaranje fajla arhive stripa Odredište: Prikaz greške kad ne uspe dobavljanje stripa Preuzimanje stripova Obrada grešaka Neuspelo dodavanje fajla u arhivu. Neuspelo stvaranje fajla sa identifikatorom %1. od početka do... od kraja do... Opšte Dobavi nove stripove... Dobavljanje stripa nije uspelo: Skok na strip Podaci Skoči na &tekući strip Skoči na &prvi strip Skoči na strip... ručni opseg Možda ne radi veza sa Internetom.
Možda je priključak stripa pokvaren.
Takođe može biti da nema stripa za ovaj dan, broj ili nisku, pa dobavljanje može uspeti ako izaberete neki drugi. Srednji klik na strip daje njegovu izvornu veličinu Nema ZIP fajla, obustavljam. Opseg: Strelice samo pri prelasku mišem URL stripa Autor stripa Identifikator stripa Naslov stripa Identifikator stripa: Opseg stripskih kaiševa za arhiviranje. Ažuriranje Poseti veb sajt stripa Poseti &veb sajt prodavnice br. %1 dana dd.MM.yyyy &Novi jezičak za novi strip Od: Do: minuta kaiševa po stripu 