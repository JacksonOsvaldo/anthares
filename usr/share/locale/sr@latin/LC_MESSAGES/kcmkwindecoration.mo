��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  ]  p     �     �  	   �     �     �  	     	             #     /     =     O     V  Q   ^  )   �     �  7   �  !   $	     F	     `	     l	  
   x	     �	  	   �	     �	     �	     �	     �	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-14 18:13+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Associated-UI-Catalogs: kwidgetsaddons5_qt
X-Environment: kde
 ogromne velike bez ivica bez bočnih ivica normalne neizmerne sićušne vrlo ogromne vrlo velike Meni programa Veličina &ivica: Dugmad Zatvori Zatvorite dvoklikom.
Da otvorite meni, držite dugme pritisnuto dok se ne pojavi. Zatvori prozor dvoklikom na &dugme menija Kontekstna pomoć Prevlačite dugmad između ovog mesta i naslovne trake. Uklonite dugme ispuštanjem ovde. Dobavi nove dekoracije... Drži iznad Drži ispod Maksimizuj Meni Minimizuj Na sve površi Traži Namotaj Tema Naslovna traka 