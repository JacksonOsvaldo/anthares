��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  1  �  !   �          !  	   (     2     C  	   L  	   V     `     m     y     �     �     �     �     �  -   �  @   �  	   (     2     F  "   W     z     �     �  ]   �  8   �     8     J     b     j  +   p      �     �     �     �     �                 $   (     M     l      |      �     �     �     �     �     �       .        :     Y  !   l     �     �     �     �     �  ,   �     "     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: oxygen_kdecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-17 18:00+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &Svojstvo prozora za poklapanje:  ogromne velike bez ivice bez bočne ivice normalne neizmerne sićušne vrlo ogromne vrlo velike velika normalna mala vrlo velika Sijanje aktivnog prozora Dodaj Ručka za promenu veličine prozora bez ivica Maksimizovanim prozorima može da se menja veličina preko ivica Animacije Veličina &dugmadi: Veličina ivica: Promena dugmeta na prelazak mišem sredina sredina (pune širine) Klasa:  Podesite pretapanje između senke i sijanja prozora kada se promeni njegovo stanje aktivnosti Podesite animaciju isticanja dugmadi pri prelasku mišem Opcije dekoracije Otkrij svojstva prozora Dijalog Uredi Uređivanje izuzetka — postavke Kiseonika Uključi/isključi ovaj izuzetak tip izuzetka Opšte Bez naslovne trake prozora Podaci o izabranom prozoru levo Pomeri nadole Pomeri nagore Novi izuzetak — postavke Kiseonika Pitanje — postavke Kiseonika regularni izraz Loša sintaksa regularnog izraza &Regularni izraz za poklapanje:  Ukloni Ukloniti izabrani izuzetak? desno Senke &Poravnanje naslova: Naslov:  Ista boja za naslovnu traku i sadržaj prozora Po klasi prozora (ceo program) Po naslovu prozora Upozorenje — postavke Kiseonika ime klase prozora Padajuća senka prozora Identifikacija prozora Izbor svojstva prozora naslov prozora Prelaz pri promeni stanja aktivnosti prozora Potiskivanja posebna po prozoru 