��    
      l      �       �      �            	               .  I   3     }  	   �  ?  �     �     �     �     �              B   '     j     y               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-08-26 09:22+0200
Last-Translator: Dalibor Djuric <daliborddjuric@gmail.com>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 &Okidačka reč: Dodaj stavku alijas Alijas: Podešavanje izvođača znakova kô̂d Daje znak prema :q: ako je heksadekadni kod ili definisani alijas. Obriši stavku Heksadekadni kod: 