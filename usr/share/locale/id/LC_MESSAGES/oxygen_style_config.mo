��    <      �  S   �      (     )  !   F  !   h     �  
   �     �     �  (   �  I   �  :   H  <   �  2   �  .   �  1   "  (   T  1   }  o   �       !   &     H  0   h     �     �     �     �     	     	  ,    	     M	     Z	     q	     w	  !   	     �	     �	     �	     �	  	   �	  
   �	  
   �	     
     
     +
     G
  
   Y
  &   d
     �
     �
     �
     �
     �
     �
     �
               1     4     I     ]  �  p       '   1  %   Y          �     �     �  *   �  G   �  5   6  :   l  6   �  0   �  0     (   @  6   i  z   �       $   "  $   G  9   l     �     �  %   �  )        /     5  4   C     x     �     �     �      �     �  
   �     �  
   �     �               &     5     J     g     |  .   �     �     �     �     �  
             '     F     O     d     g     n     t     0   !   9   :                                  	   ,       
   2   #   .                 7          <   3   5          /      $   (          '   )      1              8             6       *                                                   4      "   %   +                     ;             -   &    &Use selection color (plain) Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Animation type: Animations Bottom arrow button type: Combo box transitions Configure fading transition between tabs Configure fading transition when a combo box's selected choice is changed Configure fading transition when a label's text is changed Configure fading transition when an editor's text is changed Configure menu bars' mouseover highlight animation Configure menus' mouseover highlight animation Configure progress bars' busy indicator animation Configure progress bars' steps animation Configure toolbars' mouseover highlight animation Configure widgets' focus and mouseover highlight animation, as well as widget enabled/disabled state transition Dialog Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw toolbar item separators Draw tree branch lines Draw window background gradient Enable extended resize handles Fade Fade duration: Focus, mouseover and widget state transition Follow Mouse Follow mouse duration: Frame General Keyboard accelerators visibility: Label transitions Menu Highlight Menu bar highlight Menu highlight No button No buttons One button Oxygen Settings Progress bar animation Progress bar busy indicator Scrollbar wi&dth: Scrollbars Show Keyboard Accelerators When Needed Tab transitions Text editor transitions Toolbar highlight Top arrow button type: Two buttons Use dar&k color Use selec&tion color (subtle) Views Windows' &drag mode: px triangle sizeNormal triangle sizeSmall triangle sizeTiny Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2018-01-22 22:54+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Gunakan warna pilihan (biasa) Selalu Sembunyikan Akselerator Keyboard Selalu Tampilkan Akselerator Keyboard Tipe animasi: Animasi Tipe tombol panah bawah: Transisi kotak kombo Konfigurasi transisi memudar di antara tab Konfigurasi transisi memudar ketika pilihan terpilih kotak kombo diubah Konfigurasi transisi memudar ketika label teks diubah Konfigurasi transisi memudar ketika penyunting teks diubah Konfigurasi animasi sorot perpindahan mouse bilah menu Konfigurasi animasi sorot perpindahan mouse menu Konfigurasi animasi indikator sibuk bilah proses Konfigurasi animasi langkah bilah proses Konfigurasi animasi sorot perpindahan mouse bilah alat Konfigurasi widget fokus dan animasi sorot perpindahan mouse, begitu pula transisi kondisi widget diaktifkan/dinonaktifkan Dialog Tarik jendela dari semua area kosong Tarik jendela hanya dari bilah judul Tarik jendela dari bilah judul, bilah menu dan bilah alat Gambar pemisah item bilah alat Gambar baris cabang pohon Gambar gradasi latar belakang jendela Aktifkan penanganan ubah ukuran diperluas Pudar Durasi pudar: Transisi kondisi fokus, perpindahan mouse dan widget Ikuti Mouse Ikuti durasi mouse: Bingkai Umum Ketampakan akselerator keyboard: Transisi label Sorot Menu Sorot bilah menu Sorot menu Tak ada tombol Tak ada tombol Satu tombol Setelan Oxygen Animasi bilah proses Indikator bilah proses sibuk Lebar bilah &gulung: Bilah gulung Tampilkan Akselerator Keyboard Jika Dibutuhkan Transisi tab Transisi penyunting teks Sorot bilah alat Tipe tombol panah atas: Dua tombol Gunakan warn&a gelap Gunakan war&na pilihan (tipis) Tampilan Mode &tarik jendela: px Normal Kecil Mungil 