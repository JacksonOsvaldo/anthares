��    (      \  5   �      p     q     y     �     �     �     �     �  	   �     �     �     �               7     N     V  
   h  
   s     ~     �     �     �     �     �     �     �     �     �       U     Z   q  O   �          )     0  	   <  	   F     P     X  �  \     �     �          $     +     1     ?  	   U     _     }     �     �     �     �     �     �  
   	     	      	     (	     ;	     I	     O	     ]	     f	     l	     �	     �	     �	     �	     �	     
     !
     .
  
   ;
  
   F
  
   Q
  	   \
     f
                 
               "         (                                             %                   &                 #       '      $      !   	                                Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2017-12-27 21:56+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Adaptor Tambah Peranti Baru Tambah Peranti Baru... Alamat Audio Peranti audio Peranti yang tersedia Bluetooth Bluetooth telah Dinonaktifkan Bluetooth telah dinonaktifkan Bluetooth telah offline Telusuri File Konfigurasi &Bluetooth... Konfigurasi Bluetooth... Sambung Peranti tersambung Menyambung Putus Memutus Aktifkan Bluetooth Transfer file Input Peranti input Jaringan Tidak Tiada Adaptor yang Tersedia Tiada Peranti yang Ditemukan Tiada adaptor yang tersedia Tiada peranti tersambung Sambungan ke peranti gagal Peranti tidak terjangkau Peranti belum siap Peranti lain Disandingkan Nama Remot Kirim File Kirim file Dipercaya Ya 