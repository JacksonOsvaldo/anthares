��          �   %   �      P  9   Q     �     �     �  7   �  	                  /     ;     N     f     t     |     �     �     �  #   �  %   �       5   "     X     g     u  )   }  �  �     f     w     �      �     �  
   �     �     �     �               /     <     A     T     [     t     �     �     �  ;   �     �     �                        	                                                           
                                                    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging &Configure Power Saving... Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The battery applet has enabled system-wide inhibition Time To Empty: Time To Full: Vendor: battery percentage below battery icon%1% Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2017-12-27 22:08+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 Tersisa (%2%) %1% Baterai Tersisa %1%. Mengisi &Konfigurasi Penghematan Daya... Tak ada Kapasitas: Mengisi Konfigurasi Penghematan Daya... Tidak Mengisi Kecerahan Tampilan Aktifkan Pengelolaan Daya Penuh Terisi Umum Kecerahan Keyboard Model: Tak Ada Baterai Tersedia Tidak Mengisi %1% %1% Pengelolaan daya dinonaktifkan Applet baterai telah mengaktifkan pencegahan seluruh sistem Waktu Hingga Habis: Waktu Hingga Penuh: Penjual: %1% 