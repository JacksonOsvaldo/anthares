��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �  !   �  �   �  ^   u     �  	   �     �     �       	                  /     B     W     j     z     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-06-25 05:03-0400
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Redupkan layar setengah Redupkan layar secara keseluruhan Tampilkan opsi kecerahan layar atau atur ke kecerahan yang ditentukan oleh :q:; misalnya kecerahan layar 50 akan meredupkan layar ke kecerahan maksimum 50% Tampilkan opsi suspensi sistem (misalnya tidur, hibernasi) dan izinkan mereka untuk diaktifkan redupkan layar hibernasi kecerahan layar tidur suspensi ke cakram ke ram redupkan layar %1 kecerahan layar %1 Atur Kecerahan ke %1 Suspensi ke Cakram Suspensi ke RAM Suspensi sistem ke RAM Suspensi sistem ke cakram 