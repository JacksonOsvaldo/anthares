��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     V     e  #   v  \   �  T   �  Q   L  [   �  [   �  W   V	     �	     �	     �	     �	     �	  
   �	     �	     �	     
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-03 06:07+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Gembok pengisi Keluarkan medium Cari peranti yang namanya cocok :q: Tampilkan semua peranti dan izinkan mereka untuk dikaitkan, dilepas kaitan atau dikeluarkan. Tampikan semua peranti yang dapat dikeluarkan, dan izinkan mereka untuk dikeluarkan. Tampilkan semua peranti yang dapat dikaitkan, dan izinkan mereka untuk dikaitkan. Tampilkan semua peranti yang dapat dilepas kaitan, dan izinkan mereka untuk dilepas kaitan. Tampilkan semua peranti terenkripsi yang dapat digembok, dan izinkan mereka untuk digembok. Tampilkan semua peranti yang dapat dibuka kunci, dan izinkan mereka untuk dibuka kunci. Kaitkan peranti peranti keluar kunci kait buka kunci lepas kaitan Buka kunci pengisi Lepas kaitan peranti 