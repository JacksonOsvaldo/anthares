��          �      <      �  3   �  $   �  �   
     �  4   �     �  #   �       �     �   �  +   b     �     �     �  �   �  $   Z  (     �  �     n     �     �     �  4   �  #   �  $        *  �   /  �   �  3   �	     �	  $   �	     
     /
  (   ?
  (   h
                                                     	   
                                             1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-12-19 07:30+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 aksi untuk peranti ini %1 bebas Mengakses... Semua peranti Klik untuk mengakses peranti ini dari aplikasi lain. Klik untuk mengeluarkan cakram ini. Klik untuk mengeluarkan peranti ini. Umum Saat ini <b>tidak aman</b> untuk mengeluarkan peranti ini: aplikasi mungkin sedang mengaksesnya. Klik tombol keluar untuk mengeluarkan peranti ini dengan aman. Saat ini <b>tidak aman</b> untuk mengeluarkan peranti ini: aplikasi mungkin sedang mengakses volume lain di peranti ini. Klik tombol keluar pada volume lain tersebut untuk mengeluarkan peranti ini dengan aman. Saat ini telah aman untuk mengeluarkan peranti ini. Tak Ada Peranti Tersedia Hanya peranti yang tak dapat dilepas Hanya peranti dapat dilepas Mengeluarkan... Peranti ini tidak bisa diakses saat ini. Peranti ini tidak bisa diakses saat ini. 