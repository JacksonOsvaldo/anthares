��             +         �     �  $   �     �     �  
             $     8     H     O     c     {     �  &   �      �  	   �  �   �  �   �  �   #  �   �     I     N     ]     p     �     �     �     �     �  S   �  G   L  �  �     H	  &   Q	     x	  
   	  
   �	     �	     �	     �	  
   �	     �	     �	     
     
  *   "
     M
     h
  �   t
  �     �   �  �   B     �     �     �     �          *     D     `     s  P   |  H   �                                                                                                          	                               
       &Folder: &Microsoft® Windows® network drive &Name: &Port: &Protocol: &Recent connection: &Secure shell (ssh) &Use encryption &User: &WebFolder (webdav) (c) 2004 George Staikos Add Network Folder C&onnect Cr&eate an icon for this remote folder EMAIL OF TRANSLATORSYour emails Encoding: Enter a name for this <i>File Transfer Protocol connection</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Microsoft Windows network drive</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Secure shell connection</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>WebFolder</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. FT&P George Staikos KDE Network Wizard NAME OF TRANSLATORSYour names Network Folder Information Network Folder Wizard Primary author and maintainer Save && C&onnect Se&rver: Select the type of network folder you wish to connect to and press the Next button. Unable to connect to server.  Please check your settings and try again. Project-Id-Version: knetattach
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-10 05:48+0100
PO-Revision-Date: 2017-12-24 14:59+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Folder: Peranti jaringan Microsoft® Windows® &Nama: P&angkalan &Protokol: &Koneksi terkini: &Shell aman (ssh) &Gunakan enkripsi &Pengguna: &FolderWeb (webdav) (c) 2004 George Staikos Tambah Folder Jaringan Sa&mbung &Ciptakan ikon untuk folder jarak jauh ini andhika.padmawan@gmail.com Penyandian: Masukkan nama untuk <i>koneksi File Transfer Protocol</i> beserta alur server dan alur folder untuk digunakan lalu tekan tombol <b>Simpan & Sambung</b>. Masukkan nama untuk <i>peranti jaringan Microsoft Windows</i> beserta alur server dan alur folder untuk digunakan lalu tekan tombol <b>Simpan & Sambung</b>. Masukkan nama untuk <i>koneksi shell Aman</i> beserta alur server, pangkalan dan alur folder untuk digunakan lalu tekan tombol <b>Simpan & Sambung</b>. Masukkan nama untuk <i>FolderWeb</i> ini beserta alur server, pangkalan dan alur folder untuk digunakan lalu tekan tombol <b>Simpan & Sambung</b>. FT&P George Staikos Penyihir Jaringan KDE Andhika Padmawan Informasi Folder Jaringan Pengelola Folder Jaringan Penulis dan pengelola utama Simpan && Sa&mbung Se&rver: Pilih tipe folder jaringan yang ingin anda sambung lalu tekan tombol Berikutnya. Tak dapat menyambung ke server.  Silakan cek setelan anda lalu coba lagi 