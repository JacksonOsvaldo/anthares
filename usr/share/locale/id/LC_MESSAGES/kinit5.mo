��          �            h     i  $   �     �      �     �                >     K  '   i  �   �  �   E     �  t     �  �  *   $  )   O  !   y  !   �  "   �     �  "   �          %  2   @  �   s  �   "     �     �                           
                                	                     Could not find '%1' executable. Could not find 'kdemain' in '%1'.
%2 Could not find service '%1'. Could not find the '%1' plugin.
 Could not open library '%1'.
%2 Error loading '%1'. KDEInit could not launch '%1' Launching %1 Service '%1' is malformatted. Service '%1' must be executable to run. Unable to create new process.
The system may have reached the maximum number of processes possible or the maximum number of processes that you are allowed to use has been reached. Unable to start new process.
The system may have reached the maximum number of open files possible or the maximum number of open files that you are allowed to use has been reached. Unknown protocol '%1'.
 klauncher: This program is not supposed to be started manually.
klauncher: It is started automatically by kdeinit5.
 Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-04 03:08+0100
PO-Revision-Date: 2018-01-26 20:45+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Tak dapat menemukan '%1' dapat dieksekusi. Tak dapat menemukan 'kdemain' di '%1'.
%2 Tak dapat menemukan layanan '%1'. Tak dapat menemukan plugin '%1'.
 Tak dapat membuka pustaka '%1'.
%2 Galat memuat '%1'. KDEInit tak dapat menjalankan '%1' Menjalankan %1 Layanan '%1' salah format. Layanan '%1' harus dapat dieksekusi agar berjalan. Tak dapat memulai proses baru.
Sistem mungkin telah mencapai jumlah maksimum proses yang mungkin dijalankan atau jumlah proses maksimum yang dapat ada gunakan telah tercapai. Tak dapat memulai proses baru.
Sistem mungkin telah mencapai jumlah maksimum berkas yang mungkin dibuka atau jumlah maksimum berkas dibuka yang dapat anda gunakan telah tercapai. Protokol '%1' tak diketahui.
 klauncher: Program ini tidak seharusnya dijalankan secara manual.
klauncher: Program dijalankan secara otomatis oleh kdeinit5.
 