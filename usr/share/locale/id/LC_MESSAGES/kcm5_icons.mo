��    >        S   �      H     I     R     [     j     |     �  �  �  �   y  -   	  )   3	  -   ]	  +   �	  r   �	  	   *
  	   4
     >
     V
     ^
     g
  
   t
     
     �
     �
     �
      �
     �
     �
     �
     �
      
     +     1  r   L  5   �     �                -     L  	   Q     [     a     i  (   v     �     �     �     �     �       +     3   9     m     u     �     �  "   �  S   �  )        9  �   E  �  #     �     �     �     �     �     �  �    �   �  *   d     �     �     �  f   �       
        !     9     @  
   G  
   R  	   ]     g     o     v  ,   �     �     �     �     �      �            �   7  3   �     �            !   0     R  
   W     b     h  
   q  '   |     �     �     �     �     �  
     #     4   4     i     y  
   �     �  (   �  F   �  #        6  �   E     )            5       !                 
   8             2   -          $   %      ;   4              <      1          =          6   7       &       #       	            3      ,            :   (       9                    0   '   /              .       +       *                 >         "        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get New Themes... Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-12-24 15:02+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Jumlah &Efek: Warna kedua: &Semi-transparan &Tema (c) 2000-2003 Geert Jansen <h1>Ikon</h1>Modul ini akan memilih ikon untuk desktop anda.<p>Untuk memilih sebuah tema ikon, tekan pada nama tema dan terapkan pilihan anda dengan menekan tombol "Terapkan" di bawah. Jika anda tidak ingin menerapkan pilihan anda tekan tombol "Atur Kembali" untuk membuang perubahan.</p><p>Dengan menekan tombol "Pasang Tema Baru..." anda dapat memasang tema ikon baru anda dengan cara menuliksan alamatnya atau merambah lokasi tema. Tekan Tekan tombol "Oke" untuk menyelesaikan proses pemasangan.</p><p>Tombol "Hapus Tema" akan aktif jika anda memasang tema menggunakan  modul ini. Anda tidak dapat menghapus tema yang tepasang secara global di sini.</p><p>Anda juga dapat menentukan efek yang ingin digunakan pada ikon.</p> <qt>Apakah anda yakin ingin menghapus temap <strong>%1</strong>?<br /><br />Ini akan menghapus berkas yang dipasang oleh tema ini.</qt> <qt>Memasang tema <strong>%1</strong></qt> Aktif Nonaktifkan Baku Sebuah permasalahan terjadi dalam proses pasangasi; namun hampir semua tema dari arsip sudah terpasang Lanjutan Semua ikon Antonio Larrosa Jimenez Warna: Warnai Konfirmasi Desaturate Deskripsi Desktop Dialog Seret atau Ketikkan URL Tema keripix@gmail.com,andhika.padmawan@gmail.com Parameter Efek Gamma Geert Jansen Dapatkan Tema Baru... Dapatkan tema baru dari Internet Ikon Modul Control Panel Ikon Jika anda memiliki tema pada sistem lokal, tombol ini akan memuat tema tersebut dan menjadikannya dapat digunakan oleh aplikasi KDE lainnya. Pasang berkas tema yang sudah ada pada sistem lokal Memasang tema ikon... Jonathan Riddell Bilah Alat utama A. Akbar Hidayat,Andhika Padmawan Nama Tanpa efek Panel Pratilik Hapus Tema Hapus tema yang terpilih dari disk anda Pasang Efek... Setelan Efek Ikon Aktif Setelan Efek Ikon Baku Setelan Efek Ikon Nonaktif Ukuran: Ikon Kecil Berkas bukan tema ikon yang valid.  Ini akan menghapus tema yang dipilih dari disk anda. Menjadi abu-abu Menjadi Monokrom Bilah Alat Torsten Rahn Tidak dapat menciptakan berkas temporer. Tidak dapat mengunduh tema ikon;
harap perikas alamat %1 adalah benar. Tidak dapat menemukan tema ikon %1. Pengunaan ikon Anda perlu terhubungkan pada Internet untuk menggunakan aksi ini. Sebuah dialog akan menampilkan daftar tema dari http://www.kde.org. Menekan tombol Pasang pada sebuah tema akan mengninstal tema tersebut pada sistem lokal. 