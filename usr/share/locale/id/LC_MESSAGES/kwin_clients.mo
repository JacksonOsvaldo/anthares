��          �      �       H  "   I  !   l     �     �  �   �  �   +  Z   �     
           .     3     9     J  �  ]     �                 �   !  �   �  m   C     �     �     �     �     �     �                                                  	   
            @item:inlistbox Button size:Large @item:inlistbox Button size:Tiny Animate buttons Center Check this option if the window border should be painted in the titlebar color. Otherwise it will be painted in the background color. Check this option if you want the buttons to fade in when the mouse pointer hovers over them and fade out again when it moves away. Check this option if you want the titlebar text to have a 3D look with a shadow behind it. Colored window border Config Dialog Left Right Title &Alignment Use shadowed &text Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-02 03:16+0100
PO-Revision-Date: 2018-01-22 22:53+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Besar Mungil Animasi tombol Tengah Centang opsi ini jika batas jendela harus digambar dengan warna bilah judul. Sebaliknya batas jendela akan digambar dengan warna latar belakang. Centang opsi ini jika anda ingin agar tombol memudar ketika penunjuk mouse melayang di atas tombol dan muncul lagi ketika penunjuk mouse pindah. Centang opsi ini jika anda ingin teks bilah judul untuk mempunyai tampilan 3D dengan bayangan di belakangnya. Batas jendela berwarna Dialog Konfigurasi Kiri Kanan Perataan &Judul Gunakan &teks berbayang 