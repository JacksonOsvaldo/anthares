��          �      \      �     �  	   �     �  a   �     Q  
   i  	   t     ~     �     �     �      �      �       	   .     8     A     J  7   P  �  �     ;     H     P  u   X     �     �  
   �     �          (     <     O     [     g     n     �     �     �  ;   �                                                                                        
       	    %1 - %2 (%3) &Numbers: &Time: <h1>Formats</h1>You can configure the formats used for time, dates, money and other numbers here. Co&llation and Sorting: Currenc&y: Currency: De&tailed Settings Format Settings Changed Measurement &Units: Measurement Units: Measurement comboboxImperial UK Measurement comboboxImperial US Measurement comboboxMetric No change Numbers: Re&gion: Time: Your changes will take effect the next time you log in. Project-Id-Version: kcmlocale
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:20+0100
PO-Revision-Date: 2017-12-11 23:34+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 - %2 (%3) &Angka: &Waktu: <h1>Format</h1>Anda dapat mengkonfigurasi format yang digunakan untuk waktu, tanggal, uang dan angka lainnya di sini. Ko&lasi dan Pengurutan: Mata Uan&g: Mata Uang: Setelan Terperinci& Setelan Format Berubah Sistem &Pengukuran: Sistem Pengukuran: Imperial UK Imperial AS Metrik Tak ada perubahan Angka: Da&erah: Waktu: Perubahan anda akan berlangsung setelah anda masuk kembali. 