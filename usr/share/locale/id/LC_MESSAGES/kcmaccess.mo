��    8      �  O   �      �     �     �     �     �     �       +     *   I  +   t  #   �     �  J   �     #     ,     9     @  	   L  H   V     �      �     �  �   �  �   �  M   I     �     �  �   �    >	     K
     b
     o
     �
     �
     �
  1   �
  !   �
  .     0   A      r  :   �  #   �  
   �     �       N     H   b  =   �     �     �             5   1  D   g  D   �  .   �  �        �     �     �     �     �     �  3   
  2   >  2   q  #   �     �  J   �     "     *     =     E  
   S  O   ^     �     �     �  �   �  �   �  V   r     �     �  �   �  1  �     �     �     �     �  
          -        J  *   i  ,   �     �  :   �           ;     I     `  P   f  H   �  >         ?     ]     s     �  >   �  I   �  R   '  1   z                  *                 4          )       !                                    -      .      
       0   1   '                         2       /   ,      6   8         &          %   3              $             	   (   "   5                         +      7              #     min  msec &Bell &Keyboard Filters &Lock sticky keys &Use slow keys &Use system bell whenever a key is accepted &Use system bell whenever a key is pressed &Use system bell whenever a key is rejected (c) 2000, Matthias Hoelzer-Kluepfel Activation Gestures All screen colors will be inverted for the amount of time specified below. AltGraph Audible Bell Author Bounce Keys Browse... Click here to choose the color used for the "flash screen" visible bell. Configure &Notifications... EMAIL OF TRANSLATORSYour emails F&lash screen Here you can activate keyboard gestures that turn on the following features: 
Mouse Keys: %1
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can activate keyboard gestures that turn on the following features: 
Sticky keys: Press Shift key 5 consecutive times
Slow keys: Hold down Shift for 8 seconds Here you can customize the duration of the "visible bell" effect being shown. Hyper I&nvert screen If the option "Use customized bell" is enabled, you can choose a sound file here. Click "Browse..." to choose a sound file using the file dialog. If this option is checked, KDE will show a confirmation dialog whenever a keyboard accessibility feature is turned on or off.
Ensure you know what you are doing if you uncheck it, as the keyboard accessibility settings will then always be applied without confirmation. KDE Accessibility Tool Locking Keys Matthias Hoelzer-Kluepfel NAME OF TRANSLATORSYour names Notification Press %1 Press %1 while CapsLock and ScrollLock are active Press %1 while CapsLock is active Press %1 while NumLock and CapsLock are active Press %1 while NumLock and ScrollLock are active Press %1 while NumLock is active Press %1 while NumLock, CapsLock and ScrollLock are active Press %1 while ScrollLock is active Slo&w Keys Sound &to play: Super The screen will turn to a custom color for the amount of time specified below. Turn sticky keys and slow keys off after a certain period of inactivity. Turn sticky keys off when two keys are pressed simultaneously Us&e customized bell Use &sticky keys Use &system bell Use bou&nce keys Use gestures for activating sticky keys and slow keys Use system bell whenever a locking key gets activated or deactivated Use system bell whenever a modifier gets latched, locked or unlocked Use the system bell whenever a key is rejected Project-Id-Version: kcmaccess
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-10 03:17+0100
PO-Revision-Date: 2018-02-05 23:34+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
  men  mdet &Bel &Filter Keyboard &Gembok tombol lekat &Gunakan tombol pelan &Gunakan bel sistem kapanpun sebuah tombol diterima &Gunakan bel sistem kapanpun sebuah tombol ditekan &Gunakan bel sistem kapanpun sebuah tombol ditolak (c) 2000, Matthias Hoelzer-Kluepfel Gerak Aktivasi Semua warna layar akan dibalik selama jeda waktu yang ditentukan di bawah. AltGraf Bel Dapat Didengar Penulis Tombol Pantul Telusur... Klik di sini untuk memilih warna yang digunakan untuk bel tampak "layar kilat". Konfigurasi &Notifikasi... andhika.padmawan@gmail.com Layar ki&lat Di sini anda dapat mengaktifkan gerak keyboard yang mengaktifkan fitur berikut: 
Tombol Mouse: %1
Tombol lekat: Tekan tombol Shift selama 5 kali berturut-turut
Tombol pelan: Tahan Shift selama 8 detik Di sini anda dapat mengaktifkan gerak keyboard yang mengaktifkan fitur berikut: 
Tombol lekat: Tekan tombol Shift selama 5 kali berturut-turut
Tombol pelan: Tahan Shift selama 8 detik Di sini anda dapat menyesuaikan durasi dari efek "bel tampil" yang sedang ditampilkan. Hiper B&alik layar Jika opsi "Gunakan bel yang disesuaikan" diaktifkan, anda dapat memilih berkas suara di sini. Klik "Telusur..." untuk memilih berkas suara menggunakan dialog berkas. Jika opsi ini dicentang, KDE akan menampilkan sebuah dialog konfirmasi kapanpun sebuah fitur aksesibilitas keyboard diaktifkan atau dinonaktifkan.
Pastikan anda mengetahui apa yang anda lakukan jika anda tidak mencentangnya, karena setelan aksesibilitas keyboard akan langsung diterapkan tanpa konfirmasi. Alat Aksesibilitas KDE Menggembok Tombol Matthias Hoelzer-Kluepfel Andhika Padmawan Notifikasi Tekan %1 Tekan %1 ketika CapsLock dan ScrollLock aktif Tekan %1 ketika CapsLock aktif Tekan %1 ketika NumLock dan CapsLock aktif Tekan %1 ketika NumLock dan ScrollLock aktif Tekan %1 ketika NumLock aktif Tekan %1 ketika NumLock, CapsLock dan ScrollLock tak aktif Tekan %1 ketika ScrollLock aktif Tom&bol Pelan Suara &yang dimainkan: Super Layar akan diubah menjadi warna suai selama jeda waktu yang ditentukan di bawah. Matikan tombol lekat dan tombol pelan setelah beberapa saat tidak aktif. Matikan tombol lekat ketika dua tombol ditekan secara simultan Gunakan b&el yang disesuaikan Gunakan tombol l&ekat Gunakan bel &sistem Gunaka&n tombol pantul Gunakan gerak untuk mengaktifkan tombol lekat dan tombol pelan Gunakan bel sistem kapanpun tombol pengunci diaktifkan atau dinonaktifkan Gunakan bel sistem kapanpun sebuah pengubah digerendel, digembok atau tak digembok Gunakan bel sistem kapanpun sebuah tombol ditolak 