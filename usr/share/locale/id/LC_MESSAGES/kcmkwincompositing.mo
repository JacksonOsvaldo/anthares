��    /      �  C           6     M   P     �     �     �     �  	   �  A   �  >   "  9   a  9   �  9   �  W     E   g     �     �      �     �  7         :     [  X   p     �     �     �  �   �     �     �     �     �  
   �  
   �     �     	  E  6	     |
     �
     �
  w   �
     8     F     M     T     d  	   �     �  �  �  >   ;  V   z     �     �     �     �     
          !     *     1     7     <     Z     n     �     �  #   �  ;   �           #  o   <     �     �     �  �   �     �  
   �  $   �     �  
   �  
   �     �     �  R  �     L     j       x   �       	        '     -     A     ]     j                           %                     )              -       *       '                       $                +   !   /                                        
         #       "           .   (   ,   	                &            "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. Accurate Always Animation speed: Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-02-01 20:54+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 "Gambar ulang layar penuh" dapat menyebabkan masalah performa. "Hanya jika murah" hanya mencegah perobekan untuk perubahan layar penuh seperti video. Akurat Selalu Kecepatan animasi: Penulis: %1
Lisensi: %2 Otomatis Aksesibilitas Tampilan Pernik Fokus Alat Animasi Ganti Desktop Virtual Pengelolaan Jendela Konfigurasi filter Tajam andhika.padmawan@gmail.com Aktifkan kompositor saat permulaian Kecualikan Efek Desktop yang tidak didukung oleh Kompositor Kecualikan Efek Desktop internal Gambar ulang layar penuh Petunjuk: Untuk menemukan atau mengkonfigurasi bagaimana cara mengaktifkan efek, silakan lihat di setelan efek. Instan Tim pengembang KWin Jaga miniatur jendela: Menjaga miniatur jendela selalu mengganggu dengan ukuran jendela yang diminimalkan. Ini dapat berakibat di jendela tidak mensuspensi pekerjaan mereka ketika diminimalkan. Andhika Padmawan Tak Pernah Hanya untuk Jendela yang Ditampilkan Hanya jika murah OpenGL 2.0 OpenGL 3.1 EGL GLX Komposit OpenGL (baku) telah menyebabkan KWin mogok di masa lalu.
Kemungkinan ini disebabkan oleh bug penggerak.
Jika anda pikir ada telah memutakhirkan ke penggerak yang stabil,
anda dapat mengatur ulang perlindungan ini tapi ingat bahwa ini dapat menyebabkan kemogokan!
Sebagai alternatif, anda dapat menggunakan ujung belakang XRender. Aktifkan ulang deteksi OpenGL Daur ulang isi layar Ujung belakang render: Metode skala "Akurat" tidak didukung oleh semua peranti keras dan dapat menyebabkan regresi performa dan artifak render. Metode skala: Pencarian Halus Halus (lebih pelan) Pencegahan Sobek ("vsync"): Sangat pelan XRender 