��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     �     �     �     �     �     �     �          	          %     5     E     K     X     e     t  	   �     �     �     �     �     �     �  	   �     �     �       	              1     9  	   I     S     Z     `     m     y     �     �     �     �     �     �     �                    :     S     n     �     �     �     �  -   �                     (     9     J     a     v     �     �     �     �     �     �     �               )     G     ]     v     �     �     �     �     �      �  ;   �     *     =     L     l     y     �     �     �  U   �     .  �   F  !   �  %   �  '     %   ;     a     {     �  &   �     �  4   �  8     	   F  	   P  )   Z  "   �  (   �     �     �     �     �  
   �     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2018-01-31 08:14+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Buku Petunjuk %1 &Tentang %1 &Ukuran Sebenarnya Tambah &Markah &Mundur &Tutup &Konfigurasi %1... &Salin &Hapus &Donasi &Sunting Markah... &Cari... Halaman Pertama &Pas ke Halaman M&aju Per&gi Ke... K&e Baris... &Ke Halaman... H&alaman Terakhir &Surat... &Pindah ke Tempat Sampah &Baru Ha&laman Berikutnya &Buka &Tempel Hala&man Sebelumnya &Cetak... &Keluar Tayangkan &Ulang &Ubah nama... &Ganti... &Laporkan Bug... &Simpan &Simpan Setelan &Ejaan... &Bolak &Atas &Perbesar... &Berikutnya &Sebelumnya &Tampilkan tip saat permulaian Apakah Anda tau...?
 Konfigurasi Tip Hari Ini Tentang &KDE Ha&pus Pemeriksa ejaan di dokumen Hapus Daftar Tutup dokumen Konfigurasi &Pemberitahuan... Konfigurasi &Pintasan... Konfigurasi &Bilah Alat... Salin pilihan ke papan klip Ciptakan dokumen baru Po&tong Potong pilihan ke papan klip Tidak P&ilih andhika.padmawan@gmail.com,wantoyek@gmail.com Deteksi Otomatis Baku Mode Layar Pen&uh Cari Beri&kutnya Cari Se&belumnya Pas ke &Tinggi Halaman Pas ke Lebar Halaman Kembali di Dokumen Maju di dokumen Ke halaman pertama Ke Halaman terakhir Ke halaman berikutnya Ke halaman sebelumnya Ke atas Andhika Padmawan,Wantoyo Tak Ada Lema Buka &Terikini Buka dokumen yang baru dibuka Buka dokumen yang ada Tempel konten papan klip Prat&ayang Cetak Cetak dokumen Keluar aplikasi &Balik Ke&mbali Tayangkan ulang dokumen Balikkan tindakan bolak terakhir Kembalikan perubahan tidak tersimpan yang dibuat ke dokumen Simpan Se&bagai... Simpan dokumen Simpan dokumen dengan nama baru Pilih &Semua Pilih level perbesaran Kirim dokumen melalui surat Tampilkan &Bilah-menu Tampilkan &Bilah Alat Tampilkan Bilah-menu<p>Tampilkan bilah menu lagi setelah bilah menu disembunyikan</p> Tampilkan B&ilah Status Tampilkan Bilah Status<p>Tampilkan bilah status, yaitu bilah yang berada di bawah jendela yang digunakan untuk informasi status.</p> Tampilkan pratayang cetak dokumen Tampilkan atau sembunyikan bilah menu Tampilkan atau sembunyikan bilah status Tampilkan atau sembunyikan bilah alat Ganti &Bahasa Aplikasi... Tip Hari &Ini Bolakkan tindakan terakhir Tampilkan dokumen di ukuran sebenarnya &Apakah Ini? Anda tidak diperbolehkan untuk menyimpan konfigurasi Anda akan diminta untuk mengotentikasi sebelum menyimpan Perbesar& Perkecil& Perbesar ke tinggi pas halaman di jendela Perbesar ke pas halaman di jendela Perbesar ke lebar pas halaman di jendela &Mundur M&aju &Beranda Ba&ntuan tanpa nama 