��          �      |      �     �                (     I     ]     u     �     �     �     �     �     �     �  �   �     l  #   �  @   �  ?   �     )     -  �  5     �       
   "     -     H     \     {     �     �     �     �     �     �     �  �   �     �  )   �  F   �  G        U     ]     
                                         	                                                   (c) 2002 Daniel Molkentin Daniel Molkentin Description EMAIL OF TRANSLATORSYour emails KDE Service Manager Load-on-Demand Services NAME OF TRANSLATORSYour names Not running Running Service Start Startup Services Status Stop This is a list of available KDE services which will be started on demand. They are only listed for convenience, as you cannot manipulate these services. Unable to contact KDED. Unable to start server <em>%1</em>. Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i> Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i> Use kcmkded Project-Id-Version: kcmkded
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-16 03:36+0100
PO-Revision-Date: 2010-05-28 02:09-0400
Last-Translator: Andhika Padmawan <andhika.padmawan@gmail.com>
Language-Team: Indonesian <translation-team-id@lists.sourceforge.net>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 (c) 2002 Daniel Molkentin Daniel Molkentin Keterangan andhika.padmawan@gmail.com Manajer Layanan KDE Layanan Muat Sesuai Permintaan Andhika Padmawan Tidak berjalan Berjalan Layanan Jalankan Layanan Hidupkan Status Hentikan Ini adalah senarai layanan KDE yang akan dijalankan sesuai permintaan. Layanan ini hanya didaftarkan untuk kemudahan, sehingga anda tidak dapat memanipulasi layanan ini. Tak dapat menghubungi KDED. Tak dapat menjalankan server <em>%1</em>. Tak dapat menjalankan layanan <em>%1</em>.<br /><br /><i>Galat: %2</i> Tak dapat menghentikan layanan <em>%1</em>.<br /><br /><i>Galat: %2</i> Gunakan kcmkded 