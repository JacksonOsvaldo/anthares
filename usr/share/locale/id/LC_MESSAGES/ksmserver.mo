��          �   %   �      @     A  $   \  *   �     �     �     �     �     �       '   0     X  (   p     �  &   �     �  ,   �  _   "  !   �  )   �  J   �  c     >   }  A   �     �  �       �     �  ,   �     	  #        A     Z     a     y  +   �     �  ,   �     
	  &   '	     N	  3   n	  j   �	  "   
  /   0
  B   `
  c   �
  8     >   @                                                                     	                                                          
    

KDE is unable to start.
 $HOME directory (%1) does not exist. $HOME directory (%1) is out of disk space. $HOME not set! Also allow remote connections Halt Without Confirmation Log Out Log Out Without Confirmation Logout canceled by '%1' No read access to $HOME directory (%1). No read access to '%1'. No write access to $HOME directory (%1). No write access to '%1'. Plasma Workspace installation problem! Reboot Without Confirmation Restores the saved user session if available Starts <wm> in case no other window manager is 
participating in the session. Default is 'kwin' Starts the session in locked mode Temp directory (%1) is out of disk space. The following installation problem was detected
while trying to start KDE: The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Writing to the $HOME directory (%2) failed with the error '%1' Writing to the temp directory (%2) failed with
    the error '%1' wm Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2018-02-03 06:07+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 

KDE tak dapat dijalankan.
 Direktori $HOME (%1) tidak ada. Direktori $HOME (%1) kehabisan ruang cakram. $HOME belum diatur! Juga mengizinkan koneksi jarak jauh Matikan Tanpa Konfirmasi Keluar Keluar Tanpa Konfirmasi Keluar dibatalkan oleh '%1' Tak ada akses baca ke direktori $HOME (%1). Tak ada akses baca ke '%1'. Tak ada akses tulis ke direktori $HOME (%1). Tak ada akses tulis ke '%1'. Masalah pemasangan Ruang Kerja Plasma! Jalankan Ulang Tanpa Konfirmasi Mengembalikan sesi pengguna tersimpan jika tersedia Menjalankan <wm> jika tidak ada pengelola jendela 
lain yang berpartisipasi di sesi. Bakunya adalah 'kwin' Jalankan sesi dalam mode tergembok Direktori temporer (%1) kehabisan ruang cakram. Masalah pemasangan berikut terdeteksi
ketika coba menjalankan KDE: Pengelola sesi KDE dapat diandalkan yang berbicara protokol pengelolaan 
sesi X11R6 (XSMP) standar. Menulis ke direktrori $HOME (%2) gagal dengan galat '%1' Menulis ke direktori temporer (%2) gagal dengan
    galat '%1' wm 