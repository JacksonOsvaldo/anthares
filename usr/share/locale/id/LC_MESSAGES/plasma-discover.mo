��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �  	   �     �  A   �  *     K   D  K   �  D   �     !     3  :   @     {     �     �  $   �     �  	   �                    +     1     D     _     r     �     �     �  	   �     �     �     �  "   
  D   -  "   r     �  
   �  D   �     �        ?   	     I     V     q  
   z     �     �     �  	   �     �  	   �     �  #   �  %   �  	   $     .  	   N     X     p     ~  M   �  I   �     &  
   /     :     @     V     ]  <   k     �     �     �  
   �     �     �     �     �                @     H     g  
   x     �     �     �     �  "   �     �     �     �  	                  4     Q  "   h         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: KDE Frameworks 5 Applications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-24 21:24+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 
Juga tersedia dalam %1 %1 %1 (%2) %1 (Baku) <b>%1</b> oleh %2 <em>%1 dari %2 orang berpendapat bahwa ulasan ini bermanfaat</em> <em>Beritahu kami tentang ulasan ini!</em> <em>Bermanfaat? <a href='true'><b>Ya</b></a>/<a href='false'>Tidak</a></em> <em>Bermanfaat? <a href='true'>Ya</a>/<a href='false'><b>Tidak</b></a></em> <em>Bermanfaat? <a href='true'>Ya</a>/<a href='false'>Tidak</a></em> Menarik pembaruan Penarikan... Tidak diketahui kapan pemeriksaan terakhir untuk pembaruan Mencari pembaruan Tiada pembaruan Tiada pembaruan yang tersedia Seharusnya memeriksa untuk pembaruan Sistem telah mutakhir. Pembaruan Memperbarui... Terima Tambahkan Sumber... Addon Aleix Pol Gonzalez Sebuah penjelajah aplikasi Terapkan Perubahan Backend yang tersedia:
 Mode-mode yang tersedia:
 Kembali Batal Kategori: Memeriksa untuk pembaruan... Komentar terlalu panjang Komentar terlalu pendek Mode Ringkas (auto/ringkas/penuh). Tidak dapat menutup aplikasi, ada tugas-tugas yang perlu dikelarkan. Tidak bisa menemukan kategori '%1' Tak mampu membuka %1 Hapus asal Secara langsung buka aplikasi yang ditentukan menurut nama paketnya. Buang Discover Tampilan sebuah daftar dari entri-entri dengan sebuah kategori. Ekstensi2... Gagal mencopot sumber '%1' Terfitur Bantuan... Laman: Ringkasan penambahan-baik Pasang Terpasang Jonathan Thomas Luncurkan Lisensi: Daftar semua backend yang tersedia. Daftar semua mode-mode yang tersedia. Memuat... File paket lokal untuk dipasang Buat baku Informasi Selebihnya... Selebihnya... Tiada Pembaruan Buka Discover dalam mode tersebut. Mode-mode sesuai dengan tombol bilah-alat. Buka dengan program yang dapat menghadapi karena mimetype yang diberikan. Teruskan Peringkat: Copot Sumberdaya untuk '%1' Ulasan Mengulas '%1' Berjalan sebagai <em>root</em> adalah ngeri dan tidak perlu. Cari... Cari di '%1'... Cari... Cari: '%1' Cari: %1 + %2 Setelan Ringkasan singkat... Tampilkan ulasan (%1)... Ukuran: Maaf, tidak menemukan apa pun... Sumber: Ketentuan sumber baru untuk %1 Masih mencari... Ringkasan: Dukungan appstream: skema url Tugas Tugas (%1%) %2 %1 Tak dapat menemukan sumberdaya: %1 Perbarui Semua Perbarui Terpilih (%1) pembaruan Pembaruan Versi: pengulas tak diketahui pembaruan yang tidak dipilih pembaruan yang dipilih © 2010-2016 Tim Pengembang Plasma 