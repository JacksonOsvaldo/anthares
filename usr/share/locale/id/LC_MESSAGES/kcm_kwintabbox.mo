��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �  	   �     �     �     �     �  
   �  	                  $     6     G     V  K   j     �  *   �     �  !   �     	     "	     /	     O	     ^	     e	     k	     t	     �	     �	  �   �	  H   ;
     �
     �
     �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-12-11 22:01+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 Aktivitas Semua aktivitas lainnya Semua desktop lainnya Semua layar lainnya Semua jendela Alternatif Desktop 1 Isi Aktivitas saat ini Aplikasi saat ini Desktop saat ini Layar saat ini Filter jendela oleh Setelan kebijakan fokus membatasi fungsionalitas bernavigasi antar jendela. Maju Dapatkan Tata Letak Pengganti Jendela Baru Jendela tersembunyi Termasuk ikon "Tampilkan Desktop" Utama Minimalisasi Hanya satu jendela per aplikasi Baru Digunakan Mundur Layar Pintasan Tampilan jendela terpilih Tata pengurutan: Urutan penumpukan Jendela terpilih saat ini akan disorot dengan cara memunculkan semua jendela lainnya. Opsi ini memerlukan efek desktop untuk dapat aktif. Efek untuk menggantikan senarai jendela ketika efek desktop tidak aktif. Desktop virtual Jendela tampak Visualisasi 