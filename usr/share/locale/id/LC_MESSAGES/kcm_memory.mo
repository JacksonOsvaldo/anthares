��    $      <  5   \      0     1     9     V  �   j          -     >     E  
   R     ]     k      w     �  	   �     �     �     �     �  +   �     )     0     O     ^     n  
   }  �   �  g   :  �  �     :     L     Y     p     �  	   �  
   �  �  �     L
     U
  	   r
  �   |
     <     J     X     `     q     �     �     �     �  
   �     �     �            (   '     P     W     h     u     �  
   �  �   �  m   i  �  �     �     �     �     �     �     �  
   �                                 #                                                                    !                          $      "   	                                  
          %1 free (c) 1998 - 2002 Helge Deller 1 byte = %1 bytes = <p>The swap space is the <b>virtual memory</b> available to the system.</p> <p>It will be used on demand and is provided through one or more swap partitions and/or swap files.</p> Active memory: Application Data Charts Disk Buffers Disk Cache Disk buffers: Disk cache: EMAIL OF TRANSLATORSYour emails Free Physical Memory Free Swap Free physical memory: Free swap memory: Helge Deller Inactive memory: KDE Panel Memory Information Control Module Memory NAME OF TRANSLATORSYour names Not available. Physical Memory Shared memory: Swap Space This display shows you the current memory usage of your system. The values are updated on a regular basis and give you an overview of the physical and virtual memory being used. This graph gives you an overview of the <b>total sum of physical and virtual memory</b> in your system. This graph gives you an overview of the <b>usage of physical memory</b> in your system.<p>Most operating systems (including Linux) will use as much of the available physical memory as possible as disk cache, to speed up the system performance.</p><p>This means that if you have a small amount of <b>Free Physical Memory</b> and a large amount of <b>Disk Cache Memory</b>, your system is well configured.</p> Total Free Memory Total Memory Total physical memory: Total swap memory: Used Physical Memory Used Swap kcm_memory Project-Id-Version: kcm_memory
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:33+0200
PO-Revision-Date: 2018-02-01 21:16+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %1 bebas (c) 1998 - 2002 Helge Deller %1 bita = <p>Ruang swap adalah <b>memori virtual</b> yang tersedia untuk sistem.</p> <p>Ini akan digunakan sesuai kebutuhan dan disediakan melalui satu atau lebih partisi swap dan/atau berkas swap.</p> Memori aktif: Data Aplikasi Diagram Penyangga Cakram Singgahan Cakram Penyangga cakram: Singgahan cakram: andhika.padmawan@gmail.com Memori Fisik Bebas Swap Bebas Memori fisik bebas: Memori swap bebas: Helge Deller Memori tidak aktif: Modul Kendali Informasi Memori Panel KDE Memori Andhika Padmawan Tak tersedia Memori Fisik Memori bersama: Ruang Swap Tampilan ini menampilkan kepada anda penggunaan memori saat ini dari sistem anda. Nilai diperbarui secara berkala dan memberikan anda selayang pandang dari memori fisik dan virtual yang sedang digunakan. Grafik ini memberikan anda selayang pandang dari <b>jumlah total memori fisik dan virtual</b> di sistem anda. Grafik ini memberikan anda selayang pandang dari <b>penggunaan memori fisik</b> di sistem anda.<p>Mayoritas sistem operasi (termasuk Linux) akan menggunakan sebanyak mungkin memori fisik yang tersedia sebagai singgahan cakram, untuk meningkatkan performa sitem.</p><p>Ini berarti jika anda mempunyai sedikit <b>Memori Fisik Bebas</b> dan banyak <b>Memori Singgahan Cakram</b>, sistem anda telah dikonfigurasi dengan baik.</p> Memori Bebas Total Memori Total Memori fisik total: Memori swap total: Memori Fisik Digunakan Swap Digunakan kcm_memory 