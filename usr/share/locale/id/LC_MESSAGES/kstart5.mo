��            )   �      �  0   �  .   �  s  �  X   e     �     �  *   �           )  H   <     �  &   �  S   �                ,     M     l     �  8   �     �     �  /     -   1  S   _  *   �  *   �  �   	  �  �  0   <
  0   m
  �  �
  b   )     �     �  +   �     �     �  I   	     S  $   Z  E        �     �  #   �           /      @  K   a     �     �  ,   �  (   	  S   2  2   �  3   �  �   �              
                                        	                                                                                    (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) A regular expression matching the window title A string matching the window class (WM_CLASS property)
The window class can be found out by running
'xprop | grep WM_CLASS' and clicking on a window
(use either both parts separated by a space or only the right part).
NOTE: If you specify neither window title nor window class,
then the very first window to appear will be taken;
omitting both options is NOT recommended. Alternative to <command>: desktop file to start. D-Bus service will be printed to stdout Command to execute David Faure Desktop on which to make the window appear EMAIL OF TRANSLATORSYour emails Iconify the window Jump to the window even if it is started on a 
different virtual desktop KStart Make the window appear on all desktops Make the window appear on the desktop that was active
when starting the application Matthias Ettrich Maximize the window Maximize the window horizontally Maximize the window vertically NAME OF TRANSLATORSYour names No command specified Optional URL to pass <desktopfile>, when using --service Richard J. Moore Show window fullscreen The window does not get an entry in the taskbar The window does not get an entry on the pager The window type: Normal, Desktop, Dock, Toolbar, 
Menu, Dialog, TopMenu or Override Try to keep the window above other windows Try to keep the window below other windows Utility to launch applications with special window properties 
such as iconified, maximized, a certain virtual desktop, a special decoration
and so on. Project-Id-Version: kstart
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-07 03:59+0100
PO-Revision-Date: 2017-12-24 09:27+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) Ekspresi reguler yang cocok dengan judul jendela Benang yang cocok dengan kelas jendela (properti WM_CLASS)
Kelas jendela dapat diketahui dengan menjalankan
'xprop | grep WM_CLASS' dan klik pada jendela
(gunakan baik kedua bagian yang dipisahkan oleh spasi atau hanya bagian kanan.
CATATAN: Jika anda tidak menentukan judul jendela atau kelas jendela,
maka jendela pertama yang muncul akan diambil;
menghapus kedua opsi TIDAK direkomendasikan. Alternatif untuk <perintah>: berkas desktop untuk dijalankan. Layanan D-Bus akan dicetak ke stdout Perintah untuk dieksekusi David Faure Desktop tempat untuk membuat jendela muncul andhika.padmawan@gmail.com Ikonkan jendela Lompat ke jendela bahkan jika dijalankan di 
desktop virtual yang berbeda KStart Buat jendela muncul di semua desktop Buat jendela muncul di desktop yang aktif
ketika menjalankan aplikasi Matthias Ettrich Maksimalkan jendela Maksimalkan jendela secara mendatar Maksimalkan jendela secara tegak Andhika Padmawan Tak ada perintah yang ditentukan URL opsional untuk meneruskan <berkasdesktop>, ketika menggunakan --service Richard J. Moore Tampilkan jendela layar penuh Jendela tidak mendapat tempat di bilah tugas Jendela tidak mendapat tempat di pemisah Tipe jendela: Normal, Desktop, Dok, Bilah Alat, 
Menu, Dialog, Menu Atas atau Timpa Coba untuk menjaga jendela di atas jendela lainnya Coba untuk menjaga jendela di bawah jendela lainnya Utilitas untuk menjalankan aplikasi dengan properti jendela khusus 
seperti diikonkan, dimaksimalkan, desktop virtual tertentu, dekorasi khusus
dan seterusnya. 