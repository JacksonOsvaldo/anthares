��          �   %   �      p     q     y     �     �     �     �     �     �     �     �     �     �     �     	          )     /     5     M  
   R     ]     l     �     �     �     �     �  �  �     �  )   �  )   �     �            6   0  +   g     �     �     �  *   �          0  .   M  
   |     �  2   �     �  $   �  3   �  0   '     X  J   w  
   �     �  4   �                                                 
                                                                	          &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Default Deselect All File name pattern: File types: Hide Files Matching Icons Large More Preview Options... None Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2011-07-09 12:39+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 &Изтриване Из&чистване на кошчето Прем&естване в кошчето &Отваряне &Поставяне Информа&ция Опресн&яване на работния плот Опресн&яване на изгледа &Презареждане &Преименуване По подразбиране Размаркиране на всичко Шаблони за имена: Видове файлове: Скриване на съвпаденията Икони Големи Още настройки за прегледа... Без Маркиране на всичко Показване на всички файлове Показване на съвпаденията Показвано място: Показване директорията на работния плот Малки Директория: Напишете пътя или адреса тук 