��            )   �      �  &   �     �     �     �     �      �                    &  ,   C  3   p     �     �     �     �     �     �          !     7     P     W     o     s     �  &   �     �  �  �     i  *   r     �     �     �  9   �       :   (     c  6   }  P   �  k     i   q  <   �  	   	     "	     .	  S   J	     �	     �	  (   �	     �	  8   �	     8
     M
     i
  I   r
     �
                                       
                      	                                                                             @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-07-23 01:52+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@ludost.net>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Общи Добавяне на нов скрипт. Добавяне... Отказване? Коментар: yasen@lindeas.com,radnev@yahoo.com,zlatkopopov@fsa-bg.org Редактиране Редактиране на избрания скрипт. Редактиране... Изпълнение на избрания шрифт. Не беше създаден скрипт за интерпретатор "%1" Определянето на интерпретатор за скрипт "%1" беше неуспешно Зареждането на интерпретатор за скрипт "%1" беше неуспешно Файлът "%1" не може да бъде отворен Файл: Икона: Интерпретатор: Ясен Праматаров,Радостин Раднев,Златко Попов Име: Няма функция "%1" Няма интерпретатор "%1" Изтриване Премахване на избрания скрипт. Изпълнение Няма скрипт "%1". Стоп Спиране изпълнението на избрания шрифт. Текст: 