��    %      D  5   l      @  &   A  �   h     �                '     0     9     G     T     e      u  	   �  �   �  \   i  c   �     *     ;     G     f     n     �     �     �     �     �     �     �     �  	   �  C   �  j   ;  E   �     �     �       �    &   �	  �   �	     �
     �
  
   �
     �
          3     N     i     z     �     �  O   �  �     v   �  2         3     ?     ]  )   o  )   �     �     �  2   �          0      C     d     �  {   �  I     �   ]  $   �  &     4   8                                 
                  $                    !      #            	                            "                                      %                        (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside most menu items. If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Preview Radio button Ralf Nolden Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Toolbars Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2011-07-23 14:20+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Настройки на стил</h1> От тук може да настроите визуалния изглед на графичните елементи и ефектите при дадена операция. &Приложения &Фина настройка Бутон Поле за отметка Падащ списък &Настройване... Настройки на %1 Daniel Molkentin Описание: %1 radnev@yahoo.com Групиране Списък с предварително дефинирани стилове. Ако включите това, KDE ще показва малки икони в повечето записи в менютата. Ако включите това, KDE ще показва малки икони в някои важни бутони. Модул за настройки на стила Karol Szwed Радостин Раднев Без текст Няма налично описание. Предварителен преглед Превключвател Ralf Nolden Показване икони в менютата: Страница 1 Страница 2 Текст под иконите Текст до иконите Само текст Грешка по време на зареждане диалога за настройка на избрания стил. Предварителен преглед на избрания стил. На тази страница можете да настроите подробно изгледа на графичните елементи Ленти с инструменти Грешка при зареждане Стил на графичните елементи: 