��          �      �       0  $   1  �   V     �  4   �       #   5  +   Y     �     �     �  �   �  $   Q  �  v     .     B  !   Z  p   |  B   �  ^   0  W   �  ,   �  <     8   Q     �  ?   �                                	      
                    @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2011-07-09 12:44+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.2
 %1 свободни Достъпване... Всички устройства Натиснете за достъпване на устройството от други приложения. Натиснете за изваждане на този диск. Натиснете за безопасно изключване на устройството. Вече е безопасно да премахнете това устройство. Няма налични устройства Само непремахваемите устройства Само премахваемите устройства Премахване... Устройството в момента е достъпно. 