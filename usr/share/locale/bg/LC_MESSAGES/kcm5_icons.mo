��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     -     D     S     i  	   �     �    �  B   �               1  �   O     �          $  
   <     G     \     u     �     �  #   �  *   �               ,     @  ?   M  
   �  1   �  �   �  C   �  1   �     �          #     5     Q  "   `  I   �     �  /   �  5     3   C     w     �  K   �  K   �     3     E  $   e     �  �   �  M   1  
     I  �               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-08-22 22:33+0300
Last-Translator: Yasen Pramatarov <yasen@lindeas.com>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Ко&личество: &Ефекти: В&тори цвят: &Полупрозрачност &Теми (c) 2000-2003 Geert Jansen <qt>Сигурни ли сте, че искате темата с икони <strong>%1</strong> да бъде изтрита?<br /><br />При тази операция ще бъдат изтрити всичките файлове инсталирани от тази тема.</qt> <qt>Инсталиране на тема <strong>%1</strong>.</qt> Активни Неактивни По подразбиране Появи се проблем по време на инсталацията, но повечето от темите в архива са инсталирани. &Допълнителни Всички икони Antonio Larrosa Jimenez &Цвят: Оцветяване Потвърждение Обезцветяване Описание Работен плот Диалогови прозорци Местоположение на тема radnev@yahoo.com Параметри Гама ефект Geert Jansen Изтегляне на нови теми от Интернет Икони Контролен модул за иконите Ако вече имате локален архив с теми, това ще я разархивира и направи годна за употреба от програмите в KDE Инсталиране на тема от наличен архив Главна лента с инструменти Радостин Раднев Име Без ефект Системен панел Преглед Премахване на тема Премахване на маркираните теми от диска Ефекти... Ефекти на активните икони Ефекти на стандартните икони Ефекти на неактивните икони Размер: Малки икони Файлът не е валиден архив на тема с икони. Това ще изтрие маркираните теми от диска. Сива гама Монохромна скала Лента с инструменти Torsten Rahn Не може да бъде изтеглена темата с икони.
Моля, проверете дали адресът "%1" е правилен. Не може да бъде намерена темата с икони "%1". Икони За да извършите това действие трябва да сте свързани с Интернет. Един прозорец ще ви покаже наличните теми от сайта http://www.kde.org. Натиснете съответния бутон, за да инсталирате темата. 