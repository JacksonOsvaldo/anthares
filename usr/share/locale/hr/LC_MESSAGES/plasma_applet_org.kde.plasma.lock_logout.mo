��          �            h  &   i  +   �     �  	   �     �     �     �     �     �  (        7  ,   N     {     �  T  �  -   �  2     	   E     O  $   [     �     �  
   �     �  -   �     �  -   �     -     6                                                                  	   
           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2011-02-15 16:39+0100
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Da li želite zaustavljanje u RAM (spavanje)? Da li želite zaustavljanje na disk (hibernacija)? Općenito Hibernacija Hibernacija (zaustavljanje na disk)  Napusti Napusti … Zaključaj Zaključaj zaslon Odjavi se, ugasi ili ponovo pokreni računalo Spavanje (zaustavljanje u RAM) Pokreni paralelnu sjednicu kao drugi korisnik Zaustavi Zamijeni korisnika 