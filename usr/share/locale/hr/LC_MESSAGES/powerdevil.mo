��          �   %   �      @     A     F  ,   L  N   y  
   �      �  	   �  �   �     �     �  e   �          )     ;  
   Q     \     c  &   t  %   �  f   �     (     8     S     j  I  �     �     �     �  \   �     =  @   M  
   �  �   �     D	  3   V	  �   �	     
     /
  $   H
     m
     }
     �
     �
     �
  _   �
     4     E  (   a  0   �                                   	                                                          
                                  min After Brightness level, label for the sliderLevel Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2011-03-05 15:58+0100
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.2
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  min Nakon Razina Ne mogu se povezati na sučelje baterije.
Molim vas provjerite vašu sistemsku konfiguraciju Ne čini ništa zarko.pintar@gmail.com, adundovi@gmail.com, marko@dimjasevic.net Hiberniraj KDE-ov sustav upravljanja potrošnjom energije nije moguće inicijalizirati. Pozadinski servis prijavio je sljedeću pogrešku: %1
Provjerite konfiguraciju Vašeg sustava Zaključaj zaslon Žarko Pintar, Andrej Dundović, Marko Dimjašević Nije pronađen ni jedan valjani priključak pozadinskog servisa za upravljanje energijom. Nova instalacija možda će riješiti ovaj problem. kad se aktivira profil kad se deaktivira profil Prikaži dijaloški prozor za odjavu Pokreni skriptu Skripta Isključi nakon Strujni adapter je priključen. Strujni adapter je isključen. Odabran je "%1" profil, ali on ne postoji.
Molim vas provjerite vašu PowerDevil konfiguraciju. Isključi zaslon Nepodržana metoda obustave Kada je prijenosno računalo zaklopljeno Kada je pritisnut gumb za uključiti/isključiti 