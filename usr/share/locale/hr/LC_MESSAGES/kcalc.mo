��    @        Y         �     �     �     �  
   �     �     �     �  	   �     �  
   �     �     �     �          #     &     )     ,     /     2     5     A  	   R     \  	   c     m     t     �      �     �     �  	   �     �     �     �     �               $     3     I     U     Z     a     f     m     |     �     �  	   �     �     �  	   �  
   �     �     �                         /     7     F  D  e  
   �
     �
     �
  
   �
     �
     �
  
   �
  	   �
                          4  
   K     V     Y     \     _     b     e     h     w     �     �  	   �     �     �  
   �  ,   �     �  	          	        #     2     @     Q     k     q     �     �     �     �     �     �  	   �     �     �     �               ,     5     >     J     S     _     r     x  
   �     �     �  #   �     "   #               4                  '       3       :      
   (   =   ,       9   1                      .       $   )          	      +   &   <         5           *       %   -   0                              @   6   ?                       >                7   2                8   /       !   ;                      &Background: &Beep on error &Bin &Constants &Dec &Foreground: &Memory: &Numbers: &Oct Arc cosine Arc sine Atomic && Nuclear Button & Display Colors Button Colors C1 C2 C3 C4 C5 C6 Change sign Choose From List Clear all Colors Constants Cosine Display Colors Division EMAIL OF TRANSLATORSYour emails Electromagnetism Exponent Factorial General General Settings Gravitation Hyperbolic sine Inverse hyperbolic sine KCalc KDE Calculator Last stat item erased Mathematics Mean Median Misc Modulo Multiplication NAME OF TRANSLATORSYour names Natural log New Name for Constant New name: O&perations: Percent Precision Reciprocal Result Set Name Show B&it Edit Sine Square Standard deviation Tangent Thermodynamics Write display data into memory Project-Id-Version: kcalc 0
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:17+0100
PO-Revision-Date: 2009-06-21 10:11+0200
Last-Translator: Andrej Dundović <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 0.3
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Pozadina: Pipni kod greške &Bin &Konstante &Pro &Prednji plan: &Memorija: &Brojevi: &Lis Arkus kosinus Arkus sinus Atomske && Nuklearne Gumbi & Prikaži pismo Boje gumba C1 C2 C3 C4 C5 C6 Promijeni znak Izaberi iz liste Očisti sve Boje Konstante Kosinus Prikaži boje Dijeljenje lokalizacija@linux.hr, lokalizacija@linux.hr Elektromagnetske Potencija Faktorijelno Općenito Opće postavke Gravitacijske Sinus hiperbolni Inverzni sinus hiperbolni KCalc KDE kalkulator Zadnji stat unos je izbrisan Matematičke Sredina Min Razno Modulo Množenje Igor Jagec, Nikola Planinac Prirodni logaritam Novo ime za konstantu Novo ime &Gumbi operacija Postotak Točnost Recipročno Rezultat Postavi ime Prikaži B&it Edit Sinus Kvadrat Standardno Tangenta Termodinamičke Upiši prikazane podatke u memoriju 