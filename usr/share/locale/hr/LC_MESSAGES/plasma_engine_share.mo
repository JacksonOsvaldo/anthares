��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     ?  <  !   |  +   �  9   �  ,     *   1  )   \     �     �  !   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-03-09 14:53+0100
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Ne mogu odrediti mimetip datoteke Nije moguće pronaći sve tražene funkcije Nije moguće pronaći pružatelja s navedenim odredištem Pogreška pri pokušaju izvršavanja skripte Nevaljana putanja za traženog pružatelja Nije moguće pročitati odabranu datoteku Servis nije bio dostupan Nepoznata greška Morate navesti URL za ovaj servis 