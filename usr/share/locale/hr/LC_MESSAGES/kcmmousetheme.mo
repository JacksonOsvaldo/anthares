��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  }  �     v  �   �  m   /  Z   �     �  #    	     $	     5	  &   :	  7   a	     �	  #   �	     �	     �	     �	     
  ;   
  S   K
  1   �
                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput 0
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-12 16:40+0200
Last-Translator: Marko Dimjašević <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.2
X-Poedit-Bookmarks: 42,-1,-1,-1,-1,-1,-1,-1,-1,-1
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2003–2007 Fredrik Höglund <qt>Jeste li sigurni da želite ukloniti temu pokazivača <i>%1</i>?<br />Na ovaj ćete način izbrisati sve datoteke koje je ova tema instalirala.</qt> <qt>Ne možete izbrisati temu koju trenutno koristite.<br /> Prvo morate promijeniti na neku drugu temu.</qt> Tema naziva %1 već postoji u vašoj mapi tema ikona. Želite li ju zamijeniti ovom temom? Potvrda Postavke pokazivača su izmijenjene Tema pokazivača Opis Prevucite ili upišite URL adresu teme renato@translator-shop.org, DoDoEntertainment@gmail.com Fredrik Höglund Dohvati nove sheme boja s Interneta Renato Pavičić, Nenad Mikša Naziv Prepisati temu? Ukloni temu Datoteka %1 ne izgleda kao valjana arhiva teme pokazivača. Nije moguće pronaći arhivu teme pokazivača. Provjerite je li adresa %1 ispravna. Nije moguće pronaći arhivu teme pokazivača %1. 