��    #      4  /   L           	      L     /   l     �     �     �     �     �     �     
           /  I   @     �  *   �     �     �     �  !     "   $     G  6   d  *   �     �     �     �     �     
          +     >     V     f  F  �     �
  +  �
  a   �  /   Y     �     �     �  !   �  	   �     �            !   $  a   F     �  /   �     �     �  
          7      "   X  6   {  A   �     �                    #     ,     2     ?     N     U                                                      	               
                                                                           "      !   #     to  <p>If you have a TFT or LCD screen you can further improve the quality of displayed fonts by selecting this option.<br />Sub-pixel rendering is also known as ClearType(tm).<br /> In order for sub-pixel rendering to work correctly you need to know how the sub-pixels of your display are aligned.</p> <p>On TFT or LCD displays a single pixel is actually composed of three sub-pixels, red, green and blue. Most displays have a linear ordering of RGB sub-pixel, some have BGR.<br /> This feature does not work with CRT monitors.</p> <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Smallest font that is still readable well. Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB font usageFixed width font usageGeneral font usageMenu font usageSmall font usageToolbar font usageWindow title no hintingNone no subpixel renderingNone Project-Id-Version: kcmfonts 0
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2010-01-26 11:57+0100
Last-Translator: Andrej Dundovic <adundovi@gmail.com>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 do <p>Ako imate TFT ili LCD zaslon tada možete dodatno podignuti kvalitetu prikazanih fontona tako da odaberete ovu opciju.<br /> Podpikselno renderiranje je također poznato pod nazivom ClearType(tm).<br /> Da bi podpikselno renderiranje radilo pravilno, morate znati kako su podpikseli Vašeg zaslona poravnati.</p> <p>Na TFT i LCD zaslonima svaki je piksel sastavljen od 3 podpiksela: crvenog, zelenog i plavog (RGB). Većina zaslona ima linearno poredane piksele u smjeru RGB, dok neki imaju BGR.<br /> Ova mogućnost nije dostupna na CRT zaslonima.</p> <p>Neke izmjene, poput postavki DPI-a, imat će učinak samo na novo pokrenutim aplikacijama.</p> Neproprcionalno pismo (npr. s pisaćeg stroja). &Prilagodi sva pisma… BGR Kliknite za izmjenu svih pisama Podešavanje postavki anti-aliasa Podesi… &Izuzmi raspon: Postavke pisma su izmijenjene %1:  Prisilna vrijednost DPI za pisma: Naslućivanje je postupak koji se upotrebljava za poboljšavanje kvalitete pisama male veličine. RGB Najsitnije pismo koje je još uvijek čitljivo. Upotrijebi a&nti-aliasing: Onemogućeno Omogućeno Postavke sustava Upotrebljava se za trake izbornika i skočne izbornike. Upotrebljava se za naslov prozora. Za normalan tekst (npr. natpisi gumba, stavke popisa). Upotrebljava se za prikaz teksta pokraj ikona na alatnim trakama. Uspravni BGR Uspravni RGB Stalne širine Opće Izbornik Sitan Alatna traka Naslov prozora Ništa Ništa 