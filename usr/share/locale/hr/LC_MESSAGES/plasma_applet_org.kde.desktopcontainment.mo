��            )   �      �     �     �     �     �     �     �     �     �          	               &     9  	   E     O     c     i     o     �  
   �     �     �     �     �     �     �     �       Y    	   u          �     �  	   �  	   �     �     �  	   �  
   �                    1     B  $   Q     v     |     �     �     �     �  &   �     �  #   �     #     *     6     K                                                                   
      	                                                                &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Default Deselect All File name pattern: File types: Full path Hide Files Matching Icons Large More Preview Options... None Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Sorting: Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2011-07-12 17:59+0200
Last-Translator: Marko Dimjašević <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.2
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Izbriši Isprazni kantu za sm&eće Pre&mjesti u smeće &Otvori &Zalijepi &Svojstva Osvježi radnu površinu Osvježi pogled &Osvježi &Preimenuj Zadano Odznači sve Izraz imena datoteke: Tipovi datoteke: Cijela putanja Sakrij datoteke koje se podudaraju s Ikone Velika Više opcija pregleda … Ništa Označi sve Prikaži sve datoteke Prikaži datoteke koje se podudaraju s Prikaži mjesto: Prikaži direktorij radne površine Malena Sortiranje: Odredite direktorij: Upišite putanju ili URL ovdje 