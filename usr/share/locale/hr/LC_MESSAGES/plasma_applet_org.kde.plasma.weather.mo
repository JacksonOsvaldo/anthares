��          �      L      �     �  '   �  "         #     =     U  ?   s     �  &   �      �  >        N     m  '   �  !   �     �     �  3     ?  A     �     �     �  
   �  	   �     �     �  
   �     �     �               "     '     6     I     ]     f                                                                         	   
                        Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2010-07-18 14:17+0200
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 ° %1 dan %1 dana %1 dana V: %1 N: %2 Visoko: %1 Nisko: %1 - Molim vas da podesite Bez vjetra Vlažnost: %1%2 Vidljivost: %1 %2 Tendencija tlaka: %1 Tlak: %1 %2 %1%2 Vidljivost: %1 Izdana upozorenja: Izdana promatranja: %1 %2 %3 Nalet vjetra: %1 %2 