��          \      �       �      �   -   �        -   +  #   Y  #   }     �  ?  �     �  0        >  2   Y     �     �     �                                       Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-07-23 11:09+0200
Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>
Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Prikazuje trenutni datum Prikazuje trenutni datum u danoj vremenskoj zoni Prikazuje trenutno vrijeme Prikazuje trenutno vrijeme u danoj vremenskoj zoni datum vrijeme Današnji datum je %1 