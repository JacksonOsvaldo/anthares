��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �               1     E     M     f  0   r  E   �  "   �          (     F     O  o   d        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-28 08:38+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 &Ustawienia drukarek... Tylko wykonywane zadania Wszystkie zadania Tylko ukończone zadania Ustawienia drukarki Ogólne Brak wykonywanych zadań Brak zadań Nie ustawiono, ani nie wykryto żadnych drukarek Jedno wykonywane zadanie %1 wykonywane zadania %1 wykonywanych zadań Jedno zadanie %1 zadania %1 zadań Otwórz kolejkę drukowania Kolejka drukowania jest pusta Drukarki Wyszukaj drukarki... Jest jedno zadanie drukowania w kolejce Są %1 zadania drukowania w kolejce Jest %1 zadań drukowania w kolejce 