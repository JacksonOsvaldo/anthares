��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     =  !   C  �  e  �  	  z   �
     >     S     g  &   z     �     �  !   �     �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-06-05 08:15+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  sek. Najdłuż&szy czas powiadamiania: <h1>Pasek zadań</h1>
Możesz włączyć inną metodę powiadamiania o uruchamianiu programu
- na pasku zadań pojawi się ikona programu z obracającą się klepsydrą, .
symbolizującą uruchamianie programu.
Niektóre programy mogą ignorować tę funkcję, nie zawiadamiając o zakończeniu uruchamiania. Wówczas wskaźnik przestanie migotać po czasie określonym w polu "Najdłuższy czas powiadamiania". <h1>Zmiana wskaźnika</h1>
KDE może zaznaczać uruchamianie programu przez zmianę wyglądu wskaźnika.
Żeby tak było, wybierz jedną z opcji wzrokowego
wrażenia z pola wyboru.
Dodatkowo możesz włączyć migotanie wskaźnika w opcji poniżej.
Niektóre programy mogą ignorować tę funkcję, nie zawiadamiając o
zakończeniu uruchamiania. Wówczas wskaźnik przestanie migotać po czasie
określonym w polu "Najdłuższy czas powiadamiania". <h1>Powiadamianie o uruchamianiu</h1> Moduł umożliwia wybór sposobu powiadamiania o rozpoczęciu uruchamiania programu. Migoczący wskaźnik Skaczący wskaźnik &Zmiana wskaźnika Włącz powiadamienie na &pasku zadań Bez zmiany wskaźnika Pasywna zmiana wskaźnika Najdł&uższy czas powiadamiania: Powiadamia&nie na pasku zadań 