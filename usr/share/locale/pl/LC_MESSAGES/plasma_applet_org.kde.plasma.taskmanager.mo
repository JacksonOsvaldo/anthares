��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �  
          	         *     1     8     R     r  #   �     �  1   �  0   �       
   ,     7     K     ]     u  $   �  
   �     �     �      �     �     �               .     <     @     T     g     �     �     �  /   �     �     �     �  '        .     @     _     y     �     �     �     �     �     �  0   �  !   ,  
   N     Y  	   `     j     {     �     �     �  7   �  -   �  *     )   >  $   h  ,   �     �     �  
   �     �        	   	       	        !     2  &   B        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-28 06:48+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 &Wszystkie pulpity &Zamknij P&ełen ekran Prze&nieś &Nowy pulpit &Przypnij &Zwiń &%1 %2 Dostępne również na %1 Dodaj do bieżącej aktywności Wszystkie aktywności Pozwól na grupowanie tego programu Alfabetycznie Rozmieszczaj zadania w tylu kolumnach ile wierszy Rozmieszczaj zadania w tylu wierszach ile kolumn Rozmieszczenie Zachowanie Według aktywności Według pulpitów Według nazw programów Zamknij okno lub grupę Przełączaj zadania kółkiem myszy Nie grupuj Nie szereguj Filtry Zapomnij o ostatnich dokumentach Ogólne Grupowanie i szeregowanie Grupuj: Podświetlaj okna Rozmiar ikon: — Zawsze n&a wierzchu Zawsze &na spodzie Programy aktywujące osobno Duże Ma&ksymalizuj Ręcznie Oznaczaj aplikacje, które odtwarzają dźwięk Maksymalnie kolumn: Maksymalnie wierszy: Mi&nimalizuj Zminimalizuj/Przywróć okno lub grupę Więcej działań Przenieś na &bieżący pulpit Przenieś do &aktywności Przenieś na &pulpit Wycisz Nowe wystąpienie Na %1 Na wszystkich aktywnościach Na bieżącej aktywności Na środkowy przycisk: Grupuj tylko wtedy, gdy pasek zadań jest pełny Otwieraj grupy w oknach wysuwnych Przywróć 9,999+ Wstrzymaj Następny utwór Poprzedni utwór Zakończ &Zmień rozmiar Odepnij %1 miejsce więcej %1 miejsca więcej %1 miejsc więcej Pokaż zadania tylko z bieżącej aktywności Pokaż zadania tylko z bieżącego pulpitu Pokaż zadania tylko z bieżącego ekranu Pokaż zadania tylko zminimalizowane Pokazuj postęp i stan na przyciskach zadań Pokazuj podglądy okien Małe Uszereguj: Rozpocznij nowe wystąpienie Odtwórz Zatrzymaj Nic &Przypnij Grupuj/Rozgrupuj Dostępne na %1 Dostępne we wszystkich aktywnościach 