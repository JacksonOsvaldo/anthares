��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �       9     #   P  J   t  J   �  C   
     N     f  9   t     �     �     �     �     
           .  
   ?     J     ]     e     x     �     �     �     �     �  
   �     �     �       )   %  ?   O  #   �     �     �  <   �             &   )     P  "   `     �     �     �     �     �     �     �     �  	   �  +   �  )        B     Q     q     �  
   �     �  W   �  =     	   L     V     ]     c     s     y  @   �     �     �  	   �  
   �     �  
   	          -     G     P  
   n     y     �     �  +   �     �     �     �     �          /     E     X     f     n     �     �  *   �         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-28 06:50+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 
Dostępny również w %1 %1 %1 (%2) %1 (Domyślny) <b>%1</b> przez %2 <em>%1 z %2 osób uznało tę opinię za użyteczną</em> <em>Opowiedz nam o tej opinii!</em> <em>Użyteczna? <a href='true'><b>Tak</b></a>/<a href='false'>Nie</a></em> <em>Użyteczna? <a href='true'>Tak</a>/<a href='false'><b>Nie</b></a></em> <em>Użyteczna? <a href='true'>Tak</a>/<a href='false'>Nie</a></em> Pobieranie uaktualnień Pobieranie... Nie wiadomo kiedy po raz ostatni sprawdzano uaktualnienia Sprawdzanie uaktualnień Brak uaktualnień Brak uaktualnień Należy wyszukać uaktualnień System jest aktualny. Uaktualnienia Uaktualnianie... Zatwierdź Dodaj źródło... Dodatki Aleix Pol Gonzalez Przeglądarka programów Zastosuj zmiany Dostępne silniki:
 Dostępne tryby:
 Wstecz Anuluj Kategoria: Sprawdzanie uaktualnień... Komentarz za długi Komentarz za krótki Tryb kompaktowy (auto/kompaktowy/pełny). Nie można zamknąć aplikacji, zostały zadania do wykonania. Nie można znaleźć kategorii '%1' Nie można otworzyć %1 Usuń źródło Otwórz bezpośrednio podany program po nazwie jego pakietu. Porzuć Odkrywca Wyświetl listę wpisów z kategorią. Rozszerzenia... Nie można usunąć źródła '%1' Przedstawiane Pomoc... Strona domowa: Ulepsz podsumowanie Wgraj Wgrane Jonathan Thomas Uruchom Licencja: Wyszczególnij wszystkie dostępne silniki. Wyszczególnij wszystkie dostępne tryby. Wczytywanie... Lokalny plik pakietu do wgrania Uczyń domyślnym Więcej informacji... Więcej... Brak uaktualnień Otwórz Odkrywcę w trybie mówionym. Tryby odpowiadają przyciskom na pasku narzędzi. Otwórz w programie, który radzi sobie z podanym typem mime. Kontynuuj Ocena: Usuń Zasoby dla '%1' Oceń Opiniowanie '%1' Uruchamianie jako <em>root</em> jest niezalecane i niepotrzebne. Szukaj Znajdź w '%1'... Szukaj... Szukaj: %1 Szukaj: %1 + %2 Ustawienia Krótkie podsumowanie... Pokaż komentarze (%1)... Rozmiar: Wybacz, nic nie znaleziono... Źródło: Podaj nowe źródło dla %1 Nadal szukam... Podsumowanie: Obsługuje strumień aplikacji: schemat url Zadania Zadania (%1%) %1 %2 Nie można znaleźć zasobu: %1 Uaktualnij wszystko Uaktualnij zaznaczone Uaktualnienia (%1) Uaktualnienia Wersja: nieznany oceniający nie wybrano uaktualnień wybrano uaktualnienia © 2010-2016 Zespół programistów Plazmy 