��          L      |       �   >   �      �   9   �   A   &  
   h  �  s  *   \     �     �  7   �     �                                         '%1' needs a password to be accessed. Please enter a password. ... A default name for an action without proper labelUnknown A new device has been detected.<br><b>What do you want to do?</b> Do nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-27 07:28+0200
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 '%1' wymaga hasła. Proszę podać hasło. ... Nieznany Wykryto nowe urządzenie. <br><b>Co chcesz zrobić?</b> Nic nie rób 