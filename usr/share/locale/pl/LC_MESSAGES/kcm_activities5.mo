��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	  &   �	  2   �	     
     %
  	   ,
     6
     >
     U
     o
     �
     �
     �
  -   �
  R   �
     A     _     v  G   |     �     �     �       %        E     M     S     f     m     �     �  2   �     �  \   �  ,   K     x     �     �     �     �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-02 06:50+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 &Nie zapamiętuj Przechodzenie pomiędzy aktywnościami Przechodzenie pomiędzy aktywnościami (odwrotnie) Zastosuj Anuluj Zmień... Utwórz Ustawienia aktywności Utwórz nową aktywność Usuń aktywność Aktywności Informacje o aktywności Przełącznik aktywności Czy jesteś pewien, że chcesz usunąć '%1'? Umieść na czarnej liście wszystkie programy nie znajdujące się na tej liście Wyczyść niedawną historię Utwórz aktywność... Opis: Błąd wczytywania plików QML. Sprawdź swoją instalację.
Brakuje %1 D&la wszystkich programów Zapomnij o dniu Zapomnij wszystko Zapomnij o ostatniej godzinie Zapomnij o ostatnich dwóch godzinach Ogólne Ikona Zachowaj historię Nazwa: Tylko dla da&nych programów Inne Prywatność Prywatna - nie śledź wykonywania tej aktywności Zapamiętaj otwarte dokumenty: Pamiętaj bieżący pulpit wirtualny dla każdej aktywności (wymaga ponownego uruchomienia) Skrót do przełączenia na tę aktywność: Skróty Przełączanie Tapeta dla  miesiąc  miesiące  miesięcy zawsze 