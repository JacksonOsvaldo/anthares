��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  &        8  	   N     X      a     �     �     �     �     �     �  
   �     �     �             ?   %     e     ~     �  +   �     �     �     �     �          !     -     M     Q  4   Z  	   �  	   �  
   �  
   �  
   �  
   �     �  
   �     �               '     ,  (   E     n     r          �     �     �     �     �  
   �     �     �  	   �  	   �                         #     2     O     m     �     �  !   �     �     �     �     �       =   
     H     X     h     t     �     �  
   �     �     �     �  	   �     �     �     �     �     �  
   �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2017-08-12 08:06+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 
Moduł przeglądarki urządzeń Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 wolnych z %2 (%3% używanych) Baterie Rodzaj baterii:  Szyna:  Aparat foto Aparaty foto Stan naładowania:  Ładowanie Zwiń wszystko Czytnik Compact Flash Urządzenie Informacja o urządzeniu Pokaż wszystkie urządzenia, które są aktualnie wylistowane. Przeglądarka urządzeń Urządzenia Rozładowywanie mikmach@wp.pl, lukasz.wojnilowicz@gmail.com Zaszyfrowany Rozwiń wszystko System plików Rodzaj systemu plików:  W pełni naładowana Dysk twardy Można wypiąć pod napięciem? IDE IEEE1394 Pokaż informację o aktualnie wybranym urządzeniu. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Klawiatura Klawiatura + Mysz Etykieta:  Maksymalna szybkość:  Czytnik Memory Stick Podpięty w:  Mysz Odtwarzacze multimediów Mikołaj Machowski, Łukasz Wojniłowicz Nie Rozładowane Brak danych Niepodpięty Nieustawiona Napęd optyczny PDA Tablica partycji Podstawowy Procesor %1 Numer procesora:  Procesory Produkt:  Raid Wymienialny? SATA SCSI Czytnik SD/MMC Pokaż wszystkie urządzenia Pokaż powiązane urządzenia Czytnik Smart Media Napędy przechowywania danych Obsługiwane sterowniki:  Obsługiwane zestawy instrukcji:  Obsługiwane protokoły:  UDI:  UPS USB UUID:  Pokaż UDI bieżącego urządzenia (Unique Device Identifier) Nieznany napęd Niewykorzystany Wytwórca:  Przestrzeń wolumenu:  Przeznaczenie wolumenu:  Tak kcmdevinfo Nieznany Brak Brak Platforma Nieznany Nieznane Nieznane Nieznana Nieznane Czytnik xD 