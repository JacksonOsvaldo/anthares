��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     �     �  $   �     �     �     �     �  #        '     8     W     w  -   �  E   �  &   	  &   (	  #   O	  #   s	  !   �	  7   �	     �	  +   
     .
     4
     <
     M
     \
     n
     �
     �
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-11-25 07:12+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
  ms Akceleratory &klawiatury: Rodzaj przycisku górnej s&trzałki: Zawsze ukrywaj Zawsze pokazuj Czas &trwania animacji: Animacje Rodzaj prz&ycisku dolnej strzałki: Ustawienia Bryzy Wyśrodkuj karty na pasku kart Ze wszystkich pustych obszarów Tylko z paska tytułu Z paska tytułu, paska menu i paska narzędzi Rysuj wąską linię, aby wskazać uaktywnienie w menu i paskach menu Rysuj wskaźnik aktywności na liście Rysuj ramki wokół dokowalnych paneli Rysuj ramki wokół tytułów stron Rysuj ramki wokół paneli bocznych Rysuj kresy znaczące na suwakach Rysuj elementy oddzielające ikony na paskach narzędzi Włącz animacje Włącz rozszerzone uchwyty zmiany rozmiaru Ramki Ogólne Brak przycisków Jeden przycisk Paski przewijania Pokazuj, gdy są potrzebne Dwa przyciski Przeciąganie okien: 