��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  #   �     �     �     �     �     	  	             !     0     =     C     L     R     _     }  6   �  G   �                 )   4  
   ^     i     �  c   �  S   �     I     X     s     �  "   �     �     �     �     �     �                1      C     d       ,   �  #   �     �     �  	                  -  =   6      t     �     �     �     �     �          "  &   .  !   U     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-25 07:58+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Rozpoznaj po właściwości ok&na:  Wielkie Duże Brak obramowania Brak obramowania z boku Zwykłe Olbrzymie Małe Bardzo wielkie Bardzo duże Duży Normalny Mały Bardzo duży Poświata dla aktywnych okien Dodaj Dodaj uchwyty do zmiany rozmiaru okien bez obramowania Zezwalaj na zmianę rozmiaru okien zmaksymalizowanych za ich krawędzie Animacje Rozmiar przycisk&u: Rozmiar obramowania: Przejście przycisku po najechaniu myszą Do środka Do środka (pełna szerokość) Klasa:  Ustawienia przemijania pomiędzy cieniem i poświatą okna, gdy stan aktywny okna został zmieniony Ustawienia animacji podświetlania po wskazaniu myszą dotycząca przycisków okien Opcje wystroju Wykryj właściwości okna Okno dialogowe Edytuj Edytuj wyjątek - Ustawienia Tlenu Włącz/wyłącz ten wyjątek Rodzaj wyjątku Ogólne Ukryj pasek tytułu okna Informacje o wybranym oknie Do lewej Przesuń w dół Przesuń w górę Nowy wyjątek - Ustawienia Tlenu Pytanie - Ustawienia Tlenu Wyrażenie regularne Niepoprawna składnia wyrażenia regularnego &Rozpoznaj wyrażeniem regularnym:  Usuń Usunąć wybrany wyjątek? Do prawej Cienie Wyrówn&anie tytułu: Tytuł:  Używaj tych samych barw dla paska tytułu i zawartości okna Użyj klasy okna (cały program) Użyj tytułu okna Ostrzeżenie - Ustawienia Tlenu Nazwa klasy okna Rzucanie cienia przez okna Rozpoznawanie okna Wybór właściwości okna Tytuł okna Przejścia zmiany stanu aktywnego okna Wymuszenia dla określonych okien 