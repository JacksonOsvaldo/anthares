��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  #   E  H   i  6   �     �  (        /     M     j  4   �     �     �     �     �      	  �   	     �	  j   �	      g
  |   �
  P        V                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-25 07:51+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Zastosuj pakiet wyglądu i odczucia Narzędzie wiersza poleceń do stosowania pakietów wyglądu i odczucia. Ustawienia dotyczące wrażeń wzrokowych i dotykowych Copyright 2017, Marco Martin Ustawienia wskaźnika uległy zmienianie Pobierz nowe pakiety wyglądu lukasz.wojnilowicz@gmail.com Pobierz nowy wygląd... Wyszczególnij dostępne pakiety wyglądu i odczucia Narzędzie wyglądu i odczucia Opiekun Marco Martin Łukasz Wojniłowicz Wyzeruj układ pulpitu Plazmy Wybierz powszechny wygląd twojej przestrzeni pracy (ustawienie to wpłynie na wygląd plazmy, zestaw kolorystyczny, wskaźniki myszy, przełączniki okien i pulpitów, ekran powitalny, ekran blokady itp.) Pokaż podgląd Ten moduł umożliwia ustawienia wyglądu dla całej przestrzeni roboczej, dzięki kilku gotowym zestawom. Użyj układu pulpitu z wyglądu Uwaga: twój układ pulpitu Plazmy zostanie utracony i wyzerowany do układu domyślnego dostarczonego z wybranym wyglądem. Należy ponownie uruchomić KDE, aby zmiany do kursorów zostały uwzględnione. nazwa pakietu 