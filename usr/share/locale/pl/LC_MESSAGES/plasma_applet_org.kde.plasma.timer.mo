��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     d     x     �     �     �     �  
   �     �     �     �  R   �     C  
   U     `     u     �     �  
   �     �     �     �  h   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-25 08:27+0200
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 %1 jest uruchomiony %1 nie jest uruchomiony Wyze&ruj &Uruchom Zaawansowane Wygląd Polecenie: Wyświetlanie Wykonaj polecenie Powiadomienia Pozostały czas: %1 sekunda Pozostały czas: %1 sekundy Pozostały czas: %1 sekund Wykonaj polecenie Za&trzymaj Pokaż powiadomienia Pokaż sekundy Pokaż tytuł Tekst: Czasomierz Czasomierz skończył Czasomierz jest uruchomiony Tytuł: Użyj kółka myszy, aby zmienić cyfry lub wybierz jeden z nastawionych czasomierzy z menu podręcznego 