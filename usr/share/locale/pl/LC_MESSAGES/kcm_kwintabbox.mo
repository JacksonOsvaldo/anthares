��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �  	   �     �               -     <     I     R     ^     s     �     �     �  O   �     	  (   	     ?	     K	     h	     q	     �	     �	     �	     �	     �	     �	     �	     �	     
  [   �
     �
     �
                         !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-12-12 08:21+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Działań Pozostałe działania Pozostałe pulpity Wszystkie inne ekrany Wszystkie okna Alternatywna Pulpit 1 Zawartość Bieżące działanie Bieżący program Bieżący pulpit Bieżący ekran Odfiltruj okna według Ustawienia polityki uaktywniania ograniczają funkcjonalność nawigacji okien. Naprzód Pobierz nowy układ przełączania okien Ukryte okna Dodaj ikonę "pokaż pulpit" Główna Zminimalizowanych Tylko jedno okno na program Ostatnio używanych Wstecz Ekrany Skróty Pokaż wybrane okno Uszereguj według: Kolejności otwarcia Wybrane okno zostanie podświetlone, pozostałe okna zaś zostaną przyciemnione. Ta opcja wymaga włączenia efektów pulpitu. Efekt zastępujący przełączanie między oknami w przypadku aktywowania efektów pulpitu. Pulpitów wirtualnych Widoczne okna Wizualizacja 