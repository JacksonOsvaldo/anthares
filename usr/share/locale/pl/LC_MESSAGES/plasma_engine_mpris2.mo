��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  U   w     �     �     �          0  ?   L  E   �     �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-21 08:21+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 Próba wykonania działania '%1' zakończyła się niepowodzeniem z komunikatem '%2'. Odtwórz następny plik Odtwórz poprzedni plik Pilot multimedialny Odtwórz/Wstrzymaj odtwarzanie Zatrzymaj odtwarzanie pliku Brakuje argumentu '%1' dla działania '%2' lub podano zły typ. Odtwarzacz multimediów '%1' nie może wykonać tego działania '%2'. Operacja '%1' jest nieznana. Nieznany błąd. 