��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     �     �  C   �             2   *     ]     n  +   }      �     �  "   �  3   	         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-11-25 07:13+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Ustawienia wystroju pulpitu David Rosca eugenewolfe@o2.pl, maciej.wiklo@wp.pl, lukasz.wojnilowicz@gmail.com Pobierz nowe wystroje... Wgraj z pliku... Artur Chłond, Maciej Wikło, Łukasz Wojniłowicz Otwórz wystrój Usuń wystrój Pliki wystrojów (*.zip *.tar.gz *.tar.bz2) Nie udało się wgrać wystroju. Pomyślnie wgrano wystrój. Nie udało się usunąć wystroju. Ten moduł umożliwia określenie wystroju pulpitu. 