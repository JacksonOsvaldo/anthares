��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �         
     
             /     C     V     ^     g     ~  v   �     �  3        E     N     [     d     k     r     �     �     �     �     �     �     	  $   (	     M	     j	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-10-01 10:13+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Wybierz... Wyczyść ikonę Dodaj do ulubionych Wszystkie programy Wygląd Programy Uaktualnione programy. Komputer Przeciągaj karty pomiędzy polami, aby je pokazać/ukryć lub przeszereguj widoczne karty poprzez ich przeciąganie.  Edytuj programy... Rozszerz wyszukiwanie na zakładki, pliki i pocztę Ulubione Ukryte karty Historia Ikona: Wyjdź Przyciski menu Najczęściej używane Na wszystkch aktywnościach Na bieżącej aktywności Usuń z ulubionych Pokaż w ulubionych Pokazuj programy po nazwie Uszereguj alfabetycznie Przełączaj na kartę po najechaniu Wpisz co chcesz znaleźć... Widoczne karty 