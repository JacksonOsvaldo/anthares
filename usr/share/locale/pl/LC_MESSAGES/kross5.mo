��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     	     	     7	     G	     \	     d	     w	  
   �	  
   �	  F   �	     �	     �	  	   �	     
  2   
  3   P
  %   �
  !   �
     �
     �
     �
  (   �
  3        C     J     \     z     �     �      �  	   �  (   �     �  <   �     6                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-06-20 06:58+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer Skrypt do wykonania. Ogólne Dodaj nowy skrypt. Dodaj... Anulować? Komentarz: mrudolf@kdewebdev.org, eugenewolfe@o2.pl, lukasz.wojnilowicz@gmail.com Edycja Edytuj wybrany skrypt. Edytuj... Wykonaj wybrany skrypt. Nie można utworzyć skryptu dla interpretera "%1" Nie można określić interpretera dla skryptu "%1" Nie można wczytać interpretera "%1" Nie można otworzyć skryptu "%1" Plik: Ikona: Interpreter: Poziom bezpieczeństwa interpretera Ruby Michał Rudolf, Artur Chłond, Łukasz Wojniłowicz Nazwa: Brak funkcji "%1" Interpreter "%1" nie istnieje Usuń Usuń wybrany skrypt. Wykonaj Nie znaleziono pliku skryptu %1. Zatrzymaj Zatrzymaj wykonywanie wybranego skryptu. Tekst: Narzędzie wiersza poleceń do uruchamiania skryptów Kross. Kross 