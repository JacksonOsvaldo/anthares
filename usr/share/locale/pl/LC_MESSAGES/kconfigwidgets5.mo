��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     �     �     �     �     �     �     �  
             &     2     C  	   V     `     l     �     �     �     �     �     �     �     �     �  
     	     
        "     4     @     S     [     n     �  	   �     �  
   �     �     �     �  
   �     �       
        !     A     R     c           �     �     �     �     �       F   %     l  	        �     �     �     �     �               7     P     i     �     �  3   �     �     �  0         1     N     h     z     �     �     �     �  (   �  3   �     %     5      E     f     x     �     �     �  7   �     !  v   :      �     �     �           ,     H     U  /   p     �  (   �  3   �  
   
  
     ;      -   \  <   �     �  	   �     �     �  	   �     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-09-03 07:41+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
>
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Podrę&cznik %1 O progr&amie %1 Rzeczywisty rozmi&ar Dodaj z&akładkę &Wstecz Z&amknij &Ustawienia %1... &Kopiuj &Usuń &Darowizna &Edytuj zakładki... &Znajdź... Pi&erwsza strona &Dopasuj do strony &Naprzód I&dź do... &Idź do wiersza... I&dź do strony... &Ostatnia strona &Wyślij... &Przenieś do kosza &Nowy &Następna strona &Otwórz... &Wklej &Poprzednia strona &Drukuj... Za&kończ &Odśwież &Zmień nazwę... &Zastąp... &Zgłoś błąd... Zapi&sz Zapi&sz ustawienia Sprawdzanie pi&sowni... &Cofnij Do &góry &Powiększenie... &Następna &Poprzednia &Pokaż porady po uruchomieniu Czy wiesz, że...?
 Ustawienia Wskazówka dnia O &KDE Wy&czyść Sprawdź pisownię w dokumencie Wyczyść listę Zamknij dokument Ustawie&nia powiadomień... Ustawienia &skrótów... Ustawienia pasków &narzędzi... Kopiuj zaznaczenie do schowka Utwórz nowy dokument Wy&tnij Wytnij zaznaczenie do schowka O&dznacz wszystko mrudolf@kdewebdev.org, eugenewolfe@o2.pl, lukasz.wojnilowicz@gmail.com Wykryj samoczynnie Domyślne Tryb pełno&ekranowy Znajdź &następne Znajdź &poprzednie Dopasuj do wy&sokości strony Dopasuj do sze&rokości strony Idź wstecz w dokumencie Idź do przodu w dokumencie Idź do pierwszej strony Idź do ostatniej strony Idź do następnej strony Idź do poprzedniej strony Idź do góry Michał Rudolf, Artur Chłond, Łukasz Wojniłowicz Brak wpisów Otwórz pop&rzedni Otwórz dokument, który był ostatnio otwierany Otwórz istniejący dokument Wklej zawartość schowka Podgląd &wydruku Wydrukuj dokument Zakończ program Przyw&róć Przyw&róć Odśwież dokument Przywróć ostatnio cofnięte działanie Przywróć niezapisane zmiany dokonane w dokumencie Zapisz j&ako... Zapisz dokument Zapisz dokument pod nową nazwą &Zaznacz wszystko Wybierz poziom powiększenia Wyślij dokument przez pocztę Wyświetlaj pasek &menu Wyświetlaj pasek &narzędzi Pokaż menu<p>Ponownie wyświetla ukryty pasek menu</p> Wyświetlaj pasek s&tanu Pokaż pasek stanu <br/><br/>Pokazuje pasek stanu, który jest paskiem na dole okna, używanym do informacji o stanie. Pokaż podgląd wydruk dokumentu Pokaż lub ukryj pasek menu Pokaż lub ukryj pasek stanu Pokaż lub ukryj pasek narzędzi Zmień język ap&likacji... Porada &dnia Cofnij ostatnie działanie Obejrzyj dokument w jego rzeczywistym rozmiarze Co to jes&t? Brak uprawnień do zapisywania ustawień Do zapisania ustawień trzeba będzie podać hasło Powię&ksz Pomnie&jsz Powiększ, tak aby zmieścić stronę na wysokość w oknie Powiększ, tak aby zmieścić stronę w oknie Powiększ, tak aby zmieścić stronę na szerokość w oknie &Wstecz &Naprzód &Strona domowa Po&moc bez nazwy 