��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     t     �     �     �     �     �  J   �                          
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-10-04 20:56+0200
Last-Translator: Marta Rybczyńska <kde-i18n@rybczynska.net>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Słowo wyzwalające: Dodaj element Alias Alias: Konfiguracja: Znaki specjalne Kod Tworzy znaki z :q: jeśli jest to kod szesnastkowy lub zdefiniowany alias. Usuń element Kod szesnastkowy: 