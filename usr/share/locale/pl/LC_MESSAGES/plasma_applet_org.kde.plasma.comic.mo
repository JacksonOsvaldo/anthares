��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  z   �  &   �     #     :  &   I     p     �     �  	   �  (   �     �     �  *   	     4     H     g     n     �  0   �  %   �  $   �       1        L     \  %   o  .   �     �     �     �     �  $   	     .  
   A     L     j     �     �  �   �  \   q  #   �     �  1   �     ,     E     [     x     �  '   �  
   �     �     �     
       
              @     D     H     O     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-11-14 07:55+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 

Wybierz poprzedni pasek komiksowy, aby przejść do ostatniego paska komiksowego przechowywanego w pamięci podręcznej. &Utwórz archiwum zeszytu komiksów... &Zapisz komiks jako... &Numer strony: *.cbz|Archiwum zeszytu komiksów (Zip) Rzeczywisty rozmi&ar Z&apisz bieżącą pozycję Zaawansowane Wszystkie Wystąpił błąd dla identyfikatora %1. Wygląd Nieudane archiwizowanie komiksu Samoczynnie uaktualniaj wtyczki komiksów: Pamięć podręczna Sprawdź nowe paski komiksowe: Komiks Pamięć podręczna komiksu: Ustawienia... Nie można utworzyć archiwum w podanym miejscu. Utwórz archiwum zeszytu komiksów %1 Tworzenie archiwum zeszytu komiksów Cel: Wyświetl błąd przy nieudanym uzyskaniu komiksu Pobierz komiksy Obsługa błędów Nieudane dodawanie pliku do archiwum. Nieudane tworzenie pliku z identyfikatorem %1. Od początku do ... Od końca do ... Ogólne Pobierz nowe komiksy... Pobranie komiksu nie powiodło się: Przejdź do strony Informacja Przejdź do &aktualnej strony Przejdź do &pierwszej strony Przejdź do strony... Ręczny zakres Być może nie ma połączenia z internetem.
Być może wtyczka komiksu jest uszkodzona.
Innym powodem może być brak komiksu na dzisiaj/o tym numerze/z tą nazwą, wybranie innego może pomóc. Naciśnij środkowym przyciskiem myszy na komiks, aby wyświetlić go w pierwotnym rozmiarze Nie istnieje plik zip, przerywanie. Zakres: Pokaż strzałki tylko po najechaniu wskaźnikiem Pokaż adres URL komiksu Pokaż autora komiksu Pokaż identyfikator komiksu Pokaż tytuł komiksu Identyfikator paska: Zakres pasków komiksu do archiwizacji. Uaktualnij Odwiedź witrynę komiksu Odwiedź &witrynę sklepu # %1  dni dd.mm.rrrr &Następna karta z nowym paskiem Od: Do: minuty pasków na komiks 