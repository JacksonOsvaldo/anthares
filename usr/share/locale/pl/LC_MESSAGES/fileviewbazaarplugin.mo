��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  $   �	  +   �	  2   
     K
  "   g
     �
     �
  ,   �
  "   �
  "     *   2      ]  #   ~  &   �  )   �  0   �     $     B     U  '   l      �  +   �  !   �  $        (     :     W     f     v     �     �     �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2014-04-06 08:12+0200
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Dodano pliki do repozytorium Bazaar. Dodawanie plików do repozytorium Bazaar... Nieudane dodawanie plików do repozytorium Bazaar. Zamknięto dziennik Bazaar. Nieudane dokonywanie zmian Bazaar. Dokonano zmiany Bazaar. Przesyłanie zmian Bazaar... Nieudane pociągnięcie repozytorium Bazaar. Pociągnięto repozytorium Bazaar. Pociąganie repozytorium Bazaar... Nieudane popchnięcie repozytorium Bazaar. Popchnięto repozytorium Bazaar. Popchnięcie repozytorium Bazaar... Usunięto pliki z repozytorium Bazaar. Usuwanie plików z repozytorium Bazaar... Nieudane usuwanie plików z repozytorium Bazaar. Nieudane przeglądanie zmian. Przejrzano zmiany. Przeglądanie zmian... Nieudane uruchamianie dziennika Bazaar. Uruchamianie dziennika Bazaar... Nieudane uaktualnianie repozytorium Bazaar. Uaktualnione repozytorium Bazaar. Uaktualnianie repozytorium Bazaar... Bazaar - Dodaj... Bazaar - Prześlij zmiany... Bazaar - Usuń Dziennik Bazaar Bazaar - Pociągnij Bazaar - Popchnij  Bazaar - Uaktualnij  Pokaż lokalne zmiany Bazaar 