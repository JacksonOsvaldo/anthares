��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     w     �     �     �     �      �     �     �               #     *     <     L     U  	   \  
   f     q     y  
   {     �     �     �     �     �     �     �      �  
             4     D  D   W  F   �     �     �     �  	             )     <     A     T     i     z     }     �  	   �     �     �  
   �     �     �     �  
     O     �   a     !     *  
   A     L     j          �     �     �     �     �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-12-02 07:06+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 Aktywności Dodaj działanie Dodaj wypełniacz Dodaj elementy interfejsu... Alt Alternatywne elementy interfejsu Zawsze widoczny Zastosuj Wprowadzanie zmian Zastosuj teraz Autor: Samoczynnie ukryj Przycisk wstecz Do dołu Anuluj Kategorie Do środka Zamknij + Ustawienia Ustawienia aktywności Utwórz aktywność... Ctrl Obecnie używane Usuń Adres pocztowy: Przycisk wprzód Pobierz nowe elementy interfejsu Wysokość Przewijanie w poziomie Wprowadź tutaj Skróty klawiszowe Nie można zmienić układu, gdy elementy interfejsu są zablokowane Zmiany układu należy zastosować przed zastosowaniem kolejnych zmian Układ: Do lewej Lewy przycisk Licencja: Zablokuj elementy interfejsu Maksymalizuj panel Meta Środkowy przycisk Więcej ustawień... Działania myszy OK Wyrównanie panelu Usuń panel Do prawej Prawy przycisk Krawędź ekranu Znajdź... Shift Zatrzymane aktywność Zatrzymane aktywności: Przełącz Ustawienia bieżącego modułu uległy zmianie. Zapisać, czy porzucić zmiany? Po naciśnięciu tego skrótu pojawi się aplet; zostanie przekazana obsługa klawiatury i jeśli aplet ten będzie miał okno wysuwne (takie jak np. menu start), to okno to zostanie otwarte. Do góry Wycofaj odinstalowanie Odinstaluj Odinstaluj element interfejsu Przewijanie w pionie Widoczność Tapeta Rodzaj tapety: Elementy interfejsu Szerokość Okna mogą przykryć Okna są przykrywane 