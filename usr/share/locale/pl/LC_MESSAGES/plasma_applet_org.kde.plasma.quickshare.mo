��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �            +   $  X   P     �     �     �     �     �     �                    .  !   B     d     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-05-02 08:00+0200
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Zamknij Skopiuj samoczynnie: Nie pokazuj tego okna, skopiuj samoczynnie. Przeciągnij i upuść na mnie tekst lub obraz, aby wysłać go do usługi internetowej. Błąd przy wysyłaniu. Ogólne Rozmiar historii: Wklej Proszę czekać Spróbuj ponownie. Wysyłanie... Udostępnij Zasoby dla '%1' Pomyślnie wysłano Właśnie udostępniono adres URL Wyślij %1 do usługi sieciowej 