��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s     \     l     �     �     �     �     �  
   �  2   �     �          $     ,     ;     B     Y     v     �     �     �     �     �     �     �  %        ,     4     G     W                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-25 07:21+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 %1 autorstwa %2 Dodaj własną tapetę Dodaj katalog... Dodaj obraz... Tło: Rozmycie Wyśrodkowana Zmień co: Katalog z tapetami, z którego wyświetlać obrazy Pobierz tapety Pobierz nowe tapety... Godziny Pliki obrazów Minuty Następny obraz tapety Otwórz katalog zawierający Otwórz obraz Otwórz obraz tapety Umieszczenie: Polecany plik tapety Usuń tapetę Przywróć tapetę Przeskalowana Przeskalowana i przycięta Przeskalowana z zachowaniem kształtu Sekundy Wybierz kolor tła Jednolita barwa Kafelki 