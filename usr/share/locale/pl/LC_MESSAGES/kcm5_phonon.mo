��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  U   �  �   &      �  w   �  $   G     l  A   �     �  @   �          #     1  #   :  9   ^  8   �  5   �          &  g   .     �     �  �   �  5   P  G   �     �     �     �               &     .     F     Y  %   h     �     �  '   �     �     �     �     �       
             +     ;     [     g     t     �  (   �     �     �     �       �        �  ?   �  �        �  >   �  9   �  6   "  *   Y     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-11-09 08:28+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Aby zastosować zmianę silnika będzie trzeba wylogować się i zalogować ponownie. Lista programów obsługujących wykorzystywanych przez Phonon.  Kolejność tutaj odpowiada tej, w jakiej Phonon będzie ich używał. Zastosuj listę urządzeń do... Zastosuj obecnie pokazywaną listę preferowanych urządzeń do następujących innych kategorii odtwarzania dźwięku: Ustawienia urządzenia dźwiękowego Odtwarzanie dźwięku Preferowane urządzenie odtwarzające dźwięk dla kategorii '%1' Nagrywanie dźwięku Preferowane urządzenie nagrywające dźwięk dla kategorii '%1' Obsługa Colin Guthrie Złącze Prawa autorskie 2006 Matthias Kretz Domyślnie preferowane urządzenie odtwarzające dźwięk Domyślnie preferowane urządzenie nagrywające dźwięk Domyślnie preferowane urządzenie nagrywające obraz Domyślna/nie podana kategoria W dół Definiuje domyślny porządek urządzeń, który może zostać nadpisany przez poszczególne kategorie. Ustawienia urządzenia Preferowane urządzenia Urządzenia znalezione na twoim systemie, odpowiednie dla wybranej kategorii. Wybierz urządzenie, które ma być używane przez programy. kde-i18n@rybczynska.net, lukasz.wojnilowicz@gmail.com Nie udało się ustawienie wybranego urządzenia wyjściowego dźwięku Przedni środkowy Przedni lewy Przedni lewy środka Przedni prawy Przedni prawy środka Sprzęt Niezależne urządzenia Poziomy wejściowe Nieprawidłowe Ustawienia sprzętu dźwiękowego KDE Matthias Kretz Mono Marta Rybczyńska, Łukasz Wojniłowicz Phonon: moduł ustawień Odtwarzanie (%1) W górę Profil Tylny środkowy Tylny lewy Tylny prawy Nagrywanie (%1) Pokaż zaawansowane urządzenia Boczny lewy Boczny prawy Karta dźwiękowa Urządzenie dźwiękowe Rozmieszczenie głośników i testowanie Głośnik niskotonowy Test Przetestuj wybrane urządzenie Testowanie %1 Kolejność określa preferencje urządzeń. Jeżeli z jakiejś przyczyny nie można użyć pierwszego urządzenia, Phonon spróbuje użyć drugiego i tak dalej. Nieznany kanał Użyj obecnie pokazywanej listy urządzeń dla wielu kategorii. Różne kategorie przypadków użycia mediów. Dla każdej kategorii, możesz wybrać jakie urządzenie preferujesz do użytku przez programy Phonon. Nagrywanie obrazu Preferowane urządzenie nagrywające obraz dla kategorii '%1'  Twój sprzęt może nie obsługiwać nagrywania dźwięku Twój sprzęt może nie obsługiwać nagrywania obrazu brak preferencji dla wybranego urządzenia preferuj wybrane urządzenie 