��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �     �  |   �  C   A     �     �  8   �     �     �     	     %	     B	     T	     d	     r	     z	     �	     �	     �	     �	  +   �	     
     )
     1
     J
  
   _
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-10 09:38+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 1.5
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Działanie: Program próbuje wykonać działanie, które wymaga uprawnień. Wymagane jest uwierzytelnienie do wykonania tego działania. Inny klient właśnie uwierzytelnia, proszę spróbować później. Program: Wymagane uwierzytelnienie Uwierzytelnianie nieudane, proszę spróbować ponownie. Naciśnij, aby edytować %1 Naciśnij, aby otworzyć %1 Szczegóły lukasz.wojnilowicz@gmail.com Poprzedni opiekun Jaroslav Reznik Lukáš Tinkl Opiekun Łukasz Wojniłowicz H&asło: Hasło dla '%1': Hasło dla administratora: Hasło lub odcisk palca dla %1: Hasło lub odcisk palca dla administratora: Hasło lub odcisk palca: Hasło: Agent PolicyKit1 dla KDE Wybierz użytkownika Wytwórca: 