��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     �  	   �  >   �  
     	         *  	   3     =     I  	   Q     [  	   k     u     �     �  .   �     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-20 09:05+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Przeglądaj... &Znajdź: *.png *.xpm *.svg *.svgz|Pliki ikon (*.png *.xpm *.svg *.svgz) Działania Wszystkie Programy Kategorie Urządzenia Symbole Emotikony Źródło ikony Typy MIME &Inne ikony: Szybki dostęp Ikony &systemowe: Szukaj interaktywnie nazw ikon (np. katalogu). Wybierz ikonę Stan 