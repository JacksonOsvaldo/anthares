��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �    �     �     �     �     �  6   �     %     2     P     _  	   m     w  4        �     �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-29 08:05+0100
Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
Language-Team: Polish <kde-i18n-doc@kde.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Wygląd Przeglądaj... Wybierz obraz Piętnaście kawałków Pliki obrazów (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Kolor liczby Ścieżka do własnego obrazu Kolor kawałka Pokaż liczby Wymieszaj Rozmiar Rozwiąż poprzez ułożenie w odpowiednim porządku Rozwiązane! Spróbuj ponownie. Czas: %1 Użyj własnego obrazu 