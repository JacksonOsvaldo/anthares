��          �      �       H     I  @   ]     �     �     �     �     �     �  
   �     �     �          %  �  D       W        w     �     �     �     �     �     �  	   �     �     �  %               
   	                                                  <a href='%1'>%1</a> Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-20 14:38+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Aruncați peste mine o imagine sau text pentru a le încărca într-un serviciu online. Eroare la încărcare. General Dimensiune istoric: Lipește Așteptați Reîncercați Expediere... Partajare Partajări pentru „%1” Încărcat cu succes Încarcă %1 într-un serviciu online 