��            )         �     �  �   �     I     R     Y     e     q     }     �     �     �     �      �     �     �     �               *     7     V     [     p     �     �     �      �     �     �     �  �  �     �     �     �     �     �     �     �               !  
   6  	   A     K     Y     a     u  $   �  
   �  
   �     �     �     �     �          .     C  "   L     o  
   u     �                                                            	   
                                                                         ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Cursor Theme: Customize theme Default Description EMAIL OF TRANSLATORSYour emails General Halt Command: Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit SDDM KDE Config Session: The default cursor theme in SDDM Theme User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2015-04-20 14:26+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 ... (Dimensiuni disponibile: %1) Avansat Autor &Autentificare automată Fundal: Curăță imaginea Comenzi Temă de cursor: Personalizează tema Implicită Descriere sergiu@cip.md General Comandă de oprire: Încarcă din fișier... Ecran de autentificare folosind SDDM UID maxim: UID minim: Sergiu Bivol Denumire Nu există previzualizare Comandă de repornire: Reautentificare după ieșire Configurare KDE SDDM Sesiune: Tema de cursor implicită în SDDM Temă Utilizator Utilizator: 