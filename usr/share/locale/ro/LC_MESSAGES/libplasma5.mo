��    N      �  k   �      �     �  (   �  +   �  +   
  ,   6     c          �     �     �     �     �     �     �  	             =     Y     w     �     �     �  &   �  
   �     �     	     	  	   .	     8	     P	     Y	     l	     x	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
  
   ,
     7
     :
     J
     d
     q
     
     �
     �
     �
     �
     �
     �
          $     2  !   E     g     �     �     �     �     �     �  	   �  	   �     �     �     �               )  �  E               ,     ?     K     W     q     �     �     �     �     �     �       
        )      I     j  "   �  	   �     �     �  )   �               *     9  	   N     X     h     p     �     �  !   �     �     �     �     �          !     (     ?     Z     k     �  
   �     �     �     �     �     �     �     �          (     ?     ]  "   r     �     �     �  *   �  $        )  	   :     D  
   K     V     o  	   �     �     �     �     �     �     �     �               "       H   @      B           +              I             8              7                 ;   $   9      2   L           A   )            6   &   ,   M   /       #             <   C      0   *          =          	       ?   5   K       1   E       '   4   J          N       -   >         (   D   G   F                  :          !              3                 %   
   .          %1 Settings %1 is the name of the applet%1 Settings %1 is the name of the applet%1 Settings... %1 is the name of the appletRemove this %1 %1 is the name of the containment%1 Options A desktop has been removed. A panel has been removed. Accessibility Activate %1 Widget Activity Settings Add Widgets... Alternatives... Analog clock face Application Launchers Astronomy Background for graphing widgets Background image for panels Background image for tooltips Background image for widgets Cancel Configuration Definitions Configuration XML file Could not find requested component: %1 Data Files Date and Time Desktop Removed Development Tools Education Environment and Weather Examples Executable Scripts File System Fun and Games Generic dialog background Graphics Icon Images Images for dialogs Images for widgets Language Lock Widgets Login Manager Logout Dialog Main Script File Miscellaneous Multimedia OK Online Services Opaque images for dialogs Open with %1 Panel Removed Productivity Remove this Activity Remove this Panel Remove this Widget Run the Associated Application Screenlocker Script initialization failed Service Descriptions Splash Screen System Information The widget "%1" has been removed. Theme for the logout dialog Themed Images Translations Undo Unknown Unlock Widgets User Interface Utilities Wallpaper Wallpaper packages Widget Removed Widget Settings Widgets Windows and Tasks misc categoryMiscellaneous Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-10 02:58+0100
PO-Revision-Date: 2015-04-26 15:29+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Configurări %1 Configurări %1 Configurări %1... Elimină %1 Opțiuni %1 A fost eliminat un birou. Un panou a fost eliminat. Accesibilitate Activează controlul %1 Configurări activitate Adăugare controale... Alternative... Mutra ceasului analog Lansatori de aplicații Astronomie Fundal pentru controale grafice Imagine de fundal pentru panouri Fundal pentru indicii Imagine de fundal pentru controale Renunță Definiții de configurare Fișier de configurare XML Nu s-a putut găsi componenta cerută: %1 Fișiere cu date Data și ora Birou eliminat Unelte de dezvoltare Educație Mediu și vreme Exemple Scripturi executabile Sistem de fișiere Distracție și jocuri Fundal universal pentru dialoguri Grafică Pictogramă Imagini Imagini pentru dialoguri Imagini pentru controale Limbă Blochează controalele Gestionar de autentificare Ecran de ieșire Fișier-script principal Diverse Multimedia OK Servicii online Imagini opace pentru dialoguri Deschide cu %1 Panou eliminat Productivitate Elimină această activitate Elimină acest panou Elimină acest control Rulează aplicația asociată Blocare pentru ecran Inițializarea scriptului a eșuat Descriere servicii Ecran de întâmpinare Informații despre sistem Controlul grafic „%1” a fost eliminat. Tematică pentru dialogul de ieșire Imagini tematice Traduceri Desfă Necunoscut Deblochează controalele Interfață cu utilizatorul Utilitare Fundal Pachete cu fundaluri Control eliminat Configurări control Controale grafice Ferestre și sarcini Diverse 