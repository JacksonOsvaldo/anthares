��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     "     '  �  G  �  �  S   {
     �
     �
     �
  +   
     6     K     _                                               
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-04-25 18:23+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@lists.kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;

  sec &Temporizare indicator pornire: <h1>Notificare în bara de procese</h1>
Puteți activa a doua metodă de notificare care este utilizată de bara de procese, unde apare un buton cu o clepsidră rotitoare care simbolizează că aplicația pornită se încarcă. Se poate întâmpla ca unele aplicații să nu suporte această opțiune. În acest caz butonul dispare după perioada de timp specificată în "Temporizare indicator pornire". <h1>Cursor ocupat</h1>
KDE oferă facilitatea de cursor ocupat pentru notificarea pornirii aplicațiilor.
Pentru a activa această opțiune selectați unul din mijloacele de indicare vizuală din căsuța combinată.
Se poate întâmpla ca unele aplicații să nu suporte această opțiune. În acest caz cursorul se oprește din clipire după perioada de timp specificată în „Temporizare indicator pornire”. <h1>Pornire</h1> Aici puteți configura modul de indicare a pornirii aplicațiilor. Cursor ocupat clipitor Cursor ocupat animat Cursor &ocupat Activează notificarea în &bara de procese Fără cursor ocupat Cursor ocupat pasiv &Temporizare indicator pornire: &Notificare în bara de procese 