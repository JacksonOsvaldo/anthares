��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  U   �  c     "   t  a   �     �       F   #     j  H   ~     �     �     �     �  5     7   9  7   q  "   �     �  a   �     6     M  �   e     �  5        7     E     T     n     }  
   �     �     �     �      �     �          
          3     ?     H     O     \     j     x     �     �     �     �     �     �  	     	        #     ?  �   O     �  J      �   K     �  I   �  >   E  >   �  ,   �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-04-09 19:27+0300
Last-Translator: Cristian Oneț <onet.cristian@gmail.com>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 Pentru a aplica schimbarea suportului, va trebui să vă reautentificați în sistem. O listă cu suporturi Phonon găsite în sistem. Aceasta este ordinea în care le va folosi Phonon. Aplică lista dispozitivelor la... Aplică lista afișată cu configurări de dispozitive la următoarele categorii de redare audio: Configurare echipament audio Redare audio Configurare a dispozitivului de redare audio pentru categoria „%1” Înregistrare audio Configurare a dispozitivului de captură audio pentru categoria „%1” Suport Colin Guthrie Conector Copyright 2006 Matthias Kretz Configurare a dispozitivului de redare audio implicit Configurare a dispozitivului de captură audio implicit Configurare a dispozitivului de captură video implicit Categorie imlicită/nespecificată Amână Definește ordinea implicită a dispozitivelor, ce poate fi înlocuită de categorii individuale. Configurare dispozitiv Preferință dispozitiv Dispozitive găsite în sistem, potrivite pentru categoria aleasă. Alegeți dispozitivul care doriți să fie utilizat de către aplicații. sergiu@ase.md Setarea dispozitivului de ieșire audio ales a eșuat Centru față Stânga față Față, stânga de centru Dreapta față Față, dreapta de centru Echipament Dispozitive independente Nivele de intrare Nevalid Configurare echipament audio KDE Matthias Kretz Mono Sergiu Bivol Modul de configurare Phonon Redare (%1) Preferă Profil Centru spate Stânga spate Dreapta spate Înregistrare (%1) Afișează dispozitive avansate Stânga lateral Lateral dreapta Placă de sunet Dispozitiv de sunet Amplasare și testare difuzoare Subwoofer Testează Testează dispozitivul ales Se testează %1 Ordinea determină preferința pentru dispozitive. Dacă cumva primul dispozitiv nu poate fi folosit, Phonon îl va încerca pe al doilea și așa mai departe. Canal necunoscut Folosește lista de dispozitive afișată acum pentru mai multe categorii. Diverse categorii de utilizare multimedia. Pentru fiecare categorie, puteți alege ce dispozitiv preferați să fie utilizat de către aplicațiile Phonon. Înregistrare video Configurare a dispozitivului de captură video pentru categoria „%1”  Platforma dumneavoastră poate să nu poată înregistra audio Platforma dumneavoastră poate să nu poată înregistra video nici o preferință pentru dispozitivul ales preferă dispozitivul selectat 