��    	      d      �       �      �   .   �           "      /  *   P      {  0   �  �  �     �  &   �     �     �  !   �  -         .  ;   K     	                                         Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: The global keyboard shortcut to lock the screen. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2015-04-20 14:36+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Eroare Testarea blocării ecranului a eșuat. Imediat Blochează sesiunea Blochează ecranul automat după: Blochează ecranul la trezirea din suspendare Ce&re parolă după blocare: Accelerator de tastatură global pentru blocarea ecranului. 