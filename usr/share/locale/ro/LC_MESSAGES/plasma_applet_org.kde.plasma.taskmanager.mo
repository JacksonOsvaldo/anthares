��          �   %   �      0  U   1     �     �  
   �     �     �     �     �     �     �  	   �                     .  )   4  (   ^  '   �  "   �     �     �  9   �  J   #  �  n     H  	   _     i     z     �     �     �     �     �     �     �     �     �             /     *   F  +   q  !   �     �     �     �  %   �                                                                            
                           	                        Activities a window is currently on (apart from the current one)Also available on %1 Alphabetically By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum rows: On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2013-11-23 20:40+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Disponibilă și pe %1 Alfabetic După activitate După birou După denumirea programului Nu grupa Nu sorta Filtre General Grupare și sortare Grupare: Evidențiază ferestrele Manual Maxim rânduri: pe %1 Arată numai sarcinile din activitatea curentă Arată numai sarcinile de pe biroul curent Arată numai sarcinile de pe ecranul curent Arată numai sarcinile minimizate Arată indicii Sortare: Disponibilă pe %1 Disponibilă în toate activitățile 