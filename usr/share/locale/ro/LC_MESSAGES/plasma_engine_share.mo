��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  0     ,   4  ;   a     �  &   �  "   �          &  5   :         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-22 17:55+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 Tipul MIME al fișierului nu poate fi determinat Nu au fost găsite toate funcțiile necesare Furnizorul nu a putut fi găsit cu destinația specificată Eroare la executarea scriptului Cale nevalidă pentru furnizorul cerut Fișierul ales nu a putut fi citit Serviciul nu a fost disponibil Eroare necunoscută Trebuie să specificați un URL pentru acest serviciu 