��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %  %        -     M     `     {     �     �     �  	   �  	   �     �     �  ?   �     "     @  
   [     f     r       )   �  *   �  *   �  +        0     M                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-22 21:08+0200
Last-Translator: Cristian Oneț <onet.cristian@gmail.com>
Language-Team: Romanian <kde-i18n-ro@lists.kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.1
 %1 fișier %1 fișiere %1 de fișiere %1 dosar %1 dosare %1 de dosare procesat %1 din %2 procesat %1 din %2 la %3/s procesat %1 procesat %1 la %2/s Aspect Comportament Renunță Curăță Configurare Sarcini încheiate Listă de transferuri de fișiere și alte sarcini în derulare Mută-le către altă listă. Mută-le în altă listă. Întrerupe Elimină-le Elimină-le. Reia Afișează toate sarcinile într-o listă Afișează toate sarcinile într-o listă. Afișează toate sarcinile într-un arbore Afișează toate sarcinile într-un arbore. Afișează ferestre separate Afișează ferestre separate. 