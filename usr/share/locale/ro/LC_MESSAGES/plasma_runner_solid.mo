��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     u     �  =   �  U   �  O   0  O   �  S   �  O   $	  S   t	     �	  
   �	     �	  
   �	  	   �	     
     
     
     8
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-07 23:16+0200
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 Blochează containerul Scoate suportul Găsește dispozitive a căror denumire se potrivește cu :q: Enumeră toate dispozitivele și permite montarea, demontarea sau scoaterea acestora. Enumeră toate dispozitivele care pot fi scoase și permite scoaterea acestora. Enumeră toate dispozitivele care pot fi montate și permite montarea acestora. Enumeră toate dispozitivele care pot fi demontate și permite demontarea acestora. Enumeră toate dispozitivele care pot fi blocate și permite blocarea acestora. Enumeră toate dispozitivele care pot fi deblocate și permite deblocarea acestora. Montează dispozitivul dispozitiv scoate blochează montează deblochează demontează Deblochează containerul Demontează dispozitivul 