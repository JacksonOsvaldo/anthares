��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �      l   �     )  	   <     F     Y  
   b     m     u     }     �     �     �     �     �     	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: krunner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-06-06 14:37+0300
Last-Translator: Sergiu Bivol <sergiu.bivol@jurnaltv.md>
Language-Team: Română <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 Întunecă ecranul pe jumătate Întunecă ecranul complet Enumeră opțiunile de luminozitate ale ecranului sau îl stabilește la luminozitatea definită de :q:; de ex. luminozitate ecran 50 ar întuneca ecranul la 50% din luminozitatea maximă Enumeră opțiunile de suspendare (de ex. adormire, hibernare) ale sistemului și permite activarea acestora întunecă ecranul hibernare luminozitate ecran adormire suspendare pe disc în ram întunecă ecranul %1 luminozitate ecran %1 Stabilește luminozitatea la %1 Suspendă pe disc Suspendă în RAM Suspendă sistemul în RAM Suspendă sistemul pe disc 