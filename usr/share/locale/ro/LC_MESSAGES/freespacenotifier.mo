��          T      �       �      �      �   '   �   5     D   :       �  �     e  0   j  2   �  A   �       1                                            MiB Enable low disk space warning Is the free space notification enabled. Minimum free space before user starts being notified. The settings dialog main page name, as in 'general settings'General Warn when free space is below: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-06-24 23:15+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Română <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
  MiO Activează avertizarea insuficienței ed spațiu Dacă este activată notificarea de spațiu liber. Spațiul liber minim, înainte ca utilizatorul să fie notificat. General Avertizează când spațiu liber e mai puțin de: 