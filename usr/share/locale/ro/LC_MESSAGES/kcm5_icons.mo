��    :      �  O   �      �     �                    ,     3  �  N  �   )  -   �  )   �  -   	  +   ;	  r   g	  	   �	  	   �	     �	     
     
     
  
   $
     /
     ;
     C
     K
      b
     �
     �
     �
      �
     �
     �
  r   �
  5   ]     �     �     �  	   �     �     �     �  (   �                9     S     n     t  +   �  3   �     �     �     �     �  S     )   _     �  �   �  �  s     `     l     t     �  
   �     �  6  �  �   �  .   �     �  
   �     �  y   �     e     n     �  	   �     �  
   �  
   �  	   �     �  	   �  &   �               "     '  "   4  
   W  "   b  �   �  1        >     Y     u     z     �     �     �  $   �     �  %   �  (     *   8     c     l  ?   |  9   �     �     �            ^   '  /   �     �  �   �               2                '   (          1      !            8                                       .               5   	   6   $      )       7   4          *                 0   
             :         ,                    +      %      "   -   /   #       9           &      3    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2013-02-09 14:12+0200
Last-Translator: Cristian Oneț <onet.cristian@gmail.com>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
claudiuc@kde.org
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;

 C&antitate: &Efect: A &doua culoare: &Semitransparent &Tematică (c) 2000-2003 Geert Jansen <h1>Pictograme</h1> Acest modul vă permite să alegeți pictogramele pentru mediul dumneavoastră KDE.<p>Pentru a alege o tematică de pictograme, dați clic pe numele ei și activați-o prin apăsarea butonului „Aplică” de mai jos. Dacă nu doriți să aplicați modificările apăsați butonul „Resetează” pentru a elimina modificările.</p><p>Dacă apăsați butonul „Instalare fișier cu tematică” puteți instala noua tematică scriind locația ei în dialog sau răsfoind la locația ei. Apăsați "OK" pentru a termina instalarea.</p><p>Butonul „Șterge tematica” va fi activat numai dacă selectați o tematică instalată cu acest modul de configurare. De aici nu puteți șterge tematici instalate global.</p><p>Tot aici puteți specifica și efectele care să fie aplicate pictogramelor.</p> <qt>Doriți într-adevăr să ștergeți tematica de pictograme <strong>%1</strong>?<br /><br />Această operație va șterge fișierele instalate de această tematică.</qt> <qt>Instalez tematica <strong>%1</strong></qt> Activ Dezactivat Implicit A apărut o problemă în timpul procesului de instalare. Totuși, majoritatea tematicilor din arhivă au fost instalate. A&vansat Toate pictogramele Antonio Larrosa Jimenez &Culoare: Colorare Confirmare Desaturare Descriere Birou Dialoguri Trageți sau scrieți URL-ul tematicii ,sergiu@ase.md Parametri efect Gama Geert Jansen Preluare tematici noi din Internet Pictograme Modul de control pentru pictograme Dacă dispuneți deja de o arhivă cu tematici locală, acest buton o va despacheta și o va face disponibilă pentru aplicațiile KDE Instalează arhivă cu tematică deținută local Bara de unelte principală Claudiu Costin,Sergiu Bivol Nume Fără efect Panou Previzualizare Șterge tematica Elimină tematica aleasă de pe disc Stabilește efectul... Configurare efect pictogramă activă Configurare efect pictogramă implicită Configurare efect pictogramă dezactivată Mărime: Pictograme mici Fișierul nu este o arhivă validă de tematică de pictograme. Această acțiune va elimina tematica aleasă de pe disc. În gri În monocrom Bara de unelte Torsten Rahn Nu am putut transfera arhiva tematicii de pictograme.
Verificați că adresa %1 este corectă. Nu am găsit arhiva tematicii de pictograme %1. Utilizarea pictogramei Trebuie să fiți conectat la Internet pentru a utiliza această acțiune. Un dialog va afișa o listă cu tematici de pe situl http://www.kde.org. Făcând clic pe butonul Instalează asociat unei tematici, aceasta se va instala local. 