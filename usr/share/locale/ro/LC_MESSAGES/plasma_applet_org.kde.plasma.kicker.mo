��    3      �  G   L      h     i     x     �     �     �     �     �  
   �     �            	   '     1     E  	   Z     d     |     �     �     �     �     �     �     �  	        !     )     :     ?     F  	   Y  
   c     n  
   ~     �     �     �     �     �     �  	   �     �               #     <     Q     g     o     ~  �  �     U	     f	  2   z	     �	     �	  
   �	     �	  	   �	     �	     �	     
  	   
     
     6
     P
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  	   �
  
             %     -     5     F     R     _     p     ~     �     �     �     �     �  	   �     �               '     C     ]  
   x     �     �                       *          )       /      0          !          
   .   +            (                3                               -   %       "   &               '   $   2   #                                   	                           1   ,    Add to Desktop Add to Favorites Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Behavior Categories Contacts Description (Name) Description only Documents Edit Application... Edit Applications... Favorites Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Logout Name (Description) Name only Open with: Power / Session Properties Recent Applications Recent Contacts Recent Documents Remove from Favorites Save Session Search Search... Searching for '%1' Session Show applications as: Show recent applications Show recent contacts Show recent documents Suspend System actions Type to search. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2013-10-13 13:09+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 Adaugă la birou Adaugă la favorite Aliniază rezultatele căutării în partea de jos Toate aplicațiile %1 (%2) Aplicații Comportament Categorii Contacte Descriere (nume) Doar descrierea Documente Editează aplicația... Editează aplicațiile... Favorite Uită toate aplicațiile Uită toate contactele Uită toate documentele Uită aplicația Uită contactul Uită documentul Uită documentele recente General %1 (%2) Hibernare Ascunde %1 Ascunde aplicația Blocare Ieșire Nume (descriere) Doar numele Deschide cu: Oprire / Sesiune Proprietăți Aplicații recente Contacte recente Documente recente Elimină din favorite Salvează sesiunea Caută Caută... Se caută după „%1” Sesiune Arată aplicațiile ca: Arată aplicațiile recente Arată contactele recente Arată documentele recente Suspendare Acțiuni de sistem Tastați pentru a căuta. 