��    [      �     �      �  .   �  9   �     2  )   L     v     �     �     �     �     �     	  7   	  5   O	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     
     1
     E
     S
     f
     y
     �
     �
     �
     �
     �
     �
               '  !   8     Z     l     z     �     �     �     �     �     �     �          "     6     G     [     k     |     �     �     �     �     �     �  	   �     	          *  	   8     B     \     v          �  G   �  +   �  -   $  4   R  1   �  Z   �  1     4   F  '   {     �  4   �     �                    9  4   M  
   �     �  �  �  >   r     �  
   �     �     �                    (     4     A  ?   O  A   �  
   �  
   �  !   �     	       	        %  
   .     9     B     R  	   d     n     u     ~     �  	   �     �  	   �     �     �     �  	   �     �  !   �          #     A  	   J     T     \  
   h     s  
   y     �  	   �     �     �     �     �     �     �     �     �               (     @     P     Y     k     �     �     �     �     �     �       9   '     a  %     )   �  (   �  M   �  -   F  '   t     �     �  +   �               )     6     S  ;   m     �     �     J                      &      ;   [   T          8   /   P            .             V   W      ?   >       F       +      E   R   $                  !   
   3       -      K   @   5                 H       7       I           '   ,       %   Y   S          G       1       C   <   Z       4       9      A   L   :      D       Q          2       "         U   	      (              6      X      0              #       *      B   =   N         M   O       )                    '%1' was changed and suggests to be restarted. @action Checks the Internet for updatesCheck for Updates @info app size%1 on disk @info app size%1 to download, %2 on disk @info:statusChanging Addons @info:statusDone @info:statusDownloading @info:statusInstalling @info:statusRemoving @info:statusStarting @info:statusWaiting A change by '%1' suggests your session to be restarted. A change by '%1' suggests your system to be rebooted. Available Broken Cannot cancel transaction Category3D CategoryAccessibility CategoryAccessories CategoryArcade CategoryAstronomy CategoryBiology CategoryBoard Games CategoryCard Games CategoryChat CategoryChemistry CategoryDebugging CategoryDeveloper Tools CategoryDrawing CategoryEducation CategoryElectronics CategoryEngineering CategoryFile Sharing CategoryFonts CategoryGames CategoryGeography CategoryGeology CategoryGraphic Interface Design CategoryGraphics CategoryIDEs CategoryInternet CategoryLocalization CategoryMail CategoryMathematics CategoryMultimedia CategoryOffice CategoryPhotography CategoryPhysics CategoryProfiling CategoryPublishing CategoryPuzzles CategorySimulation CategorySports CategoryViewers CategoryWeb Browsers CategoryWeb Development Checking signatures... Cleaning up... Committing... Copying files... Downloading... Installed Installing... No Cache available Processing... Remove... Resolving dependencies... Setting up transaction... Setup... Test committing... The transaction has finished! The transaction is currently checking the signatures of the packages... The transaction is currently cleaning up... The transaction is currently copying files... The transaction is currently downloading packages... The transaction is currently removing packages... The transaction is currently resolving the dependencies of the packages it will install... The transaction is currently updating packages... The transaction is currently waiting for the lock... The transaction is currently working... The transaction was canceled The transactions is currently installing packages... Unknown Status Updating... Upgradeable Waiting for authorization... Waiting for lock... Waiting for the user to authorize the transaction... Waiting... We are waiting for something. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-08 05:55+0100
PO-Revision-Date: 2014-05-02 12:03+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 „%1” a fost modificată și sugerează să fie repornită. Caută actualizări %1 pe disc %1 de descărcat, %2 pe disc Se modifică suplimentele Gata Se descarcă Se instalează Se elimină Se pornește Se așteaptă O modificare adusă de „%1” sugerează repornirea sesiunii. O modificare adusă de „%1” sugerează repornirea sistemului. Disponibil Deteriorat Tranzacția nu poate fi anulată! 3D Accesibilitate Accesorii Aventuri Astronomie Biologie Jocuri de table Jocuri de cărți Discuție Chimie Depanare Unelte pentru dezvoltatori Desen Educație Electronică Inginerie Partajare de fișiere Fonturi Jocuri Geografie Geologie Proiectarea interfețelor grafice Grafică Medii integrate de dezvoltare Internet Traducere Poștă Matematică Multimedia Birou Fotografie Fizică Profilare Publicistică Puzzle Simulare Sporturi Vizualizare Navigatoare de Internet Dezvoltare web Se verifică semnături... Se curăță... Se comite... Se copiază fișiere... Se descarcă... Instalat Se instalează... Nicio prestocare disponibilă Se prelucrează... Elimină... Se rezolvă dependențe... Se configurează tranzacția... Configurare... Se testează comiterea... Tranzacția s-a încheiat! Tranzacția verifică momentan semnăturile pachetelor... Tranzacția curăță acum... Tranzacția copiază fișiere acum... Tranzacția descarcă pachete momentan... Tranzacția elimină pachete momentan... Tranzacția rezolvă momentan dependențele pachetelor ce vor fi instalate... Tranzacția actualizează pachete momentan... Tranzacția așteaptă blocajul acum... Tranzacția lucrează acum... Tranzacția a fost anulată Tranzacția instalează pachete momentan... Stare necunoscută Se actualizează... Actualizabil Se așteaptă autorizarea... Se așteaptă blocajul... Se așteaptă ca utilizatorul să autorizeze tranzacția... Se așteaptă... Se așteaptă ceva. 