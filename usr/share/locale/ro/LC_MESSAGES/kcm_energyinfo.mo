��    3      �  G   L      h     i     k     �     �     �     �     �     �     �     �     �                      9     @     S     q     }     �     �     �     �     �     �     �  	   �     �               +     .     ;     C     L     Y     a     o     �     �  @   �     �     �     �                    1     ?     C  �  Z     !	     #	     )	  "   -	  
   P	  
   [	     f	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	     

     
     "
     ;
     K
     [
     j
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                    &  @   3     t  )   �  
   �     �     �     �     �     �     �            !          -      	   %                    /   $   1   (                                             )   
   *                                  "             &   0   .      +      3              #                 2   '            ,               % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) Watt-hoursWh Yes literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-20 14:27+0300
Last-Translator: Sergiu Bivol <sergiu@cip.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Lokalize 1.5
 % %1 %2 %1: Consum de energie al aplicațiilor Acumulator Capacitate Procentaj încărcare Starea încărcării Se încarcă Curent °C Detalii: %1 Se descarcă sergiu@cip.md Energie Consum de energie Statistici de consum a energiei Mediu Încărcat deplin Are sursă de alimentare Kai Uwe Broulik Ultimele 12 ore Ultimele 2 ore Ultimele 24 de ore Ultimele 48 de ore Ultimele 7 zile Ultima oră Producător Model Sergiu Bivol Nu Nu se încarcă PID: %1 Cale: %1 Reîncărcabil Împrospătare Număr de serie W Sistem Temperatură Acest tip de istoric nu este disponibil pentru acest dispozitiv. Interval de timp Interval de timp pentru afișarea datelor Vânzător V Voltaj Treziri pe secundă: %1 (%2%) Wh Da % 