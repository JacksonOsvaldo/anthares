��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     �     �  H   �     �  
   �  	   �            	        %     8     D     V     ]  5   t     �     �                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2013-10-13 12:54+0300
Last-Translator: Sergiu Bivol <sergiu@ase.md>
Language-Team: Romanian <kde-i18n-ro@kde.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
 &Răsfoiește... C&aută: *.png *.xpm *.svg *.svgz|Fișiere pictogramă (*.png *.xpm *.svg *.svgz) Acțiuni Aplicații Categorii Dispozitive Embleme Emoticoni Sursă pictogramă Tipuri MIME &Alte pictograme: Locuri Pictograme de &sistem: Caută interactiv nume de pictograme (de ex.: dosar). Selectare pictogramă Stare 