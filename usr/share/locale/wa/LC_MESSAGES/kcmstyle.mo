��             +         �  &   �  �   �     w     �     �     �     �     �     �     �     �      �  	        (     9     E     d     l     �     �     �     �     �     �     �     �  	   �  C   �     9     B     X  �  f  %     �   +  
   �     �     �     �     �               !     2  A   D     �     �     �  ,   �     �     �     
	     	     $	  %   0	  	   V	  	   `	     j	     �	     �	  Q   �	     
  0   
     C
                                 
                                                                                	                               (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Preview Radio button Ralf Nolden Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. Toolbars Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2011-07-17 16:00+0200
Last-Translator: Jean Cayron <jean.cayron@base.be>
Language-Team: Walloon <linux@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 © 2002 Karol Szwed, Daniel Molkentin <h1>Stîle</h1>Ci module ci vs permet d' candjî l' rivnance des elemints di l' eterface di l' uzeu, come li stîle des ahesses eyet ls efets. &Programes &Fén apontiaedje Boton Boesse a clitchî Djivêye menu disrôlant A&pontyî... Apontyî %1 Daniel Molkentin Discrijhaedje: %1 laurent.hendschel@skynet.be,pablo@walon.org,jean.cayron@gmail.com Boesse di groupe Module do stîle di KDE Karol Szwed Lorint Hendschel,Pablo Saratxaga,Djan Cayron Nou scrijhaedje Nou discrijhaedje Prévoeyaedje Boton radio Ralf Nolden Håyner les imådjetes ezès menus : Linwete 1 Linwete 2 Tecse å dzo des imådjetes Tecse adlé ls imådjetes Rén kel tecse Åk n' a nén stî come dji tcherdjive li purnea d' apontiaedje po ci stîle ci. Bår ås usteyes Dji n' a nén savou tcherdjî l' purnea di dvize Stîle des ahesses: 