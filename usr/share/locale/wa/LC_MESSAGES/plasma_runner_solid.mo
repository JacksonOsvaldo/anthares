��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     H     [  6   n  ^   �  \     Z   a  `   �  e   	  l   �	     �	     
  	   

     
     
  
   #
  	   .
     8
     N
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-14 09:10+0200
Last-Translator: Jean Cayron <jean.cayron@base.be>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Eclawer l' contneu Fé rexhe li media Trove des éndjins k' ont leu no ki corespondèt a :q: Fwait l' djivêye di tos les éndjins eyet lzi permete d' esse montés, dismontés ou rexhous. Fwait l' djivêye di tos les éndjins k' ont sait fé rexhe eyet lzi permete d' esse rehous. Fwait l' djivêye di tos les éndjins k' ont sait monter eyet lzi permete d' esse montés. Fwait l' djivêye di tos les éndjins k' ont sait dismonter eyet lzi permete d' esse dismontés. Fwait l' djivêye di tos les éndjins eclawés k' ont sait eclawer eyet lzi permete d' esse eclawés. Fwait l' djivêye di tos les éndjins ecriptés k' ont sait dizeclawer eyet lzi permete d' esse dizeclawés. Monter l' éndjin éndjin fé rexhe eclawer monter dizeclawer dismonter Dizeclawer l' contneu Dismonter l' éndjin 