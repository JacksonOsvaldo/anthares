��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  V   �  {   �  +   T  �   �  #        +  C   ;       H   �     �     �     �        9     @   X  B   �  %   �  
     [        i     �  �   �     !  7   7     o     �     �     �     �     �     �     �       *         K     Z     _     k     �     �     �     �     �     �     �     �          "  	   6     @  $   O     t     �     �     �  �   �     c  O   v  �   �     |  K   �  H   �  J   '  (   r     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-07-04 14:48+0200
Last-Translator: Jean Cayron <jean.cayron@base.be>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Po mete en ouve li candjmint d' bouye di fond, vos vs duvroz elodjî eyet s' relodjî. Ene djivêye des bouyes di fond Phonon trovêyes so vosse sistinme. L' ôre cial dit ké est l' ôre ki Phonon ls eployrè. Mete en ouve l' djivêye des éndjins po... Mete en ouve li djivêye des preferinces di l' éndjin mostrêye pol moumint po ls ôtès categoreyes di djouwaedje odio ki shuvèt : Apontiaedje di l' éndjolreye odio Djouwaedje odio Prémetowès preferinces di djouwaedje odio pol categoreye « %1 » Eredjistraedje odio Prémetowès preferinces d' eredjistraedje odio pol categoreye « %1 » Bouye di fond Colin Guthrie Raloyeu Copyright 2006 Matthias Kretz Prémetowès preferinces di l' éndjin å djouwaedje odio Prémetowès preferinces di l' éndjin a l' eredjistraedje odio Prémetowès preferinces di l' éndjin a l' eredjistraedje videyo Prémetowe/nén specifieye categoreye Moens rade Defini l' prémetou ôre des éndjins ki pout esse candjî pas des mierseulès categoreyes. Apontiaedje di l' éndjin Preferince di l' éndjin Éndjins trovés so vosse sistinme, bon pol categoreye di tchoezeye. Tchoezixhoz l' éndjin k' vos vloz k' les programes eployexhe. jean.cayron@gmail.com Li tchoes di l' éndjin odio d' rexhowe a fwait berwete Pa dvant å mitan Pa dvant a hintche Pa dvant a hintche då mitan Pa dvant a droete Pa dvant a droete då mitan Éndjolreye Éndjins dislaxhîs Liveas d' intrêye Nén valåbe Apontiaedje di l' éndjolreye odio po KDE Matthias Kretz Mono Djan Cayron Module d' apontiaedje di Phonon Djouwaedje (%1) Pus rade Profil Pa drî å mitan Pa drî a hintche Pa drî a droete Eredjistraedje (%1) Mostrer spepieus éndjins Sol costé a hintche Sol costé a droete Cåte son Éndjin d' son Plaece do hôt-pårloe eyet sayaedje Basse di pa dzo (subwoofer) Sayî Sayî l' éndjin tchoezi Dji saye %1 L' ôre dite li preferince inte les éndjins. Si po ene råjhon, li prumî éndjin n' sait nén esse eployî, Phonon sayrè d' si siervi do deujhinme et vos nd åroz. Canå nén cnoxhou Si siervi del djivêye des éndjins mostrés pol moumint po pus di categoreyes. Sacwantès categoreyes di façon d' si siervi des medias. Po tchaeke categoreye, vos savoz tchoezi kén éndjin vos eployrîz pus voltî dins les programes ki s' siervèt d' Phonon. Eredjistraedje videyo Prémetowès preferinces d' eredjistraedje videyo pol categoreye « %1 »  Vosse bouye di fond pôreut n' nén saveur fé d' l' eredjistraedje odio Vosse bouye di fond pôreut n' nén saveur fé d' l' eredjistraedje videyo pont d' preferince po l' éndjin tchoezi pus rade l' éndjin tchoezi 