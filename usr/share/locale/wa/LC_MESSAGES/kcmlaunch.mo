��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  '   �      �    �   �	     �
     �
     �
  /   �
          ,  '   I     q                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-09-09 22:55+0200
Last-Translator: Pablo Saratxaga <pablo@walon.org>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  seg &Tårdjaedje po mostrer l' enondaedje: <H1>Notifiaedje el bår des bouyes</H1>
Vos ploz defini ene deujhinme metôde di notifyî k' on programe
a stî enondé, k' est eployeye pal bår des bouyes, wice k' aparexhrè
ene ôrlodje ki toûne, mostrant insi ki vosse programe est ki s' tchedje.
  <h1>Cursoe a l' ovraedje</h1>
KDE a-st on cursoe a l' ovraedje pol notifiaedje k' on programe est enondé.
Po mete en alaedje li cursoe a l' ovraedje, tchoezixhoz ene sôre di rtoû
vizuwel dins l' djivêye disrôlante.
I s' pout a feyes k' on programe ni cnoxhèt nén ci sistinme di notifiaedje.
Dins ç' cas, li cursoe s' arestêye di clignter après li tins metou dins
l' tchuze «Tårdjaedje po mostrer l' enondaedje». <h1>Ritoû d' enondaedje</h1> Vos ploz apontyî chal li rtoû d' enondaedje des programes; dj' ô bén li manire ki les programes mostrer k' i sont ki s' enondèt mins k' ça pout prinde ene miete di tins. Cursoe clignotant Sôtlant cursoe &Cursoe a l' ovraedje &Mete en ouve li notifiaedje el bår des bouyes Pont d' cursoe a l' ovraedje Cursoe a l' ovraedje passif Tår&djaedje po mostrer l' enondaedje: &Notifiaedje el bår des bouyes 