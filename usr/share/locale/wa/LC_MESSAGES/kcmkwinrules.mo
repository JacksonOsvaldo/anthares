��    g      T  �   �      �  
   �     �     �     �     �     	  
   	     &	  	   .	     8	     I	  "   O	     r	     �	     �	     �	     �	  	   �	     �	     �	     �	     
     
     '
     0
  +   7
     c
     k
     r
     z
     �
     �
      �
     �
     �
     �
     �
  O   �
     M     Y     a     g     y     ~  !   �      �     �     �     �                      
   +     6     D     M     g       
   �  
   �     �     �     �     �     �     �                    ,  -   5     c     i     y     �  &   �     �     �     �     �     �     �  :        =     D     L     \     j     p     |  *   �     �     �  .   �  B   �     B     O     ^     u  -   �    �  �  �  	   a     k     w     �     �     �     �  	   �     �     �       (        ?     P     `     s     �     �  "   �     �  %   �     %  	   -     7     @  +   H  	   t     ~  
   �     �     �     �     �     �     �  '   �  
     r   )     �     �     �     �     �  (   �       "   '     J     O     i     z     �     �     �     �     �     �  *   �                5  
   B     M     U     a     t     }     �     �     �     �  	   �  /   �     
            	   +  -   5     c     ~     �     �     �     �  O   �     1     7     I     b     x          �  0   �     �     �  .   �  <   %     b     s     �     �  7   �  ;  �     4       \   B   D      <   O       =          !   9           `          3           ^   S   T   a              5   .   )   W   8   N          c   L   I                  P      F       A   "          6   ?          _          >           K   7      U             M   Q   *   1   +      2   -             g                 ;   ,   f      	   G           e   &   ]          Z   $   '       R      E   /       @      C   #       d       
             %   X          H      :      V   J   b   [          0   Y   (        &Closeable &Desktop &Detect Window Properties &Focus stealing prevention &Fullscreen &Machine (hostname): &Modify... &New... &Position &Single Shortcut &Size (c) 2004 KWin and KControl Authors 0123456789-+,xX: Accept &focus All Desktops Application settings for %1 Apply Initially Apply Now Autog&roup in foreground Autogroup by I&D Autogroup with &identical C&lear Cascade Centered Class: Consult the documentation for more details. Default Delete Desktop Dialog Window Do Not Affect Dock (panel) EMAIL OF TRANSLATORSYour emails Edit Edit Shortcut Edit Window-Specific Settings Edit... Enable this checkbox to alter this window property for the specified window(s). Exact Match Extreme Force Force Temporarily High Ignore requested &geometry Information About Selected Window Internal setting for remembering KWin KWin helper utility Keep &above Keep &below Low Lubos Lunak M&aximum size M&inimized M&inimum size Machine: Match w&hole window class Maximized &horizontally Maximized &vertically Maximizing Move &Down Move &Up NAME OF TRANSLATORSYour names No Placement Normal Normal Window On Main Window Override Type Random Regular Expression Remember Remember settings separately for every window Role: Settings for %1 Sh&aded Shortcut Show internal settings for remembering Skip &taskbar Skip pa&ger Smart Splash Screen Standalone Menubar Substring Match This helper utility is not supposed to be called directly. Title: Toolbar Top-Left Corner Torn-Off Menu Type: Under Mouse Unimportant Unknown - will be treated as Normal Window Unnamed entry Utility Window WId of the window for special window settings. Whether the settings should affect all windows of the application. Window &type Window &types: Window settings for %1 Window t&itle: Window-Specific Settings Configuration Module You have specified the window class as unimportant.
This means the settings will possibly apply to windows from all applications. If you really want to create a generic setting, it is recommended you at least limit the window types to avoid special window types. Project-Id-Version: kcmkwinrules
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2009-12-26 16:19+0100
Last-Translator: Jean Cayron <jean.cayron@gmail.com>
Language-Team: Walloon <linux-wa@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 &Cloyåve &Sicribanne &Detecter prôpietés do purnea Hapaedje di &focusse Tote li &waitroûle É&ndjin (no d' lodjoe): &Candjî... &Novea... &Eplaeçmint &Simpe rascourti &Grandeu © 2004 Les oteurs di KWin eyet KControl 0123456789-+,xX: Accepter &focus Tos les scribannes Tchuzes di programe po %1 Mete en ouve å cminçmint Mete en ouve asteure G&rouper otomaticmint dins l' fond Grouper otomaticmint so l' &ID Grouper otomaticmint avou les &minmes Net&yî Riguilite Å mitan Classe: Léjhoz l' documintåcion po pus di detays. Prémetou Disfacer Sicribanne Purnea d' divize En nén riwaitî Dock (sicriftôr) jean.cayron@gmail.com Candjî Candjî rascourti Aspougnî tchuzes sipecifikes å purnea Candjî... Mete en alaedje cisse boesse a clitchî po candjî cisse prôpieté do purnea po li/les purnea(s) di specifyî(s). Corespond totafwaitmint Estrinme Foirci Foirci po kék tins Hôte Passer houte li d&jeyometreye dimandêye Informåcion sol purnea tchoezi Divintrinnès tchuzes po rimimbrer KWin Usteye d' aidance di KWin Wårder pa dz&eu Wårder pa dz&o Basse Lubos Lunak Grandeu m&acsimom Å pus &ptit Grandeu m&inimom Éndjin: Fé coresponde a t&ote li classe do purnea Å pus grand di &coûtchî Å pus grand d' &astampé Å pus grand &Dischinde &Monter Djan Cayron Pont d' plaeçmint Normåle Purnea normå Sol mwaisse purnea Sipotchî sôre Pa accidint Erîlêye ratourneure Rimimbrer Rimimbrer les tchuzes a pårt po tchaeke purnea Role: Tchuzes po %1 A l' &ombion Rascourti Mostrer les dvintrinnès tchuzes po rimimbrer Passer li båre di &bouyes Passer pa&ger Malén Waitroûle splash Båre di dressêye tote seule Corespond å boket d' tchinne Çou n' est nén çou k' est préveyou d' apeler direk ciste usteye d' aidance. Tite: Båre ås usteyes Coine del copete hintche Disrîlêye dressêye Sôre: Pa dzo l' sori Nén impôrtant Nén cnoxhou - serè riwaitî come Normå purnea Intrêye nén lomêye Purnea d' usteye Wld do purnea po speciålès tchuzes di purnea Si les tchuzes duvrént aler po tos les purneas do programe. &Sôre di purnea &Sôres di purnea: Tchuzes di purnea po %1 &Tite do purnea: Module d' apontiaedje des tchuzes sipecifikes å purnea Vos avoz specifyî kel classe do purnea n' est nén impôrtante.
Ça vout dire ki les apontiaedjes vont pleur esse metous en ouve po tos les programes. Si vs voloz vormint fé èn apontiaedje djenerike, il est rcomandé di, pol moens, limiter les sôres di purneas po n' nén aveur des sôres speciåles di purneas. 