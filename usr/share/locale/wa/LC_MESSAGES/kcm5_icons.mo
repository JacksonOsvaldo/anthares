��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     '     3     :     M     b     i  �   �  /   &  
   V     a  	   j  h   t  
   �     �     �       
        )  
   7     B  
   P     [  ;   l  A   �     �     �       $     
   5  1   @  �   r  D   �     @  ,   M     z     }  
   �  
   �     �  &   �     �  +   �  (     +   4     `     i  E   }  2   �     �     	          0  �   =  G   �         #               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2011-07-17 15:38+0200
Last-Translator: Jean Cayron <jean.cayron@base.be>
Language-Team: Walloon <linux@walon.org>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 C&wantité: &Efet: &Deujhinme coleur: Transparint a &mitan &Tinme © 2000-2003 Geert Jansen <qt>Estoz vs seur di voleur oister li tinme d' imådjetes <strong>%1</strong>?<br /><br />Çoula disfaçrè tos les fitchîs k' on stî astalés på tinme.</qt> <qt>Astalant l' tinme <strong>%1</strong></qt> En alaedje Essocté Prémetou Åk n' a nén stî tot astalant. Mins nerén, li plupårt des tinmes di l' årtchive ont stî astalés &Sipepieus Totes les imådjetes Antonio Larrosa Jimenez Co&leur: Colorijhî Racertinaedje Dissaturer Discrijhaedje Sicribanne Purneas di dvize Bodjîz et s' saetchîz ou tapez li hårdêye d' on tinme laurent.hendschel@skynet.be,pablo@walon.org,jean.cayron@gmail.com Parametes des efets Gama Geert Jansen Novea tinmes a pårti del daegntoele Imådjetes Module d' imådjetes di l' Aisse di manaedjmint Si vs avoz ddja locålmint ene årtchive di tinme di xhinêyes, ci boton l' disrôlrè eyet l' rindrè disponibe po les programes di KDE Astaler on fitchî d' årtchive di tinme ki vs avoz dedja locålmint Mwaisse bår Lorint Hendschel,Pablo Saratxaga,Djan Cayron No Nol efet Sicriftôr Vey divant Oister tinme Oister l' tinme tchoezi di vosse plake Defini efet... Defini l' efet di l' imådjete en alaedje Defini l' efet del prémetowe imådjete Defini l' efet di l' essoctêye imådjete Grandeu: Pititès imådjetes Li fitchî n' est nén ene årtchive di tinme d' imådjetes valide. Çouci oistêyrè l' tinme tchoezi di vosse plake. Viè grijhe coleur Viè ene seule coleur Bår ås usteyes Torsten Rahn Dji n' a nén savou aberweter l' årtchive di tinme d' imådjetes;
Verifyîz ki l' adresse %1 est bén coreke s' i vs plait. Dji n' a nén savou trover l' årtchive di tinme d' imådjetes %1 . Eployaedje di l' imådjete Vos dovz esse raloyî al daegntoele po fé cist accion. Ene divize håyenrè ene djivêye di tinmes del waibe http://www.kde.org. Vos pôroz astaler locålmint c' tinme so vosse copiutrece e clitchant sol boton Astaler k' est elaxhî avou l' tinme ki vs inmez. 