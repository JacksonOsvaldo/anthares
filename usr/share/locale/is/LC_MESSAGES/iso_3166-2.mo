��    M      �  g   �      �     �  
   �  
   �     �     �     �     �     �     �     �     �             	              ,     C  	   ]     g     w     �     �     �     �     �     �     �     �     �  
   �  
   
  
     
         +     9     B     S     f     y     ~     �  
   �     �     �  	   �     �     �  
   �  
   �  
   �     �     	  
   	  	   	     )	  
   B	     M	     a	     q	     z	     �	     �	     �	     �	  
   �	     �	  
   �	     �	     �	     
     "
     5
     M
     U
     g
     t
  +  �
  	   �     �  
   �     �     �     �     �     �  	                  +  
   8     C     O     `     w  
   �     �     �  
   �     �     �     �     �               (     4     E     R     _     l     y     �     �     �     �     �     �     �  
   �     �     �     �               '     3     ?     K     R  
   b  	   m     w     �     �     �  
   �     �     �                     ,     9  
   E     P     ^     n     �     �     �     �     �     �        4   L   6   ;   '   7           <       
             D       /          ?   @   1           #      (      5   M          E   )       :           3   0   C       B              I                      	             >   .            2   +         -   K      $      =   %                *         F              A          J   8           "       !      H   &         G   9   ,    Akershus Aust-Agder Austurland Berlin Blekinge län Dalarnas län East Eastern Finnmark Gotlands län Gävleborgs län Hallands län Hedmark Hordaland Hovedstaden Höfuðborgarsvæðið Jan Mayen (Arctic Region) Jerusalem Jämtlands län Jönköpings län Kalmar län Kronobergs län Madrid Midtjylland Møre og Romsdal Nord-Trøndelag Nordjylland Nordland Norrbottens län North East North Gaza North West North-East North-Western Northern Northern Ireland Norðurland eystra Norðurland vestra Oslo Paris Polynésie française Reykjavík Rogaland Roma Sjælland Skåne län Sogn og Fjordane South East South West South-East Southern Stockholms län Suðurland Suðurnes Svalbard (Arctic Region) Syddanmark Södermanlands län Sør-Trøndelag Telemark Terres australes françaises Thessaloniki Troms United Kingdom Uppsala län Vest-Agder Vestfirðir Vesturland Värmlands län Västerbottens län Västernorrlands län Västmanlands län Västra Götalands län Western Western Australia Örebro län Östergötlands län Project-Id-Version: iso_3166-2
Report-Msgid-Bugs-To: Debian iso-codes team <pkg-isocodes-devel@lists.alioth.debian.org>
POT-Creation-Date: 2017-04-25 21:36+0200
PO-Revision-Date: 2017-11-02 16:40+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Bugs: Report translation errors to the Language-Team address.
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Akershús Austur-Agder Austurland Berlín Blekingelén Dalalén Austur Austur Finnmörk Gotlandslén Gävleborgarlén Hallandslén Heiðmörk Hörðaland Höfuðstðurinn Höfuðborgarsvæðið Jan Mayen (heimsskautasvæði) Jerúsalem Jämtlandslén Jönköpingslén Kalmarlén Kronobergslén Madríd Mið-jótland Mæri og Romsdal Norður-Þrændalög Norður-jótland Norðurland Norðurbotnslén Norð-austur Norður-Gaza Norð-vestur Norð-austur Norð-vestur Norður Norður-Írland Norðurland eystra Norðurland vestra Osló París Franska pólýnesía Reykjavík Rogaland Róm Sjáland Skánarlén Sogn og Firðirnir Suð-austur Suð-vestur Suð-austur Suður Stokkhólmslén Suðurland Suðurnes Svalbarði (heimsskautasvæði) Suður-danmörk Suðurmanlandslén Suður-Þrændalög Þelamörk Frönsku suðurhafslandsvæðin Þessalóníka Troms Breska samveldið Uppsalalén Vestur-Agder Vestfirðir Vesturland Vermlandslén Vesturbotnslén Vesturnorðlandslén Vestmanlandslén Vesturgotlandslén Vestur Vestur-Ástralía Örebrolén Austurgotlandslén 