��          �            x     y     }     �     �     �  S   �  w   !  M   �     �  	   �     �          %     2     R  �  d     "     &      B     c     t  V   �  r   �  ?   K     �     �  #   �     �     �     �     �                                              	          
                        ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen Lock Screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2010-05-03 11:48+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;



  ms Töf á endu&rgangsetningu: &Skipta um skjáborð á jaðri: &Töf á virkni: Alltaf virkt Tíminn sem þarf eftir eina aðgerð áður en hægt er að gangsetja næstu aðgerð Til að gangsetja aðgerð er músarbendlinum haldið að skjájaðrinum þeim megin sem aðgerðin er skilgreind. Skipta um skjáborð þegar músarbendli er ýtt að skjábrún Læsa skjánum Engin aðgerð Aðeins meðan gluggar eru færðir Aðrar stillingar Sýna skjáborðið Óvirkt Gluggastjórnun 