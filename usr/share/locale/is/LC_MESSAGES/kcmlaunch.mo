��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �     �  �    _  �  b   
     k
     �
     �
  !   �
     �
     �
     �
                                               
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2004-02-26 22:13+0000
Last-Translator: Svanur Palsson <svanur@tern.is>
Language-Team: Icelandic <en@li.org>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


  sek Tímatakmark ræ&singar: <H1>Tilkynning á tækjaslá</H1>
Þú getur notað aðra aðferð til að láta vita af ræsingu og hún
notar tækjaslána með því að sýna takka með disk sem snýst.
Þetta táknar að forritið sem var beðið um er í ræsingu.
Hinsvegar getur verið að sum forrit viti ekki af þessum
möguleika. Í því tilviki hverfur takkinn eftir þann tíma
sem gefinn er upp í 'Tímatakmark ræsingar'. <h1>Biðbendill</h1>
KDE býður upp á biðbendil til að láta vita þegar verið er
að ræsa forrit.  Til að virkja hann, veldu þá eina tegund úr
fellivalmyndinni.
Það getur gerst að sum forrit viti ekki af ræsitilkynningunni.
Í þeim tilvikum hættir bendillinn að blikka eftir þann tíma sem
gefinn er upp í 'Tímatakmark ræsingar'. <h1>Ræsitilkynning</h1> Stillingar á því hvernig forrit láta þig vita þegar þau eru ræst. Blikkandi biðbendill Skoppandi biðbendill &Biðbendill Virkja &tilkynningu á tækjaslá Enginn biðbendill Rólegur biðbendill Tímatakmark ræ&singar: &Tilkynning á tækjaslá 