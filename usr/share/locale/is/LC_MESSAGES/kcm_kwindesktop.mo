��            )   �      �     �     �  
     
   "     -     9     R     `     r  	   {      �  �   �  *   D  H   o     �     �     �  	   �     �  (        =     U     k     �     �     �     �  	   �  �  �     �  �   �     J     V     d     s     �     �  
   �     �     �  �   �  +   �	  @   �	     �	     �	     	
     
  (   '
  0   P
     �
     �
     �
     �
     �
     �
          $                                                                                      	                                            
         msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switching Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2016-01-17 10:05+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;



  msek <h1>Fjöldi skjáborða</h1> Þessi eining gerir þér kleyft að stilla hverstu mörg sýndarskjáborð þú notar og hvernig þau eru merkt. Hreyfingar: Skjáborð %1 Skjáborð %1: Hreyfingaráhrif á skjáborði Nöfn skjáborða Skjáborðaskipting Skjáborð Tímalengd: sv1@fellsnet.is Virkjaðu þennan valkost ef þú vilt að færsla með lyklaborði eða mús út fyrir jaðrar skjáborðs færi þig á gagnstæða hlið næsta skjáborðs. Hér getur þú gefið skjáborði %1 heiti Hér getur þú stillt hversu mörg sýndarskjáborð KDE notar. Framsetning Sveinn í Felli Engar hreyfingar Flýtilyklar Sýna tákn fyrir skjáborðauppsetningu Birta flýtilykla fyrir öll möguleg skjáborð Eitt skjáborð niður Eitt skjáborð upp Eitt skjáborð til vinstri Eitt skjáborð til hægri Skipta á skjáborð %1 Á næsta skjáborð Á fyrra skjáborð Skipting 