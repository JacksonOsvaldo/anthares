��    H      \  a   �            !     .     =     J     V     q  	   z     �  
   �     �     �     �     �  	   �     �     �     �                     (  
   I     T  	   ]     g     o     �     �     �     �     �     �     �     �     �     �                    #     B     N     e     r     �  	   �  
   �     �     �     �     �     �     �     	     	     !	     4	     E	  $   M	     r	     �	     �	     �	     �	  !   
     '
  $   G
     l
  /   �
  $   �
     �
  �  �
     �     �     �     �     �     �                    #     ,     A     M     ]     j     }  	   �     �     �     �  9   �     �     �                    '     @     P     V     e     �     �     �  	   �     �  	   �     �     �  Q   �     L     g     �  
   �     �  
   �     �     �     �     �     �  !   	     +     2     8     K     Z     r     z     �     �     �     �     �          $     A      ]  3   ~  !   �     �     C       A       (         2   3          	   9      #                            1       8   /   6   )      5                    0            7          -       .   4           ,   B      :      H   &      >                  <       E       *   G   D          %   ;   "       +   $                =   @             !   
                     F      ?   '    &Background: &Beep on error &Foreground: &Functions: &Maximum number of digits: &Memory: &Numbers: Bitwise AND Bitwise OR Bitwise XOR Button & Display Colors Button Colors Change sign Clear all Clear data store Colors Cosine Decimal point Display Colors Division EMAIL OF TRANSLATORSYour emails Enter data Exponent Factorial General General Settings He&xadecimals: Hyperbolic mode KCalc KDE Calculator Last stat item erased Left bit shift Logarithm to base 10 Mean Median Memory recall Misc Modulo Multiplication NAME OF TRANSLATORSYour names Natural log Number of data entered O&perations: One's complement Percent Precision Reciprocal Result Right bit shift Select Display Font Set &decimal precision Show &result in window title Sine Square St&atistic functions: Standard deviation Stat mem cleared Tangent The background color of the display. The color of function buttons. The color of hex buttons. The color of memory buttons. The color of number buttons. The color of operation buttons. The color of statistical buttons. The font to use in the display. The foreground color of the display. Whether to beep on error. Whether to show the result in the window title. Whether to use fixed decimal places. x to the power of y Project-Id-Version: kcalc
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:17+0100
PO-Revision-Date: 2009-06-13 15:46+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


 &Bakgrunnslitur: &Flauta við villu &Forgrunnslitur: &Föll: &Hámarksfjöldi talna: &Minnishnappar: &Töluhnappar: Bita-OG Bita-OR Bita-XOR Hnappa og skjálitir Hnappalitir Breyta formerki Hreinsa allt Hreinsa gagnaminni Litir Kósínus Komma Leturgerð í útkomu Deiling bre@mmedia.is, ra@ra.is, gudjonh@hi.is, pjetur@pjetur.net Sláðu inn gögn Veldi Deiling Almennt Almennar stillingar Se&xtándagrunnshnappar: Gleiðbogahamur KCalc KDE Reiknivél Síðasta tölfræðigildi eytt Bita-hliðrun til vinstri Lógi með grunntöluna 10 Meðal Miðgildi Kalla úr minni Ýmislegt Módulus Margföldun Bjarni R. Einarsson, Richard Allen, Guðjón I. Guðjónsson, Pjetur G. Hjaltason Náttúrulegur lógarithmi Fjöldi innsleginna gagna &Aðgerðahnappar: Fyllimengi Prósent Nákvæmni Nálgun Niðurstaða Bita-hliðrun til hægri Leturgerð í útkomu &Festa nákvæmni Birta niðurstöður í titlrönd Sínus Veldi Tölfræði&föll: Staðalfrávik Tölfræði minni tæmt Tangent Bakgrunnslitur skjámyndar. Litur fallahnappanna. Litur hex hnappanna. Litur minnishnappanna. Litur talnahnappanna. Litur aðgarðahnappanna. Litur tölfræðihnappanna. Letur notað í skjámyndum. Forgrunnslitur skjámyndar. Hvort flauta skuli vegna villna. Hvort birta skuli niðurstöður í titilröndinni. Hvort nota skuli fasta aukastafi. x í veldinu y 