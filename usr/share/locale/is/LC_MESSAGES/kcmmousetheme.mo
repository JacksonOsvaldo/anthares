��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  m   d  j   �     =  $   J     o     {  '   �     �     �     �     �     �     	     	  3   &	  \   Z	  $   �	                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-05-30 22:53+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;



 (c) 2003-2007 Fredrik Höglund <qt>Ertu viss um að þú viljir fjarlægja <strong>%1</strong> bendilþemuna? <br />Þetta mun eyða út öllum skrám sem þetta þema setti inn.</qt> <qt>Þú getur ekki eytt þemu sem þú ert að nota.<br />Þú verður fyrst að skipta í aðra þemu.</qt> Þema nefnt %1 er til staðar í möppu fyrir táknmyndaþema. Viltu setja þetta í staðinn fyrir það? Staðfesting Bendilstillingum hefur verið breytt Bendilþema Lýsing Dragðu eða sláðu inn slóð þemans leosson@frisurf.no Fredrik Höglund Flytja inn litastef af netinu Arnar Leósson Heiti Skrifa yfir þema? Fjarlægja þema Skráin %1 virðist ekki vera gilt bendilþemasafn. Gat ekki hlaðið niður bendilþemasafni. Vinsamlegast athugið hvort slóðin %1 er rétt. Gat ekki fundið bendilþemasafn %1. 