��    w      �  �   �      
     
     %
     -
     ;
     D
  
   L
     W
     Y
     `
     n
     �
     �
     �
     �
     �
     �
  
   �
     �
  *   �
          *     9     F     N      W     x  "   �     �     �  
   �     �  !   �     	               &     .     5     :  !   K  ?   m  �   �      6     W     \     i     q     �     �     �  /   �  3   �          -     K  	   T     ^     n     w     �  	   �     �     �     �     �     �      �     �       
              ;     O     \     k     �     �  
   �  	   �     �     �     �     �               %     2     I  &   ]     �     �     �  '   �     �  S   �  _   1  �   �     2     ;     C     P  T   U     �     �     �     �     �     �     �               )     <     I     Y     g     x     �  �  �     T     g  
   n  
   y  	   �     �     �     �     �     �     �     �     �     �     �     �            0        E     S     d  
   s     ~     �     �     �     �     �  	   �  
   �  (   �                 
        *  	   8     B  #   R     v  �   �  2   B     u     |  	   �     �     �     �     �     �     �  !   �  &        :     G     M  	   `     j  	   �     �     �     �     �     �     �     �     �          -     =     \     s     �     �     �  3   �     �     �     	  
   !     ,     1     6     H     [     l     |  B   �  
   �     �     �  0   
     ;  d   B  t   �  �        �     �     �     �  j         k     z     �     �     �     �     �     �     �     �     
          0     >     O     U     C   ;   7   v                  W   j      M   ?   g              !   H                T   ^   E   D       %   q   a           )   @      "   _             h       9       O      *             I   F   n   V   e   
   (   k   `       X          S   \                  R          5   m   '   f   o       $   <   t   ,   Z   L                 B   w          s       P           p   .   i                    :   Q       Y   r       &   [   J   0   u                         A         =          	   G               c   #   K       3       4   +   2       N       1       6      /   l   8   >   ]   d   -   U       b           &Autodetect &Delete &Duplicate... &Edit... &New... &Shortcut: 1 Action Action window Activate window:  Active window Active window:  Add a new conditionAnd Allow Merging And_conditionAnd Application: Arguments: Call Change the exported state for the actions. Command/URL Command/URL :  Command/URL: Comment Comment: Condition typeActive Window ... Condition typeAnd Condition typeExisting Window ... Condition typeNot Condition typeOr Conditions Contains Copyright 2008 (c) Michael Jansen D-Bus Command D-Bus:  Delete Desktop Dialog Dock Does Not Contain Does Not Match Regular Expression Don't change the state of exported hotkey actions.Actual State Draw the gesture you would like to record below. Press and hold the left mouse button while drawing, and release when you have finished. EMAIL OF TRANSLATORSYour emails Edit Edit Gesture Edit... Existing window:  Export Actions Export Group Export Group... Export hotkey actions in enabled state.Enabled Export hotkey actions into disabled  stateDisabled Failed to run qdbusviewer Failed to start service '%1'. Filename Function: Gesture trigger Gestures Global Shortcut Id Import... Input Action: %1 Input Actions Daemon Is Is Not K-Menu Entry KDE Hotkeys Configuration Module KHotkeys file id. Keyboard input:  Maintainer Matches Regular Expression Menu Editor entries Menu entry:  Michael Jansen Mouse Gesture Action Mouse button: NAME OF TRANSLATORSYour names New Action New Group No service configured. Normal Not_conditionNot Or_conditionOr Remote application: Remote object: Save changes Select Application ... Send Keyboard Input Set import id for file, or leave empty Settings Shortcut trigger:  Specific window Start the Input Actions daemon on login Test The current action has unsaved changes. If you continue these changes will be lost. This "actions" file has already been imported before. Are you sure you want to import it again? This "actions" file has no ImportId field and therefore it cannot be determined whether or not it has been imported already. Are you sure you want to import it? Timeout: Trigger Trigger When Type Unable to contact khotkeys. Your changes are saved, but they could not be activated. Voice trigger:  Window Window Action Window Data Window Types Window appears Window class: Window disappears Window gets focus Window loses focus Window role: Window simple:  Window title: Window trigger:  action enabledEnabled action nameName Project-Id-Version: khotkeys
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2011-06-23 07:43+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;



 &Finna sjálfkrafa E&yða &Klóna... Br&eyta... &Nýtt... &Flýtilykill: 1 Aðgerð Aðgerðargluggi Virkja glugga:  Virkur gluggi Virkur gluggi:  Og Leyfa sameiningu Og Forrit: Breytur: Kall Breyta stöðu útflutnings fyrir aðgerðirnar. Skipun/slóð Skipun/Slóð :  Skipun/slóð: Athugasemd Athugasemd: Virkur gluggi ... Og Gluggi sem er til staðar... Ekki Eða Skilyrði Inniheldur Höfundarréttur 2008 (c) Michael Jansen D-Bus skipun D-Bus:  Eyða Skjáborð Samtalsgluggi Á spjald Inniheldur ekki Fellur ekki að reglulegu segðinni Núverandi staða Teiknaðu hreyfinguna sem þú ætlar að taka upp hér fyrir neðan. Ýttu niður oh haltu inni vinstri músarhnappnum á meðan þú teiknar, slepptu síðan þegar þú ert búin(n). ra@ra.is, nifgraup@hotmail.com, leosson@frisurf.no Breyta Breyta bendingu Breyta... Núverandi gluggi:  Flytja út aðgerðir Flytja út hóp Flytja út hóp... Virkt Óvirkt Tókst ekki að keyra qdbusviewer Mistókst að ræsa þjónustuna '%1'. Skráarheiti Fall: Bendingavekjari... Bendingar Víðvær flýtilykill Auðkenni Flytja inn... Inntaksaðgerð: %1 Inntaksaðgerðapúki Er Er ekki K-Valmyndarfærsla KDE Hotkeys stillingaeining Auðkenni KHotkeys skráar. Lyklaborðsinntak:  Umsjónarmaður Fellur að reglulegu segðinni Valmyndaritilsfærslur Valmyndarfærsla:  Michael Jansen Músarbendingaraðgerð Músarhnappur: Richard Allen, Björgvin Ragnarsson, Arnar Leósson Ný aðgerð Nýr hópur Engin þjónusta stillt Venjulegur Ekki Eða Fjarlægt forrit: Fjarlægur hlutur: Vista breytingar Velja forrit... Senda lyklaborðsinntak Setja sem auðkenni innflutnings á skrá , eða skilja eftir autt Stillingar Flýtilyklavekjari :  Sérstakur glugga Ræsa Inntaksaðgerðapúkann við innskráningu Prófa Núverandi aðgerð er með óvistaðar breytingar. Ef þú heldur áfram tapast þessar breytingar. Það er þegar búið að flytja inn þessa "aðgerðir" skrá. Ertu viss um að þú viljir flytja hana inn aftur? Þessi "aðgerðir" skrá er ekki með neitt 'ImportId' gildi og því ekki hægt að vita hvort það sé búið að flytja hana inn áður. Ertu viss um að þú viljir flytja hana inn? Tímamörk: Vekjari Vekja þegar Tegund Ekki náðist samband við khotkeys. Breytingarnar þínar voru vistaðar en ekki tókst að virkja þær. Raddvekjari :  Gluggi Gluggaaðgerð Gögn glugga Gluggategundir Gluggi birtist Flokkur glugga (class): Gluggi hverfur Gluggi fær fókus Gluggi missir fókus Tilgangur glugga: Einfaldur gluggi :  Gluggatitill: Gluggavekjari :  Virkt Heiti 