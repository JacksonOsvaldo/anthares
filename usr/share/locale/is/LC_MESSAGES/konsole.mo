��    s      �  �   L      �	     �	  
   �	     �	     �	     
     
     
  6   #
  5   Z
  +   �
  )   �
  1   �
  �     9   �  L   �  6   &  =   ]      �  ,   �  ,   �  #     *   :  "   e     �     �     �  
   �     �     �               .     =     X     a     o  
   �     �     �     �     �     �     �     �      �               -     C     P     b  +   j     �     �     �     �     �     �     �     �     �     �     �     
       0   "  6   S     �     �     �     �     �     �       %        8  	   J     T     [     k     s     |  ,   �     �  
   �     �  
   �  	   �  
   �     �               (     A     S     f     ~     �     �     �  
   �  
   �     �     �     �     �  
   
  e        {  &   �     �     �     �     �  �  �     �     �     �     �  	   �                    %  	   .     8     ?  �   E     �     �     �     �     �                          $     B     V     l     �     �     �     �     �     �     �               $     ;     I     Z     h     w     �     �     �  T   �               '     F     T  	   g  '   q  	   �     �     �     �     �     �     �  
   �     �  	   �               !  6   )  1   `     �     �     �     �  Y   �     4     Q     d     �     �     �  
   �     �     �     �  "   �  
   �            	   '     1  
   >     I     ]  
   v     �     �     �     �     �     �               &     3     ?     R     d     k     �  w   �       (   *     S     Z     l     s             5   b   d           >   G   _   V   P   R   e   A   7   M      f       8      '   2   C   p   Z       #   T   o   j          \   )   Q      l   B   E           &           n   %   S   N      h   L                c   X   U                  q       *          .       <   `                     m   @   0                     =   g   s   k   ,   H   +          
      $   Y                 r   9   	   a   ?               1   ]   W       (       F   [   K       6         -           J   :   ;              !   D             i                /   I   ^   4           3       "   O              &All Tabs in Current Window &Close Tab &Rename Tab... &Select Tabs... &Stop &ZModem Upload... ... @action:button Create an alternate color schemeNew... @action:button Create an alternate key bindingNew... @action:button Display options menuOptions @action:button Go to the next phraseNext @action:button Go to the previous phrasePrevious @info:statusThe background transparency setting will not be used because your desktop does not appear to support transparent windows. @item:inmenu The host the user is connected to via ssh%1 @item:inmenu The user's name and host they are connected to via ssh%1 on %2 @label:listbox Column header text for color namesName @label:listbox Column header text for the actual colorsColor @title:column Profile labelName @title:column Profile shortcut textShortcut @title:group Generic, common optionsGeneral @title:tab Complex optionsAdvanced @title:tab Generic, common optionsGeneral A descriptive name for the profile Activity in session '%1' Always Hide Tab Bar Always Show Tab Bar Appearance Background transparency: Bell in session '%1' Black on Light Yellow Black on Random Light Black on White Color Scheme && Background Command: Confirm Close Copy Email Address Copy Input Copy Input To Copy Link Address Dark Pastels Default (XFree 4) Default profile Description: Deselect All EMAIL OF TRANSLATORSYour emails Edit Edit Environment Edit Key Binding List Edit Profile Edit Profile "%1" Edit... Editing profile: %2 Editing %1 profiles: %2 Environment: File Filter: Font Green on Black Help Hide the scroll bar Icon: Initial directory: Insert Key Bindings Key Combination Konsole Konsole does not know how to open the bookmark:  Limit the remembered output to a fixed number of lines Linux Colors Linux console Monitor for &Activity Monitor for &Silence NAME OF TRANSLATORSYour names New Key Binding List No scrollback Number of lines of output to remember Open File Manager Open Link Output Paste Selection Percent Preview: Profile name: Remember all output produced by the terminal Remove Rename Tab S&crollback Scroll Bar Scrolling Select All Select Initial Directory Send Email To... Settings Show Tab Bar When Needed Show on left side Show on right side Silence in session '%1' Size: %1 x %2 Size: XXX x XXX Smooth fonts Solaris console Split View Tab Titles Tab bar position: Tab title format: Tabs Terminal Features Text size: This color scheme uses a transparent background which does not appear to be supported on your desktop Unlimited scrollback Vary the background color for each tab View White on Black Window ZModem Progress Project-Id-Version: konsole
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-23 04:59+0100
PO-Revision-Date: 2016-04-08 22:57+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;



 &Alla flipa í þessum glugga &Loka flipa Endu&rnefna flipa... &Velja flipa... &Stöðva &ZModem upphal... ... Nýtt... Nýtt... Valkostir Næsta Fyrra Stillingar sem varða gegnsæi á bakgrunni verða ekki notaðar, því það lítur út fyrir að skjáborðsumhverfið þitt styðji ekki gegnsæa glugga. %1 %1 á %2 Heiti Litur Heiti Flýtilykill Almennt Nánar Almennt Lýsandi nafn á setusniðið Virkni í setu '%1' Alltaf fela flipaslá Alltaf sýna flipaslá Útlit Gegnsæi bakgrunns: Bjalla í setu '%1' Svart á ljósgulu Svart á slembnum ljósum lit Svart á hvítu Litaskema && bakgrunnur Skipun: Staðfesta lokun Afrita tölvupóstfang Afrita inntak Afrita inntak í Afrita tengil Dökkir pastel Sjálfgefið (XFree 4) Sjálfgefið setusnið Lýsing: Afvelja allt logir@logi.org, ra@ra.is, andmann@andmann.eu.org, pjetur@pjetur.net, sv1@fellsnet.is Breyta Breyta umhverfi Breyta lyklaskilgreiningalista Breyta sniði Breyta sniði "%1" Breyta... Breyti sniði: %2 Breyti %1 sniðum: %2 Umhverfi: Skrá Sía: Letur Grænt á svörtu Hjálp Fela skrunslána Táknmynd: Upphafsmappa: Setja inn Lyklabindingar Lyklasamsetning Konsole Konsole veit ekki hvernig eigi að opna bókamerkið:  Takmarka feril í minni við fastan fjölda lína Linux litir Linux skjáhermir Fylgj&ast með virkni &Leita eftir þögn Logi Ragnarsson, Richard Allen, Davíð S. Geirsson, Pjetur G. Hjaltason, Sveinn í Felli Nýr lyklaskilgreiningalisti Engin afturrakning Fjöldi lína sem á að muna Opna skráastjóra Opna tengil Úttak Líma vali Prósent Forsýning: Heiti setusniðs: Muna allt úttak út skjáherminum Fjarlægja Endurnefna flipa A&fturrakning Skrunslá Afturrakning Velja allt Veldu upphafsmöppu Senda tölvupóst til... Stillingar Sýna flipaslána þegar þarf Sýna á vinstri kanti Sýna á hægri kanti Þögn í setu '%1' Stærð: %1 x %2 Stærð: XXX x XXX Mýkja letur Solaris skjáhermir Kljúfa sýn Heiti flipa Staða flipastiku: Titilsnið flipa: Flipar Eiginleikar skjáhermiforrits Textastærð: Þetta litastef notar gegnsæan bakgrunn, en það lítur út fyrir að skjáborðsumhverfið þitt styðji ekki slíkt Ótakmörkuð afturrakning Breytilegur bakgrunnur fyrir hvern flipa Skoða Hvítt á svörtu Gluggi Framvinda ZModem 