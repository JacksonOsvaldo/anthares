��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �   �  e   �     �     �                    -     5     E     T     d     }     �  %   �     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-04-14 08:39+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;


 Dimma skjáinn um helming Dimma skjáinn algerlega Listar möguleika á birtustillingum skjás eða setur stillingar birtustigs á það sem er tilgreint í :q:; t.d. birta skjás í 50 myndi dimma skjáinn í 50% af mestu birtu Listar alla möguleika á kerfishvíld (t.d. svæfa, leggja í dvala) og gerir kleift að virkja þá dimma skjá leggja í dvala birta skjás svæfa setja í bið á disk í vinnsluminni dimma skjá %1 birta skjás %1 Setja birtustigið á %1 Setja í bið á disk Setja í bið í minni (RAM) Setur kerfið í bið í vinnsluminni Setur kerfið í bið á diski 