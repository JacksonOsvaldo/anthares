��          �      �       H  &   I  +   p  	   �     �     �     �     �     �  (   �       ,   &     S     [  �  g  4   )  :   ^     �  (   �     �     �     �     �  )   �       &   <     c     q                                         	      
                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2011-08-16 16:08+0000
Last-Translator: Sveinn í Felli <sveinki@nett.is>
Language-Team: Icelandic <kde-isl@molar.is>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;




 Viltu setja í bið í vinnsluminni (svæfa - sleep) Viltu setja í bið á diski (leggja í dvala - hibernate) Leggja í dvala Leggja í dvala (setja í bið á diski) Fara Fara... Læsa Læsa skjánum Útskrá, slökkva eða endurræsa tölvu Svæfa (setja í bið í RAM) Hefja samhliða setu sem annar notandi Setja í bið Skipta um notanda 