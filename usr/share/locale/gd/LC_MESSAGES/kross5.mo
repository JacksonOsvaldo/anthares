��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     e  %   m     �     �  
   �     �     �     �     	     	     %	      .	     O	     [	  A   y	  R   �	  ,   
  1   ;
     m
     v
     �
  5   �
  	   �
     �
     �
  "   �
       '   )     Q  %   W     }  +   �     �  <   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-11-04 15:15+0000
Last-Translator: Michael Bauer <fios@akerbeltz.org>
Language-Team: Fòram na Gàidhlig
Language: gd
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
X-Generator: Poedit 1.8.4
X-Project-Style: kde
 Ùghdar Còir-lethbhreac 2006 Sebastian Sauer Sebastian Sauer An sgriobt ri ruith. Coitcheann Cuir sgriobt ùr ris. Cuir ris... A bheil thu airson sgur dheth? Beachd: fios@foramnagaidhlig.net Deasaich Deasaich an sgriobt a thagh thu. Deasaich... Ruith an sgriobt a thagh thu. Cha deach leinn sgriobt a chruthachadh airson an interpreter "%1" Cha deach leinn faighinn a-mach dè an interpreter airson an fhaidhle sgriobt "%1" Dh'fhàillig le luchdadh an interpreter "%1" Dh'fhàillig le fosgladh an fhaidhle sgriobt "%1" Faidhle: Ìomhaigheag: Interpreter: Leibheil na tèarainteachd airson an interpreter Ruby GunChleoc Ainm: Chan eil am foinscean "%1" ann Chan eil an t-interpreter "%1" ann Thoir air falbh Thoir an sgriobt a thagh thu air falbh. Ruith Chan eil am faidhle sgriobt "%1" ann. Cuir stad air Cuir stad air ruith an sgriobt a thagh thu. Teacsa: Goireas na loidhne-àithne gus sgriobtaichean Kross a ruith. Kross 