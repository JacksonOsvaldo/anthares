��    >        S   �      H     I     R     [     j     |     �  �  �  �   y  -   	  )   3	  -   ]	  +   �	  r   �	  	   *
  	   4
     >
     V
     ^
     g
  
   t
     
     �
     �
     �
      �
     �
     �
     �
      �
            r   :  5   �     �     �                -     L  	   Q     [     a     i  (   v     �     �     �     �     �       +     3   9     m     u     �     �  "   �  S   �  )        9  �   E  �  #  
   �  
   �     �     �  	          �  6  �   /  7   �     4     =     L  j   a     �     �     �       
          +   ,  
   X     c     y  1   �     �     �     �     �  :     
   ?  )   J  �   t  B        W  #   r     �     �     �     �     �     �     �       4        M  %   d  0   �  *   �  	   �     �  <     ?   C     �     �     �     �  1   �  p   �  J   m     �    �     )            5                        
   8             2   -          $   %      ;   4              <      1   7                 #   6       &       "       	            3      ,            :   (       9                 =   0   '   /              .       +       *                 >         !        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Install from File Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcm5_icons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-05-16 06:52-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 &כמות: אפק&ט: צבע משני: &שקוף למחצה ער&כה (c) 2000-2003 Geert Jansen <h1>סמלים</h1>מודול זה מאפשר לך לבחור בסמלי לשולחן העבודה שלך.<p>על מנת לבחור ערכת סמלים לחץ על שמה והחל את בחירתך על ידי לחיצה על הכפתור "החל" למטה. אם אינך רוצה להחל את בחירתך באפשרותך ללחוץ על הכפתור "אפס" על מנת להתעלם משינוייך.</p><p>בלחיצה על הכפתור "התקן ערכה מתוך קובץ..." באפשרותך להתקין ערכות סמלים על ידי כתיבת המיקום בתיבה או על ידי עיון וחיפוש המיקום. לחץ על הכפתור "אשר" על מנת לסיים את ההתקנה.</p><p>הכפתור "הסר ערכת נושא" יאופשר רק אם בחרת ערכת נושא שהתקנת בעזרת מודול זה. אין באפשרותך להסיר ערכות נושא גלובליות כאן.</p><p>באפשרותך גם לציין אפקטים שיוחלו על הסמלים.</p> <qt>האם אתה בטוח שברצונך להסיר את ערכת הסמלים <strong>%1</strong>?<br /><br />ההסרה תביא למחיקת הקבצים שהותקנו על ידי ערכה זו.</qt> <qt>מתקין את הערכה <strong>%1</strong></qt> פעיל לא־זמין ברירת־מחדל אירעה שגיאה במהלך ההתקנה, אך רוב הערכות בארכיון כבר הותקנו מתק&דם כל הסמלים Antonio Larrosa Jimenez &צבע: צביעה בקשה לאישור ביטול הרוויה (הפחתת צבע) תיאור שולחן עבודה תיבות דו־שיח גרור או הקלד את כתובת הערכה spatz@012.net.il פרמטרי אפקט שקיפות Geert Jansen הורד ערכות נושא חדשות מהאינטרנט סמלים מודול סמלים ללוח הבקרה אם כבר הורדת ערכה למחשב שלך, כפתור זה יפרוס את הערכה ויהפוך אותה לזמינה עבור על יישומי KDE התקנה של קובץ ערכה ששמורה אצלך במחשב התקן מתוך קובץ מתקין ערכות סמלים... Jonathan Riddell סרגל כלים ראשי דרור לוין שם ללא אפקט לוח תצוגה מקדימה הסר ערכה הסרת הערכה הנבחרת מהמחשב שלך הגדרת אפקט... הגדרות אפקט סמל פעיל הגדרות אפקט סמל ברירת מחדל הגדרות אפקט סמל לא זמין גודל: סמלים קטנים הקובץ אינו ארכיון ערכת סמלים תקף. זה ימחק את הערכה הנבחרת מהדיסק שלך. גווני אפור שני צבעים סרגלי כלים Torsten Rahn אין אפשרות ליצור קובץ זמני. אין אפשרות להוריד את ארכיון ערכת הסמלים.
בדוק שהכתובת %1 נכונה. אין אפשרות למצוא את ארכיון ערכת הסמלים %1. שימוש הסמל אתה צריך להיות מחובר לאינטרנט כדי להשתמש באפשרות זו. חלון חדש יציג את הערכות הזמינות מהאתר http://www.kde.org. לחיצה על כפתור ההתקנה יתקין את הערכה הנבחרת במחשב. 