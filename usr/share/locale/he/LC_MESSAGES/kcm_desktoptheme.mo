��          |      �             !     9      E     f  
   �     �  &   �     �     �     �  1     �  E  7   �     5     A  )   U          �  7   �  *   �  .     (   :  _   c                             	      
                        Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-05-16 06:52-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 הגדר ערכת נושא של שולחן העבודה David Rosca kde-l10n-he@kde.org צוות התרגום של KDE ישראל פתח ערכת נושא הסר ערכה קובצי ערכת נושא (*.zip *.tar.gz *.tar.bz2) התקנת ערכת הנושא נכשלה. ערכת הנושא הותקנה בהצלחה. הסרת ערכת הנושא נכשלה. מודל זה מאפשר לך להגדיר את ערכת נושא של שולחן העבודה. 