��    E      D  a   l      �  
   �  
   �  
             !     %     9     H     N  	   ]     g  	   o     y     �     �  
   �     �     �  	   �     �     �     �     �     �     �               "     )  
   ;     F  2   Y  ?   �     �     �     �     �     �     �     
               .     <     ?     O     \     b     o  	   {     �     �     �     �  b   �  �   	     �	     �	  	   �	     �	     �	  
   �	  	   �	     	
     
     !
     '
     9
  �  J
               -     A     Y     ]     {     �     �     �  	   �     �     �     �     �     �  
             #     0     N     e     j     �     �     �  $   �     �     �     �     
  I   "  b   l     �     �     �     �       '         H     M     a     x  
   �     �     �     �     �     �  	   �     �     �          '  c   0  �   �  
   d     o     �      �     �  
   �     �     �     �     �  $     "   *     *       D   B   3       %       C           
   ;      )   8               E      .   "                @       ?   <                        :                  A             9       &           /         0   1         !                       ,   '       5   >         =              #   -   $   +              (       7       2   4   	                  6    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.plasma.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-05-22 05:04-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 פעילויות הוסף פעולה הוסף מפריד הוסף יישומון Alt חלופות ליישומון תמיד נראה החל החל הגדרות החל כעת מחבר: הסתרה אוטומטית לחצן חזרה למטה בטל קטגוריות במרכז סגור הגדרות הגדרות פעילויות צור פעילות... Ctrl כרגע בשימוש מחק דואר אלקטרוני: כפתור הרצה השג יישומונים חדשים גובה גלילה אופקית קלט כאן קיצורי מקלדת אי אפשר לשנות פריסה כשהיישומונים נעולים יש להחיל שינויים בפריסה לפני שניתן לבצע שינויים אחרים פריסה: שמאל לחצן שמאלי רישיון: נעל יישומונים הגדל לוח לגודל המירבי Meta לחצן אמצעי עוד הגדרות... פעולות עכבר אישור ישור לוח הסר לוח ימין לחצן ימני קצה המסך חפש... Shift עצור פעילות פעילויות עצורות: החלף ההגדרות של המודול הנוכחי השתנו. התרצה לשמור אותם או לא? הקיצור יפעיל את היישומון: הוא ייתן למקלדת להתמקד בו, ואם ליישומון יש חלון קופץ (כגון תפריט התחלה), החלון הקופץ יפתח. למעלה בטל הסרת התקנה הסר הסר התקנת יישומון גלילה אנכית נראות רקע סוג רקע: יישומונים רוחב חלונות יכולים לכסות חלונות נמצאים מתחת 