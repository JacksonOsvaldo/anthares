��    ,      |  ;   �      �     �     �               +     8     D     Q  �   `  �   �  �   �  �     g   �  Y     Y   r  ]   �     *     /  
   8     C  
   U     `  	   i     s  
   �     �  ]   �     	  I   	     d	  %   }	      �	     �	     �	     �	     
  H   &
     o
  -   �
     �
     �
  *   �
     �
  �  	     �     �     �               *     <     N  u   d  �   �  y   �  �     J   �  B   �  5   :  @   p  
   �     �     �     �  
   �            "   &     I     Y  !   v     �  a   �  !     0   6  )   g  "   �     �  %   �     �  Q     %   f     �     �     �  /   �     �     "                                       (      ,           '                       	   *   #             
   )                       $                          +                           %   !   &        @title:windowChange your Face @title:windowChoose Image Add user account Choose from Gallery... Clear Avatar Delete User Delete files Email Address: Error returned when the password is invalidThe password should be at least %1 character The password should be at least %1 characters Error returned when the password is invalidThe password should contain at least %1 lowercase letter The password should contain at least %1 lowercase letters Error returned when the password is invalidThe password should contain at least %1 number The password should contain at least %1 numbers Error returned when the password is invalidThe password should contain at least %1 uppercase letter The password should contain at least %1 uppercase letters Error returned when the password is invalidThe password should not contain sequences like 1234 or abcd Error returned when the password is invalidThis password can't be used, it is too simple Error returned when the password is invalidYour name should not be part of your password Error returned when the password is invalidYour username should not be part of your password John John Doe Keep files Load from file... MyPassword New User Password: Passwords do not match Real Name: Remove user account Returned when a more specific error message has not been foundPlease choose another password Select a new face: The username can contain only letters, numbers, score, underscore and dot The username is too long The username must start with a letter This e-mail address is incorrect This password is excellent This password is good This password is very good This password is weak This user is using the system right now, removing it will cause problems This username is already used Title for change password dialogNew Password Users Verify: What do you want to do after deleting %1 ? john.doe@example.com Project-Id-Version: user_manager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:21+0100
PO-Revision-Date: 2017-05-22 05:00-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 שנה את הפרצוף שלך בחר תמונה הוסף חשבון משתמש בחר מהגלריה... נקה דמות מחק משתמש מחק קבצים כתובת דוא"ל: הסיסמה צריכה להיות לפחות תו אחד הסיסמה צריכה להיות לפחות %1 תווים הסיסמה צריכה להכיל לפחות תו אחד באותיות רגילות הסיסמה צריכה להכיל לפחות %1 תווים באותיות רגילות הסיסמה צריכה להכיל לפחות ספרה אחת הסיסמה צריכה להכיל לפחות %1 ספרות הסיסמה צריכה להכיל לפחות תו אחד באותיות רשיות הסיסמה צריכה להיות לפחות %1 תווים באותיות רשיות הסיסמה לא צריכה להכיל סדרות כגון 1234 או abcd אי אפשר להשתמש בסיסמה זו, היא קלה מדי השם לא צריך להיות חלק מהסיסמה שם המשתמש לא צריך להיות חלק מהסיסמה פלוני פלוני אלמוני שמור קבצים טען מקובץ... MyPassword משתמש חדש סיסמה: הסיסמה אינה מתאימה שם אמיתי הסר חשבון משתמש אנא בחר סיסמה אחרת בחר פרצוף חדש: שם המשתמש יכול להחיל רק אותיות, מספרים, ניקוד ונקודות. שם המשתמש ארוך מדי שם המשתמש חייב להתחיל באות כתובת המייל אינה תקינה הסיסמה הזו מצויינת הסיסמה הזו טובה הסיסמה הזו מאוד טובה הסיסמה הזו חלשה המשתמש משתמש במערכת כרגע, הסרתו תגרום לבעיות שם המשתמש כבר בשימוש סיסמה חדשה: משתמשים אמת: מה תרצה לעשות לאחר מחיקת %1 john.doe@example.com 