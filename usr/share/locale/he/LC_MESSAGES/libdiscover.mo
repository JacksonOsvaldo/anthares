��    d      <  �   \      �  9   �     �  )   �     �     	     *	     B	     X	     n	  #   �	  *   �	     �	     �	     �	  	   
     
     !
     *
     6
     M
     b
     x
     �
     �
     �
     �
     �
     �
     �
     	     "     3     K     ^     s     �     �     �     �     �  !   �               "     4     J     X     m     �     �     �     �     �     �     �          !     5     E     ^     o     �     �     �     �     �     �     �  $   	     .     H     W     h     w  	   �     �     �     �     �  	   �     �     �     �                     )     6     I     W     `     l     x  
   �  !   �     �     �     �     
  �        �     �  !   �       
     
   %     0  
   9  
   D     O  $   k     �     �     �     �     �  
   �     �     �  
             "     /     D     U     g  
   }  
   �     �     �     �     �     �     �  
             #     0     =     N     _     ~     �     �     �     �     �     �     �  
                  .     F  
   Z     e       
   �     �     �     �     �     �     �  
   �                    ,     J     a     m     �     �  
   �     �     �     �     �     �     �  !        /     =     K     `          �     �     �     �     �     �                    &     /     >     M              F   c       L   b   X   0   @   Y   -   U   =                  3   ^              &                (   W   	              Q          O                       2   a       .       D      C   A      9      E       T          !              :   
   ]   1      P   /   J         '       H   )       S       [   Z   d   N   ?       \       `      K   +          V   B   >       I      $       #                    ,   <   8                      R       "       ;          7   *       6   5   4   G   _   %    @action Checks the Internet for updatesCheck for Updates @info app size%1 on disk @info app size%1 to download, %2 on disk @info:statusDone @info:statusDownloading @info:statusInstalling @info:statusRemoving @info:statusStarting @info:statusWaiting @item:inlistboxApplication Updates @item:inlistboxImportant Security Updates @item:inlistboxSystem Updates Accept EULA Application Addons Available Broken Canceled Category3D CategoryAccessibility CategoryAccessories CategoryApplications CategoryArcade CategoryAstronomy CategoryBiology CategoryBoard Games CategoryCard Games CategoryChat CategoryChemistry CategoryDebugging CategoryDeveloper Tools CategoryDrawing CategoryDummy Category CategoryEducation CategoryElectronics CategoryEngineering CategoryFile Sharing CategoryFonts CategoryGames CategoryGeography CategoryGeology CategoryGraphic Interface Design CategoryGraphics CategoryIDEs CategoryInternet CategoryLocalization CategoryMail CategoryMathematics CategoryMultimedia CategoryOffice CategoryPhotography CategoryPhysics CategoryPlasma Addons CategoryPlasma Widgets CategoryPublishing CategoryPuzzles CategoryRole Playing CategorySimulation CategorySports CategorySystem Settings CategoryViewers CategoryWeb Browsers CategoryWeb Development Categorydummy Categorydummy 1 Categorydummy 2.1 Categorydummy 3 Categorydummy 4 Categorydummy addons Categorydummy with quite some stuff Categorydummy with stuff Cleaning up... Copying files... Downloading... Finished Installed Installing... Plasma Addons Processing... Refreshing Cache... Remove... Restart: Retrieving size information Setup... Unknown Unknown Status Unknown error %1. Unknown size Unknown status %1. Update State: Updates: Updating... Upgradeable Waiting for authorization... Waiting... comma separating package names,  package-name (version)%1 (%2) update stateStable update stateTesting update stateUnstable Project-Id-Version: libdiscover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-08 05:55+0100
PO-Revision-Date: 2017-05-16 12:46-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 בדוק עדכונים %1 על דיסק %1 להורדה, %2 על דיסק סיים מוריד מתקין מסיר מתחיל ממתין עדכוני יישומים עדכנוי אבטחה חשובים עדכוני מערכת אשר EULA תוספי יישומים זמין שבור מבוטל תלת מימד נגישות עזרים יישומים ארקייד אסטרונומיה ביולוגיה משחקי לוח משחקי קלפים צאטים כימיה ניפוי באגים כלי מפתחים שירטוט קטגורית דמה חינוכי אלקטרוניקה הנדסה שיתוף קבצים גופנים משחקים גאוגרפיה גאולוגיה מעצבי ממשק משתמש גרפיקה סביבות פיתוח אינטרנט בינאום דואר אלקטרוני מתמטיקה מולטימדיה משרד צילום פיזיקה תוספי Plasma יישומוני Plasma הוצאה לאור פזלים משחקי תפקידים סימולציות ספורט הגדרות מערכת מציגים דפדפני אינטרנט פיתוח Web דמה דמה 1 דמה 2.1 דמה 3 דמה 4 תוספי דמה דמה עם כמה דברים דמה עם דברים מנקה... מעתיק קבצים מוריד... הסתיים הותקן מתקין... תוספי Plasma מעבד... מרענן מטמון... מסיר... הפעל מחדש: אחזר מידע על הגודל מגדיר... לא ידוע מצב לא ידוע שגיאה לא ידועה %1. גודל לא ידוע מצב לא ידוע %1. מצב עדכון: עדכונים: מעדכן... ניתן לשדרוג ממתין לאימות ממתין... , %1 (%2) יציב בבדיקות לא יציב 