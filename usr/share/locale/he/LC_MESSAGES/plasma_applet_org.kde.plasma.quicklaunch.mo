��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  j   �       
        (     =     X     l     u     �     �     �     �  "   �     	        
   4                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org.kde.plasma.quicklaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2017-05-16 06:55-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 הוסף משגר... הוסף משגר על ידי גרירה ושחרור או על ידי שימוש בתפריט ההקשר. מראה סידור ערוך משגר... אפשר חלון קובץ הכנס כותרת כללי הסתר סמלים מקסימום עמודות מקסימות שורות: הפעלה מהירה הסר משגר הראה סמלים מוסתרים הראה שם משגר הראה כותרת כותרת 