��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  +   �  +   	  +   I	  %   u	  '   �	     �	  
   �	  
   �	     �	     �	  p   
  (   t
  K   �
  
   �
     �
                1     >     N     f     z  
   �  
   �     �     �  2   �  J   �     =     D     I     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-23 10:19-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 100% יוצר Copyright 2015 Harald Sitter Harald Sitter אין יישומים שמנגנים שמע אין ישומים שמקליטים שמע אין פרופילי התקן זמינים אין התקני קלט זמינים אין התקני יצוא זמינים פרופיל: PulseAudio מתקדם יישומים התקנים הוסף התקן פלט וירטואלי כדי לדמות פלט בכל כרטיסי המסך המקומיים הגדרות יציאות מתקדמות החלף אוטומטית את כל ההזרמות שפלט חדש זמין לכידה ברירת מחדל פרופילי התקן ttv200@gmail.com כניסות השתק שמע אלקנה ברדוגו צליל התראה יציאות השמעה יציאה (לא זמין) (לא מחובר) מודל  'module-gconf'  של PulseAudio נדרש המודול מאפשר להגדיר את  מערכת השמע Pulseaudio. %1: %2 100% %1% 