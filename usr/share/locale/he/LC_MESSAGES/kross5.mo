��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �  	   �  	   �     �  
     #        7  #   E  9   i  B   �  ,   �  0     	   D     N  	   V  /   `  )   �     �  !   �     �     �  $   	     *	  )   1	     [	  .   d	  	   �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2017-05-16 06:50-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 כללי הוספת תסריט חדש. הוספה... לבטל? הערה: kde-l10n-he@kde.org עריכה עריכת התסריט הנבחר. עריכה... הפעלת התסריט הנבחר. יצירת תסריט עבור המפרש "%1" נכשלה קביעת מפרש עבור קובץ התסריט "%1" נכשלה נכשלה הטעינה של המפרש "%1" פתיחת קובץ התסריט "%1" נכשלה קובץ: סמל: מפרש: רמת הבטיחות של המפרש של Ruby צוות התרגום של KDE ישראל שם: אין פונקציה כזו "%1" אין מפרש כזה "%1" הסר הסר את התסריט הנבחר. הרץ קובץ התסריט "%1" לא קיים. עצור הפסקת פעולת התסריט הנבחר. טקסט: 