��    !      $  /   ,      �     �     �     �  	   
  A     >   V  9   �  9   �  9   	  W   C  E   �     �     �      �        X   :     �     �     �     �  
   �  
   �     �               1     ?     F     M     ]  	   {     �  �  �  
   G     R     [     t     �     �     �  
   �     �  /   �     �     	     	     	  H   3	  �   |	  
   �	  )   

     4
  '   A
  
   i
  
   t
  (   
  ,   �
     �
     �
  
   
            *   7     b     t                     	                                                                                !                                   
                       Accurate Always Animation speed: Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Exclude internal Desktop Effects Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant NAME OF TRANSLATORSYour names Never Only for Shown Windows OpenGL 2.0 OpenGL 3.1 Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-05-16 06:53-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 מדויק תמיד מהירות הנפשה: אוטומטי נגישות מראה סוכריות מיקוד כלים הנפשת החלפת שולחנות עבודה ניהול חלונות הגדר מסנן מהיר kde-l10n-he@kde.org אל תכלול אפקטים פנימיים של שולחן העבודה טיפ: בכדי להפעיל אפקט או לשנות את ההגדרות שלו, אנא בדוק בהגדרות של האפקט. מיידי צוות התרגום של KDE ישראל אף־פעם רק עבור חלונות מוצגים OpenGL 2.0 OpenGL 3.1 הפעל מחדש זיהוי של OpenGL השתמש מחדש בתוכן על המסך מבצע רינדור: שיטת שינוי גודל: חיפוש חלק חלק (איטי יותר) מניעת קריעת תמונה ("vsync") איטי מאוד XRender 