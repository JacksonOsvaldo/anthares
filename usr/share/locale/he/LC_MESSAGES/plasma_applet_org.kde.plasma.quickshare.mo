��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  C     c   U      �     �     �     �               -     9     @     P  &   h  )   �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2017-05-16 06:55-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 <a href='%1'>%1</a> סגור העתק אוטומטחת: אל תראה את הדו שיח הזה, העתק אוטומטית. זרוק אלי טקסט או תמונה כדי להעלות אותם לשירות באינטרנט שגיאה במהלך העלאה כללי גודל היסטוריה: הדבק אנא המתן אנא נסה שוב. שולח... שתף שתף ל־'%1' הועלה בהצלחה כתובת האינטרנט שותפה העלה %1 לשירות באינטרנט 