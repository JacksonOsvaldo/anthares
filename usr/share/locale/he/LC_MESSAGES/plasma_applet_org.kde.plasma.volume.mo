��    #      4  /   L           	               $     1     :     J  3   Z  >   �     �     �     �        m        v     �     �     �     �  *   �      �          #  "   4     W     v     �     �     �     �     �     �  ;   �     4  �  J          
          -     ?     N     d     z     �  $   �     �     �     �     �  $   �     	     ,	     I	     R	  ?   l	  .   �	     �	     �	     
     
      %
  
   F
     Q
     j
     ~
     �
     �
     �
     �
     #                          
                                    !                 "               	                                                          % Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-05-23 09:05-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 % יישומים השמע מושתק עוצמת שמע התנהגות התקני לכידה הזרמת לכידה השתק ברירת מחדל הנמך עוצמת מיקרופון הנמך עוצמה התקנים כללי יציאות הגבר עוצמת מיקרופון הגבר עוצמה עוצמה מקסימלית: השתק השתק מיקרופון אין ישומים המשמיעים או מקליטים שמע לא נמצאו יציאות או כניסות התקני השמעה הזרמת שמע (לא זמין) (לא מחובר) העלה עוצמה מירבית עוצמה עוצמת השמע: %1% משוב עוצמה צעדי עוצמה: %1 (%2) %1: %2 100% %1% 