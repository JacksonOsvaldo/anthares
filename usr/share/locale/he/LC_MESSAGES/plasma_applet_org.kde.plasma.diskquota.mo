��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  $   �       >   +     j     �     �     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org.kde.plasma.diskquota
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-05-22 05:35-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 מכסת דיסק לא נמצאה הגבלת מכסה. התקן את 'quota'. כלי Quota אינו נמצא.

אנא התקן את 'quota'. הרצת quota נכשל %1 מתוך %2 %1 פנוי מכסה: %1% בשימוש %1: %2% בשימוש 