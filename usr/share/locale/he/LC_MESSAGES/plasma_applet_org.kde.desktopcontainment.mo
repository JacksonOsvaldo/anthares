��    R      �  m   <      �     �     �     
               &     2     C     Q     Y  /   a  -   �     �  
   �     �     �     �     �     �            
             (     7     O     b     n     u     �     �  	   �     �     �     �  	   �     �     �     �     �     �     �     	     	     	     .	     3	     8	  <   ;	     x	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     $
     8
  )   F
     p
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                  E   1  �  w     :     B     Z     o  	   w     �  !   �     �  	   �     �  	   �     �  
   �  	          
        &  "   3  "   V  
   y     �     �     �  "   �  +   �          ,  
   A  "   L     o     �     �  
   �  )   �     �     �  
   �            
     
   %     0     B     K  2   X     �     �  
   �  r   �  )        A     ]     f     z     �  
   �  
   �     �     �     �     �     �  '        6  @   I  .   �  <   �     �     �            	   *     4     K  
   X     c  1   j     �     �     �  �   �                J   :   R   .      E       M      B       O   ?                3   #       7       8          L   )         9   I   F   ;   *      2   5              N          C       "   K   
      +            Q      %                    	       4                 ,   H   '   G           >   <   =          (   -           1      D   /       @   0   &   A          !   $   6           P                     &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-05-23 08:56-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 &מחק &רוקן פח אשפה ה&עבר לאשפה &פתח &הדבק מא&פיינים &רענן שולחן העבודה &רענן תצוגה &רענן &שנה שם בחר... נקה סמלים יישור סדר ב חזור ביטול עמודוץ הגדרות שולחן עבודה כותרת מותאמת אישית תאריך ברירת מחדל יורד בטל סימון של הכל פריסת שולחן העבודה הכנס כותרת מותאמת אישית תבנית סוג הקובץ: סוגי קבצים: סינון תצוגה מקדימה קופצת תיקיות תחילה תיקיות תחילה נתיב מלא הבנתי הסתר את הקבצים התואמים ענק גודל הסמל סמלים גדול שמאל רשימה מיקום נעל במקום נעול בינוני עוד אפשרויות תצוגה מקדימה... שם כלום אישור לחץ והחזק יישומונים כדי להזיז אותם ולחשוף את לחצני השליטה שלהם תצוגה מקדימה של תוספים תצוגות מקדימות הסרה שינוי גודל שחזר ימין סיבוב שורות חפש סוג קובץ... בחר הכל בחר תיקיה סימן בחירה הצג את כל הקבצים הצג את הקבצים התואמים הצג מיקום: הראה קבצים הקשורים לפעילות הנוכחית הצג את תקיית שולחן העבודה הראה את ארגז הכלים של שולן העבודה גודל קטן קטן בינוני מיין לפי מיון: הגדר תיקייה: פיצפון התאמה סוג הקלד את הנתיב או הכתובת כאן לא ממויין ניהול יישומונים ישומונים נעולים אתה יכול ללחוץ והחזיק יישומונים כדי להזיז אותם ולחשוף את לחצני השליטה שלהם 