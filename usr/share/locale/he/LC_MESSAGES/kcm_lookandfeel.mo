��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �  "   �     �  6   �     �  #   
     .     ;  �   S      A  �   b  A   �  �   (  o   �                     
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-05-23 10:18-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 הגדרות מראה ותחושה הגדרות סמן העכבר הורד חבילות מראה ותחושה חדשות ttv200@gmail.com קבל עיצובים חדשים... Marco Martin אלקנה ברדוגו בחר ערכת נושא כללית לתחנת העבודה (כולל ערכת נושא פלזמה, ערכת צבעים, סמני עכבר, מחליף חלונות ושולחנות עבודה, מסך פתיחה, מסך נעילה וכו) הראה תצוגה מקדימה המודל נותן לך להגדיר את המראה של כל תחנת העבודה עם כמה ערכות מוכנות מראש. השתמש בפריסת שולחן עבודה מערכת נושא אזהרה: פריסת שולחן העבודה שלך תמחק ותשוחזר לפריסת ברירת המחדל של ערכת נושא שנבחרה. אתה צריך להפעיל מחדש את KDE בשביל שהשינויים של סמן עכבר יופיעו. 