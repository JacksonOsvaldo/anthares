��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  d       �
       M     2   f     �  #   �     �     �  9   �  �   7     �     �      �       
              /     >  
   R     ]  
   q     |     �     �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 06:57-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 <p> אתה בחרת לפתוח הפעלה חדשה. <br /> ההפעלה הנוכחית תוסתר ויוצג לך חלון התחברות. <br /> מקש פונקציה (F1, F2 וכו') מוקדש לכל הפעלה; F%1 מוקדש בדרך כלל להפעלה הראשונה, F%2 מוקדש להפעלה השנייה וכו'. באפשרותך להחליף בין ההפעלות על ידי לחיצה על Alt+Control ומקש הפונקציה המתאים בו זמנית. בנוסף הלוח של KDE ותפריטי שולחן העבודה מכילים פקודות כדי לעבור בין ההפעלות. </p> הצג את כל ההפעלות נעילת המסך נועל את ההפעלה הנוכחית ומפעיל את שומר המסך יוצא מסביבת העבודה הנוכחית  הפעלה חדשה מפעיל מחדש את המחשב אתחול המחשב כיבוי המחשב מתחיל הפעלה חדשה בתור משתמש אחר מחליף אל ההפעלה הפעילה עבור משתמש :q:, או מציר את כל ההפעלות הפעילות אם :q: לא סופק מכבה את המחשב הפעלות אזהרה - הפעלה חדשה נעל התנתק התנתקות התנתקות הפעלה חדשה אתחול הפעלה מחדש כיבוי החלפת משתמש החלף החלף :q: 