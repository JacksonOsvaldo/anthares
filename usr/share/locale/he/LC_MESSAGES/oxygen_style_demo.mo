��    v      �  �   |      �	     �	     �	     
     
     
     '
     /
  
   6
     A
     M
     T
  
   b
     m
     r
     z
     �
     �
     �
  
   �
  
   �
  	   �
     �
     �
  
   �
       ,     /   3     c     i  
   p     {  
   �     �  
   �     �     �     �     �     �  
   �     �     �     �                         "     (     ,     /     4     <     H     P     a     i     u     �     �     �     �     �     �     �     �     �     �     �               5     A     T     k       /   �  #   �  .   �     "     (     6  
   <     G     S     X  	   m     w     �  
   �     �     �  
   �  
   �  	   �     �     �  
   	  &     0   ;  "   l  ,   �     �     �     �     �     �     �     �     �  	                  +     <     E     J  �  S               '     ;     O  
   ^     i     v  
   �     �     �     �     �     �  
   �     �               0     D     X     n  )   �     �     �     �     �  
   �     �     �          &     /     C     W     i     r  
   �  
   �     �     �     �     �     �     �                 
   %  
   0     ;     B     S     j     s     �     �     �  
   �  
   �     �     �          '     A     J     `     r     �     �  %   �     �     �     �        .   /  N   ^  7   �  >   �     $     +     ?     H     `     x     �     �     �     �     �     �          *     >     R     h  $   �     �  6   �  F   �  6   9  :   p     �  
   �  
   �     �     �  
   �     
          2     H  (   X  $   �  
   �     �     �     j   +          L   `   p   s   ^       d       b      -   <   %       :           8       1       C          Z   Y       P      (           .   /              U   r       e   u   v   G       5   o   i   a              n   A      B   O   ;       D   4   T   0   W   h   q      ,   "   F   H                            f                  ]           S   7   $          3                =       M   
   >      N   V   I   )              g   t   l   ?          \   '      &   Q       J       #             E   @       2      R   [       !         c   6   K   	   k             X      9   *   _   m    Bottom Bottom to Top Bottom-left Bottom-right Buttons Cascade Center Checkboxes Description Dialog Document mode Down Arrow East Editors Enabled Example text First Column First Description First Item First Page First Row First Subitem First Subitem Description First item Flat Flat frame. No frame is actually drawn.Flat Flat group box. No frame is actually drawnFlat Frame Frames Grab mouse Hide tabbar Horizontal Huge (48x48) Icons Only Input Widgets Large Large (32x32) Layout Left  Left Arrow Left to Right Lists Medium (22x22) Modules New New Row Normal North Off On Open Options Oxygen Demo Partial Password editor: Preview Pushbuttons Radiobuttons Raised Right Right Arrow Right to Left Right to left layout Run Simulation Save Second Column Second Description Second Item Second Page Second Subitem Second Subitem Description Second item Select Next Window Select Previous Window Show Corner Buttons Shows the appearance of buttons Shows the appearance of lists, trees and tables Shows the appearance of tab widgets Shows the appearance of various framed widgets Small Small (16x16) South Tab Widget Tab Widgets Tabs Text Alongside Icons Text Only Text Under Icons Text and icon: Text only: Third Column Third Description Third Item Third Page Third Row Third Subitem Third Subitem Description Third item This is a normal, text and icon button This is a normal, text and icon button with menu This is a normal, text only button This is a normal, text only button with menu This is a sample text Tile Title Title: Toolbox Top Top to Bottom Top-left Top-right Up Arrow Use flat buttons Use flat widgets Vertical West password Project-Id-Version: oxygen_style_demo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2017-05-16 06:54-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Zanata 3.9.6
Plural-Forms: nplurals=2; plural=n != 1;
 למטה מטה למעלה למטה משמאל למטה מימין כפתורים מדורג ממורכז תיבות סימון תיאור תיבת דו־שיח מצג מסמך חץ למטה מזרח עורכים מופעל טקסט לדוגמה עמודה ראשונה תיאור ראשון פריט ראשון עמוד ראשון שורה ראשונה תת־פריט ראשון תיאור של תת־פריט ראשון פריט ראשון שטוח שטוח שטוח מסגרת מסגרות תפוס עכבר הסתר סרגל כלים אנכי ענק (48 על 48) סמלים בלבד פקדים קלט גדול גדול (32 על 32) פריסה משמאל חץ שמאלה משמאל לימין רשימות בינוני (22 על 22) מודולים חדש שורה חדשה פשוט צפון מקובל מופעל פתח אפשרויות הדגמה של Oxygen חלקי עורך ססמאות: תצוגה מקדימה כפתורי לחיצה כפתורי רדיו מוגבה מימין חץ ימינה מימין לשמאל מצג ימין לשמאל הפעל סימולציה שמור עמודה שנייה תיאור שני פריט שני עמוד שני תת־פריט שני תיאור של תת־פריט שני פריט שני בחר חלון הבא בחר חלון קודם הצג כפתורים בפינה מציג את התצוגה של כפתורים מציג את התצוגה של פקדי רשימות, עצים וטבלאות מציג את התצוגה של פקדי לשוניות מציג את התצוגה של פקדים בעיל מסגרת קטן קטן (16 על 16) דרום פקדי לשוניות פקדי לשוניות כרטיסיות טקסט לצד סמלים טקסט בלבד טקסט מתחת לסמלים כיתוב וסמל: טקסט בלבד עמודה שלישית תיאור שלישי פריט שלישי עמוד שלישי שורה שלישית תת־פריט שלישי תיאור תת־פריט שלישי פריט שלישי זהו כפתור רגיל, בעל כיתוב וסמל זהו כפתור רגיל, בעל כיתוב וסמל עם תפריט זהו כפתור רגיל, בעל כיתוב בלבד זהו כפתור רגיל, בעל כיתוב ותפריט זהו כיתוב לדוגמה כותרת כותרת כותרת: ארגז־כלים למעלה מעלה למאה למעמה משמאל למעלה מימין חץ למעלה השתמש בכפתורים שטוחים השתמש בפקדים שטוחים אופקי מערב ססמה 