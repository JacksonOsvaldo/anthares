��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     i     �  E   �  Z   �  ^   3  U   �  U   �  Y   >	  j   �	     
     
     #
     ,
     3
     :
     C
  !   K
     m
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 06:57-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 נעל את המיכל הוצא מדיה מוצא את התקנים אשר השם שלהם מתאים אל :q: מציג את כל ההתקנים ומאפשר לחבר, לנתק ולהוציא אותן. מציג את כל ההתקנים שניתן להוציא, ומאפשר להוציא אותם. מציג את כל ההתקנים שאפשר לחבר, ומאפשר לחבר אותם מציג את כל ההתקנים שניתן לנתן, ומאפשר לנתק אותם מציג את כל ההתקנים שניתן לנעול, ומאפשר לנעול אותם מציג את כל ההתקנים המוצפנים שניתן לשחרר, ומאפשר לשחרר אותם חבר את ההתקן התקן הוצא נעל חבר שחרר נתק  פתח את נעילת המיכל נתק את ההתקן 