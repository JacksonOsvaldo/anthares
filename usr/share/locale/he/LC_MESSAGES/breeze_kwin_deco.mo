��    ?        Y         p     q     s  !   �  "   �  &   �  ,   �  #   (  &   L  !   s  &   �  '   �  "   �  "     '   *     R  +   V     �  
   �     �     �     �     �     �     �     �     �       !        =     ]      b     �     �     �     �     �  !   �     	  	   	     	     	     9	     T	  &   g	     �	     �	     �	     �	     �	     �	     �	     �	     �	  $   
     (
     9
     S
     e
     {
     �
     �
  >   �
  �  �
     �  '   �     �     �     �      �               /     <     L     ^     g     n     �  Q   �     �     �               1     :     U     a     i     �     �  6   �  ,   �       -        L  .   f     �     �  #   �  +   �           	       +   +  !   W     y  6   �  :   �       )   
     4  
   =     H     Q     a     x  8   �      �  "   �                1     R  *   f  	   �     $       *      3      9         1   !       "       )      ?               
         ;                         =      :   4          5                    (      	   #                  -   7      8           %                  /                      >       0   &      6          '         .       ,          +   <   2        % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2017-05-23 06:18-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 % מא&פייני חלון מתאימים ענק גדול ללא קו גבול אין גבולות בצדדים רגיל גדול מדי פיצפון ענק מאוד גדול מאוד גדול קטן גדול מאוד הוסף הוסף איזור אחיזה לשינוי גודל חלונות ללא גבול משך אנימציות: אנימציות גודל כ&פתורים עובי גבול: מרכז מרכז (רוחב מלא) מחלקה: צבע: אפשרויות מסגרת זהה מאפייני חלון דו־שיח צייר עיגול מסביב לכפות הסגירה צייר רקע חלון בשיפוע צבע ערוך עריכת חריגה – הגדרות Breeze אפשר אנימציות אפשור או ביטול החריגה הזו סוג חריגה כללי הסתר את שורת הכותרת מידע עבור חלונות נבחרים שמאל הזז למטה הזז למעלה חריגה חדשה – הגדרות Breeze שאלה  – הגדרות Breeze ביטוי רגולרי תחביר הביטוי רגולרי אינו תקין התאמה באמצעות ביוטיים ר&גולריים הסר להסיר את החריגה הנבחרת ימין צללים גודל קטן מאוד יישור &כותרת כותרת:  השתמש במחלקת החלון (בכל היישום) השתמש בכותרת חלון אזהרה – הגדרות Breeze שם מחלקת חלונות מזהה חלון בחירת תכונות חלון כותרת חלון חריגות לחלונות ספציפים חוזק: 