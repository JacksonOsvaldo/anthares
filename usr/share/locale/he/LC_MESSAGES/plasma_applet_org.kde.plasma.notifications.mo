��    !      $  /   ,      �      �     
       -   5  #   c  #   �  ?   �  1   �          1  H   6  L     V   �  N   #     r  
   ~     �     �     �     �  2   �  )      8   *  #   c  .   �  -   �  D   �  6   )  0   `  A   �  =   �  9     �  K     
     .
  5   @
  4   v
     �
     �
     �
     �
            -     )   L     v     �     �     �     �     �  %   �  	          -   %     S  A   f  >   �  $   �  '     
   4     ?     Q     U     b                                                                   	                       
                                                          !           %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: plasma_applet_org.kde.plasma.notifications
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-05-23 09:03-0400
Last-Translator: Elkana Bardugo <ttv200@gmail.com>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 הודעה %1 %1 הודעות %1 מתוך %2 %3 משימה %1 פעילה %1 משימות פעילות הגדר אירועי התראות ופעולות... לפני 10 שניות לפני 30 שניות הראה פרטים הסתר פרטים נקה התראות העתק תיקיה אחת %2 מתוך %1 תיקיות קובץ אחד %2 מתוך %1 קבצים %1 מתוך %2 +%1 מידע העבודה נכשלה העבודה הסתיימה אין התראות חדשות אין הודעות או משימות פתח... %1 (מושהה) הראה יישום והתראות מערכת %1 (%2 נותרו) עקוב אחר העברות קבצים ועבודות אחרות השתמש במיקום מותאם אישית להתקראות לפני דקה לפני %1 דקות לפני %1 יום לפני %1 ימים אתמול ממש עכשיו %1: %1: נכשל %1: הסתיים 