��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     D     Z  
   c     n     �     �     �  #   �  $   �     �          1  0   H     y     �     �  $   �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org.kde.plasma.userswitcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2017-05-16 06:56-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 משתמש נוכחי כללי פריסה מסך נעילה הפעלה חדשה לא בשימוש עזוב... הראה את התמונה והשם הראה שם מלא (אם זמין) הראה שם משתמש הראה תמונה בלבד הראה שם בלבד הראה פרטים טכנים על ההפעלה ב־%1 (%2) TTY %1 הצגת שם משתמש אתה מחובר בתור <b>%1</b> 