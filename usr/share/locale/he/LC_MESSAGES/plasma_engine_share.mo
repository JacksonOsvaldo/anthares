��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  M   �  J   E  C   �  )   �  3   �  7   2  !   j     �  .   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-05-16 06:56-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 אין אפשרות לזהות את טיפוס הנתונים של הקובץ אין אפשרות למצוא את כל הפונקציות הנדרשות אין אפשרות למצא את הספק עם היעד הנבחר שגיאה בעת הפעלת התסריט נתיב לא תקני עבור הספק הנבחר אין אפשרות לקרא את הקובץ הנבחר השירות לא היה זמין שגיאה לא מוכרת עליך לספק כתובת לשירות זה 