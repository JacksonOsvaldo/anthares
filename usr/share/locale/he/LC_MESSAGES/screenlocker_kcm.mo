��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �  (   {  
   �  
   �  )   �     �     �  /   �  )   /  (   Y     �     �  *   �     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-05-16 06:58-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Hebrew <kde-i18n-doc@kde.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Zanata 3.9.6
 מסך נעילה בחזרה משינה: הפעלה שגיאה נכשל בבדיקת מסך הנעילה מייד נעל הפעלה נעל את המסך אוטומטית לאחר: מסך נעילה בחזרה מהשהיה דרוש סיסמה לאחר נעילה:  דקה  דקות  שניה  שניות קיצור הדרך לנעילת המסך. סוג רקע: 