��          �            h  �   i  m   �  `   Y     �     �     �     �           #     B     G  ?   X  Y   �  +   �  �    �     e   �  ]   �     V  #   ]  
   �      �     �     �     �     �  9   �  S     $   r         	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-12-24 01:50+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <qt>Háliidatgo duođas váldit eret sievánfáttá <i>%1</i>?<br />Válddát de eret buot fiillat mat gullet dán fáddái.</qt> <qt>It sáhte sihkkut fáttá maid dál geavahat.<br />Ferte vuos válljet muhton eará fáttá.</qt> Gávdno juo fáđđá nammaduvvon %1 govašfáddámáhpas. Hálidatgo buhttet boares fáttá? Nannen Sievánheivehusat leat rievdaduvvon Válddahus Gease dahje čále fáttá-URL:a boerre@skolelinux.no Børre Gaup Namma Buhtte fáttá? Fiila %1 ii oroleamen gustojeaddji sievánfáddávuorká. Ii sáhttán viežžat sievánfáddávuorká. Dárkkis ahte %1-čujuhus lea riekta. Ii gávdnan sievánfáddavuorká %1. 