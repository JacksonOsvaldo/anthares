��    !      $  /   ,      �     �     �     �  
          /     '   O      w  �   �  9   r     �     �  
   �     �     �     �     �  E        J      a     �  `  �     �     �               +     :     P     \     j  E  v  �  �     �
     �
     �
     �
  	   �
  8   �
  0   6  !   g  �   �  =   I     �     �  
   �     �     �     �     �  X   �     ;     X     m  �   v     O     ]     s     |     �     �     �     �     �  *  �                                            
                                      	                !                                                              line  lines  msec  pixel  pixels  pixel/sec &General &Move pointer with keyboard (using the num pad) &Single-click to open files and folders (c) 1997 - 2005 Mouse developers <h1>Mouse</h1> This module allows you to choose various options for the way in which your pointing device works. Your pointing device may be a mouse, trackball, or some other hardware that performs a similar function. Activates and opens a file or folder with a single click. Advanced Bernd Gehrmann Brad Hards Brad Hughes Button Order David Faure Dirk A. Mueller Dou&ble-click to open files and folders (select icons on first click) Double click interval: EMAIL OF TRANSLATORSYour emails Icons If you are left-handed, you may prefer to swap the functions of the left and right buttons on your pointing device by choosing the 'left-handed' option. If your pointing device has more than two buttons, only those that function as the left and right buttons are affected. For example, if you have a three-button mouse, the middle button is unaffected. Le&ft handed Ma&ximum speed: Mouse NAME OF TRANSLATORSYour names Patrick Dowler Pointer acceleration: Ralf Nolden Righ&t handed Rik Hemsley The default behavior in KDE is to select and activate icons with a single click of the left button on your pointing device. This behavior is consistent with what you would expect when you click links in most web browsers. If you would prefer to select with a single click, and activate with a double click, check this option. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-12-24 01:50+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  linnja  linnjá  ms  govvačuokkis  govvačuoggá  govvačuoggát/s &Oppalaš &Sirdde sievána boallobevddiin (numerálaš boaluiguin) &Coahkkal oktii rahpandihte fiillaid ja máhpaid © 1997–2005 Sáhpánovdanahtit <h1>Sáhpán</h1> Dán moduvllas sáhtát heivehit mo du čujuhanovttadat galgá doaibmat. Čujuhanovttadat sáhttá leat sáhpán, «trackball» dahje eará beargasat mas lea seamma doaibma. Aktivere ja rahpá fiilla dahje máhpa ovttain coahkkalemiin. Erenoamáš Bernd Gehrmann Brad Hards Brad Hughes Boalloortnet David Faure Dirk A. Müller &Duppalcoahkkal rahpandihte fiillaid ja máhpaid (vuosttáš coahkkal vállje govažiid) Duppalčoahkkalan gaskkadat: boerre@skolelinux.no Govažat Jus leat gurutgiehtat, de sáhtát molsut doaimmat mat leat olgeš ja gurutboalus jus válljet «gurutgiehtat». Jus cuigunovttadagas leat eanetgo guovtte boalu, de molsošuvvo dušše olgeš ja gurut sáhpánboalut. G&urutgiehtat &Leavttu badjerádji: Sáhpán Børre Gaup Patrick Dowler Sievána lehttohus: Ralf Nolden O&lgešgiehtat Rik Hemsley Standárda láhtten KDE:s lea válljet ja bidját johtui govažiid ovttain coahkkalemiin cuigunovttadaga gurutboaluin. Dát doaibmá seammaláhkaigo eanáš fierbmeloganat. Jus háliidat válljet ovttain coahkkalemiin ja bidjat johtui govažiid guvttiin coahkkalemiin, váldde de eret merkema dás. 