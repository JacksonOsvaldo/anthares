��          �      �        &   	  �   0     �     �     �     �     �     �      �  �     c   �     I     Z     y     �     �     �     �  	   �  C   �  j        m  �  �  %   u  v   �               %     3  	   @     J  &   Y  �   �  w   U	     �	     �	     �	     
     "
     .
     4
     :
  @   J
  s   �
      �
               
                                                                      	                   (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Description: %1 EMAIL OF TRANSLATORSYour emails Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module NAME OF TRANSLATORSYour names No description available. Preview Radio button Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-17 03:28+0200
PO-Revision-Date: 2006-01-10 20:21+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 © 2002 Karol Szwed, Daniel Molkentin <h1>Stiila</h1>Dát moduvla diktá du heivehit geavaheaddjelavtta áđaid fárdda, nugo áđa-stiilla ja -effeavttaid. Boallu Merkenboksa Lotnolasboksa &Heivet … Heivet %1 Válddahus: %1 njutsi@frisurf.no,boerre@skolelinux.no Dás sáhtát válljet ovdagihtii čilgejuvvon ahtahámi listtus (omd. mainna lágiin boalut sárgojuvvojit), maid de sáhtát geavahit oktan fáttain (lássi dieđut nugo márbmorhámis dahje ivdnerievdadeamis). Juos válljet dán vejolašvuođa, bohtet KDE-prográmmat čájehit smávva govažiid muhtin dehalaš boaluid bálddas. KDE stiilamoduvla Nils Johan Utsi,Børre Gaup Ii gávdno válddahus Ovdačájeheapmi Radioboallu Tab 1 Tab 2 Dušše teaksta Šattai meattáhus vieččadettiin dán stiila heivehusláseža. Dás oažžut ovdačájeheami aitto válljejuvvon hámis dárbbaškeahtta dan váldit atnui olles čállinbeavdái. Ii sáhttán viežžat láseža. 