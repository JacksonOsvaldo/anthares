��    	      d      �       �      �   
   a     l  *   x     �  #   �     �     �  �    �        �     �  %   �     �  .     &   3  5   Z              	                                <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-12-10 04:54+0100
Last-Translator: Børre Gaup <boerre@skolelinux.no>
Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>
Language: se
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <h1>Máŋga čállinbeavddi</h1> Dán modulas sáhtát mearridit galle virtuella čállinbeavddi don háliidat ja mearridit daid namaid. Čállinbeavdi %1 Čállinbeavdi %1: Dás čálát %1-čállinbeavddi nama Bláđđe čállinbeavdelisttus Bláđđe čállinbeavdelisttus (maŋosguvlui) Mana čállinbeavddis čállinbeavdái Mana čállinbeavddis čállinbeavdái (maŋosguvlui) 