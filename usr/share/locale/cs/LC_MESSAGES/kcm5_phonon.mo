��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  X   �       !   �  v   �          :  B   O     �  ?   �     �     �           	  7   '  5   _  5   �     �     �  b   �     Y     p  �   �  >     3   G     {     �     �     �     �     �     �       	     "   !     D     S  4   X      �     �  
   �     �     �     �     �                4     B     Q     `  )   u  	   �  	   �     �     �  �   �     �  D   �  �   �     �  @   �  =   �  =     #   V     z     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-27 09:49+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
 Pro aplikování změny podpůrné vrstvy se budete muset odhlásit a znovu přihlásit. Seznam nalezených podpůrných vrstev Phononu ve vašem systému. Toto pořadí určuje pořadí, v jakém je Phonon použije. Použít seznam zařízení na... Aplikovat právě zobrazený seznam upřednostňovaných zařízení na následující kategorie přehrávání zvuku: Nastavení zvukového hardware Přehrávání zvuku Preference zařízení pro přehrávání videa pro kategorii '%1' Nahrávání zvuku Preference zařízení pro nahrávaní zvuku pro kategorii '%1' Implementace Colin Guthrie Konektor Copyright 2006 Matthias Kretz Výchozí pořadí zařízení pro přehrávání zvuku Výchozí pořadí zařízení pro nahrávání zvuku Výchozí pořadí zařízení pro nahrávání videa Výchozí/nezadaná kategorie Nepreferovat Definuje výchozí řazení zařízení, které lze předefinovat v individuálních kategoriích. Nastavení zařízení Pořadí zařízení Vhodná zařízení nalezená ve vašem systému pro danou kategorii. Zvolte zařízení, které má být používáno aplikacemi. koty@seznam.cz, jan.holicek@gmail.com, tomas.chvatal@gmail.com Nepovedlo se nastavit vybrané vybrané zařízení Přední středový  Přední levý  Přední levý středový  Přední pravý Přední pravý středový Hardware Nezávislá zařízení Úrovně vstupu Neplatný Nastavení zvukového hardware KDE Matthias Kretz Mono Klára Cihlářová, Jan Holíček, Tomáš Chvátal Modul nastavení programu Phonon Přehrávání (%1) Preferovat Profil Zadní středový Zadní levý Zadní pravý Nahrávání (%1) Zobrazit pokročilá zařízení Boční levý Boční pravý Zvuková karta Zvukové zařízení Rozmístění reproduktorů a jejich test Subwoofer Otestovat Otestovat vybrané zařízení Testování %1 Toto pořadí představuje prioritu zařízení. Pokud nemůže být z jakéhokoliv důvodu použito zařízení první, Phonon se pokusí použít druhé a tak dále. Neznámý kanál Použít právě zobrazený seznam zařízení pro více kategorií. Různé kategorie užití multimédií. Pro každou kategorii můžete zvolit, jaké zařízení upřednostňujete pro použití v aplikacích užívajících Phonon. Nahrávání videa Preference zařízení pro nahrávání videa pro kategorii '%1' Vaše podpůrná vrstva nemusí podporovat nahrávání zvuku Vaše podpůrná vrstva nemusí podporovat nahrávání videa vybrané zařízení bez preference preferovat vybrané zařízení 