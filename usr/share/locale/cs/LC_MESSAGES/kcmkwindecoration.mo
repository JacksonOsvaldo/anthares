��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p  	   `     j     q     }  
   �  
   �     �     �     �     �     �  
   �     �  V     1   _     �  1   �  (   �     	     	     *	     7	     E	     N	     \	     p	     w	     ~	     �	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-27 15:08+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Obrovská Velká Bez okrajů Bez postranních okrajů Normální Nadměrná Malá Velmi obrovská Velmi velká Nabídka aplikací Velikost okra&je: Tlačítka Zavřít Zavřít dvojklikem:
 Pro otevření nabídky, podržte tlačítko dokud se neobjeví. Zavřít okno dvojklikem na &tlačítko nabídky: Kontextová nápověda Přetáhněte tlačítka odsud na titulkový pruh Pro odstranění upusťte tlačítko zde Získat nové dekorace... Podržet nad Ponechat pod Maximalizovat Nabídka Minimalizovat Na všech plochách Hledat Sbalit Motiv Titulkový pruh 