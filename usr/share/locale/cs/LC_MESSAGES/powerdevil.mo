��    D      <  a   \      �     �  +   �  ;     9   N  (   �  ;   �  9   �  (   '  &   P  8   w  $   �     �     �     �  /         2     S  	   o  ,   y     �  S   �     
	  N   	  
   i	      t	  n   �	     
  	   
     "
  �   1
  $   �
     �
     �
     �
  !        /  7   N  e   �     �     �       
   $     /     C     \  	   c     m     u  D   �  &   �  %   �  f     8     ;   �     �     �               0     G     a  �   g  "   �  C     J   [  N   �  M   �  �  C     ,     1     7     P  
   g     r     �     �     �  "   �     �     �     �       4     +   C  )   o  	   �     �     �     �     �  U   �     9  /   F  %   v     �  
   �     �  �   �  5   v     �     �     �  /   �  +        D  v   W     �     �     �          )     =     Q     X     `  
   f  5   q     �     �  V   �  /   <  .   l     �     �     �     �     �     �       �     %   �  D   �  O     T   c  V   �     #          4          	       *      :                  0   2          &   A       +              !      @      9   B       5   8            $             ?                   ,              =       (   %          1           D                    "       /   C      7          6           3         )         <   -   >      
          '   ;   .               min %1 is vendor name, %2 is product name%1 %2 @action:inmenu Global shortcutDecrease Keyboard Brightness @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Keyboard Brightness @action:inmenu Global shortcutIncrease Screen Brightness @action:inmenu Global shortcutPower Off @action:inmenu Global shortcutSuspend @action:inmenu Global shortcutToggle Keyboard Backlight @label:slider Brightness levelLevel AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Battery Critical (%1% Remaining) Battery Low (%1% Remaining) Bluetooth Brightness level, label for the sliderLevel Can't open file Cancel timeout that will automatically suspend system because of low batteryCancel Charge Complete Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Execute action on lid close even when external monitor is connectedEven when an external monitor is connected Extra Battery Added Hibernate Hybrid suspend KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Keyboard Battery Low (%1% Remaining) Leave unchanged Lock screen Mobile broadband Mouse Battery Low (%1% Remaining) NAME OF TRANSLATORSYour names Name for powerdevil shortcuts categoryPower Management No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Running on AC power Running on Battery Power Script Shut down Suspend Switch off after The battery in an external deviceDevice Battery Low (%1% Remaining) The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off Turn off screen Turn on Unsupported suspend method When laptop lid closed When power button pressed Wi-Fi Your battery is low. If you need to continue using your computer, either plug in your computer, or shut it down and then change the battery. Your battery is now fully charged. Your battery level is critical, save your work as soon as possible. Your battery level is critical, the computer will be halted in 60 seconds. Your battery level is critical, the computer will be hibernated in 60 seconds. Your battery level is critical, the computer will be suspended in 60 seconds. Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2017-03-13 12:59+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
  min %1 %2 Snížit jas klávesnice Snížit jas obrazovky Hibernovat Zvýšit jas klávesnice Zvýšit jas obrazovky Vypnout Uspat Přepnout podsvícení klávesnice Úroveň Síťový adaptér je zapojen Správce aktivit Po Všechny probíhající akce uspání byly zrušeny. Baterie je v kritickém stavu (zbývá %1%) Baterie je téměř vybitá (zbývá %1%) Bluetooth Úroveň Nelze otevřít soubor Zrušit Nabití bylo dokončeno Nepovedlo se připojit k rozhraní baterie.
Prosím, zkontrolujte nastavení systému Nedělat nic vit@pelcak.org,mkyral@email.cz,david@kolibac.cz I když je připojen externí monitor Přidána extra baterie Hibernovat Hybridní uspání Systém správy napájení KDE nemůže být inicializován. Podpůrná vrstva nahlásila následující chybu: %1
Zkontrolujte prosím konfiguraci svého systému Baterie klávesnice je téměř vybitá (zbývá %1%) Ponechat nezměněné Zamknout obrazovku Mobilní Broadband Baterie myši je téměř vybitá (zbývá %1%) Vít Pelčák,Marián Kyral,David Kolibáč Správa napájení Není dostupný vhodný zásuvný modul implementace správy napájení. Toto může být vyřešeno novou instalací. Při načtení profilu Při ukončení profilu Vyvolat odhlašovací dialog Spustit skript Napájeno ze sítě Napájeno z baterie Skript Vypnout Uspat Spustit po Baterie zařízení je téměř vybitá (zbývá %1%) Zdroj napájení byl připojen. Zdroj napájení byl odpojen. Byl vybrán neexistující profil "%1".
Prosím, zkontrolujte nastavení v PowerDevil. Tyto zásady aktivit brání vypnutí obrazovky Tyto zásady aktivit brání uspání systému Vypnout Vypnout monitor Zapnout Nepodporovaná metoda uspání Když je laptop zavřen Pokud je stisknut spínač Wi-Fi Baterie je téměř vybitá. Pokud potřebujete pokračovat v používání počítače, zapojte jej do zásuvky nebo ho vypněte a nabijte baterii. Vaše baterie je nyní plně nabitá. Baterie dosáhla kritické úrovně. Uložte okamžitě svou práci. Vaše baterie dosáhla kritické úrovně. Počítač bude vypnut za 60 sekund. Vaše baterie dosáhla kritické úrovně. Počítač bude hibernován za 60 sekund. Vaše baterie dosáhla kritické úrovně. Počítač bude uspán do RAM za 60 sekund. 