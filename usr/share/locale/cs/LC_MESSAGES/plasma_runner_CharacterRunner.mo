��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     G     \     m     s     z     �  O   �     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2011-06-10 09:40+0200
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Spouš&těcí slovo: Přidat položku Alias Alias: Nastavení spouštěče znaků Kód Vytvoří znaky z :q:, pokud je to hexadecimální kód nebo definovaný alias. Smazat položku Šestnáctkový kód: 