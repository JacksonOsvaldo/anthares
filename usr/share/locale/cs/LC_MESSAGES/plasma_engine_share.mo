��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <     �  )     4   6  (   k  ,   �  '   �     �       "            	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-09 11:14+0200
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nelze zjistit MIME typ souboru Nelze najít všechny požadované funkce U zvoleného cíle nelze nelze nalézt poskytovatele Chyba při pokusu os spuštění skriptu Neplatná cesta pro zvoleného poskytovatele Zvolený soubor se nepovedlo přečíst Služba nebyla dostupná Neznámá chyba Pro tuto službu musíte zadat URL 