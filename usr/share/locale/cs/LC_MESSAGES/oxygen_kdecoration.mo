��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j      *  	   K     U     \     h  
   �  
   �     �     �     �     �  
   �     �     �     �     �  6   �     5     =     S     d  
   �     �  	   �  T   �  F        O     b     |     �  $   �     �     �     �     �                !     2  "   D     g     �  ,   �  %   �  	   �     �                    2  @   ;  &   |     �     �     �     �                *  ,   7     d         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-02-24 09:55+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Odpovídající vlastnost ok&na: Obrovská Velká Bez okrajů Bez postranních okrajů Normální Nadměrná Malá Velmi obrovská Velmi velká Velká Normální Malá Velmi velká Záře aktivního okna Přidat Přidat úchytku pro změnu velikosti oken bez okrajů Animace Velikost &tlačítka: Velikost okraje: Přechod při přejezdu myší Uprostřed Na střed (plná šířka) Třída:  Nastavit přechod mezi stínem a září okna, pokud se změní stav aktivního okna Nastavit zvýrazňující animaci při přejezdu přes tlačítko okna Možnosti dekorace Detekovat vlastnosti okna Dialog Upravit Upravit výjimku - Nastavení Oxygen Povolit/zakázat tuto výjimku Typ výjimky Obecné Skrýt titulkový pruh okna Informace o vybraném oknu Vlevo Přesunout dolů Přesunout nahoru Nová výjimka - Nastavení Oxygen Otázka - Nastavení Oxygen Regulární výraz Syntaxe regulárního výrazu je nesprávná Re&gulární výraz pro porovnání:  Odstranit Odebrat zvolenou výjimku? Vpravo Stíny Zarovnání titu&lku: Název:  Používat stejné barvy pro pruh s titulkem jako pro obsah okna Použít třídu okna (celá aplikace) Použít titulek okna Varování - Nastavení Oxygen Název třídy okna Stín vrhaný oknem Identifikace okna Výběr vlastnosti okna Titulek okna Přechody při změně aktivního stavu okna Výjimky pro specifická okna 