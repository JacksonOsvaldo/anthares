��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     `     h     l  	   �  �   �  ;     	   O  &   Y  )   �     �     �     �  '   �     	     (	     8	     F	     O	     o	     w	     �	  .   �	  0   �	      
     
     %
     :
  
   L
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-12 10:28+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Činnost: Aplikace se pokouší provádět činnost vyžadující privilegia. K provedení této činnosti je potřeba ověření oprávnění. Už se ověřuje jiný klient. Prosím, zkuste to později. Aplikace: Je vyžadováno ověření totožnosti Selhalo ověření, prosím zkuste znova. Klikněte pro úpravy %1 Klikněte pro otevření %1 Podrobnosti vit@pelcak.org, tomas.chvatal@gmail.com Předchozí správce Jaroslav Reznik Lukáš Tinkl Správce Vít Pelčák, Tomáš Chvátal &Heslo: Heslo pro %1: Heslo pro superuživatele: Ověřit heslo nebo otisk prstu uživatele: %1 Ověřit heslo nebo otisk prstu uživatele root: Heslo nebo přejeďte prstem: Heslo: PolicyKit1 Agent KDE Vybrat uživatele Dodavatel: 