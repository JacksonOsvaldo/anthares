��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;       '   4     \  	   d  
   n     y     �  
   �  	   �     �  .   �     �     �  0   
     ;     A     V     k        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-02-24 10:56+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Přejete si uspat do RAM? Přejete si uspat na disk (hibernovat)? Obecné Činnosti Hibernovat Hibernovat (Uspat na disk) Opustit Opustit... Uzamknout Uzamknout obrazovku Odhlásit, vypnout nebo restartovat počítač Ne Uspat (do paměti) Spustit souběžné sezení jako jiný uživatel Uspat Přepnout uživatele Přepnout uživatele Ano 