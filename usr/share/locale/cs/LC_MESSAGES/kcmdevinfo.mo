��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  9   �       	   ,     6     ?     ]     e     s          �     �     �     �     �     �     �  ?   �     ;     T     `     l     {     �     �     �     �     �     �     �     �  6     	   >  	   H  
   R  
   ]  
   h     s          �     �     �     �     �     �                          8     E     Q     d     h  
   �     �     �  	   �  	   �     �     �     �     �     �     �           6     K     X      q     �     �     �     �     �  P   �       
         +     7     I     ]  
   a  	   l     v       	   �  	   �  	   �  	   �  	   �  	   �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-15 15:29+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Modul pro prohlížení zařízení postavený KDE Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 volné z %2 (%3% použito) Baterie Typ baterie:  Sběrnice:  Fotoaparát Fotoaparáty Stav nabití:  Nabíjí se Svinout vše Čtečka Compact Flash Zařízení Informace o zařízení Zobrazí všechna zařízení, která jsou aktuálně vypsána. Prohlížeč zařízení Zařízení Vybíjí se vit@pelcak.org Zašifrováno Rozvinout vše Souborový systém Typ souborového systému:  Plně nabitá Pevný disk Připojitelné za běhu? IDE IEEE1394 Zobrazí informace o aktuálně vybraném zařízení. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Klávesnice Klávesnice a myš Popisek  Maximální rychlost:  Čtečka karet Memory Stick Připojeno v:  Myš Multimediální přehrávače Vít Pelčák Ne Vybitá Nejsou dostupná žádná data Nepřipojeno Nenastaveno Optická mechanika PDA Tabulka diskových oddílů Primární Procesor %1 Číslo procesoru:  Procesory Produkt:  RAID Vyměnitelné? SATA SCSI Čtečka karet SD/MMC Zobrazit všechna zařízení Zobrazit relevantní zařízení Čtečka Smart Media Úložiště Podporované ovladače:  Podporované instrukční sady:  Podporované protokoly:  UDI:  UPS USB UUID:  Zobrazuje UDI (jedinečný identifikátor zařízení) aktuálního zařízení. Neznámý disk Nepoužito Dodavatel:  Velikost svazku:  Zaplnění svazku:  Ano kcmdevinfo Neznámé Žádné Žádné Platforma Neznámé Neznámé Neznámé Neznámé Neznámé Čtečka karet xD 