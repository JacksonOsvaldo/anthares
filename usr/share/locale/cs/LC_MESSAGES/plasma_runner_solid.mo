��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     _     r  0   �  V   �  N     Q   Z  M   �  _   �  ]   Z	     �	     �	     �	     �	  	   �	  	   �	      
     
     
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-12 10:46+0200
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
 Zamknout mechaniku Vysunout médium Najde zařízení s názvem odpovídajícím :q: Vypíše všechna zařízení a umožní jejich připojení, vypojení nebo zasunutí. Vypíše všechna zařízení, která lze vysunout, umožní jejich vysunutí. Vypíše všechna zařízení, která lze připojit, umožní jejich připojení. Vypíše všechna zařízení, která lze odpojit, umožní jejich odpojení. Vypíše všechna zašifrovaná zařízení, která lze uzamknout a umožní jejich uzamčení. Vypíše všechna šifrovaná zařízení, která lze odemknout a umožní jejich odemčení. Připojit zařízení zařízení vysunout zamknout připojit odemknout odpojit Odemknout mechaniku Odpojit zařízení 