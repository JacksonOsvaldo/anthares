��    D      <  a   \      �  
   �  
   �  
   �                    )     8     >  	   M     W  	   _     i     u     |  
   �     �     �  3   �  	   �     �     �     �                     '     6     F     M  
   _     j  ?   }     �     �     �     �     �     �     �                     -     0     @     M     S     `  	   l     v     |     �     �  b   �     	     	  	   	     %	     6	  
   F	  	   Q	     [	     k	     s	     y	     �	  �  �	     {     �     �     �     �     �     �     �     �     �               +     <     B  	   J     T     [     d     f     o     �     �     �     �     �     �     �     �     �  	          C   &     j     w     }     �     �     �     �     �     �     �     �               $     +     =  	   M     W     ]     o  	   �  W   �     �     �               (     6     B     I     U     ]     f     �     *       C   A   3       %       B           
   :      )   8           !   D      .   "                ?       >   ;                         9                  @                     &           /         0   1                                ,   '       5   =         <              #   -   $   +              (       7       2   4   	                  6    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-11 13:22+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Aktivity Přidat činnost Přidat mezeru Přidat widgety... Alt Alternativní widgety Vždy viditelné Použít Provést nastavení Aplikovat nyní Autor: Automaticky skrývat Tlačítko zpět Dolů Zrušit Kategorie Střed Zavřít + Nastavit Nastavit aktivitu Vytvořit aktivitu... Ctrl V současnosti používaný Smazat E-mail: Tlačítko vpřed Získat nové widgety Výška Vodorovný posuv Vstup zde Klávesové zkratky Změny rozložení musí být aplikovány před ostatními změnami Rozvržení: Vlevo Levé tlačítko Licence: Uzamknout widgety Maximalizovat panel Meta Prostřední tlačítko Více nastavení... Činnosti myši OK Zarovnání panelu Odstranit panel Vpravo Pravé tlačítko Okraj obrazovky Hledat... Shift Zastavit aktivitu Zastavené aktivity: Přepnout V aktivním modulu jsou neuložené změny. Přejete si změny aplikovat, nebo zahodit? Nahoru Vrátit odinstalaci zpět Odinstalovat Odinstalovat widget Svislý posuv Viditelnost Tapeta Typ tapety: Widgety Šířka Může být překryt okny Okna jdou pod 