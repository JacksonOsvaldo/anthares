��    *      l  ;   �      �  �  �      �  
   �     �     �     �  <   �       	   *     4     G     ]     d     z     �     �     �     �     �     �  9   �          -     5     A     N     S  	   j     t  B   }  &   �  ?   �  '   '  )   O  5   y  +   �     �     �     	  9   	  C   N	  �  �	  �  C          1     :     M     m  C   u     �     �     �     �  	   �     	     !     '     E     a     h     n     �  >   �     �     �     �               &     E     L  J   X  )   �  5   �  !         %  .   F  &   u     �     �     �  B   �                             !   
                                  '   #                 	              (       &                          *   $          %                   )                                  "    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> @title:windowCreate a New Vault Activities Backend: Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Close the vault Configure Configure Vault... Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog EncFS Encrypted data location Failed to execute Failed to fetch the list of applications using this vault Forcefully close  General Mount point Mount point: Next Open with File Manager Password: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unknown device Vaul&t name: Wrong version installed. The required version is %1.%2.%3 formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-11-16 17:06+0100
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> Vytvořit nový sejf Aktivity Podpůrná vrstva: Nelze otveřít neznámý sejf. Změnit Vyberte šifrovací systém, který chcete použít pro tento sejf: Zavřít sejf Nastavit Nastavit sejf... Byla nalezena správná verze Vytvořit Vytvořit nový sejf... CryFS Zařízení je již otevřeno Zařízení není otevřeno Dialog EncFS Umístění šifrovaných dat Nelze spustit Selhalo získávání seznamu aplikací používajících sejf Vynutit zavření Obecné Přípojný bod Místo připojení: Následující Otevřít ve správci souborů Heslo: Předchozí Adresář přípojného bodu není prázdný. Otevření sejfu zamítnuto. Zadaná podpůrná vrstva není dostupná Nastavení sejfu lze změnit pouze pokud je uzavřen. Sejf není znám. Nelze zavřít. Sejf není znám. Nelze zničit. Sejf nejde zavřít. Je používán aplikací. Sejf nejde zavřít. Používá jej %1 Verzi nelze zjistit Neznámé zařízení Název sej&fu: Byla nainstalována špatná verze. Požadovaná verze je %1.%2.%3 %1: %2 