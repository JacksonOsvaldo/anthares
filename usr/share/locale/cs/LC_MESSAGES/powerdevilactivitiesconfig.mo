��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �      �  #        1  
   @     K     Y  +   t      �      �  0   �            �   !  p   �  z   	     �	  >   �	     �	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2014-10-22 11:51+0200
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  min Chovat se jako Vždy Definice speciálního chování Nepoužívat speciální nastavení vit@pelcak.org Hibernovat Vít Pelčák Nikdy nevypínat obrazovku Nikdy neuspávat nebo nevypínat počítač Počítač je napájen ze sítě Počítač je napájen z baterie Počítač je napájen téměř vybitou baterií Vypnout Uspat Zdá se, že servis správy napájení neběží.
Toto může být vyřešeno spuštěním nebo načasováním ve "Spuštění a ukončení" Servis aktivit neběží.
Bez tohoto servisu nelze měnit chování správy napájení pro jednotlivé aktivity. Běžící servis aktivit má spuštěny pouze nejzákladnější funkce.
Jména a ikony aktivit nemusejí být dostupná. Aktivita "%1" Použít oddělené nastavení (pouze pokročilí uživatelé) po 