��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �  
   �     �     �     �     �     �     �     �     
  	   *     4     P     j     �     �     �  ?   �       E     
   c     n     �  O   �     �  !   �  �       �     �     �     �     �     �                "  8   1     j          �     �     �     �     �     �     �               +     @     R     o     �     �     �     �     �            O   -     }  C   �            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-09-11 13:57+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
  [skryté] &Komentář: &Smazat &Popis: Ú&pravy &Soubor &Název: &Nová podnabídka... Spustit pod &jiným uživatelem &Seřadit &Seřadit vše podle popisu &Seřadit vše dle názvu &Seřadit výběr podle popisu &Seřadit výběr podle názvu &Uživatelské jméno: &Pracovní cesta: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Pokročilé Všechny podnabídky '%1' budou odstraněny. Přejete si pokračovat? Pří&kaz: Nelze zapsat do %1 Současná k&lávesa: Přejete si obnovit systémovou nabídku? Tímto odstraníte vlastní nabídky. lukas@kde.org Povo&lit odezvu při spouštění Za příkazem můžete použít několik zástupných znaků, které budou nahrazeny skutečnými hodnotami při spuštění programu:
%f - název souboru
%F - seznam souborů; použijte pro aplikace, které umí otevřít několik lokálních souborů naráz.
%u - jedno URL
%U - seznam URL
%d - složka souboru k otevření
%D - seznam složek
%i - ikona
%m - mini ikona
%c - komentář Obecné Obecné možnosti Skrytá položka Název položky: Editor nabídky KDE Editor nabídky KDE Hlavní panel nástrojů Správce Matthias Elter Změny v nabídce nelze uložit kvůli tomuto problému: Předvybrat nabídku Montel Laurent Posunout &dolů Posunout nahor&u Lukáš Tinkl Nová &položka... Nová položka Nový o&ddělovač Nová podnabídka Zobrazovat pouze v KDE Původní autor Předchozí správce Raffaele Sandrini Vrátit systémovou nabídku Spustit v term&inálu Uložit změny nabídky? Zobrazovat skryté položky Kontrola pravopisu Možnosti kontroly pravopisu Předvybrat podnabídku Název podnabídky: M&ožnosti terminálu: Nelze kontaktovat khotkeys. vaše změny byly uloženy, ale nelze je aktivovat. Waldo Bastian Změnili jste nabídku.
Přejete si změny uložit nebo zapomenout? 