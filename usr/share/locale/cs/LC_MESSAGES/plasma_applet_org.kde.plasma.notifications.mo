��    $      <  5   \      0      1     R     ^  -   }  #   �  #   �  ?   �  1   3     e     y  H   ~  L   �       V     N   s     �  
   �     �     �     �       2     
   P     [  )   {  8   �  #   �  .     -   1  D   _  6   �  0   �  A     =   N  9   �  �  �  )   z
  
   �
  ?   �
  1   �
  
   !  
   ,     7     L     _  
   r     }  *   �     �     �     �  	   �     �     �          &     F     S     d     p  ,   �     �  )   �  @   �  )   7      a     �     �     �     �     �                                                
       "                                       $                                     #      	           !                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-09-07 12:27+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1 oznámení %1 oznámení %1 oznámení %1 z %2 %3 %1 běžící úloha %1 běžící úlohy %1 běžících úloh Nastavit upozornění na události a činnosti... před 10 s před 30 s Zobrazit podrobnosti Skrýt podrobnosti Vymazat oznámení Kopírovat 1 adr %2 z %1 adr %2 z %1 adr 1 soubor %2 z %1 souborů %2 z %1 souborů Historie %1 z %2 +%1 Informace Úloha selhala Úloha byla dokončena Žádná nová oznámení. Žádná oznámení nebo úlohy Otevřít... %1 (Pozastaveno) Vybrat vše Zobrazit historii upozornění Zobrazit oznamování systému a a aplikací %1 (%2 zbývá) Sledovat přenosy souborů a jiné úlohy Použít vlastní umístění pro vyskakovací okno oznamování Před minutou Před %1 min %Před minutou včera před %1 dny před %1 dny Včera Právě teď %1: %1: Selhalo %1: Dokončeno 