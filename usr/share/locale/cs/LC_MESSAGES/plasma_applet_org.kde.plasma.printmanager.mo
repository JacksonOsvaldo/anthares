��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �     	          *  0   :  <   k     �     �     �  	   �       ]           	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2016-01-21 13:14+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Nastavit tis&kárny... Pouze aktivní úlohy Všechny úlohy Pouze dokončené úlohy Nastavit tiskárnu Obecné Žádné aktivní úlohy Žádné úlohy Nebyly nalezeny či nastaveny žádné tiskárny Jedna aktivní úloha %1 aktivní úlohy %1 aktivních úloh Jedna úloha %1 úlohy %1 úloh Otevřít frontu tisku Fronta tisku je prázdná Tiskárny Hledat tiskárnu... Ve frontě tisku je jedna úloha Ve frontě tisku jsou %1 úlohy Ve frontě tisku je %1 úloh 