��            )   �      �     �     �     �     �     �     �     �     �  0   �     #     7     M     S     _     g     |  
   �     �     �     �     �     �     �               1     9     Q  �  W     J     S     k     ~     �     �     �     �  0   �     �          #     )     ;     A  !   `     �     �     �     �     �     �     �          %     >     E     [                                            
                                                                                      	        %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-01 12:28+0100
Last-Translator: Vit Pelcak <vpelcak@suse.cz>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 %1 od %2 Přidat vlastní tapetu Přidat složku... Přidat obrázek... Pozadí: Rozostření Vystředěná Změnit každých: Adresář s tapetami pro zobrazení promítání Stáhnout tapety Získat nové tapety... Hodin Soubory obrázků Minut Následující obrázek tapety Otevřít odpovídající složku Otevřít obrázek Otevřít obrázek s tapetou Umístění: Doporučovaný soubor s tapetou Odstranit tapetu Obnovit tapetu Zvětšená Zvětšená a oříznutá Zvětšená s proporcemi Sekund Vyberte barvu pozadí Vydlážděná 