��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �               #     ,     2     F     Z     l     �     �     �  +   �     �      �     	     	     !	     >	     Q	  	   `	     j	     r	     �	     �	  {   �	  L   +
     x
     �
     �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-23 19:46+0200
Last-Translator: Lukáš Tinkl <lukas@kde.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.4
 Aktivity Všechny ostatní aktivity Všechny ostatní plochy Všechny ostatní obrazovky Všechna okna Alternativní Plocha 1 Obsah Současná aktivita Současná aplikace Současná plocha Současná obrazovka Filtrovat okna podle Nastavení pravidel  Vpřed Získat nové rozvržení přepínače oken Skrytá okna Zahrnout ikonu "Zobrazit plochu" Hlavní Minimalizace Pouze jedno okno na aplikaci Nedávno použité Obrátit směr Obrazovky Zkratky Zobrazit zvolené okno Pořadí řazení: Pořadí vrstvení Vybrané okno bude zvýrazněno zeslabením ostatních oken. Toto nastavení vyžaduje, aby efekty plochy byly aktivovány. Efekt nahrazující seznam oken pokud jsou aktivní efekty pracovní plochy. Virtuální plochy Viditelná okna Vizualizace 