��    !      $  /   ,      �     �     �  )        ,  $   G  &   l  #   �  !   �  "   �     �               2     J     ]     {     �     �      �     �  
   �     �     �          	       "        :  <   Y     �  ;   �     �  �       �     �     �     �  %   �  #     &   9  '   `  )   �     �  
   �     �     �     �     �     	  	   	     	     0	     ?	     F	     S	     a	     t	     }	     �	     �	     �	  ?   �	     �	     �	     �	     !                                                                             	                                                                  
           100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Advanced Output Configuration Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-04 15:19+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 100% Autor Copyright 2015 Harald Sitter Harald Sitter Žádná aplikace nepřehrává audio Žádná aplikace nenahrává audio Žádné dostupné profily zařízení Žádná dostupná vstupní zařízení Žádná dostupná výstupní zařízení Profil: PulseAudio Pokročilé Aplikace Zařízení Pokročilá nastavení výstupu Zachytávat Výchozí Profily zařízení vit@pelcak.org Vstupy Ztlumit zvuk Vít Pelčák Zvuky oznamování Výstupy Přehrávání Port (nedostupné)  (nepřipojen) Tento modul umožňuje nastavit zvukový podsystém Pulseaudio. %1: %2 100% %1% 