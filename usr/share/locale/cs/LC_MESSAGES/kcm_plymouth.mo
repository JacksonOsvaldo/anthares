��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     L  )   e  $   �  "   �     �  -   �          4     R     f     s  6   �     �  8   �  A   	     P	  <   e	     �	  h   �	  '   )
     Q
     ^
  )   r
  (   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-02-22 12:29+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Initramfs nelze spustit. update-alternatives se nepovedlo spustit. Nastavit úvodní obrazovku Plymouth Stáhnout nové úvodní obrazovky vit@pelcak.org Získat nové motivy startovací obrazovky... Initramfs se nepovedlo spustit. Initramfs navrátil chybu %1. Nainstalovat motiv. Marco Martin Vít Pelčák V pomocných parametrech nebylo určen žádný motiv. Instalátor motivů Plymouth Vyberte globální úvodní obrazovku pro váš systém. Motiv pro instalací musí být existující archivovaný soubor. Motiv %1 neexistuje. Motiv je poškozen: soubor .plymouth v motivu nebyl nalezen. Složka motivu %1 neexistuje. Tento modul umožňuje nastavit vzhled celé pracovní plochy pomocí několika připravených předloh. Nelze ověřit/vykonat činnost: %1, %2 Odinstalovat Odinstalovat motiv. update-alternatives se nepovedlo spustit. update-alternatives navrátilo chybu %1. 