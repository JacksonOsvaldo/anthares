��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     '     <     D     P     d  
   r  
   }     �  *   �     �     �       )     
   B     M  !   T  '   v                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-11 15:27+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Aktuální uživatel Obecné Rozvržení Uzamknout obrazovku Nové sezení Nepoužito Opustit... Zobrazit avatara i jméno Zobrazit celé jméno (pokud je dostupné) Zobrazit uživatelské jméno Zobrazit pouze avatara Zobrazit pouze název Zobrazit technické informace o sezeních na %1 (%2) TTY %1 Zobrazení uživatelského jména Nyní jste přihlášeni jako <b>%1</b> 