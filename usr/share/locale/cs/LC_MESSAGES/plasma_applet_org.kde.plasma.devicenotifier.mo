��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  d   A  	   �     �     �  :   �     	  >   '	     f	  �   n	  �   
  )   �
  !   �
     
      )  !   J  A   l     �     �  $   �  '                                                      	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-21 16:22+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 1 činnost pro toto zařízení %1 činnosti pro toto zařízení %1 činností pro toto zařízení %1 volné Přístup... Všechna zařízení Klikněte pro přístup k zařízení z jiných aplikací. Kliknutím vysunete disk. Klikněte pro bezpečné odebrání zařízení z počítače. Obecné Momentálně <b>není bezpečné</b> odebrat toto zařízení: aplikace jej mohou stále používat. Kliknutím na tlačítko Vysunout jej bezpečně odeberete. Momentálně <b>není bezpečné</b> odebrat toto zařízení: aplikace mohou stále přistupovat k jeho svazkům. Kliknutím na tlačítko Vysunout jej bezpečně odeberete. Nyní lze bezpečné zařízení odebrat. Naposledy připojené zařízení Žádná dostupná zařízení Pouze neodpojitelná zařízení Nastavit odpojitelná zařízení Otevřít vyskakovací okno pokud je připojeno nové zařízení Pouze odpojitelná zařízení Odpojování... Toto zařízení je nyní dostupné. Toto zařízení nyní není dostupné. 