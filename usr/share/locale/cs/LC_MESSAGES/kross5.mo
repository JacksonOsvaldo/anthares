��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     n     t     �     �     �     �  
   �     �     �     �     	     	  
   8	     C	  0   \	  A   �	  #   �	  .   �	     "
     *
     1
  (   =
     f
     �
     �
     �
  	   �
     �
     �
  $   �
       )        >  5   D     z                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-04 13:49+0200
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
X-Language: cs_CZ
X-Source-Language: en_US
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer Skript ke spuštění. Obecné Přidat nový skript. Přidat... Zrušit? Komentář: vit@pelcak.org,mkyral@email.cz Úpravy Upravit zvolený skript. Upravit... Vykonat zvolený skript. Vytvoření skriptu pro interpreter "%1" selhalo Pokus o určení interpreteru pro skriptovací soubor "%1% selhal Selhalo načtení interpreteru "%1" Otevření skriptovacího souboru "%1" selhalo Soubor: Ikona: Interpretr: Úroveň zabezpečení Ruby interpreteru Vít Pelčák, Marián Kyral Jméno: Funkce "%1" neexistuje Nenalezen interpreter "%1" Odstranit Odstranit zvolený skript. Spustit Skriptovací soubor "%1" neexistuje. Stop Pozastavit vykonání zvoleného skriptu. Text: Příkazový řádek pro spouštění skriptů Kross. Kross 