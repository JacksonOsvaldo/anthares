��    1      �  C   ,      8     9     =     B  .   Q  5   �     �     �     �     �  	   �     �               &  1   :     l     r          �  
   �     �  '   �     �     �     �            '        @     O     V     ^  5   g     �     �     �     �     �  $   �  A   �  �   @     &     -  C   >  '   �     �     �     �  �  �     �
     �
     �
     �
     �
               .     7     J  "   S     v     �     �  L   �            #   (     L     Y  !   p  
   �     �     �  (   �     �     �  N   �     >  	   O     Y     e  @   m     �     �  	   �     �     �  	   �       ,     	   B     L     _     x          �     �               #   /       -                      $                +         (   1          .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-10-06 13:44+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 %1% Zpět Baterie je na %1% Přepnout rozložení Virtuální klávesnice Zrušit CapsLock je zapnut Zavřít Zavřít hledání Nastavit Nastavit moduly pro vyhledávání Sezení plochy: %1 Jiný uživatel Rozvržení klávesnice: %1 Odhlášení za sekundu Odhlášení za %1 sekundy Odhlášení za %1 sekund Přihlášení Přihlášení selhalo Přihlásit se jako jiný uživatel Odhlásit se Následující skladba Nepřehrává se žádné médium Nepoužito OK Heslo Přehrát nebo pozastavit přehrávání Předchozí skladba Restartovat Restartovat za 1 vteřinu Restartovat za %1 vteřiny Restartovat za %1 vteřin Nedávné dotazy Odstranit Restartovat Vypnout Vypnutí za sekundu Vypnutí za %1 sekundy Vypnutí za %1 sekund Začít nové sezení Uspat Přepnout Přepnout sezení  Přepnout uživatele Hledat... Hledat '%1'... Plasma byla vytvořena v rámci projektu KDE Odemknout Odemčení selhalo na TTY %1 (obrazovka %2) TTY %1 Uživatelské jméno %1 (%2) v kategorii nedávné dotazy 