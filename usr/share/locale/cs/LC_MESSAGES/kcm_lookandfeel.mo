��          |      �             !     A      Y     z  
   �     �     �     �  ]   �     -  :   K  �  �     :     W     s     �     �     �     �     �  h   �  .   9  9   h               
          	                                  Configure Look and Feel details Cursor Settings Changed EMAIL OF TRANSLATORSYour emails Get New Looks... Maintainer Marco Martin NAME OF TRANSLATORSYour names Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-02-22 14:19+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Nastavte podrobnosti vzhledu Změna v nastavení kurzoru vit@pelcak.org Získat nové vzhledy... Správce Marco Martin Vít Pelčák Zobrazit náhled Tento modul umožňuje nastavit vzhled celé pracovní plochy pomocí několika připravených předloh. Použít rozvržení pracovní plochy z motivu Změna kurzoru se projeví až po novém spuštění KDE. 