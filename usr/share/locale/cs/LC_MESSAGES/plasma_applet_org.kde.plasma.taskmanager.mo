��    F      L  a   |                           "     (     5  .   <  U   k     �     �      �     	          $     -  
   9     D     T  $   j     �     �     �     �     �     �  	   �     �  
                  2  	   8     B  !   K     m     ~  	   �      �     �     �     �     �      	     	     	     	     *	     B	  (   S	  =   |	     �	     �	  "   �	     
     (
  )   0
  (   Z
  '   �
  "   �
     �
     �
     �
     �
     �
          %  N   ;  9   �  J   �  �         	        &     :     F  	   ^     h     o     �     �     �     �     �  	   �     �               .  (   I     r          �     �     �     �     �     �     �               8     ?     N  (   V          �     �  '   �     �      �          ,     B     J     Y     _     u  )   �  *   �     �  
   �     �               (  +   :  )   f  ,   �  %   �     �                  	   ,     6     ?     H     d     t     %   =                  $      F              1   4   3          &   9          2   C   *               @   6      (       8   )      A      ,       +          -       7   E   !   5      D   "                     
   '   ;                                   :              ?   0         B   	      /          .             <   #                >              &All Desktops &Close &Fullscreen &Move &New Desktop &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-03-27 17:11+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Všechny pr&acovní plochy &Zavřít Na celou o&brazovku Pře&sunout &Nová pracovní plocha Z&asunout &%1 %2 Také dostupné na %1 Přidat do současné aktivity Všechny aktivity Povolit programu seskupování Podle abecedy Uspořádání Chování Podle aktivity Podle plochy Podle jména programu Zavřít okno nebo skupinu Procházet úlohy pomocí kolečka myši Neseskupovat Neseřazovat Filtry Zapomenout nedávné dokumenty Obecné Seskupování s seřazování Seskupovaní: Zvýraznit okna Velikost ikony: Podržet n&ad ostatními Podržet pod ostatní&mi Velký Ma&ximalizovat Ručně Označte aplikace přehrávající hudbu Sloupců maximálně: Řádků maximálně: Mi&nimalizovat Minimalizovat/Vrátit okno nebo skupinu Další činnosti Přesunou&t na současnou plochu Přesunout do aktivity Přesunout na &plochu Ztlumit Nová instance Na %1 Ve všech aktivitách V současné aktivitě Při kliknutí prostředním tlačítkem: Seskupit jen pokud je správce úloh plný Obnovit Pozastavit Následující skladba Předchozí skladba Ukončit Změnit v&elikost Zobrazit pouze úlohy z aktuální aktivity Zobrazit pouze úlohy z aktuální plochy Zobrazit pouze úlohy z aktuální obrazovky Zobrazit pouze minimalizované úlohy Zobrazovat nástrojové tipy Malý Seřazování: Spustit novou instanci Přehrát Zastavit Žádný Seskupit/zrušit seskupení Dostupné na %1 Dostupné ve všech aktivitách 