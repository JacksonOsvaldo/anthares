��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �   D  )   �     �          $     2  
   ;      F  �   g  h     4   �  %   �     �     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-04-16 09:48+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Klikněte na tlačítko a poté zadejte zkratku, kterou si přejete v programu.
Příklad pro Ctrl+A: držte klávesu Ctrl a stiskněte A. Konflikt se standardní zkratkou aplikace vit@pelcak.org Shell aplikace KPackage QML Vít Pelčák Žádná Přeřadit Rezervovaná klávesová zkratka Klávesová kombinace '%1' již byla přiřazena standardní činnosti "%2", kterou mnoho aplikací využívá.
Opravdu si ji přejete použít jako globální klávesovou zkratku? Klávesa F12 je na Windows rezervována a nelze ji tedy použít jako globální.
Vyberte prosím jinou. Právě stisknutá klávesa není podporována v Qt. Unikátní název aplikace (povinné) Nepodporovaná klávesa Vstup 