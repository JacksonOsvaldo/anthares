��    )      d  ;   �      �     �     �     �     �     �               0     F     \  3   r      �     �     �     �     �     �          .     L     a  #   ~     �     �     �  S   �     >     E     N     [     m     |     �     �     �     �  
   �     �  \   �       �        �     �     �  
   �  
   �  	   	  	   	     	     $	     +	  $   8	     ]	  
   t	  	   	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	  T   
     b
     i
     r
     �
     �
     �
     �
     �
     �
  	   �
     �
       L        \                            (                              &                 )                $             '   %                              !                                   	       #      
      "        @action:buttonClone @action:buttonClose @action:buttonCommit @action:buttonExport @action:buttonImport @action:buttonOptions @action:buttonPull @action:buttonRename @action:buttonSelect @action:buttonUpdate @action:inmenu<application>Mercurial</application> @action:inmenuCreate new branch @buttonBrowse @labelOptions @labelPort @labelSource @label:buttonMerge @label:checkboxForce Pull @label:groupGeneral Settings @label:groupOptions @label:groupPlugin Settings @label:label to source fileSource  @label:renameRename to  @lobelDestination @title:groupCommit Message A KDE text-editor component could not be found;
please check your KDE installation. Branch Branch:  Copy Message Create New Branch Create New Tag Dialog height Dialog width Error! Filename Options Remove Tag Tag The KTextEditor component could not be found;
please check your KDE Frameworks installation. URLs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:48+0200
PO-Revision-Date: 2017-05-04 13:13+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Klonovat Zavřít Commit Exportovat Importovat Možnosti Stáhnout Přejmenovat Vybrat Aktualizovat <application>Mercurial</application> Vytvořit novou větev Procházet Možnosti Port Zdroj Sloučit Vynutit odeslání Obecná nastavení Možnosti Nastavení modulů Zdroj  Přejmenovat na  Cíl Commmit zpráva Nelze nalézt součást textový editor KDE.
Prosím, prověřte svou instalaci KDE. Větev Větev:  Kopírovat zprávu Vytvořit novou větev Vytvořit novou značku Výška dialogu Šířka dialogu Chyba. Název souboru Možnosti Odstranit značku Značka Nelze nalézt součást KTextEditor.
Prosím, prověřte svou instalaci KDE. URL 