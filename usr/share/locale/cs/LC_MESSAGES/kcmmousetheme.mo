��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     K  �   j  r   �     a	     {	  [   �	  
   �	     �	     
     %
  0   +
     \
     j
     {
  ,   �
     �
     �
     �
     �
     �
  ;     P   C  )   �  8   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-21 15:51+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Opravdu si přejete odstranit motiv kurzoru <i>%1</i>?<br /> Tímto smažete všechny soubory instalované tímto motivem.</qt> <qt>Nemůžete smazat motiv, který právě používáte.<br />Nejprve je třeba se přepnout na jiný motiv.</qt> (Dostupné velikosti: %1) Závislé na rozlišení Motiv s názvem %1 již existuje ve vaší složce s motivy. Přejete si ji nahradit touto? Potvrzení Změna v nastavení kurzoru Motiv kurzoru Popis Zadejte URL motivu nebo jej přetáhněte myší lukas@kde.org Fredrik Höglund Získat nový motiv Získat nová barevná schémata z internetu Instalovat ze souboru Lukáš Tinkl Název Přepsat motiv? Odstranit motiv Soubor %1 nevypadá jako platný archiv s motivem kurzorů. Nelze stáhnout archiv s motivem kurzoru. Zkontrolujte, zda je URL %1 správné. Nelze najít archiv s motivem kurzoru %1. Pro provedení změn musíte restartovat sezení Plasma. 