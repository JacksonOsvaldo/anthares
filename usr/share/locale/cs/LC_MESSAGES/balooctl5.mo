��    0      �  C         (     )     C     ]  !   v     �     �     �  ,   �     "  #   ?     c  $   |     �      �     �     �     �               4     J     \     y     �     �     �     �  
   �  "   �          &     ?     ^     r     �     �     �     �     �          %     ;  	   T     ^     u     }     �  �  �     H
     c
     �
     �
     �
     �
     �
  6   	     @  !   ^     �  /   �  !   �     �     �  
          	   :     D     ^     {     �     �     �     �     �          $     -     F     _     |     �     �     �     �     �     �          2     K     e     �     �  	   �     �     �     "              ,      $                                    .       -                	   (      /   )                                       '         &               
   +   %                            !   #       0      *               A filter must be provided A folder must be provided A value must be provided Baloo File Indexer is not running Baloo File Indexer is running Baloo Index could not be opened Check database for consistency Check for any unindexed files and index them Checking for unindexed files Config parameter could not be found Disable the file indexer Display the disk space used by index Display this help menu EMAIL OF TRANSLATORSYour emails Enable the file indexer File: %1 Forget the specified files Idle Index the specified files Indexed %1 / %2 files Indexer state: %1 Indexing Extended Attributes Indexing file content Indexing modified files Indexing new files Initial Indexing Invalid value Maintainer Manipulate the Baloo configuration Modify the Baloo configuration Monitor the file indexer NAME OF TRANSLATORSYour names Path does not exist Path is not a directory Possible Commands: Print the status of the Indexer Print the status of the indexer Restart the file indexer Resume the file indexer Start the file indexer Stop the file indexer Suspend the file indexer Suspended The command to execute Unknown Vishesh Handa balooctl Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2017-03-20 11:52+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Musí být poskytnut filtr Musí být poskytnuta složka Musí být poskytnuta hodnota Indexer Baloo neběží Indexer Baloo běží Nelze otevřít index Baloo. Ověřit konzistenci databáze Vyhledat všechny neindexované soubory a indexovat je Hledám neindexované soubory Parametr nastavení nelze nalézt Zakázat indexer souborů Zobrazit místo na disku zabrané indexováním Zobrazit tuto nabídku nápovědy vit@pelcak.org Povolit indexer souborů Soubor: %1 Zapomenout zadané soubory Nečinný Indexovat zadané soubory Indexováno %1 / %2 souborů Stav indexeru: %1 Indexuji rozšířené atributy Indexuji obsah souboru Indexuji změněné soubory Indexuji nové soubory Počáteční indexování Neplatná hodnota Správce Úpravy nastavení Baloo Úpravy nastavení Baloo Monitorovat indexer souborů Vít Pelčák Cesta neexistuje Cesta není adresář Možné příkazy: Zobrazit status indexeru Zobrazit status indexeru Restartovat indexer souborů Znovu spustit indexer souborů Spustit indexer souborů Zastavit indexer souborů Pozastavit indexer souborů Pozastaveno Příkaz ke spuštění Neznámý Vishesh Handa balooctl 