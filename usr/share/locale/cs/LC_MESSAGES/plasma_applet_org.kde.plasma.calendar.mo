��    	      d      �       �   	   �      �   <   �   5   0     f     �  4   �     �  �  �     �     �     �     �  &   �                                                         	    Ends at 5 General Show the number of the day (eg. 31) in the iconDay in month Show the week number (eg. 50) in the iconWeek number Show week numbers in Calendar Starts at 9 What information is shown in the calendar iconIcon: Working Day Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-21 16:21+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Language: cs_CZ
X-Source-Language: en_US
 Končí v 5 Obecné Den měsíce Číslo týdne Zobrazit v kalendáři čísla týdnů Začíná v 9 Ikona: Pracovní den 