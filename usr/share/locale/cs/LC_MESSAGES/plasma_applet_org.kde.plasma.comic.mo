��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  S   J     �     �     �     �               1     =  (   B     k     r  )   �     �  '   �     �     �       7        O     l     �  /   �     �     �  &   �  2        @     T     d     l     �     �  	   �      �     �     �       �   !  C   �  )   +     U  +   ]     �     �     �     �     �  *        1  &   >     e     �     �  
   �  (   �     �     �     �     �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-04-13 14:19+0200
Last-Translator: Vit Pelcak <vpelcak@suse.cz>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
 

Vyberte předcházející strip pro přechod na poslední proužek v mezipaměti. &Vytvořit archiv komiksů... &Uložit komiks jako... Číslo &stripu: *.cbz|Archiv komiksů (Zip) &Skutečná velikost Uložit aktuální &pozici Pokročilé Vše Vyskytla se chyba pro identifikátor %1. Vzhled Archivování komiksu selhalo Automaticky aktualizovat moduly komiksů: Mezipaměť Zkontrolovat nové komiksové proužky: Komiks Mezipaměť komiksů: Nastavit... Nebylo možné vytvořit archiv v zadaném umístění. Vytvořit %1 archiv komiksů Vytvářím archiv komiksů Cíl: Pokud stažení komiksu selhalo, zobrazit chybu Stáhnout komiksy Zacházení s chybami Selhalo přidání souboru do archivu. Selhalo vytvoření souboru s identifikátorem %1. Od začátku po ... Od konce po ... Obecné Získat nové komiksy... Stažení komiksu selhalo: Přejít na strip Informace Př&ejít na současný proužek &Přejít na první proužek Přejít na proužek... Ruční rozsah Možná nejste připojen k Internetu.
Možná je poškozen modul komiksů.
Další důvod může být, že neexistuje komiks pro tento den/číslo/řetězec, takže výběr jiného může fungovat. Prostřední kliknutí na komiks jej zobrazí v původní velikosti Neexistuje žádný soubor zip, končím. Rozsah: Zobrazit šipky pouze při přejetí myší Zobrazit URL komiksu Zobrazit autora komiksu Zobrazit identifikátor komiksu Zobrazit název komiksu Identifikátor komiksu: Rozsah komiksových proužků k archivaci. Aktualizovat Navštívit domovskou stránku komiksu Navštívit stránky o&bchodu # %1 dnů dd.mm.rrrr &Následující karta s novým proužkem Od: Komu:  minut proužků na komiks 