��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     x  N   �     �     �     �          !     0     8     E     [     q     �     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2017-02-24 11:41+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Přidat spouštěč... Spouštěče přidáte přetažením myší nebo pomocí kontextové nabídky. Vzhled Uspořádání Upravit spouštěč... Povolit vyskakování Zadejte název Obecné Skrýt ikony Sloupců maximálně: Řádků maximálně: Rychlé spuštění Odstranit spouštěč Zobrazit skryté ikony Zobrazovat jména spouštěčů Zobrazit název Název 