��          <      \       p   !   q   3   �      �   �  �   '   �  8   �     �                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-07-04 14:15+0200
Last-Translator: Lukáš Tinkl <ltinkl@redhat.com>
Language-Team: Czech <kde-czech-apps@lists.sf.net>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Najde sezení Kate odpovídající :q:. Vypíše všechny sezení editoru Kate pro tento účet. Otevřít sezení Kate 