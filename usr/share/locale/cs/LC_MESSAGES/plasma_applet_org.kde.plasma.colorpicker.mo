��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  *   �     �     �          $     =     E     `     n     |      �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-02-22 14:15+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Automaticky zkopírovat barvu do schránky Vyčistit historii Možnosti barev Zkopírovat do schránky Výchozí formát barvy: Obecné Otevřít dialog pro barvy Vyberte barvu Vyberte barvu Zobrazit historii Při stisku klávesové zkratky: 