��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     ]     s  "        �     �     �     �     �  *        8  $   R     w  :   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-11-01 12:28+0100
Last-Translator: Vit Pelcak <vpelcak@suse.cz>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Nastavte motiv plochy David Rosca vit@pelcak.org,pavelfric@seznam.cz Získat nové motivy... Instalovat ze souboru... Vít Pelčák, Pavel Fric Otevřít motiv Odstranit motiv Soubory motivů (*.zip *.tar.gz *.tar.bz2) Instalace motivu selhala. Motiv byl úspěšně nainstalován. Odstranění motivu selhalo. Tento modul vám umožní nastavit motiv pracovní plochy. 