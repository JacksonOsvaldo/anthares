��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K       &     �  4  u  �  Z   K
     �
     �
     �
  $   �
            -  &   N     u                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-10 13:46+0200
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.5
  sek Časový limit indikace &spouštění: <H1>Upozornění v panelu úloh</H1>
Druhou metodou upozornění na start aplikace je zobrazení
tlačítka s rotujícím diskem v panelu úloh, který symbolizuje,
že spuštěná aplikace se načítá. Může se stát, že některé aplikace
neumí spolupracovat s upozorněním na start. V takovém případě
přestane kurzor blikat po uplynutí doby stanovené v sekci
'Časový limit indikace spouštění'. <h1>Zaneprázdněný kurzor</h1>
KDE nabízí zaneprázdněný kurzor pro upozorňování na startování aplikace.
K jeho povolení vyberte typ odezvy ze seznamu.
Může se stát, že některé aplikace neumí spolupracovat s upozorněním na start.
V takovém případě přestane kurzor blikat po uplynutí doby stanovené v sekci
'Časový limit indikace spouštění'. <h1>Informace o spouštění</h1>Zde je možné nastavit odezvu při spuštění aplikace. Blikající kurzor Poskakující kurzor Zanepráz&dněný kurzor Povoli&t upozornění v panelu úloh Žádný zaneprázdněný kurzor Pasivní zaneprázdněný kurzor Časový limit indikace spo&uštění: Upozor&nění v panelu úloh 