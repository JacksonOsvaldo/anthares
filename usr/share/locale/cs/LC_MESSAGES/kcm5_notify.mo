��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  )   �     �     �               $     4  4   K         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-10 15:06+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 (c) 2002-2006 tým KDE <h1>Systémová hlášení</h1>Plasma vám umožňuje značnou kontrolu nad způsobem, jakým budete zpravováni o určitých událostech v systému. Je několik způsobů hlášení událostí:<ul><li>Tak, jak bylo původně určeno v programu.</li><li>Pípnutím nebo jiným zvukem.</li><li>Pomocí otevření dialogového okna s doplňující informací.</li><li>Zaznamenáním události do záznamového souboru bez dalšího vizuálního nebo akustického upozornění.</li></ul> Carsten Pfeiffer Charles Samuels Vypnout zvuky pro všechny tyto události flidr@kky.zcu.cz Zdroj události: KNotify Miroslav Flídr Olivier Goffart Původní implementace Modul ovládacího panelu pro systémová hlášení 