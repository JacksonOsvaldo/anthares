��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  3   �     �  2        D     J     O                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-30 10:14+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Aktuální čas je %1 Zobrazí aktuální datum Zobrazí aktuální datum v daném časovém pásmu Zobrazí aktuální datum Zobrazí aktuální čas v daném časovém pásmu datum čas Dnešní datum je %1 