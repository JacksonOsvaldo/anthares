��          t      �                 '     6     R     Z     w  Q   �     �      �       �       �            	   3     =     T     r     x     �     �     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-04-11 15:29+0100
Last-Translator: Vít Pelčák <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 V&yžadovat spouštěcí slovo Spouš&těcí slovo: Zkontroluje pravopis :q:. Správně Slovník nelze najít. Nastavení kontroly pravopisu %1:q: Navrhovaná slova: %1 ,  kontrola 