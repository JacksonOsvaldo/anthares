��            )   �      �     �     �     �     �                    )  -   8     f     k  
   t          �     �  	   �     �  
   �     �     �               6     L     g  -   }     �     �     �  �  �          �     �     �     �     �     �       0        K     Q     ^     o     �     �     �     �     �     �     �  )        <     S     h     �     �     �  	   �     �                                                           	                                                                      
        @title:windowChange your Face @title:windowChoose Image Add user account Choose from Gallery... Clear Avatar Delete User Delete files Email Address: Enable administrator privileges for this user John John Doe Keep files Load from file... Log in automatically New User Password: Passwords do not match Real Name: Remove user account Select a new face: The username is too long This password is excellent This password is good This password is very good This password is weak Title for change password dialogNew Password Users Verify: john.doe@example.com Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:21+0100
PO-Revision-Date: 2017-11-13 14:25+0100
Last-Translator: Vit Pelcak <vit@pelcak.org>
Language-Team: Czech <kde-i18n-doc@kde.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 2.0
 Změňte si obličej Vybrat obrázek Přidat účet uživatele Vyberte z galerie... Vymazat avatara Odstranit uživatele Smazat soubory E-mailová adresa: Udělit tomuto uživateli práva administrátora Josef Josef Novák Ponechat soubory Načíst ze souboru... Automaticky přihlašovat Nový uživatel Heslo: Hesla se neshodují Skutečné jméno: Odstranit účet uživatele Vyberte nový obličej: Uživatelské jméno je příliž dlouhé Toto heslo je skvělé Toto heslo je dobré Toto heslo je velmi dobré Toto heslo je slabé Nové heslo Uživatelé Ověřit: uzivatel@priklad.cz 