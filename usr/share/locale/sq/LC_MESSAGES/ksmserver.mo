��          D      l       �      �      �   ,   �   c   �   �  P  <   0     m  @   �  \   �                          Also allow remote connections Logout canceled by '%1' Restores the saved user session if available The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Project-Id-Version: kdebase-workspace
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2009-08-08 18:13+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2011-05-06 00:48+0000
X-Generator: Launchpad (build 12959)
 Gjithashtu lejo lidhjet me kompjutera nga jashtë (të huaj) Dalja u anullua nga '%1' Rikthe seancën e mëparshme, sërish nëse është në disponim Menaxheri i zellshëm i sesionit KDE, i cili po ashtu zotëron Protokollin 
standard X11R6 . 