��          T      �       �   	   �   H   �           "  
   ;     F  �  ^     <  J   H     �     �  
   �     �                                        
Syntax:
   kioclient openProperties 'url'
            # Opens a properties menu

 Arguments for command Command (see --commands) KIO Client Show available commands Project-Id-Version: kdebase-runtime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-02-19 15:07+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-04-22 15:26+0000
X-Generator: Launchpad (build 12883)
Plural-Forms: nplurals=2; plural=n != 1;
 
Sintaksa:
   kioclient openProperties 'url'
            # Hap një menu parametrash

 Argumentet e komandës Komanda (shih --komandat) KIO Klient Trego komandat e mundshme 