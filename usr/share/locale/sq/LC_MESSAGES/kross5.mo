��          �      \      �  &   �     �     
                    5     =     Z     `     f     s     y     �     �     �     �  &   �     �  �  �     �     �     �     �     �  !   �  
              ?     H     O     ^     d     |     �     �     �     �     �                                                             	                        
              @title:group Script propertiesGeneral Add a new script. Add... Comment: Edit Edit selected script. Edit... Execute the selected script. File: Icon: Interpreter: Name: No such function "%1" Remove Remove selected script. Run Stop Stop execution of the selected script. Text: Project-Id-Version: kde4libs
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-01-19 00:20-0500
Last-Translator: Agron Selimaj <as9902613@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2010-11-24 03:19+0000
X-Generator: Lokalize 1.2
 Të Përgjithshme Shto një skript të ri. Shto... Komento: Ndrysho Modifiko skriptin e përzgjedhur. Ndrysho... Ektekuto skriptën e zgjedhur. Skedari: Ikona: Interpretuesi: Emri: Nuk ka funksion si "%1" Hiqe Hiqe skriptin e përzgjedhur. Ekzekuto Ndalo Hiqe skriptën e zgjedhur. Teksti: 