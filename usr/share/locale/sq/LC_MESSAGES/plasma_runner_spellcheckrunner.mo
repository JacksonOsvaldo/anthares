��    	      d      �       �      �      �           "     *  Q   ?     �     �  �  �     �     �     �     �  +   �            	   +                            	                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: kdeplasma-addons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-07-31 03:01+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-04-22 01:33+0000
X-Generator: Launchpad (build 12883)
Plural-Forms: nplurals=2; plural=n != 1;
 &Kërkon fjalë nismëtare &Fjala nismëtare: Kontrollon gërmëzimin e :q:. Korrigjo Parametrat e Kontrolluesit të Gërmëzimit %1:q: Fjalët e sugjeruara: %1 gërmëzo 