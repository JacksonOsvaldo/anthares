��    e     D  �  l      �     �     �     �          6     F     V     [     `     e     j  6   o     �  2   �  	   �     �                     !  	   0  �   :  H   �  H         X      v      �      �      �      �      �       �      !     &!     -!     6!     ?!     K!     S!     Y!     h!     m!     v!     �!     �!     �!     �!     �!     �!     �!     �!      "  '   "     E"     ]"     i"     �"  "   �"  "   �"     �"     �"     #     #     #     #  &   :#  '   a#     �#     �#  	   �#     �#     �#     �#     �#     �#  	   �#  !   $     )$     :$     B$     T$     ]$     q$     v$     }$     �$  	   �$     �$  
   �$     �$     �$     �$     �$     �$  	   �$     %     %      '%  '   H%     p%     w%     �%     �%     �%     �%     �%     �%     �%     �%     &  
   &     '&     7&     F&     R&     Y&  &   g&  :   �&     �&     �&     �&     �&  	   �&      '     '     '     '     3'     D'     M'  
   a'     l'     s'     y'     �'     �'     �'     �'     �'     �'     �'     �'     �'     (     (     (     !(     ((     D(     T(     ](  "   l(  	   �(     �(  
   �(  
   �(     �(     �(     �(     �(     �(     �(     )  
   )     *)     0)     >)     B)     H)     b)     s)     �)     �)     �)     �)     �)     �)     �)     �)     *     "*     (*     .*     7*     J*     ]*     f*  	   u*     *     �*     �*     �*  !   �*     �*     +  $   +  $   0+     U+      k+     �+     �+  	   �+  
   �+     �+  	   �+  
   �+     �+     �+     ,     ,     ,     9,  	   =,     G,     S,     W,     l,     �,     �,     �,     �,     �,     �,     �,     �,      -     -  B   -     `-     h-  
   n-     y-     �-     �-     �-     �-     �-     �-     �-     �-     	.     .     #.     2.     ;.  
   D.     O.     c.     �.     �.     �.     �.     �.     �.     �.     �.  
   /     /     /     ./     E/     W/     p/     �/     �/     �/     �/     �/     �/  -   �/     0  	   0     0     0     $0     60     L0  	   `0     j0     y0     �0     �0     �0     �0     �0  
   �0     �0     �0     1     1     "1     '1     A1     F1     Z1     s1     |1     �1     �1     �1     �1  #   �1     �1      2      92     Z2     g2     x2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2     3     3     3     3  -   3     K3     S3     \3     b3     |3     �3     �3     �3     �3     �3  
   �3     �3     �3     �3     �3  %   �3     4     )4     14     E4     N4     b4     �4  	   �4     �4     �4     �4     �4     �4     �4     �4  �  5     �6     �6     �6     7     17     G7     [7     `7     e7     j7     o7  @   t7     �7  <   �7     8     8     8     ,8  
   98     D8     W8  h   j8  /   �8  /   9     39  !   N9     p9     �9     �9     �9     �9  )   �9     :     :     :     ):     2:     C:     K:     P:     a:     f:     o:     �:     �:     �:     �:     �:     �:     �:     �:  !   ;  (   2;     [;     o;     {;     �;  )   �;  -   �;     <     "<  	   7<     A<     J<      R<  (   s<  2   �<     �<     �<     �<     =     =     !=     /=     >=  	   U=  "   _=     �=     �=     �=     �=     �=     �=     �=     �=     �=     �=     >  
   >     >     0>  
   9>     D>     X>     k>  !   }>     �>  /   �>  3   �>     ?      ?     2?     C?     S?     s?  "   �?     �?     �?     �?  	   �?     �?     �?     @     @     '@     /@  <   H@     �@     �@     �@     �@     �@     �@     �@     �@     �@     A     "A     4A     @A     SA     _A     fA     lA     �A     �A     �A     �A  5   �A     �A     �A     �A     B     B     $B  	   ,B  
   6B  +   AB     mB     �B     �B     �B     �B     �B  
   �B     �B     �B     C     !C     (C     8C     @C     IC  
   bC     mC     sC     �C     �C     �C     �C     �C     �C     �C     
D     D     2D     GD     YD     kD      }D     �D     �D     �D     �D     �D     �D     �D  	   E     E     )E     <E     RE  #   rE     �E     �E  #   �E  #   �E     �E     F     2F     OF     eF     mF     |F     �F     �F     �F     �F     �F     �F  0   �F     &G  	   *G     4G     AG     EG  !   `G  	   �G     �G     �G     �G     �G     �G     �G     
H     "H     3H  +   CH     oH     uH     {H     �H     �H     �H     �H     �H     �H     �H     I     I     8I  	   JI     TI     fI     oI     xI     �I     �I      �I  $   �I     �I     J     J     !J  	   <J     FJ  
   ^J     iJ     vJ     �J     �J     �J     �J     �J     �J  
   K     K     K     $K  B   0K     sK     K     �K     �K     �K  "   �K  $   �K     �K     L      L     3L     CL     UL     iL     yL     �L     �L     �L     �L     �L     �L  ,   �L     ,M     5M  (   IM  	   rM     |M     �M     �M     �M     �M  $   �M     N  &   N  &   BN     iN     vN     �N     �N     �N     �N     �N     �N     �N     �N     �N     O     O     O     .O     7O     >O  
   EO     PO  5   YO  	   �O     �O     �O  $   �O     �O     �O     �O     P     
P     P  
   P     #P     6P     HP     WP  %   eP     �P  
   �P     �P     �P     �P      �P     Q  	   $Q     .Q     =Q     PQ     dQ     gQ     sQ     �Q     �          X   t       3   Q   b  �   ^         {           �   F  B          �   �         G    �   W                    �   I       �   T  '   �   �   I    ]  v   �           a   �         �     �   ;  Z           D  �   �          �           �   $      �   9   �   e   �       H      5   �   �       Z      C      4          A  u   �   �   �       N          =  �   9  �   �               �       :  �   ?   C   �      @   �   p      M  �         �           �   �         �   /   7   <    ;   <           O   g              �       [   /  %  B       ,      X  $   
   �       d   0     �   �   �       �   �   �       i   �      ~   G       #        6   �   J  �       2      E   �   }   F   V  T                          �          &   \  �   �              P   �   b                 U      K  |   �   �   �                               m   j   S     �   5  3         �         D   �   �   *       y   �   +   k       ,      a      6  �   J   �     �       1  �   �   �   >   H       �   �             L               o   �       A   )  !    �   �   x       �      �   �            �           	  U          �   .   �   �   Y  (     :   �   _   �   0    -  .  �   &  "      `     4   �   �   Y               �       �       �            �   �   �   �           �   �   R  �   �   =   ^   �       �   _  �   !       W       �   �   -   �       #   e  +  *  ]   c  �   f                 "   �   8    �   �   K       L  P  s   >  N   �   [          %   h   �       �       \   R   7        �   M   �           E  �   8   �                 q   �   �       O  `  r   c   1   )      @  V   (         n   �   	   '  l              Q            �   2  d     �   �      �   �       �          �       w   
          �   �       �   �   �       z   ?  S   %1 [%2] &Main Toolbar (C) Craig Drummond, 2004-2007 (C) Craig Drummond, 2007 ...plus %1 more 1 Font %1 Fonts 12pt 18pt 24pt 36pt 48pt <p>Do you really want to delete</p><p>'<b>%1</b>'?</p> <p>No information</p> <qt>A group named <b>'%1'</b> already exists.</qt> Add Fonts Add... Administrator Aegean Numbers All All Characters All Fonts All letters of the alphabet (in upper/lower case pairs), followed by numbersAaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789 All of the letters of the alphabet, lowercaseabcdefghijklmnopqrstuvwxyz All of the letters of the alphabet, uppercaseABCDEFGHIJKLMNOPQRSTUVWXYZ Alphabetic Presentation Forms Ancient Greek Musical Notation Ancient Greek Numbers Arabic Arabic Presentation Forms-A Arabic Presentation Forms-B Arabic Supplement Are you sure you wish to delete: Armenian Arrows AutoSkip Balinese Basic Latin Bengali Black Block Elements Bold Bopomofo Bopomofo Extended Box Drawing Braille Braille Patterns Buginese Buhid Byzantine Musical Symbols CJK Compatibility CJK Compatibility Forms CJK Compatibility Ideographs CJK Compatibility Ideographs Supplement CJK Radicals Supplement CJK Strokes CJK Symbols and Punctuation CJK Unified Ideographs CJK Unified Ideographs Extension A CJK Unified Ideographs Extension B Canadian Aboriginal Cannot Print Category Charcell Cherokee Combining Diacritical Marks Combining Diacritical Marks Supplement Combining Diacritical Marks for Symbols Combining Half Marks Common Condensed Control Pictures Coptic Counting Rod Numerals Craig Drummond Create New Group Cuneiform Cuneiform Numbers and Punctuation Currency Symbols Cypriot Cypriot Syllabary Cyrillic Cyrillic Supplement Date Delete Delete Font Delete Fonts Demi Bold Deseret Devanagari Developer and maintainer Dingbats Disable Disable Font Disable Fonts Disabling Disabling font(s)... Duplicate Fonts EMAIL OF TRANSLATORSYour emails ERROR: Could not determine font's name. Enable Enable Font Enable Fonts Enabling Enabling font(s)... Enclosed Alphanumerics Enclosed CJK Letters and Months Ethiopic Ethiopic Extended Ethiopic Supplement Expanded Extra Bold Extra Condensed Extra Expanded Extra Light Family File Location File containing list of fonts to print First letter of the alphabet (in upper then lower case)Aa Font Font Installer Font Printer Font Viewer Font/File FontConfig Match Fonts Foundry General Punctuation Geometric Shapes Georgian Georgian Supplement Glagolitic Gothic Greek Greek Extended Greek and Coptic Group Gujarati Gurmukhi Half-Width and Full-Width Forms Han Hangul Hangul Compatibility Jamo Hangul Jamo Hangul Syllables Hanunoo Heavy Hebrew High Private Use Surrogates High Surrogates Hiragana IPA Extensions Ideographic Description Characters Inherited Install fonts Install... Installing Installing font(s)... Italic Kanbun Kangxi Radicals Kannada Katakana Katakana Phonetic Extensions Kharoshthi Khmer Khmer Symbols Lao Latin Latin Extended Additional Latin Extended-A Latin Extended-B Latin Extended-C Latin Extended-D Latin-1 Supplement Letter, Lowercase Letter, Modifier Letter, Other Letter, Titlecase Letter, Uppercase Letter-Like Symbols Light Limbu Linear B Linear B Ideograms Linear B Syllabary Links To Low Surrogates Malayalam Mark for Deletion Mark, Enclosing Mark, Non-Spacing Mark, Spacing Combining Mathematical Alphanumeric Symbols Mathematical Operators Medium Miscellaneous Mathematical Symbols-A Miscellaneous Mathematical Symbols-B Miscellaneous Symbols Miscellaneous Symbols and Arrows Miscellaneous Technical Modifier Tone Letters Mongolian Monospaced Move Move Font Move Fonts Moving Moving font(s)... Musical Symbols Myanmar NAME OF TRANSLATORSYour names NKo New Group New Tai Lue Nko No characters found. No duplicate fonts found. No fonts Normal Nothing to Delete Nothing to Disable Nothing to Enable Nothing to Move Number Forms Number, Decimal Digit Number, Letter Number, Other Numbers and characters0123456789.:,;(*!?'/\")£$€%^&-+@~#<>{}[] Oblique Ogham Old Italic Old Persian Open in Font Viewer Optical Character Recognition Oriya Osmanya Other, Control Other, Format Other, Not Assigned Other, Private Use Other, Surrogate Personal Personal Fonts Phags Pa Phags-pa Phoenician Phonetic Extensions Phonetic Extensions Supplement Please enter new text: Please specify "%1" or "%2". Preview Text Preview Type Print Print Font Samples Print... Private Use Area Properties Proportional Punctuation, Close Punctuation, Connector Punctuation, Dash Punctuation, Final Quote Punctuation, Initial Quote Punctuation, Open Punctuation, Other Regular Reload Remove Remove Group Remove file containing list of fonts to print Remove group Rename... Roman Runic Scanning Files... Scanning font list... Select Font to View Semi Bold Semi Condensed Semi Expanded Separator, Line Separator, Paragraph Separator, Space Set Criteria Shavian Show Face: Simple font installer Simple font printer Simple font viewer Sinhala Size Size index to print fonts Skip Small Form Variants Spacing Modifier Letters Specials Standard Preview Style Superscripts and Subscripts Supplemental Arrows-A Supplemental Arrows-B Supplemental Mathematical Operators Supplemental Punctuation Supplementary Private Use Area-A Supplementary Private Use Area-B Syloti Nagri Symbol, Currency Symbol, Math Symbol, Modifier Symbol, Other Symbol/Other Syriac System System Fonts Tagalog Tagbanwa Tags Tai Le Tai Xuan Jing Symbols Tamil Telugu Thaana Thai Thin This displays a preview of the selected font. Tibetan Tifinagh Tools Type here to filter on %1 UCS-4 URL to install URL to open UTF-16 UTF-8 Ugaritic Ultra Bold Ultra Condensed Ultra Expanded Ultra Light Unclassified Unified Canadian Aboriginal Syllabics Uninstalling Unknown Unmark for Deletion Updating Variation Selectors Variation Selectors Supplement Vertical Forms Waterfall Where to Install Writing System XML Decimal Entity Yi Yi Radicals Yi Syllables Yijing Hexagram Symbols Project-Id-Version: kdebase-workspace
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:19+0100
PO-Revision-Date: 2009-05-23 18:07+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <sq@li.org>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2011-05-06 01:48+0000
X-Generator: Launchpad (build 12959)
 %1 [%2] &Shiriti Kryesor (C) Craig Drummond, 2004-2007 (C) Craig Drummond, 2007 ...plus %1 më tepër 1 Gërmë %1 Gërma 12pt 18pt 24pt 36pt 48pt <p>Me të vërtetë dëshironi të fshini</p><p>'<b>%1</b>'?</p> <p>Pa informacion</p> <qt>Një grup i emërtuar <b>'%1'</b> ekziston tashmë.</qt> Shto Gërmat Shto... Administratori Numra Aegean Të Gjitha Të Gjitha Gërmat Të Gjitha Gërmat AaBbCcÇçDdDhdhEeËëFfGgGjgjHhIiJjKkLlLlllMmNnNjnjOoPpQqRrRrrrSsShshTtThthUuVvXxXhxhYyZzZhzh0123456789 abcçddheëfggjhijklllmnnjopqrrrsshtthuvxxhyzzh ABCÇDDHEËFGGJHIJKLLLMNNJOPQRRRSSHTTHUVXXHYZZH Forma prezantimi alfabetor Nota muzikore të Greqisë antike Numra të Greqisë antike Arabisht Format-A të prezantimit arabik Format-B të prezantimit arabik Arabe Shtesë Jeni i sigurtë se dëshironi të fshini: Armene Shigjeta VetëKapërce Balinese Latinishte Bazë Bengali I Zi Elementë blloku I Zi Bopomofo Bopomofo e zgjeruar Vizatues drejtkëndëshi Braille Kampione braille Buginese Buhid Simbole muzikore Bizantinase Përputhshmëri CJK Forma përputhshmërie CJK Kompatibilitet ideografikësh CJK Shtesë e ideografikëve kompatibël CJK Shtesa radikale CJK CJK Strokes Simbole dhe pikëshënime CJK Ideografikë të unifikuar CJK Zgjerimi A ideografikë të unifikuar CJK Ideografikë të zgjeruar B të unifikuar CJK Kanadez Aborigjen Nuk Mund të Printoj Kategoria Charcell Sheroke Shenja diakritike të kombinuara Shenja diakritike të kombinuara shtesë Shenja diakritike të kombinueshme sipas simboleve Gjysmëshënja të kombinuara Përbashkët I Kondoensuar Figura kontrolli Koptike Viza numerale Craig Drummond Krijo një Grup të Ri Kuneiform Numra dhe Pikëshënime Kuneiforme Simbolet e Monedhave Qipriote Silabari Qiproite Cirilik Cirilike Shtesë Data Fshije Fshije Gërmën Fshiji Gërnat Gjysëm i Zi Deseret Devanagari Krijues dhe mirëmbajtës Dingbats Çaktivizo Çaktivizo Gërmën Çaktivizo Gërmat Duke Çaktivizuar Duke çaktivizuar gërmat(ën)... Gërma të Dyfishta kde-shqip@yahoogroups.com,vilsongjeci@gmail.com GABIM: Nuk mund të përcaktojmë emrin e gërmës. Aktivizo Aktivizo Gërmën Aktivizo Gërmat Duke Aktivizuar Duke aktivizuar gërmat(ën)... Alfanumerikë të mbyllur Shkronja CJK të mbyllura dhe muaj Etiopian Etiope e zgjeruar Etiope Shtesë Shpalosur Ekstra i Zi Ekstra i Kondensuar Ekstra i Shpalosur Shumë i Lehtë Familja Vendndodhja e Skedarëve Skedari që përmban listën e gërmave që do të printohen Aa Gërmat Instaluesi i Gërmave Grëma e Printerit Treguesi i Fonteve Gërma/Skedari FontConfig Përputhet Gërmat Shkrirësja Pikshënimi i përgjithshëm Forma gjeometrike Gjeorgjiane Georgjisht shtesë Glagolitike Gotike Greke Greqishte e zgjeruar Greqishte dhe Koptike Grupo Gujarati Gurmuki Forma me Gjysëm Gjerësie dhe Me Gjerësi të Plotë Han Hangul Hangul Kompatibël Jamo Hangul Jamo Hangul Syllables Hanunoo I Rëndë Hebraishte Surrogatë për përdorim privat të lartë Surrogatë të lartë Hiragana Prapashtesa IPA Simbole përshkrimi ideografike I Trashëguar Instalo gërmat Instalo... Duke Instaluar Duke instaluar gërmën(at)... të Pjerrëta Kanbun Radikale Kangxi Kannada Katakana Shtesa fonetike Katakana Kharoshthi Khmer Simbole Khmer Lao Latin Latinishte e shtuar e zgjeruar Latinishte e zgjeruar-A Latinishte e zgjeruar-B Latinishte e zgjeruar-C Latinishte e zgjeruar-D Shtesë Latinishte-1 Shkronja, e Vogël Shkronja, Ndryshuesi Shkronja, Tjetër Shkronja, Titulli Shkronja, e Madhe Simbole të Ngjajshme me Gërmat I Lehtë Limbu Linear B Ideografikë lineare B Linear B Syllabary Lidhjet Për Tek Surrogatë të ulët Malajalam Përzgjidhe Për Fshirje Shenjë, e mbyllur Shenjë, pa hapësira Shenjë, hapësirë e kombinuar Simbole alfanumerikë matematikorë Vepruesit Matematikë Mesatar Simbole matematikë të ndryshëm-A Simbole matematikë të ndryshëm-B Simbole të Përziera Simbole e shigjeta të ndryshme Simbole teknike të ndryshme Modifier Tone Letters Mongole monohapësirë Lëvize Lëvize Gërmën Lëvizi Gërmat Duke lëvizur Duke lëvizur gërmën(at)... Simbole muzikore Mjanmar (Birmania) KDE Shqip, Launchpad Contributions: Vilson Gjeci NKo Grup i Ri Tai Lue e Re Nko Nuk u gjet asnjë gërmë. Nuk u gjetën gërma të dyzuara. Pa gërma Normal Asgjë Për Tu Fshirë Asgjë Për Tu Çaktivizuar Asgjë Për Tu Aktivizuar Asgjë për të Lëvizur Format e Numrave Numër, shifra dhjetore Numër, shkronja Numër, tjetër 0123456789.:,;(*!?'/\")£$€%^&-+@~#<>{}[] Vezor Ogham Italike antike Persishte e Vjetër Hape në Shfaqësin e Fonteve Njohja optike e simboleve Oriya Osmania Tjetër, Kontrolli Tjetër, Formati Tjetër, i Pa Caktuar Tjetër, përdorim privat Tjetër, Surrogat Personale Gërmat Personale Phags Pa Phags-pa Fenikase Zgjerime Fonetike Zgjerime fonetike shtesë Ju lutemi vendosni tekstin e ri: Ju lutemi specifikoni "%1" apo "%2". Parapamje e Tekstit Lloji i Parapamjes Printo Printo Shembujt e Gërmave Printo... Zonë përdorimi privat Parametrat Proporcional Pikësimi, e mbyllur Pikësimi, Lidhësi Pikësimi, kërcim Pikësimi, kuota e fundit Pikësimi, kuota e parë Pikësimi, e hapur Pikësimi, tjetër I Rregullt Ringarko Hiqe Hiqe Grupin Hiqe skedarin që përmban listën e gërmave që do të printohen Hiqe grupin Riemërto... Roman Runik Duke Skanuar Skedarët... Duke skanuar listën e gërmave... Zgjidh Gërmën Që Do Të Shfaqësh Pjesërisht i Zi Gjysëm i Kondensuar Gjysëm i Zgjeruar Ndarës, rresht Ndarës, paragraf Ndarës, hapësirë Vendos Kriteret Shavian Shfaq Fytyrën: Instalues i thjeshtë gërmash Gërmë e thjeshtë e printerit Shfaqës i thjeshtë gërmash Sinhala Përmasa Përmasa e indeksit për të printuar gërma Kapërce Ndryshime të vogla Ndryshues të hapësirës së shkronjës Specialë Parapamje Standarte Stili Supershkrime dhe nënshkrime Shigjeta suplementare-A Shigjeta suplementare-B Operatorë matematikë suplementarë Pikshënim shtesë Zonë përdorimi privat suplementare-A Zonë përdorimi privat suplementare-B Syloti Nagri Simboli, valutë Simboli, Matematikë Simboli, ndryshues Simboli, tjetër Simboli/ Të Tjera Siriane Sistemi Gërmat e Sistemit Tagalog Tagbanwa Etiketat Tai Le Simbolet Tai Xuan Jing Tamileze Telugu Thaana Tailandeze I Hollë Kjo jep një parapamje të gërmës së përzgjedhur. Tibetiane Tifinagh Veglat Shtyp këtu për të filtruar në %1 UCS-4 URl që do të instalohet URL që do të hapet UTF-16 UTF-8 Ugarite Ultra i Zi Ultra i Kondensuar Ultra i Shpalosur Ultra i Lehtë Pa klasifikim Unified Canadian Aboriginal Syllabics Duke Çinstaluar I Panjohur Hiqe Përzgjedhjen Për Fshirje Duke Përditësuar Seleksionues ndryshimesh Seleksionues ndryshimesh shtesë Forma vertikale Ujëvarë Ku ta Instaloj Sistemi i Shkrimit XML Njësi Dhjetore Yi Radikale Yi Yi Syllables Simbole heksagrame Yijing 