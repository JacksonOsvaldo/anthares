��    A      $  Y   ,      �  #   �     �     �     �  J   �          )     9     N     c     u     �     �     �     �  	   �     �     �     �                ;     T      k     �      �     �     �  '   �            !   0     R     `     r  
   �     �  	   �     �     �     �     �     �     �     	     	     %	     3	     A	     \	     n	     �	     �	     �	     �	     �	     �	     �	  	   �	     	
     
     (
  0   4
  3   e
  �  �
  f   G     �     �     �  �   �  "   �  /   �  ?     4   X  %   �     �     �     �  e   �     N     a     u  0   �  (   �  W   �  N   =  A   �  A   �  W     W   h     �     �  :   �  [     (   {  B   �  e   �  +   M  5   y  E   �     �               ;  )   K  %   u  (   �     �  .   �  "     $   3  .   X  :   �  M   �  K     %   \  5   �     �  P   �  ;   #  /   _  "   �  1   �  %   �  +   
  .   6  "   e  �   �  �   !                        ,          "   $   
   <       #   =       7          ;   0       9          *   6             !                 2   3   &   %   .                -   	   5         8       A                      +   4      (      )              ?         /              @          >                1            :      '    &Enter a name for the color scheme: (c) 2007 Matthew Woehlke 0 1 A color scheme with that name already exists.
Do you want to overwrite it? Active Text Active Titlebar Active Titlebar Text Alternate Background Button Background Button Text Color: Colors Colorset to view/modify Contrast Contrast: Current color schemeCurrent Default color schemeDefault Disabled color Disabled color effect amount Disabled color effect type Disabled contrast amount Disabled contrast type Disabled intensity effect amount Disabled intensity effect type EMAIL OF TRANSLATORSYour emails Error Focus Decoration Get new color schemes from the Internet Hover Decoration Import Color Scheme Import a color scheme from a file Inactive Text Inactive Titlebar Inactive Titlebar Text Intensity: Jeremy Whiting Link Text Matthew Woehlke NAME OF TRANSLATORSYour names Negative Text Neutral Text New Row Normal Background Normal Text Options Positive Text Remove Scheme Remove the selected scheme Save Color Scheme Selection Background Selection Inactive Text Selection Text Shade sorted column &in lists Tooltip Background Tooltip Text Varies View Background View Text Visited Text Window Background Window Text You do not have permission to delete that scheme You do not have permission to overwrite that scheme Project-Id-Version: kcmcolors
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-01-05 15:48+0530
Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>
Language-Team: Oriya <oriya-it@googlegroups.com>
Language: or
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);



 ରଙ୍ଗ ଯୋଜନା ପାଇଁ ଗୋଟିଏ ନାମ ଭରଣ କରନ୍ତୁ (&E): (c) 2007 Matthew Woehlke 0 1 ସେହି ନାମ ବିଶିଷ୍ଟ ଗୋଟିଏ ରଙ୍ଗ ଯୋଜନା ପୂର୍ବରୁ ଅବସ୍ଥିତ ଅଛି।
ଆପଣ ଏହାର ନବଲିଖନ କରିବାକୁ ଚାହୁଁଛନ୍ତି କି? ସକ୍ରିୟ ପାଠ୍ୟ ସକ୍ରିୟ ଶୀର୍ଷକ ପଟି ସକ୍ରିୟ ଶୀର୍ଷକ ପଟି ପାଠ୍ୟ ବୈକଳ୍ପିକ ପୃଷ୍ଠଭୂମି ବଟନ ପୃଷ୍ଠଭୂମୀ ବଟନ ପାଠ୍ୟ ରଙ୍ଗ: ରଙ୍ଗ ଦେଖିବା/ପରିବର୍ତ୍ତନ କରିବା ପାଇଁ ରଙ୍ଗ ସେଟ ପ୍ରଭେଦ ପ୍ରଭେଦ: ପ୍ରଚଳିତ ପୂର୍ବନିର୍ଦ୍ଧାରିତ ନିଷ୍କ୍ରିୟ ରଙ୍ଗ ନିଷ୍କ୍ରିୟ ତୀବ୍ରତା ପ୍ରଭାବ ପରିମାଣ ନିଷ୍କ୍ରିୟ ରଙ୍ଗ ପ୍ରଭାବ ପ୍ରକାର ନିଷ୍କ୍ରିୟ ପ୍ରଭେଦ ପରିମାଣ ନିଷ୍କ୍ରିୟ ପ୍ରଭେଦ ପ୍ରକାର ନିଷ୍କ୍ରିୟ ତୀବ୍ରତା ପ୍ରଭାବ ପରିମାଣ ନିଷ୍କ୍ରିୟ ତୀବ୍ରତା ପ୍ରଭାବ ପ୍ରକାର mgiri@redhat.com ତ୍ରୁଟି କେନ୍ଦ୍ରୀଭୂତ ସାଜସଜ୍ଜା ଇଣ୍ଟରନେଟରୁ ନୂତନ ରଙ୍ଗ ଯୋଜନା ଆଣନ୍ତୁ ଆଖପାଖ ସାଜସଜ୍ଜା ରଙ୍ଗ ଯୋଜନା ଆମଦାନୀ କରନ୍ତୁ ଗୋଟିଏ ଫାଇଲରୁ ରଙ୍ଗ ଯୋଜନା ଆମଦାନୀ କରନ୍ତୁ ନିଷ୍କ୍ରିୟ ପାଠ୍ୟ ନିଷ୍କ୍ରିୟ ଉପକରଣ ପଟି ନିଷ୍କ୍ରିୟ ଉପକରଣ ପଟି ପାଠ୍ୟ ତୀବ୍ରତା: Jeremy Whiting ସଂଯୋଗ ପାଠ୍ୟ Matthew Woehlke ମନୋଜ କୁମାର ଗିରି ଋଣାତ୍ମକ ପାଠ୍ୟ ନିରପେକ୍ଷ ପାଠ୍ୟ ନୂତନ ଧାଡ଼ି ସାଧାରଣ ପୃଷ୍ଠଭୂମି ସାଧାରଣ ପାଠ୍ୟ ବିକଳ୍ପଗୁଡ଼ିକ ଯୁକ୍ତାତ୍ମକ ପାଠ୍ୟ ଯୋଜନାକୁ କାଢ଼ିଦିଅନ୍ତୁ ବଚ୍ଛିତ ଯୋଜନାକୁ କାଢ଼ିଦିଅନ୍ତୁ ରଙ୍ଗ ଯୋଜନାକୁ ସଂରକ୍ଷଣ କରନ୍ତୁ ଚୟନ ପୃଷ୍ଠଭୂମୀ ଚୟନ ନିଷ୍କ୍ରିୟ ପାଠ୍ଯ ଚୟନ ପାଠ୍ଯ ତାଲିକାରେ ଛାୟା ସଜ୍ଜିତ ସ୍ତମ୍ଭ (&i) ଉପକରଣ ସୂଚନା ପୃଷ୍ଠଭୂମୀ ଉପକରଣ ସୂଚନା ପାଠ୍ୟ ଭିନ୍ନ ହୋଇଥାଏ ପୃଷ୍ଠଭୂମୀ ଦେଖନ୍ତୁ ପାଠ୍ୟ ଦେଖନ୍ତୁ ପରିଦର୍ଶିତ ପାଠ୍ୟ ୱିଣ୍ଡୋ ପୃଷ୍ଠଭୂମୀ ୱିଣ୍ଡୋ ପାଠ୍ୟ ସେହି ଯୋଜନାକୁ ଅପସାରଣ କରିବା ପାଇଁ ଆପଣଙ୍କ ପାଖରେ ଅନୁମତି ନାହିଁ ସେହି ଯୋଜନାର ନବଲିଖନ କରିବା ପାଇଁ ଆପଣଙ୍କ ପାଖରେ ଅନୁମତି ନାହିଁ 