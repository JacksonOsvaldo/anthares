��          �   %   �      P     Q  $   b  $   �     �     �  )   �  /   �  \   %  X   �  *   �  /        6     H      Z     {     �     �  )   �     �  &     =   4     r     z  "   �  2   �  �  �  9   �  �   �  �   k     �     		  R   	  F   `	  J  �	  &  �
  �     �   �  1   M  C        �     �  )   �  K     o   [  b   �  h   .  �   �     x  q   �  w   �  h   o                            	                                                                               
                  &Run in terminal &Use KMail as preferred email client &Use Konsole as terminal application (c), 2002 Joseph Wenninger ... 1 second remaining: %1 seconds remaining: <qt>Open <b>http</b> and <b>https</b> URLs</qt> <qt>You changed the default component of your choice, do want to save that change now ?</qt> Choose from the list below which component should be used by default for the %1 service. Click here to browse for terminal program. Click here to browse for the mail program file. Component Chooser Default Component EMAIL OF TRANSLATORSYour emails Joseph Wenninger NAME OF TRANSLATORSYour names No description available Select preferred Web browser application: Select preferred email client: Select preferred terminal application: Select this option if you want to use any other mail program. Unknown Use a different &email client: Use a different &terminal program: in an application based on the contents of the URL Project-Id-Version: kcmcomponentchooser
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-11 07:06+0200
PO-Revision-Date: 2009-01-05 16:52+0530
Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>
Language-Team: Oriya <oriya-it@googlegroups.com>
Language: or
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);



X-Generator: KBabel 1.11.4
 ଟର୍ମିନାଲରେ ଚଲାନ୍ତୁ (&R) KMailକୁ ପସନ୍ଦ ଯୋଗ୍ୟ ଇମେଲ କ୍ଲାଏଣ୍ଟ ପରି ବ୍ୟବହାର କରନ୍ତୁ (&U) କୋନସୋଲକୁ ଟର୍ମିନାଲ ପ୍ରୟୋଗ ଭାବରେ ବ୍ୟବହାର କରନ୍ତୁ (&U) (c), 2002 Joseph Wenninger ... 1 ସେକଣ୍ଡ ବଳିଅଛି: %1 ସେକଣ୍ଡ ବଳିଅଛି: <qt>ଖୋଲନ୍ତୁ <b>http</b> ଏବଂ <b>https</b> URLs</qt> <qt>ଆପଣ ନିଜ ପସନ୍ଦରେ ପୂର୍ବନିର୍ଦ୍ଧାରିତ ଉପାଦାନକୁ ପରିବର୍ତ୍ତନ କରିଛନ୍ତି, ସେହି ପରିବର୍ତ୍ତନଗୁଡ଼ିକୁ ସଂରକ୍ଷଣ କରିବାକୁ ଚାହୁଁଛନ୍ତି କି ?</qt> %1 ସର୍ଭିସ ପାଇଁ କେଉଁ ଉପାଦାନଗୁଡ଼ିକୁ ପୂର୍ବନିର୍ଦ୍ଧାରିତ ଭାବେ ବ୍ୟବହାର କରିବା ଉଚିତ ତାହା ନିମ୍ନଲିଖିତ ତାଲିକାରୁ ବାଛନ୍ତୁ। ଟର୍ମିନାଲ ପ୍ରଗ୍ରାମ ବ୍ରାଉଜ କରିବା ପାଇଁ ଏଠାରେ କ୍ଲିକ କରନ୍ତୁ। ମେଲ ପ୍ରଗ୍ରାମ ଫାଇଲକୁ ବ୍ରାଉଜ କରିବା ପାଇଁ ଏଠାରେ କ୍ଲିକ କରନ୍ତୁ। ଉପାଦାନ ଚୟନକର୍ତ୍ତା ପୂର୍ବନିର୍ଦ୍ଧାରିତ ଉପାଦାନ mgiri@redhat.com Joseph Wenninger ମନୋଜ କୁମାର ଗିରି କୌଣସି ବର୍ଣ୍ଣନା ଉପଲବ୍ଧ ନାହିଁ ପସନ୍ଦ ଯୋଗ୍ୟ ୱେବ ବ୍ରାଉଜର ପ୍ରୟୋଗକୁ ବାଛନ୍ତୁ: ପସନ୍ଦ ଯୋଗ୍ୟ ଇମେଲ କ୍ଲାଏଣ୍ଟକୁ ବାଛନ୍ତୁ: ପସନ୍ଦ ଯୋଗ୍ୟ ଟର୍ମିନାଲ ପ୍ରୟୋଗକୁ ବାଛନ୍ତୁ: ଏହି ବିକଳ୍ପକୁ ବାଛନ୍ତୁ ଯଦି ଆପଣ ଅନ୍ୟ କୌଣସି ମେଲ ପ୍ରଗ୍ରାମକୁ ବ୍ୟବହାର କରିବାକୁ ଚାହୁଁଛନ୍ତି। ଅଜଣା ଭିନ୍ନ ଏକ ଇମେଲ କ୍ଲାଏଣ୍ଟକୁ ବ୍ୟବହାର କରନ୍ତୁ (&e): ଭିନ୍ନ ଏକ ଟର୍ମିନାଲ ପ୍ରଗ୍ରାମ ବ୍ୟବହାର କରନ୍ତୁ (&t): URL ବିଷୟବସ୍ତୁ ଉପରେ ଆଧାରିତ ଗୋଟିଏ ପ୍ରୟୋଗରେ 