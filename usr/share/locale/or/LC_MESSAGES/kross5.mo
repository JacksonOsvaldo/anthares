��            )   �      �  &   �     �     �     �      �     �                  ,   ;  3   h     �     �     �     �     �  '   �          ;     A     W     p     w     �     �     �  &   �     �  �  �     �  G   �     �          $  (   5  Z   ^  +   �  ]   �  f   C  �   �  P   =	  Z   �	     �	     �	     
  ?   .
  )   n
  
   �
  8   �
  G   �
     $  S   =     �  V   �  "   �  f   !     �                                       	                                              
                                                     @title:group Script propertiesGeneral Add a new script. Add... Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2009-01-02 17:37+0530
Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>
Language-Team: Oriya <oriya-it@googlegroups.com>
Language: or
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);




X-Generator: KBabel 1.11.4
 ସାଧାରଣ ନୂତନ ସ୍କ୍ରିପ୍ଟ ଯୋଗକରନ୍ତୁ। ଯୋଗକରନ୍ତୁ... ଟିପ୍ପଣୀ: mgiri@redhat.com ସମ୍ପାଦନ କରନ୍ତୁ ଚୟିତ ସ୍କ୍ରିପ୍ଟକୁ ସମ୍ପାଦନ କରନ୍ତୁ। ସମ୍ପାଦନ କରନ୍ତୁ... ଚୟିତ ସ୍କ୍ରିପ୍ଟକୁ ନିଷ୍ପାଦନ କରନ୍ତୁ। ଅନୁବାଦକ "%1" ପାଇଁ ସ୍କ୍ରିପ୍ଟ ତିଆରିରେ ବିଫଳ ସ୍କ୍ରିପ୍ଟ ଫାଇଲ "%1" ପାଇଁ ଅନୁବାଦକ ନିର୍ଦ୍ଧାରଣ କରିବାରେ ବିଫଳ ଅନୁବାଦକ "%1" କୁ ଧାରଣକରିବାରେ ବିଫଳ ସ୍କ୍ରିପ୍ଟ ଫାଇଲ "%1" କୁ ଖୋଲିବାରେ ବିଫଳ ଫାଇଲ: ଚିତ୍ରସଂକେତ: ଅନୁବାଦକ: Ruby ଅନୁବାଦକର ସୁରକ୍ଷାସ୍ତର ମନୋଜ କୁମାର ଗିରି ନାମ: ଏପରି କିଛି ଫଳନ ନାହିଁ "%1" ଏପରି କୌଣସି ଅନୁବାଦକ "%1" ନାହିଁ କାଢ଼ନ୍ତୁ ଚୟିତ ସ୍କ୍ରିପ୍ଟକୁ କାଢିଦିଅନ୍ତୁ। ଚଲାନ୍ତୁ ସ୍କ୍ରିପ୍ଟ ଫାଇଲ "%1" ଅବସ୍ଥିତ ନାହିଁ। ବିରାମ କରନ୍ତୁ ଚୟିତ ସ୍କ୍ରିପ୍ଟର ନିଷ୍ପାଦନ ବନ୍ଦକରନ୍ତୁ। ପାଠ୍ୟ: 