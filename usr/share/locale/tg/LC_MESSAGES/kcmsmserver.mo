��          �      ,      �     �     �     �  7  �  �  �  +   �  ^   �     W     g  x   o  �   �     �     �     �     �       �  5     �  $   �  &   	  �  ;	  �  6  T   �    C  8   K  
   �    �  e  �  ;   �  !   3  Y   U  :   �  )   �                                           	       
                            &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-16 13:49+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Тоҷикӣ
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Хуруҷи сеанс &Бозоғозии компютер &Хомӯшкунии компютер <h1>Диспетчер сеанса</h1> Здесь осуществляется настройка менеджера сеансов. Это включает в себя такие опции, как подтверждение выхода из KDE, восстановление параметров завершающегося сеанса при следующем запуске KDE и автоматическое выключение компьютера после выхода из системы. <ul>
<li><b>Сохранять предыдущий сеанс:</b> Все работающие приложения будут сохранены при выходе из сеанса и восстановлены при открытии нового сеанса.</li>
<li><b>Восстанавливать сохраненный вручную сеанс: </b> Позволяет сохранить сеанс в любое время командой "Сохранить сеанс" в К-меню. Это означает, что запущенные в этот момент придожения буудт сохранены и затем восстановлены при открытии нового сеанса.</li>
<li><b>Начинать с пустого сеанса:</b> Не сохранять ничего. При открытии нового сеанса начинать с чистого рабочего стола.</li>
</ul> Барномаҳо аз сеанс ба &кор дароварда мешаванд: Ин интихоботро интихоб кунед, агар шумо хоҳед ки роҳбари сессия қуттии диалогро барои тасдиқ кардани баромадани аз система ба шумо нишон диҳад. &Хуруҷи системаро тасдиқ кунед Умумӣ Дар ин ҷо шумо метавонед, дар вақти баромадани аз система бо пешфарз, интихоб кунед. Ин маънои онро дорад ки, агар шумо аз KDM ба система дароед. Здесь вы можете ввести разделённый запятыми список приложений, которые не должны сохраняться в сеансе и поэтому не будут запускаться при восстановлении сеанса. Например: «xterm:konsole» или «xterm,konsole». &Танзимоти имконотҳои хомӯшкунӣ Ҳангоми воридшавӣ Сеанси &дастурамали захирашударо барқарор кунед Сеанси &пешинаро барқарор кунед Оғозкунии сеанси &холӣ 