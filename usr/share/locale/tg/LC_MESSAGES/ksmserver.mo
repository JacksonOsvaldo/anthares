��          D      l       �      �      �   ,   �   c   �   �  P  =   �  /   &  {   V  �   �                          Also allow remote connections Logout canceled by '%1' Restores the saved user session if available The reliable KDE session manager that talks the standard X11R6 
session management protocol (XSMP). Project-Id-Version: ksmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-05 03:18+0100
PO-Revision-Date: 2009-01-11 16:23+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 Пайвастшавии дурдаст иҷоза диҳед Хуруҷ манъ карда шуд бо '%1' Барқароркунии сеансҳои корванди захирашуда, агар имконпазир бошад Мудири боэътимоди сеансҳои KDE, ки қарордоди идораи сеансҳои 
муқаррарии X11R6-ро истифода мебарад (XSMP). 