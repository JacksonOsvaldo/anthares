��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �  
   �  #   �     �     �  	   �     �       5        T  9   d  ,   �  i   �  A   5     w  	   �     �     �  '   �     �     	  2   	     >	     W	  9   o	     �	  2   �	     �	  P   �	  	   ?
                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-06-04 17:36+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik Language
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=2; plural=n != 1;
 Умумӣ Иловаи скрипти нав. Илова... Отменить? Эзоҳ: victor.ibragimov@gmail.com Таҳрир Таҳрири скрипти интихобшуда. Таҳрир... Иҷрокунии скрипти интихобшуда. Failed to create script for interpreter "%1" Муайянкунии муфассир барои файлскрипти "%1" қатъ карда шуд Боркунии муфассир қатъ карда шуд "%1" Failed to open scriptfile "%1" Файл: Нишона: Интерпретатор: Level of safety of the Ruby interpreter Victor Ibragimov Ном: Чунин вазифа мавҷуд нест "%1" No such interpreter "%1" Нобуд кардан Несткунии скрипти интихобшуда. Оғоз Скриптфайли "%1" мавҷуд нест. Манъ Остановить выполнение выбранного сценария. Матн: 