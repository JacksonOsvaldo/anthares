��          �      L      �  0   �     �                2  H   E     �  &   �     �     �     �             /   &  -   V  *   �  *   �  �   �  �  r  0         8     Y     e  !   {  �   �  	   0  N   :     �     �     �  *   �       F     ;   [  e   �  c   �  �   a	                                                                         	                    
       (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) Command to execute David Faure EMAIL OF TRANSLATORSYour emails Iconify the window Jump to the window even if it is started on a 
different virtual desktop KStart Make the window appear on all desktops Matthias Ettrich Maximize the window NAME OF TRANSLATORSYour names No command specified Richard J. Moore The window does not get an entry in the taskbar The window does not get an entry on the pager Try to keep the window above other windows Try to keep the window below other windows Utility to launch applications with special window properties 
such as iconified, maximized, a certain virtual desktop, a special decoration
and so on. Project-Id-Version: kstart
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-07 03:59+0100
PO-Revision-Date: 2009-01-11 13:35+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) Фармон барои иҷро David Faure H_Abrorova@rambler.ru Печонидани тиреза Гузариш ба тиреза, ҳатто агар ки он дар мизи 
кории маҷозии дигар оғоз ёфта бошад KОғоз Тиреза дар ҳамаи мизҳои корӣ пайдо мешавад Matthias Ettrich Кушодани тиреза Абророва Хиромон Фармон таъин нашудааст Richard J. Moore Тиреза ба панели супоришҳо намедарояд Тиреза ба саҳифабанд намедарояд Кӯшиши боло баровардани тиреза аз болои дигар тирезаҳо Кӯшиши поён фаровардани тиреза ба зери дигар тирезаҳо Барномаи пуштибон барои сардиҳии замима бо хосиятҳои махсуси 
тирезавӣ, ба монанди: печонидан, мизи кории маҷозӣ, ороиши махсус
ва ғайра. 