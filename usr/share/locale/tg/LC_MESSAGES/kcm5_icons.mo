��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     &     5     C     _     t     �  �   �  <   O  
   �  
   �     �  �   �     b     r     �  
   �     �     �     �     �     �       8        X  #   s  
   �     �  <   �     �  =   �  �   ;  W     6   i     �     �     �     �     �  $   �  :        T  5   s  9   �  7   �          )  b   E  S   �  &   �     #     :     V  �   c  N   �     D  �  d               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2010-01-28 14:27+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Тоҷикӣ
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Миқдор: &Таъсир &Ранги иловагӣ: &Ним шаффоф М&авзӯъ (c) 2000-2003 Geert Jansen <qt>Удалить набор значков <strong>%1</strong>?<br /><br />Будут удалены все файлы принадлежащие этому набору.</qt> <qt>Насб кунии<strong>%1</strong> мавзӯъ</qt> Фаъол Хомӯш Стандартӣ Произошла ошибка во время установки. Тем не менее, некоторые файлы  из архива были установлены &Иловагӣ Ҳамма Нишонаҳо Antonio Larrosa Jimenez Ран&г: Рангкунӣ Тасдиқ Серранг накардан Тасвир Мизи корӣ Диалогҳо Мавзӯи URL Кашондан ё Чоп кардан victor.ibragimov@gmail.com Таъсири Параметрҳо Анвоъ Geert Jansen Получить новую тему из Интернета Нишонаҳо Нишонаҳои Роҳбари Пайраҳаи Модул Если архив с набором значков уже сохранён локально, это действие распакует его и сделает доступным для приложений KDE Установить тему из сохранённого локально файла Основная панель инструментов Victor Ibragimov Ном Бе Таъсир  Пайраҳа Нигоҳи пешакӣ Мавзӯъ нобун кардан Удалить выделенную тему с диска Сохтори таъсир... Насби Таъсири Нишонаи Активӣ Насби Таъсири Нишонаи Хомушона Насби Таъсири Нишонаи Нотавон Андоза: Нишонаҳои хурд Указанный файл не является архивом с набором значков. Выделенная тема будет удалена с вашего диска. То Ранги Хокистаранг Монохромные Пайраҳаи Асбоб Torsten Rahn Не удаётся загрузить архив с набором значков.
Проверьте правильность адреса: %1. Не удаётся найти архив с набором значков %1. Истифодаи Нишона Для выполнения этого действия необходимо подключение к Интернету. Вам будет показан список всех наборов значков с сайта http://www.kde.org. Нажатие кнопки напротив набора значков приведёт к его установке на компьютер. 