��          �            x     y  $  �     �  	   �  	   �  F   �       
        '  
   .     9  	   B     L  	   Q     [  �  h  4   �  i  4     �
     �
  !   �
  u   �
     G  !   S  	   u  !        �     �  	   �     �  /   �            
                                     	                              &Select test picture: <h1>Monitor Gamma</h1> This is a tool for changing monitor gamma correction. Use the four sliders to define the gamma correction either as a single value, or separately for the red, green and blue components. You may need to correct the brightness and contrast settings of your monitor for good results. The test images help you to find proper settings.<br> You can save them system-wide to XF86Config (root access is required for that) or to your own KDE settings. On multi head systems you can correct the gamma values separately for all screens. Blue: CMY Scale Dark Gray Gamma correction is not supported by your graphics hardware or driver. Gamma: Gray Scale Green: Light Gray Mid Gray RGB Scale Red: Screen %1 Sync screens Project-Id-Version: kgamma
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-18 03:02+0200
PO-Revision-Date: 2004-08-04 15:07+0500
Last-Translator: Victor Ibragimov <youth_opportunities@tajik.net>
Language-Team:  Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
Plural-Forms: nplurals=2; plural=n != 1;
 &Интихоби тасвироти озмоишӣ: <h1>Анвои тасҳеҳоти монитор</h1> Ин барнома иҷозати тасҳеҳ кардани анвои мониторро медиҳад. Чаҳор чархакро барои таъғири анвоъ ҳамчун аҳамияти бутун ё ҷудо аз сурх, сабз ва кабуд таркибёфта низ истифода кунед. Ба қадри имкон, шумо бояд равшанӣ ва тазоди мониторро барои хубтар намудани натиҷот тасҳеҳ кунед. Тасвироти озмоишӣ барои ёфтани мутобиқоти лозимӣ кӯмак мерасонад.<br> Шумо метавонед танзимотҳои анвоъро дар файли XF86Config (танҳо истифодакунандаи root барои қайд дар ин файл ё дар танзимотҳои инфиродии KDE-и шумо имтиёз дорад. Ҳангоми истифодаи якчанд монитор, анвоъ-тасҳеҳотро барои ҳар яки он гузаронидан лозим аст. Кабуд: CMY  Хокистарии баланд Анвои тасҳеҳот бо видеокорт ё драйвери шумо дастгирӣ намешавад. Анвоъ: Заминаи хокистарӣ Сабз: Хокистарии равшан Хокистаранг RGB Сурх: Экрани %1 Синхронизонидани экранҳо 