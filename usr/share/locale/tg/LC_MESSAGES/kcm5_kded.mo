��          �      |      �     �                (     I     ]     u     �     �     �     �     �     �     �  �   �     l  #   �  @   �  ?   �     )     -  �  5     �     �        /     '   =  H   e  8   �  %   �  #        1  
   >  %   I  
   o     z  A  �  =   �  H   	  h   L	  h   �	     
     /
     
                                         	                                                   (c) 2002 Daniel Molkentin Daniel Molkentin Description EMAIL OF TRANSLATORSYour emails KDE Service Manager Load-on-Demand Services NAME OF TRANSLATORSYour names Not running Running Service Start Startup Services Status Stop This is a list of available KDE services which will be started on demand. They are only listed for convenience, as you cannot manipulate these services. Unable to contact KDED. Unable to start server <em>%1</em>. Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i> Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i> Use kcmkded Project-Id-Version: kcmkded
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-16 03:36+0100
PO-Revision-Date: 2009-01-16 13:41+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik Language
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002 Daniel Molkentin Daniel Molkentin Тасвир victor.ibragimov@gmail.com, rkovacs@khujand.org Хидмати идоракунии KDE Хизматҳои боргузори дар ҳангоми тақозо Виктор Ибрагимов, Роҷер Ковакс Дар ҳолати иҷро нест Дар ҳолати иҷро аст Хизмат Шуруъ Хизматҳои Роҳандозӣ Ҳолат Манъ Ин рӯйхат аз хизматҳои мавҷуд дар KDE мебошад, ки ҳангоми тақозо оғоз мегардад. Онҳо фақат барои роҳати шумо аст, дар он ҷое ки шумо наметавонед дар хизматхои онҳо дастгири кунед Ба KDED алоқаманд карда натавонист. Хидматгоҳи <em>%1</em> оғоз карда намешавад. Хидматгоҳи <em>%1</em> оғоз карда намешавад..<br /><br /><i>Хато: %2</i> Хидматгоҳи <em>%1</em> манъ карда намешавад..<br /><br /><i>Хато: %2</i> Истифода kcmkded 