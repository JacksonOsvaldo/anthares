��    	      d      �       �   �   �   9   r  7   �  
   �     �     �       
   '  �  2  �   �  K   �  b        t     �  ;   �  ,   �                    	                               Check this option if the application you want to run is a text mode application. The application will then be run in a terminal emulator window. Enter the password here for the user you specified above. Enter the user you want to run the application as here. Pass&word: Run %1 Run as a different &user Run in &terminal window User&name: Project-Id-Version: krunner_shellrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2009-01-13 17:41+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
 Интихоб кунед, агар шумо мехоҳед барномаро дар ҳолати матнӣ иҷро кунед. Баъд аз ин барнома ба воситаи терминал иҷро карда мешавад. Пароли корбари муайяншударо ворид кунед. Номи корбарро барои иҷро кардани барнома ворид кунед. &Парол: Иҷро кардани %1 Ҳамчун корбари &дигар иҷро кунед Дар &терминал иҷро кунед &Номи корбар: 