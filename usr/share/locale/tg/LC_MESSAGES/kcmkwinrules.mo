��    V      �     |      x  
   y     �     �     �     �  
   �     �  	   �     �  "   �                .     ;  	   K     U     \     d     m     t     |     �     �     �     �      �     �     �  O   �     2	     >	     F	     L	  !   Q	      s	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     #
  
   9
     D
     M
     l
     y
     �
     �
     �
     �
     �
     �
  -   �
     �
                 &   #     J     X     d     j     x     �     �     �     �     �     �     �     �  *   �               .     ;     J  -   Y    �  �  �     )     E  /   X     �  $   �     �  
   �     �     �  0        5     F     _     ~     �     �     �     �  	   �                )     ;  #   [          �     �     �  �   �  %   g     �     �     �  ?   �  K   �     H  &   M  &   t     �     �  $   �     �  "   �       J      (   k  (   �     �     �     �          1  !   D      f     �     �     �     �  q   �     c      q     �     �  g   �  -   *  &   X       )   �  3   �  #   �          "     >     ]  	   u          �  l   �       0   '     X     o  !   �  V   �  .       .   8   N   D   B       *          G            P   (   ;                  V   <      I       O              !              3   M                 5      1   >   R   -      A   2           U   4       T      6   Q   )      F              J      +   $               :   %              7   K   #          /       H                0   ?                     C   =                          ,   &   9                   S   	          E   @          
      "       L              '    &Closeable &Desktop &Focus stealing prevention &Fullscreen &Machine (hostname): &Modify... &New... &Position &Size (c) 2004 KWin and KControl Authors 0123456789-+,xX: Accept &focus All Desktops Apply Initially Apply Now C&lear Cascade Centered Class: Default Delete Desktop Dialog Window Do Not Affect Dock (panel) EMAIL OF TRANSLATORSYour emails Edit Edit... Enable this checkbox to alter this window property for the specified window(s). Exact Match Extreme Force High Information About Selected Window Internal setting for remembering KWin Keep &above Keep &below Low Lubos Lunak M&aximum size M&inimized M&inimum size Machine: Match w&hole window class Maximized &horizontally Maximized &vertically Move &Down Move &Up NAME OF TRANSLATORSYour names No Placement Normal Normal Window On Main Window Override Type Random Regular Expression Remember Remember settings separately for every window Role: Settings for %1 Sh&aded Shortcut Show internal settings for remembering Skip &taskbar Skip pa&ger Smart Splash Screen Standalone Menubar Substring Match Title: Toolbar Top-Left Corner Torn-Off Menu Type: Under Mouse Unimportant Unknown - will be treated as Normal Window Unnamed entry Utility Window Window &type Window &types: Window t&itle: Window-Specific Settings Configuration Module You have specified the window class as unimportant.
This means the settings will possibly apply to windows from all applications. If you really want to create a generic setting, it is recommended you at least limit the window types to avoid special window types. Project-Id-Version: kcmkwinrules
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2009-01-11 15:34+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Пӯшидашаванда &Мизи Корӣ Пешгирии дуздидани &фокус &Экрани пурра &Мошина (номи соҳиб): &Тағирдиҳӣ... &Нав... &Мавқеъ &Андоза (c) 2004 KWin ва Муаллифони KControl 0123456789-+,xX: Қабули &фокус Ҳама Мизҳои Корӣ Қабули Ибтидоӣ Қабул кунед &Тоза кардан Қабат кардан Марказонида Синф: Стандартӣ Нест кардан Мизи Корӣ Тирезаи Муколама Таъсир Намерасонад Даргоҳ (панел) H_Abrorova@rambler.ru Таҳрир Таҳрир... Барои тағири хусусиятҳои тиреза барои тиреза(ҳо)и таъиншуда, ин қуттиро фаъол созед. Мувофиқати Дақиқона Экстрималӣ Таъсир Баланд Ахборот Оиди Тирезаи Интихобгашта Гузоришҳои дохилӣ барои дар хотир доштан KWin Дар &боло нигоҳ доред Дар &поён нигоҳ доред Паст Lubos Lunak Андозаи &калонтарин &Печонидашуда Андозаи &хурдтарин Мошина: Ба &ҳамаи синфи тиреза мувофиқат мекунад Ба таври &уфуқӣ кушоед Ба таври &амудӣ кушоед Ҳаракат &Ба поён Ҳаракат &Ба боло Абророва Хиромон Ҷойгирӣ нест Муқаррирӣ Тирезаи Муқаррарӣ Дар Тирезаи Асосӣ Радкунии Навъ Тасодуфӣ Ифодаи Муқаррарӣ Дар хотир доштан Барои ҳар як тиреза гузоришҳоро дар алоҳидигӣ дар хотир доред Вазифа: Гузоришҳо барои %1 &Сояпартофта Тугмаҳои тез Барои дар хотир доштан гузоришҳои дохилиро намоиш диҳед Радкунии &панели супориш Радкунии &саҳифабанд Ҳушманд Экрани ба рӯй бароянда Панели Менюи алоҳида истода Мувофиқати Зерсатр Сарлавҳа: Панели асбобҳо Кунҷи Чапи Болоӣ Кандани Меню Навъ: Зери Муш Номуҳим Номаълум - ҳамчун Тирезаи Муқаррарӣ маънидод карда мешавад Элементи беном Тирезаи Барномаи пуштибон &Навъ тиреза &Навъҳои тиреза: &Сарлавҳаи тиреза: Тиреза-Модули Танзимкунии Гузоришҳои Мушаххас Шумо синфи тирезаро ҳамчун номуҳим муайян намудед.
Ин маънои онро дорад, ки гузоришҳо эҳтимолан ба тиреза аз ҳамаи замимаҳо қабул мегарданд. Агар дар ҳақиқат хоҳед, ки гузоришҳои умумиро офаред, ба шумо маслиҳат дода мешавад, ки барои пешгирии навъҳои махсуси тиреза, ақалан навъҳои тирезаро маҳдуд кунед. 