��          t      �              $   1     V     s     �     �  '   �  �   �  �   �     O  �  g  4     8   <  3   u  B   �     �  ;   �  S   6  �   �  �   d  +   8                   	       
                                  Could not find '%1' executable. Could not find 'kdemain' in '%1'.
%2 Could not find service '%1'. Could not open library '%1'.
%2 Launching %1 Service '%1' is malformatted. Service '%1' must be executable to run. Unable to create new process.
The system may have reached the maximum number of processes possible or the maximum number of processes that you are allowed to use has been reached. Unable to start new process.
The system may have reached the maximum number of open files possible or the maximum number of open files that you are allowed to use has been reached. Unknown protocol '%1'.
 Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-04 03:08+0100
PO-Revision-Date: 2013-06-04 17:36+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik Language
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=2; plural=n != 1;
 Барнома бо номи '%1' ёфт нашуд. Не удалось найти «kdemain» в «%1».
%2 Не удалось найти службу «%1». Не удалось открыть библиотеку «%1».
%2 Оғози %1 Служба '%1' имеет неверный формат. Хидмати '%1' барои иҷро бояд иҷрошаванда бошад. Не удалось создать процесс.
Возможно, превышен предел количества запущенных процессов в системе или для пользователя. Не удалось запустить процесс.
Возможно, превышен предел количества открытых файлов в системе или для пользователя. Протоколи номуайян: '%1'.
 