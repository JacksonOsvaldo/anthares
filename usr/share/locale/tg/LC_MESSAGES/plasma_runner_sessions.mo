��          �      ,      �  �  �     N     ^     j          �     �     �     �     �     �           &  "   G     j     v  �  �  $  3     X	     n	  #   �	  *   �	  (   �	     �	     
  
   
  
   &
     1
     E
     ^
     o
     �
     �
                                            	             
                     <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lock the screen New Session Restart the computer Shutdown the computer Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-13 17:25+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
 <p>Шумо сеанси мизи кории дигарро интихоб кардед.<br />Сеанси ҷорӣ пинҳон карда мешавад ва экрани вуруди нав нишон дода мешавад.<br />Тугмаи F барои ҳар як сеанс таъин карда шуд; Тугмаи F%1 барои сеанси аввалин таъин карда шуд, тугмаи F%2 барои сеанси дуввум таъин карда шуд ва дигар. Шумо метавонед бо истифодаи тугмаҳои Ctrl, Alt ва тугмаи F мизҳои кориро мубодила кунед. Шумо ҳам метавонед панели KDE ва менюи мизи корӣ дар байни сеансҳо мубодила кунед.</p> Қулфи экран Сеанси нав Бозоғозии компютер Хомӯш кардани компютер Огоҳинома - Сеанси нав Қулф Хуруҷ кардан Хуруҷ Хуруҷ Сеанси нав Азнавборкунӣ Бозоғозӣ Хомӯш кардан Мубодилаи корбар Мубодила 