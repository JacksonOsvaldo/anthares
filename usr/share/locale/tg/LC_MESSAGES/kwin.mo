��    P      �  k         �     �     �     �     �  
   �     �       #   *  &   N     u     �     �  
   �     �      �     �  ,      +   -  /   Y     �     �     �  p   �     &	     9	     L	     e	     ~	     �	     �	     �	  	   �	  
   �	     �	     �	     �	     
     ,
  	   G
     Q
     a
     m
  (   �
  #   �
     �
     �
          3     Q     b     q     �     �  9   �     �     �               %     =     S     r     �     �     �  :   �  '        =     W  #   q     �     �     �     �     �         ,  �   A  S     �  s  !        (     4     M  
   e  /   p  &   �  O   �  E        ]     {     �     �  N   �       .     J   L  D   �  J   �  #   '     K  9   P  �   �  2   D  4   w  @   �  B   �  I   0  $   z     �     �     �     �  E   �     =     N  (   l  (   �     �  !   �  &   �       I   ,  F   v  ?   �  ?   �  =   =  =   {  -   �  -   �  8     :   N     �  h   �  '     !   :  (   \  8   �  8   �  8   �  C   0  E   t  2   �  J   �  6   8  �   o  _      A   `  3   �  5   �  "      $   /      T      r   %   �   O   �     !  f  ##  �   �$     !              3      A   K      6   H              <   N   7       L   0      P         I             C   >   =       5      
       -             2              8   $   :   (            ?   M      .   9      /      @       )   D       J   *          ,   ;         O                             '   #       +   1   B       G                  &   	   E         4   %           "   F               &All Desktops &Close &Fullscreen &Move &No Border &Terminate Application %1 Activate Window (%1) Activate Window Demanding Attention Caption of the window to be terminated Close Window Cristian Tibirna Daniel M. Duley Desktop %1 Disable configuration options EMAIL OF TRANSLATORSYour emails Hide Window Border Hostname on which the application is running ID of resource belonging to the application Indicate that KWin has recently crashed n times KDE window manager KWin KWin helper utility KWin is unstable.
It seems to have crashed several times in a row.
You can select another window manager to run: Keep &Above Others Keep &Below Others Keep Window Above Others Keep Window Below Others Keep Window on All Desktops Kill Window Lower Window Luboš Luňák Ma&ximize Maintainer Make Window Fullscreen Matthias Ettrich Maximize Window Maximize Window Horizontally Maximize Window Vertically Mi&nimize Minimize Window Move Window NAME OF TRANSLATORSYour names Name of the application to be terminated PID of the application to terminate Pack Grow Window Horizontally Pack Grow Window Vertically Pack Shrink Window Horizontally Pack Shrink Window Vertically Pack Window Down Pack Window Up Pack Window to the Left Pack Window to the Right Raise Window Replace already-running ICCCM2.0-compliant window manager Resize Window Shade Window Show Desktop Suspend Compositing Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Next Desktop Switch to Next Screen Switch to Previous Desktop This helper utility is not supposed to be called directly. Time of user action causing termination Toggle Window Raise/Lower Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Walk Through Windows Walk Through Windows (Reverse) Window Operations Menu Window to Next Screen You have selected to show a window in fullscreen mode.
If the application itself does not have an option to turn the fullscreen mode off you will not be able to disable it again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. You have selected to show a window without its border.
Without the border, you will not be able to enable the border again using the mouse: use the window operations menu instead, activated using the %1 keyboard shortcut. kwin: unable to claim manager selection, another wm running? (try using --replace)
 Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:16+0100
PO-Revision-Date: 2010-01-29 22:34+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 &Ҳамаи Мизҳои корӣ &Хуруҷ &Тамоми экран &Ҷойивазкунӣ &No Border &Завершить приложение «%1» Активировать окно (%1) Фаъолкунии Аҳамияти Дархостшудаи Тирезавӣ Заголовок окна, которое нужно закрыть Пӯшидани Тиреза Cristian Tibirna Daniel M. Duley Мизи кории %1 Батанзимдарории интихобҳо ғайри имкон аст victor.ibragimov@gmail.com Русткунии Сарҳади Тиреза Имя узла, на котором работает приложение ID ресурса, принадлежащего приложению Показывает, что сбои KWin происходили n раз Мудири Тирезаҳои KDE KWin KWin барномаи пуштибони ёрирасон KWin нестабилен.
Произошло несколько серьёзных ошибок подряд.
Вы можете выбрать другой диспетчер окон: Нигаҳдорӣ &Аз болои Дигарҳо Нигаҳдорӣ &Дар поёни Дигарҳо Нигаҳдории Тиреза аз Болои Дигарҳо Нигаҳдории Тиреза дар Поёни Дигарҳо Нигаҳдории Тиреза дар Ҳамаи Мизҳои корӣ Нест кардани Тиреза Поёнкунии Тиреза Luboš Luňák &Кушодан Тайёркунанда Тамоми Саҳфанамоиши Тирезаро Мекунад Matthias Ettrich Кушодани Тиреза Уфуқӣ Кушодани Тиреза Амудӣ Кушодани Тиреза &Печонидан Печонидани Тиреза Ҷой ивазкунии Тиреза Victor Ibragimov Имя приложения, которое нужно завершить PID приложения, которое нужно завершить Уфуқӣ Фишурдани Тирезаи Колоншуда Амудӣ Фишурдани Тирезаи Колоншуда Уфуқӣ Фишурдани Тирезаи Хурдшуда Амудӣ Фишурдани Тирезаи Хурдшуда Фишурдани Тиреза ба Поён Фишурдани Тиреза ба Боло Фишурдани Тиреза ба тарафи Чап Фишурдани Тиреза ба тарафи Рост Болокунии Тиреза Заменить уже запущенный ICCCM2.0-совместимый диспетчер окон Бозандозгирии Тиреза Ҳифзпардаи Тиреза Показать рабочий стол Отключить графические эффекты Гузариш ба Як Мизи корӣ ба Поён Гузариш ба Як Мизи корӣ ба Боло Гузариш ба Як Мизи корӣ ба тарафи Чап Гузариш ба Як Мизи корӣ ба тарафи Рост Гузариш ба Мизи кории Оянда Переключиться на следующий рабочий стол Гузариш ба Мизи кории Гузашта Вспомогательная утилита не должна запускаться как самостоятельная программа. Время действия пользователя, вызвавшего завершение Зомини Тирезавии Болокунӣ/Поёнкунӣ Ба як Фаҳристи Мизи Корӣ Пеш Ба як Фаҳристи Мизи Корӣ Пушт Ба як Мизи Корӣ Пеш Ба як Мизи Корӣ Пушт Ба як Тиреза Пеш Ба як Тиреза Пушт Менюи Амал бо Тиреза Переместить окно на следующий рабочий стол Вы решили отображать окно в полноэкранном режиме.
Если само приложение не имеет параметра для отключения полноэкранного режима, вы не сможете отключить полноэкранный режим снова при помощи мыши. Используйте вместо этого меню действий с окном, которое можно активировать комбинацией клавиш %1. Вы решили отображать окно без рамки.
Без рамки, вы не сможете включить рамку снова при помощи мыши. Используйте вместо этого меню действий с окном, которое можно активировать комбинацией клавиш %1. kwin: талаби интихобкунии мудир ғайри имкон аст, дигар wm-ро корандози кунам? (кӯшиш кунед ҷойивазкуниро истифода баред)
 