��          �      ,      �  	   �  P   �  �   �  R   �  e     P   h  M   �  x     H   �     �     �  4   �     -  
   =     H     `  �  s       g        �  n   �  �   	  r   �	  g   �	  �   g
  e        }  /   �  S   �     !     :  0   M  %   ~         	                                                
                     
Syntax:
   kioclient cat 'url'
            # Writes out the contents of 'url' to stdout

   kioclient download ['src']
            # Copies the URL 'src' to a user-specified location'.
            #   'src' may be a list of URLs, if not present then
            #   a URL will be requested.

   kioclient exec .
             // Opens the current directory. Very convenient.

   kioclient exec file:/home/weis/data/test.html
             // Opens the file with default binding

   kioclient exec file:/root/Desktop/emacs.desktop
             // Starts emacs

   kioclient exec ftp://localhost/
             // Opens new window with URL

   kioclient move 'src' 'dest'
            # Moves the URL 'src' to 'dest'.
            #   'src' may be a list of URLs.
   kioclient openProperties 'url'
            # Opens a properties menu

 Arguments for command Command (see --commands) Command-line tool for network-transparent operations Destination URL KIO Client Show available commands Source URL or URLs Project-Id-Version: kioclient
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-01-13 13:08+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.2
Plural-Forms: nplurals=2; plural=n != 1;
 
Синтакс:
   kioclient cat 'url'
            # Дорои файлро аз 'url' ба stdout менависад

   kioclient download ['src']
            #  URL 'src' ба ҷои муайяншуда нусха мемонад.
            #   'src' метавонад ҳамчун рӯйхати URL бошад.
            #    URL номавҷуд дархост мешавад.

   kioclient exec .
             // Ҷузвдони ҷориро мекушояд. Хеле оддӣ аст.

   kioclient exec file:/home/weis/data/test.html
             // Файлро бо васлҳои стандартӣ мекушояд

   kioclient exec file:/root/Desktop/emacs.desktop
             // Барномаи emacs оғоз мекунад

   kioclient exec ftp://localhost/
             // Тирезаи навро бо URL мекушояд

   kioclient move 'src' 'dest'
            # Таҳвилкунии URL 'src' ба 'dest'.
            #   'src' метавонад ҳамчун рӯйхати URL бошад.
   kioclient openProperties 'url'
            # Менюи хусусиятҳоро мекушояд

 Вурудҳои фармонӣ Фармон (ворид кунед --commands) Барномаи фармонҳо барои иҷрои амалҳои шабака Ҷойришавии URL Корбари KIO Намоиши фармонҳои дастрас Манбаъи Source URL ё URL'ҳо 