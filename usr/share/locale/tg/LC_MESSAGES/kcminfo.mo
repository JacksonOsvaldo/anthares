��    C      4  Y   L      �  '   �  f   �     @     L  7   ]  y   �          !     (     ;     G      X     y     �     �     �  
   �      �  
   �     �     �  	   �            	        #     ,     ;     D     M     b     h     {     �     �     �     �     �     �     
	     	     "	     3	     ;	     M	  
   a	     l	     {	     �	     �	     �	     �	  F   �	  ?   
     V
     [
     l
     t
     z
     �
     �
     �
     �
  "   �
     �
  	   �
  �    K   �  B   �     2  )   >  7   h  �   �  "   �     �     �     �  #   �  B     7   I     �     �     �     �     �     �     �               3  +   7     c     y  )   �     �     �  =   �  
      .        :     K     R     j  @   |  .   �  %   �            #   >     b  1   r     �     �  &   �  *   �          7  0   F  =   w     �  �   5  
   �  &   �  +        1     >  ?   K     �  &   �     �  :   �  &        =     '   -               	   )          =   1       
                    C                5   B      $              3   7              <      :   6          !   #                 "   .   0          +   2      ,   &              >       A   *          9   /           ?      8                     (      ;   %       4   @          %1 BPP, Depth: %2, Scanline padding: %3 %1 is one of the modules of the kinfocenter, cpu info, os info, etcNo information available about %1. %1 x %2 dpi (Default Screen) (c) 2008 Nicolas Ternisien
(c) 1998 - 2002 Helge Deller All the information modules return information about a certain aspect of your computer hardware or your operating system. Available Screens Bitmap Black %1, White %2 DMA-Channel Default Colormap Default Number of Colormap Cells Depth of Root Window Depths (%1) Description Device Dimensions EMAIL OF TRANSLATORSYour emails Event = %1 Helge Deller I/O-Port I/O-Range IRQ Image Byte Order Interrupt LSBFirst Largest Cursor Location MSBFirst Maximum Request Size Model Motion Buffer Size NAME OF TRANSLATORSYour names Name Name of the Display Nicolas Ternisien No I/O port devices found. No PCI devices found. Number of Colormaps Order PA-RISC Processor PA-RISC Revision Padding Pixmap Format #%1 Preallocated Pixels Resolution Root Window ID Screen # %1 Server Information Status Supported Extensions Supported Pixmap Formats The PCI subsystem could not be queried, this may need root privileges. This list displays system information on the selected category. Unit Unknown Order %1 Used By Value Vendor Release Number Vendor String Version Number When mapped X-Server backing-store: %1, save-unders: %2 minimum %1, maximum %2 unlimited Project-Id-Version: kcminfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-09 06:04+0100
PO-Revision-Date: 2009-01-11 14:58+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Тоҷикӣ
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.1
Plural-Forms: nplurals=2; plural=n != 1;
 %1 бит ба пиксел, Чуқурӣ: %2, бит, scanline padding: %3 Иттилооти дар бораи %1 дастнорас аст. %1 x %2 dpi (Сафҳанамоиши пешфарз) (c) 2008 Nicolas Ternisien
(c) 1998 - 2002 Helge Deller Ҳамаи бахшҳои иттилооте, иттилооти хосе дар мавриди мавзӯъоти хосе аз компютери шумо ё системаи омилатонро бармегардонанд. Экранҳои  дастрасӣ Битнақш Сиёҳ %1, Сафед %2 Канали DMA Нақшаранги пешфарз Миқдори ранг дар нақшаранги пешфарз Бит ба пиксел дар решаи тиреза Бит ба пиксел (%1) Тавзеҳ Дастгоҳ Андозаҳо youth_opportunities@tajik.net Рӯйдоди = %1 Helge Deller Даргоҳи I/O Маҳдудаи I/O IRQ Тартиби байт дар тасвир Қатъ кардан LSBFirst Бузургтарин маконнамо Ҷойгоҳ MSBFirst Бештарин андозаи мавриди дархост Модул Андозаи миёнгири ҳаракат Tajik KDE Teams  Ном Номи дисплей Nicolas Ternisien Ҳеҷ дастгоҳ ва даргоҳи I/O ёфт нашуд. Ҳеҷ дастгоҳи PCI ёфт нашуд. Миқдори нақшарангҳо Тартиб Протсессори PA-RISC Таҷдиди назари PA-RISC Пур кунӣ Андозаи акснақши шумораи %1 Preallocated Pixels Иҷозат Шиносои решаи тиреза Сафҳанамоиши шумораи %1 Иттилооти сервер Вазъият Пасвандҳои пуштибонӣ шуда Андозаи акснақшҳои пуштибон шуда Субсистемаи PCI тафтиш нашудааст, ин мумкин ба имтиёзи root эҳтиёҷ бошад. Ин рӯйхати иттилооти марбут ба системаи шуморо дар мавзӯи интихоб шуда нишон медиҳад. Воҳид Тартиби ношинохтаи %1 Истифодашудаи тавассут Мазмун Тафсир Ахборот дар бораи истеҳсолкунанда Адади тафсир Вақти равшан буданаш Сервери X бозгашт-захира: %1, захира-таҳт: %2 минимум %1, максимум %2 номаҳдуд 