��    Q      �  m   ,      �  !   �       	        #  S   ,  	   �     �     �     �     �     �     �     �     �     �     	  J        g     o      {  	   �  
   �     �     �     �     �     �     �  L   �  	   H	  	   R	  
   \	  
   g	  
   r	     }	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
     
     
     %
     3
     7
     G
     O
     \
  
   o
  	   z
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     &     <     B     F     J  H   Q     �     �     �     �     �     �  
   �  	   �  �  �  /   �       	        %  2   .     a     t     �     �     �     �     �     �      �       !      E   B     �     �     �     �     �     �  +        @  >   ^     �     �  Z   �  	     	     
     
   $  
   /     :      O     p  $   |  $   �     �     �  %   �               !     =     Q     j     �  &   �     �     �  "   �     �          $  #   -     Q     V     [  ,   o  2   �  '   �     �  &   
  5   1  (   g     �     �     �     �  `   �  !        (     E     e  (   ~     �  
   �     �        H   O   C             B   >                 M                                 L           F       /   )           1   I           K          5   ?      ;   
   $   #   *   6   +      -           =      3          9             A   '   @   "   E              0   !         %   .                         (   J      8           G      2   <          	      :   7      &   4       D       ,   N   Q   P    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo xD Reader Project-Id-Version: Tajik KDE Software Localization
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2010-08-29 14:59+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <victor.ibragimov@gmail.com>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Tajik
X-Poedit-Country: TAJIKISTAN
Plural-Forms: nplurals=2; plural=n != 1;
 
Модули намоиши дастгоҳҳо (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 аз %2 озод (%3% истифодашуда) Батареяҳо Намуди батарея: Bus:  Камера Камераҳо Тағйири ҳолат: Барқгирӣ Пӯшидан Хониши корти флэш Дастгоҳ Иттилооти дастгоҳ Намоиши ҳамаи дастгоҳҳои пайвастшуда Дастгоҳҳо Холикунии барқ victor.ibragimov@gmail.com Рамзгузорӣ Кушодан Системаи файлҳо Намуди системаи файлҳо: Диски компютерӣ Оё пайвастшавандаи фаъол мебошад? IDE IEEE1394 Намоиши иттилоот дар бораи дастгоҳи интихобшуда. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Клавиатура Клавиатура ва муш Тамға: Суръати калонтарин: Хониши корти хотира Васл шуд дар: Муш Плеерҳои мултимедиа Victor Ibragimov Не Барқ намегирад Васл нашуд Танзимнашуда Дастгоҳи оптикӣ PDA Ҷадвали қисмҳои диск Аслӣ Протсессори %1 Рақами протсессор: Протсессорҳо Маҳсулот: Рэйд Оё ҷудошаванда аст? SATA SCSI Хониши SD/MMC Намоиши ҳамаи дастгоҳҳо Намоиши дастгоҳҳои мувофиқ Хониши дастгоҳҳои Smart Захирагоҳ Драйверҳои дастгорӣ: Пуштибонии маҷмӯъи дастурҳо: Протоколҳои дастгорӣ: UDI:  UPS USB UUID:  Намоиши дастгоҳҳои UDI (Муайянкунандаи дастгоҳи аслӣ) Дастгоҳи номуайян Истифоданашуда Истеҳсолкунанда: Андозаи диск: Андозаи истифодашуда: Ҳа kcmdevinfo Хониши xD 