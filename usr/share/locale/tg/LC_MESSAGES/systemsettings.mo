��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  s   T
     �
  \   �
     <     Q  ,   p  7   �  4   �     
  "        :  !   K  N   m     �     �     �  S   �  
   O     Z     k  l   �  Q   �  -   H     v     �     �  .   �  l   �  �   \     �  z        |  ?   �  %   �  �   �  �   �  u   \     �     �  �   �     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2010-01-31 01:27+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 0.2
 %1 является сторонней программой и была запущена автоматически (c) 2009, Ben Cooksley <i> 1 чиз дар бар мегирад</i> <i> %1 чиз дар бар мегирад</i> Дар бораи %1 О текущем модуле О текущем представлении Дар бораи танзимотҳои система Танзимотҳоро истифода баред Муалиф Бэн Куксли (Ben Cooksley) Танзимот Танзимоти система Следует ли показывать подробные подсказки Разработчик Диалог victor.ibragimov@gmail.com Автоматически разворачивать верхний уровень Умумӣ Роҳнамоӣ Намуди нишонаҳо Внутреннее представление модуля, внутренняя модель модуля Внутреннее имя используемого представления Миёнбурҳои клавиатура: %1 Сопровождение Mathias Soeken Виктор Ибрагимов Представления не найдены Показывает модули управления в виде значков по категориям. Показывает модули управления в виде классической древовидной структуры. Перезапустить %1 Отменить все текущие изменения и вернуться к предыдущим значениям Ҷу&стуҷӯ Показывать подсказки о содержимом Танзимотҳои система Программа настройки системы не смогла найти ни одного представления, а значит, нечего отображать. Программа настройки системы не смогла найти ни одного представления, а значит, настраивать нечего. В текущем модуле имеются несохранённые изменения.
Применить их? Дарахт Услуби намоиш Добро пожаловать в «Параметры системы», основное место для настройки вашего компьютера. Will Stephenson 