��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  (   �  s      �  `   �	     
     #
     6
  !   I
     k
     �
     �
     �
                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-12-26 10:47-0600
Last-Translator: Cindy McKee <cfmckee@gmail.com>
Language-Team: Esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  sek Tempolimo por &lanĉmontrado (sekundoj): <H1>Avizo en la taskostrio</H1>
Vi povas enŝalti duan metodon de atentigo pri lanĉado de
aplikaĵo. Tiuokaze en la taskostrio aperos butono kun
turniĝanta sablohorloĝo, kiu simbolas, ke via lanĉita aplikaĵo
prepariĝas. Povas okazi, ke iuj programoj ne scias pri la lanĉa avizo. Tiam la butono malaperos post la tempo
donita de vi ĉe "Tempolimo por lanĉmontrado" <h1>Okupiteco-kursoro</h1>
KDE ofertas apartan muskursoron kiu indikas la lanĉo de aplikaĵo.
Por uzi tion, elektu 'Enŝalti okupiteco-kursoron'.
Povas esti, ke iuj aplikaĵoj ne scias pri tiu avizo.
Tiam la butono malaperos post la tempo
donita de vi ĉe 'Tempolimo por lanĉmontrado' <h1>Lanĉoretrokuplado</h1> Vi povas agordi la retrokupladon de prepariĝanta aplikaĵo tie ĉi. Pulsanta kursoro Resaltanta kursoro Okup&iteco-kursoro Enŝalti avizon en la &taskostrio Neniu okupiteco-kursoro Pasiva okupiteco-kursoro Tempolimo por &lanĉmontrado: Taskostria a&vizo 