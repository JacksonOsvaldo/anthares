��          |      �          &   !  +   H     t  	   |     �     �     �     �  (   �     �        �    $   �  +   �  	   �  
   �          &     .     5  0   G     x     �     	                                    
                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Suspend Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2009-10-25 12:37+0200
Last-Translator: Markus Sunela <markus.sunela@tut.fi>
Language-Team: eo <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Ĉu vi volas halteti al RAM (dormi)? Ĉu vi volas halteti al disko (pasivumigi)? Ĝenerala Pasivumigi Pasivumigi (halteti al disko) Lasi... Ŝlosi Ŝlosi la ekranon Elsaluti, malŝalti aŭ restartigi la komputilon Dormi (halteti al RAM) Halteti 