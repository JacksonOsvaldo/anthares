��          �   %   �      p     q     w     �  
     
        "     .     <     N  	   W      a  �   �  *      H   K     �     �     �     �  	   �     �     �          /     O     d     {  	   �  �  �     U     Y  �   q  	   �               %     7     K     W     _  �   y  -   	  `   B	     �	     �	     �	     �	     �	     �	     
  "   
     A
     a
     y
     �
  	   �
                                                                                                                 
      	     msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Desktop %1 Desktop %1: Desktop Names Desktop Switching Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation Shortcuts Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2009-11-10 09:29+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: Esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
  ms &Kvanto da labortabloj: <h1>Pluraj labortabloj</h1>Per ĉi tiu modulo, vi povas fiksi la nombron de virtualaj labortabloj kiujn vi volas uzi, kaj iliajn titoletojn. Animacio: Labortablo %1 Labortablo %1: Labortablaj nomoj Labortabla Ŝaltado Labortabloj Daŭro  axel@esperanto-jeunes.org Ebligi, se vi volas klavan aŭ alian labortablan navigadon kiu povas atingi preter la rando de la nuna labortablo al la ekstrema rando de nova labortablo. Ĉi tie vi entajpas la nomon de labortablo %1 Ĉi tie vi fiksas la kvanton de virtualaj labortabloj kiujn vi volas uzi en via KDE-laborspaco.  Aranĝo K&vanto da linioj:  Axel Rousseau Neniu animacio Klavkombinoj Ŝalti al suba labortablo Ŝalti al supra labortablo Ŝalti al la maldekstra labortablo Ŝalti al la dekstra labortablo Ŝalti al Labortablo %1 Ŝalti al venonta labortablo Ŝalti al pasinta labortablo Ŝaltanta 