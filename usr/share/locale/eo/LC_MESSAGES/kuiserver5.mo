��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �               6     C     Y     a     i     p  	   w     �  3   �     �     �     �               !     *     I     i     �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-12 16:08+0100
Last-Translator: Michael Moroni <michael.moroni@mailoo.org>
Language-Team: Esperanto <kde-i18n-eo@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: KBabel 1.11.4
 %1 dosiero %1 dosieroj %1 dosierujo %1 dosierujoj %1 el %2 traktitaj %1 el %2 traktitaj ĉe %3/s %1 traktitaj %1 traktitaj ĉe %2/s Aspekto Konduto Nuligi Vakigi Agordi... Finitaj taskoj Listo de rulantaj dosiertransigoj/tskoj (kuiserver) Movi ilin en malsaman liston Movi ilin en malsaman liston. Paŭzigi Forigi ilin Forigi ilin. Daŭrigi Montri ĉiujn taskojn en listo Montri ĉiujn taskojn en listo. Montri ĉiujn taskojn en arbo Montri ĉiujn taskojn en arbo. Montri apartajn fenestrojn Montri apartajn fenestrojn. 