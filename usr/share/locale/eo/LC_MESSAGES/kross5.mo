��    !      $  /   ,      �     �     �  &        A     S     Z     b      k     �     �     �     �  ,   �  3   �     -     M     l     r     x  '   �     �     �     �     �                     $     D  &   I     p     v  �  �     <     D  	   T     ^  	   u          �  z   �            
   4     ?  /   ]  :   �  %   �  (   �     	      	     ,	  .   :	  J   i	     �	     �	     �	     �	     �	     

     
     -
  "   5
     X
     `
                             
                                !                                   	                                                               @info:creditAuthor @info:creditSebastian Sauer @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-05-22 12:30+0200
Last-Translator: Michael Moroni <michael.moroni@mailoo.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Aŭtoro Sebastian SAUER Ĝenerala Aldoni novan skripton. Aldoni... Ĉu rezigni? Komento: matthias@peick.de, okellogg@users.sourceforge.net, cfmckee@gmail.com, axel@esperanto-jeunes.org, michael.moroni@mailoo.org Redakti Redakti elektitan skripton. Redakti... Lanĉi la elektitan skripton. Kreado de skripto por interpretilo "%1" fiaskis Determinado de interpretilo por skriptdosiero "%1" fiaskis Ŝargado de interpretilo "%1" fiaskis Malfermado de skriptdosiero "%1" fiaskis Dosiero: Piktogramo: Interpretilo: Nivelo de sekureco por la interpretilo de Ruby Matthias Peick, Oliver Kellogg, Cindy McKee, Axel Rousseau, Michael Moroni Nomo: Neniu funkcio  "%1" Neniu interpretilo "%1" Forpreni Forigi elektitan skripton. Ruli Skriptdosiero %1 ne ekzistas. Haltigi Haltigi rulon de elektita skripto. Teksto: Kroso 