��          D      l       �   6   �   3   �   8   �      -  �  ;  9   �  .   )  5   X     �                          A command-line tool for querying the KDE trader system A constraint expressed in the trader query language A servicetype, like KParts/ReadOnlyPart or KMyApp/Plugin KTraderClient Project-Id-Version: ktraderclient
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:51+0200
PO-Revision-Date: 2012-05-23 14:39+0200
Last-Translator: Michael Moroni <michael.moroni@mailoo.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Komandlinia ilo por peti la sistemon de komercilo por KDE Limigo esprimita en la petolingvo de komercilo Servotipo, kiel KParts/ReadOnlyPart aŭ KMyApp/Plugin KTraderClient 