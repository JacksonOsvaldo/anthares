��          �            h  m   i     �  "   �  F     F   N  F   �  J   �     '  %   8  $   ^  $   �  &   �  �   �     W  �  j     @     O  %   `  Q   �  7   �  7     6   H          �     �     �     �     �     �           	                                                             
        Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordmount Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-02 23:07+0100
Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>
Language-Team: Esperanto <kde-i18n-doc@kde.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Esperanto
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 Ŝlosi la ujon Elĵeti medianon Trovi aparaton kies nomo kongruas :q: Listi ĉiujn aparatojn kaj ebligi ilin esti surmetitaj, demetitaj kaj elĵetitaj. Listi ĉiujn aparatojn kaj ebligi ilin esti elĵetitaj. Listi ĉiujn aparatojn kaj ebligi ilin esti surmetitaj. Listi ĉiujn aparatojn kaj ebligi ilin esti demetitaj. Surmeti aparaton aparato elĵeti surmeti demeti Malŝlosi la ujon Demeti aparaton 