��          �      L      �     �  '   �  "         #     =     U  ?   s     �  &   �      �  >        N     m  '   �  !   �     �     �  3     �  A     �     �     �     �  
                  #     )     :     M     h     z          �  
   �     �     �                                                                         	   
                        Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2012-08-14 09:26+0800
Last-Translator: Lê Hoàng Phương <herophuong93@gmail.com>
Language-Team: Vietnamese <kde-i18n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 ° %1 ngày C: %1 T: %2 Cao: %1 Thấp: %1 - Hãy cấu hình Nhẹ Độ ẩm: %1%2 Tầm nhìn: %1 %2 Xu hướng áp suất: %1 Áp suất: %1 %2 %1%2 Tầm nhìn: %1 Cảnh báo đã phát: Theo dõi: %1 %2 %3 Gió mạnh: %1 %2 