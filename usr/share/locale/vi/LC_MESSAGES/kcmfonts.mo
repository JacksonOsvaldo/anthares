��          �      \      �     �  /   �                    9     W     d     t     �  I   �     �     �  "   �        6   =  *   t     �     �  �  �     ^  7   f  "   �     �  %   �  $   �            %   4     Z  b   v     �     �  ?   �  /   1  J   a  L   �     �                                                 	                           
                             to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Use a&nti-aliasing: Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2007-06-23 17:29+0930
Last-Translator: Phan Vĩnh Thịnh <teppi82@gmail.com>
Language-Team: Vietnamese <kde-l10n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: KBabel 1.10
  tới  Phông chữ không tỷ lệ (như phông máy chữ). Chỉnh tất cả các &phông... BGR Nhấn để thay đổi mọi phông Cấu hình thiết lập làm mịn Cấu hình... &Vùng loại trừ: Thiết lập phông đã thay đổi Buộc chấm/insơ phông: Vết là tiến trình dùng để tăng chất lượng của phông chữ kích thước nhỏ. RGB Làm trơ&n phông: Dùng bởi thanh thực đơn và thực đơn chuột phải. Dùng bởi thanh tiêu đề của cửa sổ. Dùng cho chữ thông thường (ví dụ, nhãn nút, mục danh sách). Dùng để hiển thị chữ bên cạnh biểu tượng thanh công cụ. BGR đứng RGB đứng 