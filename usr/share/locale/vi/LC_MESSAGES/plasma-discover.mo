��          �      ,      �  5   �  #   �  >   �     "  <   )     f     n     w  	        �     �     �     �     �  	   �     �  �  �  E   a  ;   �  F   �     *  Q   0  
   �  
   �     �  
   �     �     �  	   �     �     �     �                                              	      
                               <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> Cancel Directly open the specified application by its package name. Discard Discover Install Installed Launch Rating: Remove Review Search in '%1'... Search... Summary: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2012-06-13 10:17+0800
Last-Translator: HeroP <herophuong93@gmail.com>
Language-Team: Vietnamese <kde-i18n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=1; plural=0;
 <em>%1 trong số %2 người thấy nhận xét này hữu ích</em> <em>Hãy nói với chúng tôi về nhận xét này!</em> <em>Hữu ích? <a href='true'>Có</a>/<a href='false'>Không</a></em> Huỷ Mở trực tiếp ứng dụng được chỉ định bằng tên gói của nó Huỷ bỏ Khám phá Cài đặt Đã cài  Chạy Đánh giá: Gỡ bỏ Nhận xét Tìm kiếm trong '%1'... Tìm kiếm... Tóm tắt: 