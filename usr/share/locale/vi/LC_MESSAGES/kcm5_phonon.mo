��    A      $  Y   ,      �  i   �  m   �     i  b   �     �     �  6        ?  7   O     �     �  	   �     �  (   �  )   �  )        B     _  Y   e     �     �  �   �      i	     �	  
   �	     �	     �	     �	     �	     �	     �	     
     
     $
     3
     8
     W
     s
     �
     �
     �
  	   �
  
   �
     �
     �
  	   �
  
   �
  
   �
     �
       	   !     +     0  
   I  �   T     �  8   �  �   2     �  8   �  ,     ,   0  %   ]     �  �  �  e   K  �   �  )   B  h   l  #   �     �  D        T  4   b     �     �     �  /   �  :   �  ,   3  .   `  .   �     �  l   �     <     U  �   l          3     G     V     v      �     �     �  
   �     �  '   �          #     /     E     a  	   r     |     �  	   �  
   �     �     �  
   �     �     �     �       
   *  
   5  #   @     d  �   r     h  K   y  �   �  	   �  6   �  ?   �  A     7   T  "   �     #       $          :   +          A                     "                 6   8       *              (   >   ,   ;         5                                   )   1       =         7       0       ?       -   4   2   9   @         %      .                3              /      !             <   '   &         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcmphonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-10 10:57+0700
Last-Translator: Lê Hoàng Phương <herophuong93@gmail.com>
Language-Team: Vietnamese <kde-i18n-vi@kde.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.4
 Để áp dụng thay đổi hậu trường bạn sẽ phải đăng xuất và đăng nhập lại. Danh sách các hậu trường Phonon nằm trên hệ thống này. Thứ tự tại đây quyết định thứ tự Phonon sẽ sử dụng. Áp dụng danh sách thiết bị cho... Áp dụng danh sách ưu tiên thiết bị hiện có cho các thể loại phát lại âm thanh sau: Cài đặt phần cứng âm thanh Phát lại âm thanh Ưu tiên thiết bị âm thanh mặc định cho thể loại '%1' Thu âm thanh Ưu tiên thiết bị thu âm cho thể loại '%1' Hậu trường Colin Guthrie Trình kết nối Bản quyền © năm 2006 của Matthias Kretz Ưu tiên thiết bị phát lại âm thanh mặc định Ưu tiên thiết bị thu âm mặc định Ưu tiên thiết bị quay phim mặc định Mặc định/thể loại chưa chỉ định Giảm ưu tiên Đặt thứ tự thiết bị mặc định (có thể được thay thế bởi các thể loại riêng.) Cấu hình thiết bị Ưu tiên thiết bị Các thiết bị tìm thấy trên hệ thống của bạn và phù hợp cho thể loại đã chọn. Hãy chọn thiết bị mà bạn muốn dùng bởi các ứng dụng. kde-l10n-vi@kde.org Trung tâm trước Trái trước Trái trước của trung tâm Phải trước Phải trước của trung tâm Phần cứng Thiết bị độc lập Cấp vào Không hợp lệ Cài đặt phần cứng âm thanh KDE Matthias Kretz Đơn kênh Nhóm Việt hoá KDE Mô-đun cấu hình Phonon Phát lại (%1) Ưu tiên Hồ sơ Trung tâm sau Trái sau Phải sau Thu (%1) Hiện thiết bị nâng cao Bên trái Bên phải Card âm thanh Thiết bị âm thanh Vị trí loa và kiểm tra Loa trầm Kiểm tra Kiểm tra thiết bị đã chọn Kiểm tra %1 Thứ tự sẽ quyết định độ ưu tiên của thiết bị. Nếu do vài lý do nào đó mà thiết bị đầu tiên không thể dùng được, Phonon sẽ cố gắng sử dụng thiết bị thứ hai và tiếp tục như vậy. Kênh không rõ Sử dụng danh sách thiết bị hiện có cho các thể loại khác. Các thể lại tương ứng các cách sử dụng phương tiện khác nhau. Với mỗi thể loại, bạn có thể chọn thiết bị nào bạn ưu tiên sử dụng bởi ứng dụng Phonon. Quay phim Ưu tiên thiết bị quay phim cho thể loại '%1' Hậu trường của bạn có thể không hỗ trợ thu âm Hậu trường của bạn có thể không hỗ trợ quay phim không có độ ưu tiên cho thiết bị đã chọn ưu tiên thiết bị đã chọn 