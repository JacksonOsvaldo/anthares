��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �     �  
   �     �       '        B  &   K  4   r  H   �  )   �  &     
   A     L     ]  ,   q     �     �     �  #   �     �  "   �     	  )   "	     L	  -   S	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-09-23 18:43+0800
Last-Translator: Lê Hoàng Phương <herophuong93@gmail.com>
Language-Team: American English <kde-i18n-vi@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
 Chung Thêm một văn lệnh mới. Thêm... Hủy bỏ? Ghi chú : kde-l10n-vi@kde.org Hiệu chỉnh Hiệu chỉnh văn lệnh đã chọn. Sửa... Thực hiện văn lệnh đã chọn. Lỗi tạo văn lệnh cho bộ giải thích "%1". Thất bại khi quyết định trình dịch cho tập tin lệnh "%1" Thất bại khi nạp trình dịch "%1" Lỗi mở tập tin văn lệnh "%1". Tập tin: Biểu tượng: Bộ giải thích: Cấp an toàn của bộ giải thích Ruby Nhóm Việt hoá KDE Tên: Không có hàm "%1" Không có bộ giải thích "%1". Bỏ Gỡ bỏ văn lệnh đã chọn. Chạy Tập tin lệnh "%1" không tồn tại. Dừng Dừng thực hiện văn lệnh đã chọn. Văn bản: 