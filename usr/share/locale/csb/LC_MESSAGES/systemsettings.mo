��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  ;   �
     �
  c   �
     [     a     v     �     �  	   �     �     �     �  B     
   N     Y  %   `  %   �  
   �     �     �  :   �  *        9  	   V     `  &   o     �  R   �  F   	     P  :   b     �  #   �     �  e   �  x   F  S   �          %  g   9     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2009-12-12 12:21+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
X-Generator: Lokalize 1.0
 %1 je bùtnową aplikacëją ë òsta aùtomatno zrëszonô (c) 2009, Ben Cooksley <i>Zamëkô w se 1 element</i> <i>Zamëkô w se %1 elementë</i> <i>Zamëkô w se %1 elementów</i> Ò %1 Ò biéżnym mòdule Ò biéżnym wëzdrzatków Ò Systemòwëch nastôwch Systemòwé nastôwë Ùsôdzca Ben Cooksley Kònfigùracëjô Kònfigùrëje swòją systemã Przë włączonyn bãdą wëskrzënioné pòdpòwiescë z detalama Rozwijôrz Dialog michol@linuxcsb.org,mark@linuxcsb.org Rozłożë pierszą légã aùtomatno Spòdlowé Pòmòc Wëzdrzatk ikònów Bënowô reprezentacëja mòdułu, bënowi mòdel mòdułu Bënowé miono brëkòwónegò wëzdrzatka Klawiaturowô skrodzëna: %1 Òpiekùn Mathias Soeken Michôł Òstrowsczi,Mark Kwidzińsczi Ni ma nalazłëch wëzdrzatków Dôwô skategòrizowóné wëzdrzatczi ikònów dlô kòntrolowónëch mòdułów. Dôwô klasykòwi wëzdrzatk drzewa dlô kòntrolowónëch mòdułów. Zrëszë znowa %1 Resetëje wszëtczé zmianë do pòprzédnëch wôrtnotów Szëkba: Pòkażë pòdpòwiescë z detalama Systemòwé nastôwë Systemòwé nastawë ni mògłë nalezc niżódnëch wëzdrzatków, téj nick ni mòże wëskrzënic. Systemòwé nastôwë ni mògłë nalezc niżódnëch wëzdrzatków, téj nick nie je przëstãpné do kònfigùracëji. Są niezapisóné zmianë w aktiwnym mòdule.
Chcesz je zacwierdzëc czë rëmnąc? Wëzdrzatk drzewa Sztél wëzdrzatkù Witôj w "Systemòwëch nastôwach", przédnym môlu dlô kònfigùracrji twòji kòmpùtrowi systemë. Will Stephenson 