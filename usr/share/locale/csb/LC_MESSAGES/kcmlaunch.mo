��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K       "   %  �  H  �   	     �
     X     o     �  /   �     �     �  !   �  &                                             
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-03-19 17:39+0100
Last-Translator: Michôł Òstrowsczi <michol@linuxcsb.org>
Language-Team: Kashubian
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
  sek. &Maksymalny czas zmianë kùrsora: <h1>Lëstwa dzejaniów</h1>
Mòżesz włączëc jiną metodã òdkôzewaniô ò zrëszanim programë
- na lëstwie dzejaniów zjôwi sã ikòna programë z òbracającą sã klepsydrą, 
chtërna symbòlizëjë zrëszanié programë.
Niechtërné programë mògą ignorowac tã fùnkcëjã, nie òdkôzewając ò zakùńczenim zrëszaniô. Tedë kùrsor przestanié mërgòtac pò czase òpisónym w pòlu "Maksymalny czas zmianë ikòny". <h1>Zmiana kùrsora</h1>
KDE może zaznaczac zrëszanié programë przez zmianã wëzdrzatkù kùrsora.
Żebë tak bëło, wëbierzë òptacëjã "Zmieni kùrsor òbczas zrëszania
programë".
Dodôwno mòżesz włączëc mërgòtanié kùrsora w òptacëji niżi.
Niechtërné programë mògą ignorowac tã fùnkcëjã, nie òdkôzewùjąc ò
zakùńczenim zrëszania. Tedë kùrsor przestanié mërgòtac pò czase
wpisónym w pòlu "Maksymalny czas zmianë kùrsora". <h1>Òdkôzanié ò zrëszanim</h1> Mòduł nen pòzwôlô na wëbiérk metodë òdkôzënkù ò naczãcu zrëszania programë. Mërgòtający kùrsor Skaczący kùrsor &Zmiana kùrsora Włączë wëdowiédzã na &lëstwie dzajaniów Bez zmianë kùrsora Pasywnô zmiana kùrsora Ma&ksymalny czas òdkôzëwaniô: &Òdkôzywanié na lëstwie dzejaniów 