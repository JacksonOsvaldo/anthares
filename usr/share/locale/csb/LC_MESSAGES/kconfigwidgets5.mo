��    H      \  a   �            !  	   .     8     E     S     Y     `     q     w     �     �  	   �     �     �  
   �     �     �  
   �     �     �     �  	          
             +     :     @     O     \     b     f  
   o     z  
   �     �     �     �     �  	   �      �          !     8  
   J     U     d     x     �  
   �     �     �     �     �     �     �     �     	     	  C   $	     h	     x	     �	     �	     �	  	   �	     �	     �	     �	     �	     
  �  
               #     4     H     P     X     q          �     �     �     �     �     �     �                    ,     3     H     W     d     r       	   �     �     �     �  	   �     �     �               -  "   J  )   m     �     �  &   �     �     �               &     :     X  '   x     �     �     �     �     �     
          2     G     _  @        �     �     �  
     	     
        %  
   -     8     I  	   R             D   ?   8   .          -   @         F          "   !   A                  3   C      #       2                  :         9   =   7             (                   /           ,   >   )             H              0   G       ;       
                E       5   	   &      %   6          $       *   <                 1   '      4      B   +       %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Find... &First Page &Fit to Page &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... About &KDE C&lear Clear List Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Cu&t Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width NAME OF TRANSLATORSYour names No Entries Open &Recent Print Previe&w Quit application Re&do Re&vert Save &As... Select &All Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Switch Application &Language... Tip of the &Day What's &This? Zoom &In Zoom &Out go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2009-12-04 22:09+0100
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Ùcz&bòwnik %1 Ò pro&gramie %1 &Aktualnô miara Dodôj z&ałóżkã &Nazôd Z&amkni &Kònfigùracëjô %1... &Kòpérëjë N&alezë... &Pierszô starna &Dopasëjë do starnë &Biéj do... &Biéj do réżczi... &Biéj do starnë... &Slédnô starna &Wëslë... &Nowi &Zôstnô starna Ò&temkni... &Wlepi &Pòprzédnô starna &Drëkùjë... Za&kùńczë Òd&swiéżë &Zastãpi... &Rapòrt ò felë... &Zapiszë &Zapiszë nastôwë &Bezzmiłkòwòsc pisënkù... &Copni W &górã &Zwikszenié... Wëdowiédzô ò &KDE Wë&czëszczë Wëczëszczë lëstã Kònfigùrëjë ò&dkazë... Kònfigùracëjô s&krodzënów... Kònfigùracëjô listwów nô&rzãdzy... Wë&tni &Rëmôj zaznaczenié michol@linuxcsb.org, mark@linuxcsb.org Aùtomatné wëkrëwanié Domëszlné Fùl&ekranowi trib &Nalezë zôstne Nalezë pòp&rzedni Dopasëjë do &wiżë starnë Dopasëjë do &szérzë starnë Michôł Òstrowsczi, Mark Kwidzińsczi Bez wprowadzeniô Òtemkni wcza&sniészi Pòdzérk &wëdrëkù Zakùńczë programã &Doprowôdzë nazôd &Doprowôdzë nazôd Zapiszë j&akno... Zaznaczë &wszëtkò Pòkôżë listew &menu Pòkôżë listew &nôrzãdzów Pòkôżë menu<p>Pòkazëje jesz rôz zataconą listew menu</p> Pòk&ôżë listew stónu Zmieni &jãzëk programë... Pòrada &dnia Co &to je? Z&wikszë Z&miészë &Nazôd &W przódk &Domôcô starna Pòmò&c bez miona 