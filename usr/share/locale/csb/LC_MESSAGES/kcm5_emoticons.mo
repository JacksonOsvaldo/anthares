��    #      4  /   L           	     !     .  +   5      a     �  ;   �     �     �                 >     _  9   m     �  3   �  	   �     �  )   �  %   )  |   O  [   �  5   (  *   ^     �     �     �     �     �  +   �  (        A  n   a  3   �  �       �	     
  	   
  9   &
  +   `
     �
  ?   �
     �
  &   �
     %  5   @  &   v     �  C   �     �  F        L     Y  '   r  "   �  w   �  c   5  ?   �  :   �  '        <     V     ^     m  (   �  "   �  &   �  w      "   x                          #      
                                         !             	          "                                                             %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet If you already have an emoticon theme archive locally, this button will unpack it and make it available for KDE applications Insert the string for the emoticon.  If you want multiple strings, separate them by spaces. Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons Start a new theme by assigning it a name. Then use the Add button on the right to add emoticons to this theme. This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2009-10-20 18:19+0200
Last-Translator: Mark Kwidzińśczi <mark@linuxcsb.org>
Language-Team: Kaszëbsczi <i18n-csb@linuxcsb.org>
Language: csb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Téma %1 ju òbstoji Dodôj emòtikònã Dodôj... Wëbierzë ôrt nowi témë emòtikònów do ùsôdzeniô Ni mòże zainstalowac témë emòtikònów Ùsôdzë nową emòtikònã Ùsôdzë nową emòtikònã dodając ji ikònã ë jaczi tekst Rëmôj emòtikònã Zabédëjë nową témã emòtikònów Chesz rëmnąc téż  %1 ? Wpiszë abò przëcygnie adresã témë emòtikònów michol@linuxcsb.org, mark@linuxcsb.org Editëjë emòtikònã Editëjë wëbróną emòtikònã bë zmienic ji ikònã abò tekst Editëjë... Témë emòtikònów mùszą bëc zainstalowóné z môlowich lopków. Emòtikònë Menadżera emòtikònów Wpiszë miono nowi témë emòtikònów Zladëjë nowé témë z internetu Jeżlë môsz ju zladowóną témã, dzãka ti knąpie rozpakùjesz ją ë zrobisz przëstãpną dlô aplikacëjów KDE Wpiszë stegną do emòtikònów. Jeżlë brëkùjesz czile stegnów to rozparłãczë je spacjama. Instalëjë lopk z témą jaką môsz ju na swòjim kòmpùtrze Zdmòdifikùjë ikònã abò tekst wëbróny emòtikònë  Michôł Òstrowsczi, Mark Kwidzińsczi Nowô téma emòtikònów Rëmôj Rëmôj témã Rëmôj wëbróną emòtikònã Rëmôj wëbróną emòtikònã z diskù Rëmôj wëbróną témã z diskù Zdefiniëjë plac wkół emòtikònczi Zaczni nową témã przez dodanié ji miona. Téj ùżëjë knãpë Dodôj z prawa bë dodac emòtikònë do ti témë. Rëmô wëbróną témã z diskù. 