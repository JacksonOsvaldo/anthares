��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     3  �   R  ^   �     B	     _	  X   |	     �	  %   �	     
     
  !   #
     E
     V
     g
  *   z
     �
     �
     �
     �
     �
  F   �
  c   @  9   �  P   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2018-02-04 13:23+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 (c) 2003-2007 Fredrik Höglund <qt>Sei sicuro di voler rimuovere il tema del puntatore <strong>%1</strong>?<br/> Questo eliminerà tutti i file installati da questo tema.</qt> <qt>Non puoi eliminare il tema che stai usando.<br />Devi prima passare ad un altro tema.</qt> (Dimensioni disponibili: %1) Dipendente dalla risoluzione Un tema chiamato %1 esiste già nella tua cartella dei temi. Voi sostituirlo con questo? Conferma Impostazioni del puntatore modificate Tema del puntatore Descrizione Trascina o immetti l'URL del tema nicola@nxnt.org, Fredrik Höglund Scarica nuovo tema Scarica nuovi schemi di colore da internet Installa da file Nicola Ruggero,Andrea Rizzi Nome Sovrascrivo il tema? Rimuovi tema Il file %1 non sembra essere un archivio di tema del puntatore valido. Impossibile scaricare l'archivio del tema del puntatore; controlla che l'indirizzo %1 sia corretto. Impossibile trovare l'archivio del tema del puntatore %1. Devi riavviare la sessione di Plasma affinché queste modifiche abbiano effetto. 