��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �     �     �     	     	     "	     .	     J	     X	     d	     m	     s	     �	     �	     �	     �	     �	  3   �	  /   
     ;
     W
     n
     �
     �
      �
     �
     �
     �
     �
                    !        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-22 15:05+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 % Regola il volume per %1 Applicazioni Audio silenziato Volume audio Comportamento Dispositivi di acquisizione Flussi di acquisizione Silenzia Predefinito Riduci volume del microfono Riduci volume Dispositivi Generale Porte Aumenta volume del microfono Aumenta volume Volume massimo: Silenzia Silenzia %1 Silenzia microfono Nessuna applicazione che riproduca o registri audio Nessun dispositivo di uscita o ingresso trovato Dispositivi di riproduzione Flussi di riproduzione  (non disponibile)  (non collegata) Alza il volume al massimo Mostra opzioni aggiuntive per %1 Volume Volume al %1% Risposta del volume Incremento del volume: %1 (%2) %1: %2 100% %1% 