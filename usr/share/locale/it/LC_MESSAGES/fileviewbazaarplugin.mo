��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  #   �	  )   �	  4   �	     
  $   "
      G
  $   h
  ,   �
     �
  !   �
  1   �
  &   ,  &   S  #   z  +   �  5   �  '         (     ?  /   \  $   �  1   �     �  &        (     =     Y     k     ~     �     �  $   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-10-13 23:24+0200
Last-Translator: Marcello Pogliani <marcello.pogliani@gmail.com>
Language-Team: Italian <kde-i18n-doc@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 File aggiunti al repository Bazaar. Aggiunta dei file al repository Bazaar... Aggiunta di file nel repository Bazaar non riuscita. Registro di Bazaar chiuso. Commit delle modifiche non riuscito. Commit effettuato correttamente. Commit delle modifiche con Bazaar... Ricezione da repository Bazaar non riuscita. Repository Bazaar aggiornato. Ricezione da repository Bazaar... Invio modifiche a repository Bazaar non riuscito. Modifiche inviate a repository Bazaar. Invio modifiche a repository Bazaar... File rimossi dal repository Bazaar. Rimozione dei file dal repository Bazaar... Rimozione di file nel repository Bazaar non riuscita. Revisione delle modifiche non riuscita. Modifiche revisionate. Revisione delle modifiche... Esecuzione del registro di Bazaar non riuscita. Esecuzione del registro di Bazaar... Aggiornamento del repository Bazaar non riuscito. Repository Bazaar aggiornato. Aggiornamento del repository Bazaar... Aggiungi a Bazaar... Esegui commit con Bazaar... Elimina da Bazaar Registro di Bazaar Ricevi da repository Bazaar Invia a repository Bazaar Aggiorna con Bazaar Mostra le modifiche locali di Bazaar 