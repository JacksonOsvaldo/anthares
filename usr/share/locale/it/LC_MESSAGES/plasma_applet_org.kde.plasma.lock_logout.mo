��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  %   �  '        ?     H     O     [     {     �     �     �  1   �     �     �  2   �     '     3     A     O        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-08-02 08:35+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Vuoi sospendere in RAM (sospensione)? Vuoi sospendere su disco (ibernazione)? Generale Azioni Ibernazione Ibernazione (sospendi su disco) Esci Esci... Blocca Blocca lo schermo Termina la sessione, spegni o riavvia il computer No Sospensione (sospendi in RAM) Avvia una sessione parallela con un utente diverso Sospensione Cambia utente Cambia utente Sì 