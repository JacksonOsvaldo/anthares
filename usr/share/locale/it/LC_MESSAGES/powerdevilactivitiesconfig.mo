��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �  #   �  "        8     N     U     t  )   �     �     �     �     	       �      �   �  �   j	     �	  0   
     8
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-06 07:51+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  minuti Fai come Sempre Definisci un comportamento speciale Non usare le impostazioni speciali smart2128@baslug.org, Iberna Vincenzo Reale,Federico Zenith Non spegnere mai lo schermo Non sospendere o spegnere mai il computer PC collegato all'alimentatore PC usa la batteria PC con batteria quasi scarica Spegni Sospendi in RAM Il servizio di gestione energetica non sembra essere in esecuzione.
Questo problema può essere risolto avviandolo o pianificandone l'esecuzione in «Avvio e spegnimento» Il servizio delle attività non è in esecuzione.
È necessario che lo sia per configurare il comportamento della gestione energetica per ciascuna attività. Il servizio delle attività è in esecuzione con funzionalità minime.
I nomi e le icone delle attività potrebbero non essere disponibili. Attività «%1» Usa impostazioni separate (solo utenti avanzati) dopo 