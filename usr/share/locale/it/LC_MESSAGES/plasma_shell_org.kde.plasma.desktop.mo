��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
  	   /     9     I     ]     q     u     �     �     �     �     �     �      �          
  	             #     *  	   ,     6     L     ^     e     |     �     �     �     �     �  
   �     �  K     j   Z     �     �     �     �     �                    /     E     V     Y     o     �     �     �     �     �     �     �     �  [   �  �   E     �               +     A     W     c     j     z  	   �     �      �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-09 17:55+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Attività Aggiungi azione Aggiungi spaziatore Aggiungi oggetti... Alt Oggetti alternativi Sempre visibile Applica Applica impostazioni Applica ora Autore: Nascondi automaticamente Pulsante di ricerca all'indietro Basso Annulla Categorie Centro Chiudi + Configura Configura l'attività Crea attività... Maiusc Attualmente utilizzata Elimina Posta elettronica: Pulsante di ricerca in avanti Ottieni nuovi oggetti Altezza Scorrimento orizzontale Digita qui Scorciatoie da tastiera La disposizione non può essere modificata mentre gli oggetti sono bloccati Le modifiche della disposizione devono essere applicate prima che altre modifiche possano essere apportate Disposizione: Sinistra Pulsante destro Licenza: Blocca oggetti Massimizza pannello Meta Pulsante centrale Altre impostazioni... Azione del mouse OK Allineamento pannelli Rimuovi pannello Destra Pulsante centrale Bordo dello schermo Cerca... Maiusc Ferma l'attività Attività fermate: Cambia Le impostazioni del modulo attuale sono cambiate. Vuoi applicare le modifiche o annullarle? Questa scorciatoia attiverà l'applet: ciò consentirà l'immissione da tastiera, e se l'applet ha una finestra a comparsa (come il menu di avvio), la finestra a comparsa sarà aperta. Alto Annulla la disinstallazione Disinstalla Disinstalla l'oggetto Scorrimento verticale Visibilità Sfondo Tipo di sfondo: Oggetti Larghezza Le finestre possono coprire Le finestre passano sullo sfondo 