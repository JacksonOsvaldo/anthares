��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     L  ,   P  )   }  ,   �  *   �     �  
     *   $     O  )   f  ,   �  0   �  _   �  H   N	  1   �	  4   �	  2   �	  0   1
     b
  ?   �
     �
  /   �
                    )     5  5   J     �  +   �                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-19 00:06+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  ms Visi&bilità degli acceleratori da tastiera: &Tipo di pulsante per la freccia in alto: Nascondi sempre gli acceleratori da tastiera Mostra sempre gli acceleratori da tastiera Dura&ta delle animazioni: Animazioni Tip&o di pulsante per la freccia in basso: Impostazioni di Brezza Centra le schede della barra delle schede Trascina le finestre da qualsiasi area vuota Trascina le finestre solo dalla barra del titolo Trascina le finestre dalla barra del titolo, dalla barra dei menu e dalle barre degli strumenti Disegna una linea sottile per indicare i menu e le barre dei menu attivi Disegna l'indicatore di messa a fuoco nelle liste Disegna una cornice intorno ai pannelli agganciabili Disegna una cornice intorno ai titoli delle pagine Disegna una cornice intorno ai pannelli laterali Disegna i marcatori del cursore Disegna i separatori degli elementi della barra degli strumenti Abilita le animazioni Abilita le maniglie di ridimensionamento estese Cornici Generale Nessun pulsante Un pulsante Barre di scorrimento Mostra gli acceleratori da tastiera quando necessario Due pulsanti Modal&ità di trascinamento delle finestre: 