��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  F   i     �  "   0  l   S     �     �  D   �     6  E   J     �     �  
   �     �  8   �  9     8   B  %   {     �  i   �           ;  �   R     �  @   �     ,     ?     R     n          �     �     �  
   �  $   �     �               0     P     b     t     |     �     �     �     �     �     �            #   &  	   J     T      Z     {  �   �     /  G   B  �   �     -  D   @  =   �  ;   �  1   �  %   1     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-07-18 14:31+0200
Last-Translator: Nicola Ruggero <nicola@nxnt.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Per applicare le modifiche al backend devi scollegarti e ricollegarti. Una lista di backend per Phonon trovati nel sistema. L'ordine che vedi qui determina l'ordine con cui verranno usati da Phonon. Applica lista dei dispositivi a... Applica l'elenco di preferenza dei dispositivi mostrato anche alle seguenti categorie di riproduzione audio: Configurazione hardware audio Riproduzione audio Preferenze dispositivo di riproduzione audio per la categoria «%1» Registrazione audio Preferenze dispositivo di registrazione audio per la categoria «%1» Backend Colin Guthrie Connettore Copyright 2006 Matthias Kretz Preferenze dispositivo di riproduzione audio predefinito Preferenze dispositivo di registrazione audio predefinito Preferenze dispositivo di acquisizione video predefinito Categoria predefinita/non specificata Diminuisci priorità Definisce l'ordinamento predefinito dei dispositivi, che può essere reimpostato nelle singole categorie. Configurazione dispositivo Preferenze dispositivo Trovati dispositivi nel sistema adatti per la categoria selezionata. Scegli il dispositivo che desideri venga usato dalle applicazioni. nicola@nxnt.org, Impossibile impostare il dispositivo audio di uscita selezionato Anteriore centrale Anteriore sinistro Anteriore centrale sinistro Anteriore destro Anteriore centrale destro Hardware Dispositivi indipendenti Livelli di ingresso Non valido Configurazione hardware audio di KDE Matthias Kretz Mono Nicola Ruggero,Marcello Anni Modulo di configurazione Phonon Riproduzione (%1) Aumenta priorità Profilo Posteriore centrale Posteriore sinistro Posteriore destro Registrazione (%1) Mostra dispositivi avanzati Laterale sinistro Laterale destro Scheda audio Dispositivo audio Posizionamento altoparlanti e prove Subwoofer Prova Prova il dispositivo selezionato Prova di %1 L'ordine determina la preferenza dei dispositivi. Se per qualche ragione il primo dispositivo non può essere usato, Phonon cercherà di usare il secondo, e così via. Canale sconosciuto Usa per più categorie l'elenco di preferenza dei dispositivi mostrato. Varie categorie per diversi usi della parte multimediale. Per ogni categoria puoi scegliere quale dispositivo preferisci sia utilizzato dalle applicazioni Phonon. Acquisizione video Preferenze dispositivo di acquisizione video per la categoria «%1» Il tuo backend potrebbe non supportare la registrazione audio Il tuo backend potrebbe non supportare l'acquisizione video Nessuna preferenza per il dispositivo selezionato Preferisci il dispositivo selezionato 