��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  4     K   9     �     �     �     �     �     �     �  	   �     �          -  !   M     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-05-03 10:32+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Chiudi Copia automaticamente: Non mostrare questa finestra, copia automaticamente. Rilascia del testo o un'immagine qui per caricarlo su un servizio in linea. Errore durante il caricamento. Generale Dimensione cronologia: Incolla Attendi Prova ancora. Invio in corso... Condividi Condivisioni per «%1» Caricato correttamente L'URL è stato appena condiviso Carica %1 su un servizio in linea 