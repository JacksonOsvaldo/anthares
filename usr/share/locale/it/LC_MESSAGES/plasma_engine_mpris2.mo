��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  Q   C  $   �  $   �     �     �  "     D   2  E   w  #   �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-03-18 00:32+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Il tentativo di eseguire l'azione «%1» non è riuscito. Il messaggio è «%2». Riproduzione del supporto successivo Riproduzione del supporto precedente Controllo multimediale Riproduci/Pausa supporto Ferma la riproduzione del supporto L'argomento «%1» per l'azione «%2» è mancante o di tipo errato. Il lettore multimediale «%1» non riesce a eseguire l'azione «%2». L'operazione «%1» è sconosciuta. Errore sconosciuto. 