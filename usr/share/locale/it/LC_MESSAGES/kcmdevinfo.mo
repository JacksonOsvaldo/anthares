��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  1   �        	             (     D     M     `  
   f  
   q     |  	   �     �     �     �     �  0   �          3  
   ?     J     i     q          �     �     �     �     �     �  <   �  	     	   &  
   0  
   ;  
   F     Q     Z     k     w     �     �     �     �     �     �     �     �                    +     /     H     Q     _  
   w  
   �     �     �     �     �     �     �     �     �            "   0     S     k     q     u     y  L   �     �     �     �     �            
   !     ,     8     @     H     T     `     l     x     �  
   �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-05-09 15:55+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 
Modulo del visore di dispositivi basato su Solid © 2010 David Hubner AMD 3DNow ATI IVEC %1 liberi di %2 (usato %3%) Batterie Tipo di batteria:  Bus:  Fotocamera Fotocamere Stato della batteria:  In carica Contrai tutto Lettore CompactFlash Un dispositivo Informazioni sul dispositivo Mostra tutti i dispositivi attualmente elencati. Visore di dispositivi Dispositivi In scarica federico.zenith@member.fsf.org Cifrato Espandi tutto File system Tipo di file system:  Batteria carica Disco fisso Collegabile a caldo? IDE IEEE1394 Mostra informazioni sul dispositivo attualmente selezionato. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Tastiera Tastiera e mouse Etichetta:  Velocità massima:  Lettore Memory Stick Montato a:  Mouse Lettori multimediali Federico Zenith No Batteria scarica Nessun dato disponibile Non montato Non impostata Unità ottica PDA Tabella delle partizioni Primario Processore %1 Numero del processore:  Processori Prodotto:  RAID Rimovibile? SATA SCSI Lettore SD/MMC Mostra tutti i dispositivi Mostra i dispositivi rilevanti Lettore SmartMedia Unità di memorizzazione Driver supportati: Insiemi di istruzioni supportati:  Protocolli supportati:  UDI:  UPS USB UUID:  Mostra l'UDI (identificativo univoco di dispositivo) del dispositivo attuale Unità sconosciuta Inutilizzata Fornitore:  Spazio del volume: Uso del volume:  Sì kcmdevinfo Sconosciuto Nessuno Nessuno Piattaforma Sconosciuto Sconosciuto Sconosciuto Sconosciuta Sconosciuto Lettore xD 