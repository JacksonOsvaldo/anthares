��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  +   r     �     �     �     �     �     �     �     �     �                         ,     K  @   T  R   �  
   �     �       /   #  	   S     ]     z  t   �  ]   �     V  #   p     �     �  +   �  !   �     �            '   ,  
   T     _     o  (   ~      �     �  1   �  %        5      =     ^     g     m     �  T   �  ,   �          /     O      m     �  )   �     �  5   �  %   #     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-15 15:54+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Proprietà della fi&nestra corrispondente:  Enorme Grande Nessun bordo Nessun bordo laterale Normale Fuori misura Sottile Davvero enorme Molto grande Grande Normale Piccola Molto grande Bagliore della finestra attiva Aggiungi Aggiungi una maniglia per ridimensionare le finestre senza bordo Consenti il ridimensionamento delle finestre massimizzate dai bordi delle finestre Animazioni Dimensione dei p&ulsanti: Dimensione del bordo: Transizioni dei pulsanti al passaggio del mouse Al centro Al centro (larghezza intera) Classe:  Configura la sfumatura tra l'ombra della finestra e il bagliore quando cambia lo stato di attivazione della finestra Configura l'animazione dell'evidenziazione dei pulsanti della finestra al passaggio del mouse Opzioni della decorazione Scopri le proprietà della finestra Finestra Modifica Modifica eccezione - Impostazioni di Oxygen Attiva/disattiva questa eccezione Tipo di eccezione Generale Nascondi la barra del titolo Informazioni sulla finestra selezionata A sinistra Sposta in basso Sposta in alto Nuova eccezione - Impostazioni di Oxygen Domanda - Impostazioni di Oxygen Espressione regolare La sintassi dell'espressione regolare è inesatta Espressione regolare da veri&ficare:  Elimina Elimina l'eccezione selezionata? A destra Ombre A&llineamento del titolo: Titolo:  Utilizza gli stessi colori per la barra del titolo e per il contenuto della finestra Usa classe di finestre (applicazione intera) Usa il titolo della finestra Avviso - Impostazioni di Oxygen Nome della classe di finestre Ombra per le finestre a comparsa Identificazione della finestra Selezione delle proprietà della finestra Titolo della finestra Transizioni del cambio di stato della finestra attiva Ridefinizioni specifiche per finestra 