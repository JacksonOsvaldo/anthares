��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     /     C     [     c     j     s     {  
   �     �  	   �  7   �     �     �     �          #     1     8     >     N     h  e   p                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-07-15 19:14+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.2
 %1 è in esecuzione %1 non è in esecuzione Azze&ra &Avvia Avanzate Aspetto Comando: Visualizza Esegui un comando Notifiche Tempo rimanente: %1 secondo Tempo rimanente: %1 secondi Esegui un comando &Ferma Mostrare le notifiche Mostra i secondi Mostra titolo Testo: Timer Timer terminato Il timer è in esecuzione Titolo: Usa la rotella del mouse per cambiare le cifre o scegliere dai timer predefiniti nel menu contestuale 