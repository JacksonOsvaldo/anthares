��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �               "     9  0   I  %   z     �     �     �  	   �     �  J           	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-03-02 01:25+0100
Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 &Configura le stampanti... Solo i lavori attivi Tutti i lavori Solo i lavori completati Configura la stampante Generale Nessun processo attivo Nessun processo Nessuna stampante è stata configurata o trovata Un processo attivo %1 processi attivi Un processo %1 processi Apri la coda di stampa La coda di stampa è vuota Stampanti Cerca una stampante... C'è un lavoro di stampa nella coda Ci sono %1 lavori di stampa nella coda 