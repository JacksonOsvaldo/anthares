��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  N   9     �     �     �     �     �     	     )     2  3   8     l  &   t  3   �     �  '   �     �            ;   %     a  "        �  G   �     �       .     6   M     �     �     �     �  1   �     �               5     N     c  �   v  X   ^  1   �     �  ,   �     "     9  !   S     u     �  &   �     �     �     �            
   &  )   1     [     _     b     i     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-08 09:59+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 

Scegli la striscia precedente per spostarti all'ultima striscia memorizzata. &Crea archivio del fumetto... &Salva fumetto come... Numero della &striscia: *.cbz|Archivio di fumetti (Zip) Dimensione &attuale Memorizza la &posizione attuale Avanzate Tutte Si è verificato un errore per l'identificatore %1. Aspetto Archiviazione del fumetto non riuscita Aggiorna automaticamente le estensioni del fumetto: Cache Controlla la presenza di nuove strisce: Fumetto Cache del fumetto: Configura... Impossibile creare un archivio nella posizione specificata. Crea l'archivio di fumetti %1 Creazione dell'archivio di fumetti Destinazione: Visualizza l'immagine di errore in caso di mancato recupero del fumetto Scarica fumetti Gestione degli errori Aggiunta di un file all'archivio non riuscita. Creazione del file con identificatore %1 non riuscita. Dall'inizio a ... Dalla fine a ... Generale Scarica nuovi fumetti... Recupero della striscia del fumetto non riuscita: Vai alla striscia Informazioni Vai alla stris&cia attuale Vai alla &prima striscia Vai alla striscia... Intervallo manuale Forse non disponi di una connessione a Internet.
Forse l'estensione dei fumetti è danneggiata.
Un'altra ragione potrebbe essere la mancanza del fumetto per il giorno/numero/stringa. Scegliendone una differente potrebbe funzionare. Clic con il tasto centrale del mouse sul fumetto per mostrarlo alle dimensioni originali Non esiste alcun file zip, interruzione in corso. Intervallo: Mostra le frecce solo al passaggio del mouse Mostra URL del fumetto Mostra autore del fumetto Mostra identificatore del fumetto Mostra titolo del fumetto Identificatore del fumetto: L'intervallo di strisce da archiviare. Aggiorna Visita il sito web del fumetto Visita il sito &web del negozio # %1 giorni gg.MM.aaaa Scheda successiva co&n una nuova striscia Da: A: minuti strisce per fumetto 