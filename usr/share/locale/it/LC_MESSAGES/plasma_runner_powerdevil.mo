��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �   �  b   w     �     �     �               '     0     7     T     q     �     �     �     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-04-22 21:44+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Oscura parzialmente lo schermo Oscura completamente lo schermo Elenca le opzioni di luminosità dello schermo o lo imposta alla luminosità definita da :q:; ad es. luminosità dello schermo 50 oscurerà lo schermo al 50% della luminosità massima Elenca le opzioni di sospensione del sistema (ad es. sospendi, iberna) e ne permette l'attivazione oscura lo schermo ibernazione luminosità dello schermo sospensione sospendi su disco in RAM oscuramento dello schermo %1 luminosità dello schermo %1 Imposta luminosità a %1 Sospendi su disco Sospendi in RAM Sospende il sistema in RAM Sospende il sistema su disco 