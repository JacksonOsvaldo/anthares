��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     =     L     U     b     q     �     �     �  (   �     �     �       .     
   J     U     \  %   |                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org.kde.plasma.userswitcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-11 20:51+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Utente attuale Generale Disposizione Blocca schermo Nuova sessione Inutilizzata Esci... Mostra l'immagine e il nome Mostra il nome completo (se disponibile) Mostra il nome utente Mostra solo l'immagine Mostra solo il nome Mostra le informazioni tecniche sulle sessioni su %1 (%2) TTY %1 Visualizzazione del nome utente Hai eseguito l'accesso come <b>%1</b> 