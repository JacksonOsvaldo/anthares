��          �      ,      �     �     �  	   �  $   �     �     �     �          "  	   1  
   ;     F     X  "   f  	   �  '   �  �  �     h     j     y  )   �  -   �      �          "     /  	   ?     I     R     j  $   }     �  +   �                                   	                                       
    / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Request Error: %1 Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: kdevreviewboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-04-14 22:50+0200
Last-Translator: Simone Solinas <ksolsim@gmail.com>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 / Autenticazione Cartella di base: Impossibile creare la nuova richiesta:
%1 Impossibile ottenere l'elenco delle revisioni Impossibile impostare i metadati Impossibile caricare la patch Destinazione errore JSON: %1 Password: Deposito Richiesta di errore: %1 Aggiorna revisione Nome utente nel servizio specificato Nome utente: Da dove è stato ispezionato questo oggetto 