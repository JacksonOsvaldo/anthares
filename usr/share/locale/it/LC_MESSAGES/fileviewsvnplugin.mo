��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     A      G  &   h  1   �  #   �     �  !   �      	  '   ;	  2   c	  %   �	  )   �	  4   �	  V   
  .   r
     �
  ,   �
     �
     �
     	          *     >     Z     o     |     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-10 10:50+0100
Last-Translator: Federico Zenith <zenith.federico@gmail.com>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Invia File aggiunti al repository SVN. Aggiunta di file nel repository SVN... Aggiunta di file nel repository SVN non riuscita. Invio delle modifiche non riuscito. Modifiche inviate. Invio delle modifiche in corso... File rimossi dal repository SVN. Rimozione di file dal repository SVN... Rimozione di file dal repository SVN non riuscita. File ripristinati dal repository SVN. Ripristino dei file dal repository SVN... Ripristino dei file dal repository SVN non riuscito. Aggiornamento dello stato non riuscito. Disabilito l'opzione «Mostra aggiornamenti». Aggiornamento del repository SVN non riuscito. Repository SVN aggiornato. Aggiornamento del repository SVN in corso... Aggiungi a SVN Invia con SVN... Elimina da SVN Ripristina da SVN Aggiorna con SVN... Mostra modifiche SVN locali Mostra aggiornamenti Descrizione: Invia con SVN Mostra aggiornamenti 