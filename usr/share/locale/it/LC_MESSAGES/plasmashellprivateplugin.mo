��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .     �     �  
   �  6   �     .     N     U     o  "   v     �  ,   �     �     �     �                     :     O     n     �  5   �  C   �     $	     ,	     1	  0   8	     i	     y	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-09-05 16:30+0100
Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Esegui Tutti gli oggetti Categorie: Console di creazione script della shell per il desktop Scarica nuovi oggetti di Plasma Editor Esecuzione script alle %1 Filtri Installa oggetto da file locale... Installazione non riuscita Installazione del pacchetto %1 non riuscita. Carica Nuova sessione Apri file di script Output In esecuzione Tempo di esecuzione: %1ms Salva file di script Blocco dello schermo abilitato Scadenza per il salvaschermo Seleziona un file di plasmoide Imposta dopo quanti minuti lo schermo viene bloccato. Imposta se lo schermo debba essere bloccato dopo il tempo indicato. Modelli KWin Plasma Impossibile caricare il file di script <b>%1</b> Disinstallabile Usa 