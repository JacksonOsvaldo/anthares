��          �      \      �     �     �  
   �                :      N     o  
   }     �     �     �  	   �  (   �  \   	     f  2   t     �     �  �  �     h     w  
   �  )   �     �     �  $   �          3     @  '   N     v     �  ?   �  h   �     J  6   W     �     �     	                                                                                 
             %1 Terms: %2 (c) 2012, Vishesh Handa Baloo Show Device id for the files EMAIL OF TRANSLATORSYour emails File Name Terms: %1 Inode number of the file to show Internal Info Maintainer NAME OF TRANSLATORSYour names No index information found Print internal info Terms: %1 The Baloo data Viewer - A debugging tool The Baloo index could not be opened. Please run "%1" to see if Baloo is enabled and working. The file urls The fileID is not equal to the actual Baloo fileID This is a bug Vishesh Handa Project-Id-Version: balooshow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 03:17+0100
PO-Revision-Date: 2018-01-31 09:49+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1 termini: %2 (c) 2012, Vishesh Handa Baloo Show Identificativo del dispositivo per i file luigi.toscano@tiscali.it Termini di nome di file: %1 Numeri di inode del file da mostrare Informazioni interne Responsabile Luigi Toscano Nessun informazioni trovata nell'indice Stampa le informazioni interne Termini: %1 Il visualizzatore di dati di Baloo - uno strumento per il debug Impossibile aprire l'indice di Baloo. Esegui «%1» per controllare se Baloo è abilitato e in funzione. URL del file L'ID del file non corrisponde all'ID corrente di Baloo Questo è un errore Vishesh Handa 