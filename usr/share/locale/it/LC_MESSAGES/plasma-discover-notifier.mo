��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     O  5   V  ;   �  5   �     �  I     &   g     �     �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: muon-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-19 09:53+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1, %2 Un pacchetto da aggiornare %1 pacchetti da aggiornare Un aggiornamento di sicurezza %1 aggiornamenti di sicurezza Un pacchetto da aggiornare %1 pacchetti da aggiornare Nessun pacchetto da aggiornare di cui un aggiornamento di sicurezza di cui %1 aggiornamenti di sicurezza Aggiornamenti di sicurezza disponibili Sistema aggiornato Aggiornamento Aggiornamenti disponibili 