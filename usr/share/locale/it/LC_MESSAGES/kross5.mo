��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     (     /     N     ^     u     ~     �  
   �  	   �  "   �     �     �     	     	  5   /	  A   e	  (   �	  +   �	     �	     
     	
  )   
  I   ?
     �
      �
  )   �
     �
     �
       $     
   -  1   8     j  9   q     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-01-11 20:36+0100
Last-Translator: Federico Zenith <zenith.federico@gmail.com>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Autore Copyright 2006 Sebastian Sauer Sebastian Sauer Lo script da eseguire. Generale Aggiungi un nuovo script. Aggiungi... Annullare? Commento: federico.zenith@member.fsf.org,,,, Modifica Modifica lo script selezionato. Modifica... Esegui lo script selezionato. Impossibile creare uno script per l'interprete «%1» Impossibile determinare l'interprete per il file di script «%1» Impossibile caricare l'interprete «%1» Impossibile aprire il file di script «%1» File: Icona: Interprete: Livello di sicurezza dell'interprete Ruby Federico Zenith,Vincenzo Reale,Dario Panico,Nicola Ruggero,Federico Cozzi Nome: Nessuna funzione dal nome «%1» Nessun interprete «%1» è stato trovato Rimuovi Rimuovi lo script selezionato. Esegui Il file di script «%1» non esiste. Interrompi Interrompi l'esecuzione dello script selezionato. Testo: Strumento a riga di comando per eseguire script di Kross. Kross 