��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  !   �  �    �   	  \   �
  "   �
  "        >  0   U     �     �  !   �  (   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-05-23 08:49+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
  sec Tempo massimo indicazione a&vvio: <h1>Notificazione barra delle applicazioni</h1>
Puoi abilitare un secondo metodo di notificazione dell'avvio che sarà
usato dalla barra delle applicazioni, dove apparirà un pulsante con una
clessidra che ruota, che sta ad indicare che l'applicazione che hai avviato sta
caricando.
Potrebbe verificarsi che alcune applicazioni non siano al corrente di questa
notificazione dell'avvio. In questo caso, il scomparirà trascorso il tempo
indicato da «Tempo massimo indicazione avvio» <h1>Puntatore di occupato</h1>
KDE offre un puntatore di occupato per notificare l'avvio di un'applicazione.
Per abilitare il puntatore occupato, seleziona un tipo di notifica visiva dalla casella.
Può succedere che alcune applicazioni non siano al corrente di questa
notifica dell'avvio. In questo caso, il puntatore smetterà di lampeggiare
trascorso il tempo indicato da «Tempo massimo indicazione avvio» <h1>Notifica lancio</h1> Puoi configurare il tipo di notifica dell'avvio delle applicazioni. Puntatore di occupato lampeggiante Puntatore di occupato che rimbalza Puntatore di occ&upato Abilita no&tifica nella barra delle applicazioni Nessun puntatore di occupato Puntatore di occupato passivo Tempo massimo indicazione a&vvio: &Notifica nella barra delle applicazioni 