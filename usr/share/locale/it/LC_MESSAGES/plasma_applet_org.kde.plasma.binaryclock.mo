��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     5     =     E     Z     m  *   �  (   �  ,   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-20 11:44+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aspetto Colori: Visualizza i secondi Disegna la griglia Mostra LED inattivi: Usa colore personalizzato per i LED attivi Usa colore personalizzato per la griglia Usa colore personalizzato per i LED inattivi 