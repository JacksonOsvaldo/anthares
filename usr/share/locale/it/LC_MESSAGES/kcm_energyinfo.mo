��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	  %   �	     �	  	   �	     �	     �	     
     
     #
     '
     4
     G
     \
     d
  "   w
     �
     �
     �
     �
     �
     �
                    -     =  
   T  
   _     j     r     �     �     �     �     �     �     �     �     �     �  M   �     0  ,   D  	   q     {  	   }     �     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-22 22:31+0200
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 % %1 %2 %1: Consumo di energia delle applicazioni Batteria Capacità Percentuale di carica Stato di carica In ricarica Corrente °C Dettagli: %1 In fase di scarica smart2128@baslug.org Energia Consumo di energia Statistiche sul consumo di energia Ambiente Piena capacità costruttiva Completamente carica Ha l'alimentatore Kai Uwe Broulik Ultime 12 ore Ultime 2 ore Ultime 24 ore Ultime 48 ore Ultimi 7 giorni Ultima piena capacità Ultima ora Produttore Modello Vincenzo Reale No Non in carica PID: %1 Percorso: %1 Ricaricabile Aggiorna Numero seriale W Sistema Temperatura Questo tipo di storico non è attualmente disponibile per questo dispositivo. Intervallo di tempo Intervallo di tempo dei dati da visualizzare Venditore V Voltaggio Wakeup al secondo: %1 (%2%) W Wh Sì Consumo % 