��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  %   �       0         L     m  	   v     �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org.kde.plasma.diskquota
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-11-22 20:23+0100
Last-Translator: Vincenzo Reale <smart2128@baslug.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Quota disco Non c'è alcuna limitazione di quota. Installa «quota» Strumento quota non trovato.

Installa «quota» Esecuzione di quota non riuscita %1 di %2 %1 liberi Quota: %1% utilizzato %1: %2% utilizzato 