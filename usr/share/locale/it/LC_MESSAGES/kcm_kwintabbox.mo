��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �  	   �     �     �     �     �     �  	   �  	   �               *     :     J  g   a  	   �  1   �     	  %   	  
   =	     H	  "   W	     z	     �	     �	     �	     �	     �	     �	  �   �	  Z   �
     �
     �
                         !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-07 22:33+0200
Last-Translator: Federico Zenith <federico.zenith@member.fsf.org>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Attività Altre attività Altri desktop Altri schermi Tutte le finestre Alternativo Desktop 1 Contenuto Attività attuale Applicazione attuale Desktop attuale Schermo attuale Filtra le finestre per Le impostazioni della politica di attivazione limitano la funzionalità della navigazione tra finestre. In avanti Recupera la disposizione del nuovo cambiafinestre Finestre nascoste Includi l'icona «Mostra il desktop» Principale Minimizzazione Solo una finestra per applicazione Usati di recente All'indietro Schermi Scorciatoie Mostra la finestra selezionata Ordinamento: Ordine di impilamento La finestra attualmente selezionata verrà evidenziata facendo dissolvere tutte le altre. Questa opzione richiede che siano attivi gli effetti del desktop. L'effetto per sostituire la finestra di elenco quando gli effetti del desktop sono attivi. Desktop virtuali Finestre visibili Visualizzazione 