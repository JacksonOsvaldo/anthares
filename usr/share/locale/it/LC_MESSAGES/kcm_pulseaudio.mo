��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  (   �  '   	  )   /	  *   Y	  (   �	     �	  
   �	     �	     �	     �	  X   �	  !   <
  ]   ^
     �
     �
     �
     �
     �
               &     <     C     P     V     i  ,   z  H   �     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-03 20:29+0200
Last-Translator: Vincenzo Reale
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 100% Autore Copyright 2015 Harald Sitter Harald Sitter Nessuna applicazione che riproduce audio Nessuna applicazione che registra audio Nessun profilo di dispositivo disponibile Nessun dispositivo di ingresso disponibile Nessun dispositivo di uscita disponibile Profilo: PulseAudio Avanzate Applicazioni Dispositivi Aggiungi il dispositivo virtuale per l'uscita simultanea su tutte le schede audio locali Configurazione di uscita avanzata Commuta automaticamente tutti i flussi in riproduzione quando è disponibile una nuova uscita Cattura Predefinito Profili di dispositivo smart2128@baslug.org  Ingressi Silenzia l'audio Vincenzo Reale Suoni delle notifiche Uscite Riproduzione Porta  (non disponibile)  (non collegata) Richiede il modulo PulseAudio 'module-gconf' Questo modulo consente di configurare il sottosistema sonoro Pulseaudio. %1: %2 100% %1% 