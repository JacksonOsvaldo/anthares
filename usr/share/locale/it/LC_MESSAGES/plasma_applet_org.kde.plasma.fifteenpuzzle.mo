��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     d  
   l     w     �  5   �     �  %   �               ,  
   4     ?     ^  	   u          	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-03-02 09:48+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aspetto Sfoglia... Scegli un'immagine Gioco del quindici File immagine (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Colore del numero Percorso dell'immagine personalizzata Colore del pezzo Mostra i numeri Mescola Dimensioni Risolvi ripristinando l'ordine Risolto! Prova ancora. Tempo: %1 Usa un'immagine personalizzata 