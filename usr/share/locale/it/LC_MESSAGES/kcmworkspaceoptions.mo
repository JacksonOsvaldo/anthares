��    	      d      �       �      �   -   �   *   %      P  '   q  
   �     �     �  �  �       <   �  9   �       1   *     \     i     v                   	                           (c) 2009 Marco Martin Display informational tooltips on mouse hover Display visual feedback for status changes EMAIL OF TRANSLATORSYour emails Global options for the Plasma Workspace Maintainer Marco Martin NAME OF TRANSLATORSYour names Project-Id-Version: kcmworkspaceoptions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-11-17 10:46+0100
Last-Translator: Paolo Zamponi <zapaolo@email.it>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 © 2009 Marco Martin Visualizza i suggerimenti informativi al passaggio del mouse Visualizza un riscontro visivo per i cambiamenti di stato federico.zenith@member.fsf.org Opzioni globali per lo spazio di lavoro di Plasma Responsabile Marco Martin Federico Zenith 