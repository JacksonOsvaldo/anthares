��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  #   �  	   �     �     	  '        A  0   T     �  9   �  H   �  _     =   x  9   �     �     �     	  L   ,	  :   y	  	   �	  :   �	  '   �	     !
  :   4
  
   o
  ;   z
  
   �
  V   �
                                            
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.5.5
 ئادەتتىكى يېڭى قوليازما قوش… قوش… ۋاز كەچ؟ ئىزاھات: sahran.ug@gmail.com,  gheyret@gmail.com تەھرىرلەش تاللانغان قوليازما تەھرىر تەھرىر… تاللانغان قوليازمىنى ئىجرا قىل "%1" تەرجىمە قىلغۇچقا قوليازما قۇرالمىدى "%1" قوليازما ھۆججەتكە تەرجىمە قىلغۇچ بەلگىلىيەلمىدى "%1" تەرجىمە قىلغۇچنى يۈكلىيەلمىدى "%1" قوليازما ھۆججەتنى ئاچالمىدى ھۆججەت: سىنبەلگە: تەرجىمە قىلغۇچ Ruby تەرجىمە قىلغۇچنىڭ بىخەتەرلىك دەرىجىسى ئابدۇقادىر ئابلىز, غەيرەت كەنجى ئاتى: <icode>%1</icode> دەيدىغان فۇنكسىيە يوق "%1" تەرجىمە قىلغۇچ يوق چىقىرىۋەت تاللانغان قوليازمىنى چىقىرىۋەت ئىجرا "%1" قوليازما ھۆججەت مەۋجۇت ئەمەس. توختا تاللانغان قوليازمىنىڭ ئىجرا قىلىنىشىنى توختات تېكىست: 