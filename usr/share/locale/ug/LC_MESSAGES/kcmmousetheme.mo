��          �      L      �     �  m   �  `   N     �     �     �     �     �           %  '   6     ^     }     �     �  ?   �  Y   �  +   :  �  f       �   &  {   �     n  0   }     �     �  [   �  '   ?     g  H   x  :   �     �  %   	  #   +	  h   O	  �   �	  C   >
                                                                
                            	        (c) 2003-2007 Fredrik Höglund <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 (c) 2003-2007 Fredrik Höglund <qt>نۆۋەتتە ئىشلىتىۋاتقان ئۆرنەكنى ئۆچۈرگىلى بولمايدۇ.<br />ئالدى بىلەن باشقا بىر ئۆرنەككە ئالمىشىشىڭىز كېرەك.</qt> سىنبەلگە  ئۆرنەك قىسقۇچىدا %1 ئاتلىق ئۆرنەك مەۋجۇت. ئالماشتۇرامسىز؟ جەزملەش نۇربەلگە تەڭشىكى ئۆزگەردى نۇربەلگە ئۆرنىكى چۈشەندۈرۈش ئۆرنەك تور ئادرېسى(URL)نى سۆرەپ كېلىڭ ياكى كىرگۈزۈڭ sahran.ug@gmail.com,  gheyret@gmail.com Fredrik Höglund ئىنتېرنېتتىن يېڭى رەڭ لايىھىسىنى ئېلىش ئابدۇقادىر ئابلىز, غەيرەت كەنجى ئاتى ئۆرنەكنى قاپلامسىز؟ ئۆرنەكنى چىقىرىۋەت «%1» ئىناۋەتلىك نۇربەلگە ئۆرنەك  ئارخىپى ئەمەستەك تۇرىدۇ. نۇربەلگە ئۆرنەك ئارخىپىنى چۈشۈرگىلى بولمىدى؛ ئادرېس %1 توغرىمۇ تەكشۈرۈڭ. نۇربەلگە ئۆرنەك ئارخىپى %1 تېپىلمىدى. 