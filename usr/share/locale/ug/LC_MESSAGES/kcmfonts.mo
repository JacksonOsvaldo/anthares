��    "      ,  /   <      �     �     �               1     O     \     l     �     �  I   �     �  *   �          6  !   P  "   r     �  6   �  *   �          !     .     E     X     h     y     �     �     �     �     �     �  �       �  9   �     �  O   �     C     E     S  7   r     �  ?   �  �   �     �	  [   �	  %   �	  %   $
  #   J
  f   n
  B   �
  <     �   U  !   �  !   
     ,     D     W  
   h     s     �     �     �     �     �     �     "                
                      	                                                                                                               !     to  Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Smallest font that is still readable well. Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB font usageFixed width font usageGeneral font usageMenu font usageSmall font usageToolbar font usageWindow title full hintingFull medium hintingMedium no hintingNone no subpixel renderingNone slight hintingSlight Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
  غا  بارلىق خەت نۇسخىلىرىنى تەڭشە(&J) BGR ھەممە خەت نۇسخىلىرىنى كۆرسىتىش ئۈچۈن چېكىڭ c سەپلە… يەكلەش دائىرىسى: خەت نۇسخا تەڭشىكى ئۆزگەرتىلدى %1:  خەت نۇسخىسىنىڭ DPI نى بۇنىڭغا زورلا: خىنتلاش — كىچىك خەتلەرنى ئېنىق قىلىپ كۆرسىتىش ئۈچۈن ئىشلىتىلىدىغان بىر تەرەپ قىلىش ئۇسۇلىدۇر. RGB كىچىك خەت نۇسخىسىنى يەنىلا ياخشى ئوقۇغىلى بولىدۇ. ئىناۋەتسىز قىلىنغان ئىناۋەتلىك قىلىنغان سىستېما تەڭشەكلىرى تىزىملىك بالدىقى ۋە سەكرىمە تىزىملىكلەردە ئىشلىتىلىدۇ. كۆزنەك ماۋزۇ بالدىقىدا ئىشلىتىلىدۇ. نورمال تېكىست ئۈچۈن ئىشلىتىلىدۇ. قورال بالدىقىنىڭ سىنبەلگىسىنىڭ يېنىدا تېكىستلەرنى كۆرسىتىش ئۈچۈن ئىشلىتىلىدۇ. تىك يۆنىلىشتىكى BGR تىك يۆنىلىشتىكى RGB مۇقىم كەڭلىك ئادەتتىكى تىزىملىك كىچىك قورال بالداق كۆزنەك ماۋزۇسى تامامەن ئوتتۇراھال يوق يوق ئاز 