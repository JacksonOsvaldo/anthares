��            )   �      �     �  9   �  (   �  9   	  $   C     h     y  ,     N   �  
   �        	   '  �   1     �     �  e   �     L     \     n  
   �     �     �  &   �  %   �  f   �     [     k     �     �  �  �     Z  ?   f  
   �  9   �     �  %   �     	     +	  s   8	     �	  '   �	  
   �	  �   �	     �
  :   �
  �     2   �  7     0   9  "   j     �  G   �  <   �  J   #  �   n       4     ;   R  5   �                                                                                                                         
      	           min @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Screen Brightness @label:slider Brightness levelLevel Activity Manager After Brightness level, label for the sliderLevel Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
  مىنۇت ئېكران يورۇقلۇق دەرىجىسى تۆۋەنلەت ئۈچەك ئېكران يورۇقلۇق دەرىجىسى ئاشۇر دەرىجە پائالىيەت باشقۇرغۇچ داۋامى دەرىجە توكدان ئارايۈزىگە باغلىنالمىدى.
سىستېما سەپلىمىسىنى تەكشۈرۈڭ. ھېچقانداق قىلما sahran.ug@gmail.com,  gheyret@gmail.com ئۈچەك KDE مەنبە باشقۇرۇش سىستېمىسىنى دەسلەپلەشتۈرگىلى بولمىدى. ئارقائۇچ تۆۋەندىكى خاتالىقنى مەلۇم قىلدى: %1 ئېكراننى قۇلۇپلا ئابدۇقادىر ئابلىز, غەيرەت كەنجى ئىناۋەتلىك توك مەنبە باشقۇرۇش ئارقا ئۇچ قىستۇرمىسى يوق. يېڭى ئورنىتىش بۇ مەسىلىنى ھەل قىلالىشى مۇمكىن. سەپلىمە ھۆججەت يۈكلەنگەندە سەپلىمە ھۆججەت يۈك چۈشۈرگەندە چىقىش سۆزلەشكۈسىنى كۆرسەت قوليازما ئىجرا قىل قوليازما تاقاشتىن ئىلگىرى كېچىكتۈرىدىغان ۋاقىت توك مەنبە ماسلاشتۇرغۇچى چېتىلدى. توك مەنبە ماسلاشتۇرغۇچى ئاجرىتىۋېتىلدى. «%1» دېگەن profile تاللاندى، بىراق ئۇ مەۋجۇت ئەمەس.
PowerDevil سەپلىمىسىنى تەكشۈرۈپ كۆرۈڭ. ئېكراننى تاقا قوللىمايدىغان توڭلات ئۇسۇلى يان كومپيۇتېر قاپقاقنى ياپقاندا توك مەنبە توپچىسى بېسىلغاندا 