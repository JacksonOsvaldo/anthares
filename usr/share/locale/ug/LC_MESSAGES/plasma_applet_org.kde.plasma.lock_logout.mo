��          �            x  &   y  +   �  	   �     �     �     �            (        ?     B  ,   Y     �     �     �  �  �  Q   L  M   �  
   �  .   �     &     3     C  %   R  ^   x     �  '   �  a        h  '   u     �                                
                                    	           Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2013-09-08 07:05+0900
Last-Translator: Gheyret Kenji <gheyret@gmail.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 سىز سىستېمىنى ئەسلەككە توڭلىتامسىز(ئۇخلات)؟ سىز سىستېمىنى دىسكىغا توڭلىتامسىز(ئۈچەك)؟ ئۈچەك ئۈچەك (دىسكىغا توڭلىتىدۇ) ئايرىل ئايرىل… قۇلۇپلا ئېكراننى قۇلۇپلايدۇ كومپيۇتېرنى تىزىمدىن چىقار، تاقا ياكى قايتا قوزغات ياق ئۇخلا(RAM غا توڭلىتىدۇ) باشقا ئىشلەتكۈچى سۈپىتىدە پاراللېل ئەڭگىمە باشلايدۇ توڭلات ئىشلەتكۈچى ئالماشتۇر ھەئە 