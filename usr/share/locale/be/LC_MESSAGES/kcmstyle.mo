��          �   %   �      `  &   a  �   �                    (     6     C     T      d  	   �  �   �  c   X     �     �     �     �               '     3     9  	   ?  C   I  j   �     �  �    A     �   F     0	     =	     [	     y	     �	  #   �	     �	  -   �	     
  C  5
  �   y     @     ^  )   r  (   �     �  )   �     �          '     8  m   P  X   �  2                                                                            
                  	                                 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. Unable to Load Dialog Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2007-08-22 12:31+0000
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);

 (c) 2002 Карал Швед, Даніель Молькенцін <h1>Стыль</h1>Гэты модуль дазваляе змяняць вонкавы выгляд элементаў інтэрфейсу карыстальніка, напрыклад, стыль віджэтаў і эфекты. Кнопка Параметр выбару Выпадаючае меню На&стаўленні... Настаўленне %1 Даніель Молькенцін Апісанне: %1 greendeath@mail.ru, ihar.hrachyshka@gmail.com Рамка групавання Тут вы можаце выбраць адзін з прадвызначаных стыляў віджэтаў (г.зн. спосаб прамалёўкі кнопак), які можна ці нельга спалучаць з тэмай (напрыклад, мармуравая тэкстура ці градыент). Калі вы ўключыце, гэты параметр, праграмы KDE будуць паказваць маленькія значкі каля некаторых важных кнопак. Модуль стыляў KDE Карал Швед Eugene Zelenko, Ігар Грачышка Апісанне недаступнае. Прагляд Кнопка-пераключальнік Ральф Нолдэн Укладка 1 Укладка 2 Толькі тэкст Адбылася памылка адкрыцця вакна настаўленняў гэтага стылю. Гэтая вобласць паказвае выгляд выбранага стылю. Немагчыма загрузіць дыялог 