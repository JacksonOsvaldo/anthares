��    U      �  q   l      0     1     9     W     p     u     z          �  6   �  	   �     �     �  	   �  H   �  H   (     q     x     �     �     �     �     �     �     �     �     �     �     �     	     '	     @	     H	      X	     y	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	  
   �	  
   �	     �	     �	     �	     
     
     
     
     :
     T
  B   f
     �
     �
     �
     �
     �
     �
     �
     �
       	                  2     H     \     o     t     z     �     �     �     �     �     �     �     �     �     �  &   �  �                 4     M     T     [     b     i  M   p     �     �     �     �  A     A   E     �     �  
   �  )   �     �  1   �     %     ?     Y     j     s     �  7   �  $   �  ,   
     7     J     b     s     �     �  
   �  !   �     �     �       
     !   !     C     X     k     x  
   �     �     �     �  '   �  /   �  "   .  +   Q     }     �  #   �  #   �     �               &     B     ^     |  3   �  2   �  $   �  0     
   F  
   Q     \     k     �     �     �     �     �     �               )  A   G     *       .       7   <   4   B      M      (   I   )            P   %                  -   G   @         O   #   S              L   8   ,   :   $                      K       &   1   J   !      "          R   T      3          5              >   D   C   /   H   ;   6   	   A                         =       '   F   2   N      +      ?      9              
            U              E          Q                 0        %1 [%2] (C) Craig Drummond, 2004-2007 (C) Craig Drummond, 2007 12pt 18pt 24pt 36pt 48pt <p>Do you really want to delete</p><p>'<b>%1</b>'?</p> Add Fonts Add... All All Fonts All of the letters of the alphabet, lowercaseabcdefghijklmnopqrstuvwxyz All of the letters of the alphabet, uppercaseABCDEFGHIJKLMNOPQRSTUVWXYZ Arabic Armenian Black Cannot Print Category Change Preview Text... Change Text... Craig Drummond Cyrillic Date Delete Font Delete Fonts Delete all selected fonts Deleting font(s)... Developer and maintainer Disable Duplicate Fonts EMAIL OF TRANSLATORSYour emails Enable Enabling Family Font Font Installer Font Printer Font Viewer Font/File Group Install fonts Install... Installing Italic Latin Light Links To Medium Moving NAME OF TRANSLATORSYour names No duplicate fonts found. Nothing to Delete Numbers and characters0123456789.:,;(*!?'/\")£$€%^&-+@~#<>{}[] Personal Personal Fonts Preview String Preview Type Print... Reload Remove Remove Group Remove group Rename... Roman Select Font to View Simple font installer Simple font printer Simple font viewer Size Style System System Fonts Tools URL to install URL to open UTF-8 Unclassified Uninstalling Unknown Updating Where to Install You did not select anything to delete. Project-Id-Version: kfontinst
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:19+0100
PO-Revision-Date: 2007-10-28 14:01+0200
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %1 [%2] (c) Craig Drummond, 2004-2007 (c) Craig Drummond, 2007 12пт 18пт 24пт 36пт 48пт <p>Вы сапраўды хочаце выдаліць</p><p>'<b>%1</b>'?</p> Дадаць шрыфты Дадаць... Усе Усе шрыфты абвгдеёжзійклмнопрстуўфхцчш'ыьэюя АБВГДЕЁЖЗІЙКЛМНОПРСТУЎФХЦЧШ'ЫЬЭЮЯ Арабская Армянская Чорны Немагчыма надрукаваць Катэгорыя Змяніць тэкст перадаглду... Змяніць тэкст Крэг Драммонд Кірыліца Дата Выдаліць шрыфт Выдаліць шрыфты Выдаліць усе вылучаныя шрыфты Выдаленне шрыфтоў... Распрацоўка і падтрымка Адключыць Шрыфты-дублі symbol@akeeri.tk Уключыць Уключэнне Сям’я Шрыфт Устаноўка шрыфтоў Друк шрыфта Прагляд шрыфта Шрыфт/Файл Група Устанавіць шрыфты Устанавіць Устаноўка Курсіў Лацінка Лёгкі Спасылаецца на Сярэдні Перамяшчэнне Дарафей Праляскоўскі Шрыфты-дублі не знойдзены Няма чаго выдаляць 0123456789.:,;(*!?'/\")£$€%^&-+@~#<>{}[] Асабістыя Асабістыя шрыфты Страка перадагляда Страка перадагляда Друк... Абнавіць Выдаліць Выдаліць групу Выдаліць групу Перайменаваць... Просты Выберыце шрыфт для прагляду Просты ўстаноўшчык шрыфтоў Просты друк шрыфтоў Просты праглядчык шрыфтоў Памер Стыль Сістэма Сістэмныя шрыфты Начынне URL для устаноўкі Адкрыць URL UTF-8 Некласіфікаваны Выдаленне Невядомы Абнаўленне Куды устанавіць Вы не вылучылі нічога, каб выдаліць. 