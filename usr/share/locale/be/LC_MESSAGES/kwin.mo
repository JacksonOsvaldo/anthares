��    E      D  a   l      �     �     �            
     #   #     G     T     e  
   u     �      �     �     �     �     �     �          $     =     V     r     ~     �  	   �  
   �     �     �     �     �       	        )     9     E     d     u     �     �     �  9   �     �     
	      	     -	     E	     [	     z	     �	     �	     �	  :   �	     
     7
  #   Q
     u
     �
     �
     �
     �
     �
          ,     L     c     z     �  S   �  �  �  %   �       $   &     K     a  I   t     �     �     �       4   /  *   d  "   �  !   �     �  (   �  +     +   .  5   Z  5   �  G   �       &   &     M     c     {  J   �     �  !   �  =     9   O     �     �     �  A   �  *   !  ,   L  *   y  ,   �  $   �  l   �  $   c  3   �     �  F   �  F     B   f  D   �  H   �  9   7  J   q  c   �  6      >   W  Q   �  3   �  F         c  3   �  6   �  6   �  2   &  4   Y  +   �  8   �  )   �  :     �   X            (   )       -      >      "       *   ,   =   #          !   7       A      /   ?               $          1              &   3   '   4             %      	      .      E      
   :                                               ;       8   @       5      B          6   0   D   <              9                +                       C          2    &All Desktops &Close &Fullscreen &Move &No Border Activate Window Demanding Attention Close Window Cristian Tibirna Daniel M. Duley Desktop %1 Disable configuration options EMAIL OF TRANSLATORSYour emails Hide Window Border KDE window manager KWin KWin helper utility Keep &Above Others Keep &Below Others Keep Window Above Others Keep Window Below Others Keep Window on All Desktops Kill Window Lower Window Luboš Luňák Ma&ximize Maintainer Make Window Fullscreen Matthias Ettrich Maximize Window Maximize Window Horizontally Maximize Window Vertically Mi&nimize Minimize Window Move Window NAME OF TRANSLATORSYour names Pack Window Down Pack Window Up Pack Window to the Left Pack Window to the Right Raise Window Replace already-running ICCCM2.0-compliant window manager Resize Window Setup Window Shortcut Shade Window Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Next Desktop Switch to Next Screen Switch to Previous Desktop This helper utility is not supposed to be called directly. Toggle Window Raise/Lower Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Walk Through Windows Walk Through Windows (Reverse) Window One Desktop Down Window One Desktop Up Window One Desktop to the Left Window One Desktop to the Right Window Operations Menu Window to Next Desktop Window to Next Screen Window to Previous Desktop kwin: unable to claim manager selection, another wm running? (try using --replace)
 Project-Id-Version: kwin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:16+0100
PO-Revision-Date: 2007-12-09 18:32+0200
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Усе працоўныя сталы За&крыць &Поўнаэкранны рэжым &Перасунуць &Без межаў Актывізаваць вакно, якое патрабуе ўвагі Закрыць акно Крысціян Цібірна Даніэль М. Дулі Працоўны стол %1 Выключыць параметры наладкі ihar.hrachyshka@gmail.com,symbol@akeeri.tk Схаваць межы вакна Кіраўнік вокнаў KDE KWin Дапаможная утыліта KWin Трымаць &вышэй астатніх Трымаць &ніжэй астатніх Трымаць вакно вышэй астатніх Трымаць вакно ніжэй астатніх Трымаць вакно на ўсіх працоўных сталах Забіцьв акно Апусціць вакно ніжэй Любаш Люнак Най&большыць Адказны Разгарнуць вакно ў поўнаэкранным рэжыме Матыяс Эттрых Найбольшыць вакно Найбольшыць вакно па гарызанталі Найбольшыць вакно па вертыкалі Най&меншыць Найменшыць вакно Перасунуць вакно Ігар Грачышка,Дарафей Праляскоўскі Згрувапаць вокны ўнізе Згрувапаць вокны ўверсе Згрувапаць вокны ўлева Згрувапаць вокны ўправа Падняць вакно вышэй Замяніць ужо запушчаны кіраўнік вокнаў, сумяшчальны з ICCCM2.0 Змяніць памер вакна Усталяваць скарот для вакна Зацяніць вакно Пераключыцца на ніжэйшы працоўны стол Пераключыцца на вышэйшы працоўны стол Пераключыцца на працоўны стол злева Пераключыцца на працоўны стол справа Пераключыцца на наступны працоўны стол Пераключыцца на наступны экран Пераключыцца на папярэдні працоўны стол Гэтую дапаможную праграму не трэба выклікаць уручную. Перанос на задні/пярэдні план Пераход па спісе працоўных сталоў Адваротны пераход па спісе працоўных сталоў Пераход па працоўных сталах Адваротны пераход па працоўных сталах Пераход па вокнах Адваротны пераход па вокнах Акно на ніжэйшы працоўны стол Акно на вышэйшы працоўны стол Акно на працоўны стол злева Акно на працоўны стол справа Меню дзеянняў з вокнамі Акно на наступны працоўны стол Акно на наступны экран Акно на папярэдні працоўны стол kwin: немагчыма стаць кіраўніком вокнаў, запушчаны іншы wm? (паспрабуйце --replace)
 