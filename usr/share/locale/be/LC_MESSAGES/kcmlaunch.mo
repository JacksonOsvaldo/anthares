��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     A  *   I  �  t  �  @
  �   �  !   `     �     �  O   �  &     (   ,  *   U  6   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-10-28 14:23+0200
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  сек Час індыкацыі &запуску: <H1>Абвяшчэнне ў панелі заданняў</H1>
Вы можаце задзейнічаць другі тып абвяшчэння запуску, які
выкарыстоўваецца панеллю заданняў, дзе пераварочваецца кнопка з пясочным гадзіннікам,
што сведчыць пра загрузку запушчанай праграмы.
Можа стацца, што некаторыя праграмы не маюць на ўвазе гэтае абвяшчэнне празапуск
Тады кнопка знікне пасля выхаду часу,
вызначанага ў секцыі 'Час індыкацыі запуску' <h1>Заняты курсор</h1>
KDE прапануе заняты курсор для абвяшчэння запуску праграм.
Каб задзейнічаць заняты курсор, вылучце адзін тып візуальнай адваротнай
сувязі з выпадальнага спісу.
Можа стацца, што некаторыя праграмы не маюць на ўвазе гэтае абвяшчэнне аб запуску
Тады курсор скончыць мільгаценне пасля выхаду часу,
вызначанага ў секцыі 'Час індыкацыі запуску' <h1>Адказ на запуск</h1> Тут вы можаце наставіць зваротны адказ на запуск праграм. Курсор мільгаціць Курсор скача Заняты &курсор Задзейнічаць &абвяшчэнне ў панелі заданняў Без занятага курсора Пасіўны заняты курсор Час &індыкацыі запуску: Абвешчанне ў &панелі заданняў 