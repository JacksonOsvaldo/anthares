��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  �   0  �     n   �     "  8   ;     t  ]   �     �     �  
   	  "   "	     E	  f   _	  �   �	  M   c
        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-08-26 22:21+0300
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 <qt>Вы сапраўды хочаце выдаліць тэму курсораў <strong>%1</strong>?<br />Гэтае дзеянне выдаліць усе файлы, устаноўленыя з гэтай тэмай.</qt> <qt>Вы не можаце выдаліць тэму, якой карыстаецеся.<br />Вам трэба спачатку выбраць іншую.</qt> Тэма з назвай %1 ужо існуе. Хочаце замяніць старую тэму новай? Пацверджанне Настаўленні курсору змяніліся Апісанне Перацягніце сюды ці вызначце самі спасылку на тэму ihar.hrachyshka@gmail.com Ігар Грачышка Назва Перазапісаць тэму? Выдаліць тэму Файл %1 не з'яўляецца сапраўдным архівам з тэмай курсору. Немагчыма сцягнуць архіў з тэмай курсору. Калі ласка, праверце правільнасць адраса %1. Немагчыма знайсці архіў з тэмай курсору %1. 