��          �      \      �     �     �  
   �     �                    '     E     d     j     v     �     �     �     �     �     �       �            -     E     a     t     �  !   �  4   �  5   �  
   *     5     K     b  6   w  7   �  6   �  7     2   U  3   �                                                
                                               	    %1 of %2 processed %1 processed Appearance Behavior Cancel Configure... Finished Jobs Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-08-26 21:06+0300
Last-Translator: Darafei Praliaskouski <komzpa@licei2.com>
Language-Team: Belarusian <i18n@mova.org>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: KBabel 1.11.4
 %1 з %2 апрацавана %1 апрацавана Вонкавы выгляд Паводзіны Скасаваць Наставіць... Скончаныя заданні Перамясціць іх у асобны спіс Перамясціць іх у асобны спіс. Паўза Выдаліць іх Выдаліць іх. Працягнуць Паказваць усе заданні ў спісу Паказваць усе заданні ў спісу. Паказваць усе заданні ў дрэве Паказваць усе заданні ў дрэве. Паказваць аддзеленыя вокны Паказваць аддзеленыя вокны. 