��    :      �  O   �      �     �                    ,     3  �  N  �   )  -   �  )   �  -   	  +   ;	  r   g	  	   �	  	   �	     �	     
     
     
  
   $
     /
     ;
     C
     K
      b
     �
     �
     �
      �
     �
     �
  r   �
  5   ]     �     �     �  	   �     �     �     �  (   �                9     S     n     t  +   �  3   �     �     �     �     �  S     )   _     �  �   �  �  s  
   (     3     <     K     ]     d  �    �   #  +   �     �  	   �     �  `   �  	   W     a     n     �     �     �     �     �  
   �     �  $   �     �          !     '  $   4     Y     `  f     ?   �     &     9  	   K     U     b     j     y  "   �     �  !   �  "   �  #        )     2  !   ?  0   a  
   �  	   �  	   �     �  W   �  &        =  �   Q               2                '   (          1      !            8                                       .               5   	   6   $      )       7   4          *                 0   
             :         ,                    +      %      "   -   /   #       9           &      3    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2013-06-11 08:31+0300
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 D&audzums: &Efekts: &Otrā krāsa: Pu&scaurspīdīgs &Tēma (c) 2000-2003 Geert Jansen <h1>Ikonas</h1> Šis modulis ļauj izvēlēties darbvirsmas ikonas. <p>Lai izvēlētos ikonu tēmu, klikšķiniet uz tās nosaukuma un tad apstipriniet savu izvēli ar "Pielietot" pogu. "Atstatīt" poga savukārt atcels veikās izmaiņas.</p><p>Izmantojot "Instalēt jaunu tēmu" pogu ir iespējams pievienot jaunu tēmu, kuru var izvēlēties no failu pārlūkošanas loga. Tēmas instalēšana tiks veikta pēc "Labi" pogas nospiešanas.</p><p>"Noņemt tēmu" poga būs aktīva tikai tad, ja izvēlēsieties tēmu, kuru jūs esat instalējis. Jūs nevarat noņemt globāli instalētās tēmas.</p><p>Šeit ir iespējams arī norādīt ikonām pielietojamos specefektus.</p> <qt>Vai jūs tiešām gribat noņemt ikonu tēmu <strong>%1</strong>?<br /><br />Tiks nodzēsti visi šīs tēmas instalētie faili</qt> <qt>Instalē tēmu <strong>%1</strong></qt> Aktīva Atslēgta Noklusētā Gadījās problēma tēmas instalācijas procesā, bet lielākā daļa no tēmas tika instalēta P&aplidus Visas ikonas Antonio Larrosa Jimenez &Krāsa: Krāsot Apstiprinājums Nepiesātināt Apraksts Darbvirsma Dialogi Ievielciet vai uzraksties tēmas URL viesturs.zarins@mi.lu.lv Efekta parametri Gamma Geert Jansen Ielādēt jaunas tēmas no Interneta Iconas Ikonu vadības paneļa modulis Ja jums ir lokāli saglabāts tēmas arhīvs, šī poga to atpakos un padarīs pieejamu KDE lietotnēm Instalē tēmas arhīva failu, kas jau saglabāts jūsu datorā Galvenā rīkjosla Viesturs Zariņš Nosaukums Bez efektiem Panelis Priekšapskate Izdzēst tēmu Izdzēst izvēlēto tēmu no diska Uzstādīt efektu... Uzstādīt aktīvas ikonas efektu Uzstādīt noklusēto ikonu efektu Uzstādīt aizliegtas ikonas efektu Izmērs: Mazas ikonas Fails nav derīgs tēmas arhīvs. Šis izdzēsīs izvēlēto tēmu no jūsu diska. Uz pelēku Melnbalts Rīkjosla Torsten Rahn Nevar lejupielādēt ikonas tēmas arhīvu;
lūdzu pārbaudiet ka adrese %1 ir pareiza. Nevar atrast ikonas tēmas arhīvu %1. Ikonas pielietojums Lai izmantotu šo darbību, nepieciešamas Interneta pieslēgums. Tiks parādīts loga ar pieejamajām tēmām no http://www.kde.org vietnes.Nospiežot pie tēmas pogu Instalēt, tā instalēta sistēmā. 