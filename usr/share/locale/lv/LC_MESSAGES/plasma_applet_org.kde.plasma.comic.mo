��    /      �  C           ;        U     s     �     �  6   �  A   �     ,     5  $   9  
   ^     i  #   �     �     �     �  7   �          (     D     Q  $   `  ,   �     �     �     �     �     �               +     @     R  �   _  "        >     E  %   W     }     �     �     �  
   �  7   �     	     1	  �  I	  [     '   t     �     �  %   �     �  #   �     #     1  &   6     ]     e  ,   �  "   �     �     �  1   �  %   $  !   J  	   l     v  %   �  /   �     �     �       &        4     A     N     e     y      �  �   �  !   n  
   �     �  %   �     �     �  !   �       	   "     ,     K     O     &   "      (                                  !             )   	          +           .      #       '   -                             /         %                            *   ,   
                  $                                  

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Check for new comic strips: Comic Comic cache: Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. No zip file is existing, aborting. Range: Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2012-07-05 21:11+0300
Last-Translator: Einars Sprugis <einars8@gmail.com>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 

Izvēlieties iepriekšējo lentu, lai pārietu uz pēdējo kešatmiņā saglabāto lentu. &Saglabāt komiksu grāmatas arhīvu... &Saglabāt komiksu kā... &Lentas numurs: *.cbz|Komiksu grāmatas arhīvs (Zip) &Patiesais izmērs Saglabāt šobrīdējo &novietojumu Paplašināti Visi Pēc identifikatora %1 radās kļūda. Izskats Komiksa arhivēšana neizdevās Automātiski atjaunināt komiksa spraudņus: Pārbaudīt jaunas komiksa lentas: Komikss Komiksu kešatmiņa: Neizdevās izveidot arhīvu norādītajā vietā. Izveidot %1 komiksu grāmatas arhīvu Izveido komiksu grāmatas arhīvu Mērķis: Kļūdu apstrāde Neizdevās pievienot failus arhīvam. Neizdevās izveidot failu ar identifikatoru %1. No sākuma līdz... No beigām līdz... Pamata Komiksa lentas saņemšana neizdevās: Iet uz lentu Informācija Iet uz &jaunāko lentu Iet uz &pirmo lentu Iet uz lentu... Pašrocīgi iestatīts diapozons Varbūt nav savienojuma ar internetu.
Varbūt komiksu spraudnis ir bojāts.
Vēl viens iespējamais iemesls — nav komiksa ar šo dienu/numuru/lentu, tādēļ varat mēģināt izvēlēties citu. Nav neviena zip faila, pārtrauc. Diapozons: Lentas identifikators: Komiksa lentu diapozons, ko arhivēt. Atjaunināt Apmeklēt komiksa lapu Doties uz veikala &tīmekļa lapu # %1 dd.MM.ggg &Nākamā cilne ar jaunu lentu No: Līdz: 