��            )   �      �  9   �     �     �     �          -     H  7   _  	   �     �     �     �     �     �     �     	          %     ,     C  #   P  %   t     �  �   �     C     R     `     z  )   �  �  �     �     �     �     �     �     �          !     %     2     >     \     h     z     �     �     �     �     �     �     �     �     �  �   	     �	     �	     �	     �	     
                                                                                                              
                       	    %1 is remaining time, %2 is percentage%1 Remaining (%2%) %1% Battery Remaining %1%. Charging %1%. Plugged in %1%. Plugged in, not Charging &Configure Power Saving... Battery and Brightness Battery is currently not present in the bayNot present Capacity: Charging Configure Power Saving... Discharging Display Brightness Enable Power Management Fully Charged General Keyboard Brightness Model: No Batteries Available Not Charging Placeholder is battery capacity%1% Placeholder is battery percentage%1% Power management is disabled The capacity of this battery is %1%. This means it is broken and needs a replacement. Please contact your hardware vendor for more details. Time To Empty: Time To Full: Used for measurement100% Vendor: battery percentage below battery icon%1% Project-Id-Version: plasma_applet_battery
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-03 03:15+0100
PO-Revision-Date: 2016-10-18 10:15+0200
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian <kde-i18n-doc@kde.org>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 %1 atlicis (%2%) %1% no baterijas atlikuši %1% Uzlādējas %1% Iesprausts %1% Iesprausts, bet nelādē &Konfigurēt energotaupību... Baterija un spilgtums Nav Kapacitāte: Uzlādējas Konfigurēt energotaupību... Izlādējas Ekrāna spilgtums Ieslēgt energokontroli Pilnībā uzlādēta Pamata Tastatūras spilgtums Modelis: Baterijas nav pieejamas Nelādē %1% %1% Energokontrole ir izslēgta Baterijas kapacitāte ir %1%. Tas nozīmē, ka tā ir bojāta un to ir nepieciešams nomainīt. Sazinieties ar savu aparatūras ražotāju, lai saņemtu vairāk informācijas par to. Laiks līdz tukšai: Laiks līdz pilnai: 100% Ražotājs: %1% 