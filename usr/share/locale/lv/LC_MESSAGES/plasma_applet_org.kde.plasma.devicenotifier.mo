��          �            x  $   y  �   �     "  4   .     c  #   }  �   �  �   /  +   �          /     J  �   a  $   �  (     �  4  	   �  	          C     #   ^  ,   �  �   �  �   F  #   	     =	     S	     p	  	   �	     �	  "   �	                                               
           	                     @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2012-01-07 08:29+0200
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 %1 brīvs Montē... Visas ierīces Klikšķiniet, lai piekļūtu šai iekārtai no citām programmām. Klikšķiniet, lai izgrūstu disku. Klikšķiniet, lai droši noņemtu iekārtu. Pašlaik <b>NAV</b> droši noņemt šo iekārtu. Programmas var vēl lietot to. Klišķiniet uz izgrūšanas ikonas, lai droši noņemtu šo iekārtu. Šobrīd šo iekārtu <b>nevar droši noņemt</b> - iespējams, ka programmas vēl lieto citus iekārtas sējumus. Uzklikšķiniet uz "izgrūst" podziņas pie citiem sējumiem, lai varētu noņemt šo iekārtu. Šo iekārtu var droši ņemt nost. Nav pieejamu ierīču Tikai nenoņemamās ierīces Tikai noņemamās ierīces Noņem... Iekārta ir pieejama. Iekārta ir pašlaik nav pieejama. 