��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  O   �  u   �      Z  _   {      �     �  9        J  :   _  	   �     �     �     �  4   �  5     5   B  #   x     �  Z   �            �   3  A   �  6   �     0     E     \     z     �  
   �     �     �  	   �  $   �            5         V     u     �     �     �     �     �     �  "   �          *     7     E  #   U     y     �     �     �  �   �     h  @   z  �   �     M  ;   b  5   �  5   �  &   
  &   1     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-06-11 08:32+0300
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Lai pabeigtu aizmugures maiņu, jums būs jāatsakās un jāpiesakās vēlreiz. Jūsu sistēmā instalēto Phonon aizmuguru saraksts. Kārtība sarakstā nosaka, kādā secībā Phonon tās lietos. Pielietot ierīču sarakstu .... Pielietot redzamo ierīču vēlamības sarakstu šīm citām audio atskaņošanas kategorijām: Audio aparatūras uzstādīšana Audio atskaņošana Audio atskaņošanas ierīces vēlamība kategorijai '%1' Audio ierakstīšana Audio ierakstīšanas ierīces vēlamība kategorijai '%1' Aizmugure Colin Guthrie Savienotājs (c) 2006 Matthias Kretz Noklusētā audio atskaņošanas ierīces vēlamība Noklusētā audio ierakstīšanas ierīces vēlamība Noklusētā video ierakstīšanas ierīces vēlamība Noklusētā/Nenorādīta kategorija Izvairīties no Nosaka noklusēto ierīču kārtību, ko pēc tam var mainīt individuālās kategorijās. Ierīces konfigurācija Ierīču vēlamība Sistēmā atrastās ierīces, kas piemērotas izvēlētajai kategorijai. Izvēlieties ierīci, kuru programmām ieteicams izmantot. maris.kde@gmail.com, viesturs.zarins@mii.lu.lv, einars8@gmail.com Neizdevās iestatīt izvēlēto audio izvades iekārtu Priekšējais centra Priekšējais kreisais Centra priekšējais kreisais Priekšējais labais Centra priekšējais labais Aparatūra Neatkarīgas ierīces Ievades līmeņi Nederīgs KDE audio aparatūras uzstādīšana Matthias Kretz Mono Māris Nartišs, Viesturs Zariņš, Einārs Sprūģis Phonon konfugurācijas modulis Atskaņošana (%1) Dot priekšroku Profils Aizmugurējais centra Aizmugurējais kreisais Aizmugurējais labais Ierakstīšana (%1) Parādīt paplašinātās ierīces Sānu kreisais Sānu labais Skaņas karte Skaņas ierīce Skaļruņu izvietojums un pārbaude Zemfrekvences reproduktors Pārbaudīt Pārbaudīt izvēlēto ierīci Pārbauda %1 Ierīču secība nosaka to vēlamību. Ja kāda iemesla dēļ pirmā ierīce nav pieejama, Phonon mēģinās izmantot nākamo sarakstā norādīto ierīci. Nezināms kanāls Lietot pašreiz rādīto ierīču sarakstu vairāk kategorijām. Dažādas multivides izmantošanas gadījumu kategorijas. Katrai kategorijai varat izvēlēties ieteicamo ierīci, ko izmantot Phonon lietotnēm. Video ierakstīšana Video ierakstīšanas ierīces vēlamība kategorijai '%1'  Jūsu aizmugure var neatbalstīt audio ierakstīšanu Jūsu aizmugure var neatbalstīt video ierakstīšanu izvēlētajai ierīcei nav vēlamības dot priekšroku izvēlētajai ierīcei 