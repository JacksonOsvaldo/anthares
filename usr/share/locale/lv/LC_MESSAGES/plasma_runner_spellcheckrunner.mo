��    	      d      �       �      �      �           "     *  Q   ?     �     �  �  �  &   �     �     �     �  -   �          $     9                            	                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-07-03 08:44+0300
Last-Translator: Viesturs Zariņš <viesturs.zarins@mii.lu.lv>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 &Nepieciešamas aktivizēšanas vārds &Aktivizēšanas vārds: Pārbauda :q: pareizrakstību Pareizi Pareizrakstības pārbaudītāja iestatījumi %1:q: Ieteiktie vārdi: %1 pareizrakstība 