��          <      \       p   !   q   3   �      �   �  �   $   �  +   �                        Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-06-03 20:59+0300
Last-Translator: Viesturs Zariņš <viesturs.zarins@mii.lu.lv>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Atrod Kate sesijas, kas atbilst :q:. Parāda visas jūsu Kate redaktora sesijas. Atvērt Kate sesiju 