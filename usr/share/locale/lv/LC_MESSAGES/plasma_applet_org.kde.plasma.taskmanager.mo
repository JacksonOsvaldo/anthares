��          �   %   �      p     q          �     �     �  
   �     �     �     �     �     �     �  	   	          %     .     ?     M     f     w  (   }  )   �  (   �  '   �  "   !     D     R  �  [     1     D     V     c     p          �  	   �  	   �     �     �     �     �     �                 &   0     W     q  ,   w  ,   �  +   �  &   �     $     ?     R                            
                                 	                                                              &All Desktops &New Desktop Alphabetically Arrangement Behavior By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum columns: Maximum rows: Move &To Current Desktop Move To &Desktop On %1 Only group when the task manager is full Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2016-10-18 10:20+0200
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian <kde-i18n-doc@kde.org>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Vis&as darbvirsmas Jau&na darbvirsma Alfabētiski Izkārtojums Izturēšanās Pēc darbvirsmas Pēc programmas nosaukuma Negrupēt Nekārtot Filtri Pamata Grupēšana un kārtošana Grupēšana: Izcelt logus Pašrocīgi Maksimums kolonu: Maksimums rindu: Pārvietot uz &pašreizējo darbvirsmu Pārvietot uz &darbvirsmu Uz %1 Grupēt tikai tad, kad uzdevumjosla ir pilna Rādīt tikai no pašreizējās aktivitātes Rādīt tikai no pašreizējās darbvirsmas Rādīt tikai no pašreizējā ekrāna Rādīt tikai minimizētos Rādīt paskaidres Kārtošana: 