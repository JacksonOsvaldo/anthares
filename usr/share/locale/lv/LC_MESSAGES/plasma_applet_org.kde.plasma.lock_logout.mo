��          �      �       H  &   I  +   p  	   �     �     �     �     �     �  (   �       ,   &     S     [  �  g  1   )  1   [  	   �  "   �     �  	   �     �     �  +   �  "     ,   0     ]     i                                         	      
                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch user Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2012-01-07 08:39+0200
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Vai vēlaties sastindzināt uz RAM (iemidzināt)? Vai vēlaties sastindzināt uz disku (hibernēt)? Hibernēt Hibernēt (sastindzināt uz disku) Pamest Pamest... Slēgt Slēgt ekrānu Atteikties, izslēgt vai pārstarēt datoru Iemidzināt (sastindzināt uz RAM) Sākt paralēlu sesiju kā citam lietotājam Iemidzināt Pārslēgt lietotāju 