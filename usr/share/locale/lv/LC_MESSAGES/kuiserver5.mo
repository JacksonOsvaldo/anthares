��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �          '     =     [     k     �     �     �  	   �     �     �  G   �           2     S  
   Y     d     q     y     �     �     �     �                                 
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-07 23:23+0300
Last-Translator: Rūdofls Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 1.1
 %1 fails %1 faili %1 failu %1 mape %1 mapes %1 mapju %1 no %2 apstrādāts %1 no %2 apstrādāts ar %3/s %1 apstrādāts %1 apstrādāts ar %2/s Izskats Izturēšanās Atcelt Notīrīt Konfigurēt... Pabeigtie darbi Saraksts ar strādājošām failu pārsūtīšanām/darbiem (kuiserver) Pārvietot tos uz citu sarakstu Pārvietot tos uz citu sarakstu. Pauze Dzēst tos Noņemt tos. Atsākt Rādīt visus darbus sarakstā Rādīt visus darbus sarakstā. Rādīt visus darbus kā koku Rādīt visus darbus kā koku. Rādīt atsevišķos logus Rādīt atsevišķos logus. 