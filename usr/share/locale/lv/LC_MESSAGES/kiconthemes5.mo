��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     l  	   {  ?   �  	   �  
   �     �     �  	   �     �       	             '     .  4   A     v     �                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2011-07-06 12:49+0300
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 &Pārlūkot... &Meklēt: *.png *.xpm *.svg *.svgz|Ikonu faili (*.png *.xpm *.svg *.svgz) Darbības Programmas Kategorijas Ierīces Emblēmas Emocijas Ikonu avots MIME tipi Ci&tas ikonas: Vietas &Sistēmas ikonas: Interaktīvi meklēt ikonu nosaukumos (piem., mape). Izvēlēties ikonu Statuss 