��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     �
     �
  3   �
  1   �
          %     8     L  $   \  l   �     �     �          #  
   *  
   5  
   @     K  	   X     b     o     x  
   �     �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-08 12:56+0300
Last-Translator: Rūdofls Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 <p>Jūs izvēlējāties atvērt citu darbvirsmas sesiju.<br />Pašreizējā sesija tiks paslēpta un tiks parādīts jauns pieteikšanās logs.<br />Katrai no sesijām tiek piešķirts funkcionālais taustiņš; F%1 parasti tiek piešķirts pirmajai sesijai, F%2 - otrajai un tā tālāk. Jūs varat pārslēgties starp sesijām nospiežot vienlaikus Ctrl, Alt un atbilstošās sesijas funkcionālo taustiņu. Papildus tam KDE panelis un darbvirsma piedāvā iespējas pārslēgties starp sesijuām.</p> Parāda visas sesijas Slēgt ekārnu Slēdz aktīvo sesiju un palaiž ekrānsaudzētāju Atsakās, izejot no aktīvās darbvirsmas sesijas Jauna sesija Pārstartē datoru Pārstartēt datoru Izslēgt datoru Sāk jaunu sesiju ar citu lietotāju Pārslēdz uz lietotāja :q: aktīvo sesiju, vai parāda visu aktīvo sesiju sarakstu, ja :q: nav norādīts Izslēdz datoru sesijas Brīdinājums - jauna sesija slēgt atteikties Atteikties atteikties jauna sesija pārsākt pārstartēt izslēgt pārslēgt lietotāju pārslēgt pārslēgt :q: 