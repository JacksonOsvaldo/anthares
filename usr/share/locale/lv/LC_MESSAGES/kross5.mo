��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �                 -   "  	   P     Z     x     �  .   �  5   �  (         1     R     Y     `  '   p  "   �  
   �     �     �     �     	  	   "	     ,	     K	     T	     q	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2011-07-06 12:57+0300
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Launchpad-Export-Date: 2007-11-22 17:44+0000
X-Generator: KBabel 1.11.4
 Pamata Pievienot jaunu skriptu. Pievienot... Atcelt? Komentārs: viesturs.zarins@mi.lu.lv, maris.kde@gmail.com Rediģēt Rediģēt izvēlēto skriptu. Rediģēt... Darbināt izvēlēto skriptu. Neizdevās izveidot skriptu interpretatoram %1 Neizdevās noteikt interpretatoru skripta failam "%1" Neizdevās ielādēt interpretatoru "%1" Nevar atvērt skripta failu "%1" Fails: Ikona: Interpretators: Ruby interpretatora drošības līmenis Viesturs Zariņš, Māris Nartišs Nosaukums: Nav tādas funkcijas "%1" Nav tāda interpretatora "%1" Noņemt Izņemt izvēlēto skriptu. Darbināt Skripta fails "%1" neeksistē. Apturēt Apturēt izvēlēto skriptu. Teksts: 