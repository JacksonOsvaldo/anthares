��          �   %   �      p     q     v     �  ,   �  N   �  
   	        	   5  �   ?     �     �  e   �     Z     j     |  
   �     �     �  &   �  %   �  f     8   i  ;   �     �     �     	        �  :     �     �            Y        y  ,   �  	   �  �   �     F	  $   U	  j   z	     �	     �	     
     (
     8
     @
     N
     m
  b   �
  8   �
  2   %     X  &   i      �      �                                 
                                                                          	                 min Activity Manager After Brightness level, label for the sliderLevel Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2012-01-07 08:18+0200
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
  min Aktivitāšu pārvaldnieks Pēc Līmenis Neizdevās pieslēgties baterijas saskarnei.
Lūdzu, pārbaudiet sistēmas konfigurāciju Nedarīt neko viesturs.zarins@mii.lu.lv, einars8@gmail.com Hibernēt KDE energokontroles sistēmu neizdevās inicializēt. Aizmugure ziņoja šādu kļūdu: %1
Lūdzu, pārbaudiet sistēmas konfigurāciju Slēgt ekrānu Viesturs Zariņš, Einārs Sprūģis Nav pieejams neviens derīgs energokontroles spraudnis. Šo problēmu varētu atrisināt pārinstalācija. Ielādējot profilu Izlādējot profilu Rādīt atteikšanās logu Palaist skriptu Skripts Izslēgt pēc Tika iesprausts strāvas vads. Tika izrauts strāvas vads. Tika izvēlēts profils "%1", bet tāds neeksistē.
Lūdzu, pārbaudiet PowerDevil konfigurāciju. Šīs aktivitātes politika liedz ekrāna energokontroli Šīs aktivitātes politika liedz sistēmai iemigt Izslēgt ekrānu Neatbalstīta apstādināšanas metode Kad aizvērts klēpjdatora vāks Kad nospiesta izslēgšanas poga 