��          4      L       `   $   a   /   �   �  �   %   }  %   �                    Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-06-04 20:04+0300
Last-Translator: Viesturs Zariņš <viesturs.zarins@mii.lu.lv>
Language-Team: Latvian <locale@laka.lv>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Atrod Konsole sesijas, kas satur :q:. Parāda visas jūsu konsoles sesijas. 