��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s  '   p     �     �  %   �  "   �       "   &  D   I  B   �  r   �  G   D  (   �  R   �            *   )  3   T                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-07-07 11:52+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Текущий пользователь Основное Внешний вид Заблокировать экран Начать новый сеанс Не используется Завершить работу... Показывать имя и значок пользователя Показывать полное имя (если указано) Показывать имя пользователя, используемое для входа в систему Показывать только значок пользователя Показывать только имя Показывать техническую информацию о сеансах на %1 (%2) Терминал %1 Вид имени пользователя Вход выполнен от имени <b>%1</b> 