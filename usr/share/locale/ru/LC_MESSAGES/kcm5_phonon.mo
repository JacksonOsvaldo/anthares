��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  <  �  �     �   �  B   �  �   �  >   �  )   �  p   �     a  ^   y  3   �               '  g   ?  U   �  U   �     S     q  Z   z  '   �  %   �  �   #       s   3  -   �  !   �  .   �  #   &  0   J     {  +   �     �     �  B   �     -     <  #   E  ;   i  #   �     �     �  %   �          !     =  B   O     �     �     �  %   �  <   
     G     X  :   i     �  D  �  !     �   '  �   �     m  ^   �  j   �  j   O  V   �  V         $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-01-18 21:11+0400
Last-Translator: Yuri Efremov <yur.arh@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Чтобы изменение библиотеки воспроизведения вступило в силу, необходимо выйти из системы и зайти снова. Список установленных библиотек воспроизведения Phonon. Здесь определяется приоритет их использования. Использовать список устройств для... Использовать приоритет использования устройств для следующих категорий воспроизведения звука: Настройка звукового оборудования Воспроизведение звука Приоритет устройств воспроизведения звука для категории «%1» Запись звука Приоритет устройств записи звука для категории «%1» Библиотеки воспроизведения Colin Guthrie Разъём © Matthias Kretz, 2006 Приоритет устройств воспроизведения звука по умолчанию Приоритет устройств записи звука по умолчанию Приоритет устройств записи видео по умолчанию Общая категория Ниже Определение приоритета использования устройств. Настройка устройства Приоритет устройств Обнаруженные в системе устройства, подходящие для выбранной категории. Выберите устройство, которое будет использоваться приложениями. shaforostoff@kde.ru Ошибка установки выбранного устройства воспроизведения звука Центральный фронтальный Левый фронтальный Фронтальный левее центра Правый фронтальный Фронтальный правее центра Оборудование Независимые устройства Уровни на входе Ошибка Настройка звукового оборудования KDE Matthias Kretz Моно Николай Шафоростов Модуль настройки подсистемы Phonon Воспроизведение (%1) Выше Профиль Центральный тыловой Левый тыловой Правый тыловой Запись (%1) Показать дополнительные устройства Левый боковой Правый боковой Звуковая плата Звуковое устройство Проверка конфигурации динамиков Сабвуфер Проверка Проверить выбранное устройство Тестирование %1 Приоритет определяет порядок использования устройств: если невозможно вывести звук на первое устройство, будет предпринята попытка вывода на следующем устройстве и так далее. Неизвестный канал Использовать текущий список устройств для некоторых других категорий. Различные категории работы с мультимедиа. Для каждой категории вы можете назначить отдельное устройство. Запись видео Приоритет устройств записи видео для категории «%1» Выбранный механизм вероятно не поддерживает запись звука Выбранный механизм вероятно не поддерживает запись видео уменьшить приоритет для выбранного устройства увеличить приоритет для выбранного устройства 