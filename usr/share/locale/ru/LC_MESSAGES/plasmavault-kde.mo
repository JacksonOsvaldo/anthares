��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �    �  �  �  r"  6   K'     �'     �'  D   �'  e   �'     J(  P   [(  7   �(  4   �(     )      ,)  ^   M)  T   �)  9   *     ;*  7   J*     �*  *   �*  (   �*     �*  J   �*     4+  A   :+  a   |+  &   �+  �   ,  &   �,  )   �,     �,  �  �,  J   �.  �   �.  "   �/  5   �/  "   0  
   $0  3   /0     c0  ,   q0  c   �0  
   1  �   1  L   �1  y   �1  {   ^2  {   �2  �   V3  W   �3  f   04  R   �4  5   �4  >    5  +   _5  h   �5  A   �5  .   66  �   e6  �   7     �7     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-01-29 02:51+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Предупреждение об опасности:</b>
                             Согласно аудиту, выполненному Тейлором Хорнби
                             (Taylor Hornby) из компании Defuse Security, нынешняя
                             реализация Encfs уязвима или потенциально уязвима
                             к атакам нескольких типов.
                             Например, атакующий с доступом на чтение и запись
                             к зашифрованным данным может понизить сложность
                             шифрования для шифруемых в дальнейшем данных,
                             и это действие останется незамеченным законным
                             пользователем, либо может использовать временной
                             анализ для извлечения данных.
                             <br /><br />
                             Это означает, что не следует синхронизировать
                             зашифрованные данные с облачным хранилищем
                             либо по-другому работать с ними таким образом,
                             что злоумышленники могут часто иметь доступ
                             зашифрованным данным.
                             <br /><br />
                             Более подробная информация — на странице <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a>. <b>Предупреждение об опасности:</b>
                             Система CryFS шифрует ваши файлы так, что их
                             можно безопасно хранить где угодно.
                             Она хорошо сочетается с облачными хранилищами
                             вроде Dropbox, iCloud, OneDrive и другими.
                             <br /><br />
                             В отличие от некоторого другого программного
                             обеспечения, CryFS также скрывает от посторонних
                             глаз структуру каталогов, количества файлов и их
                             размеры.
                             <br /><br />
                             Но несмотря на то, что CryFS считается безопасной,
                             пока не было проведено независимого аудита,
                             который бы это подтвердил. Создание зашифрованной папки Комнаты Модуль: Не удалось создать точку подключения Зашифрованная папка неизвестна, открыть её невозможно. Изменить Выберите систему шифрования для этой папки: Выберите алгоритм шифрования: Закрыть зашифрованную папку Настройка Настроить папку... Невозможно создать настроенный служебный модуль: %1 Настроенный служебный модуль не существует: %1 Найдена поддерживаемая версия. Создать Создать зашифрованную папку... CryFS Устройство уже открыто Устройство не открыто Диалог Больше не показывать это предупреждение EncFS Расположение зашифрованных данных: Не удалось создать каталоги, проверьте права доступа Не удалось выполнить Не удалось получить список приложений, использующих эту зашифрованную папку. Не удалось открыть: %1 Закрыть принудительно Основное Если ограничить работу с зашифрованной папкой некоторыми комнатами, она будет видна в виджете только в выбранных комнатах. При переключении на комнату, в которой папка не видна, зашифрованная папка будет автоматически закрыта. Ограничить доступ следующими комнатами: Помните, что невозможно восстановить пароль. Если вы забудете пароль, зашифрованные данные будут потеряны. Точка подключения: Не указана точка подключения Точка подключения: Далее Открыть в диспетчере файлов Пароль: Зашифрованные папки Plasma Введите пароль для открытия этой зашифрованной папки: Назад Каталог точки подключения не пуст, невозможно открыть зашифрованную папку Указанный служебный модуль не существует Параметры можно менять, только когда зашифрованная папка закрыта. Невозможно закрыть зашифрованную папку, потому что она неизвестна. Невозможно удалить зашифрованную папку, потому что она неизвестна. Это устройство уже зарегистрировано, невозможно создать его повторно. Этот каталог уже содержит зашифрованные данные Невозможно закрыть папку, она используется приложением Невозможно закрыть папку, она используется %1 Не удалось определить версию Не удалось выполнить это действие Неизвестное устройство Неизвестная ошибка, не удалось создать служебный модуль. Использовать алгоритм по умолчанию &Имя зашифрованной папки: Программа не установлена или имеет неподдерживаемую версию. Требуется версия %1.%2.%3 или выше. Необходимо выбрать пустые каталоги для зашифрованного хранилища и для точки подключения %1: %2 