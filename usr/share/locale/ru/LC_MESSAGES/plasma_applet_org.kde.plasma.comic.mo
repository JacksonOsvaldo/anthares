��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  N  �  �   �  9   c  *   �     �  2   �  $     1   :     l     �  G   �     �  2   �  X        x  P        �     �     �  Q   �  8   O  9   �  !   �  h   �  !   M     o  B   �  U   �     (     B  
   X  -   c  2   �  !   �     �  0   �  .   (  !   W  ,   y  �  �  x   1  =   �     �  U   �  .   P  0     >   �  6   �  (   &  U   O     �  (   �  '   �                 ?   .     n     t  
   z      �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2017-11-16 14:50+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 

Выберите предыдущий стрип, чтобы перейти к последнему кэшированному стрипу. С&оздать архив книги комиксов... &Сохранить комикс как... Номер &стрипа: *.cbz|Архив книги комиксов (Zip) &Фактический размер Запомнить текущую &позицию Дополнительно Все Произошла ошибка для идентификатора %1. Внешний вид Сбой архивирования комикса Автоматическое обновление расширений комиксов: Кэш Проверка на наличие новых стрипов комиксов: Комиксы Кэш: Настроить... Не удалось создать архив по указанному пути. Создать архив книги комиксов %1 Создание архива книги комиксов Сохранить в файле: Показывать уведомление в случае ошибки загрузки комикса Загрузка комиксов Обработка ошибок Ошибка при добавлении файла в архив. Ошибка при создании файла с идентификатором %1. От начала до... С конца до... Общие Получить новые комиксы... Не удалось загрузить стрип: Перейти к стрипу... Сведения Перейти к &текущему стрипу Перейти к &первому стрипу Перейти к стрипу... Выбор диапазона вручную Возможно, пропало соединение с Интернетом.
Или виджет комиксов повреждён.
Ещё одной причиной может быть то, что не существует комикса с указанным значением дня, номера или строки. Попробуйте указать другие значения. Щелчок средней кнопкой мыши показывает комикс в исходном размере Zip-файл не существует, прерывание. Диапазон: Показывать стрелки только при наведении мышью Показывать адрес комикса Показывать автора комикса Показывать идентификатор комикса Показывать заголовок комикса Идентификатор стрипа: Временной диапазон стрипов для архивирования. Обновления Посетить сайт комикса Посетить с&айт студии № %1 дней дд.ММ.гггг С&ледующая вкладка с новым стрипом От: До: минут стрипов на комикс 