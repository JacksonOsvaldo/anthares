��          �            x     y  !   �     �     �     �  @   �     *  $   ?     d     k  "   �     �  %   �     �     �  �         &     F   3     z  0   �  +   �     �  h     ;   l  !   �  #   �  )   �               ;                        
                                    	                   Artist of the songby %1 Artist of the songby %1 (paused) Choose player automatically General No media playing Open player window or bring it to the front if already openOpen Pause playbackPause Pause playback when screen is locked Paused Play next trackNext Track Play previous trackPrevious Track Quit playerQuit Remaining time for song e.g -5:42-%1 Start playbackPlay Stop playbackStop Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2018-02-01 10:44+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 от %1 от %1 (приостановлено) Выбирать проигрыватель автоматически Основное Ничего не воспроизводится Показать проигрыватель Приостановить Приостанавливать воспроизведение при блокировке экрана Воспроизведение приостановлено Следующая дорожка Предыдущая дорожка Закрыть проигрыватель -%1 Воспроизведение Остановить 