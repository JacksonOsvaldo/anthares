��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  ;  �  2     0   P    �  �   �  !   I	     k	     �	     �	     �	     �	  	   �	  $   �	     
  6   ,
  B   c
  >   �
  >   �
  B   $                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-10-28 23:10+0400
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Затемнить экран наполовину Затемнить экран полностью Показать режимы яркости экрана и позволяет установить яркость экрана :q:. Например, яркость экрана 50 затемнит экран до 50% от максимальной яркости экрана Показать варианты приостановки работы (ждущий режим, спящий режим), с возможностью их выполнения затемнение экрана спящий режим яркость экрана ждущий режим приостановка на диск в ОЗУ затемнение экрана %1 яркость экрана %1 Установить яркость экрана в %1 Приостановить с сохранением на диск Приостановить с сохранением в ОЗУ Приостановить с сохранением в ОЗУ Приостановить с сохранением на диск 