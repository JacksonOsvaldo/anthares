��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  F  r  -   �	  .   �	  &   
    =
  �  D  |   �  �   {  "   P  5   s  
   �  9  �  �  �  =   r  #   �  Q   �  ?   &  0   f                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-19 21:48+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Завершить текущий сеанс Пе&резагрузить компьютер Выключи&ть компьютер <h1>Диспетчер сеансов</h1> Здесь осуществляется настройка диспетчера сеансов. Это включает в себя такие параметры, как подтверждение выхода из KDE, восстановление параметров завершающегося сеанса при следующем запуске KDE и автоматическое выключение компьютера после выхода из системы. <ul>
<li><b>Сохранять предыдущий сеанс:</b> Все работающие приложения будут сохранены при выходе из сеанса и восстановлены при открытии нового сеанса.</li>
<li><b>Восстанавливать сохранённый вручную сеанс: </b> Позволяет сохранить сеанс в любое время командой «Сохранить сеанс» в К-меню. Это означает, что запущенные в этот момент приложения будут сохранены и затем восстановлены при открытии нового сеанса.</li>
<li><b>Начинать с пустого сеанса:</b> Не сохранять ничего. При открытии нового сеанса начинать с чистого рабочего стола.</li>
</ul> Приложения, которые &не следует восстанавливать в следующем сеансе: Включите этот параметр, если вы хотите, чтобы при завершении сеанса выводился диалог для подтверждения выхода из KDE. По&дтвердить выход Параметр выхода по умолчанию Общие Здесь можно выбрать, что должно произойти по умолчанию после выхода из системы. Эти параметры действуют только в том случае, если вход в систему производился при помощи KDM. Здесь вы можете ввести разделённый запятыми или двоеточиями список приложений, которые не должны сохраняться в сеансе и поэтому не будут запускаться при восстановлении сеанса. Например «xterm,xconsole» или «xterm:konsole». П&редлагать параметры выключения При входе в систему Восстанавливать сеанс, сох&ранённый вручную Восстанавливать &предыдущий сеанс &Начинать с пустого сеанса 