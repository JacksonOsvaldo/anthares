��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     �  A   �     �     �  !     5   9     o     �     �  J   �  M   �  J   M	  t   �	     
  B   �
  P   �
  L   !  F   n  ;   �  d   �  )   V  f   �  
   �     �               -  6   M     �  '   �                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-17 14:08+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
  мс &Видимость клавиш быстрого доступа: Кнопки с&верху: Всегда скрывать Всегда показывать Продолжительность &анимаций: Анимация Кнопки с&низу: Настройка Breeze Выравнивать заголовки вкладок по центру Перетаскивание за все пустые области окна Перетаскивание только за заголовок окна Перетаскивание за заголовок, строку меню и панели инструментов Использовать тонкую линию для выделения фокуса в меню и в строке меню Рисовать индикатор фокуса в списках Рисовать рамки вокруг перемещаемых панелей Рисовать рамки вокруг заголовков страниц Рисовать рамки вокруг боковых панелей Показывать деления на ползунках Показывать разделители элементов панели инструментов Использовать анимацию Использовать расширенные элементы управления размером Рамки Основное Нет кнопок Одна кнопка Полосы прокрутки Показывать при необходимости Две кнопки П&еретаскивание окон: 