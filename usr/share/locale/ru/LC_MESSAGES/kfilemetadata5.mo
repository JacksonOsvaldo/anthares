��    H      \  a   �            !     5     D     R     f     s     �     �     �     �     �     �     �     �          "     0     B     P     ]     m     }     �     �     �     �     �     �     �               &     3     @     \     s     �      �     �     �     
	     %	     =	  #   \	     �	     �	     �	  "   �	     
  $   !
     F
     c
     
     �
     �
  3   �
       $   (     M  &   f  <   �  !   �  8   �  0   %  !   V  C   x  X   �  P     R   f      �  +   �  �    %     
   )     4  #   K     o  
   �     �     �     �     �     �             7   +  "   c  
   �     �     �     �     �     �     �  #        2     I     Z  )   p  %   �     �  
   �     �     �     �  '     H   /  :   x  +   �  #   �       )      %   J     p  %   �  G   �     �          !  &   9     `  '   u     �     �     �     �     �  (        @     M     c     l  #   �     �  "   �     �       &   +  :   R  6   �  (   �  
   �  '   �        -      0   G   C   4                      +   ,                 "      '   	          <             ;   9   3              *   A   2   %      )   :   =                           @   5                   H   &   B   $             /   
      ?       E       D   6                 F          #   !   1                  8       7       .      (       >       @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDocument Generated By @labelDuration @labelFolder @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music disc numberDisc Number @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label the URL a file was originally downloded fromDownloaded From @label the message ID of an email this file was attached toE-Mail Attachment Message ID @label the sender of an email this file was attached toE-Mail Attachment Sender @label the subject of an email this file was attached toE-Mail Attachment Subject @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2017-12-02 17:08+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Исполнитель альбома Архив Исполнитель Соотношение сторон Аудиофайл Автор Битрейт Число каналов Комментарий Композитор Авторские права Дата создания Документ Программа, создавшая документ Продолжительность Папка Частота кадров Высота Изображение Ключевые слова Язык Автор текста Количество страниц Презентация Издатель Год выпуска Частота дискретизации Электронная таблица Тема Текст Заголовок Видеофайл Ширина Дата и время создания Производитель съёмочного оборудования Модель съёмочного оборудования Ориентация изображения Значение диафрагмы Экспокоррекция Длительность выдержки Диафрагменное число Фотовспышка Фокусное расстояние Фокусное расстояние в 35 мм эквиваленте Высота по GPS Широта по GPS Долгота по GPS Чувствительность (ISO) Экспозамер Исходные дата и время Насыщенность Резкость Баланс белого Размер по оси X Размер по оси Y Дата создания шаблона Альбом Номер диска Жанр Номер дорожки Черновых переводов Количество строк Строк для перевода Переведено строк Количество слов Источник в Интернете Идентификатор письма-источника Отправитель письма-источника Тема письма-источника Автор Последнее обновление 