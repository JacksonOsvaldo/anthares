��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  1   �  R   �  9     =   T     �  <   �  &   �  M   	     V	     u	  #   �	  g   �	  @   
  <   O
  u   �
  &     b   )  1   �  �   �  M   �     �     �  G     m   K                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2018-01-28 21:35+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Не удалось запустить initramfs. Не удалось запустить программу «update-alternatives». Настройка экрана загрузки Plymouth Установка новых экранов загрузки kekcuHa@gmail.com Получить новые экраны загрузки... Ошибка запуска initramfs. Выполнение initramfs завершилось ошибкой: «%1». Установить тему. Marco Martin Александр Яворский Тема не указана в параметрах вспомогательного процесса. Установка тем экрана загрузки Plymouth Выберите экран загрузки системы: Устанавливаемая тема, это должен быть существующий файл архива. Тема %1 не существует. Тема повреждена: файл «.plymouth» не найден внутри архива. Папка темы %1 не существует. Этот модуль позволяет выбрать общий вид рабочей среды, используя заранее продуманные разработчиками параметры. Не удалось войти и выполнить действие: %1, %2 Удалить Удалить тему. Ошибка запуска программы «update-alternatives». Выполнение программы «update-alternatives» завершилось ошибкой: «%1» 