��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  ;       V     ^     e     m     �  #   �     �     �  )   �  -     !   3     U     p  @   �     �     �  &   �       ,   =     j     �  $   �     �      �     �       0        G     d     f  
   x  J   �     �  I   �  :        U  !   m  &   �     �     �     �     �                :     T     l     �     �     �  '   �     �                 .   '  &   V     }     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
   �       "        /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-02-01 11:11+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  мин %1, %2 %1 (%2) Внешний вид шкала Бофорта градусы Цельсия (°C) ° Подробности градусы Фаренгейта (°F) %1 день %1 дня %1 дней %1 день гектопаскали (гПа) Макс.: %1, мин.: %2 Максимальная: %1 дюймы ртутного столба (дюймы рт.ст.) кельвины (K) километры километры в час (км/ч) килопаскали (кПа) морские мили в час (узлы) Местоположение: Минимальная: %1 метры в секунду (м/с) мили мили в час (мили/ч) миллибары (мбар) н/д Не найдены станции для «%1» Предупреждения % Давление: Поиск Выбрать поставщиков информации о погоде - Выводить температуру в компактном виде: Необходимо выполнить настройку Температура: Единицы измерения Интервал обновления: Видимость: Метеостанция Безветренно Скорость ветра: %1 (%2%) Влажность: %1 %2 Видимость: %1 %2 Точка росы: %1 Влажность: %1 понижается повышается не меняется Изменение давления: %1 Давление: %1 %2 %1%2 Видимость: %1 %1 (%2) Получено предупреждений: Получено наблюдений: В ВСВ ВЮВ С СВ ССВ ССЗ СЗ Ю ЮВ ЮЮВ ЮЮЗ ЮЗ переменчивый З ЗСЗ ЗЮЗ %1 %2 %3 Штиль Резкость: %1 Скорость ветра: %1 %2 