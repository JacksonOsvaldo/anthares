��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  ;  <     x	     �	  '   �	  !   �	     �	  
   �	  8   �	     2
  #   :
     ^
  5   m
  #   �
  0   �
     �
       '   !  *   I     t  )   �  $   �  '   �     �  ,     '   H  ,   p     �     �  9   �       D   "  (   g  H   �     �  V   �  5   H     ~  A   �  u   �     B  2   K     ~     �     �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-17 14:01+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (Wayland) ... (Доступные размеры: %1) Выбор изображения Дополнительно Автор &Автоматический вход в систему Фон: Убрать изображение Команды Не удалось распаковать архив Тема курсоров мыши: Настройка темы оформления По умолчанию Описание Загрузка новых тем SDDM victorr2007@yandex.ru,aspotashev@gmail.com Основное Загрузить новые темы... Команда выключения: Установить из файла... Установить тему. Некорректный пакет темы Установить из файла... Экран входа в систему SDDM Максимальный UID: Минимальный UID: Виктор Рыжих,Александр Поташев Название Предварительный просмотр недоступен Команда перезагрузки: Повторный вход после завершения сеанса Удалить тему Модуль настройки диспетчера входа в систему SDDM Утилита для установки тем SDDM. Сеанс: Стандартная тема курсоров мыши в SDDM Устанавливаемая тема, это должен быть существующий файл архива. Тема Не удалось установить тему. Удалить тему. Пользователи Пользователь: 