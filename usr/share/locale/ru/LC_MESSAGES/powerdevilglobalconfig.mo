��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  E  8     ~  '   �     �  /   �  9   �  A   0  7   r  ,   �  A   �       5   8     n       7   �  Y   �  e   )	  m   �	  %   �	     #
    ;
                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-11-15 21:51+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 % &Критический уровень: &Низкий уровень: <b>Уровни заряда батареи</b> Пр&и критическом уровне заряда: Критический уровень заряда батареи Низкий уровень заряда батареи Настроить уведомления... Критический уровень заряда батареи Ничего не делать aspotashev@gmail.com,shaforostoff@kde.ru,skull@kde.ru Включить Спящий режим Низкий уровень заряда батареи Низкий уровень заряда периферийного устройства: Александр Поташев,Николай Шафоростов,Андрей Черепанов Приостанавливать воспроизведение при приостановке работы: Выключить компьютер Ждущий режим Похоже, что служба управления питанием не запущена.
Это можно исправить, запустив службу или запланировав её запуск в настройках «Запуск и завершение».  