��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �  E   �       2   !  :   T  &   �  >   �  C   �     9     U     ]  (   r     �     �     �     �  #   �     	     '  (   :  &   c  *   �  f   �       ;   /     k  (     $   �  &   �  !   �          2  E   P     �     �     �  .   �  !   �          &  %   A     g  #   �     �  8   �  6      :   7     r      �  !   �  /   �       !        <     M  '   h  #   �  %   �     �  /   �       &   =  -   d  $   �  H   �        
     !   )  
   K     V  
   h  $   s  *   �  7   �  M   �  I   I  K   �  <   �  8     :   U     �     �  B   �  h   �     d     |     �  /   �     �  #   �  %     -   5  K   c  T   �              Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-05 03:22+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Обновить или удалить приложение «%1»... Выбрать... Вернуть стандартный значок Добавить значок на рабочий стол Добавить в избранное Добавить виджет запуска на панель Расположить результаты поиска внизу Все приложения %1 (%2) Приложения Программы и документы Поведение Категории Компьютер Контакты Описание (Название) Только описания Документы Изменить приложение... Редактировать меню... Завершить сеанс работы Искать также среди закладок, файлов и электронной почты Избранное Упросить меню до одноуровневого Убрать все Убрать все приложения Убрать все контакты Убрать все документы Убрать приложение Убрать контакт Убрать документ Очистить список последних документов Основное %1 (%2) Спящий режим Скрыть %1|/|Скрыть $[lowercase %1] Скрыть приложение Значок: Заблокировать Заблокировать экран Завершить сеанс Название (Описание) Только названия Часто используемые приложения Часто используемые документы Популярные (часто используемые) Во всех комнатах В текущей комнате Открыть с помощью: Закрепить на панели задач Точки входа Завершение работы Свойства Перезагрузить Последние приложения Последние контакты Последние документы Последние Последние использованные Внешние носители Убрать из избранного Перезагрузить компьютер Выполнить команду... Выполнить команду или поисковый запрос Сохранить сеанс Поиск Результаты поиска Поиск Поиск «%1» Сеанс Свойства контакта... Показывать в избранном Формат наименования программ: Показывать часто используемые приложения Показывать часто используемые контакты Показывать часто используемые документы Показывать последние приложения Показывать последние контакты Показывать последние документы Показывать Выключить Сортировать пункты меню по алфавиту Начать параллельный сеанс от имени другого пользователя Ждущий режим Ждущий режим Спящий режим Переключить пользователя Система Системные действия Выключить компьютер Введите текст для поиска Вернуть скрытые приложения в подменю «%1» Вернуть все скрытые приложения в этом подменю Виджеты 