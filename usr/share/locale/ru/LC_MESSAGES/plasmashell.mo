��    &      L  5   |      P     Q     p  	   ~  0   �  .   �     �  7         ?     `     i  \   �  t   �  9   Z  -   �  $   �     �  [   �     P  E   o  G   �  >   �     <     C     Z     g     �     �     
     )  <   .  :   k  s   �  o   	  .   �	  ,   �	  .   �	  ,   
  A  B
  >   �     �     �     �       9   '  V   a  $   �  V   �  1   4  �   f  4    >   P  -   �  L   �  $   
  �   /  @   �  !     !   4     V     p  -   w     �  �   �  (   �  G   �  C        [  4   d  2   �  8   �  6     2   <  0   o     �     �                 &   %                     !   	      
                       #       "                     $                                                                             Activate Task Manager Entry %1 Activities... Add Panel Bluetooth was disabled, keep shortBluetooth Off Bluetooth was enabled, keep shortBluetooth On Configure Mouse Actions Plugin Do not restart plasma-shell automatically after a crash EMAIL OF TRANSLATORSYour emails Empty %1 Enable QML Javascript debugger Enables test mode and specifies the layout javascript file to set up the testing environment Fatal error message bodyAll shell packages missing.
This is an installation issue, please contact your distribution Fatal error message bodyShell package %1 cannot be found Fatal error message titlePlasma Cannot Start Force loading the given shell plugin Hide Desktop Load plasmashell as a standalone application, needs the shell-plugin option to be specified NAME OF TRANSLATORSYour names OSD informing that some media app is muted, eg. Amarok Muted%1 Muted OSD informing that the microphone is muted, keep shortMicrophone Muted OSD informing that the system is muted, keep shortAudio Muted Plasma Plasma Failed To Start Plasma Shell Plasma is unable to start as it could not correctly use OpenGL 2.
 Please check that your graphic drivers are set up correctly. Show Desktop Stop Current Activity Unable to load script file: %1 file mobile internet was disabled, keep shortMobile Internet Off mobile internet was enabled, keep shortMobile Internet On on screen keyboard was disabled because physical keyboard was plugged in, keep shortOn-Screen Keyboard Deactivated on screen keyboard was enabled because physical keyboard got unplugged, keep shortOn-Screen Keyboard Activated touchpad was disabled, keep shortTouchpad Off touchpad was enabled, keep shortTouchpad On wireless lan was disabled, keep shortWifi Off wireless lan was enabled, keep shortWifi On Project-Id-Version: plasma
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:10+0100
PO-Revision-Date: 2017-01-14 20:51+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Открыть %1-ю кнопку на панели задач Комнаты... Добавить панель Bluetooth выключен Bluetooth включён Настройка модуля действий мыши Не перезапускать Plasma автоматически после сбоя. shafff@ukr.net, darkstar@altlinux.ru Пустая %1|/|Пустая $[replace-pairs-exact %1 'Панель' 'панель'] Включить отладчик QML Javascript. Включить режим тестирования и задать файл сценария на JavaScript, который настроит тестовое окружение. Не найдено ни одного пакета оболочки.
Такая проблема возникает в результате неправильной установки Plasma, обратитесь к разработчикам дистрибутива операционной системы. Не удалось найти пакет оболочки %1. Не удалось запустить Plasma Принудительный запуск указанного модуля. Скрыть рабочий стол Запустить plasmashell как самостоятельное приложение. Требуется подача параметра «--shell-plugin». Николай Шафоростов, Альберт Валиев Звук из %1 выключен Микрофон выключен Звук выключен Plasma Не удалось запустить Plasma Оболочка Plasma Plasma не может продолжить работу из-за невозможности использовать OpenGL 2.
Проверьте правильность установки драйверов видеокарты. Показать рабочий стол Прекратить выполнение текущей комнаты Не удалось загрузить файл сценария %1 файл Мобильный Интернет выключен Мобильный Интернет включён Экранная клавиатура выключена Экранная клавиатура включена Сенсорная панель выключена Сенсорная панель включена Wi-Fi выключен Wi-Fi включён 