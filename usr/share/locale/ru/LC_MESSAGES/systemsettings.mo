��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
       I  #  s   m     �     �  �         �  	   �     �  ,     ;   2     n  %   �  
   �  
   �     �  '   �     �  ,     �   <  N   �          4  "   A  S   d  $   �     �     �     �  l   
  Q   w  %   �     �            0   (  >   Y  ?   �  .   �  l     g   t  �   �     c  z   �  
   �       ?        U     q  !   �  �   �  �   c  u        �     �  �   �     O                                            ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2018-02-01 11:15+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 является сторонней программой и была запущена автоматически © Ben Cooksley, 2009 © Marco Martin, 2017 <i>Содержит %1 элемент</i> <i>Содержит %1 элемента</i> <i>Содержит %1 элементов</i> <i>Содержит %1 элемент</i> Ничего не найдено О «%1» О текущем модуле О текущем представлении О программе «Параметры системы» Все параметры Применить параметры Автор Назад Ben Cooksley Центр настройки от KDE. Настроить... Настройка вашей системы Содержит %1 элемент Содержит %1 элемента Содержит %1 элементов Содержит %1 элемент Следует ли показывать подробные подсказки Разработчик Диалог shafff@ukr.net,semenukha@gmail.com Автоматически разворачивать верхний уровень Часто используемые: Основное Справка Значки Внутреннее представление модуля, внутренняя модель модуля Внутреннее имя используемого представления Комбинация клавиш: %1 Сопровождающий Marco Martin Mathias Soeken Часто используемые модули Часто используемый модуль номер %1 Николай Шафоростов,Стёпа Семенуха Представления не найдены Показывает модули управления в виде значков по категориям. Показывает список модулей управления на боковой панели. Показывает модули управления в виде классической древовидной структуры. Перезапустить %1 Отменить все текущие изменения и вернуться к предыдущим значениям Поиск Поиск... Показывать подсказки о содержимом Боковая панель Боковая панель Параметры системы Программа настройки системы не смогла найти ни одного представления, а значит, нечего отображать. Программа настройки системы не смогла найти ни одного представления, а значит, настраивать нечего. В текущем модуле имеются несохранённые изменения.
Применить их? Дерево Вид меню Добро пожаловать в «Параметры системы», основное место для настройки вашего компьютера. Will Stephenson 