��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  ;  s     �  &   �     �  *        6     >     M  3   _  d   �  )   �  )   "     L  !   O     q  )   x  ;   �  '   �  0   	     7	  '   Q	     y	  0   �	  %   �	  <   �	  0   %
     V
     Y
     x
     �
                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2018-02-01 10:34+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1, автор — %2 Добавить другие обои Добавить папку... Добавить изображение... Фон: Размыть По центру Сменять изображение каждые: Каталог с изображениями для слайд-шоу в качестве обоев Загрузить новые обои... Загрузить новые обои... ч Файлы изображений мин Следующее изображение Открыть содержащую объект папку Открытие изображения Открыть изображение обоев Расположение: Рекомендованные обои Удалить обои Вернуть обои в этот список На весь рабочий стол Масштабирование с кадрированием По центру пропорционально с Выбор цвета фона Сплошной цвет Черепицей 