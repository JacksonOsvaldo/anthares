��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  Q  
     \     p  =     `   �  �     7   �     �     	     &  Q   7  6   �     �     �     �  '     <   )  J   f     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_pastebin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-09-03 20:23+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <a href='%1'>%1</a> Закрыть Копировать ссылку автоматически: Не показывать этот диалог, копировать автоматически Перетащите сюда текст или изображение чтобы опубликовать его на общедоступном сервере. При отправке произошла ошибка Основное Размер журнала: Вставить Подождите, пока отправка не будет завершена. Попробуйте отправить ещё раз. Отправка... Публикация Отправка «%1» Успешно опубликовано Адрес URL только что был отправлен Опубликовать %1 на общедоступном сервере 