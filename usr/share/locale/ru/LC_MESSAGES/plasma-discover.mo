��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  ;  �  "   "     E     H     P     m  b     R   �  p   5  p   �  i     .   �     �  �   �  4   P     �  .   �  E   �  �        �     �     �  $   �     
          2  %   R  !   x  !   �  
   �     �     �  7   �  9      ;   Z  _   �  t   �  9   k  /   �     �  =   �     3  (   F  K   o     �  :   �          -  "   ?  !   b     �     �     �     �     �  8   �  8   "     [  �   o  0     4   >     s     �  �   �  �   k                  #      2      O      k   �   �   
   !     !     )!     7!     F!     Z!  1   m!  %   �!     �!  !   �!     �!  1   "  $   9"     ^"  7   r"     �"     �"     �"  3   �"     #  #   #     @#     Z#     o#  !   }#  (   �#  #   �#  >   �#         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-29 02:59+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Также доступно в %1 %1 %1 (%2) %1 (по умолчанию) <b>%1</b> от %2 <em>%1 из %2 пользователей считают этот отзыв полезным</em> <em>Сообщите нам своё мнение об этом отзыве!</em> <em>Считаете отзыв полезным? <a href='true'><b>Да</b></a>/<a href='false'>Нет</a></em> <em>Считаете отзыв полезным? <a href='true'>Да</a>/<a href='false'><b>Нет</b></a></em> <em>Считаете отзыв полезным? <a href='true'>Да</a>/<a href='false'>Нет</a></em> Идёт загрузка обновлений Получение... Неизвестно, когда в последний раз выполнялась проверка наличия обновлений Проверка наличия обновлений Обновлений нет Нет доступных обновлений Следует проверить наличие обновлений Программное обеспечение на этом компьютере находится в актуальном состоянии Обновления Обновление... Отправить Добавить источник... Дополнения Aleix Pol Gonzalez Центр приложений Применить изменения Доступные модули:
 Доступные режимы:
 Назад Отмена Категория: Проверка наличия обновлений... Комментарий должен быть короче Комментарии должен быть длиннее Компактный режим (автоматически/компактный/полный). Невозможно закрыть приложение, так как не все задачи выполнены. Не удалось найти категорию «%1». Не удалось открыть файл %1. Удалить источник Запуск программы по имени пакета. Отклонить Центр приложений Discover Отображение списка записей с категорией. Расширения... Не удалось удалить источник «%1» Рекомендованные Справка... Домашняя страница: Улучшите описание Установить Установленные Jonathan Thomas Запустить Лицензия: Список всех доступных модулей. Список всех доступных режимов. Загрузка... Локальный (расположенный на этом компьютере) файл пакета, который следует установить. Использовать по умолчанию Дополнительная информация... Дополнительно... Обновлений нет Открыть Discover в указанном режиме. Названия режимов соответствуют названиям кнопок панели инструментов. Открыть с помощью программы, которая способна обрабатывать данные этого типа MIME. Продолжить Оценка: Удалить Ресурсы для «%1» Написать отзыв Отзыв о «%1» Запуск от имени администратора (<em>root</em>) не требуется и не рекомендуется. Поиск Поиск в «%1»... Поиск... Поиск: %1 Поиск: %1 + %2 Настройка Краткий заголовок отзыва... Показать отзывы (%1)... Размер: Ничего не найдено. Источник: Добавление источника для %1 Выполняется поиск... Заголовок: Поддерживает схему URL «appstream:». Задачи Задачи (%1%) %1 %2 Не удалось найти ресурс: «%1» Обновить все Обновить выбранные Обновления (%1) Обновления Версия: Неизвестный автор обновлений не выбраны обновлений выбраны © Команда разработчиков Plasma, 2010-2016 