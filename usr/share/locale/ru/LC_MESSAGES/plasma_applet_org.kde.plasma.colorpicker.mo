��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  U  �  S   E     �  %   �  /   �  0        >  D   O     �  "   �     �  8   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-01-22 04:06+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Автоматически копировать цвет в буфер обмена Очистить журнал Представления цвета Копировать в буфер обмена Формат цвета по умолчанию: Основное Открыть диалоговое окно выбора цвета Выбрать цвет Начать выбор цвета Показать журнал При нажатии комбинации клавиш: 