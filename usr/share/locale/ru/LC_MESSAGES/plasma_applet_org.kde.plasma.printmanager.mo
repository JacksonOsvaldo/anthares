��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  6  �  '   #  ,   K     x  2   �  !   �     �  &   �       c   1  �   �  M   '  *   u  &   �     �     �  �   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-04-28 06:20+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: 
X-Text-Markup: 
 Н&астроить принтеры... Только активные задания Все задания Только выполненные задания Настроить принтер Основное Нет активных заданий Нет заданий Не найдено ни одного настроенного или нового принтера %1 активное задание %1 активных задания %1 активных заданий Одно активное задание %1 задание %1 задания %1 заданий Одно задание Открыть очередь печати Очередь печати пуста Принтеры Поиск принтера... В очереди печати %1 задание В очереди печати %1 задания В очереди печати %1 заданий В очереди печати %1 задание 