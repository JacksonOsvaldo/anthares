��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  R  �  7   �          +  )   8  '   b     �     �     �  ,   �  2   	  /   <  P   l  }   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-17 13:59+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Настройка темы рабочего стола David Rosca skull@kde.ru Загрузить новые темы... Установить из файла... Андрей Черепанов Открытие темы Удалить тему Файлы тем (*.zip *.tar.gz *.tar.bz2) Не удалось установить тему. Тема успешно установлена. При попытке удаления темы произошла ошибка. Этот модуль позволяет настроить тему оформления рабочего стола Plasma. 