��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       	  
   	     	     3	  L   A	  F   �	  >   �	  8   
  :   M
     �
  
   �
     �
     �
     �
  �   �
  G   |  �   �     F  0   ^  !   �  *   �  
   �     �  9     !   =     _     l  	   �     �     �  ;   �  g        l     s     x     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-11-11 14:25+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 100% Автор © Harald Sitter, 2015 Harald Sitter Ни одно приложение не воспроизводит звук. Ни одно приложение не записывает звук. Нет доступных профилей устройств. Нет доступных устройств ввода. Нет доступных устройств вывода. Профиль: PulseAudio Дополнительно Приложения Устройства Добавить виртуальное устройство для параллельного вывода на все звуковые карты Дополнительные параметры вывода звука Автоматически переключать все потоки при появлении нового устройства Захват звука Использовать по умолчанию Профили устройств victorr2007@yandex.ru,aspotashev@gmail.com Входы Выключить звук Виктор Рыжих,Александр Поташев Звуки уведомлений Выходы Воспроизведение Порт:  (недоступен)  (не подключён) Необходим модуль PulseAudio «module-gconf» Этот модуль позволяет настроить звуковую систему PulseAudio. %1: %2 100% %1% 