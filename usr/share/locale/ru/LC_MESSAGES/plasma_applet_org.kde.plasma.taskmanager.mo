��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  N  �  *   D     o          �  #   �  0   �  '        .  +   5  1   a     �  E   �     �  o     o   x     �                .  "   O  8   r  L   �     �          2  2   G     z  .   �     �  1   �          #  3   '  6   [  N   �     �     �       Y     5   o  /   �     �  W   �     ?  C   Z     �  4   �     �  C     N   H     �  -   �  %   �  `   
  G   k  +   �     �  9   �  1      3   R  )   �     �  4   �  �     Q   �  ^   �  O   A  A   �  i   �  >   =     |     �  0   �  +   �            0   6  D   g      �  '   �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-01 11:07+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 На &всех рабочих столах &Закрыть На весь &экран &Переместить &Новый рабочий стол За&крепить на панели задач &Свернуть в заголовок &%1 %2 Есть также в комнатах: %1 Добавить в текущую комнату Во всех комнатах Позволять группировать эту программу По имени Не начинать новый столбец, пока он не заполнится максимально Не начинать новую строку, пока она не заполнится максимально Расположение Поведение По активности По рабочим столам По имени программы Закрывает окно или группу окон Переключение между задачами колесом мыши Не группировать Не сортировать Фильтрация Очистить список документов Основное Группировка и сортировка Группировать: Выделять окна из остальных Размер значков: — Поддержи&вать поверх других Поддерживать на &заднем плане Запретить перемешивать с кнопками запуска Большие Р&аспахнуть Вручную Помечать приложения, которые воспроизводят звук Максимальное число столбцов: Максимальное число строк: &Свернуть Сворачивает/разворачивает окно или группу окон Дополнительно П&ереместить на текущий рабочий стол В &комнатах Переместить &на рабочий стол Выключить звук Открывает новый экземпляр программы %1|/|На рабочем столе $[wo-prefix 'Рабочий стол ' %1] Во всех комнатах Только в текущей комнате Средняя кнопка мыши: Группировать, только когда нет места на панели задач Объединять каждую группу в одну кнопку Показать проигрыватель 9 999+ Приостановить воспроизведение Перейти к следующему файлу Перейти к предыдущему файлу Закрыть проигрыватель &Изменить размер Убрать значок с панели задач Ещё %1 точка входа... Ещё %1 точки входа... Ещё %1 точек входа... Ещё %1 точка входа... Показывать задачи только из текущей комнаты Показывать задачи только с текущего рабочего стола Показывать задачи только с текущего экрана Показывать только свёрнутые задачи Показывать в кнопках состояние и ход выполнения операций Показывать всплывающие подсказки Маленькие Сортировка: Запустить новый экземпляр Начать воспроизведение Остановить Ничего не делает За&крепить на панели задач Группирует или разделяет группу окон Есть в комнатах: %1 Есть во всех комнатах 