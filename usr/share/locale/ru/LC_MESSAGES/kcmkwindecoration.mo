��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  L  p     �     �     �     �       #        ?     L     a     {     �     �     �  �   �  U   �	  %   
  |   ;
  D   �
  *   �
  2   (  5   [     �     �     �  )   �  
   �  &   �          1                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2016-01-06 05:37+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Огромные Широкие Нет рамки Нет рамки сбоку Обычные Невероятно широкие Тонкие Гигантские Очень широкие Меню приложения Грани&цы окна: Кнопки Закрыть В режиме закрытия окна двойным щелчком по кнопке меню, чтобы
открыть меню, нажмите на кнопку меню и подержите, пока
оно не появится. За&крывать окна двойным щелчком по кнопке меню Контекстная справка Перетаскивайте кнопки между этой областью и эскизом заголовка окна Переместите сюда для удаления кнопки Загрузить оформления... Поддерживать поверх других Поддерживать на заднем плане Распахнуть Меню Свернуть На всех рабочих столах Поиск Свернуть в заголовок Оформление Заголовок окна 