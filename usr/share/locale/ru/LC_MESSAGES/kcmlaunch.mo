��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  ;  K     �  .   �    �  �  �
  �   Z     �           ,  B   M  (   �  6   �  .   �  4                                             
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-02-19 14:27+0300
Last-Translator: Andrey Cherepanov <skull@kde.ru>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  с &Время индикации запуска: <H1>Уведомление панели задач</H1>
Вы можете использовать другой метод уведомления о запуске, который
используется панелью задач, изображающей кнопку с вращающимся диском,
символизирующую загрузку запускаемого приложения.
Это может быть полезно поскольку некоторые приложения не осуществляют
уведомление о загрузке. В этом случае, диск прекратит вращаться по истечению
времени, указанного в параметре «Время индикации запуска» <h1>Курсор занятости</h1>
KDE предоставляет курсор занятости для уведомления о
запуске приложения. Чтобы включить курсор занятости,
выберите тип визуального отклика в выпадающем списке.
Некоторые приложения не поддерживают уведомление о
запуске. В этом случае курсор прекращает мигать через
промежуток времени, заданный параметром «Время
индикации запуска». <h1>Отклик запуска</h1> Здесь можно настроить реакцию системы на запуск приложения. Мигающий курсор Прыгающий курсор Курсор &занятости Разрешить &уведомление панели задач Нет курсора занятости Статический курсор занятости &Время индикации запуска: Уведомление &на панели задач 