��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  D  �     7     G     `     p     �  	   �     �     �  K   �       4   $  .   Y  B   �  <   �  !        *  >   F     �  ^   �     �  +        ;  �   [     *  -   H  �  v  #   *     N     _     y     �     �  6   �            y   -  H   �     �      �  "         C     c  *   �  (   �     �  (   �  '     1   B     t  4   �  )   �  /   �  0     %   F  8   l  C   �     �  '      �   (     �  �   �            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-08-31 19:22+0400
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  [Скрыто] &Комментарий: &Удалить &Описание: &Правка &Файл &Имя: &Новое подменю... Запускать от имени &другого пользователя &Сортировка Сортировать всё по о&писанию Сортировать &всё по имени Сортировать выделенное по &описанию Сортировать выделенное по &имени &Имя пользователя: &Рабочая папка: © Waldo Bastian, Raffaele Sandrini, Matthias Elter, 2000-2003 Дополнительно Будут удалены все меню, вложенные в «%1». Продолжить? К&оманда Невозможно записать в %1 Текущая &клавиша: Восстановить изначальное состояние системного меню? Это приведёт к удалению всех меню созданных пользователем. szh@chat.ru, leon@asplinux.ru Включить &отклик запуска В командной строке можно указать несколько следующих параметров, которые будут на действительные значения при запуске программы:
%f - одиночное имя файла
%F - список файлов, используйте с приложениями, которые могут открыть несколько локальных файлов одновременно
%u - одиночный URL
%U - список URL
%d - папка открываемого файла
%D - список папок
%i - значок
%m - мини-значок
%c - заголовок окна Основные параметры Основное Скрытый пункт Имя пункта: Редактор меню KDE Редактор меню KDE Основная панель инструментов Сопровождающий Matthias Elter Изменения в меню не могут быть сохранены из-за следующей проблемы: Пункт меню для предварительного выбора Montel Laurent Переместить &вниз Переместить вв&ерх Zhitomirsky Sergey, Leon Kanter Добавить &пункт... Добавление пункта меню Добавить разд&елитель Новое подменю Показывать только в KDE Первоначальный автор Предыдущий сопровождающий Raffaele Sandrini Восстановить системное меню &Запускать в терминале Сохранить изменения меню? Показывать скрытые пункты Проверка орфографии Параметры проверки орфографии Подменю для предварительного выбора Имя подменю: &Параметры терминала: Невозможно подключиться к khotkeys. Изменения будут сохранены, но не применены. Waldo Bastian Вами были сделаны изменения в меню.
Сохранить изменения или отклонить их? 