��          4      L       `   �   a   L   �   4  E  �  z     8                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-12-13 14:07+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Virtaal 0.5.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Преобразовывает значение :q:, указанное в одной из единиц измерения в формате «значение единица [>, в] единица». Воспользуйтесь виджетом преобразования единиц, чтобы ознакомиться со списком единиц, которые можно использовать в преобразованиях. в 