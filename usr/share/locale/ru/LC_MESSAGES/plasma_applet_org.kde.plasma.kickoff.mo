��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       Q    
   k     v     �  2   �  &   �     �          $  3   9     m  �   �  &   	  [   >	     �	     �	     �	     �	  
   �	     �	     
     $
  -   C
  &   q
  *   �
  6   �
  ,   �
  H   '  0   p     �                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_launcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-01 10:43+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 на %2 %2 на %3 (%1) Выбрать... Вернуть стандартный значок Добавить в избранное Все приложения Внешний вид Приложения Список приложений обновлён. Компьютер Перетаскивайте кнопки, чтобы добавить или убрать вкладки или изменить их порядок. Редактировать меню... Также искать закладки, файлы и электронные письма Избранное Скрытые вкладки Последние Значок: Выход Вкладки меню Популярные Во всех комнатах Только в текущей комнате Убрать из избранного Показывать в избранном Показывать названия программ Сортировать по алфавиту Переключать вкладки при наведении мыши Введите текст для поиска... Видимые вкладки 