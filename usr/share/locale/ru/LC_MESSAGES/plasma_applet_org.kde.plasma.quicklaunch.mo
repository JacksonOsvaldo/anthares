��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  ;  �  /      �   0     �  4   �  /   �  <   .  !   k     �     �  5   �  /   �       *   :  ,   e  A   �  '   �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-03-17 10:41+0300
Last-Translator: Alexander Potashev <aspotashev@gmail.com>
Language-Team: Russian <kde-russian@lists.kde.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Добавить кнопку запуска... Добавляйте кнопки запуска перетаскиванием или через контекстное меню. Внешний вид Расположение кнопок запуска Изменить кнопку запуска... Использовать всплывающую панель Введите заголовок Основное Скрыть значки Максимальное число столбцов: Максимальное число строк: Панель запуска Удалить кнопку запуска Показать скрытые значки Показывать названия кнопок запуска Показывать заголовок Заголовок 