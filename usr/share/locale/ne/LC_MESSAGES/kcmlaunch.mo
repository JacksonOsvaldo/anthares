��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  B   �  w  B    �
  �   �  (   �  %   �  "     ]   3  8   �  >   �  B   	  .   L                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-08-19 16:36+0545
Last-Translator: Nabin Gautam <nabin@mpp.org.np>
Language-Team: Nepali <info@mpp.org.np>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n !=1

X-Generator: KBabel 1.11.4
  सेकेन्ड सुरुआत सूचना समयसमाप्ति: <H1>कार्यपट्टी सूचना</H1>
तपाईँले कार्यपट्टीद्वारा प्रयोग हुने सुरुआत सूचनाको दोस्रो विधि
सक्षम पार्न सक्नुहुन्छ जहाँ तपाईँले सुरुआत गरेको अनुप्रोग लोड भइराखेको
सङ्केत दिने, घुम्ने आउर ग्लाससँग बटन देखा पर्छ ।
यो सुरुआत सूचनाबाट, केही अनुप्रयोग सचेत
नहुने हुन सक्छ । यस अवस्थामा, बटन 'सुरुआत सूचना समयसमाप्ति' खण्डमा
केही समयपछि देखापर्छ <h1>व्यस्त कर्सर</h1>
केडीई ले अनुप्रयोगको सुरुआतमा व्यस्त कर्सर प्रदर्शन गर्छ ।
व्यस्त कर्सर सक्षम पार्न, कम्बो बाकसबाट
एक प्रकारको दृश्य पृष्ठपोषण चयन गर्नुहोस् ।
यो सुरुआत सूचनाबाट
केही अनुप्रयोग, सचेत नहुने पनि हुन
सक्छ । यो अवस्थामा, 'सुरुआत सूचना समयसमाप्ति' मा दिएको 
केही समय पछि कर्सर झिम्कन छाड्छ <h1>पृष्ठपोषण सुरुआत</h1> तपाईँले यहाँ अनुप्रयोग सुरुआत पृष्ठपोषण कन्फिगर गर्न सक्नुहुन्छ । झिम्किने कर्सर उफ्रिने कर्सर व्यस्त कर्सर कार्यपट्टी सूचना सक्षम पार्नुहोस् व्यस्तता विहिन कर्सर निस्क्रिय व्यस्त कर्सर सुरुआत सूचना समयसमाप्ति: कार्यपट्टी सूचना 