��    	      d      �       �   m   �      O     W     u     �     �  %   �     �  �  �  3  �     �  ]   �  1   M  "     D   �  x   �  Q   `                             	                 A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Backend Copyright 2006 Matthias Kretz Device Preference Matthias Kretz Phonon Configuration Module no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-08-24 14:24+0545
Last-Translator: Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>
Language-Team: Nepali <info@mpp.org.np>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n !=1
X-Generator: KBabel 1.11.4
 फोनोन ब्याकइन्डको सूची तपाईँको प्रणालीमा फेला पर्यो । यहाँको क्रमले फोनोनले प्रयोग गर्ने क्रमलाई निर्धारण गर्दछ । ब्याकइन्ड प्रतिलिपिअधिकार २००६ माथियस क्रेज यन्त्र प्राथमिकता माथियस क्रेज फोनोन कन्फिगरेसन मोड्युल चयन गरिएका यन्त्रका लागि कुनै प्राथमिकता छैन चयन गरिएका यन्त्र रूचाउनुहोस् 