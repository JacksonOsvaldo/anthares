��          D      l       �      �   
   	       *      �  K  �  �     �     �  �   �                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2007-11-07 14:18+0545
Last-Translator: Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>
Language-Team: Nepali <info@mpp.org.np>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n !=1
X-Generator: KBabel 1.11.4
 <h1>बहुँविध डेस्कटप</h1>यो मोड्युलमा, तपाईँले कति वास्तविक डेस्कटपहरू कन्फिगर गर्न चाहनुभएको छ र यो कसरी लेबुल गरिनेछ भन्ने कुरा कन्फिगर गर्न सकिन्छ । डेस्कटप %1 डेस्कटप %1: यहाँ तपाईँ डेस्कटप %1 का लागि नाम प्रविष्ट गर्न सक्नुहुन्छ 