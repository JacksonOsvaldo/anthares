��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
     �
  	   �
     �
               !     )     ?     V     c  .   v     �     �     �     �     �     �                  !        :     G  ,   P     }     �     �     �     �  0   �     �  
                        -     5     H     \     d     x     �  
   �     �  "   �               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-28 14:05+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Tilbage Batteri på %1% Skift layout Virtuelt tastatur Annullér Lås skift er aktiveret Luk Luk søgning Indstil Indstil søge-plugins Skrivebordssession: %1 Anden bruger Tastaturlayout: %1 Logger ud om 1 sekund Logger ud om %1 sekunder Log ind Login mislykkedes Log ind som anden bruger Log ud Næste spor Ingen medier spiller Ubrugt O.k. Adgangskode Afspil medie eller sæt på pause Forrige spor Genstart Genstart om 1 sekund Genstart om %1 sekunder Nylige forespørgsler Fjern Genstart Vis mediekontroller: Luk ned Lukker ned om 1 sekund Lukker ned om %1 sekunder Start ny session Suspendér Skift Skift session Skift bruger Søg... Søg efter "%1"... Plasma skabt af KDE Lås op Kunne ikke låse op på TTY %1 (skærm %2) TTY %1 Brugernavn %1 (%2) i kategorien nylige forespørgsler 