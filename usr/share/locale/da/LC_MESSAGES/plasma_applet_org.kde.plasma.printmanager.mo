��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �     �     �  	     3        C     `     n     }     �     �  =   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-05-26 22:21+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 &Konfigurér printere... Kun aktive job Alle job Kun gennemførte job Konfigurér printer Generelt Ingen aktive jobs Ingen job Ingen printere er blevet konfigureret eller opdaget Et aktivt job %1 aktive jobs Et job %1 job Åben printkø Udskriftskøen er tom Printere Søg efter en printer... Der er et udskriftsjob i køen Der er %1 udskriftsjob i køen 