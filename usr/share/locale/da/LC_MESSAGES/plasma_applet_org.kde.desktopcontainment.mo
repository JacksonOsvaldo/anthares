��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l          #     0     A     G     P     \     s     �     �     �     �     �  	   �     �     �     �     �  	   �     �     �               "     +     4     @     O     a     �     �     �  	   �     �  $   �     �     �     �  	             #     )     8     ?     D     L  	   R  
   \     g     w     }  +   �     �     �     �  
   �  E   �          .     N     T     e     m     t     {     �     �     �     �     �     �     �  .        1     D  
   c     n     t     �     �  
   �     �     �     �     �     �     �     �     �  	              1     H  P   Y     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: desktop files
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-01-28 14:06+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Slet &Tøm affald &Flyt til affald &Åbn &Indsæt &Egenskaber &Genopfrisk skrivebord &Genopfrisk visning &Genindlæs &Omdøb Vælg... Ryd ikon Justér Udseende: Opstil i Opstil i Opstilling: Tilbage Annullér Kolonner Indstil skrivebord Brugervalgt titel Dato Standard Faldende Beskrivelse Afmarkér alle Skrivebordslayout Angiv en brugervalgt titel her Funktioner: Filnavnsmønster: Filtype Filtyper: Filter Pop-op til forhåndsvisning af mappe Mapper først Mapper først Fuld sti Forstået Skjul filer som matcher Enorm Ikonstørrelse Ikoner Stor Venstre Liste Placering Placering: Lås til stedet Låst Medium Flere indstillinger til forhåndsvisning... Navn Ingen O.k. Panelknap: Tryk og hold på widgets for at flytte dem og få vist deres håndtag Plugins til forhåndsvisning Forhåndsvisning med miniaturer Fjern Ændr størrelse Genskab Højre Rotér Rækker Søg efter filtype... Markér alle Markér mappe Markeringsfremhævning Vis alle filer Vis filer som matcher Vis en placering: Vis filer relateret til den aktuelle aktivitet Vis Desktop-mappen Vis skrivebordsværktøjskasse Størrelse Lille Under medium Sortér efter Sortér efter Sortering: Angiv en mappe: Tekstlinjer Meget lille Titel: Værktøjstips Tilpasninger Type Angiv en sti eller en URL her Usorteret Brugervalgt ikon Håndtering af widgets Widgets låst op Du kan trykke og holde på widgets for at flytte dem og få vist deres håndtag. Visningstilstand 