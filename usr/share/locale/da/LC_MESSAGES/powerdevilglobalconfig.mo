��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     �     �     �  +   �     +  J   @  >   �     �     �  
   �       	        )     /  #   B     f  0   w     �  
   �     �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-10-28 20:08+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 % &Kritisk niveau: &Lavt niveau: <b>Batteriniveauer                     </b> &På kritisk niveau: Batteriet vil blive anset for at være kritisk, når det når dette niveau Batteriet vil blive anset for lavt, når det når dette niveau Indstil bekendtgørelser... Kritisk batteriniveau Gør intet mschlander@opensuse.org Aktiveret Dvale Lavt batteriniveau Lavt niveau for tilkoblede enheder: Martin Schlander Sæt medieafspillere på pause ved suspendering: Luk ned Suspendér Strømstyringstjenesten lader til ikke at køre.
Dette kan løses ved at starte eller skemalægge den i "Opstart og nedlukning" 