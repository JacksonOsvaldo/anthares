��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     2     >     O     f     y     }     �     �     �  	   �     �     �     �     �  	   �  
      	                       #     5     H     M     b     g  	   o     y     �     �  	   �     �  /   �  B   �     .     6     >     K     S     `     p  
   u     �     �     �     �     �     �     �  
   �     �     �     �            f     �   �     $     ,     C     P     d  	   s     }     �     �     �     �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-10-28 19:47+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aktiviteter Tilføj handling Tilføj afstandsstykke Tilføj widgets... Alt Alternative widgets Altid synlig Anvend Anvend indstillinger Anvend nu Ophavsmand: Skjul automatisk Tilbage-knap Nederst Annullér Kategorier Centreret Luk + Indstil Indstil aktivitet Opret aktivitet... Ctrl Bruges i øjeblikket Slet E-mail: Frem-knap Hent nye widgets Højde Vandret rulning Input her Tastaturgenveje Layoutet kan ikke ændres mens widgets er låst Layoutændringer skal anvendes før andre ændringer kan foretages Layout: Venstre Venstre knap Licens: Lås widgets Maksimér panel Meta Midterknap Flere indstillinger... Musehandlinger O.k. Paneljustering Fjern panel Højre Højre knap Skærmkant Søg... Skift Stop aktivitet Stoppede aktiviteter: Skift Indstillingerne i det aktuelle modul er blevet ændret. Vil du anvende ændringerne eller kassere dem? Denne genvej vil aktivere appletten: Den vil give tastaturfokus til den, og hvis appletten har en pop-op (som f.eks. startmenuen), vil den pop-op blive åbnet. Øverst Fortryd afinstallation Afinstallér Afinstallér widget Lodret rulning Synlighed Baggrundsbillede Baggrundsbilledetype: Widgets Bredde Vinduer kan dække Vinduer går under 