��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �     �     �  !   �          3     ;     C  #   Y  
   }     �     �     �     �     �     �     �     �                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-08-26 20:15+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Skift stregkodetype Ryd historik Udklipsholderindhold Udklipsholderens historik er tom. Udklipsholderen er tom Code 39 Code 93 Indstil udklipsholder Oprettelse af stregkode mislykkedes Datamatrix Redigér indhold +%1 Fremkald handling QR-kode Fjern fra historik Tilbage til udklipsholder Søg Vis stregkode 