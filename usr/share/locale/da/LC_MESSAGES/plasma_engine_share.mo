��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  %   �  ,     6   5  &   l  $   �  +   �     �       (            	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-08 10:23+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Kunne ikke detektere filens MIME-type Kunne ikke finde alle påkrævede funktioner Kunne ikke finde udbyderen med den angivne destination Fejl under forsøg på at køre script Ugyldig sti til den ønskede udbyder Det var ikke muligt at læse den valgte fil Tjenesten var ikke tilgængelig Ukendt fejl Du skal angive en URL for denne tjeneste 