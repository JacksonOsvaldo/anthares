��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �     �     �     �     �       
             .  x   7     �  /   �  
   �                    #     *     6     C     X     s     �     �     �     �     �     �                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-10-28 20:09+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Vælg... Ryd ikon Føj til favoritter Alle programmer Udseende Programmer Programmer opdateret. Computer Træk faneblade mellem boksene for at vise/skjule dem, eller ændr rækkefølge på de synlige faneblade ved at trække. Redigér programmer... Udvid søgning til bogmærker, filer og e-mails Favoritter Skjulte faneblade Historik Ikon: Forlad Menuknapper Ofte anvendt På alle aktiviteter På den aktuelle aktivitet Fjern fra favoritter Vis i favoritter Vis programmer efter navn Sortér alfabetisk Skift faneblad under mus Skriv for at søge... Synlige faneblade 