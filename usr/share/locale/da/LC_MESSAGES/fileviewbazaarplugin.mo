��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  '   �	      �	  *   �	     �	  &   
  "   2
     U
      t
     �
     �
  $   �
     �
       (   +  !   T  +   v  #   �     �     �     �       !   (     J     i     �     �     �  
   �     �     �     �     �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2012-03-26 18:41+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Filerne blev føjet til Bazaar-depotet. Føjer filer til Bazaar-depot... Kunne ikke føje filer til Bazaar-depotet. Bazaar-log lukket. Kunne ikke indsende Bazaar-ændringer. Bazaar-ændringerne blev indsendt. Indsender Bazaar-ændringer... Kunne ikke trække Bazaar-depot. Bazaar-depotet blev trukket. Trækker Bazaar-depot... Kunne ikke skubbe Bazaar-depotet ud. Bazaar-depotet blev skubbet ud. Skubber Bazaar-depot ud... Filerne blev fjernet fra Bazaar-depotet. Fjerner filer fra Bazaar-depot... Kunne ikke fjerne filer fra Bazaar-depotet. Eftersyn af ændringer mislykkedes. Eftersete ændringer. Eftersyn af ændringer... Kunne ikke køre Bazaar-log. Kører Bazaar-log... Kunne ikke opdatere Bazaar-depot. Bazaar-depotet blev opdateret. Opdaterer Bazaar-depot... Bazaar add... Bazaar commit... Bazaar delete Bazaar-log Bazaar pull Bazaar push Bazaar update Vis lokale Bazaar-ændringer 