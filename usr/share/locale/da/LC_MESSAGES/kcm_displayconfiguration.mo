��    5      �  G   l      �     �     �     �     �     �     �  !   �  '        >     M  -   _     �     �     �     �     �     �     �     �           
     +  	   3     =     N     [  
   i     t     �     �  A   �                     %     2     @     L  <   Z  <   �     �  3   �       �        �     �  ;   �     	     	     *	  %   6	  b   \	  �  �	     c     i     �     �     �     �     �      �     �       '   %     M     R     i     n          �     �     �     �     �  	   �  
   �               '     7     E     V  &   l  ?   �     �     �     �  	   �                !     0     3     6  5   ?     u  �   �  
   ?  
   J  J   U     �     �     �  %   �  i   �             ,      &      3           0   #                 *      /      5       -                                       '   (   !   )   2   4             "          %                         .   +                    1                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale multiplier, show everything at 1 times normal scale1x Scale multiplier, show everything at 2 times normal scale2x Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-10-28 19:56+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 Hz &Deaktivér alle output &Omkonfigurér (c), 2012-2013 Dan Vrátil 90° med uret 90° mod uret Deaktivér alle output Ikke-understøttet konfiguration Aktiv profil Avancerede indstillinger Vil du virkelig deaktivere alle output? Auto Adskil forenede output Knap Afkrydsningsfelt Kombinationsfelt Konfiguration af skærme Daniel Vrátil Konfiguration af skærme Skærm: mschlander@opensuse.org Aktiveret Gruppefelt Identificér output KCM test-app Bærbars skærm Vedligeholder Martin Schlander Intet primært output Ingen tilgængelige skærmopløsninger Ingen KScreen-backend fundet. Tjek din installation af kscreen. Normal Orientering: Primær skærm Radioknap Genopfriskningsrate: Opløsning: Skalér skærm 1x 2x Skalér: Ændring af skalering vil få virkning efter genstart Skærmskalering Beklager, din konfiguration kunne ikke anvendes.

Det skyldes normalt at den samlede skærmstørrelse er for stor, eller at du aktiverede flere skærme end dit grafikkort understøtter. Faneblad 1 Faneblad 2 Tip: Hold Ctrl nede mens du trækker en skærm for at deaktivere hægtning Forenede output Foren output På hovedet Advarsel: Der er ingen aktive output! Dit system understøtter kun op til %1 aktiv skærm Dit system understøtter kun op til %1 aktive skærme 