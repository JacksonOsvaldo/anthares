��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  +   �     �  *   �               "                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-08-26 20:23+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Klokken er %1 Viser den aktuelle dato Viser den aktuelle dato i en given tidszone Viser den aktuelle tid Viser den aktuelle tid i en given tidszone dato tid Dags dato er %1 