��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     �  G     �     �     �     �               "     +     :     H     X     d     m     �  	   �     �     �     �  
   �     �     �     �     �     �       #        8     F     H     N     S     U     f     r     z     �     �     �     �     �     �     �     �     �           	               *     6     ;     J     \     u     x     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-06-18 14:56+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  min %1 %2 %1 (%2) Beaufort-skalaen bft Celsius °C ° Detaljer Fahrenheit °F 1 dag %1 dage Hektopascal hPa H: %1 L: %2 Høj: %1 Tommer kviksølv inHg Kelvin K Kilometer Kilometer i timen km/t Kilopascal kPa Knob kt Placering: Lav: %1 Sekundmeter m/s Miles Miles i timen mph Millibar mbar Utilg. Ingen vejrstationer fundet for "%1" Bemærkninger % Tryk: Søg - Indstil venligst Temperatur: Enheder Opdatér hvert: Sigtbarhed: Vejrstation Roligt Vindhastighed: %1 (%2%) Luftfugtighed: %1%2 Sigtbarhed: %1 %2 Dugpunkt: %1 Humidex: %1 faldende stigende stabilt Tryktendens: %1 Tryk: %1 %2 %1%2 Sigtbarhed: %1 Udstedte varsler: Udstedte overvågninger: Ø ØNØ ØSØ N NØ NNØ NNV NV S SØ SSØ SSV SV VR V VNV VSV %1 %2 %3 Roligt Kuldeindeks: %1 Vindstød: %1 %2 