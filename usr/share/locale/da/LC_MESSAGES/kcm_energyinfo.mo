��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  	   �	     �	     �	     �	  
   �	     	
     
     
     "
     :
     A
     O
  	   l
     v
     �
     �
     �
     �
     �
     �
     �
     �
            	   #     -     3     D  
   H     S     [     c  
   q     |     �     �  
   �  G   �     �  #   �  	          	      !   *     L     N     Q     T     \     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-18 14:40+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Programmers strømforbrug Batteri Kapacitet Opladningsprocent Opladningstilstand Lader op Nuværende °C Detaljer: %1 Aflader mschlander@opensuse.org Energi Energiforbrug Statistik over energiforbrug Omgivelse Fuldt design Fuldt opladet Har strømforsyning Kai Uwe Broulik Seneste 12 timer Seneste to timer Seneste 24 timer Seneste 48 timer Seneste 7 dage Seneste fulde Seneste time Producent Model Martin Schlander Nej Lader ikke PID: %1 Sti: %1 Genopladeligt Genopfrisk Serienummer W System Temperatur Denne type historik er i øjeblikket ikke tilgængelig for denne enhed. Tidsforløb Tidsforløb for data der skal vises Producent V Spænding Opvågninger pr. sekund: %1 (%2%) W Wh Ja Forbrug % 