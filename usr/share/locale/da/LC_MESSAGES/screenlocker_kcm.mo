��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �     g  
   �     �  "   �     �     �     �  2   �  "        ?     K  1   W     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2016-09-24 21:54+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Lås skærm ved genoptag: Aktivering Fejl Kunne ikke teste skærmlåsningen. Straks Lås session Lås automatisk skærmen efter: Lås skærmen når der vågnes op fra suspendering &Kræv adgangskode efter låsning:  min.  min.  sek.  sek. Den globale tastaturgenvej til at låse skærmen. Baggrundsbillede&type: 