��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  w     2
     J
  5   X
  3   �
  
   �
     �
     �
     �
  )   
  q   4     �  	   �     �     �     �     �     �  
   �     �                    "  	   (                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-05-17 23:32+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 <p>Du har valgt at åbne endnu en desktop-session.<br />Den nuværende session vil blive blive skjult, og en ny login-skærm vil blive vist.<br />En F-tast tildeles til hver session: F%1 tildeles normalt til den første session, F%2 til den anden og så fremdeles. Du kan skifte mellem sessioner ved at trykke Ctrl+Alt og den passende F-tast samtidigt. Desuden har KDE's panel- og desktop-menuer handlinger til at skifte mellem sessioner.</p> Oplister alle sessioner Lås skærmen Låser de aktuelle sessioner og starter pauseskærmen Logger ud, forlader den aktuelle skrivebordssession Ny session Genstarter computeren Genstart computeren Luk computeren ned Starter en ny session som en anden bruger Skifter til den aktive session for brugeren :q:, eller viser en liste over aktive sessioner hvis :q: ikke angives Slukker computeren sessioner Advarsel - ny session lås log ud Log ud log ud ny session genstart genstart sluk skift bruger skift skift :q: 