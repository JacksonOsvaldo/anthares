��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �   �  j   s     �     �     �       
                  '     9     M     c     w     �     �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: krunner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-11-12 15:25+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Dæmp skærmen halvt Dæmp skærmen fuldstændigt Viser en liste over lysstyrkeindstillinger og sætter det til lysstyrken defineret af :q:, f.eks. vil skærm-lysstyrke 50 dæmpe skærmen til 50% af maksimal lysstyrke Viser en liste over systemets suspenderingsmuligheder (f.eks. slumre, dvale) og lader dem blive aktiverede dæmp skærmen dvale skærm-lysstyrke slumre suspendér til disk til ram dæmp skærmen %1 skærm-lysstyrke %1 Sæt lysstyrke til %1 Suspendér til disk Suspendér til ram Suspenderer systemet til ram Suspenderer systemet til disk 