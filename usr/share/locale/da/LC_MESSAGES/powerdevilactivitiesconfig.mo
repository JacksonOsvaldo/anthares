��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �     �      �          0     6     G  '   _      �     �     �     �  
   �     �  �   r  x   	     �	  8   �	     �	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-06-18 14:40+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
  min Optræd som Altid Bestem en særlig opførsel Brug ikke særlige indstillinger mschlander@opensuse.org Dvale Martin Schlander Luk aldrig skærmen ned Suspendér eller sluk aldrig computeren Pc'en kører på strømforsyning Pc'en kører på batteri Pc'en kører på lavt batteri Luk ned Suspendér Strømstyringstjenesten lader til ikke at køre.
Dette kan løses ved at starte eller skemalægge den i "Opstart og nedlukning" Aktivitetstjenesten kører ikke.
Det er påkrævet at aktivitetshåndteringen kører for at kunne indstille aktivititetsspecifik opførsel for strømstyringen. Aktivitetstjenesten kører med basisfunktionalitet.
Navne og ikoner for aktiviteter vil måske ikke være tilgængelige. Aktivitet "%1" Brug separate indstillinger (kun for avancerede brugere) efter 