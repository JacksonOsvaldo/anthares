��          �   %   �      p     q     ~     �     �     �     �     �     �  )   �               !     4     I     ]     m  3   u     �     �  <   �  5     8   T  8   �     �     �     �     �  �  �     �     �     �     �     �     �     �       6   
     A     O     U     n     �     �     �  +   �     �     �  F     7   ^  4   �  4   �  	    	     
	     	     1	                        
                                                	                                                   Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Pad Paths Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2016-06-18 14:43+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Tilføj filer... Tilføj mappe... Baggrund Skift billede hvert Vælg en mappe Vælg filer Indstil plasmoid... Generelt Venstreklik på billede åbner det i ekstern fremviser Margenafstand Stier Pause når musen er over Bevar aspektbeskæring Bevar aspektforhold Randomisér elementer Stræk Billedet mangfoldiggøres vandret og lodret Billedet ændres ikke Billedet skaleres for at passe Billedet skaleres ensartet for at passe, med beskæring om nødvendigt Billedet skaleres ensartet for at passe uden beskæring Billedet strækkes vandret, og fliseudlægges lodret Billedet strækkes lodret, og fliseudlægges vandret Fliselæg Fliseudlæg vandret Fliseudlæg lodret s 