��    $      <  5   \      0      1     R     ^  -   }  #   �  #   �  ?   �  1   3     e     y  H   ~  L   �       V     N   s     �  
   �     �     �     �       2     
   P     [  )   {  8   �  #   �  .     -   1  D   _  6   �  0   �  A     =   N  9   �  �  �  &   j
     �
     �
  9   �
     �
                     /     D     L     g          �     �     �     �     �     �      �     �            "     &   A     h  %   x  3   �     �     �     	                    ,                                                
       "                                       $                                     #      	           !                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-10-28 20:00+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 bekendtgørelse %1 bekendtgørelser %1 af %2 %3 %1 kørende job %1 kørende job &Indstil bekendtgørelser og handlinger for hændelser... 10 sek. siden 30 sek. siden Vis detaljer Skjul detaljer Ryd bekendtgørelser Kopiér 1 mappe %2 ud af %1 mapper 1 fil %2 ud af %1 filer Historik %1 af %2 +%1 Information Job mislykkedes Job færdigt Ingen nye bekendtgørelser. Ingen bekendtgørelser eller job Åbn... %1 (på pause) Markér alt Vis historik over bekendtgørelser Vis program- og systembekendtgørelser %1 (%2 tilbage) Overvåg filoverførsler og andre job Brug selvvalgt position til bekendtgørelses-pop-op %1 min. siden %1 min. siden %1 dag siden %1 dage siden I går Lige nu %1: %1: Mislykkedes %1: Færdig 