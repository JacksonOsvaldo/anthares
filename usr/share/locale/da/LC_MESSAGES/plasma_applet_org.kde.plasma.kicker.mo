��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     p     �     �     �     �     �     �     �       
          	   2  
   <     G  	   P     Z     m  
   }     �     �     �  /   �  
   �  )   �     '     0     E     Y     n     {     �     �     �     �     �     �     �     �     �     �     �     �               0     I     V     k  	   �     �     �     �  
   �     �     �     �     �     
          (     @     U     i  )   z     �     �     �     �     �     �     �               &     C     _     |     �     �     �     �     �  -   �  
             )     =     J     Q     b     r  $   �  /   �     �         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-28 20:00+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Håndtér "%1"... Vælg... Ryd ikon Føj til skrivebordet Føj til favoritter Føj til panel (widget) Justér søgeresultater nederst Alle programmer %1 (%2) Programmer Programmer og dokumenter Opførsel Kategorier Computer Kontakter Beskrivelse (navn) Kun beskrivelse Dokumenter Redigér program... Redigér programmer... Afslut session Udvid søgning til bogmærker, filer og e-mails Favoritter Gør menuen flad ned til et enkelt niveau Glem alt Glem alle programmer Glem alle kontakter Glem alle dokumenter Glem program Glem kontakt Glem dokument Glem nylige dokumenter Generelt %1 (%2) Dvale Skjul %1 Skjul program Ikon: Lås Lås skærmen Log ud Navn (beskrivelse) Kun navn Ofte anvendte programmer Ofte anvendte dokumenter Ofte anvendt På alle aktiviteter På den aktuelle aktivitet Åbn med: Fastgør til opgavelinje Steder Strøm / Session Egenskaber Genstart Nylige programmer Nylige kontakter Nylige dokumenter Nyligt anvendt Nyligt anvendt Flytbare lagringsmedier Fjern fra favoritter Genstart computeren Kør kommando... Kør en kommando eller søgeforespørgsel Gem session Søg Søgeresultater Søg... Søger efter "%1" Session Vis kontaktinformation... Vis i favoritter Vis programmer som: Vis ofte anvendte programmer Vis ofte anvendte kontakter Vis ofte anvendte dokumenter Vis nylige programmer Vis nylige kontakter Vis nylige dokumenter Vis: Luk ned Sortér alfabetisk Start en parallel session som en anden bruger Suspendér Suspendér til ram Suspendér til disk Skift bruger System Systemhandlinger Sluk computeren Skriv for at søge. Stop med at skjule programmer i "%1" Stop med at skjule programmer i denne undermenu Widgets 