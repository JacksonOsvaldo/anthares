��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  +   �     �     �     �          -     6     G     V     e     r     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-02-11 12:21+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Kopiér automatisk farven til udklipsholder Ryd historik Farveindstillinger Kopiér til udklipsholderen Standard farveformat: Generelt Åbn farvedialog Vælg en farve Vælg en farve Vis historik Ved tryk på tastaturgenvejen: 