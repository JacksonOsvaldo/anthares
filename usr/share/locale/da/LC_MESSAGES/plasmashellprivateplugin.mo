��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .     �     �     �  $   �          2     9     N  "   U     x  &   �     �  
   �     �     �     �     �     �     �          /  6   B  :   y  
   �     �     �  (   �     �     		     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-09-24 21:53+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Kør Alle widgets Kategorier: Scripting-konsol til desktop-skallen Download nye Plasma-widgets Editor Kører script på %1 Filtre Installér widget fra lokal fil... Installationsfejl Installation af pakken %1 mislykkedes. Indlæs Ny session Åbn script-fil Output Kører Runtime: %1ms Gem script-fil Skærmlås aktiveret Tidsudløb på pauseskærm Vælg plasmoid-fil Angiver det antal minutter, hvorefter skærmen låses. Angiver om skærmen skal låses efter det angivne tidsrum. Skabeloner KWin Plasma Kan ikke indlæse script-filen <b>%1</b> Kan ikke installeres Brug 