��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     ?     H     P     ]     i  )   �  !   �  -   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-11-27 19:40+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Udseende Farver: Vis sekunder Tegn gitter Vis inaktive lysdioder: Brug tilpasset farve til aktive lysdioder Brug tilpasset farve til gitteret Brug brugervalgt farve til inaktive lysdioder 