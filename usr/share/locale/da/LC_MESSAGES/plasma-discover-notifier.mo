��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     J  /   Q  0   �  /   �     �  G   �  $   F     k     �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-28 14:08+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1, %2 1 pakke til opdatering %1 pakker til opdatering 1 sikkerhedsopdatering %1 sikkerhedsopdateringer 1 pakke til opdatering %1 pakker til opdatering Ingen pakker til opdatering hvoraf 1 er en sikkerhedsopdatering hvoraf %1 er sikkerhedsopdateringer Sikkerhedsopdateringer tilgængelige Dit system er fuldt opdateret Opdatér Opdateringer tilgængelige 