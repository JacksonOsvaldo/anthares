��          <      \       p      q   *   �   /   �   �  �   #   �  2   �  4   �                   Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2010-05-27 12:50+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 Kan ikke finde "%1" ved brug af %2. Forbindelser til vejrserveren %1 havde tidsudløb. Hentning af vejrinformation for %1 tog for lang tid. 