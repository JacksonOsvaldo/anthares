��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  D   >     �     �     �     �     �     �  	          &        C  $   L  '   q     �  !   �  
   �     �  
   �  5   �          7     U  .   b     �     �  #   �  0   �          "     8     A  )   Y     �     �     �     �     �     �  �   �  @   �       	   +  '   5     ]     r     �     �     �  4   �               8     T     Y     ^  !   m     �     �     �     �     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-11-11 23:06+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 

Vælg den forrige stribe for at gå til den senest cachede stribe. &Opret tegneseriearkiv... &Gem tegneserie som... &Stribe nummer: *.cbz|tegneseriearkiv (Zip) &Faktisk størrelse Gem nuværende &position Avanceret Alle En fejl opstod for identifikatoren %1. Udseende Arkivering af tegneserie mislykkedes Opdatér tegneserie-plugins automatisk: Cache Tjek efter nye tegneseriestriber: Tegneserie Tegneseriecache: Indstil... Kunne ikke oprette arkivet på den angivne placering. Opret %1 tegneseriearkiv Oprettelse af tegneseriearkiv Destination: Vis fejl når hentning af tegneserie mislykkes Download tegneserier Fejlhåndtering Kunne ikke føje filen til arkivet. Kunne ikke oprette filen med identifikatoren %1. Fra begyndelsen til... Fra slutningen til... Generelt Hent nye tegneserier... Hentning af tegneseriestribe mislykkedes: Gå til stribe  Information Gå til &nuværende stribe Gå til &første stribe Gå til stribe... Manuelt interval Måske er der ikke forbindelse til internettet.
Måske er tegneserie-pluginet ødelagt.
En anden årsag er måske at der ikke er nogen tegneserie for dagen/nummeret/strengen, så valg af en anden vil måske virke. Midterklik på tegneserien for at vise den i original størrelse Ingen zip-fil findes, afbryder Interval: Vis kun pile når musen føres hen over Vis tegneseriens URL Vis tegneseriens forfatter Vis tegneseriens identifikator Vis tegneseriens titel Stribens identifikator: Intervallet af tegneseriestriber der skal arkiveres. Opdatér Besøg tegneseriens hjemmeside Besøg &butikens hjemmeside # %1 dage dd.mm.åååå &Næste faneblad med en ny stribe Fra: Til: minutter striber pr. tegneserie 