��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     /  $   7     \  1   |  *   �     �     �  %   	      ;	  /   \	  *   �	  '   �	  4   �	  S   
  $   h
     �
     �
     �
     �
     �
     �
     �
          &     ;     H     X                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-12-08 18:54+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Indsend Filerne blev føjet til SVN-depotet. Føjer filer til SVN-depotet... Tilføjelse af filer til SVN-depotet mislykkedes. Indsendelse af SVN-ændringer mislykkedes. SVN-ændringerne blev indsendt. Indsender SVN-ændringer... Filerne blev fjernet fra SVN-depotet. Fjerner filer fra SVN-depotet... Fjernelse af filer fra SVN-depotet mislykkedes. Filerne blev rullet tilbage i SVN-depotet. Ruller filer tilbage fra SVN-depotet... Tilbagerulning af filer fra SVN-depotet mislykkedes. SVN-statusopdatering mislykkedes. Deaktiverer indstillingen "Vis SVN-opdateringer". Opdatering af SVN-depot mislykkedes. SVN-depotet blev opdateret. Opdaterer SVN-depot... Føj til SVN Indsend til SVN... Slet fra SVN Rul tilbage i SVN Opdatér SVN Vis lokale SVN-ændringer Vis SVN-opdateringer Beskrivelse: Indsend til SVN Vis opdateringer 