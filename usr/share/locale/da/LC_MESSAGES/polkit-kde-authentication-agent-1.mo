��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     R     Z     ^  	   u  {     6   �     2     ;  (   S     |     �     �     �     �     �     �      	     	     	     -	     A	  &   W	  (   ~	     �	     �	     �	     �	     �	                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-08-26 20:29+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Handling: Et program forsøger at udføre en handling der kræver kræver privilegier. Godkendelse kræves for at udføre handlingen. En anden klient godkender allerede, prøv igen senere. Program: Autentificering kræves Autentificering mislykkedes, prøv igen. Klik for at redigere %1 Klik for at åbne %1 Detaljer mschlander@opensuse.org Tidligere vedligeholder Jaroslav Reznik Lukáš Tinkl Vedligeholder Martin Schlander &Adgangskode: Adgangskode for %1: Adgangskode for root: Adgangskode eller fingeraftryk for %1: Adgangskode eller fingeraftryk for root: Adgangskode eller fingeraftryk: Adgangskode: PolicyKit1 KDE-agent Vælg bruger Leverandør: 