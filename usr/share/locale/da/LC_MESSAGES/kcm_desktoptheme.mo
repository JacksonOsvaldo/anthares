��          |      �             !     9      E     f  
   �     �  &   �     �     �     �  1     �  E                $     <  	   M  
   W  $   b  #   �     �      �  4   �                             	      
                        Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2016-06-29 20:09+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Indstil skrivebordstema David Rosca mschlander@opensuse.org Martin Schlander Åbn tema Fjern tema Temafiler (*.zip *.tar.gz *.tar.bz2) Installation af temaet mislykkedes. Tema installeret korrekt. Fjernelse af temaet mislykkedes. Dette modul lader dig konfigurere skrivebordstemaet. 