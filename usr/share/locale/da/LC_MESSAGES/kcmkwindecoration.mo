��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p     %     +     0     =     N     U     d     p  
   |     �     �     �     �  T   �  -        3  (   P     y     �  	   �  
   �  	   �     �     �     �     �     �     �  
   �                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-11-22 22:07+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Enorm Stor Ingen kanter Ingen sidekanter Normal Overstørrelse Meget lille Meget enorm Meget stor Programmenu Kant&størrelse: Knapper Luk Luk med dobbeltklik:
 For at åbne menuen holdes knappen nede, indtil den dukker op. Luk vinduer ved at dobbeltklikke på menuknap Sammenhængsafhængig hjælp Træk knapper her og fra/til titellinjen Slip her for at fjerne knap Hent nye dekorationer... Hold over Hold under Maksimér Menu Minimér På alle skriveborde Søg Skyg Tema Titellinje 