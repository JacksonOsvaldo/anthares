��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �     i     �     �  
   �     �     �     �     �     �  
   �     �     �     �  
   �            ?   #  H   c     �     �     �  $   �  	   �            Q   (  D   z     �     �     �     �  *     $   /  
   T     _     h     �     �     �     �  $   �  "   �     �  +     $   <     a     g     �     �     �     �  5   �  $   �     
          <     S     n     �     �  0   �      �     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-01-28 14:05+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Matchende vinduesegenskab:  Enorm Stor Ingen kant Ingen sidekanter Normal Overstørrelse Meget lille Meget enorm Meget stor Stor Normal Lille Meget stor Glød på aktivt vindue Tilføj Tilføj håndtag til at ændre størrelse på vinduer uden kant Tillad ændring af størrelse fra vindues kanter på maksimerede vinduer Animationer K&napstørrelse: Kantstørrelse: Overgang ved museoverkørsel af knap Centreret Centrér (fuld bredde) klasse:  Indstil fading mellem vinduesskygge og -glød når vinduers aktivtilstand ændres Indstil vinduesknappernes fremhævningsanimation ved museoverkørsel Dekorationsindstillinger Detektér vinduesegenskaber Dialog Redigér Redigér undtagelse - Oxygen-indstillinger Aktivér/deaktivér denne undtagelse Undtagelse Generelt Skjul vinduets titellinje Information om valgt vindue Venstre Flyt ned Flyt op Ny undtagelse - Oxygen-indstillinger Spørgsmål - Oxygen-indstillinger Regulært udtryk Syntaks i regulært udtryk er ikke korrekt  Regulært udtryk der &skal matches:  Fjern Fjern den markerede undtagelse? Højre Skygger Tite&ljustering: Titel:  Brug samme farver til titellinjen og vinduets indhold Brug vinduesklasse (hele programmet) Brug vinduestitel Advarsel - Oxygen-indstillinger Navn på vinduesklasse Fald-ned-skygge på vindue Vinduesidentifikation Valg af vinduesegenskab Vinduestitel Overgange ved ændring af vinduers aktivtilstand Vinduesspecifik tilsidesættelse 