��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  d    �  t  �  M  f  �     B"     Z"     f"  "   o"  (   �"     �"  B   �"     #      #     5#     =#  0   W#  )   �#     �#     �#     �#     �#     �#     $     #$     *$     E$     K$  0   h$     �$  G   �$     �$     %     %  �   $%  '   &  v   -&     �&     �&     �&     �&     �&     '     '  <   $'     a'  G   i'  (   �'  C   �'  /   (  3   N(  ;   �(  *   �(  6   �(  7    )     X)     u)     �)  (   �)     �)     �)  @   �)  M   7*     �*     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kde
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-01-19 14:32+0200
Last-Translator: scootergrisen
Language-Team: Danish
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Sikkerhedsnotits:</b>
                             I følge en sikkerhedsprøve af Taylor Hornby (forsvarssikkerhed),
                             er den nuværende implementering af Encfs sårbar eller potientiel sårbar
                             over for flere typer angreb.
                             F.eks. kan en angriber med læse-/skriveadgang
                             til krypterede data måske sænke afkrypteringens kompleksitet
                             for efterfølgende krypterede data uden at en legitim bruger lægger mærke til,
                             eller bruge timinganalyse til er indhente information.
                             <br /><br />
                             Det betyder at du ikke bør synkronisere
                             den krypterede data til en lagertjeneste i skyen,
                             eller bruge den i andre omstændigheder hvor angriberen
                             ofte kan få adgang til den krypterede data.
                             <br /><br />
                             Se <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for mere information. <b>Sikkerhedsnotits:</b>
                             CryFS krypterer dine filer, så du kan lagre dem sikkert overalt.
                             Det virker godt sammen med skytjenester såsom Dropbox, iCloud, OneDrive og andre.
                             <br /><br />
                             I modsætning til nogle filsystems-overlægningsløsninger,
                             så afslører den ikke mappestrukturen,
                             antallet af filer og ej heller filstørrelserne
                             gennem det krypterede dataformat.
                             <br /><br />
                             En ting der er vigtig at bemærke, er at
                             selvom CryFS betragtes som sikker,
                             så er der ingen uafhængig sikkerhedsprøve
                             som bekræfter det. Opret ny sikkerhedsboks Aktiviteter Backend: Kan ikke oprette monteringspunktet Kan ikke åbne en ukendt sikkerhedsboks. Skift Vælg det krypteringssystem du vil bruge til denne sikkerhedsboks: Vælg det anvendte chiffer: Luk sikkerhedsboksen Indstil Indstil sikkerhedsboks... Den konfigurerede backend kan ikke initieres: %1 Den konfigurerede backend findes ikke: %1 Korrekt version fundet Opret Opret en ny sikkerhedsboks... CryFS Enheden er allerede åben Enheden er ikke åben Dialog Vis ikke denne besked igen EncFS Placering af krypterede data Kunne ikke oprette mapper, tjek dine rettigheder Kunne ikke køre Kunne ikke hente listen over programmer som bruger denne sikkerhedsboks Kunne ikke åbne: %1 Gennemtving lukning Generelt Hvis du begrænser sikkerhedsboksen til bestemte aktiviteter, så vise den kun i appleten når du er i de aktiviteter. Når du skifter til en aktivitet den ikke skal være tilgængelig i, så vil den automatisk blive lukket. Begrænsning på de valgte aktiviteter: Husk på at en glemt adgangskode ikke kan genfindes. Hvis du glemmer adgangskoden, så er dine data så godt så tabt. Monteringspunkt Monteringspunkt ikke angivet Monteringspunkt: Næste Åbn med filhåndtering Adgangskode: Plasma-sikkerhedsboks Indtast venligst adgangskoden for at åbne sikkerhedsboksen: Forrige Monteringspunktets mappe er ikke tom, nægter at åbne sikkerhedsboksen Den angivne backend er ikke tilgængelig Sikkerhedsboksens konfiguration kan kun ændres mens den er lukket. Sikkerhedsboksen er ukendt, kan ikke lukke den. Sikkerhedsboksen er ukendt, kan ikke destruere den. Enheden er allerede registreret. Kan ikke oprette den igen. Mappen indeholder allerede krypterede data Kan ikke lukke sikkerhedsboksen, et program bruger den Kan ikke lukke sikkerhedsboksen, den bliver brugt af %1 Kunne ikke detektere version Kan ikke udføre handlingen Ukendt enhed Ukendt fejl, kan ikke oprette backenden. Brug standardchiffer Na&vn på sikkerhedsboks: Forkert version installeret. Den påkrævede version er %1.%2.%3 Du skal vælge tomme mapper til det krypterede lager og til monteringspunktet %1: %2 