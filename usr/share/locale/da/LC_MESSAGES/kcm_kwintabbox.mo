��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �     �  
   �                    ,     <     O     ]  ]   t     �  "   �     �  !   
	     ,	  
   4	     ?	     Y	     h	     p	     x	     �	     �	     �	  �   �	  P   I
     �
     �
     �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-06-16 11:01+0200
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Aktiviteter Alle andre aktiviteter Alle andre skriveborde Alle andre skærme Alle vinduer Alternativ Skrivebord 1 Indhold Aktuel aktivitet Aktuelt program Aktuelt skrivebord Aktuel skærm Filtrér vinduer efter Indstillingerne for fokuspolitik begrænser funktionaliteten til at navigere igennem vinduer. Frem Hent nyt layout til vinduesskifter Skjulte vinduer Inkludér "Vis skrivebordet"-ikon Generel Minimering Kun et vindue pr. program Nyligt anvendt Omvendt Skærme Genveje Vis det valgte vindue Sorteringsrækkefølge: Stakrækkefølge Det aktive vindue vil blive fremhævet ved at alle andre vinduer fades ud. Denne indstilling kræver at skrivebordseffekter er aktiveret. Den effekt der skal erstatte listevinduet når skrivebordseffekter er aktiveret. Virtuelle skriveborde Synlige vinduer Visualisering 