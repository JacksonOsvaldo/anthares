��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     h     y  )   �     �     �     �     �     �       ,            
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-10-28 20:05+0100
Last-Translator: Martin Schlander <mschlander@opensuse.org>
Language-Team: Danish <kde-i18n-doc@kde.org>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 (c) 2002-2006 KDE-holdet <h1>Systembekendtgørelser</h1>Plasma tillader meget stor kontrol over, hvordan det bliver bekendtgjort, når visse hændelser forekommer. Der er adskillige valg om, hvordan du får bekendtgørelser:<ul><li>Som programmet oprindeligt blev konstrueret.</li><li>Med et bip eller anden støj.</li><li>Ved en pop-op-dialog med ekstra information.</li><li>Ved at registrere hændelsen i en logfil uden anden visuel eller hørbar advarsel.</li></ul> Carsten Pfeiffer Charles Samuels Deaktivér lyde for alle disse hændelser erik@binghamton.edu Hændelseskilde: KNotify Erik Kjær Pedersen Olivier Goffart Oprindelig implementering Kontrolpanelmodul til systembekendtgørelser 