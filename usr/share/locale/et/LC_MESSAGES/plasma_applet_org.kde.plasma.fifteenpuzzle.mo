��          �      �       0  
   1     <     K     X     m     y     �     �     �     �  B   �       �       �     �  
   �     �               ,     <     C     Z     u     }           
                 	                              Appearance Fifteen Puzzle Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2016-01-11 13:30+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Välimus Viieteistkümnemäng Arvu värv Kohandatud pildi asukoht Klotside värv Arvude näitamine Tükid segamini Suurus Tükkide kordaseadmine Lahendatud! Proovi uuesti. Aeg: %1 Kohandatud pildi kasutamine 