��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  �         )   "     L  )   l     �  6   �     �          %            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2016-01-10 23:50+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1, %2 1 uuendatav pakett %1 uuendatavat paketti 1 turbeuuendus %1 turbeuuendust 1 uuendatav pakett %1 uuendatavat paketti Pole ühtegi paketti uuendada millest 1 on turbeuuendus millest %1 on turbeuuendused Saadaval on turvauuendused Süsteem on täiesti ajakohane Saadaval on uuendused 