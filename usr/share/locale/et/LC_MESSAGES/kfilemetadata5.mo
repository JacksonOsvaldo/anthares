��    F      L  a   |                      $     2     F     S     a     p     �     �     �     �     �     �     �     �                     0     @     P     b     v     �     �     �     �     �     �     �     �               6     N      l     �     �     �     �      	  #   	     C	     b	     �	  "   �	     �	  $   �	     	
     &
     B
     b
     �
  3   �
     �
     �
  &     <   +  !   h  8   �  0   �  !   �  C     X   Z  P   �  R         W  +   x  �  �     @     O     V     ^     k     p  
   v     �  
   �  	   �     �     �     �     �     �     �     �     �     �     �               !  
   )     4     E     Y     f     l     r     {     �     �     �     �     �     �     �     �  
                  /     I     Z     n     �     �  $   �     �     �     �     �               .     4     :     F  
   U     `     n     v     �     �     �     �     �     �               /   E   A   3                      *   +   '              !      &   	          :                 8   2              )   ?   1   $      (   9   ;                           >   4                   F   %   @   #             .   
      =       C       B   5                 D         "       0                  7       6       -      ,       <       @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDuration @labelFolder @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label the URL a file was originally downloded fromDownloaded From @label the message ID of an email this file was attached toE-Mail Attachment Message ID @label the sender of an email this file was attached toE-Mail Attachment Sender @label the subject of an email this file was attached toE-Mail Attachment Subject @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2016-09-09 01:42+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Albumi esitaja Arhiiv Esitaja Proportsioon Heli Autor Bitikiirus Kanalid Kommentaar Helilooja Autoriõigus Loomise kuupäev Dokument Kestus Kataloog Kaadrisagedus Kõrgus Pilt Võtmesõnad Keel Sõnade autor Lehekülgede arv Esitlus Kirjastaja Väljalaskeaasta Diskreetimissagedus Arvutustabel Teema Tekst Pealkiri Video Laius Pildi kuupäev ja kellaaeg Pildi valmistaja Pildi mudel Pildi orientatsioon Foto ava väärtus Foto särinihe Foto säriaeg Foto F-arv Foto välklamp Foto fookuskaugus Foto fookuskaugus (35 mm) Foto GPS-kõrgus Foto GPS-laiuskraad Foto GPS-pikkuskraad Foto ISO kiirus Foto mõõterežiim Foto originaali kuupäev ja kellaaeg Foto küllastus Foto teravus Foto värvustasakaal Foto X-mõõde Foto Y-mõõde Malli loomise aeg Album Žanr Raja number Mustandtõlked Ridade arv Tõlkeühikud Tõlked Sõnade arv Allalaadimise asukoht E-kirja manuse kirja ID E-kirja manuse saatja E-kirja manuse teema Autor Viimati uuendatud 