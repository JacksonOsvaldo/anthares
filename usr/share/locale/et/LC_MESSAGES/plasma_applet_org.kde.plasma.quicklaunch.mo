��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     `  8   s     �     �     �     �     �     �     �               +     :     M     d     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-07-27 17:53+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Lisa käivitaja... Lisa käivitajaid lohistades või kontekstimenüü abil. Välimus Paigutus Muuda käivitajat... Hüpikute lubamine Nimi Üldine Peida ikoonid Maksimaalselt veerge: Maksimaalselt ridu: Kiirkäivitaja Eemalda käivitaja Näita peidetud ikoone Käivitaja nimede näitamine Nime näitamine Nimi 