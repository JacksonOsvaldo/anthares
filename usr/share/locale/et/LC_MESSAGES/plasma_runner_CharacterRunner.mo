��    
      l      �       �      �            	               .  I   3     }  	   �  �  �     %     6     C     I     P     p  K   u     �     �               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-10-26 03:40+0300
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=2; plural=n != 1;
 Käivi&tussõna: Lisa element Alias Alias: Märkide käivitaja seadistused Kood Märkide loomine :q: põhjal, kui see on 16nd-kood või defineeritud alias. Kustuta element Kuueteistkümnendkood: 