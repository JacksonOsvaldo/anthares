��          t      �                 '     6     R     Z     w  Q   �     �      �       �       �     �     �                2     S     Y     n  	   q     
      	                                                 &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 separator for a list of words,  spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-07-27 17:36+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Käi&vitussõna nõudmine Käivi&tussõna: :q: õigekirja kontrollimine. Paranda Sõnaraamatut ei leitud. Õigekirja kontrolli seadistused %1:q: Pakutavad sõnad: %1 ,  õigekiri 