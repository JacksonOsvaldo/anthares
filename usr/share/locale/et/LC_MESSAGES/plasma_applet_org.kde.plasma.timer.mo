��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     %     1  
   ?  	   J     T     X     a  
   h     s     �  3   �     �     �     �     �     
          &     -     =  	   M  L   W                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-01-11 16:13+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 töötab %1 ei tööta Lä&htesta &Käivita Muu Välimus Käsk: Näitamine Käsu täitmine Märguanded Aega jäänud: %1 sekund Aega jäänud: %1 sekundit Käsu käivitamine &Peata Märguannete näitamine Sekundite näitamine Pealkirja näitamine Tekst: Taimer Taimer lõpetas Taimer töötab Pealkiri: Kasuta hiireratast arvu muutmiseks või vali valmistaimer kontekstimenüüst 