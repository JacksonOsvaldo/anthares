��    !      $  /   ,      �      �     
       -   5  #   c  #   �  ?   �  1   �          1  H   6  L     V   �     #  
   /     :     G     ]     v  2   ~  
   �  )   �  8   �  #     .   C  -   r  D   �  6   �  0     A   M  =   �  9   �  �       �	  
   �	  "   �	  0   �	     
     &
     2
     D
     V
     j
     r
     �
     �
     �
     �
     �
     �
     �
       	     
     /   #     S  *   d  0   �     �     �     �     �       
   
                                                                         	                      
                                                          !           %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2016-12-09 00:31+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 märguanne %1 märguannet %1 / %2 %3 %1 käimas töö %1 käimas tööd &Seadista sndmuste märguandeid ja toiminguid... 10 s tagasi 30 s tagasi Näita üksikasju Peida üksikasjad Puhasta märguanded Kopeeri 1 kataloog %2 %1 kataloogist 1 fail %2 %1 failist %1 / %2 Teave Töö nurjus Töö lõpetatud Uusi märguandeid pole. Märguanded ja tööd puuduvad Ava... %1 (paus) Vali kõik Rakenduste ja süsteemi märguannete näitamine %1 (%2 jäänud) Failiedastuste ja muude tööde jälgimine Hüpikmärguannete kohandatud asukoha kasutamine %1 min tagasi %1 min tagasi %1 päeva eest %1 päeva eest Eile Praegu %1: %1: nurjus %1: lõpetatud 