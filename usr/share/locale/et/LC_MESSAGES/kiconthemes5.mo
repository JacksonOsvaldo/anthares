��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     Q  
   ]  C   h  	   �  
   �     �     �  	   �  
   �     �     �                 4   2     g     w                   
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2012-12-17 15:51+0200
Last-Translator: Marek Laane <bald@smail.ee>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 &Lehitse... Ot&simine: *.png *.xpm *.svg *.svgz|Ikoonide failid (*.png *.xpm *.svg *.svgz) Tegevused Rakendused Kategooriad Seadmed Embleemid Emotikonid Ikooni allikas MIME tüübid Muud i&koonid: Kohad &Süsteemsed ikoonid: Interaktiivne ikooninimede (nt. kataloogi) otsimine. Ikooni valimine Staatus 