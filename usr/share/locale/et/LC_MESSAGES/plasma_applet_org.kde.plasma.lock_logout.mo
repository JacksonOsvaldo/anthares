��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  +   �  0        D  	   L     V  #   _  
   �     �     �     �  B   �     �     �  8        V     e     y     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-01-11 14:14+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Kas minna passiivseisundisse mälus (unne)? Kas minna passiivseisundisse kettal (talveunne)? Üldine Toimingud Talveuni Talveuni (passiivne seisund kettal) Väljumine Väljumine... Lukusta Ekraani lukustamine Väljalogimine või arvuti väljalülitamine või taaskäivitamine Ei Uni (passiivne seisund mälus) Paralleelse seansi käivitamine teise kasutaja õigustes Passiivseisund Kasutaja vahetamine Kasutaja vahetamine Jah 