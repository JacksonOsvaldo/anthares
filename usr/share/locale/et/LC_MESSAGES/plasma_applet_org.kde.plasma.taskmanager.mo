��    B      ,  Y   <      �     �     �     �     �     �     �  U   �     2     J      Y     z  /   �  /   �     �     �     �  
   
          %  $   ;     `     m     y     �     �     �  	   �     �     �     �     �  	             #     4  	   B      L     m     z     �     �     �     �     �  (   �     	  =   	     W	     l	  "   �	     �	     �	  )   �	  (   �	  '    
  "   H
  4   k
     �
     �
     �
     �
     �
     �
  9     J   A  �  �     ;     M     T     a     j     y     �     �     �     �     �  9   �  9   4     n  
   w     �     �     �     �  &   �     �  
   	       #        @     H     b     p     �     �     �     �     �     �     �  
   �  )        0     B     b     v  	   �     �     �  3   �     �     	               $     1     8  ,   G  -   t  +   �  '   �  1   �     (  
   @     K     ^     d     j     w     �               %   ;           8          ,   !      $   (          >                      =   .   "          7       5          6              )      :   @                 /          3      +              0          4      
   	         1   *       -          2       B   A      &   ?          9                 <   '       #    &All Desktops &Close &Fullscreen &Move &New Desktop &Shade Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Keep &Above Others Keep &Below Others Keep launchers separate Ma&ximize Manually Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop New Instance On %1 On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2016-12-09 00:34+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Kõik tööl&auad &Sulge Täisek&raan Lii&guta &Uus töölaud &Keri kokku Saadaval ka tegevuses %1 Lisa aktiivsele tegevusele Kõik tegevused Luba seda rakendust grupeerida Tähestiku järgi Ülesanded paigutatakse alati nii mitme reaga veergudesse Ülesanded paigutatakse alati nii mitme veeruga ridadesse Paigutus Käitumine Tegevuse järgi Töölaua järgi Rakenduse nime järgi Akna või rühma sulgemine Ülesannete läbikerimine hiirerattaga Ei rühmitata Ei sordita Filtrid Unusta viimati kasutatud dokumendid Üldine Rühmitamine ja sortimine Rühmitamine: Akende esiletõstmine &Teiste peal T%eiste all Käivitajate hoidmine eraldi Ma&ksimeeri Käsitsi Maksimaalselt veerge: Maksimaalselt ridu: Mi&nimeeri Akna või rühma minimeerimine/taastamine Rohkem toiminguid Liiguta &aktiivsele töölauale Lii&guta tegevusele Liiguta &töölauale Uus isend Töölaual %1 Hiire keskmine klõps: Rühmitamine ainult siis, kui tegumihaldur on täis Rühmade avamine hüpikuna Taasta Paus Järgmine pala Eelmine pala Välju &Muuda suurust Ainult aktiivse tegevuse tegumite näitamine Ainult aktiivse töölaua tegumite näitamine Ainult aktiivse ekraani tegumite näitamine Ainult minimeeritud tegumite näitamine Edenemis- ja olekuteabe näitamine teguminuppudel Kohtspikrite näitamine Sortimine: Käivita uus isend Esita Peata Mitte midagi Saadaval tegevuses %1 Saadaval kõigis tegevustes 