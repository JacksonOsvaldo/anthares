��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     

     
     +
     3
     D
     \
     e
     q
     �
     �
     �
     �
     �
     �
     �
                )     0     6     B     E     L     T     `  
   g     r          �     �  ?   �  
   �     �               
          0     2     5  	   9     C     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-07-27 01:41+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 % %1 %2 %1: Rakenduste energiatarbimine Aku Täituvus Täituvuse protsent Täituvuse olek Laeb Praegu °C Üksikasjad: %1 Laeb tühjaks qiilaq69@gmail.com Energia Energiatarbimine Energiatarbe statistika Keskkond Ettenähtud Täielikult laetud Võrgutoitega Kai Uwe Broulik Viimasel 12 tunnil Viimasel 2 tunnil Viimasel 24 tunnil Viimasel 48 tunnil Viimasel 7 päeval Viimasel täislaadimisel Viimasel tunnil Tootja Mudel Marek Laane Ei Ei lae PID: %1 Asukoht: %1 Laetav Värskenda Seerianumber W Süsteem Temperatuur Seda tüüpi ajalugu ei ole selle seadme jaoks praegu saadaval. Ajavahemik Näidatavate andmete ajavahemik Tootja V Voltaaž Ärkamisi sekundis: %1 (%2%) W Wh Jah Tarbimine % 