��    3      �  G   L      h     i     o     �     �     �     �  !   �  '   �          -  -   ?     m     r     �     �     �     �     �     �     �      �       	             .     ;  
   I     T     s     �  A   �     �     �     �                     ,     :  3   A     u  �   �     *     0  ;   6     r     �     �  %   �  b   �  �  %	     �
     �
     �
     �
               0     M     a     r  %   �  
   �      �     �  
   �     �     �               &     ,     ?  	   G     Q     i     v     �     �     �     �  H   �          $     +  
   8     C     Y     h     v  ?   �     �  �   �     �     �  H   �     �     �       %     }   <             *      &      1           .   #                 (      -      3       +                                       '   0   !           2             "          %                         ,   )                    /                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2016-07-27 01:46+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 Hz &Keela kõik väljundid &Seadista uuesti (c) 2012-2013: Daniel Vrátil 90° päripäeva 90° vastupäeva Kõigi väljundite keelamine Toetamata seadistus Aktiivne profiil Muud seadistused Kas tõesti keelata kõik väljundid? Automaatne Katkesta väljundite ühendamine Nupp Märkekast Liitkast Kuvade seadistamine Daniel Vrátil Ekraani seadistamine Kuva: qiilaq69@gmail.com Lubatud Grupikast Väljundite tuvastamine KCM Test App Sülearvuti ekraan Hooldaja Marek Laane Esmane väljund puudub Lahutusvõimet pole saadaval Kscreeni taustaprogrammi ei leitud. Palun kontrolli kscreeni paigaldust. Tavaline Suund: Esmane kuva: Valikunupp Värskendamissagedus: Lahutusvõime: Skaleeri kuva Skaleerimine: Skaleerimismuudatused rakenduvad alles pärast taaskäivitamist Ekraani skaleerimine Vabandust, sinu seadistust ei saa rakendada.

Levinumad põhjused peituvad selles, et üldine ekraani suurus on liiga suur või oled lubanud rohkem kuvasid, kui sinu GPU toetab. Kaart 1 Kaart 2 Vihje: hoia kuva lohistamisel klahvi Ctrl all, et vältida külgetõmmet Ühendatud väljundid Ühenda väljundid Pea peal Hoiatus: aktiivseid väljundeid pole! Sinu süsteem toetab maksimaalselt ainult %1 aktiivset ekraani Sinu süsteem toetab maksimaalselt ainult %1 aktiivset ekraani 