��          |      �             !     9      E     f  
   �     �  &   �     �     �     �  1     �  E     �               .     :     H  &   V     }     �     �  1   �                             	      
                        Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2016-07-27 01:44+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Töölauateema seadistamine David Rosca qiilaq69@gmail.com Marek Laane Teema avamine Eemalda teema Teemafailid (*.zip *.tar.gz *.tar.bz2) Teema paigaldamine nurjus. Teema paigaldati edukalt. Teema eemaldamine nurjus. See moodul võimaldab seadistada töölauateemat. 