��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �  !   _     �     �  )   �     �     �  '   �  0      '   1  	   Y  	   c  *   m     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2016-12-09 00:50+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 E&kraani lukustamine taastamisel: Aktiveerimine Tõrge Ekraanilukustaja edukas testimine nurjus. Kohe Seansi lukustamine Ekraani automaatne lukustamine pärast: Ekraani lukustamine passiivseisundist ärkamisel &Parooli nõudmine pärast lukustamist:  min  min  sek  sek Globaalne kiirklahv ekraani lukustamiseks. &Taustapildi tüüp: 