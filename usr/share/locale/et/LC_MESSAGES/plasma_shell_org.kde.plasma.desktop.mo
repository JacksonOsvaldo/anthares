��    C      4  Y   L      �  
   �  
   �  
   �     �     �     �     �                 	   %     /     ;     B  
   I     T     [  	   a     k     ~     �     �     �     �     �     �     �     �  
   �     �  2        B     J     O     [     d     q     �     �     �     �     �     �     �     �     �     �  	   �     �               #  b   *  �   �     ,	     0	  	   ?	     I	     Z	  
   j	  	   u	     	     �	     �	     �	     �	  �  �	  	   \     f     s     �     �     �     �     �     �     �     �  
   �     �                               )     ;     J     O     b     j  	   {     �     �     �     �     �  2   �  	   �       
     	        #     3     E     J     X     l     |          �     �  
   �     �     �     �     �     �     �  R   �  �   H     �     �     �     �     
  	     
   )     4     H     P     V     p     %                 /             -   <   >          1            .   +   5   A      ,   B   ;   $   7                                  C      4       =       0       3       ?                  :      6       #                8                    !   &             	   *   2   '   "      (              9          @   )   
    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2016-09-08 04:31+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Tegevused Lisa toiming Lisa eraldaja Lisa vidinaid... Alt Alternatiivsed vidinad Alati nähtav Rakenda Rakenda seadistused Autor: Automaatne peitmine Tagasinupp All Loobu Kategooriad Keskel Sulge Seadista Seadista tegevust Loo tegevus... Ctrl Kasutatakse praegu Kustuta E-posti aadress: Edasinupp Hangi uusi vidinaid Kõrgus Rõhtsuunas kerimine Sisend Kiirklahvid Paigutust ei saa muuta, kui vidinad on lukustatud. Paigutus: Vasakul Vasak nupp Litsents: Lukusta vidinad Maksimeeri paneel Meta Keskmine nupp Muud seadistused... Hiire toimingud OK Paneeli joondus Eemalda paneel Paremal Parem nupp Ekraani serv Otsi... Shift Peata tegevus Peatatud tegevused: Vaheta Aktiivse mooduli seadistusi on muudetud. Kas muudatused rakendada või tühistada? See kiirklahv aktiveerib apleti: annab selle klaviatuurifookuse ja kui apletil on hüpikelement (näiteks käivitusmenüü), siis avab ka selle. Ülal Tühista eemaldamine Eemalda Eemalda vidin Püstsuunas kerimine Nähtavus Taustapilt Taustapildi tüüp: Vidinad Laius Võib jääda akende alla Võib jääda akende peale 