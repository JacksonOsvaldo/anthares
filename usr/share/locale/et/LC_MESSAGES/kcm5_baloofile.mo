��          �      �       0     1  #   G      k      �     �  J   �       &   )     P     o  *        �  �  �     T  '   n  !   �     �     �  G   �  "   )  /   L     |     �  !   �     �                                  	                 
       Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2016-01-09 21:32+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Failiotsingu seadistamine Autoriõigus 2007-2010: Sebastian Trüg Järgmistes asukohtades ei otsita qiilaq69@gmail.com Failiotsingu lubamine Failiotsing aitab kiiresti tuvastada kõik vajalikud failid sisu järgi Kataloog %1 on juba välja jäetud Kataloogi emakataloog %1 on juba välja jäetud Marek Laane Sebastian Trüg Väljajäetava kataloogi valimine Vishesh Handa 