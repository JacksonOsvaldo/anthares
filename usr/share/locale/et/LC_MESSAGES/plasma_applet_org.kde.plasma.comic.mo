��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  J   /     z     �     �     �     �     �          
  #        4     =  (   [     �     �     �     �     �  +   �     �       	   -  2   7     j     �     �  *   �     �     �                !     <     L     R     o     �     �  �   �  D   z     �     �  *   �          )  #   D     h     �  &   �  
   �      �     �            
   #     .     N     V     \     k     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2016-01-11 12:14+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 

Vali eelmine koomiks, et näha viimati vahemällu salvestatud koomiksit. Loo &koomiksiarhiiv... &Salvesta koomiks kui... Koomik&si number: *.cbz|Koomiksiarhiiv (Zip) Te&gelik suurus Ak&tiivse asukoha salvestamine Muu Kõik Identifikaatoriga %1 tekkis tõrge. Välimus Koomiksi arhiveerimine nurjus Koomiksipluginate automaatne uuendamine: Puhver Uute koomiksite kontrollimine: Koomiks Koomiksipuhver: Seadista... Arhiivi loomine määratud asukohta nurjus. %1 koomiksiarhiivi loomine Koomiksiarhiivi loomine Sihtkoht: Veateate näitamine koomiksi hankimise nurjumisel: Koomiksite allalaadimine Vigade kohtlemine Faili lisamine arhiivi nurjus. Faili loomine identifikaatoriga %1 nurjus. Algusest kuni... Lõpust kuni... Üldine Hangi uusi koomikseid... Koomiksi hankimine nurjus: Mine koomiksile Teave Hü&ppa tänasele koomiksile &Hüppa esimesele koomiksile Hüppa koomiksile... Vahemik käsitsi Võib-olla puudub internetiühendus.
Võib-olla on koomiksiplugin katki.
Põhjus võib peituda ka selles, et antud päeva/numbri/stringiga koomiksit polegi. Sel juhul võib aidata mõne muu valimine. Hiire keskmise nupu klõps koomiksile näitab seda originaalsuuruses Zip-faili pole, loobutakse. Vahemik: Noolte näitamine ainult hiirekursori all: Koomiksi URL-i näitamine Koomiksi autori näitamine Koomiksi identifikaatori näitamine Koomiksi pealkirja näitamine Koomiksiosa identifikaator: Arhiveeritavate koomiksiosade vahemik. Uuendamine Külasta koomiksi kodulehekülge Külasta kodule&hekülge nr %1  päeva järel pp.KK.aaaa &Järgmine kaart uue koomiksiga Alates: Kuni:  minuti järel  osa koomiksi kohta 