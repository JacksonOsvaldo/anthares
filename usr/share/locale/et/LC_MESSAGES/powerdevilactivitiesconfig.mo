��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �     �     �               !     -  2   G     z     �  '   �  
   �     �  �   �  r   �  s   	     v	  ;   �	     �	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-07-27 02:10+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
  min Tegutsemise viis Alati Erikäitumise määramine Eriseadistusi ei kasutata qiilaq69@gmail.com Talveuni Marek Laane Ekraani ei seisata kunagi Arvutit ei seisata kunagi ega viida uneseisundisse Vooluvõrgust töötav arvuti Aku peal töötav arvuti Peaaegu tühja aku peal töötav arvuti Seiskamine Passiivseisund Tundub, et energiasäästuteenus ei tööta.
Seda probleemi saab lahendada teenust käivitades või seda ajastades "Käivitamise ja väljalülitamise" all. Tegevuste teenus ei tööta.
Tegevuspõhise toitehalduse seadistamiseks peab tegevuste haldur kindlasti töötama. Tegevuste teenus töötab kõige elementaarsemal tasemel.
Tegevuste nimed ja ikoonid ei pruugi olla kättesaadavad. Tegevus "%1" Eraldi seadistuste kasutamine (ainult kogenud kasutajatele) pärast 