��    P      �  k         �     �     �     �     �     �     �     
          )     1  /   9  -   i     �  
   �     �     �     �     �     �     �  
   �     �     �     
     "     5     A     H     ^     l  	   z     �     �     �  	   �     �     �     �     �     �     �     �     �     �     	     	     	  <   	     K	     [	     n	     u	     |	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	  )   �	     (
     @
     Y
     ^
     d
     q
     y
     �
     �
     �
     �
     �
     �
     �
     �
  E   �
  �  /     �     �                 	   !     +     B     U     c     r     z  
   �  	   �     �  	   �     �     �     �  	   �  	   �     �          "     =     O     ^     e     �     �     �     �  %   �  
   �     �          
                    &  
   <     G     P     o     t     {  J   ~     �     �     �     �                    $  
   9     D     W     f  '   �     �  -   �     �  %        .     =     D  	   V  
   `  	   k     u     }     �     �  	   �     �     �  K   �               N           +   )   P   L   5   E       D   1   
             .   3   :      &   2         0   F   G   '              %      I   6             "   K   4          7   ,   #                 *      =       H   B   <   	          J      @           $   ?   >   C                       9                  M   ;   (         /                 A   !   -                              8      O    &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2016-08-20 02:06+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 &Kustuta Tühj&enda prügikast &Viska prügikasti &Ava &Aseta &Omadused Vä&rskenda töölauda Vä&rskenda vaadet Laa&di uuesti &Nimeta ümber Vali... Puhasta ikoon Joondamine Paigutus: Loobu Veergudes Seadista töölauda Kohandatud pealdis Kuupäeva järgi Vaikimisi Alanevalt Tühista kõigi valik Töölaua kujundus Sisesta kohandatud pealdis Failinime muster: Failitüübid: Filter Kataloogi hüpikeelvaatlused Kataloogid esimesena Kataloogid esimesena Täielik asukoht Selge Failide peitmine vastavalt sobivusele Hiiglaslik Ikooni suurus Ikoonid Suur Vasakule Loend Asukoht Lukustamine asukohale Lukustatud Keskmine Rohkem eelvaatluse valikuid... Name Puudub OK Vidinate liigutamiseks ja pidemete nähtavale toomiseks vajuta ja hoia all Eelvaatluse pluginad Eelvaatluse pisipildid Eemalda Muuda suurust Paremale Pööra Ridades Otsi failitüüpi... Vali kõik Kataloogi valimine Valikumarkerid Kõigi failide näitamine Failide näitamine vastavalt sobivusele Asukoha näitamine: Aktiivse tegevusega seotud failide näitamine Töölauakataloogi näitamine Töölaua tööriistakasti näitamine Suuruse järgi Väike Keskmiselt väike Sortimine Sortimine: Kataloog: Tilluke Trikid Tüübi järgi Kirjuta siia asukoht või URL Sortimata Vidinate käitlemine Vidinad on lahtilukustatud Vidinate liigutamiseks ja pidemete nähtavale toomiseks vajuta ja hoia all. 