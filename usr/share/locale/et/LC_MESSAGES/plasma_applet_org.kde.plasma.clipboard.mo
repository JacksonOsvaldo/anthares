��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �     �     �     �          -     5     =     X     q          �     �     �     �     �     �     �                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2016-01-11 12:09+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Muuda ribakoodi tüüpi Ajaloo puhastamine Lõikepuhvri sisu Lõikepuhvri ajalugu on tühi Lõikepuhver on tühi Code 39 Code 93 Seadista lõikepuhvrit ... Ribakoodi loomine nurjus Andmenaatriks Sisu muutmine +%1 Toimingu rakendamine QR-kood Eemaldamine ajaloost Tagasi lõikepuhvrisse Otsing Ribakoodi näitamine 