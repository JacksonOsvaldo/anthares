��    (      \  5   �      p     q     u  .   z     �     �     �     �  	   �     �     �     
       1   -     _     e     r     �  '   �     �     �  '   �     �                  5        N     `     h     o     ~  $   �  A   �     �     �  C   	  '   M     u     ~  �  �     6     :     A     R     X     j     p     }     �     �     �     �  B   �  
   	     )	     >	     ]	  
   i	     t	     {	  @   �	     �	     �	     �	     �	  :   �	     3
     F
     Y
     `
     n
  	   ~
     �
     �
     �
     �
     �
     �
     �
                        '               #   &          $         
                     	   (   %      !                                                                       "           %1% Back Button to change keyboard layoutSwitch layout Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Nobody logged in on that sessionUnused Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2016-12-09 00:35+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Tagasi Vaheta paigutust Loobu Caps Lock on sees Sulge Sulge otsing Seadista Seadista otsingupluginaid Töölauaseanss: %1 Teine kasutaja Klaviatuuripaigutus: %1 Väljalogimine 1 sekundi pärast Väljalogimine %1 sekundi pärast Logi sisse Sisselogimine nurjus Sisselogimine teise kasutajana Logi välja Kasutamata Parool Taaskäivita Taaskäivitus 1 sekundi pärast Taaskäivitus %1 sekundi pärast Viimased päringud Eemalda Taaskäivita Seiska Seiskamine 1 sekundi pärast Seiskamine %1 sekundi pärast Alusta uut seanssi Passiivseisundisse Vaheta Vaheta seanss Vaheta kasutaja Otsing... "%1" otsing... Eemalda lukustus Lukustuse eemaldamine nurjus konsoolis %1 (kuva %2) TTY %1 Kasutajanimi %1 (%2) 