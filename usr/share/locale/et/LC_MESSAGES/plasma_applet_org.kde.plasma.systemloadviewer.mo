��    !      $  /   ,      �     �               '     +     4     8     ?     K     _     h     x     �     �     �     �     �     �     �     �     �     �     �     �                    $     )     5  
   F     Q  �  W       	             7     <     E     I     P     e     y     �     �     �     �     �     �     �     �     �     �     �  	             .     :     C     U     l     q     �     �  	   �        	                                            
                                  !                                                                         Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cached: Circular Colors Compact Bar General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Project-Id-Version: plasma_applet_systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2016-01-11 15:54+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 s Rakendus: Keskmine kella sagedus: %1 MHz Tulp Puhvrid: CPU CPU %1 Protsessori jälgija CPU%1: %2% @ %3 Mhz CPU: %1% Protsessorid eraldi Puhverdatud: Ring Värvid Kompaktne tulp Üldine IOWait: Mälu Mälu jälgija Mälu: %1/%2 MiB Jälgija tüüp: Viisakus: Värvide määramine käsitsi Näitamine: Saaleala Saaleala jälgija Saaleala: %1% / %2 MiB Sys: Süsteemi koormus Uuendamise intervall: Kasutatav saalemälu: Kasutaja: 