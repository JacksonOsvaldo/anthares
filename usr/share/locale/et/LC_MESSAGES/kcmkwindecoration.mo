��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p  
        #  	   (     2  	   A     K     [     c  
   s     ~     �     �     �  M   �  /   �     )  )   6  #   `     �     �  
   �  
   �     �  	   �     �     �     �     �  
   �                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2016-01-10 12:27+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Hiiglaslik Suur Piireteta Külgpiireteta Normaalne Ülemõõduline Tilluke Superhiiglaslik Väga suur Rakendusemenüü Piirde &suurus: Nupud Sulge Sulgemine topeltklõpsuga:
 menüü avamiseks hoia nuppu all, kuni see ilmub. Akende sulgemine &topeltklõpsuga menüünupule Kontekstiabi Lohista nuppe siit tiitliribale ja tagasi Nupu eemaldamiseks lohista see siia Hangi uusi dekoratsioone... Teiste peal Teiste all Maksimeeri Menüü Minimeeri Kõigil töölaudadel Otsing Varja Teema Tiitliriba 