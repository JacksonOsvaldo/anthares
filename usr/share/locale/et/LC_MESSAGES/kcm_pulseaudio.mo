��          �      �           	       )   "     L  $   g  &   �  !   �  "   �     �            J   2  L   }     �      �  
   �     �       <   1     n  ;   �     �  �  �     y     ~      �     �     �      �     �          .  
   7  
   B  Z   M  O   �  	   �               "     .  >   ?     ~     �     �                 	                                         
                                              100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabApplications Add virtual output device for simultaneous output on all local sound cards Automatically switch all running streams when a new output becomes available Default EMAIL OF TRANSLATORSYour emails Mute audio NAME OF TRANSLATORSYour names Notification Sounds This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2016-09-08 04:04+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 100% Autor Autoriõigus 2015: Harald Sitter Harald Sitter Ükski rakendus ei esita heli Ükski rakendus ei salvesta heli Saada pole ühtegi sisendseadet Pole ühtegi väljundseadet Profiil: PulseAudio Rakendused Virtuaalse väljundseadme lisamine üheaegseks väljundiks kõigil kohalikel helikaartidel Kõigi töötavate voogude automaatne ümberlülitamine, kui ilmub uus väljund Vaikimisi qiilaq69@gmail.com Heli tummaks Marek Laane Märguande helid See moodul võimaldab seadistada PulseAudio heli allsüsteemi. %1: %2 100% %1% 