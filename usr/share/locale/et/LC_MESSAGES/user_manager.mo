��    0      �  C         (     )     H     c     t     �     �     �     �  �   �  r   G  }   �  �   8  �   �  �   b  �   )  g   �  h   0	  Y   �	  Y   �	  ]   M
     �
     �
  
   �
     �
  
   �
     �
  	   �
     �
  
          ]   *     �  I   �     �  %   �      $     E     `     v     �  H   �     �  -        <     B  *   J     u  �  �     &     4     C     Z     m     |     �     �  N   �  C      L   D  _   �  O   �  �   A  [   �  >   +  5   j  1   �  $   �  ,   �     $     *     9     L  
   ]     h     u     }     �     �     �  
   �  O   �     3      N  $   o     �     �     �     �  F   �     4  
   T  	   _     i  /   q      �     "                                                     .              -   	                 (   '   )      *              &                   
         $                 !      0   ,                             %   /             #   +    @title:windowChange your Face @title:windowChoose Image Add user account Choose from Gallery... Clear Avatar Delete User Delete files Email Address: Error returned when the password is invalidThe password should be at least %1 character The password should be at least %1 characters Error returned when the password is invalidThe password should be more varied in letters, numbers and punctuation Error returned when the password is invalidThe password should contain a mixture of letters, numbers, spaces and punctuation Error returned when the password is invalidThe password should contain at least %1 lowercase letter The password should contain at least %1 lowercase letters Error returned when the password is invalidThe password should contain at least %1 number The password should contain at least %1 numbers Error returned when the password is invalidThe password should contain at least %1 special character (like punctuation) The password should contain at least %1 special characters (like punctuation) Error returned when the password is invalidThe password should contain at least %1 uppercase letter The password should contain at least %1 uppercase letters Error returned when the password is invalidThe password should not contain sequences like 1234 or abcd Error returned when the password is invalidThe password should not contain too many repeated characters Error returned when the password is invalidThis password can't be used, it is too simple Error returned when the password is invalidYour name should not be part of your password Error returned when the password is invalidYour username should not be part of your password John John Doe Keep files Load from file... MyPassword New User Password: Passwords do not match Real Name: Remove user account Returned when a more specific error message has not been foundPlease choose another password Select a new face: The username can contain only letters, numbers, score, underscore and dot The username is too long The username must start with a letter This e-mail address is incorrect This password is excellent This password is good This password is very good This password is weak This user is using the system right now, removing it will cause problems This username is already used Title for change password dialogNew Password Users Verify: What do you want to do after deleting %1 ? john.doe@example.com Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:21+0100
PO-Revision-Date: 2016-08-20 03:02+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Näo muutmine Pildi valimine Kasutajakonto lisamine Laadi galeriist... Puhasta avatar Kasutaja kustutamine Kustuta failid E-posti aadress: Paroolis peab olema vähemalt %1 märk Paroolis peab olema vähemalt %1 märki Paroolis peab olema mitmekesisemaid tähti, arve ja kirjavahemärke Parool peab koosnema tähtede, arvude, tühikute ja kirjavahemärkide segust Parool peab sisaldama vähemalt %1 väiketähte Parool peab sisaldama vähemalt %1 väiketähte Parool peab sisaldama vähemalt %1 arvu Parool peab sisaldama vähemalt %1 arvu Parool peab sisaldama vähemalt %1 erimärki (näiteks kirjavahemärk) Parool peab sisaldama vähemalt %1 erimärki (näiteks kirjavahemärk) Parool peab sisaldama vähemalt %1 suurtähte Parool peab sisaldama vähemalt %1 suurtähte Parool ei tohi sisaldada selliseid jadasid nagu 1234 või abcd Parool ei tohi sisaldada liiga palju korduvaid märke Seda parooli ei saa kasutada, see on liiga lihtne Sinu nimi ei tohi olla osa paroolist Sinu kasutajanimi ei tohi olla osa paroolist Juhan Juhan Juurikas Jäta failid alles Laadi failist... MinuParool Uus kasutaja Parool: Paroolid ei sobi kokku Tegelik nimi: Kasutajakonto eemaldamine Palun vali mõnu muu parool Uus nägu: Kasutajanimi tohib sisaldada ainult tähti, arve, kriipsu, alakriipsu ja punkti Kasutajanimi on liiga pikk Kasutajanimi peab algama tähega See e-posti aadress ei ole korrektne See parool on suurepärane See parool on hea See parool on väga hea See parool on nõrk Kasutaja kasutab praegu süsteemi, tema eemaldamine tekitaks probleeme Kasutajanimi on juba kasutusel. Uus parool Kasutajad Kordus: Mida soovid ette võtta pärast %1 kustutamist? juhan.juurikas@teenusepakkuja.ee 