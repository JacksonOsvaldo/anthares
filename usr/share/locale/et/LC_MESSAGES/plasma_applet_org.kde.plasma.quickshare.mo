��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  /   �  :        C     [     c     r  
   x     �     �     �     �     �     �     �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2016-01-11 15:41+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Sulge Automaatne kopeerimine: Dialoogi ei näidata, kopeeritakse automaatselt Lohista tekst või pilt üleslaadimiseks võrguteenusesse. Tõrge üleslaadimisel. Üldine Ajaloo suurus: Aseta Palun oota Palun proovi uuesti. Saatmine... Jagamine "%1" jagamised Edukalt üles laaditud URL just jagati Laadi %1 võrguteenusesse 