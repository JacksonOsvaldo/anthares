��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     C     K     O     g  f   p  7   �  	          (   0     Y     n     �     �     �     �     �     �     �     �     �     	  $   	  (   ?	     h	     �	     �	     �	     �	                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2016-01-11 21:25+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 (%2) %1: (c) 2009: Red Hat, Inc. Toiming: Rakendus püüab sooritada toimingut, mis nõuab privileege. Selleks toiminguks on vaja end autentida. Teine klient juba autendib, palun proovi hiljem uuesti. Rakendus: Vajalik on autentimine Autentimine nurjus, palun proovi uuesti. Klõpsa, et muuta %1 Klõpsa, et avada %1 Üksikasjad qiilaq69@gmail.com Endine hooldaja Jaroslav Reznik Lukáš Tinkl Hooldaja Marek Laane P&arool: Kasutaja %1 parool: Administraatori parool: Kasutaja %1 parool või sõrmejälg: Administraatori parool või sõrmejälg: Parool või sõrmejälg: Parool: PolicyKit1 KDE agent Vali kasutaja Tootja: 