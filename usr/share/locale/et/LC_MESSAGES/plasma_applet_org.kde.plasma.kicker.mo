��    K      t  e   �      `     a     p     �     �     �     �     �  
   �     �     �            	   (     2     F     [  ,   g  	   �     �  
   �     �     �     �     	          +     ;     S     [  	   {     �     �     �     �     �     �  	   �  
   �     �     �  
   �      	     	     	     +	     <	     J	     \	     r	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     
     ,
     E
     Z
  	   p
  ,   z
     �
     �
     �
     �
     �
     �
     �
            #   .  �  R     �     �           0     A  
   I  
   T     _     k  	   r     |     �  
   �     �     �     �  <   �       #   $     H     U     m     �     �     �     �  #   �     �     �  	   �     	          !     )     8     D     U     a     v          �     �     �     �     �     �          &     >     R  '   d     �     �     �  	   �     �     �     �     �  '     '   *  (   R     {  4   �     �     �     �     �               +     A  #   U  *   y         >                  *   %       H   B       $      ?   D   J          (         "               
   <       9           5         F           )      !       2                       @      '   ,   ;   +   7            0              4   :             1              .   	   6   &      3   E   K   /             #      8   C   =       -      A   I             G           Add to Desktop Add to Favorites Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Lock screen Logout Name (Description) Name only Open with: Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show applications as: Show recent applications Show recent contacts Show recent documents Shut Down Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2016-07-27 17:52+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Lisa töölauale Lisa lemmikute sekka Otsingutulemuste joondamine alla Kõik rakendused %1 (%2) Rakendused Käitumine Kategooriad Arvuti Kontaktid Kirjeldus (nimi) Ainult kirjeldus Dokumendid Muuda rakendust... Muuda rakendusi... Lõpeta seanss Otsingu laiendamine järjehoidjatele, failidele ja kirjadele Lemmikud Menüü ahendamine ühele tasandile Unusta kõik Unusta kõik rakendused Unusta kõik kontaktid Unusta kõik dokumendid Unusta rakendus Unusta kontakt Unusta dokument Unusta viimati kasutatud dokumendid Üldine %1 (%2) Talveunne Peida %1 Peida rakendus Lukusta Lukusta ekraan Logi välja Nimi (kirjeldus) Ainult nimi Avamine rakendusega: Asukohad Toide / Seanss Omadused Taaskäivita Viimati kasutatud rakendused Viimati kasutatud kontaktid Viimati kasutatud dokumendid Viimati kasutatud Eemaldatavad andmekandjad Eemalda lemmikute seast Taaskäivita arvuti Käivita käsk... Käsu või otsingupäringu käivitamine Salvesta seanss Otsing Otsingutulemused Otsing... "%1" otsimine' Seanss Näita kontaktiteavet... Rakenduste kuvamine: Viimati kasutatud rakenduste näitamine Viimati kasutatud kontaktide näitamine Viimati kasutatud dokumentide näitamine Seiska Käivita paralleelne seanss teise kasutaja õigustes Passiivseisundisse Passiivseisundisse mälus Passiivseisundisse kettal Vaheta kasutaja Süsteem Süsteemi toimingud Lülita arvuti välja Kirjuta otsimiseks. Näita taas "%1" peidetud rakendusi Näita taas alammenüü peidetud rakendusi 