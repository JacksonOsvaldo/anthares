��          t      �         %        7     E     S     e     {  
   �     �     �  $   �  �  �  -   ~     �     �     �     �     �  
   �     
          .     	                       
                                Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-01-11 12:11+0200
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-et@linux.ee>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Värvi automaatne kopeerimine lõikepuhvrisse Puhasta ajalugu Värvivalikud Kopeeri lõikepuhvrisse Värvi vaikevorming: Üldine Vali värv Valitakse värv Näidatakse ajalugu Kiirklahvi vajutamisel: 