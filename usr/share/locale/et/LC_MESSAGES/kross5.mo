��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y       "   #     F     V     j     r     �     �     �     �     �     �     �     �  -   	  4   1	  %   f	      �	     �	     �	     �	     �	     �	     �	     �	  #   
     >
     F
     c
      l
     �
  $   �
     �
  1   �
     �
                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2016-09-09 02:53+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Autor Autoriõigus 2006: Sebastian Sauer Sebastian Sauer Käivitatav skript. Üldine Uue skripti lisamine. Lisa... Kas loobuda? Kommentaar: qiilaq69@gmail.com Muuda Valitud skripti muutmine. Muuda... Valitud skripti käivitamine. Skripti loomine interpretaatorile "%1" nurjus Skriptifaili "%1" interpretaatori tuvastamine nurjus Interpretaatori "%1" laadimine nurjus Skriptifaili "%1" avamine nurjus Fail: Ikoon: Interpretaator: Ruby interpretaatori turvatase Marek Laane Nimi: Funktsiooni "%1" ei ole olemas Interpretaatorit "%1" ei ole olemas Eemalda Valitud skripti eemaldamine. Käivita Skriptifaili "%1" ei ole olemas. Peata Valitud skripti täitmise peatamine. Tekst: Käsurearakendus Krossi skriptide käivitamiseks. Kross 