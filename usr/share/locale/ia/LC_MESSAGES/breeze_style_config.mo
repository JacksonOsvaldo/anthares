��          �   %   �      0  "   1     T  !   l  !   �     �  
   �     �     �  !   �       0   >     o     �     �     �     �     �  
   �  
   �  
      &        2     >  �  S  +   �  "     %   <  (   b     �     �  "   �     �  %   �  -     M   =  #   �  8   �     �  ,   �     *     7     ?  	   N     X  5   h     �     �                                                               
                              	                                  &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw focus indicator in lists Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-09-20 22:27+0100
Last-Translator: giovanni <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Visibilitate de acceleratores de &claviero: &Typo de button de flecha in alto: Sempre cela acceleratores de claviero Sempre monstra acceleratores de claviero Duration de anima&tion: Animationes T&ypo de button de flecha a basso: Preferentias de Breeze Traher fenestras  ex omne areas vacue Traher fenestras solmente per barra de titulo Traher fenestras  per barra de titulo, barra de menu e barras de instrumentos Designa indicator de foco in listas Designa separatores de elementos de barra de instrumento Habilita animationes Habilita manicos per redimensionar extendite Photogrammas General Nulle buttones Un button Barras de rolar Monstra acceleratores de claviero quando il necessita Duo buttones Modo de traher de W&indows: 