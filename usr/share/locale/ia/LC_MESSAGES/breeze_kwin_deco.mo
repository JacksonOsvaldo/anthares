��    ?        Y         p     q     s  !   �  "   �  &   �  ,   �  #   (  &   L  !   s  &   �  '   �  "   �  "     '   *     R  +   V     �  
   �     �     �     �     �     �     �     �     �       !        =     ]      b     �     �     �     �     �  !   �     	  	   	     	     	     9	     T	  &   g	     �	     �	     �	     �	     �	     �	     �	     �	     �	  $   
     (
     9
     S
     e
     {
     �
     �
  >   �
  �  �
     �  &   �     �     �     �     �             	        $     4     A     P     V     c  7   h     �     �     �     �     �     �               !     8     X  -   `  &   �     �  +   �     �  #   �           2      :      [     |     �     �  '   �      �     �  (   �  -   %     S     Z     x     �     �  	   �     �     �  ,   �     �                =  $   X     }      �     �     $       *      3      9         1   !       "       )      ?               
         ;                         =      :   4          5                    (      	   #                  -   7      8           %                  /                      >       0   &      6          '         .       ,          +   <   2        % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2017-02-02 23:25+0100
Last-Translator: giovanni <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % &Proprietate de fenestra coincidente:  Ponderose (Huge) Grande (Large) Necun Bordo Necun boordos lateral Normal Super dimensionate Minuscule Multe Ponderose Multe Grande Grande (Large) Parve Multe Grande Adde Adde manico [rp redimensionar fenestras con necun bordo Duration de Anima&tiones; Animationes Dimension de b&utton: Dimension de bordo: Centro Centro (Largessa complete) Classe: Color: Optiones de decoration Releva proprietates de fenestra Dialogo Designa un circulo circa le button de clauder Designa gradiente de fundo de fenestra Edita Modifica exception - Preferentias de Breeze Habilita animationes habilita/Dishabilita iste exception Typo de exception General Cela barra de titulo de fenestra Information re fenestra seligite Sinistra Move a basso  Move in alto Nove exception - Preferentias de Breeze Demanda - Preferentias de Breeze Expression Regular Syntaxe de expression regular incorrecte &Expression regular que on debe corresponder: Remove Remove le exception seligite? Dextera Umbras Dimen&sion: Minuscule &Alineamento de Titulo Titulo:  Usa classe de fenestra (application integre) Usa titulo de fenestra Aviso - Preferentias de Breeze Nomine de Classe de fenestra Identification de fenestra Selection de proprietate de fenestra Titulo de fenestra Annullationes specific de Window For&tia 