��          �      <      �  &   �  +   �       	             2     8     A     F  (   V          �  ,   �     �     �     �     �  �  �  !   �  %   �     �     �     �     �               "  -   3     a     d  1   {     �     �     �     �        	      
                                                                                       Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2014-04-22 14:15+0200
Last-Translator: G.Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-l10n-ia@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Tu vole suspender a RAM (dormir)? Tu vole suspender a disco (hibernar)? General Hiberna Hiberna (Suspende a Disco) Lassa (abandonar) Lassa (abandonar)... Bloca Bloca le schermo Claude session, stoppa o re-starta computator No Dormi (suspende a RAM) Initia un session parallel como differente usator Suspende Commuta Usator Commuta Usator Si 