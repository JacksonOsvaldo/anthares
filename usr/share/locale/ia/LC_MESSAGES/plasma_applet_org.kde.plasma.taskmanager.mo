��          �   %   �      0  U   1     �     �  
   �     �     �     �     �     �     �  	   �                     .  )   4  (   ^  '   �  "   �     �     �  9   �  J   #  �  n          &     7     F     U  
   m  
   x     �     �     �     �     �     �     �     �  4   �  4     1   N  ,   �     �     �     �      �                                                                            
                           	                        Activities a window is currently on (apart from the current one)Also available on %1 Alphabetically By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum rows: On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2013-10-10 15:00+0200
Last-Translator: Giovanni Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-ia@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Anque disponibile sur %1 Alphabeticamente Per Activitate Per scriptorio Per nomine de programma Non gruppa Non ordina Filtros General Gruppar e ordinar Gruppar: Evidentia fenestras Manualmente Maxime lineas: Sur %1 Solmente Il monstra cargas ex le activitate currente Solmente il monstra cargas ex le scriptorio currente Solmente Il monstra cargas ex le schermo currente Il monstra solmente cargas que es minimisate Monstra consilios Ordinar: Disponibile sur %1 Disponibile sur omne activitates 