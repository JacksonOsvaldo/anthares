��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  q   8  a   �       !        ;     K     W     q     �  +   �     �     �     �     �  @   �  f   :	  4   �	                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-11-21 13:25+0100
Last-Translator: Giovanni Sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-ia@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.4
 (c) 2003-2007 Fredrik Hoglund <qt>Tu es secur que tu vole remover le thema de cursor <i>%1</i> ?<br />Isto cancellara omne files installate per iste thema.</qt> <qt>Tu non pote cancellar le thema que tu es usante currentemente.<br />Prime tu debe passar a altere thema.</qt> Un thema appellate %1 ja existe in tu dossier de thema de icone. Tu vole reimplaciar lo con isto? Confirmation Preferentias de cursor modificate Thema de cursor Description Trahe o typa URL de thema g.sora@tiscali.it Fredrik Hoglund Obtene nove schemas de color ex le Internet Giovanni Sora Nomine Super scriber le thema? Remove Thema Le file %1 non sembla esser un valid archivo de thema de cursor. Il non pote cargar le archivo de thema de cursor; pro favor tu verifica que le adresse %1 es correcte. Il non pote trovar le archivo de thema de cursor %1. 