��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �  .   �  �    �  �  ^   |
     �
     �
     �
  )        <     U  2   p      �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-09 16:45+0100
Last-Translator: g.sora <g.sora@tiscali.it>
Language-Team: Interlingua <kde-i18n-it@kde.org>
Language: ia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 sec &Tempore de Expiration de Indication de Initio <H1>Notification de barra de cargas</H1>
Tu pote habilitar un secunde methodo de notification de initio que es
usate per le barra de cargas ubi le button con le horologio rotante appare,
symbolizante que tu application que initiava es cargante.
Il pote occurrer que alcun applicationes non es avisate de iste notification
de initio. In iste caso, le button dispare postea le tempore date in
le section 'Tempore de Expiration de Indication de Initio' <h1>Cursor de occupate</h1>
KDE offere un cursor de occupate pro notification de initio de application.
Pro habilitar le cursor de occupate, tu selectiona un genere de retorno visual
ex le quadro combo.
Il pote occurrer que alcun applicationes non es avisate de iste notification
de initio. In iste caso, le cursor stoppa de palpebrar postea le tempore
date in le section 'Tempore de Expiration de Indication de Initio' <h1>Retorno de lanceamento</h1>Tu ci pote configurar le retorno de lanceamento de application. Cursor palpebrante Cursor pulsante C&ursor de occupate Habilita no&tification de barra de cargas Nulle cursor de occupate Cursor de occupate passive E&xpiration de tempore de indication pro  Initiar  &Notification de barra de cargas 