��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %    {  
   }     �  
   �     �     �  	   �  	   �     �     �  
   �  %   �               %  
   2  
   =     H  	   O     Y     o     �  Y   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-07-25 11:42+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 køyrer %1 køyrer ikkje &Nullstill &Start Avansert Utsjånad Kommando: Vising Køyr kommando Varslingar Tid att: %1 sekund Tid att: %1 sekund Køyr kommando S&topp Vis varsling Vis sekund Vis tittel Tekst: Nedteljar Nedteljinga er ferdig Nedteljaren køyrer Tittel: Bruk mushjulet til å endra siffer, eller vel frå ferdiglaga nedteljarar i kontektmenyen 