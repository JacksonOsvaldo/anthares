��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �    �     �                    !     2  	   8     B     I     c     �     �  
   �  5   �  5   �     *     2     :     J     [     m  #   �     �     �     �     �     �     �               #     1     5     F     X     l  	   q     {      �     �     �     �  #   �     �     �          1     G  
   L     W     ^     s     �  2   �      �     �     �       
             "     *     :     @  ,   Z  -   �  )   �     �  8   �     6     G  
   M     X     i     n     t     �     �     �     �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-24 20:02+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Alle skrivebord &Lukk &Fullskjerm &Flytt &Nytt skrivebord &Fest &Rull opp &%1 %2 Også tilgjengelig på %1 Legg til i gjeldande aktivitet Alle aktivitetar Tillat gruppering av programmet Alfabetisk Ordna alltid oppgåver i kolonnar med så mange rader Ordna alltid oppgåver i rader med så mange kolonnar Ordning Åtferd Etter aktivitet Etter skrivebord Etter programnamn Lukk vindauge/gruppe Bla gjennom oppgåver med mushjulet Ikkje grupper Ikkje sorter Filter Gløym nyleg brukte dokument Generelt Gruppering og sortering Gruppering: Framhev vindauge Ikonstorleik: – Hald &over andre Hald &under andre Hald startar skilde Stor Ma&ksimer Manuell Marker program som kan spela lyd Maks kolonnar: Maks rader: Mi&nimer Minimer/gjenopprett vindauge/gruppe Fleire handlingar Flytt &til dette skrivebordet Flytt til &aktivitet Flytt til &skrivebord Demp Ny instans På %1 På alle aktivitetar På gjeldande aktivitet Ved midtklikking: Lag berre grupper når oppgåvehandsamaren er full Opna grupper i sprettoppvindauge Gjenopprett 9 999+ Pause Neste spor Førre spor Avslutt &Endra storleik Løys %1 stad til %1 stadar til Vis berre oppgåver frå gjeldande aktivitet Vis berre oppgåver frå gjeldande skrivebord Vis berre oppgåver frå gjeldande skjerm Vis berre minimerte oppgåver Vis framdrifts- og statusinformasjon på oppgåveknappar Vis hjelpebobler Liten Sortering: Start ny instans Spel Stopp Gjer ingenting &Fest Grupper/avgrupper Tilgjengeleg på %1 Tilgjengeleg i alle aktivitetar 