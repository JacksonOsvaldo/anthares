��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %          .     B     R  	   m     w  	   �     �     �     �     �     �  @   �          #     @  	   F  
   P  	   [     e     �     �     �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-05-31 10:15+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Éi fil %1 filer Éi mappe %1 mapper %1 av %2 ferdig %1 av %2 handsama ved %3/s %1 ferdig %1 handsama ved %2/s Utsjånad Åtferd Avbryt Tøm Set opp … Ferdige jobbar Oversikt over fileroverføringar/-jobbar som pågår (kuiserver) Flytt dei til ei anna liste Flytt dei til ei anna liste. Pause Fjern dei Fjern dei. Hald fram Vis alle jobbane i ei liste Vis alle jobbane i ei liste. Vis alle jobbane i eit tre Vis alle jobbane i eit tre. Vis separate vindauge Vis separate vindauge. 