��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  �                   	   $     .     C     O     R     [     j     y     �     �     �     �  	   �     �     �     �     �     �                     1     ?  #   C     g     n  
   r     }     �     �     �     �     �     �     �     �     �        	     	             *     6     D     P     Y     a     i     z     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  	   �          
          /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-24 20:03+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  minutt %1 %2 %1 (%2) Utsjånad Beaufort-skalaen bft Celsius °C ° Detaljar Fahrenheit °F 1 dag %1 dagar Hectopascal hPa min. %1, maks. %2 maks. %1 Merkur-tommar inHg Kelvin K Kilometer Kilometer i timen km/t Kilopascal kPa Knop kt Stad: min. %1 Meter i sekundet m/s Mile Mile i timen mph Millibar mbar – Fann ingen vêrstasjonar for «%1» Varsel  % Lufttrykk: Søk Vel leverandør av vêrdata − Vis temperatur i kompaktmodus: Set opp Temperatur: Einingar Oppdater kvart: Sikt: Vêrstasjon Stille Vindfart: %1 (%2 %) Luftfukt: %1%2 Sikt: %1 %2 Doggpunkt: %1 Humidex: %1 fallande aukande stabilt Trykktendens: %1 Trykk: %1 %2 %1%2 Sikt: %1 %1 (%2) Ekstremvêrvarsel: Aktsemdsvarsel: A ANA ASA N NA NNA NNV NV S SA SSA SSV SV var. V VNV VSV %1, %2 %3 Stille Vindkjøling: %1 Vind: %1 %2 