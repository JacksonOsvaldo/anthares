��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �     �  
   �     �     �          *     G     T     \     d     u  
   }  
   �  	   �     �     �     �     �     �     �  ,   �  
   ,     7  
   U     `     v     �     �     �     �     �     �     �  
   �     
          !     '     ,     :     B  
   S     ^     r  
   �     �     �  	   �     �     �     �     �               &     =     S     `     m     �     �     �  !   �     �     �        	             .     3     O     `     q     �     �     �     �     �     
            ,   1  
   ^     i       
   �     �     �     �     �     �     �              Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-23 05:56+0100
PO-Revision-Date: 2018-01-24 19:58+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Handsam «%1» … Vel … Fjern ikon Legg til skrivebord Legg til favorittar Legg til panel (skjermelement) Plasser søkjeresultat nedst Alle program %1 (%2) Program Program og hjelp Åtferd Kategoriar Datamaskin Kontaktar Skildring (namn) Berre skildring Dokument Rediger program … Rediger program … Avslutt økta Utvid søket til bokmerke, filer og e-postar Favorittar Flat ut menyen til eitt nivå Gløym alt Gløym alle programma Gløym alle kontaktane Gløym alle dokumenta Gløym program Gløym kontakt Gløym dokument Gløym nyleg brukte dokument Generelt %1 (%2) Dvalemodus Gøym %1 Gøym program Ikon: Lås Lås skjermen Logg ut Namn (skildring) Berre namn Ofte brukte program Ofte brukte dokument Ofte brukt På alle aktivitetar På gjeldande aktivitet Opna med: Fest tilo oppgåvelinje Stadar Straum / økt Eigenskapar Start på nytt Nyleg brukte program Nyleg brukte kontaktar Nyleg brukte dokument Nyleg brukte Nyleg brukte Flyttbare lagringseiningar Fjern frå favorittar Start datamaskina på nytt Køyr kommando … Køyr ein kommando eller eit søk Lagra økta Søk Søkjeresultat Søk … Søkjer etter «%1» Økt Vis kontaktinformasjon … Vis i favorittar Vis program som: Vis ofte brukte program Vis ofte brukte kontaktar Vis ofte brukte dokument Vis nyleg brukte program Vis nyleg brukte kontaktar Vis nyleg brukte dokument Vis: Slå av maskina Sorter alfabetisk Start ei parallell økt som ein annan brukar Kvilemodus Kvilemodus til minnet Dvalemodus til disk Byt brukar System Systemhandlingar Slå av datamaskina Skriv for å søkja. Vis program i «%1» Vis program i denne undermenyen Skjermelement 