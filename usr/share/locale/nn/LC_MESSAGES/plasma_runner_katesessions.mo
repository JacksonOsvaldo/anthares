��          <      \       p   !   q   3   �      �   �  �   '   �  -   �     #                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-06-22 22:42+0200
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 0.3
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Finn Kate-økter med treff på «:q:». Listar opp alle Kate-øktene på kontoen din. Opna Kate-økt 