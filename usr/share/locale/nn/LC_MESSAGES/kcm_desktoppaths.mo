��            )   �      �  �   �  	   W     a     q     �     �  	   �     �  	   �     �  %   �     �               #     )     5     >  X   M  S   �  �   �    �  I   �  F     E   _  H   �  B   �  :   1  7   l  �  �  �   �
  	   a     k     y  
   �     �     �     �     �     �     �     �     �     �     �  
   �       	     N     A   g  �   �  �   Q  F   4  D   {  D   �  D     9   J     �     �                                                        	                                                                
                 <h1>Paths</h1>
This module allows you to choose where in the filesystem the files on your desktop should be stored.
Use the "Whats This?" (Shift+F1) to get help on specific options. Autostart Autostart path: Confirmation Required Desktop Desktop path: Documents Documents path: Downloads Downloads path: Move files from old to new placeMove Move the directoryMove Movies Movies path: Music Music path: Pictures Pictures path: The path for '%1' has been changed.
Do you want the files to be moved from '%2' to '%3'? The path for '%1' has been changed.
Do you want to move the directory '%2' to '%3'? This folder contains all the files which you see on your desktop. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder contains applications or links to applications (shortcuts) that you want to have started automatically whenever the session starts. You can change the location of this folder if you want to, and the contents will move automatically to the new location as well. This folder will be used by default to load or save documents from or to. This folder will be used by default to load or save movies from or to. This folder will be used by default to load or save music from or to. This folder will be used by default to load or save pictures from or to. This folder will be used by default to save your downloaded items. Use the new directory but do not move anythingDo not Move Use the new directory but do not move filesDo not Move Project-Id-Version: kcmkonq
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2018-01-22 21:08+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <h1>Stiar</h1> I denne modulen kan du velja kvar i filsystemet filene på skrivebordet skal lagrast.
Bruk funksjonen «Kva er dette?» (Shift&nbsp;+ F1) for å få hjelp om spesifikke innstillingar. Autostart Autostartsti: Treng stadfesting Skrivebord Skrivebordssti: Dokument Dokumentsti: Nedlastingar Nedlastingssti: Flytt Flytt Filmar Filmsti: Musikk Musikksti: Bilete Biletsti: Stien for «%1» er endra.
Vil du at filene i «%2» skal flyttast til «%3»? Stien for «%1» er endra.
Vil du flytta mappa «%2» til «%3»? Denne mappa inneheld alle filene du ser på skrivebordet. Du kan endra plasseringa av denne mappa. I så fall vert innhaldet automatisk flytta til den nye plasseringa. Denne mappa inneheld program eller lenkjer til program (snarvegar) du vil starta automatisk kvar gong økta startar. Du kan endra plasseringa av denne mappa. I så fall vert innhaldet automatisk flytta til den nye plasseringa. Dette er den mappa dokument vanlegvis vert lagra til eller lesne frå. Dette er den mappa filmar vanlegvis vert lagra til eller lesne frå. Dette er den mappa musikk vanlegvis vert lagra til eller lesne frå. Dette er den mappa bilete vanlegvis vert lagra til eller lesne frå. Dette er den mappa nedlasta filer vanlegvis vert lagra i. Ikkje flytt Ikkje flytt 