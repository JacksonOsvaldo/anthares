��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  !   �     �     �     �     �               $     -     6     B     G     N     T  '   `     �  H   �  G   �     "     .     >     L  	   k     u     �  P   �  3   �          4     O     ^  '   f     �     �     �     �  "   �       	     	     %   !  $   G     l  .   ~     �     �  !   �  
   �     �     �       6     (   N     w  "   �     �     �     �     �       &         C     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2018-01-27 12:13+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Tilhøyrande vindaugseigenskap:  Enorm Stor Inga kantlinje Ingen sidekantlinjer Vanleg Altfor stor Ørliten Kolossal Svært stor Stor Vanleg Liten Svært stor Kantlinjeglød på det aktive vindauget Legg til Legg til handtak for å endra storleik på vindauge som ikkje har rammer Tillatt endring av storleik på maksimerte vindauge frå vindaugskantar Animasjonar &Knappstorleik: Kantstorleik: Overgang ved peikar over knapp Midtstilt Midstilt (full breidd) Klasse:  Set opp inn- og uttoning for skugge og glød på vindauge som endrar aktivstatus Set opp animasjon for når peikaren er over knappar Alternativ for dekorasjon Oppdag vindaugseigenskapar Dialogvindauge Rediger Rediger unntak – Oxygen-innstillingar Slå dette unntaket på/av Unntakstype Generelt Gøym tittellinja til vindauget Informasjon om det valde vindauget Til venstre Flytt ned Flytt opp Nytt unntak – Oxygen-innstillingar Spørsmål – Oxygen-innstillingar Regulært uttrykk Syntaksen i det regulære uttrykket er ugyldig Regulært uttrykk:  Fjern Vil du fjerna det valde unntaket? Til høgre Skuggar &Titteljustering: Tittel:  Bruk dei same fargane på tittellinje og vindaugsflate Bruk vindaugsklasse (i heile programmet) Bruk vindaugstittel Åtvaring – Oxygen-innstillingar Vindaugsklassenamn Skugge på vindaugsnedtrekk Vindaugsidentifikasjon Val av vindaugseigenskap Vindaugstittel Overgangar ved endring av aktiv status Vindaugsspesifikke overstyringar 