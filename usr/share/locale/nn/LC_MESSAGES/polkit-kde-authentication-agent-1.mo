��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     �     �     �  	   �  s   �  ?   :     z     �  #   �     �     �     �     �     	     	     *	     8	     F	  	   Z	     d	     t	  !   �	  (   �	     �	     �	     �	  
   
  
   
                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-07-26 19:39+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 (%2) %1: © 2009 Red Hat Inc. Handling: Eit program prøver å gjera noko som treng utvida løyve. For at det skal få gjera dette, må du autentisera deg. Ein annan klient autentiserer allereie. Prøv på nytt seinare. Program: Treng autentisering Autentiseringsfeil. Prøv på nytt. Trykk for å redigera %1 Trykk for å opna %1 Detaljar karl@huftis.org Tidlegare vedlikehaldar Jaroslav Reznik Lukáš Tinkl Vedlikehaldar Karl Ove Hufthammer P&assord: Passord for %1: Passord for rotbrukar: Passord eller fingertrekk for %1: Passord eller fingertrekk for rotbrukar: Passord eller fingertrekk: Passord: PolicyKit1 KDE-Agent Vel brukar Produsent: 