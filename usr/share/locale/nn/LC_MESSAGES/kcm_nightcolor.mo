��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     4     7     ?     Z  
   n  	   y     �  !   �  !   �     �     �  
   �     �  
             )  
   :  
   E     P     Y     e  
   r                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2018-01-22 21:08+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  K (HH:MM) (i minutt, frå 1 til 600) Slå på nattfargar Automatisk Finn stad karl@huftis.org Feil: Morgon er ikkje før kveld. Feil: Overgangstidene overlappar. Breiddegrad Stad Lengdegrad Karl Ove Hufthammer Nattfargar Nattfarge-temperatur:  Operasjonsmodus: Roman Gilg Solrenning Soleglad Klokkeslett Overgangstid og sluttar 