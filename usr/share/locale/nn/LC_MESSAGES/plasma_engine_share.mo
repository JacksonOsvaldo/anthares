��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  %   +  .   Q  9   �     �  1   �           )     H  .   T         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-15 00:21+0200
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Klarte ikkje finna MIME-type for fila Klarte ikkje finna alle nødvendige funksjonar Klarte ikkje finna leverandøren med det oppgjevne målet Feil ved køyring av skript Ugyldig adresse for den etterspurde leverandøren Klarte ikkje lesa den valde fila Tenesta var ikkje tilgjengeleg Ukjend feil Du må oppgje ei nettadresse for denne tenesta 