��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	     �
       !     4   ?     t     �     �     �     �     �     �     �     �     	                $     0     >     J     Z     p  	   �  
   �     �     �  "   �     �  $   �  .        A     a     }     �     �     �  
   �                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-24 20:03+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 varsling %1 varslingar %1 av %2 %3 %1 jobb køyrer %1 jobbar køyrer &Set opp varslingar og handlingar for hendingar … 10 sekund sidan 30 sekund sidan Vis detaljar Gøym detaljar Tøm varslingar Kopier Kopier lenkjeadresse 1 mappe %2 av %1 mapper 1 fil %2 av %1 filer Varslingslogg %1 av %2 +%1 Informasjon Jobb mislukka Jobb ferdig Fleire val … Ingen nye varslingar. Ingen varslingar eller jobbar Opna … %1 (pause) Merk alt Vis varslingslogg Vis program- og system­varslingar %1 (%2 att) Vis filoverføringar og andre jobbar Bruk tilpassa plassering for varslingsvindauge %1 minutt sidan %1 minutt sidan %1 dag sidan %1 dagar sidan I går Nett no %1: %1: mislukka %1: ferdig 