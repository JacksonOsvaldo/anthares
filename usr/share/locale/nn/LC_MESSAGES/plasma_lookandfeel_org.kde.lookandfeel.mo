��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
               !     /     A     H     ]  	   b     l     t     �     �     �  ,   �     �     �     	     #  
   +     6     N     U     X  
   `     k     w  :   �  
   �     �     �     �     �  *   �     )  
   7     B     F  
   O  	   Z     d     u     �     �     �     �  
   �     �     �               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-24 19:59+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 % Tilbake Batterinivå: %1 % Byt utforming Virtuelt tastatur Avbryt «Caps Lock» er på Lukk Lukk søk Set opp Set opp søkjetillegg Skrivebordsøkt: %1 Annan brukar Tastaturoppsett: %1 Loggar ut om 1 sekund Loggar ut om %1 sekund Logg inn Feil ved innlogging Logg inn som annan brukar Logg ut Neste spor Ingen medium vert spela Ubrukt OK Passord Spel/pause Førre spor Start på nytt Startar på nytt om 1 sekund Startar på nytt om %1 sekund Siste søk Fjern Start på nytt Vis mediekontrollar: Slå av Slår av om 1 sekund Slår av om %1 sekund Start ny økt Kvilemodus Byt Byt økt Byt brukar Søk … Søk «%1» … Plasma frå KDE Lås opp Klarte ikkje låsa opp på terminal %1 (%2) TTY %1 Brukarnamn %1 (%2) i kategorien «nylege søk» 