��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	             #  <   $  d   a  P   �  	     '   !     I     P  �   `     '  
   C  	   N  	   X     b     k     q  )   z     �     �     �  &   �  #   �  ;     "   S     v     �  a   �            '   -  �   U  $        3     9     R  
   i  
   t          �  ;  �     �     �     �  �        �     �     �     �     �     �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-27 12:02+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 «Nyoppteikning av fullskjerm» kan føra til ytingsproblem. «Berre ved låg kostnad» hindrar berre rifting for fullskjermsendringar, for eksempel for videoar. «Bruk skjerminnhaldet på nytt» gjev kraftig redusert yting på MESA-drivarar. Nøyaktig Tillat program å blokkera samansetjing Alltid Animasjonsfart: Program kan oppgje eit hint for å blokkera samansetjing når vindauga deira er opne.
 Dette gjer at for eksempel spel kan køyra raskare.
 Innstillinga kan overstyrast av vindaugsspesifikke reglar. Utviklar: %1
Lisens: %2: %2 Automatisk Tilgjenge Utsjånad Godsaker Fokus Verktøy Animering ved byte av virtuelt skrivebord Vindaugshandsaming Set opp filter Skarp karl@huftis.org,korsvoll@skulelinux.no Slå på sammensetjing ved oppstart Utelat skrivebordseffektar som samansetjaren ikkje støttar Utelat interne skrivebordseffektar Nyoppteikning av fullskjerm Hent nye effektar … Hint: Sjå på effektinnstillingane for å finna ut korleis du set opp eller tek effekten i bruk. Med ein gong KWin-utviklingslaget Ta vare på miniatyrbilete av vindauge: Å ta vare miniatyrbilete av vindauge vil alltid øydeleggja for minimeringstilstanden til vindauga. Dette kan føra til at vindauge ikkje set arbeidet i pause når dei vert minimerte. Karl Ove Hufthammer,Håvard Korsvoll Aldri Berre for viste vindauge Berre ved låg kostnad OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL-samansetjing (standarden) har krasja KWin tidlgere.
Dette kjem truleg av ein feil i drivaren.
Viss du meiner at du i mellomtida har oppgradert til ein stabil drivar,
kan du slå av dette vernet, men ver merksam på at det kan føra til krasj med ein gong.
Elles kan du kanskje bruka XRender-motoren i staden. Slå på OpenGL-oppdaging Bruk skjerminnhaldet på nytt Oppteikningsmotor: Ikkje all maskinvare støttar skaleringsmetoden «Nøyaktig», og det kan føra til dårleg yting og artefaktar ved oppteikning. Skaleringsmetode: Søk Jamn Jamn (tregare) Riftførebygging («vsync»): Svært sakte XRender 