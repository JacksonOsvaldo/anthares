��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     |     �     �     �     �     �     �     �     �          	          #     0     5  
   <     G     P     U     Z     b     t     �     �     �     �  	   �     �     �     �     �     �  0   �  G   0  
   x     �     �     �     �     �     �     �     �     �     �            
         +  
   9  	   D     N     T     d     x  X   |  �   �     k     p     �     �     �  
   �     �     �     �     �     �          +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2018-01-25 20:55+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Aktivitetar Legg til handling Legg til mellomrom Legg til element … Alt Alternative element Alltid synleg Bruk Bruk innstillingar Bruk no Laga av: Gøym automatisk Tilbakeknapp Nede Avbryt Kategoriar I midten Lukk  +  Set opp Set opp aktivitet Legg til aktivitet … Ctrl Vert brukt no Slett E-postadresse: Framknapp Hent nye element Høgd Vassrett rulling Inndata her Snøggtastar Kan ikkje byta utforming når elementa er låste Du må ta i bruk utformingsendringane før du kan gjera andre endringar Utforming: Til venstre Venstreknappen Lisens: Lås elementa Maksimer panelet Meta Midtknappen Fleire innstillingar … Musehandlingar OK Panelplassering Fjern panelet Til høgre Høgreknappen Skjermkant Søk … Shift Stopp aktivitet Stoppa aktivitetar: Byt Innstillingane i denne modulen er endra. Ønskjer du å bruka eller forkasta endringane? Denne snøggtasten vil starta skjermelementet og gje det skrivefokus. Viss det har ein sprettopp (som for eksempel startmenyen har), vert denne opna. Oppe Angra avinstallering Avinstaller Avinstaller element Loddrett rulling Synlegheit Bakgrunnsbilete Type bakgrunn: Skjermelement Breidd Vindauge kan dekkja over Vindauge kan liggja under 