��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �         
          
   !     ,     @  	   M     W     _  
   s  e   ~     �  ,   �  
   &     1     ?     D     J     R  
   ^     i     ~     �     �     �     �     �     	     	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-25 20:52+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Vel … Fjern ikon Legg til favorittar Alle program Utsjånad Program Program oppdaterte. Datamaskin Dra faner mellom boksane for å gøyma/visa dei, eller bytt om på dei synlege fanene ved å dra dei. Rediger program … Utvid søket til bokmerke, filer og e-postar Favorittar Gøymde faner Logg Ikon: Avslutt Menyknappar Ofte brukt På alle aktivitetar På gjeldande aktivitet Fjern frå favorittar Vis i favorittar Vis program etter namn Sorter alfabetisk Byt faner når peikaren er over Skriv for å søkja … Synlege faner 