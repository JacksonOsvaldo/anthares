��    5      �  G   l      �     �     �     �     �     �     �  !   �  '        >     M  -   _     �     �     �     �     �     �     �     �           
     +  	   3     =     N     [  
   i     t     �     �  A   �                     %     2     @     L  <   Z  <   �     �  3   �       �        �     �  ;   �     	     	     *	  %   6	  b   \	  �  �	     �     �     �     �     �               .     D     Q  2   i  
   �     �     �     �     �     �     �               "     2  
   6     A     V     f     x     �     �  '   �  I   �          %     .  
   ;     F     X     e     s     v  
   y  0   �     �  �   �     i     p  I   w     �     �     �  (   �  j                ,      &      3           0   #                 *      /      5       -                                       '   (   !   )   2   4             "          %                         .   +                    1                   
   	         $    %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale multiplier, show everything at 1 times normal scale1x Scale multiplier, show everything at 2 times normal scale2x Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Tip: Hold Ctrl while dragging a display to disable snapping Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2018-01-22 21:08+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 Hz &Slå av alle utgangar &Set opp på nytt © 2012–2013 Dan Vrátil 90° med klokka 90° mot klokka Slå av alle utgangar Ikkje-støtta oppsett Aktiv profil Avanserte innstillingar Er du sikker på at du vil slå av alle utgangane? Automatisk Bryt opp sameinte utgangar Knapp Avkryssingsboks Kombinasjonsboks Oppsett av skjermar Daniel Vrátil Skjermoppsett Skjerm: karl@huftis.org På Gruppefelt Identifiser utgangar KCM-testprogram Skjerm på berbar Vedlikehaldar Karl Ove Hufthammer Ingen hovudutgang Ingen tilgjengelege skjermoppløysingar Fann ikkje nokon kscreen-motor. Kontroller at kscreen er rett installert. Vanleg Retning: Hovudskjerm: Radioknapp Oppfriskingsrate: Oppløysing: Skaler vising 1x 2x Skalering: Endringar i skalering får verknad etter omstart Skjermskalering Oppsettet ditt kunne ikkje takast i bruk.

Vanlege grunnar er at skjermstorleiken er sett for stor, eller du slo på fleire skjermar enn det skjermkortet støttar. Fane 1 Fane 2 Tips: Hald inne Ctrl mens du drar eit skjermbilete for å slå av festing Samein utgangar Samein utgangar Opp ned Åtvaring: Det er ingen aktive utgangar. Systemet ditt støttar berre høgst %1 aktiv skjerm Systemet ditt støttar berre høgst %1 aktive skjermar 