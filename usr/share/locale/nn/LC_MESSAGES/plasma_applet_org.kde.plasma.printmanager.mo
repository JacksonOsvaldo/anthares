��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �     �            *   *  $   U     z     �     �  	   �     �  G   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2016-10-25 19:15+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Set opp skrivarar … Berre verksame jobbar Alle jobbar Berre fullførte jobbar Set opp skrivar Generelt Ingen verksame jobbar Ingen jobbar Ingen skrivarar er sette opp eller oppdaga Éin verksam jobb %1 verksame jobbar Éin jobb %1 jobbar Opna utskriftskø Utskriftskøen er tom Skrivarar Søk etter skrivar … Det ligg éin utskriftsjobb i køen Det ligg %1 utskriftsjobbar i køen 