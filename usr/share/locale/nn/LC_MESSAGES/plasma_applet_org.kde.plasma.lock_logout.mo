��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7    ;  -   ?  -   m     �  
   �  
   �     �     �     �     �     �  1   �     *     .  ,   D  
   q  
   |  
   �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-11-28 11:45+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Vil du setja maskina i kvilemodus til minnet? Vil du setja maskina i dvalemodus til disken? Generelt Handlingar Dvalemodus Dvalemodus til disken Avslutt Avslutt … Lås Lås skjermen Logg av, avslutt eller start datamaskina på nytt Nei Kvilemodus til minnet Start ei parallell økt som ein annan brukar Kvilemodus Byt brukar Byt brukar Ja 