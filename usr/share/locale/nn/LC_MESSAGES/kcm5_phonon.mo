��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  :   �  y        �  d   �          $  1   1  	   c  1   m     �     �  
   �     �  ,   �  )     +   ,      X     y  X   �     �     �  z         �  4   �     �     �     �          '  
   D     O  	   c     m     u     �     �  &   �     �     �  
   �     �                    .     :     Q  
   ]     h  	   p     z     �     �     �  	   �  �   �     p  D   }  ^   �     !  3   -  .   a  0   �  +   �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-05-16 14:16+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 For å ta i bruk motorendringa må du logga ut og inn att. Ei liste over Phonon-motorar som vart funne på systemet ditt. Rekkjefølgja her er den same som den Phonon brukar dei i. Bruk einingslista på … Bruk den viste lista for føretrekte einingar på desse andre kategoriane for lydavspelingseiningar: Oppsett av lydmaskinvare Lydavspeling Føretrekt avspelingseining for kategorien «%1» Lydopptak Føretrekt lydopptakseining for kategorien «%1» Motor Colin Guthrie Tilkopling © 2006 Matthias Kretz Innstilling for standard lydavspelingseining Innstilling for standard lydopptakseining Innstilling for standard videoopptakseining Standard / uspesifisert kategori Ikkje føretrekk Oppgjev standard rekkjefølgje for einingar som kan overstyrast av einskilde kategoriar. Einingsoppsett Einingsinnstillingar Einingar som finst på systemet ditt og som passar for den valde kategorien. Vel eininga du vil skal brukast av programma. eirbir@gmail.com,karl@huftis.org Klarte ikkje definera den valde lydavspelingseininga Framme i midten Framme venstre Framme til venstre for midten Framme til høgre Framme til høgre for midten Maskinvare Uavhengige einingar Inn-nivå Ugyldig Oppsett av lydmaskinvare Matthias Kretz Mono Eirik U. Birkeland,Karl Ove Hufthammer Oppsettmodul for Phonon Avspeling (%1) Føretrekk Profil Bak i midten Bak til venstre Bak til høgre Opptak (%1) Vis avanserte einingar Til venstre Til høgre Lydkort Lydeining Høgtalarplassering og -testing Basshøgtalar Test Test den valde eininga Testar %1 Rekkjefølgja viser kor føretrekt eininga er. Dersom den første eininga av ein eller annan grunn ikkje kan brukast, vert den andre, tredje og så vidare brukt. Ukjend kanal Bruk den viste lista for føretrekte einingar på fleire kategoriar. Ulike kategoriar for mediebruk. Til kvar kategori kan du velja kva eining som skal få utdata. Videoopptak Føretrekt videoopptakseining for kategorien «%1» Denne motoren støttar kanskje ikkje lydopptak Denne motoren støttar kanskje ikkje videoopptak ingen føretrekkingar for den valde eininga føretrekk den valde eininga 