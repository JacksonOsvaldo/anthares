��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
       �  #  4         U     j  4        �     �     �     �     �               *     8     @      M     n     v  &   �  1   �     �     �     �  $        (     5     >  
   D  .   O  "   ~     �     �     �     �     �     �            1   2  2   d  1   �     �  4   �       	          
   9     D     V  F   d  N   �  X   �  	   S  
   ]  @   h     �                                            ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2018-01-23 21:15+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1 er eit eksternt program og vart starta automatisk © 2009 Ben Cooksley © 2017 Marco Martin <i>Inneheld 1 element</i> <i>Inneheld %1 element</i> Fann ingen søkjetreff Om %1 Om denne modulen Om utforminga Om systemoppsettet Alle innstillingar Bruk innstillingane Opphavsperson Tilbake Ben Cooksley Sentralt oppsettsenter frå KDE. Set opp Set opp datamaskina Inneheld 1 element Inneheld %1 element Vel om detaljerte hjelpebobler skal takast i bruk Utviklar Dialog karl@huftis.org Utvid automatisk det første nivået Mest brukte: Generelt Hjelp Ikonvising intern modulrepresentasjon, intern modulmodell Internnamn på visinga som er bruk Tastatursnarveg: %1 Vedlikehaldar Marco Martin Mathias Soeken Mest brukte Mest brukte modul nummer %1 Karl Ove Hufthammer Fann ingen utformingar Ei kategoribasert ikonvising av kontrollmodulane. Ein kategoribasert sidestolpe med kontrollmodular. Ei klassisk trebasert vising av kontrollmodulane. Start om att %1 Tilbakestill alle endringane til dei førre verdiane Søk Søk … Vis detaljerte hjelpebobler Sidestolpe Sidestolpe-vising Systemoppsett Systemoppsettet fann ingen utformingar, og kan derfor ikkje visa noko. Systemoppsettet fann ingen utformingar, og du kan derfor ikkje setja opp noko. Innstillingane i denne modulen er endra.
Ønskjer du å bruka eller forkasta endringane? Trevising Utforming: Velkommen til systemoppsettet, der du kan setja opp datamaskina. Will Stephenson 