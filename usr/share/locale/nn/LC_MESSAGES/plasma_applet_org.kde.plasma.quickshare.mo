��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �            -   %  M   S     �     �     �     �  	   �     �     �     �     �            %   /     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-10-08 21:30+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 <a href='%1'>%1</a> Lukk Kopier automatisk: Ikkje vis dette vindauget; kopier automatisk. Dra tekst eller bilete hit for å lasta det opp til ei teksttavle på nettet. Feil ved opplasting. Generelt Loggstorleik: Lim inn Vent litt Prøv igjen. Sender … Del Deling av «%1» Ferdig opplasta Adressa vart delt Last opp %1 til teksttavle på nettet 