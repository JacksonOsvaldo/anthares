��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .          $     1  '   =     e     �     �     �  +   �     �  #   �               "     1     8     L     ]     m     �     �  +   �  ,   �     	     	     	  '   &	     N	     f	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-03-05 14:35+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Køyr Alle element Kategoriar: Konsoll for skalskript på skrivebordet Last ned nye skjermelement Skriveprogram Køyrer skript på %1 Filter Installer skjermelement frå lokal fil … Mislukka installering Feil ved installering av pakken %1. Last inn Ny økt Opna skriptfil Utdata Element som køyrer Køyretid: %1 ms Lagra skriptfil Skjermlås er slått på Tid før pauseskjermen startar Vel skjermelement-fil Skjermen vert låst etter så mange minutt. Vel om skjermen skal låsast etter vald tid. Malar KWin Plasma Klarte ikkje lasta skriptfila <b>%1</b> Avinstallerbare element Bruk 