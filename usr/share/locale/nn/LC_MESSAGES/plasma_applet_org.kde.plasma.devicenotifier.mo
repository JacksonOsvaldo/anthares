��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     
  F  <   Q     �     �     �  >   �  #   �  (   	     A	  �   J	  �   �	  %   �
     �
          1     P  (   k     �     �  !   �  $   �                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-20 20:15+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 1 handling for denne eininga %1 handlingar for denne eininga %1 ledig Tek i bruk … Alle einingar Trykk for å få tilgang til denne eininga frå andre program. Trykk for å løysa ut denne plata. Trykk for å fjerna denne eininga trygt. Generelt Det er no <b>ikkje trygt</b> å fjerna denne eininga; det kan vera program som er i ferd med å bruka henne. Trykk på «løys ut»-knappen for å fjerna eininga trygt. Det er no <b>ikkje trygt</b> å fjerna denne eininga; det kan vera program som er i ferd med å bruka andre lagringsområde på henne. Trykk på «løys ut»-knappen på desse andre lagringsområda for å fjerna eininga trygt. Du kan no trygt fjerna denne eininga. Sist tilkopla eining Ingen einingar er tilgjengelege Berre ikkje-flyttbare einingar Set opp flyttbare einingar Vis sprettopp ved tilkopling av einingar Berre flyttbare einingar Fjernar … Denne eininga er no tilgjengeleg. Denne eininga er ikkje tilgjengeleg. 