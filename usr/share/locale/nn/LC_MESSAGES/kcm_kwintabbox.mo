��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �          #     7  
   E     P     ]     e     y     �     �     �  O   �     	  #   	     A	  "   R	     u	  
   {	     �	     �	     �	     �	  	   �	     �	  
   �	     �	  �   �	  [   {
     �
     �
     �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-09-19 14:13+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Aktivitetar Alle andre aktivitetar Alle andre skrivebord Alle andre skjermar Alle vindauge Alternativ Skrivebord 1 Innhald Gjeldande aktivitet Gjeldande program Gjeldande skrivebord Gjeldande skjerm Filtrer vindauge etter Fokusinnstillingane reduserer funksjonaliteten ved navigering gjennom vindauge. Fram Hent nye utforming av vindaugsbytar Gøymde vindauge Ta med ikon for «Vis skrivebord» Hovud Minimering Berre eitt vindauge per program Nyleg brukte Omvendt Skjermar Snarvegar Vis valt vindauge Sortering: Stabling Det gjeldande vindauget vert markert vert å dempa alle dei andre vindauge. Dette fungerer berre om skrivebordeffektane er slått på. Effekten brukt til å byta ut vindaugsoversikta når skrivebordseffektane er tilgjengelege. Virtuelle skrivebord Synlege vindauge Visualisering 