��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     �
     �
  %   �
     �
     �
     �
     	     $  '   8  d   `     �     �     �     �     �                         "     *  
   2     =     A                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-07-15 00:16+0200
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 <p>Du har valt å opna ei ny skrivebordsøkt.<br />Økta som no køyrer, vert gøymd, og eit nytt innloggingsvindauge vert vist.<br />Det er tildelt ein «F»-tast til kvar økt, vanlegvis «F%1» for den første økta, «F%2» for den neste, og så vidare. Du kan byta mellom øktene ved å halda inne «Ctrl» og «Alt» og trykkja den tilhøyrande «F»-tasten samtidig. Du kan òg byta mellom øktene frå KDE-panelet og skrivebordsmenyane.</p> Vis alle økter Lås skjermen Låser økta og startar pauseskjermen Loggar ut av økta som køyrer Ny økt Startar datamaskina på nytt Start datamaskina på nytt Slå av datamaskina Startar ei ny økt som ein annan brukar Byter til den aktive økta for brukaren :q:, eller viser alle dei aktive øktene om :q: ikkje er med Slår av datamaskina økter Åtvaring – ny økt lås logg ut Logg ut loggut ny økt reboot omstart avslutt byt brukar byt byt :q: 