��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �  	   	  	   	     	     !	     1	     A	     J	     S	     g	     s	     |	     �	     �	     �	     �	     �	     �	     �	  )   �	     
     $
     7
     J
     `
     n
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-26 05:50+0100
PO-Revision-Date: 2018-01-27 12:13+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  % Juster lydstyrke for %1 Program Dempa lyd Lydstyrke Åtferd Opptakseiningar Opptaksstraumar Demp lyd Standard Lågare mikrofonlyd Lågare lyd Einingar Generelt Portar Høgare mikrofonlyd Høgare lyd Høgste lydstyrke: Demp lyd Demp %1 Slå av mikrofonen Ingen program spelar av eller tek opp lyd Fann ingen lydeiningar Avspelingseiningar Avspelingsstraumar  (ikkje tilgjengeleg)  (kopla frå) Auk høgste lydstyrke Vis fleire val for %1 Lydstyrke Lydstyrken er %1 % Lydstyrkeannonsering Lydstyrke-steg: %1 (%2) %1: %2 100 % %1 % 