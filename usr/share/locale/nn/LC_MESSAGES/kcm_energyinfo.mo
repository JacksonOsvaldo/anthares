��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     
  	   
     
  
   ,
     7
     =
     D
     H
     U
     ^
     n
     u
     �
  	   �
     �
     �
     �
     �
     �
     �
               #  	   1  	   ;  	   E     O     V     j     n     z     �  	   �     �     �     �     �  
   �  G   �               (     4     6  !   ?     a     c     f     i     w     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-07-20 20:30+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  % %1 %2 %1: Energiforbruk for program Batteri Kapasitet Ladingsprosent Ladestatus Ladar Straum °C Detaljer: %1 Ladar ut karl@huftis.org Energi Energiforbruk Statistikk over energibruk Omgjevnad Full utforming Fullt opplada Har straumforsyning Kai Uwe Broulik Siste 12 timar Site 2 timar Siste 24 timar Siste 48 timar Siste 7 dagar Sist full Site time Produsent Modell Karl Ove Hufthammer Nei Ladar ikkje PID: %1 Adresse: (%1) Oppladbar Oppdater Serienummer W System Temperatur Denne sorten historie er for tida ikkje tilgjengeleg for denne eininga. Tidsrom Vis data for tidsrom Leverandør V Spenning Vekkingar per sekund: %1 (%2 %) W Wh Ja Energiforbruk  % 