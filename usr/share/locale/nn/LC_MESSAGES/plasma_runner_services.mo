��          D      l       �      �   6   �   a   �      /    ?     D  9   L  	   �     �                          Applications Finds applications whose name or description match :q: Jump list search result, %1 is action (eg. open new tab), %2 is application (eg. browser)%1 - %2 System Settings Project-Id-Version: plasma_runner_services
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-14 03:09+0100
PO-Revision-Date: 2016-07-20 20:17+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Program Finn program med namn eller skildringar i samsvar med :q: %1 – %2 Systemoppsett 