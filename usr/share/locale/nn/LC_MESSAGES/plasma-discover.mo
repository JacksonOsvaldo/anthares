��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �       5     "   L  E   o  E   �  >   �     :     O  C   [     �     �  $   �     �          $     2     B     H     ]     e     x     �     �     �     �     �  	   �     �            "   7  B   Z     �     �     �  =   �          "  0   )     Z  !   j     �  
   �  
   �     �  	   �     �     �     �     �  (   �          =  $   I     n     �  	   �     �  M   �  >     	   A  	   K     U     [     p     w  D   �     �     �  	   �     �     �               '  	   A     K     i     q     �  
   �     �  	   �     �     �     �     �               (     6     ?     O     i  )   }         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-10 05:47+0100
PO-Revision-Date: 2018-01-23 21:10+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Òg tilgjengeleg i %1 %1 %1 (%2) %1 (standard) <b>%1:</b> av %2 <em>%1 av %2 syntest denne vurderinga var nyttig</em> <em>Fortel oss om vurderinga!</em> <em>Nyttig? <a href='true'><b>Ja</b></a>/<a href='false'>Nei</a></em> <em>Nyttig? <a href='true'>Ja</a>/<a href='false'><b>Nei</b></a></em> <em>Nyttig? <a href='true'>Ja</a>/<a href='false'>Nei</a></em> Hentar oppdateringar Hentar … Det er ikkje kjent når siste sjekk etter oppdateringar vart gjord. Ser etter oppdateringar Ingen oppdateringar Ingen oppdateringar er tilgjengelege Skal sjå etter oppdateringar Systemet er oppdatert Oppdateringar Oppdaterer … Godta Legg til kjelde … Tillegg Aleix Pol Gonzalez Ein programutforskar Bruk innstillingane Tilgjengelege motorar:
 Tilgjengelege modusar:
 Tilbake Avbryt Kategori: Ser etter oppdateringar … Kommentaren er for lang Kommentaren er for kort Kompakt modus (auto/kompakt/full). Kunne ikkje lukka programmet. Det finst oppgåver som fullførast. Fann ikkje kategorien «%1» Klarte ikkje opna %1 Slett kjelda Opna direkte det oppgjevne programmet etter pakkenamnet sitt. Forkast Oppdag Vis ei liste over oppføringar med ein kategori. Utvidingar … Klarte ikkje fjerna kjelda «%1» Framheva Hjelp … Heimeside: Forbetra samandraget Installer Installerte Jonathan Thomas Start Lisens: Vis alle tilgjengelege bakgrunnsmotorar. Vis alle tilgjengelege modusar. Lastar … Lokal pakkefil som skal installerast Gjer til standard Meir informasjon … Meir … Ingen oppdateringar Opna Discover i vald modus. Modusar samsvarer med knappane på verktøylinja. Opna med eit program som kan handtera den aktuelle MIME-typen. Hald fram Karakter: Fjern Ressursar for «%1» Vurder Vurderer «%1» Å køyra som <em>rotbrukar</em> er unødvendig og sterkt frårådd. Søk Søk i «%1» … Søk … Søk: %1 Søk: %1 + %2 Innstillingar Kort samandrag … Vis vurderingar (%1) … Storleik: Fann dessverre ingenting … Kjelde: Vel ny kjelde for %1 Leitar framleis … Samandrag: Støttar appstream: URL-format Oppgåver Oppgåver (%1 %) %1 %2 Finn ikkje ressurs: %1 Oppdater alle Oppdater valde Oppdater (%1) Oppdateringar Versjon: ukjend vurderar ikkje-valde oppdateringar valde oppdateringar © 2010–2016 Utviklingslaget for Plasma 