��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A    l     s     z     �     �     �     �     �     �  	   �     �       
          
        (     0     8     A     I     P     Y     l     ~     �     �  	   �     �     �     �     �     �     �  	          #        :     H     V  	   c      m     �     �     �     �     �     �     �     �     �     �     �  $   �     	                 D   #     h     �     �     �     �     �     �     �     �  	   �  	   �               #     B  -   P     ~      �     �     �     �     �     �  
   �     �                    !  
   .     9     >     W     `     u     �  O   �     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-01-23 21:11+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Slett &Tøm papirkorga &Flytt til papirkorga &Opna &Lim inn &Eigenskapar &Last skrivebordet om att &Last visinga om att &Oppdater &Endra namn Vel … Fjern ikon Juster Utsjånad: Ordna i Ordna i Ordning: Tilbake Avbryt Kolonnar Set opp skrivebord Sjølvvald tittel Dato Standard Synkande Skildring Fjern merking Skrivebordsoppsett Skriv inn sjølvvald tittel her Funksjonar: Filnamnmønster: Filtype Filtypar: Filter Infobobler for mappeførehandvising Mapper først Mapper først Full adresse Forstått Gøym filer i samsvar med søket Enorm Ikonstorleik Ikon Stor Venstre Liste Adresse Stad: Lås på plass Låst Middels Fleire val for førehandsvising … Namn Ingen OK Panelknapp: Trykk og hald inne skjermelementa for å flytta dei og visa handtaka Tillegg for førehandsvising Minibilete for førehandvising Fjern Endra storleik Gjenopprett Høgre Drei Rader Søk etter filtype … Merk alle Vel mappe Utvalsmarkørar Vis alle filene Vis filer i samsvar med søket Vis ein stad: Vis filer kopla til den gjeldande aktiviteten Vis skrivebordsmappa Vis verktøylinja for skrivebord Storleik Liten Liten/middels Sorter etter Sorter etter Sortering: Vel ei mappe: Tekstlinjer Ørliten Tittel: Hjelpebobler Finpussing Type Skriv inn ei adresse her Usortert Bruk sjølvvalt ikon Handtering av skjermelement Skjermelement låste opp Du kan trykkja og halda inne skjermelementa for å flytta dei og visa handtaka. Visingsmodus 