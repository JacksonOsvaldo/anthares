��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  '   �     �               2     G     a     m     �  -   �      �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2018-01-22 21:18+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 *.kwinscript|KWin-skript (*.kwinscript) Set opp KWin-skript karl@huftis.org Hent nye skript … Importer KWin-skript Importer KWin-skript … KWin-skript Opsett av KWin-skript Karl Ove Hufthammer Klarte ikkje importera det valde skriptet.
%1 Skriptet «%1» er no importert. Tamás Krutki 