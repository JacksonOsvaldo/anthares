��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1          3     G     V  	   g  	   q     {  
   �     �  0   �     �     �  	   �     �           ;     Y     q  *   x     �  %   �  C   �  <   (	  1   e	  1   �	     �	     �	     �	     �	                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2018-01-25 20:53+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Legg til filer … Legg til mappe … Bakgrunnsramme Byt bilete etter Vel mappe Vel filer Set opp skjermelement … Fyllmodus: Generelt Venstreklikk for å opna i eksternt biletprogram Legg luft rundt Adresser Adresser: Pause når peikaren er over Hald fast høgd/lengd-avskjering Hald fast høgd/lengd-forhold Tilfeldig rekkjefølgje Strekk Biletet vert gjenteke vassrett og loddrett Biletet vert ikkje skalert Biletet vert skalert for å få plass Biletet vert skalert til å få plass, med avskjering om nødvendig Biletet vert skalert likt i begge retningar, utan avskjering Biletet vert strekt vassrett og gjenteke loddrett Biletet vert strekt loddrett og gjenteke vassrett Jamsides Jamsides vassrett Jamsides loddrett s 