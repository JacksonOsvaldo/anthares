��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  �  �     �  3   �  2   �  3        6  A   U  %   �     �     �     �                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-25 20:54+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: NorwegianNynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 %1, %2 1 pakke skal oppdaterast %1 pakkar skal oppdaterast 1 tryggleiksoppdatering %1 tryggleiksoppdateringar 1 pakke skal oppdaterast %1 pakkar skal oppdaterast Ingen pakkar treng oppdatering og 1 er ei tryggleiksoppdatering og %1 er tryggleiksoppdateringar Tryggleiksoppdateringar tilgjengelege Systemet er oppdatert Oppdater Oppdateringar tilgjengelege 