��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     �     �     �     �     �     �               8     M  $   i  #   �  :   �  ;   �     )	     E	     d	     	     �	  0   �	     �	  +   �	     %
     ,
     5
  
   C
  	   N
     X
  
   x
     �
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2016-11-19 19:09+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
  ms &Vising av tastatursnarvegar: &Knapptype for øvre pil: Gøym alltid tastatursnarvegar Vis alltid tastatursnarvegar &Animasjonslengd: Animasjonar K&nappetype for nedre pil: Breeze-innstillingar Sentrer faner på fanelinje Dra vindauge frå alle tomme område Berre dra vindauge frå tittellinja Dra vindauge frå tittellinja, menylinja og verktøylinjer Vis ei tynn ramme som fokusindikator i menyar og menylinjer Teikn fokusmarkør i lister Vis ramme rundt dokkbare panel Vis ramme rundt sidetitlar Vis ramme rundt sidepanel Teikn merke på glidebrytarar Vis skiljemerke mellom element på verktøylinja Bruk animasjonar Bruk utvida handtak for endring av storleik Rammer Generelt Ingen knappar Éin knapp Rullefelt Vis tastatursnarvegar ved behov To knappar &Dra-modus for vindauge: 