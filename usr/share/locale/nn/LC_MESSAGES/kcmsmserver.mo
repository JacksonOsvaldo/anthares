��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     l	     �	     �	  �   �	  �  �
  .   Y  k   �     �            t   #    �     �     �     �     �                                
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-06-19 22:25+0200
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Avslutt &denne økta Start &maskina om att &Slå av maskina <h1>Økthandsamar</h1>Du kan setja opp økthandsamaren her. Du kan velja om du skal stadfesta avslutting (utlogging), om den førre økta skal gjenopprettast neste gong du loggar inn og om datamaskina automatisk skal slåast av etter utlogging. <ul><li><b>Gjenopprett førre økt:</b> Lagrar alle programma som køyrer når du avsluttar, og startar dei att neste gong du loggar inn.</li> <li><b>Gjenopprett manuelt lagra økt:</b> Gjer slik at økta kan lagrast når som helst med «Lagra økt» i K-menyen. Då vert dei programma som køyrer no starta automatisk neste gong.</li> <li><b>Start med tom økt:</b> Lagrar ingenting, slik at skrivebordet vert heilt tomt neste gong.</li></ul> Program som &ikkje skal reknast med i øktene: Kryss av her om du vil at økthandsamaren skal visa ein dialogboks der du kan stadfesta at du vil logga ut. Stadfest ut&logging Standard lukkeval Generelt Her kan du velja kva som skal skje når du loggar ut. Innstillinga fungerer berre når du har logga inn gjennom KDM. Her kan du skriva inn ei liste over program som ikkje skal lagrast med øktene, slik at dei ikkje vert starta når ei økt vert gjenoppretta. Du kan oppgje fleire program med kolon eller komma mellom, som for eksempel «xterm:xconsole» eller «xterm,konsole». &Tilby val for avslutning Ved innlogging Gjenopprett &manuelt lagra økt Gjenopprett &førre økt Start med &tom økt 