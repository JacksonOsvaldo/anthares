��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b    �     �     �  &   �     �     �  $     	   5  
   ?  $   J     o     �     �  5   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2018-01-22 21:08+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Set opp skrivebordstema David Rosca karl@huftis.org,korsvoll@skulelinux.no Hent nye tema … Installer frå fil … Karl Ove Hufthammer,Håvard Korsvoll Opna tema Fjern tema Temafiler (*.zip *.tar.gz *.tar.bz2) Klarte ikkje installera temaet. Temaet er no installert. Feil ved fjerning av tema. Med denne modulen kan du setja opp skrivebordstemaet. 