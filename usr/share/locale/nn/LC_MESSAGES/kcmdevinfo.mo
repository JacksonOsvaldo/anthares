��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  %        =  	   R     \     e     �     �     �     �     �     �     �     �     �  	   �     �  $        &     3     <     E     U     ^  	   k     u     �     �     �     �  	   �  '   �  	   �  	   �  
   �  
     
             "     1  
   =     H     X     f     j     x     �     �     �     �  
   �     �     �     �     �     �            	   $     .  	   3     =     B     G     Y     k     �     �     �     �     �     �     �     �     �  9   �     9     H     O     [     u     �  
   �     �     �     �  	   �     �     �     �     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-11-13 01:20+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 
Solid-basert modul for einingsvising © 2010 David Hubner AMD 3DNow ATI IVEC %1 ledig av %2  (%3 % brukt) Batteri Batteritype:  Buss:  Kamera Kamera Ladestatus:  Ladar Fald saman alle CompactFlash-lesar Ei eining Einingsinformasjon Viser alle einingar som er lista no. Einingsvisar Einingar Ladar ut karl@huftis.org Kryptert Fald ut alle Filsystem Filsystemtype:  Ferdig ladd Harddisk Hotplug-kompatibel: IDE IEEE 1394 Viser informasjon om den valde eininga. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Tastatur Tastatur + mus Merkelapp:  Maksfart:  Minnepinnelesar Montert på:  Mus Mediespelarar Karl Ove Hufthammer Nei Inga lading Ingen data tilgjengeleg Ikkje montert Ikkje valt Optisk stasjon PDA Partisjonstabell Primær Prosessor %1 Prosessornummer:  Prosessorar Produkt:  RAID Flyttbar? SATA SCSI SD-/MMC-kortlesar Vis alle einingar Vis relevante einingar Smart media-lesar Lagringsstasjonar Støtta drivarar:  Støtta instruksjonssett:  Støtta protokollar:  UDI:  UPS USB UUID:  Viser UDI (Unique Device Identifier) for gjeldande eining Ukjent stasjon Ubrukt Produsent:  Namn på lagringsområde: Bruk av lagringsområde:  Ja kcmdevinfo Ukjend Ingen Ingen Plattform Ukjend Ukjend Ukjend Ukjend Ukjend xD-lesar 