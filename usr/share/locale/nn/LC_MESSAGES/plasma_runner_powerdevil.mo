��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  �  �     �     �  �     B   �     �  
   �     �  
   �  
     
     
        )     :     T     i     z      �      �                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: KDE 4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2010-01-19 21:55+0100
Last-Translator: Eirik U. Birkeland <eirbir@gmail.com>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Ton skjermen halvvegs ned Ton skjermen heilt ned Gjev oversikt over lysstyrkeval for skjermen, eller set lysstyrken til :q: (eksempelvis vil verdien 50 gje 50 % av maks lysstyrke) Gjev oversikt over vente- og dvalemodus, og lèt deg ta dei i bruk demp skjermen kvilemodus lysstyrke på skjermen dvalemodus kvilemodus til disken til minnet demp skjermen %1 lysstyrke på skjermen %1 Vel %1 som lysstryke Dvale til disken Dvale til minnet Legg systemet i dvale til minnet Legg systemet i dvale til disken 