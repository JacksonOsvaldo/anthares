��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     	     	     )	     7	     S	  &   m	  "   �	  !   �	     �	  
   �	     �	     �	     �	  F   
     M
  N   e
     �
     �
     �
     �
     �
     �
     �
       
     	   '     1     6     L  (   _  A   �     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2018-01-22 21:15+0100
Last-Translator: Karl Ove Hufthammer <karl@huftis.org>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 100 % Opphavsperson © 2015 Harald Sitter Harald Sitter Ingen program spelar av lyd Ingen program tek opp lyd Ingen einingsprofilar er tilgjengelege Ingen inneiningar er tilgjengelege Ingen uteiningar er tilgjengelege Profil: PulseAudio Avansert Program Einingar Legg til virtuell lydeining for samtidig lyd på alle tilkopla lydkort Avansert utdata-oppsett Flytt automatisk alle køyrande straumar når ei ny uteining vert tilgjengeleg Opptak Standard Einingsprofilar karl@huftis.org Inneiningar Demp lyd Karl Ove Hufthammer Varslingslydar Uteiningar Avspeling Port  (ikkje tilgjengeleg)  (ikkje kopla til) Krev PulseAudio-modulen «module-gconf» Denne modulen vert brukt til å setja opp lydsystemet PulseAudio. %1: %2 100 % %1 % 