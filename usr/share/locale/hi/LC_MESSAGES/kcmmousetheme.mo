��          �      <      �     �  �   �  `   R     �     �     �     �     �           )     :     Y     ^     o  ?   |  Y   �  +     �  B     �     �  �          D      %   e     �  _   �     �     	     #	  
   @	  6   K	  "   �	  �   �	  �   I
  c   	                                                                
                             	        (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-29 03:20+0530
Last-Translator: G Karunakar
Language-Team: Hindi <en@li.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 (c) 2003-2007 Fredrik Höglund <qt>क्या आप वाक़ई संकेतक प्रसंग<i>%1</i> मिटाना चाहते हैं?<br />यह इस प्रसंग द्वारा संस्थापित सभी फ़ाइलों को मिटा देगा.</qt> एक प्रसंग %1 नाम से प्रतीक प्रसंग फ़ोल्डर में पहले से ही उपलब्ध है. क्या आप उसे इससे बदलना चाहते हैं? पुष्टिकरण संकेतक विन्यास परिवर्तित संकेतक प्रसंग वर्णन प्रसंग यूआरएल खींचलाएँ या टाइप करें karunakar@indlinux.org Fredrik Höglund जी करूणाकर नाम  प्रसंग मिटाकर लिखें? प्रसंग हटाएँ फ़ाइल %1 वैध संकेतक प्रसंग अभिलेखागार जैसा प्रतीत नहीं होता है. संकेतक प्रसंग अभिलेख डाउनलोड करने में अक्षम; कृपया जाँचें कि पता %1 सही है. संकेतक प्रसंग अभिलेख %1 पाने में अक्षम. 