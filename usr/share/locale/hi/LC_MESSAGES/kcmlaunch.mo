��          �      �       0     1     6  ^  S  P   �               #     0     M     \     p     �  �  �  
   e  M   p    �  �   �	  .   �
  "   �
  %   �
  <     (   I  9   r  M   �  7   �                                      
   	                  sec &Startup indication timeout: <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-10-23 11:04+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@yahoo.com>
Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 सेक. स्टार्टअप इंडीकेशन टाइमआउटः व्यस्त संकेतक<h1>Busy Cursor</h1>
केडीई प्रस्तुत करता है एक व्यस्त संकेतक- अनुप्रयोग प्रारंभ करने के बारे में बताने के लिए.
 व्यस्त संकेतक सक्षम करने के लिए एक प्रकार का विज़ुअल फ़ीडबैक
कॉम्बो बक्से में से चुनें.
ऐसा हो सकता है कि कुछ अनुप्रयोग इस विशेषता- प्रारंभ करने के बारे में बताने के लिए- अनभिज्ञ हों
. ऐसी परिस्थिति में, कर्सर टिमटिमाना बन्द कर देता है
 'स्टार्टअप इंडीकेशन टाइमआउट' में दिए गए समय के उपरांत. <h1>लांच फ़ीडबैक</h1> यहाँ आप एप्लीकेशन-लांच फ़ीडबैक कॉन्फ़िगरेशन कर सकते हैं. टिमटिमाता संकेतक उछलता संकेतक व्यस्त संकेतक कार्य पट्टी सक्षम करें अव्यस्त संकेतक अक्रिय व्यस्त संकेतक  स्टार्टअप इंडीकेशन टाइमआउटः कार्यपट्टी अधिसूचना 