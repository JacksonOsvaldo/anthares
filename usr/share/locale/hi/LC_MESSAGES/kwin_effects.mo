��    k      t  �   �       	     !	     $	     *	     9	     =	     A	     I	     R	  	   `	     j	  	   s	     }	  
   �	     �	     �	  5   �	  %   �	     
     $
  
   4
     ?
     R
     [
  
   b
     m
     �
  	   �
  
   �
     �
     �
     �
     �
     �
  
   �
     	               +     A     S     d     k      x     �     �     �     �      �       D        P     `     }     �  
   �  
   �     �     �     �     �     �     �     �     �                         '  	   ,     6     >     F     M     S     Y  
   f     q     w     �     �     �     �  	   �     �     �     �     �     �             
        #     2  #   G     k     o  	   x     �     �     �     �  
   �     �     �     �  �  �     �     �  +   �     �     �     �     �       '   (     P     o          �     �     �     �     �       /   "     R     n     �     �  	   �  [   �  [        s     �     �      �      �      �  \        u     �     �  =   �  C   �  3   0  2   d     �     �     �     �  5   �  3   !  @   U  U   �     �  �     2   �     �  	             ,     H  &   b  #   �  )   �     �     �     �       +   #     O     c     }     �  	   �  )   �     �  !   �          2     ?  #   L      p     �     �     �  W   �  !   4  ;   V  "   �  +   �     �     �     �  	             +     B     b  ?   �  c   �  	   $     .     L     j  *   �  *   �     �  %   �     "  3   8     l     K          9           b       
   	              (      G      [         Y      1   P   f   H       A       =       :       7   5   )   #       a       !   $   N   V   S   ;              C      Z   D   -      W       L                              +   ,      I   R   T           >         `   g   O   F       J   c   X   8       .   E           k   d   /       \   3   2       j   _                    ?       4   e                    ^   *   "         ]      Q             '      <   %   h       @           U   0          6   B   &           i   M                %  msec  pixel  pixels  px  ° &Color: &Height: &Layout mode: &Opacity: &Radius: &Spacing: &Stiffness: &Strength: &Width: &Wobbliness @title:group actions when clicking on desktopDesktop @title:tab Advanced SettingsAdvanced @title:tab Basic SettingsBasic Activate window Activation Additional Options Advanced Angle: Appearance Apply effect to &groups Apply effect to &panels Automatic Background Bottom Bottom Left Bottom Right Bottom-Left Bring window to current desktop Cap color: Caps Center Clear All Mouse Marks Clear Last Mouse Mark Clear Mouse Marks Combobox popups: Custom Desktop Cube Desktop name alignment:Disabled Dialogs: Display desktop name Display image on caps Display window &icons Do not change opacity of windows Dra&g: Draw with the mouse by holding Shift+Meta keys and moving the mouse. Dropdown menus: Duration of rotationDefault Far Faster Fill &gaps Filter:
%1 Inactive windows: Inside Graph Invert cursor keys Layout mode: Left Left button: Less Maximum &width: Menus: Middle button: More Natural Near No action Nowhere Opacity Opaque Pager Plane Popup menus: Reflection Right Right button: Rotation duration: Send window to all desktops Show &panels Show Desktop Grid Show caps Show desktop Size Tab 1 Tab 2 Text Text alpha: Text color: Text font: Text position: Toggle Invert Effect Toggle Thumbnail for Current Window Top Top Left Top Right Top-Left Torn-off menus: Translucency Transparent Wallpaper: Windows Windows hover above cube Zoom Project-Id-Version: kwin_effects
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-02 03:18+0100
PO-Revision-Date: 2010-01-31 20:51+0530
Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>
Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);

  % मि.से. पिक्सेल पिक्सेल px  ° रंग: (&C) ऊंचाईः (&H) खाका मोड (&L) अपारदर्शिता: (&O) त्रिज्या: (&R) जगह: (&S) जडता (&S) सामर्थ्य (&S): चौड़ाईः (&W) डोलना (&W) डेस्कटॉप विस्तृत मूलभूत विंडो सक्रिय करें सक्रियकरण अधिक विकल्प विस्तृत कोणः रूप प्रभावों को समूहों में लागू करें (&g) प्रभावों को पैनलों में लागू करें (&p) स्वचालित पृष्ठभूमि नीचे निचला बायाँ  निचला दायाँ  निचला बायाँ  विंडो को वर्तमान डेस्कटॉप में लाएँ कैप रंग: कैप्स बीच में सभी माउस चिह्न साफ करें आखिरी माउस चिह्न साफ करें माउस चिह्न साफ करें कॉम्बोबाक्स पॉपअप: मनपसंद डेस्कटॉप घन अक्षम संवाद: डेस्कटॉप नाम दिखाएँ कैप्स पर छवि दिखाएँ विंडो संप्रतीक दिखाएँ (&i) विंडो की अपारदर्शिता नहीं बदलें खीचें (&g) शिफ्ट + मेटा कुंजी को दबाए रख कर तथा माउस को खिसकाकर आप ड्राइंग कर सकते हैं ड्रापडाउन मेन्यूः  तयशुदा दूर ओर तेजी से जगह भरें (&g) फ़िल्टर:
%1 असक्रिय विंडो: ग्राफ के भीतर उलट कर्सर कुंजी खाका मोड: बायाँ बायाँ बटनः कम अधिकतम चौड़ाई (&W): मेन्यू: मध्य बटनः अधिक प्राकृतिक पास कोई क्रिया नहीं कहीं नहीं अपारदर्शिता अपारदर्शी पेजर समतल पॉपअप मेन्यू: प्रतिबिंब (&R) दायाँ दायाँ बटनः घूर्ण अवधि: विंडो को सभी डेस्कटॉप में ले जाएँ पटल दिखाएँ (&p) डेस्कटॉप ग्रिड दिखाएँ कैप्स दिखाएँ डेस्कटॉप दिखाएँ आकार टैब 1 टैब 2 पाठ पाठ अल्फा: पाठ रंगः पाठ फ़ॉन्टः पाठ स्थिति: इनवर्ट प्रभाव टॉगल करें मौज़ूदा विंडो के लिए लघुछवि टॉगल करें ऊपर ऊपरी बायाँ  ऊपरी दायाँ  ऊपरी बांया टॉर्न-ऑफ मेन्यू: अल्पपारदर्शिता पारदर्शिता स्क्रीन चित्र विंडोज़ घन पर मंडराते विंडो बडा करें 