��    b      ,  �   <      H  
   I     T     ]     x     �  
   �     �  	   �     �     �  "   �     �     	     	     	     8	  	   H	     R	     Y	     a	     j	  +   q	     �	     �	     �	     �	     �	     �	      �	     �	     
     
     /
  O   7
     �
     �
     �
     �
     �
     �
  !   �
      �
               /     ;     G     K     W  
   e     p     ~     �     �     �  
   �  
   �     �     �               !     /     >     L     S     f  -   o     �     �     �     �  &   �     �     �                    ,  :   <     w     ~     �     �     �     �     �  *   �     �     �  .   
  B   9     |     �     �     �  -   �  �  �  .   �     �  F   �  0   E  .   v     �     �     �  '   �       P   '     x  0   �  "   �  E   �  0   #  #   T     x     �     �     �  u   �     R     k     ~     �  2   �  "   �          !  8   4  ^   m     �  �   �  %   �  	   �     �  C   �     1  ]   >  S   �  P   �     A  3   R     �     �     �     �  $   �       '   1     Y  a   i  ;   �  ;     7   C     {     �  S   �  #   
     .  %   D  &   j  "   �     �  G   �       �   )     �  %   �     �     �  p     6   �  $   �     �  +   �  0   $  7   U  r   �             )   4  )   ^     �      �     �  r   �  (   K   (   t   �   �   �   !  '   �!  (   �!  9   �!  (   3"  v   \"     3   I   F               Q   $   9   K       D                  Z       :   S   ?   7       !   N                  H   8       T       <   "   .                  U   a   	          5   -   #      \   R   B          1   *      M          ^      E   0   W          
   &   Y   2                 4   _   >         G   ,             +   %   ]          C              P   6   ;       )                 (      L           b   `   J   [   /      V   X      =      O         '   @                 A    &Closeable &Desktop &Focus stealing prevention &Fullscreen &Machine (hostname): &Modify... &New... &Position &Single Shortcut &Size (c) 2004 KWin and KControl Authors 0123456789-+,xX: Accept &focus All Desktops Application settings for %1 Apply Initially Apply Now C&lear Cascade Centered Class: Consult the documentation for more details. Default Delete Desktop Dialog Window Do Not Affect Dock (panel) EMAIL OF TRANSLATORSYour emails Edit Edit Shortcut Edit Window-Specific Settings Edit... Enable this checkbox to alter this window property for the specified window(s). Exact Match Extreme Force Force Temporarily High Ignore requested &geometry Information About Selected Window Internal setting for remembering KWin KWin helper utility Keep &above Keep &below Low Lubos Lunak M&aximum size M&inimized M&inimum size Machine: Match w&hole window class Maximized &horizontally Maximized &vertically Maximizing Move &Down Move &Up NAME OF TRANSLATORSYour names No Placement Normal Normal Window On Main Window Override Type Random Regular Expression Remember Remember settings separately for every window Role: Settings for %1 Sh&aded Shortcut Show internal settings for remembering Skip &taskbar Skip pa&ger Smart Splash Screen Standalone Menubar Substring Match This helper utility is not supposed to be called directly. Title: Toolbar Top-Left Corner Torn-Off Menu Type: Under Mouse Unimportant Unknown - will be treated as Normal Window Unnamed entry Utility Window WId of the window for special window settings. Whether the settings should affect all windows of the application. Window &type Window &types: Window settings for %1 Window t&itle: Window-Specific Settings Configuration Module Project-Id-Version: kcmkwinrules
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2007-10-22 15:31+0530
Last-Translator: Ravishankar Shrivastava <raviratlami@yahoo.com>
Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
 बन्द करने योग्य (&C) डेस्कटॉप (&D) फोकस स्टीलिंग प्रिवेंशन (&F) संपूर्ण स्क्रीन (&F) मशीन (होस्ट-नाम): (&M) सुधारें... (&M) नया... (&N) स्थिति (&P) एकल शॉर्टकट... (&S) आकार (&S) (c) 2004 के-विन तथा के-कन्ट्रोल लेखक 0123456789-+,xX: फ़ोकस स्वीकारें (&f) सभी डेस्कटॉप %1 के लिए अनुप्रयोग विन्यास आरंभ में लागू करें अभी लागू करें साफ करें (&l) एक के बाद एक केंद्रित वर्गः अतिरिक्त विवरणों के लिए दस्तावेजों को देखें डिफ़ॉल्ट मिटाएँ डेस्कटॉप संवाद विंडो प्रभावित नहीं करें डाक करें (फलक) raviratlami@aol.in, संपादन शॉर्टकट संपादित करें विंडो-विशिष्ट विन्यास संपादित करें संपादन... उल्लेखित विंडो के विंडो-गुणों को बदलने के लिए इस चेक-बक्से को सक्षम करें. सटीक जोड़ीदार चरम बाध्य करें अस्थायी रूप से बाध्य करें उच्च निवेदित ज्यामिति की उपेक्षा करें (&g) चयनित विंडो के बारे में जानकारी याद रखने के लिए आंतरिक विन्यास के-विन के-विन मदद यूटिलिटी ऊपर रखें (&A) नीचे रखें (&B) निम्न लुबॉस लुनाक अधिकतम आकार (&a) न्यूनतम (&i) न्यूनतम आकार (&i) मशीनः संपूर्ण विंडो वर्ग की जोड़ी मिलाएँ (&h) आड़े में अधिकतम करें (&h) खड़े में अधिकतम करें (&v) अधिकतम किया जा रहा है नीचे जाएं (&D) ऊपर जाएं (&U) रविशंकर श्रीवास्तव, जी. करूणाकर कोई स्थल नहीं सामान्य सामान्य विंडो मुख्य विंडो पर ओवरराइड टाइप बेतरतीब नियमित (रेगुलर) एक्सप्रेशन. याद रखें प्रत्येक विंडो के लिए विन्यासों को अलग-अलग याद रखें भूमिकाः %1 हेतु विन्यास छाया (&a) शॉर्टकट याद रखने के लिए आंतरिक विन्यासों को दिखाएँ कार्यपट्टी छोड़ें (&t) पेजर छोड़ें (&g) स्मार्ट स्प्लैश स्क्रीन अलग-थलग मेनू-पट्टी सबस्ट्रिंग जोड़ीदार यह मदद यूटिलिटी सीधे ही बुलाया नहीं जा सकता. शीर्षक: औज़ार पट्टी ऊपरी बायाँ कोना टॉर्न-ऑफ मेन्यू क़िस्मः माउस के नीचे महत्वहीन अज्ञात - सामान्य विंडो के रूप में माना जाएगा अनाम प्रविष्टि यूटिलिटी विंडो विशिष्ट विंडो सेटिंग के लिए विंडो का डबल्यूआईडी  सेटिंग क्या अनुप्रयोग के सभी विंडो पर प्रभाव डालेगा विंडो प्रकार (&t) विंडो प्रकार (&t): %1 के लिए विंडो विन्यास विंडो शीर्षक (&i): विंडो-विशिष्ट विन्यास कॉन्फ़िगरेशन मॉड्यूल 