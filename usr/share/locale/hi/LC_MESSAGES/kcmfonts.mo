��    "      ,  /   <      �     �  L   �  /   K     {     �     �     �     �     �     �     �       I        i  *   m     �     �     �  !   �  "        &  6   C  *   z     �     �     �     �     �     �     
          5     E  �  `     �  �   �  {   �  J   8	     �	  �   �	  j   
     �
  ,   �
  D   �
       I     �   b     +  i   >  S   �     �       %     �   B  f   �  �   2  �   �     �     �  (   �     �               #  "   C     f     }               
                                                       	                                                                          !          "     to  <p>Some changes such as DPI will only affect newly started applications.</p> A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Font Settings Changed Font role%1:  Force fonts DPI: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Smallest font that is still readable well. Use a&nti-aliasing: Use anti-aliasingDisabled Use anti-aliasingEnabled Use anti-aliasingSystem Settings Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB font usageFixed width font usageGeneral font usageMenu font usageSmall font usageToolbar font usageWindow title no hintingNone no subpixel renderingNone Project-Id-Version: kcmfonts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2010-12-29 03:07+0530
Last-Translator: G Karunakar
Language-Team: Hindi <en@li.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n!=1);
  to  <p>कुछ परिवर्तन जैसे कि डीपीआई सिर्फ नए प्रारंभ किए अनुप्रयोगों में ही लागू होगा</p> एक अ-समानुपातिक फ़ॉन्ट (जैसे टाइपराइटर फ़ॉन्ट). सभी फ़ॉन्ट्स समंजित करें... (&j) बीजीआर सभी फ़ॉन्ट्स को परिवर्तित करने के लिए क्लिक करें एन्टी अलियासिंग विन्यास कॉन्फ़िगर करें कॉन्फ़िगर... रेंज बाहर करें: (&x) फ़ॉन्ट विन्यास परिवर्तित %1:  फ़ॉन्ट्स डीपीआई बाध्य करें: हिन्टिंग एक प्रक्रिया है जो कि छोटे आकारों के फ़ॉन्ट्स के गुणों को उभारता है आरजीबी सबसे छोटा फ़ॉन्ट जो फिर भी पढा जा सकता है एंटी-अलियासिंग इस्तेमाल करें (&n) अक्षम सक्षम तंत्र विन्यास मेन्यू बार तथा पॉपअप मेन्यू द्वारा उपयोग में आता है. विंडो शीर्षक पट्टी के द्वारा उपयोग में सामान्य पाठ के लिए उपयोग में (जैसे बटन लेबल, वस्तु सूची). औज़ार पट्टी प्रतीक के बाज़ू से दिखाए जाने वाले पाठ के लिए उपयोग में आता है. खड़ा बीजीआर खड़ा आरजीबी निश्चित चौड़ाई सामान्य मेन्यू छोटा औज़ार पट्टी विंडो शीर्षक कुछ नहीं कुछ नहीं 