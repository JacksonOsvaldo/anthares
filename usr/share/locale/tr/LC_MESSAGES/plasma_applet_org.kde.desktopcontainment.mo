��    R      �  m   <      �     �     �     
               &     2     C     Q     Y  /   a  -   �     �  
   �     �     �     �     �     �            
             (     7     O     b     n     u     �     �  	   �     �     �     �  	   �     �     �     �     �     �     �     	     	     	     .	     3	     8	  <   ;	     x	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     $
     8
  )   F
     p
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                  E   1  �  w               3     J     O     \     i          �     �     �     �     �     �     �     �  	   �     �          &     ,     9     @     ]  (   s     �     �     �  &   �     �                           ;     B     O     X     `     d     j     p     �     �  "   �     �     �     �  c   �     .     B     ]     e     z     �     �  	   �     �     �     �     �     �            .   1     `      |     �     �     �     �  
   �     �     �     �     �  !   �          /     ?  \   X                J   :   R   .      E       M      B       O   ?                3   #       7       8          L   )         9   I   F   ;   *      2   5              N          C       "   K   
      +            Q      %                    	       4                 ,   H   '   G           >   <   =          (   -           1      D   /       @   0   &   A          !   $   6           P                     &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-06-13 16:58+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Sil Çö&p Kutusunu Boşalt &Çöp Kutusuna Taşı &Aç &Yapıştır &Özellikler Masaüstünü &Yenile &Görünümü Tazele &Yeniden Yükle Ye&niden adlandır Seç... Simgeyi Temizle Hizala Şurada Sırala Geri İptal Sütunlar Masaüstünü Yapılandır Özel başlık Tarih Öntanımlı Azalan Tümünün Seçimini Kaldır Masaüstü Yerleşimi Buraya özelleştirilmiş başlık girin Dosya adı kalıbı: Dosya türleri: Süzgeç Klasör önizleme açılan pencereleri Önce Dizinler Önce dizinler Tam yol Anladım Eşleşen Dosyaları Gizle Devasa Simge Boyutu Simgeler Büyük Sol Liste Konum Olduğu yere kilitle Kilitli Orta Daha Fazla Önizleme Seçeneği... İsim Hiçbiri Tamam Gereçleri hareket ettirmek ve tutamaçlarını ortaya çıkarmak için üzerlerine basın ve tutun Eklentileri Önizle Küçük resimleri önizle Kaldır Yeniden Boyutlandır Geri Yükle Sağ Döndür Satırlar Dosya türü ara... Tümünü Seç Klasör Seçin Seçim işaretleyicileri Tüm Dosyaları Göster Eşleşen Dosyaları Göster Bir konum göster: Geçerli etkinliğe bağlı dosyaları göster Masaüstü dizinini göster Masaüstü araçlarını göster Boyut Küçük Küçük Buna Göre Sırala Sıralama: Bir dizin belirt: Ufak İnce Ayarlar Tür Buraya bir yol ya da adres yazın Sıralanmamış Gereç İşleme Gereçler kilitli değil Gereçlerin tutamaçlarını ortaya çıkarmak için onlara basıp hareket ettirebilirsiniz. 