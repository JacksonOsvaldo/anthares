��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  	   �     �     �     �  
   �            
   .  z   9     �  9   �               #     ,     3     9     K     \     o     �     �      �     �  #   �     	     5	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-10-04 12:11+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Seçin... Simgeyi Sil Beğenilenlere Ekle Tüm Uygulamalar Görünüm Uygulamalar Uygulamalar güncellendi. Bilgisayar Sekmeleri göstermek/gizlemek için kutular arasında sürükleyin ya da görünen sekmeleri sürükleyerek sıralamayın. Uygulamaları Düzenle... Aramayı yer imlerine, dosyalara ve e-postalara genişlet Yer İmleri Gizli Sekmeler Geçmiş Simge: Çık Menü Düğmeleri Sık Kullanılan Tüm Etkinliklerde Sadece Geçerli Etkinlikte Yer İmlerinden Kaldır Sık Kullanılanlarda Göster Uygulamaları isme göre göster Alfabetik Olarak Sırala Üzerine gelince sekmeyi değiştir Aramak için yazın... Görünür Sekmeler 