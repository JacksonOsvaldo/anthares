��          �      |      �     �     �               2     B     O     e  )   m     �     �     �     �  3   �     �  8     8   R     �     �     �     �  �  �     X     k     {     �     �     �     �     �  =   �     ,  "   3     V  	   m  -   w     �  ?   �  ?        A     H     U     b           
                                         	                                             Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... General Left click image opens in external viewer Paths Pause on mouseover Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: plasma_applet_org.kde.plasma.mediaframe
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-10-23 11:24+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-i18n-doc@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Dosyaları ekle... Klasör ekle... Arkaplan çerçevesi Resmi değiştirme aralığı Bir klasör seç Dosyaları seç Plasmoid yapılandır... Genel Resime sol tıklamak, onu harici bir görüntüleyicide açar Yollar Fare üzerine geldiğinde duraklat Ögeleri rastgele seç Genişlet Resim, yatay ve dikey olarak çoğaltılmış Resim dönüştürülmemiş Resim, yatay olarak genişletilmiş ve dikey olarak döşenmiş Resim, dikey olarak genişletilmiş ve yatay olarak döşenmiş Döşe Yatay döşe Dikey döşe s 