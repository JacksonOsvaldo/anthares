��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �  "   C     f     r     �     �     �     �     �  *   �               7  =   T         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-23 11:23+0000
Last-Translator: İşbaran <isbaran@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Masaüstü Temasını Yapılandır David Rosca tulliana@gmail.com Yeni Temaları Getir... Dosyadan kur... Serdar Soytetir Tema Aç Temayı Kaldır Tema Dosyaları (*.zip *.tar.gz *.tar.bz2) Tema kurulumu başarısız. Tema başarıyla kuruldu. Tema kaldırma başarısız. Bu modül masaüstü temanızı yapılandırmanızı sağlar. 