��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     j	     v	  "   �	     �	  	   �	     �	     �	     �	     �	     
     
     &
     7
  6   I
  2   �
     �
     �
     �
  C   �
     1     I     Y     h     w     �     �     �     �  !   �     �     �  ,   �     
  W   %  %   }     �     �     �     �     �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-03-14 14:21+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr_TR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Hatırlama Etkinlikler arasında gezin Etkinlikler arasında gezin (Ters) Uygula İptal Et Değiştir... Oluştur Etkinlik Ayarları Yeni Bir Etkinlik Oluştur Etkinliği Sil Etkinlikler Etkinlik bilgisi Etkinlik geçişi '%1' etkinliğini silmek istediğinizden emin misiniz? Listede olmayan tüm uygulamaları kara listeye al Yakın geçmişi temizle Etkinlik oluştur... Açıklama: QML dosyaları yüklenirken hata. Kurulumunuzu denetleyin.
%1 eksik T&üm uygulamalar için Bir günü unut Her şeyi unut Son saati unut Son iki saati unut Genel Simge Geçmişi tut İsim: S&adece belirli uygulamalar için Diğer Gizlilik Özel - bu etkinilk için kullanımı izleme Açık belgeleri hatırla: Her etkinlik için geçerli sanal masaüstünü hatırla (yeniden başlatma gerektirir) Bu etkinliğe geçmek için kısayol: Kısayollar Geçiş Duvar Kağıdı tut:   ay  ay sonsuza kadar 