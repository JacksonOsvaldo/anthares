��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  ;   �  j   	  i   t     �  5   �  	        $  �   9          5     >  
   M     X     h     q  (   z     �     �     �  )   �  2   �  D   /  (   t     �     �  �   �     O     W  #   q  �   �     B     `  #   e     �  
   �  
   �     �     �  x  �  +   6     b     �  �   �     -     C     G     P     g     �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-23 11:20+0000
Last-Translator: İşbaran <isbaran@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 "Tam ekran onarumı" performans sorunlarına yok açabilir. "Sadece kalitesiz olduğunda" sadece bir video gibi tam ekran değişikliklerinde parçalanmayı engeller. "Ekran içeriğini yeniden kullan" MESA sürücülerinde şiddetli performans sorunlarına yol açabilir. Kesin Uygulamaların, birleştirmeyi engellemesine izin ver Her zaman Canlandırma hızı: Uygulamalar, pencere açıkken birleşimi engellemek için bir ipucu belirleyebilir.
Bu, örneğin; oyunlar için performans iyileştirmeleri getirebilir.
Ayar, pencereye özgü kurallar tarafından geçersiz kılınabilir. Geliştirici: %1
Lisans: %2 Otomatik Erişebilirlik Görünüm Güzelleştirme Odaklama Araçlar Sanal Masaüstü Değiştirme Animasyonu Pencere Yönetimi Süzgeci yapılandır Gevrek tulliana@gmail.com, volkangezer@gmail.com Masaüstü Efektlerini başlangıçta etkinleştir Dizgici tarafından desteklenmeyen Masaüstü Efektlerini hariç tut Dahili Masaüstü Efektlerini hariç tut Tam ekran yeniden çizim Yeni Efektleri Getir... İpucu: Bir efekti nasıl yapılandıracağınızı ya da etkinleştireceğinizi anlamak için efektlerin seçeneklerine bakın. Anında KWin geliştirme takımı Pencere küçük resimlerini sakla: Pencere küçük resimlerini tutumak herzaman pencerelerin simge durumuna gelmesini engeller. Bunun sonucu olarak, pencere küçültüldüğünde kendi işini durdurabilir. Serdar Soytetir, Volkan Gezer Asla Sadece Gösterilen Pencereler için Sadece maliyetsiz ise OpenGL 2.0 OpenGL 3.1 EGL GLX Daha önceden OpenGL derlemesi (öntanımlı) KWin'in çökmesine sebep oldu.
Bu muhtemelen bir sürücü hatasıdır.
Eğer bu durumun kararlı bir sürücüye yükseltme yaparken olduğunu düşünüyorsanız
bu korumayı sıfırlayabilirsiniz ancak bu işlemin ani çökmeye neden olabileceğini unutmayın!
Alternatif olarak, XRender arka ucunu kullanmak isteyebilirsiniz. OpenGL bulma işlevini yeniden etkinleştir Ekran içeriğini tekrar kullan İşleme arka ucu: "Kesin" ölçekleme yöntemi tüm donanımlar tarafından desteklenmemektedir ve performans düşüklüğü veya gerçekleme sorunlarına yolaçabilir. Ölçekleme yöntemi: Ara Yumuşak Yumuşak (daha yavaş) Yaşarma Koruması ("vsync"): Çok yavaş XRender 