��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     &  
   2     =     I     X     \     q     �     �     �     �     �     �     �     �     �     �     �     �     �               /     4     L     P     Y     j  
   |     �     �     �  1   �  O   �  
   ;     F     J     S     [     n     �  	   �     �     �     �     �     �     �  	   �     �     �                  
   .  e   9  �   �     H     M     b     j     y     �     �     �  	   �  	   �     �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-10-04 12:04+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Etkinlikler Eylem Ekle Ayraç Ekle Gereç Ekle... Alt Alternatif Gereçler Her Zaman Görünür Uygula Ayarları Uygula Şimdi uygula Yazar: Otomatik Gizle Geri Düğmesi Alt İptal Kategoriler Orta Kapat + Yapılandır Etkinliği yapılandır Etkinlik oluştur... Ctrl Şu anda kullanılıyor Sil E-posta: İleri Düğmesi Yeni gereçler al Yükseklik Yatay Kaydırma Buraya Girin Klavye kısayolları Yerleşim gereçler kilitli iken değiştirilemez Yerleşim değişiklikleri, diğer değişiklikler yapılmadan uygulanmalıdır Yerleşim: Sol Sol Tuş Lisans: Gereçleri Kilitle Panel Ekranı Kaplasın Meta Orta Tuş Daha Fazla Ayar... Fare Eylemleri Tamam Panel Hizalama Paneli Kaldır Sağ Sağ Tuş Ekran Kenarı: Ara... Shift Etkinliği durdur Dururulmuş eylemler: Değiştir Etkin modülün ayarları değiştirildi. Bu değişiklikleri uygulamak mı yoksaymak mı istersiniz? Bu kısayol uygulamayı etkinleştirecektir: klavyenin odaklanmasını sağlayacak ve eğer uygulama açılır pencereye sahipse (başlat menüsü gibi) açılacaktır. Üst Kaldırmayı geri al Kaldır Gereci kaldır Dikey Kaydırma Görünürlük Duvar Kağıdı Duvar Kağıdı Türü: Gereçler Genişlik Pencereler Kaplayabilir Pencereler Alta Gider 