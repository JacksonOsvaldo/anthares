��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     �     �  
   	  
   	     	      	  	   5	     ?	     M	  &   V	     }	     �	     �	  
   �	     �	     �	     �	     �	     
     
     &
     :
     P
     c
     �
     �
     �
     �
     �
     �
  %        *     :     U     j  .   r  A   �     �     �     �               !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-23 11:20+0000
Last-Translator: İşbaran <isbaran@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Kullanılabilir boyutlar: %1) Resim seç Gelişmiş Yazar &Otomatik Oturum Aç Arkaplan: Resmi Temizle Komutlar Arşiv sıkıştırması çözülemedi İmleç Teması: Temayı özelleştir Öntanımlı Açıklama Yeni SDDM Temalarını İndirin volkangezer@gmail.com Genel Yeni Tema Al Askıya Alma Komutu: Dosyadan Kur Bir tema yükleyin. Geçersiz tema paketi Dosyadan yükle... SDDM kullanan oturum ekranı Azami Kullanıcı Kimliği: Asgari Kullanıcı Kimliği: Volkan Gezer İsim Kullanılabilir önizleme yok Yeniden Başlatma Komutu: Çıktıktan sonra yeniden oturum aç Temayı Kaldır SDDM KDE Yapılandırması SDDM tema yükleyici Oturum: SDDM içerisindeki öntanımlı imleç teması Yüklenecek temanın varolan bir arşiv dosyası olması gerekir. Tema Tema yüklenemiyor Bir temayı kaldırın. Kullanıcı Kullanıcı: 