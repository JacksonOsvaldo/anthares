��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     "  !   (     J     Z     u     {     �     �     �  P   �     	     	     '	     3	  2   R	  5   �	  "   �	      �	     �	     
     
  -   
  C   J
     �
     �
  !   �
     �
     �
     �
            *   !     L  C   S     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-11-12 00:33+0200
Last-Translator: Kaan Ozdincer <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.4
 Yazar Telif Hakkı 2006 Sebastian Sauer Sebastian Sauer Çalıştırılacak betik. Genel Yeni bir betik ekle. Ekle... İptal edilsin mi? Yorum: tulliana@gmail.com, gorkem@kde.org, ibrahim@pardus.org.tr, volkangezer@gmail.com Düzenle Seçilen betiği düzenle. Düzenle... Seçilen betiği çalıştır. "%1" yorumlayıcısı için betik oluşturulamadı "%1" betik dosyası için yorumlayıcı belirlenemedi "%1" yorumlayıcısı yüklenemedi "%1" betik dosyası açılamadı Dosya: Simge: Yorumlayıcı: Ruby yorumlayıcısı için güvenlik düzeyi Serdar Soytetir, Görkem Çetin, H. İbrahim Güngör, Volkan Gezer İsim: "%1" isimli bir fonksiyon yok "%1" isimli bir yorumlayıcı yok Çıkar Seçilen betiği kaldır. Çalıştır "%1" betik dosyası yok. Dur Seçilen betiği çalıştırmayı durdur. Metin: Kross betiklerini çalıştırmak için komut satırı uygulaması. Kross 