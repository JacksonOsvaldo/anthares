��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  
   a  	   l     v     �  8   �     �     �     �     �                     =  	   [     e     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: kdeplasma-addons-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-04-24 16:58+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Görünüm Gözat... Bir resim seçin Onbeşli Yapboz Resim Dosyaları (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Sayı rengi Özel resmin yolu Parça rengi Rakamları göster Karıştır Boyut Sırayla düzenleyerek çöz Çözüldü! Yeniden deneyin. Süre: %1 Özel resim kullan 