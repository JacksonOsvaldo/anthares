��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �                &     4  6   =     t     �     �     �     �     �  C   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: kdeutils-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-05-19 05:49+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish (http://www.transifex.com/projects/p/kdeutils-k-tr/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 &Yazıcıları Yapılandır... Sadece etkin görevler Tüm görevler Sadece tamamlanmış görevler Yazıcıyı yapılandır Genel Etkin iş yok İş yok Hiçbir yazıcı yapılandırılmadı veya bulunamadı Bir etkin iş %1 etkin iş Bir iş %1 iş Yazdırma kuyruğunu aç Yazdırma kuyruğu boş Yazıcılar Yeni yazıcı ara... Kuyrukta %1 yazdırma görevi var Kuyrukta %1 yazdırma görevi var 