��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     �  G     �     �     �               +     .     ;     J     Y     i     u     �     �  	   �     �     �     �     �     �     �     �     �            ,        E     L  	   N     X     \     ^     v     �     �     �     �  	   �     �  	   �  	   �     �          &  
   3     >     J     P     f     v     {     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �               '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: kdeplasma-addons-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2017-05-10 07:27+0000
Last-Translator: Kaan <kaanozdincer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  dk %1 %2 %1 (%2) Beaufort ölçeği bft Santigrat °C ° Ayrıntılar Fahrenhayt °F 1 Gün %1 Gün Hektopascal hPa Y: %1 D: %2 Yüksek: %1 İnç Cıva inHg Kelvin K Kilometre Kilometre Saat km/h Kilopascal kPa Knot kt Konum: Düşük: %1 Metre Saniye m/s Mil Mil Saat mph Millibar mbar N/A '%1' için hava durumu istasyonu bulunamadı Notlar % Basınç: Ara - Lütfen Yapılandırın Sıcaklık: Birimler Güncelleme aralığı: Görüş mesafesi: Hava Durumu İstasyonu Dinginlik Rüzgar hızı: %1 (% %2) Nem: %1%2 Görüş mesafesi: %1 %2 Çiy Noktası: %1 Nemlilik: %1 düşüyor yükseliyor sabit Basınç Eğilimi: %1 Basınç: %1 %2 %1%2 Görüş mesafesi: %1 Belirtilen Uyarılar: Bildirilen İzlemeler: D DKD DGD K KD KKD KKB KB G GD GGD GGB GB RY B BKB BGB %1 %2 %3 Sakin Rüzgâr Soğutması: %1 Fırtına: %1 %2 