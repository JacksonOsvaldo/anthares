��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     G     _  &   p  i   �  T     M   V  J   �  X   �  h   H	     �	     �	     �	     �	     �	  
   �	     �	     �	     
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-21 09:49+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.1
 Taşıyıcıyı kilitle Ortamı çıkart Adı :q: ile eşleşen aygıtları bul Tüm aygıtları listeler, aygıtların bağlanmasını, ayrılmasını veya çıkartılmasını sağlar. Çıkartılabilecek durumdaki aygıtları listeler ve çıkartılmalarını sağlar. Bağlanılabilir durumdaki aygıtları listeler ve bağlanmalarını sağlar. Ayrılabilecek durumdaki aygıtları listeler ve ayrılmalarını sağlar. Kilitlenebilir olan tüm şifrelenmiş aygıtları listeler ve kilitlenmelerini sağlar. Kilidi açılabilir olan tüm şifrelenmiş aygıtları listeler ve kilitlerinin açılmasını sağlar. Aygıtı bağla aygıt çıkart kilitle bağla kilidi aç ayır Taşıyıcının kilidini çöz Aygıtı ayır 