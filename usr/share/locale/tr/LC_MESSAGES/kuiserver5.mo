��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �  $   �  3        D     S  
   q  
   |     �     �     �     �  9   �     �          .     5     =     F  #   O  $   s  $   �  %   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-21 09:49+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 %1 dosya %1 dosya %1 dizin %1 dizin %2 dosyanın %1 kadarı tamamlandı  %2 dosyanın %1 kadarı %3/s hızıyla tamamlandı  %1 tamamlandı %1 %2/s hızıyla tamamlandı Görünüm Davranış İptal Temizle Yapılandır... Tamamlanmış Görevler Çalışan aktarımların/görevlerin listesi (kuiserver) Başka bir listeye taşı Farklı bir listeye taşı. Durdur Kaldır Kaldır. Devam Et Tüm görevleri bir listede göster Tüm görevleri bir listede göster. Tüm görevleri bir ağaçta göster Tüm görevleri bir ağaçta göster. Ayrı pencereler göster Ayrı pencereler göster. 