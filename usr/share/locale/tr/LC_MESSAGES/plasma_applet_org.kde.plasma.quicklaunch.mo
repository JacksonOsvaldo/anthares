��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     c  I   x  
   �  
   �     �     �                    .     E     \     l     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2017-04-24 16:59+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Başlatıcı Ekle... Bağlam menüsü kullanarak Sürükle ve Bırak ile başlatıcı ekleyin. Görünüm Düzenleme Başlatıcıyı Düzenle... Pencereyi etkinleştir Başlık gir Genel Simgeleri gizle Azami sütun sayısı: Azami satır sayısı: Hızlı başlat Başlatıcıyı Kaldır Gizli simgeleri göster Başlatıcı adlarını göster Başlığı göster Başlık 