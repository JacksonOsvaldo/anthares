��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  d   �  �   �     ~  U   �     �  
     1     
   E  3   P     �     �     �     �  (   �  *   �  ,     $   G     l  ]   s     �     �  �   �     �  /   �     �     �     �     �     �               *  	   ;     E     b     q     v     �     �  	   �     �  	   �     �  	   �     �     �  	   
  
     
        *     7  	   U     _     d     }  �   �     (  >   9  �   x       5     9   T  ;   �      �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-10-21 09:48+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Arka uç değişikliğinin etkinleştirilmesi için oturumu kapatıp yeniden giriş yapmalısınız. Sisteminizde bulunan Phonon Arka Uçlarının bir listesidir.  Buradaki sıralama bu arka uçların Phonon içerisindeki kullanım sırasını belirler. Aygıt Listesini Uygula... Gösterilen aygıt tercihlerini aşağıdaki diğer ses çalma kategorilerine uygula: Ses Donanımı Ayarları Ses Çalma '%1' Kategorisi için Ses Çalma Aygıtı Tercihi Ses Kaydı '%1' Kategorisi için Ses Kaydetme Aygıtı Tercihi Arka Uç Colin Guthrie Bağlayıcı Copyright 2006 Matthias Kretz Öntanımlı Ses Çalma Aygıtı Tercihi Öntanımlı Ses Kaydetme Aygıtı Tercihi Öntanımlı Video Kaydetme Aygıtı Tercihi Öntanımlı/Belirtilmemiş Kategori Ertele Kategoriler tarafından gözardı edilecek olan öntanımlı aygıt sırlamasını tanımlar. Aygıt Yapılandırması Aygıt Tercihi Sisteminizde bulunana aygıtlar seçilen kategori için uygun. Aygıtın hangi uygulamalar tarafından kullanılmasını istediğinizi seçin. tulliana@gmail.com Seçilen ses çıkışı aygıtı ayarlanamadı Ön Orta Ön Sol Ön Ortanın Solu Ön Sağ Ön Ortanın Sağı Donanım Bağımsız Aygıtlar Girdi Düzeyleri Geçersiz KDE Ses Donanımı Ayarları Matthias Kretz Mono Serdar Soytetir Phonon Yapılandırma Modülü Çalma (%1) Tercih Et Profil Arka Orta Arka Sol Arka Sağ Kayıt (%1) Gelişmiş aygıtları göster Sol Taraf Sağ Taraf Ses Kartı Ses Aygıtı Hoparlör Yerleşimi ve Testi Subwoofer Dene Seçili aygıtı test et %1 Test ediliyor Sıralama aygıt tercihini belirler. Eğer bazı nedenlerle birinci aygıt kullanılamazsa Phonon ikinci aygıtı deneyecek ve bu böyle devam edecektir. Bilinmeyen Kanal Gösterilen aygıt listesini daha fazla kategori için kullan. Çeşitli ortam kategorileri için kullanım durumları. Her kategori için Phonon tarafından hangi aygıtın kullanılacağını tercih edebilirsiniz. Video Kaydı '%1' Kategorisi için Video Kaydetme Aygıtı Tercihi Kullandığını arka uç ses kaydını desteklemeyebilir Kullandığını arka uç video kaydını desteklemeyebilir seçilen aygıt için tercih yok seçilen aygıtı tercih et 