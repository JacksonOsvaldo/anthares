��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  <   L  '   �  !   �     �  &   �            
   1     <  ,   C  
   p  &   {  .   �  	   �  '   �               *  +   :      f  %   �     �  2   �     �     �       4   ,     a     {     �     �  '   �     �     �     �               (  �   7  [   �  *   @     k  *   t     �     �  )   �  #        +  -   E  	   s     }  (   �     �     �  
   �     �  	   �           	          !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: kdeplasma-addons-kde4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-05-22 12:33+0000
Last-Translator: Necdet <necdetyucel@gmail.com>
Language-Team: Turkish (http://www.transifex.com/projects/p/kdeplasma-addons-k-tr/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 

Son saklanan şeride geçmek için önceki şeridi seçin. &Çizgi Roman Kitap Arşivi Oluştur... Çizgi Romanı &Farklı Kaydet... &Şerit Numarası: *.cbz|Çizgi Roman Kitap Arşivi (Zip) Ö&zgün Boyut Mevcut &konumu sakla Gelişmiş Tümü %1 tanımlayıcısı için bir hata oluştu. Görünüm Çizgi roman alma işlemi başarısız Çizgi roman eklentilerini otomatik güncelle: Önbellek Yeni çizgi roman şeridini kontrol et: Çizgi Roman Çizgi Roman Önbelleği: Yapılandır... Belirtilen konumda arşiv oluşturulamadı. %1 Çizgi Roman Arşivi Oluştur Çizgi Roman Kitap Arşivi Oluşturma Hedef: Çizgi roman alınırken oluşan hataları göster Çizgi romanları indir Hata Yönetimi Bir dosya arşive eklenemedi. %1 tanımlayıcısı ile bir dosya oluşturulamadı. Başlangıç şuradan ... Bitiş şuradan ... Genel Yeni Çizgi Romanlar Al... Çizgi roman alma işlemi başarısız: Şeride Git Bilgiler &Geçerli şeride atla &İlk Şeride Atla Şeride Atla ... El ile aralık Internet bağlantısı olmayabilir.
Çizgi roman eklentisi bozuk olabilir.
Veya, bu gün/sayı/dizge için çizgi roman olmayabilir, başka birini seçmek işe yarayabilir. Çizgi romanın üzerine farenin orta tuşuyla tıklamak şeridi özgün boyutta göstersin Zip dosyası mevcut değil, çıkılıyor. Aralık: Okları yalnızca üzerine gelince göster Çizgi roman adresini göster Çizgi roman yazarını göster Çizgi roman tanımlayıcısını göster Çizgi roman başlığını göster Şerit tanımlayıcısı: Arşivlenecek çizgi roman şerit aralığı. Güncelle Çizgi romanın sitesine git Alışveriş &ağ sayfasını ziyaret et # %1 gün gg.AA.yyyy Ye&ni bir şeritte Yeni Sekme Şuradan: Şuraya: dakika çizgi romanlar başına şerit 