��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j     	     )     0     8     F     \     c     t  
   �     �     �     �     �     �     �     �  H   �     &     3     D  "   V     y     ~  	   �  t   �  b        u     �     �     �  '   �  /   �          /  !   5      W     x     |     �      �     �     �  #   �  "        +  #   3     W  	   \     f  
   z  B   �  +   �     �          ,  *   A     l     }     �  /   �     �         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-04-24 16:56+0100
Last-Translator: Volkan Gezer <volkangezer@gmail.com>
Language-Team: Turkish <kde-l10n-tr@kde.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Eşleşen pencere özelliği:  Devasa Büyük Kenarlık Yok Kenarda Çizgiler Yok Normal Aşırı Büyük Çok Küçük En Büyük Çok Büyük Büyük Normal Küçük Çok Büyük Etkin Pencere Parlaması Ekle Karnalığı olmayan pencereleri boyutlandırmak için bir tutamaç ekle Animasyonlar &Düğme boyutu: Kenarlık boyutu: Düğmenin üzerine gelme geçişi Orta Orta (Tam Genişlik) Sınıf:  Pencerenin etkinlik durumu değiştirildiğinde, pencere gölgesi ve parlaması arasındaki kaybolmayı yapılandır Pencere düğmelerinin üzerine gelindiğinde yapılacak vurgulama canlandırmasını yapılandır Dekorasyon Seçenekleri Pencere Özelliklerini Algıla İletişim Kutusu Düzenle İstisnayı Düzenle - Oxygen Ayarları Bu istisnayı etkinleştir/devre dışı bırak İstisna Türü Genel Pencere başlık çubuğunu gizle Seçilen Pencere Hakkında Bilgi Sol Aşağı Taşı Yukarı Taşı Yeni İstisna - Oxygen Ayarları Soru - Oxygen Ayarları Düzenli İfade Düzenli ifade sözdizimi geçersiz &Eşleştirilecek düzenli ifade:  Kaldır Seçili istisna kaldırılsın mı? Sağ Gölgeler Baş&lık hizalama: Başlık:  Başlık çubuğu ve pencere içeriği için aynı renkleri kullan Pencere sınıfını kullan (tüm uygulama) Pencere başlığını kullan Uyarı - Oxygen Ayarları Pencere Sınıf Adı Aşağıya Doğru Düşen Pencere Gölgesi Pencere Kimliği Pencere Özellik Seçimi Pencere Başlığı Pencere etkinliği durumunun değişme geçişi Pencereye Özel Yoksaymalar 