��          D      l       �      �   
   	       *      �  K  �   �     �     �  0   �                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2007-11-21 17:57+0100
Last-Translator: Rinse de Vries <rinsedevries@kde.nl>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 <h1>Meardere buroblêden</h1>Yn dizze module kinne jo ynstelle hoefolle firtuele buroblêden jo brûke wolle en hoe dizzen neamd wurde. Buroblêd %1 Buroblêd %1: Hjir kinne jo de namme foar buroblêd %1 ynfiere 