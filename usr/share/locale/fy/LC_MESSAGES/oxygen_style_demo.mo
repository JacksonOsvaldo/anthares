��    o      �  �         `	     a	     h	     v	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
  
   %
  
   0
  	   ;
     E
     S
  
   m
     x
     }
     �
     �
     �
  
   �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
          %     )     1     8     >     B     E     J     V     ^     o     w     �     �     �     �     �     �     �     �     �     �     �          (     4     G     ^     r  /   �  =   �  #      *   $     O     h     p     v     �     �  
   �     �     �     �  	   �     �     �  
   �     �       
     
   #  	   .     8     F  
   `     k     p     v     }     �     �     �     �  	   �     �     �     �     �  
   �     �  �  �     �     �     �     �     �  	   �     �  	   �     �                    !  	   =  
   G     R     `     m     �     �     �     �     �     �     �     �     �     �     �               %     8     L     Q     ^     d  	   v     �     �     �  	   �     �     �     �     �     �     �  	   �     �                    %     5     =     O     h     p     }     �     �     �     �     �     �     �       "   0  1   S  ;   �  *   �  4   �     !     >     F     K     X     ]     f     u     �     �     �     �     �     �     �               "     .     :     I     d     p     v     |     �  
   �     �     �     �     �     �     �  	   �     �       	        <              @   7      Z       !           N   [   	   S              ]   U   8   
   %   K          O   B       H       /      ^       6   F       W   I   m   E   *   n   Y      J   C          )          ,           g          V   1   L               P   T       4   d       2               i                                  =      _   l          3   c                +   ;   A   $   ?         M   0   5   R   e   .              `   Q   >      -   b              a   (   &       \             #       X      D   h   :   k   o   9   "   '      j       G   f    Bottom Bottom to Top Bottom-left Bottom-right Buttons Cascade Center Checkboxes Description Dialog Document mode East Editable combobox Editors Enabled Example text First Column First Description First Item First Page First Row First Subitem First Subitem Description First item Flat Frame Frames GroupBox Hide tabbar Horizontal Huge (48x48) Icons Only Input Widgets Large Large (32x32) Left  Left to Right Lists Medium (22x22) Multi-line text editor: New New Row Normal North Off On Open Oxygen Demo Partial Password editor: Preview Pushbuttons Radiobuttons Raised Right Right to Left Right to left layout Save Second Column Second Description Second Item Second Page Second Subitem Second Subitem Description Second item Select Next Window Select Previous Window Show Corner Buttons Shows the appearance of buttons Shows the appearance of lists, trees and tables Shows the appearance of sliders, progress bars and scrollbars Shows the appearance of tab widgets Shows the appearance of text input widgets Single line text editor: Sliders Small Small (16x16) South Spinbox: Tab Widget Tab Widgets Tabs Text Alongside Icons Text Only Text Under Icons Text and icon: Text only: Third Column Third Description Third Item Third Page Third Row Third Subitem Third Subitem Description Third item Tile Title Title: Toolbox Toolbuttons Top Top to Bottom Top-left Top-right Use flat buttons Use flat widgets Vertical West Wrap words password Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2010-07-13 13:55+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Under Under nei boppe Lofts ûnder Rjochts ûnder Knoppen Trepfoarm Sintraal Karfakjes Beskriuwing Dialooch Dokumint modus East Te bewurkjen kombinaasjefak Bewurkers Ynskeakele Foarbyldtekst Earste kolom Earste beskriuwing Earste item Earste side Earste rige Earste subitem Earste subitem beskriuwing Earste item Flak Ramt Ramten Groepfak Ljepperbalke ferbergje Horizontaal Mansk (48x48) Allinne byldkaikes Widgets faor ynfier Grut Grut (32x32) Lofts Lofts-nei-rjochts Opsomming Middel (22x22) Tekstbewurker meardere-rigels: Nij Nije rige Normaal Noard Ut Oan Iepenje Oxygen Demo Dieltelik Wachtwurd bewurker: Foarbyld Drukknoppen Radioknoppen Nei foaren hele Rjochts Rjochts-nei-lofts Lofts nei rjochts opmaak Bewarje Twadde kolom Twadde beskriuwing Twadde item Twadde side Twadde subitem Twadde subitem beskriuwing Twadde item Selektearje folgjend finster Selektearje foarich finster Râne knoppen sjen litte It úterlik fan knoppen sjen litte Lit it úterlik fan listen, beamen en tafels sjen Lit it úterlik fan gliders, fuortgongs- en skowbalken sjen It úterlik fan ljepper widgets sjen litte It úterlik fan widgets foar tekst ynfier sjen litte Tekst bewurker inkele-rigel: Gliders Lyts Lyts (16x16) Sûd Spinfak: Ljepper widget Widgets foar ljeppers Ljeppers Tekst njonken byldkaikes Allinne tekst Tekst ûnder byldkaikes Tekst en byldkaike: Allinne tekst: Tredde kolom Tredde beskriuwing Tredde item Tredde side Tredde rige Tredde subitem Tredde subitem beskriuwing Tredde item Tegel Titel Titel: Arkbak Arkknoppen Boppe Boppe nei ûnderen Lofts boppe Boppe rjochts Flakke knoppen brûke Flakke widgets brûke Fertikaal West Wurden omslaan wachtwurd 