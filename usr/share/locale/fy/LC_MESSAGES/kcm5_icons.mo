��    9      �  O   �      �     �     �     �     
          #  �   >  -   �  )   �  -   "  +   P  r   |  	   �  	   �               #     ,  
   9     D     P     X     `      w     �     �     �      �     �     �  r   �  5   r     �     �     �  	   �     �     �     �  (   �     '	     5	     N	     h	     �	     �	  +   �	  3   �	     �	     �	     
     
  S    
  )   t
     �
  �   �
  �  �     *     3     <     K     _     e  �   �  4        T  
   [     f  n   o     �     �     �       	        $  
   1     <  	   H     R  +   [  (   �     �     �     �  '   �  
   �       v   %  &   �  	   �     �     �     �     �            +        H  +   [  -   �  -   �     �     �  /   �  4   ,     a     q          �  R   �  /   �       �   +               1                &              0   '                7                            
          -               4      5   #      (       6   3          )                 /   	             9         +                    *      $      !   ,   .   "       8           %      2    &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2009-12-19 18:38+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Omf&ang: &Effekt: T&wadde kleur: &Semi-trochsichtich &Tema (c) 2000-2003 Geert Jansen <qt>Binne jo wis dat jo bydlkaiketema <strong>%1</strong> fuortsmite wollle?<br /><br />Alle triemmen dy't troch dit tema ynstallearre binne wurde wiske.</qt> <qt>Tema <strong>%1</strong> wurdt ynstallearre</qt> Aktive Utskeakele Standert Der barde in flater ûnder it ynstallearjen. Dochs is it grutste diel fan de tema's út it argyf ynstallearre. A&vansearre Alle byldkaikes Antonio Larrosa Jimenez K&leur: Ynkleurje Befêstiging Untsêding Beskriuwing Buroblêd Dialogen URL-adres dan tema hir ynfiere/nei ta slepe rinsedevries@kde.nl, berendy@bigfoot.com Effektparameters Gamma Geert Jansen Nije tema's ophelje fan ôf it ynternet Byldkaikes Byldkaike konfiguraasjemodule As jo in tema argyf al lokaal te stean hawwe, dan sil dizze knop it útpakke en beskikber foar KDE applikaasje meitsje Ynstallearje in lokale tema argyftriem Haadbalke Rinse de Vries, Berend Ytsma Namme Gjin effekt Paniel Foarbyld Tema fuortsmite It selektearre tema fan de skiif fuortsmite Effekt ynstelle... Ynstellings foar effekt fan aktyf byldkaike Ynstellings foar effekt fan standertbydlkaike Ynstellings foar effekt fan ynaktyf byldkaike Grutte: Lytse byldkaikes De triem is net in jildich byldkaiketema-argyf. Dit sil it selektearre tema fan de skiif fuortsmite. Nei griiswearde Nei monogroom Arkbalke Torsten Rahn It byldkaiketema-argyf koe net ynladen wurde.
Kontrolearje of it adres %1 just is. Het pictogramthema-archief %1 is niet gevonden. Byldkaike brûkens Jo moatte ferbûn wêze mei it ynternet om dizze aksje te dwaan. In dialooch sil in list fan tema's fan http://www.kde.org werjaan. It klikken op in ynstallaasje knop dy by in tema heard sil it tema lokaal ynstallearje. 