��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     �     �  �    �  �  d   O
     �
     �
     �
  "   �
          1     J     i                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-08-18 20:19+0100
Last-Translator: berend ytsma <berendy@bigfoot.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.1
Plural-Forms: nplurals=2; plural=n != 1;
  sek Tiidslymit op&startindikaasje: <H1>Taakbalkenotifikaasje</H1>
Do kinst in twadde metoade ynskeakelje foar de opstartnotifikaasje. Dizze
notifikaasje sil brûkt  wurde as taakbalke, wêr dan in sânrinner
ferskynt. Dizze jout oan dat de starte applikaasje laden wurdt.
It kin barre dat bepaalde applikaasje net bewust binne fan dizze
opstartnotifikaasje. yn dat gefal ferdwynt de knop nei it tiidsbestek dat opjûn
 is yn it ûnderdiel "Tiidslymit opstartindikaasje". <h1>Dwaande-rinnerke</h1>
KDE jout in dwaande-rinnerke as notifikaasje foar startende programma's.
Om de dwaande-rinnerke te aktivearjen, selektearje ien fan de opsjes yn it kombinaasjefjild.
It kin barre dat bepaalde applikaasje net bewust binne fan dizze
opstartnotifikaasje. yn dat gefal ferdwynt de knop nei it tiidsbestek dat opjûn
 is yn it ûnderdiel "Tiidslymit opstartindikaasje". <h1>Opstartnotifikaasje</h1>Hjir kinst de wize fan oantsjutting op in begjinnend programma ynstelle. Knipperjend dwaande-rinnerke Stuiterjend rinnerke Dwaande r&innerke &Taakbalkenotifikaasje ynskeakelje Gjin dwaande-rinnerke Passive dwaande-rinnerke Tiidslymit op&startindikaasje: Taakbalke&notifikaasje 