��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  �   �  ]   }  Z   �     6     C     `  "   l     �     �     �     �     �  /   �  L     $   T        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-07-16 13:14+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 <qt>Binne jo wis dat jo it rinnerketema <strong>%1</strong> fuortsmite wolle?<br />Alle triemmen hokker ynstallearre binne mei dit tema sille wiske wurde.</qt> <qt>Jo kinne it tema dat jo no brûke net wiskje.<br />Skeakel earst oer nei in oar tema</qt> In styl mei de namme %1 bestiet al yn jo byldkaikestylmap. Wolle jo it ferfange mei dizze? Befêstiging Oanwizerynstellings feroarje Beskriuwing In styl URL-adres ynfiere of slepe berendy@bigfoot.com Berend Ytsma Namme Styl oerskriuwe? Tema fuortsmite De triem %1 is gjin jildich oanwizersstyltriem. It oanwizerstylargyf kin net ynladen wurde. Soargje dat it adres %1 just is. It oanwizerstylargyf %1 is net fûn. 