��    #      4  /   L           	     !     .  +   5      a     �  ;   �     �     �                 >     _  9   m     �  3   �  	   �     �  )   �  %   )  |   O  [   �  5   (  *   ^     �     �     �     �     �  +   �  (        A  n   a  3   �  �       �	     �	     �	  7   �	  &   
     ?
  F   \
     �
  #   �
     �
  ,   �
     #     5  N   K     �  F   �     �     �  *     -   ?  }   m  q   �  &   ]  -   �     �     �  
   �     �  &   �  0     +   H  %   t     �  4                             #      
                                         !             	          "                                                             %1 theme already exists Add Emoticon Add... Choose the type of emoticon theme to create Could Not Install Emoticon Theme Create a new emoticon Create a new emoticon by assigning it an icon and some text Delete emoticon Design a new emoticon theme Do you want to remove %1 too? Drag or Type Emoticon Theme URL EMAIL OF TRANSLATORSYour emails Edit Emoticon Edit the selected emoticon to change its icon or its text Edit... Emoticon themes must be installed from local files. Emoticons Emoticons Manager Enter the name of the new emoticon theme: Get new icon themes from the Internet If you already have an emoticon theme archive locally, this button will unpack it and make it available for KDE applications Insert the string for the emoticon.  If you want multiple strings, separate them by spaces. Install a theme archive file you already have locally Modify the selected emoticon icon or text  NAME OF TRANSLATORSYour names New Emoticon Theme Remove Remove Theme Remove the selected emoticon Remove the selected emoticon from your disk Remove the selected theme from your disk Require spaces around emoticons Start a new theme by assigning it a name. Then use the Add button on the right to add emoticons to this theme. This will remove the selected theme from your disk. Project-Id-Version: kcm_emoticons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-04 06:03+0100
PO-Revision-Date: 2010-07-13 11:57+0100
Last-Translator: Berend Ytsma <berendy@gmail.com>
Language-Team: Frysk <kde-i18n-fry@kde.org>
Language: fy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
 Tema %1 bestiet al Emobyldkaike taheakje Taheakje... Kies de soarte fan emobyldkaike tema om oan te meitsjen Koe emobyldkaike tema net ynstallearje Nije emobyldkaike oanmeitsje Meitsje in nij emobyldkake troch in byldkaike en wat tekst ta te wizen Emobyldkaike wiskje In nije emo0yldkaike tema ûntwerpe Wolle jo %1 ek fuortsmite? Emobyldkaike tema url-adres ynfiere of slepe berendy@gmail.com Emobyldkaike bewurkje Bewurkje de selektearre emiobyldkaike om it byldkaike of te tekst te feroarjen Bewurkje... Emobyldkaikes tema's moatte ynstallearre wurde fan de lokale triemmen. Emobyldkaikes Emobyldkaikes temabehear Fier de namme fan de nije emobyldkaike yn: Nije byldkaike tema's ophelje fan it ynternet As jo al in emobyldkaike tema lokaal te stean ha, dan sil dizze knop it útpakke en it beskikber meitsje foar KDE programma's Fier de tekenreeks yn foar emobyldkaike. As jo mear dan ien tekenreeks wolle dan wurdt it skieden troch spaasjes. Ynstallearje in lokale tema argyftriem De selektearre emobyldkaike of tekst feroarje Berend Ytsma Nij emobyldkaike tema Fuortsmite Tema fuortsmite De selektearre emobyldkaike fuortsmite De selektearre byldkaike fan de skiif fuortsmite It selektearre tema fan de skiif fuortsmite Fereasket romte rûn de emobyldkaikes Begjin in nij tema troch in namme ta te wizen. Brûk dan de rjochter knop Taheakje om emobyldkaikes oan dit tema ta te heakjen. Dit sil it selektearre tema fan de skiif fuortsmite. 