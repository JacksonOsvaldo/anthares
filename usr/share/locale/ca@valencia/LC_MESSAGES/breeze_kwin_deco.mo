��    D      <  a   \      �     �     �     �  !     "   %  &   H  ,   o  #   �  &   �  !   �  &   	  '   0  "   X  #   {  !   �  "   �  '   �       +     2   <     o  
   �     �     �     �     �     �     �     �     �     	  !   	  +   *	     V	     v	      {	     �	     �	     �	     �	     �	  !   �	     
  	    
     *
     2
     R
     m
  &   �
     �
     �
     �
     �
     �
     �
     �
            $        A     R     l     ~     �     �     �  >   �  �       �     �  .   �               #     /     D     K  
   X     c  	   o     y     ~     �     �  	   �     �  >   �  X   �     8  
   S     ^     q     �     �     �     �     �  ,   �     �  3      ;   4  +   p     �  '   �     �  "   �            (     ,   F     s  	   |  	   �  &   �      �     �  2   �  )        H  !   P     r     x       	   �     �     �  /   �     �     �          6  &   T     {  *   �     �     D   8      1                 )                  .               3   @   4       &      /   >       ?      <   ;             7       (          
                 $                  0   5       A   :   #   2         C   +                        =   '             *                 	          ,   !                             B   9   6       "       %   -     ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Medium @item:inlistbox Button size:None @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw separator between Title Bar and Window Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2018-01-09 20:25+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
  ms % &Propietat de coincidència de les finestres:  Enorme Gran Sense vores Sense vores laterals Normal Fora de mida Minúscula Molt enorme Molt gran Gran Mitjà Sense Petit Molt gran Afig Afig la nansa per canviar la mida de les finestres sense vores Permet el redimensionament de les finestres maximitzades des de les vores de la finestra D&urada de les animacions: Animacions Mida dels b&otons: Mida de la vora: Centre Centre (amplària completa) Classe:  Color: Opcions de decoració Detecció de les propietats de les finestres Diàleg Dibuixa un cercle al voltant del botó de tancament Dibuixa un separador entre la barra de títol i la finestra Dibuixa el degradat del fons de la finestra Edita Edita excepció - Arranjament del Brisa Activa les animacions Habilita/inhabilita esta excepció Tipus d'excepció General Oculta la barra de títol de la finestra Informació quant a la finestra seleccionada Esquerra Mou avall Mou amunt Excepció nova - Arranjament del Brisa Pregunta - Arranjament del Brisa Expressió regular La sintaxi de l'expressió regular no és correcta Expressió regular que h&a de coincidir:  Elimina Elimino l'excepció seleccionada? Dreta Ombres &Mida: Minúscul A&lineació del títol: Títol:  Usa la classe de finestra (aplicació completa) Usa el títol de la finestra Avís - Arranjament del Brisa Nom de classe de finestra Identificació de la finestra Selecció de propietats de la finestra Títol de la finestra Substitucions específiques de la finestra In&tensitat: 