��          �      �       0     1     9  
   S     ^     u     �     �     �     �     �     �     �  �  �     �     �  	   �     �          $  	   =     G     b     {  #   �     �     	   
                                                  (Empty) @title:windowSelect Font Appearance Configure Input Method Custom Font: Exit Input Method Hide %1 Reload Config Select Font Show Use Default Font Vertical List Project-Id-Version: plasma_applet_org.kde.kimpanel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:21+0200
PO-Revision-Date: 2017-03-09 22:11+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 (buit) Selecció del tipus de lletra Aparença Configura el mètode d'entrada Tipus de lletra personalitzada: Ix del mètode d'entrada Oculta %1 Recarrega la configuració Trieu el tipus de lletra Mostra Usa el tipus de lletra per omissió Llista vertical 