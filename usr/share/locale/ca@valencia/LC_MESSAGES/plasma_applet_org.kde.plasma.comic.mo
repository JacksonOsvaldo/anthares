��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  L   e  &   �     �     �       
   !     ,     L     U  .   Y  	   �     �  4   �     �  (   �          $     <  9   I  +   �  +   �     �  /   �          &  (   7  4   `     �     �     �     �  )   �               (     @     Y     l  �   |  E   M  *   �  	   �  8   �            !   3     U     q  ,   �  
   �     �     �     �     �  
     $        3     ;     >     E     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_org.kde.plasma.comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-05-29 20:39+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 

Seleccioneu la tira prèvia per anar a la darrera tira en la memòria cau. &Crea un arxiu de llibre de còmics... Al&ça el còmic com a... &Tira número: *.cbz|Comic Book Archive (Zip) Mida re&al Emmagatzema la &posició actual Avançat Tot Hi ha hagut un error per a l'identificador %1. Aparença Ha fallat l'arxivat del còmic: Actualitza automàticament els connectors de còmic: Memòria cau Comprova si hi ha tires de còmic noves: Còmic Memòria cau de còmic: Configura... No s'ha pogut crear l'arxiu en la ubicació especificada. Creació d'un arxiu de llibre de còmics %1 S'està creant l'arxiu de llibre de còmics Destinació: Mostra un error si falla l'obtenció del còmic Baixa còmics Gestió d'errors Ha fallat en afegir un fitxer a l'arxiu. Ha fallat en crear el fitxer amb l'identificador %1. Des del principi a...  Des del final a... General Aconsegueix còmics nous... Ha fallat l'obtenció de la tira còmica: Vés a la tira Informació Salta a la tira &actual Salta a la &primera tira Salta a la tira... Interval manual Potser no hi ha connexió a Internet.
Potser el connector de còmics està trencat.
Una altra raó podria ser que no hi ha cap còmic per este dia/número/text, i si en trieu un de diferent, podria funcionar. Cliqueu amb el botó del mig al còmic per veure'l a la mida original No existeix cap fitxer zip, es cancel·la. Interval: Mostra les fletxes només en passar el ratolí per sobre Mostra l'URL del còmic Mostra l'autor del còmic Mostra l'identificador del còmic Mostra el títol del còmic Identificador de tira: L'interval de les tires de còmic a arxivar. Actualitza Visita la web del còmic Visita la web de la &botiga Núm. %1 dies dd.MM.yyyy Pestanya següe&nt amb una tira nova Des de: A: minuts tires per còmic 