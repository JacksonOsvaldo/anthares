��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    #   �  
     	          /        N     \     p  1   �  6   �  '   �          "  6   /     f                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-09 20:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 B&loqueja la pantalla en reprendre: Activació Aparença Error Ha fallat en provar el bloqueig de la pantalla. Immediatament Drecera del teclat: Bloqueja la sessió Bloqueja la pantalla automàticament després de: Bloqueja la pantalla en eixir de l'estat de suspensió &Cal contrasenya després del bloqueig:  min.  mins.  seg.  segs. La drecera global de teclat per bloquejar la pantalla. &Tipus de fons d'escriptori: 