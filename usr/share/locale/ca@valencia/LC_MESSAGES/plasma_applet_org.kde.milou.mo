��          D      l       �   ;   �   <   �           	  �    M   �  J   ?     �     �                          Drag categories to change the order in which results appear Only the selected components are shown in the search results Search Search Results Project-Id-Version: plasma_applet_org.kde.milou
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-05-11 21:38+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 Arrossegueu les categories per canviar l'orde en què apareixen els resultats Només es mostren els components seleccionats en els resultats de la busca Busca Resultats de la busca 