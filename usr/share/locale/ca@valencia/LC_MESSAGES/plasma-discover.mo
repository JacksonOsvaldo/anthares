��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �     �  :   �  %   5  C   [  C   �  <   �  &         G  E   ]     �     �  &   �  &   �     $     A     Q     i     q     ~     �     �     �     �     �     �  
   �  
     )        <     R  &   g  9   �  (   �     �       A        ]     f  /   o     �  $   �  
   �     �     �     �  
                  +     2  $   >  "   c     �  $   �     �     �     �     �  ]     ;   i     �     �     �     �     �     �  D   �     <     B     T  	   ]     g     v     �     �     �     �     �  "   �     �       )        ?     G     U  "   [     ~     �     �     �     �     �      �     	  2   '         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: plasma-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-16 20:37+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 
També disponible a %1 %1 %1 (%2) %1 (Predeterminat) <b>%1</b> per %2 <em>%1 de %2 persones han trobat útil este comentari</em> <em>Parleu-nos d'este comentari!</em> <em>Útil? <a href='true'><b>Si</b></a>/<a href='false'>No</a></em> <em>Útil? <a href='true'>Si</a>/<a href='false'><b>No</b></a></em> <em>Útil? <a href='true'>Si</a>/<a href='false'>No</a></em> S'estan recuperant les actualitzacions S'està recuperant... Es desconeix quan es va fer la darrera comprovació d'actualitzacions S'estan cercant actualitzacions Cap actualització No hi ha cap actualització disponible Cal comprovar si hi ha actualitzacions El sistema està actualitzat Actualitzacions S'està actualitzant... Accepta Afig font... Complements Aleix Pol Gonzalez Un explorador d'aplicacions Aplica els canvis Dorsals disponibles:
 Modes disponibles:
 Arrere Cancel·la Categoria: S'estan comprovant les actualitzacions... Comentari massa llarg Comentari massa curt Mode compacte (auto/compacte/complet). No es pot tancar l'aplicació, hi ha tasques que cal fer. No s'ha pogut trobar la categoria «%1» No s'ha pogut obrir %1 Suprimeix l'origen Obri directament l'aplicació especificada pel seu nom de paquet. Descarta Discover Mostra una llista d'entrades amb una categoria. Extensions... Ha fallat en eliminar la font «%1» A destacar Ajuda... Pàgina web: Millora el resum Instal·la Instal·lat Jonathan Thomas Engega Llicència: Llista tots els dorsals disponibles. Llista tots els modes disponibles. S'està carregant... Fitxer local de paquet a instal·lar Converteix en predeterminat Més informació... Més... Cap actualització Obri el Discover en un mode indicat. Els modes es corresponen als botons de la barra d'eines. Obri amb un programa que pot gestionar el tipus MIME donat. Continua Puntuació: Elimina Recursos per «%1» Comenta S'està comentant «%1» L'execució com a <em>root</em> es desaconsella i és innecessària. Busca Busca a «%1»... Busca... Busca: %1 Busca: %1 + %2 Preferències Resum breu... Mostra les ressenyes (%1)... Mida: No s'ha trobat res... Font: Especifiqueu la nova font per a %1 Encara s'està cercant... Resum: Implementa l'esquema d'URL «appstream:» Tasques Tasques (%1%) %1 %2 No s'ha pogut trobar el recurs: %1 Actualitza-ho tot Actualitza els seleccionats Actualitza (%1) Actualitzacions Versió: revisor desconegut actualitzacions no seleccionades actualitzacions seleccionades © 2010-2016 l'equip de desenvolupament del Plasma 