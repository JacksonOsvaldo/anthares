��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �  
   �     �     �          .     B     N  	   [     e     v     �     �     �  \   �      	  7   &	     ^	  '   p	  	   �	     �	  "   �	     �	  	   �	  	   �	     �	     
     !
     7
  �   H
  Y   �
     <     Q     d                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-23 06:48+0100
Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Activitats Totes les altres activitats Tots els altres escriptoris Totes les altres pantalles Totes les finestres Alternativa Escriptori 1 Contingut Activitat actual Aplicació actual Escriptori actual Pantalla actual Filtra les finestres per L'arranjament de la política de focus limita la funcionalitat de navegar per les finestres. Avant Obtén una disposició nova pel commutador de finestres Finestres ocultes Inclou la icona «Mostra l'escriptori» Principal Minimització Només una finestra per aplicació Usats recentment Inverteix Pantalles Dreceres Mostra la finestra seleccionada Criteri d'ordenació: Orde d'apilament La finestra actualment seleccionada es realçarà esvaint totes les altres finestres. Esta opció requereix que els efectes d'escriptori estiguen actius. L'efecte de substituir la llista de finestres quan els efectes d'escriptori estan actius. Escriptoris virtuals Finestres visibles Visualització 