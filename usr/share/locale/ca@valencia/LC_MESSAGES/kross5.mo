��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     =     C     b     r     �     �     �     �  
   �     �     �     �     �     �  5   	  @   R	  )   �	  -   �	     �	     �	     �	  ,   
     3
     D
     I
     h
     �
     �
     �
  '   �
     �
  *   �
       :        P                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2015-05-09 17:41+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer L'script a executar. General Afig un script nou. Afig... Cancel·lo? Comentari: txemaq@gmail.com Edita Edita l'script seleccionat. Edita... Executa l'script seleccionat. Ha fallat en crear l'script per a l'intèrpret «%1» Ha fallat en determinar l'intèrpret pel fitxer de script «%1» Ha fallat en carregar l'intèrpret «%1» Ha fallat en obrir el fitxer de script «%1» Fitxer: Icona: Intèrpret: Nivell de seguretat de l'intèrpret del Ruby Josep Ma. Ferrer Nom: No existeix cap funció «%1» No existeix l'intèrpret «%1» Elimina Elimina l'script seleccionat. Executa El fitxer de script «%1» no existeix. Atura Atura l'execució de l'script seleccionat. Text: Utilitat de línia d'ordes per executar scripts del Kross. Kross 