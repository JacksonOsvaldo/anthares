��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .                 A   )     k     �     �     �  ,   �     �  &   �     &     .     ;     T     [     h     �  (   �  '   �     �  A   	  E   N	  
   �	     �	     �	  4   �	     �	     �	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: plasmashellprivateplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-08-30 20:16+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 &Executa Tots els estris Categories: Consola per crear scripts d'intèrpret d'ordes per a l'escriptori Baixa estris de Plasma nous Editor S'està executant l'script a %1 Filtres Instal·lació d'un estri des d'un fitxer... Ha fallat la instal·lació Ha fallat en instal·lar el paquet %1. Carrega Sessió nova Obri un fitxer de script Eixida En execució Temps d'execució: %1ms Guarda el fitxer de script El bloqueig de la pantalla està activat Temps d'espera de l'estalvi de pantalla Selecció d'un fitxer Plasmoide Estableix els minuts després dels quals es bloqueja la pantalla. Defineix si la pantalla s'ha de bloquejar després del temps indicat. Plantilles KWin Plasma No s'ha pogut carregar el fitxer de script <b>%1</b> No instal·lable Usa 