��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  ;   /  	   k     u     �  ?   �     �  3   �     /	  �   7	  �   �	  9   �
     �
       %   1  %   W  :   }  "   �     �  ,   �  /                                                      	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_org.kde.plasma.devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-02 16:57+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 1 acció per este dispositiu %1 accions per este dispositiu %1 lliure S'està muntant... Tots els dispositius Cliqueu per accedir a este dispositiu des d'altres aplicacions. Cliqueu per expulsar el disc. Cliqueu per extreure amb seguretat este dispositiu. General Actualment <b>no és segur</b> extreure este dispositiu: les aplicacions poden estar-hi accedint. Cliqueu el botó d'expulsió per extreure amb seguretat este dispositiu. Actualment <b>no és segur</b> extreure este dispositiu: les aplicacions poden estar accedint a altres volums d'este dispositiu. Cliqueu el botó d'expulsió d'estos altres volums per extreure amb seguretat este dispositiu. Actualment es pot extreure amb seguretat este dispositiu. Dispositiu més recent Sense dispositius disponibles Només els dispositius no extraïbles Configura els dispositius extraïbles Obri un missatge emergent quan s'endolli un dispositiu nou Només els dispositius extraïbles S'està desmuntant... Actualment este dispositiu està accessible. Actualment este dispositiu no està accessible. 