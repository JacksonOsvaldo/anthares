��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     	  )   	  &   ;	  ,   b	  ,   �	  +   �	     �	  
   �	     �	     
     
  i   
      �
  [   �
                    /     F     O     a     n     �     �     �     �     �  3   �  =   �     /     6     ;     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-03 19:32+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 100% Autor Copyright 2015 Harald Sitter Harald Sitter No hi ha cap aplicació reproduint àudio No hi ha cap aplicació gravant àudio No hi ha cap perfil de dispositiu disponible No hi ha cap dispositiu d'entrada disponible No hi ha cap dispositiu d'eixida disponible Perfil: PulseAudio Avançat Aplicacions Dispositius Afig un dispositiu d'eixida virtual per enviar simultàniament l'eixida a totes les targetes de so locals Configuració avançada d'eixida Canvia automàticament tots els fluxos en execució quan hi haja disponible una nova eixida Captura Predeterminat Perfils de dispositiu antonibella5@yahoo.com Entrades Silencia l'àudio Antoni Bella Sons de notificació Eixides Reproducció Port  (no disponible)  (desendollat) Requereix el mòdul «module-gconf» del PulseAudio Este mòdul permet configurar el subsistema de so PulseAudio. %1: %2 100% %1% 