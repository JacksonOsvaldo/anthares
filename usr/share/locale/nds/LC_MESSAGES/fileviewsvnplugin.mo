��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  �  �     ?  #   H  *   l  3   �  &   �     �       $   &  ,   K  5   x  V   �  *   	     0	     G	     f	     ~	     �	     �	     �	     �	     �	     
     
                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-01-12 10:06+0100
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Inspelen Dateien na't SVN-Archiev toföögt. Dateien warrt dat SVN-Archiev toföögt... Tofögen vun Dateien na't SVN-Archiev is fehlslaan. Inspelen vun SVN-Ännern is fehlslaan. SVN-Ännern inspeelt. SVN-Ännern warrt inspeelt... Dateien ut dat SVN-Archiev wegmaakt. Dateien warrt ut dat SVN-Archiev wegmaakt... Wegmaken vun Dateien ut dat SVN-Archiev is fehlslaan. SVN-Status lett sik nich opfrischen. Funkschoon "SVN-Opfrischen wiesen" warrt utmaakt. Opfrischen vun't SVN-Archiev is fehlslaan. SVN-Archiev opfrischt. SVN-Archiev warrt opfrischt... Na SVN-Archiev tofögen Na SVN-Archiev inspelen... Ut SVN-Archiev wegmaken SVN-Archiev opfrischen Lokaal SVN-Ännern wiesen SVN-Opfrischen wiesen Beschrieven: Na SVN-Archiev inspelen Opfrischen wiesen 