��          �   %   �      0  U   1     �     �  
   �     �     �     �     �     �     �  	   �                     .  )   4  (   ^  '   �  "   �     �     �  9   �  J   #  �  n           6     B     P     `     p     �     �     �     �     �     �     �     �     �  3     6   5  1   l  "   �     �  	   �     �  #   �                                                                            
                           	                        Activities a window is currently on (apart from the current one)Also available on %1 Alphabetically By Activity By Desktop By Program Name Do Not Group Do Not Sort Filters General Grouping and Sorting Grouping: Highlight windows Manually Maximum rows: On %1 Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show tooltips Sorting: Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2014-02-19 12:41+0100
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 Ok op %1 verföögbor Alfabeetsch Na Aktiviteet Na Schriefdisch Na Programmnaam Nich tosamenkoppeln Nich sorteren Filtern Allmeen Tohoopkoppeln un Sorteren Tosamenkoppeln: Finstern rutheven Vun Hand Gröttst Regentall: Op %1 Bloots Programmen vun de aktuelle Aktiviteet wiesen Bloots Programmen op den aktuellen Schriefdisch wiesen Bloots Programmen vun den aktuellen Schirm wiesen Bloots minimeert Programmen wiesen Kortinfos wiesen Sorteren: Verföögbor binnen %1 Binnen all Aktiviteten verföögbor 