��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     _     p  1   �  G   �  ?   �  =   ;  =   y  T   �  T   	     a	  	   w	  	   �	     �	     �	     �	     �	     �	     �	                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-10 14:03+0100
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 Gelaats afsluten Schiev rutfohren Na Reedschappen söken, de ehr Naam op :q: passt. List all Reedschappen op un lett Di se inhangen, afhangen un rutfohren. List all rutfohrbor Reedschappen op un lett se Di ok rutfohren. List all inhangbor Reedschappen op un lett se Di ok inhangen. List all afhangbor Reedschappen op un lett se Di ok afhangen. List all verslötelt Reedschappen op de sik afsluten laat un lett se Di ok afsluten. List all verslötelt Reedschappen op de sik opsluten laat un lett se Di ok opsluten. De Reedschap inhangen Reedschap Rutfohren Afsluten Inhangen Opsluten Afhangen Gelaats opsluten De Reedschap afhangen 