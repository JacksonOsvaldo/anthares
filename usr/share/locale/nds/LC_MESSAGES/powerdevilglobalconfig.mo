��          �      �       0     1  *   3  >   ^  9   �     �     �  
   	        	   5     ?     Q  �   p  �  �     �  *   �  1   �  -   �     ,     C     Z  "   c     �     �     �  a   �                                      
                	    % <b>Battery Levels                     </b> Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Hibernate Low battery level NAME OF TRANSLATORSYour names The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2014-06-05 16:25+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 % <b>Batteriestopen                     </b> De Batterie langt bi disse Stoop bi "Kritisch" an De Batterie langt bi disse Stoop bi "Siet" an Bescheden instellen... Kritisch-Batteriestoop Nix doon s_dibbern@web.de, m.j.wiese@web.de Infreren Siet-Batterie-Stoop Sönke Dibbern, Manfred Wiese As dat lett löppt dat Stroomkuntrullsysteem nich.
Dat lett sik över "An- un Utmaken" instellen. 