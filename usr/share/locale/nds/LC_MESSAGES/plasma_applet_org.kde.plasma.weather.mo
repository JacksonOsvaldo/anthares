��          �   %   �      P     Q  '   h  "   �     �  
   �     �     �     �  ?        T     Z     h  &   |      �  %   �     �  >        C     b  '   y  !   �     �     �       3      �  T                 	   %  
   /     :     C     J     L  	   \     f     ~     �     �     �     �     �     �     �     �     �                )     >              
             	                                                                                                  Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Kilometers Low temperatureLow: %1 Miles Short for no data available- Shown when you have not set a weather providerPlease Configure Units Update every: Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2009-09-09 23:03+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 ° 1 Dag %1 Daag H: %1 S: %2 Hooch: %1 Kilometers Siet: %1 Mielen - Bitte instellen Eenheiten Opfrischen jümmers na: Still Fuchtigkeit: %1%2 Sichtigkeit: %1 %2 Daupunkt: %1 Föhlt Hitt: %1 Drucktendenz: %1 Druck: %1 %2 %1%2 Sichtigkeit: %1 Rutgeven Wohrschoen: Rutgeven Beluern: %1 %2 %3 Föhlt Windküll: %1 Windböen: %1 %2 