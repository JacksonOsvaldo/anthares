��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  o     
     
  8   .
  9   g
  
   �
     �
     �
     �
  &   �
  k        �     �     �     �     �     �     �     �     �     �     �     �     
  
                                  	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: krunner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-19 06:27+0100
Last-Translator: Manfred Wiese <m.j.wiese@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
 <p>Du wullt noch en Schriefdischtörn opmaken. <br />De aktuelle Törn warrt versteken un en nieg Anmellschirm wiest. <br />För elkeen Törn gifft dat en Funkschoontast, normalerwies F%1 för den eersten, F%2 för den tweten usw. Du kannst de Törns wesseln, wenn Du Strg, Alt un de tohören F-Tast op eenmaal drückst. Du kannst ok de Akschonen binnen dat KDE-Paneel oder dat Schriefdischmenü bruken.</p> List all Törns op Den Schirm afsluten Slutt den aktuellen Törn af un röppt den Pausschirm op Meldt Di af, de aktuelle Schriefdischtörn warrt utmaakt. Nieg Törn Start den Reekner nieg Den Reekner nieg starten Den Reekner utmaken Start en nieg Törn as en anner Bruker Wesselt na den aktiven Törn vun den Bruker :q:, oder list all aktiev Törns op, wenn Du :q: nich angiffst. Maakt den Reekner ut Törns Wohrschoen - Nieg Törn lock afmellen Afmellen Afmellen new session reboot restart shutdown Bruker wesseln switch switch :q: 