��    .      �  =   �      �     �  ;   �  9   2  (   l  ;   �  9   �  8     $   D     i          �  /   �      �     �  ,        0     @  N   P  
   �      �  	   �  �   �     _     k  e   �     �             
   (     3     G     `     g  &   x  %   �  f   �  8   ,	  ;   e	     �	     �	     �	     �	  �   �	  "   �
  C   �
  �  �
     �     �     �     �     �     �  !   	     +  !   1     S     e  .   h  "   �     �     �     �     �  _        l  "   u     �  �   �     %     5  {   S     �     �               )     C     V  
   ]      h  "   �  h   �  @     I   V     �     �  %   �  !   �  �        �  H   �                             #          $      
   "   %   	                              !   .             &            -          +          *                                 ,   '                (   )                min @action:inmenu Global shortcutDecrease Keyboard Brightness @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Keyboard Brightness @action:inmenu Global shortcutIncrease Screen Brightness @action:inmenu Global shortcutToggle Keyboard Backlight @label:slider Brightness levelLevel AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Battery Critical (%1% Remaining) Battery Low (%1% Remaining) Brightness level, label for the sliderLevel Can't open file Charge Complete Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Running on AC power Running on Battery Power Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Your battery is low. If you need to continue using your computer, either plug in your computer, or shut it down and then change the battery. Your battery is now fully charged. Your battery level is critical, save your work as soon as possible. Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2014-08-11 12:48+0200
Last-Translator: Sönke Dibbern <s_dibbern@web.de>
Language-Team: Low Saxon <kde-i18n-nds@kde.org>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
  Min. Tastatuur düüsterer maken Schirm düüsterer maken Infreren Tastatuur heller maken Schirm heller maken Tastatuur-Achterlicht an-/utmaken Stoop Stroomkavel is dat Nett tokoppelt Aktivitetenpleger Na All anstahn Utsett-Akschonen wöörn afbraken. Batterie is "Kritisch" (%1% över) Batterie is "Siet" (%1% över) Stoop Datei lett sik nich opmaken Opladen afslaten Tokoppeln na Batterie-Koppelsteed nich mööglich.
Prööv bitte de Instellen vun Dien Systeem. Nix doon s_dibbern@web.de, m.j.wiese@web.de Infreren Dat KDE-Stroomkuntrullsysteem lett sik nich torechtmaken. Dat Hülpprogramm geev dissen Fehler ut: %1
Bitte prööv Dien Instellen. Schirm afsluten Sönke Dibbern, Manfred Wiese Keen gellen Hülpprogramm-Modulen för de Stroomkuntrull verföögbor. Villicht lööst en Nieginstallatschoon dat Problem. Bi't Laden vun en Profil Bi't Afladen vun en Profil Afmelldialoog wiesen Skript utföhren Löppt över Wesselstroom Löppt op Batterie Skript Utmaken na Dat Stroomkavel wöör insteken. Dat Stroomkavel wöör ruttrocken. Du hest dat Profil "%1" utsöcht, man dat gifft dat gor nich.
Prööv bitte Dien Stroomdüvel-Instellen. De Regeln för disse Aktiviteet verhöödt de Schirmstroompleeg. De Regeln för disse Aktiviteet verhöödt, dat de Reekner inslapen deit. Schirm utmaken Nich ünnerstütt Utsett-Metood Wenn Klappreeknerdeckel tomaakt warrt Bi't Drücken vun den Anmaakknoop Dien Batterieladen is siet. Wenn Du Dien Reekner wiederbruken muttst, koppel em Nettstroom to oder maak em ut un wessel de Batterie. Dien Batterie is heel laadt. Dien Batteriestoop is kritisch, seker Dien Arbeit sodraad as mööglich! 