��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �  I   �     $     :     K  u   b     �  ^   �     S  c   r  �   �  �   [	  R   �	  f   D
     �
     �
     �
  N   �
  �   ?       :     7   Z     �  Z   �       S     	   j  {   t     �                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-11-04 23:55+0630
Last-Translator: Bhuvan Krishna <bhuvan@swecha.org>
Language-Team: Telugu <kde-i18n-doc@kde.org>
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4;
 సాధారణ కొత్త స్క్రిప్ట్ ను చేర్చు. చేర్చు... రద్దు? వ్యాఖ్య: infyquest@gmail.com,kkrothap@redhat.com,bhuvan@swecha.org,gvs.giri@swecha.net,sreekalyan3@gmail.com,naveen@swecha.net సరిదిద్దు ఎంచుకొన్న స్క్రిప్ట్ ను సరిదిద్దు. సరిదిద్దు... ఎంచుకొన్న స్క్రిప్ట్ ను మొదలుపెట్టు "%1" దుబాసీ కొరకు స్క్రిప్ట్ ను తయారు చెయలేకపొయాను . "%1" స్క్రిప్ట్ దస్త్రం కొరకు దుబాసీ నిర్ణయించ లేకపొయాను. "%1" దుబాసీ ని ఎక్కించ లేకపొయాను . "%1" స్క్రిప్ట్ దస్త్రం తెరువలేక పొయాను. దస్త్రం: ప్రతిమలు: దుబాసీ: రూబీ వ్యాఖ్యాత భద్రతా స్థాయి విజయ్ కిరణ్ కముజు,కృష్ణబాబు కె, భువన్ కృష్ణ, జి వి యస్ గిరి, శ్రీ కళ్యన్న్, ననిన్ పేరు: "%1" ఇలాంటి ప్రమేయం లేదు "%1" ఇలాంటి దుబాసీ లేదు తొలగించు ఎంచుకొన్న స్క్రిప్ట్ ను తొలగించు నడుపు "%1" స్క్రిప్ట్ దస్త్రం కనపడలేదు. ఆగు ఎంచుకొన్న స్క్రిప్‍ట్‌యొక్క నిర్వర్తనను ఆపు వచనం: 