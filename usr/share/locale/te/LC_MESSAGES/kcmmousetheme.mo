��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  [  �  �   Y  A  X     �	  M   �	     
  n   
  &   �
  T   �
     �
  H   	  .   R  �   �  �   "  t           	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2009-01-16 22:16+0530
Last-Translator: Krishna Babu K <kkrothap@redhat.com>
Language-Team: Telugu <en@li.org>
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4;
X-Generator: KBabel 1.11.4
 <qt>మీరు ఖచ్చితంగా <i>%1</i> కర్సర్ థీమ్‌ను తీసివేద్దామని అనుకొనుచున్నారా?<br />ఈ థీమ్ ద్వారా సంస్థాపించబడిన అన్ని ఫైళ్ళను తొలగిస్తుంది.</qt> <qt>మీరు ప్రస్తుతం వుపయోగిస్తున్న థీమ్‌ను మీరు తొలగించలేరు.<br />మీరు ముందు వేరొక థీమ్‌కు మారాలి.</qt> %1 నామముతో వున్న థీమ్ యిప్పటికే మీ ప్రతిమ థీమ్ ఫోల్డర్‌లో వుంది. మీరు దానిని దీనితో పునఃస్థాపించుదామని అనుకొనుచున్నారా? నిర్ధారణ కర్సర్ అమర్పులు మార్చబడినవి వర్ణన థీమ్ URLను లాక్కోనితెమ్ము లేదా టైపు చేయుము k.meetme@gmail.com,infyquest@gmail.com కృష్ణబాబు కె , విజయ్ కిరణ్ కముజు పేరు థీమ్‌ను వోవర్‌రైట్ చేయాలా? థీమ్‌ను తీసివేయి ఫైలు %1 చెల్లునటువంటి కర్సర్ థీమ్ ఆర్చివ్ వలె అనిపించుటలేదు. కర్సర్ థీమ్ ఆర్చివ్‌ను డౌనులోడు చేయలేక పోయింది, చిరునామా %1 సరైనదేనేమో దయచేసి పరిశీలించండి. కర్సర్ థీమ్ ఆర్చివ్‌ను %1 కనుగొనలేక పోయింది. 