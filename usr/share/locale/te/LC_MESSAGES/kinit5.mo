��    
      l      �       �      �   $        6     S     s     �     �  '   �     �  �  �  <   �  1     (   5  P   ^  R   �  3     Z   6  f   �  ;   �           
           	                       Could not find '%1' executable. Could not find 'kdemain' in '%1'.
%2 Could not find service '%1'. Could not open library '%1'.
%2 KDEInit could not launch '%1' Launching %1 Service '%1' is malformatted. Service '%1' must be executable to run. Unknown protocol '%1'.
 Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-04 03:08+0100
PO-Revision-Date: 2013-11-04 23:55+0630
Last-Translator: Bhuvan Krishna <bhuvan@swecha.org>
Language-Team: Telugu <kde-i18n-doc@kde.org>
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : 4;
 '%1' కార్యక్రమం కనపడలేదు '%1'లో 'kdemain' కనపడలేదు.
%2 '%1' సేవ కనపడలేదు. '%1' లైబ్రరీ ని తెరువలేకపోయాను.
%2 కెడిఈ '%1' ప్రారంభించలేకపొయింది. %1 ప్రారంభిస్తున్నా '%1' సేవ తప్పుగా ఫార్మేట్ చేయబడినది. సేవా '%1' నడుపుటను నిర్వర్తించగదు తప్పక. తెలియని '%1' ప్రొటోకాల్.
 