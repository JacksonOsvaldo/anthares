��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  0  y  
   �  (   �     �  (   �  
   '	  #   2	     V	     d	     v	  %   �	     �	  +   �	     �	  -   �	  \   )
  Y   �
  C   �
  B   $  	   g     q     �  I   �  9   �     !  "   )  .   L     {  -   �     �  7   �     �  F        S  d   _     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2017-09-28 17:58+0200
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
X-Wrapping: fine
 Аутор © 2006, Себастијан Сауер Себастијан Сауер Скрипта за извршавање Опште Додај нову скрипту. Додај... Одустати? Коментар: toptan@kde.org.yu,caslav.ilic@gmx.net Уређивање Уреди изабрану скрипту. Уреди... Изврши изабрану скрипту. Не могу да направим скрипту за интерпретатор „%1“. Не могу да одредим интерпретатор за скрипту ‘%1’ Не могу да учитам интерпретатор „%1“ Не могу да отворим фајл скрипте ‘%1’ Фајл: Иконица: Интерпретатор: Ниво безбедности интерпретатора рубија Топлица Танасковић,Часлав Илић Име: Нема функције „%1“ Нема интерпретатора „%1“ Уклони Уклони изабрану скрипту. Изврши Скриптни фајл ‘%1’ не постоји. Заустави Заустави извршавање изабране скрипте. Текст: Алатка командне линије за извршавање Кросових скрипти Крос 