��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  ,  �  n   �  ,   !  $   N     s  (   �      �  +   �            E        _  7   l  J   �     �  0   �  
   '     2     K  V   [  -   �  5   �       N   *  %   y     �  =   �  T   �     N     j  
   �  )   �  6   �     �       (     $   =     b     �  G  �  [   �  1   :     l  <   x     �     �  '   �     	  (   #  G   L     �  )   �  2   �            
     1   !     S     Y     _      l     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-03-14 17:59+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 

Изаберите претходни стрип да пређете на последњи кеширани. &Направи архиву стрипа... &Сачувај стрип као... Број &стрипа: *.cbz|архива стрипа (ЗИП) &Стварна величина Запамти текући &положај Напредно сви Дошло је до грешке за идентификатор %1. Изглед Архивирање стрипа није успело Аутоматски ажурирај стрипске прикључке: Кеш Тражи нове каишеве стрипа: Стрип Кеш стрипова: Подеси... Не могу да направим архиву на задатој локацији. Стварање архиве стрипа %1 Стварање фајла архиве стрипа Одредиште: Приказ грешке кад не успе добављање стрипа Преузимање стрипова Обрада грешака Неуспело додавање фајла у архиву. Неуспело стварање фајла са идентификатором %1. од почетка до... од краја до... Опште Добави нове стрипове... Добављање стрипа није успело: Скок на стрип Подаци Скочи на &текући стрип Скочи на &први стрип Скочи на стрип... ручни опсег Можда не ради веза са Интернетом.
Можда је прикључак стрипа покварен.
Такође може бити да нема стрипа за овај дан, број или ниску, па добављање може успети ако изаберете неки други. Средњи клик на стрип даје његову изворну величину Нема ЗИП фајла, обустављам. Опсег: Стрелице само при преласку мишем УРЛ стрипа Аутор стрипа Идентификатор стрипа Наслов стрипа Идентификатор стрипа: Опсег стрипских каишева за архивирање. Ажурирање Посети веб сајт стрипа Посети &веб сајт продавнице бр. %1 дана dd.MM.yyyy &Нови језичак за нови стрип Од: До: минута каишева по стрипу 