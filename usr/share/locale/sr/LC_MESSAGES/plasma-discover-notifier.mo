��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  &       �  �   �  �   I  �   �  -   �  �   �  9   �     �  "   �            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: muon-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2015-11-14 16:11+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 %1, %2 %1 пакет за ажурирање %1 пакета за ажурирање %1 пакета за ажурирање %1 пакет за ажурирање %1 безбедносна допуна %1 безбедносне допуне %1 безбедносних допуна %1 безбедносна допуна %1 пакет за ажурирање %1 пакета за ажурирање %1 пакета за ажурирање %1 пакет за ажурирање Нема пакета за ажурирање од чега је %1 допуна безбедносна од чега су %1 допуна безбедносне од чега је %1 допуна безбедносних од чега је %1 допуна безбедносна Доступне су безбедносне допуне Систем је ажуран Доступне су допуне 