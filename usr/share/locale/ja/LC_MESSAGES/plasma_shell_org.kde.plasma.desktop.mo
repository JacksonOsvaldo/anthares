��    :      �  O   �      �  
   �  
     
             )     -     A     P     V     e  	   m     w     ~  
   �     �     �     �     �     �     �     �     �     �  
   �     �     
               #     ,     9     H     M     [     l     z     }     �     �     �     �  	   �     �     �     �  b   �  �   G     �  	   �     �  
     	             )     1     7     I  �  Z     A
     W
     p
     �
     �
     �
     �
     �
     �
     �
     �
                    %  	   ,  !   6     X     ]     d  $   {     �     �     �  $   �     �                     "     A     W     \     l     �     �     �     �     �     �     �  	   �     �  "   �     !  ~   .    �     �     �     �     �     �     �            '   "  -   J     $         7   
            0   6                8      :   )                -   !      4      	   '   2               "      *                9           .          1   /      &      (      +               5       #                              ,                       %          3    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Author: Auto Hide Bottom Cancel Categories Center Close Create activity... Ctrl Delete Email: Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Uninstall Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2015-09-26 17:22-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.5
 アクティビティ アクションを追加 スペーサーを追加 ウィジェットを追加... Alt 代替ウィジェット 常に表示 適用 設定を適用 作者: 自動的に隠す 下 キャンセル カテゴリ 中央 閉じる アクティビティを作成... Ctrl 削除 メールアドレス: 新しいウィジェットを入手 高さ 横スクロール ここに入力 キーボードショートカット レイアウト: 左 左ボタン ライセンス: ウィジェットをロック パネルを最大化 Meta 中央ボタン その他の設定... マウスアクション OK パネルの配置 パネルを削除 右 右ボタン スクリーンエッジ 検索... Shift 停止したアクティビティ: 切り替え 現在のモジュールの設定が変更されています。変更を適用しますか？それとも破棄しますか？ このショートカットはアップレットをアクティブにします。アップレットにキーボードフォーカスが与えられ、スタートメニューのようなポップアップを持つ場合は、そのポップアップが開かれます。 上 アンインストール 縦スクロール 表示 壁紙 壁紙の種類: ウィジェット 幅 ウィンドウはパネルを覆える ウィンドウはパネルの下に隠れる 