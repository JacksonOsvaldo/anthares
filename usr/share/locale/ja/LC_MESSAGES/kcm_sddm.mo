��            )   �      �     �  �   �     9     B     I     U     a     m     v     �     �     �      �     �     �     �     �               '     F     K     `     p     �      �     �     �     �  �  �     �     �     �     �     �     �     �     �               =     D     K     Z     a  !   u  /   �     �     �     �     �  '   �        !   7     Y  #   j  	   �  	   �  
   �                                                            	   
                                                                          ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Cursor Theme: Customize theme Default Description EMAIL OF TRANSLATORSYour emails General Halt Command: Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Session: The default cursor theme in SDDM Theme User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2015-05-02 15:06-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.5
 ... (利用可能な大きさ: %1) 詳細設定 作者 自動ログイン(&L) 背景: 画像をクリア コマンド カーソルテーマ: テーマをカスタマイズ 標準 説明 ftake@geeko.jp 一般 終了コマンド: ファイルから読み込み... SDDM を利用したログインスクリーン 最大 UID: 最小 UID: Fuminobu Takeyama 名前 プレビューは利用できません 再起動コマンド: 終了後に再ログインする セッション: SDDM の標準カーソルテーマ テーマ ユーザ ユーザ: 