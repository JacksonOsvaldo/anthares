��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  C   �
     �
  "   �
     �
  $        3  "   R     u     �     �     �  !   �  H   �  	             %  -   :     h  	   o     y  <   �  $   �  (   �          $     3  !   @  K   b  9   �     �  6   �     /  *   6     a  i   x  o   �  |   R     �     �  �   �     �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2010-06-13 02:02+0900
Last-Translator: Taiki Komoda <kom@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.0
 外部アプリケーション %1 を自動的に起動しました (c) 2009, Ben Cooksley (1 アイテム) (%1 アイテム) %1 について 現在のモジュールについて 現在のビューについて KDE システム設定について 設定を適用 作者 Ben Cooksley 設定 あなたのシステムを設定 詳細なツールチップを使用するかどうかを指定します 開発者 ダイアログ ybando@k6.dion.ne.jp 最上位レベルを自動的に展開する 全般 ヘルプ アイコン表示 内部モジュール表現、内部モジュールモデル 使用されるビューの内部名 キーボードショートカット: %1 メンテナ Mathias Soeken Yukiko Bando ビューが見つかりません 設定モジュールをカテゴリ別にアイコンで表示します。 設定モジュールをツリー状に表示します。 %1 を再起動 現在のすべての変更を元の値に戻します 検索 詳細なツールチップを表示する KDE システム設定 システム設定がビューを見つけられなかったので表示するものはありません。 システム設定がビューを見つけられなかったので設定が可能なものはありません。 現在のモジュールの設定が変更されました。
変更を適用しますか？それとも破棄しますか？ ツリー表示 表示スタイル KDE システム設定へようこそ。ここはあなたのコンピュータシステムを設定するための中心となる場所です。 Will Stephenson 