��    )      d  ;   �      �  !   �  "   �  &   �  #     &   )  !   P  &   r  '   �  "   �  #   �  "     '   +     S     W     d     k     s     �     �     �     �     �     �  !   �       	              (  &   ;     b     i     �     �  $   �     �     �     �     �     
       �  1     �     	  	   	     	     	     $	     (	  	   5	     ?	     C	     J	  	   N	     X	     _	     p	     w	     �	  *   �	     �	  (   �	     �	     

  6   
  0   H
     y
     }
     �
     �
  -   �
     �
  *   �
            E     '   ]     �     �  *   �     �          	       '                              (   $                   )                        !   #                                    &                             %                      
   "                  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Border size: Center Class:  Decoration Options Detect Window Properties Edit Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up Regular Expression Regular Expression syntax is incorrect Remove Remove selected exception? Right Title:  Use window class (whole application) Use window title Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2010-06-12 23:54+0900
Last-Translator: Taiki Komoda <kom@kde.gr.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 とても大 大 枠なし 普通 特大 小 さらに大 より大 大 普通 小 より大 追加 枠のサイズ: 中央 クラス:  装飾のオプション ウィンドウのプロパティを検出 編集 この例外を有効/無効にします 例外のタイプ 全般 ウィンドウのタイトルバーを表示しない 選択されたウィンドウに関する情報 左 下へ移動 上へ移動 正規表現 正規表現に構文エラーがあります 削除 選択した例外を削除しますか？ 右 タイトル:  ウィンドウのクラスを使う (アプリケーション全体) ウィンドウのタイトルを使う ウィンドウのクラス名 ウィンドウの識別 ウィンドウのプロパティの選択 ウィンドウのタイトル ウィンドウ固有の設定 