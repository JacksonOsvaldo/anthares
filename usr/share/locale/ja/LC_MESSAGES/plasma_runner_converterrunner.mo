��          4      L       `   �   a   L   �   �  E  �        �                    Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: krunner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2010-01-21 23:28+0900
Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 :q: として “値 単位 [>, to, as, in] 単位” の形で与えられた値を変換します。使用可能な単位は Plasma ウィジェット “単位コンバータ” で確認できます。 in;to;as 