��    $      <  5   \      0     1     :     A  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a     ~  X   �     �     �          +     1     H  
   X  
   c     n     �     �     �     �     �     �     �  	           �  (     	     	     	     3	     :	     S	     Z	     j	  	   z	  <   �	  $   �	     �	     �	     
  -   
     F
  p   e
     �
  +   �
     	  	     *         K  
   d  
   o     z     ~     �  %   �     �     �     �  #   �          )        "       !                #                             $                                                       
                        	                                         Accurate Always Animation speed: Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Full screen repaints Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant Keep window thumbnails: NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX Re-use screen content Rendering backend: Scale method: Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2015-05-22 22:20-0700
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.5
 正確 常に アニメーションの速度: 自動 アクセシビリティ 外観 キャンディ フォーカス ツール 仮想デスクトップの切り替えアニメーション ウィンドウマネージメント フィルタの設定 鮮明 ybando@k6.dion.ne.jp コンポジタを起動時に有効にする フルスクリーン再描画 【ヒント】 それぞれの効果の使い方については、効果の設定を参照してください。 即時 ウィンドウのサムネイルを保つ: Yukiko Bando しない 表示されているウィンドウのみ チープな場合のみ OpenGL 2.0 OpenGL 3.1 EGL GLX 画面の内容を再利用 レンダリングバックエンド: 拡大方法: スムーズ スムーズ (低速) ティアリング防止 ("vsync"): 非常に遅く XRender 