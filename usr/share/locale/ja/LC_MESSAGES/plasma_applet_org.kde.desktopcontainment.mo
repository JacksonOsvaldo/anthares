��    <      �  S   �      (     )     1     B     Q     W     ^     j     {     �     �     �  
   �     �     �     �     �     �  
   �     �     �               "     )     ?     M  	   [     e     y          �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                  .     B  )   P     z     �     �     �     �     �     �     �     �  �  �  
   �	     �	     
  
   
     $
     5
     I
     i
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  6        H     e     |  -   �     �     �     �  $   �          (     ,     0  	   7  !   A     c     j     q  !   t     �     �     �     �     �  !   �     �     �  !     $   &     K  K   \  '   �  	   �     �     �  
   �     �       &        >            %       0       #                        7      &                      5      3       '            :   +   .      4   9                                              
       8   1      ,   $          !      /      ;   -          <   2       	                   6      *          )   "   (       &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Align Arrange In Cancel Columns Custom title Date Default Descending Deselect All Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Hide Files Matching Icons Large Left Location Locked More Preview Options... Name None OK Preview thumbnails Remove Resize Right Rotate Rows Search file type... Select All Select Folder Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Size Small Sort By Sorting: Specify a folder: Type Type a path or a URL here Unsorted Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2015-05-24 17:08+0900
Last-Translator: Fuminobu TAKEYAMA <ftake@geeko.jp>
Language-Team: Japanese <kde-i18n-doc@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 1.5
 削除(&D) ごみ箱を空にする(&E) ごみ箱に移動(&M) 開く(&O) 貼り付け(&P) プロパティ(&P) デスクトップを更新(&R) ビューを更新(&R) 再読み込み(&R) 名前変更(&R) 整列 次で整列 キャンセル 列 カスタムタイトル 日付 標準 降順 すべて選択解除 ここにカスタムタイトルを入力します。 ファイル名パターン: ファイルタイプ: フィルタ フォルダプレビューポップアップ フォルダを先に表示 フォルダを先に フルパス マッチするファイルを隠す アイコン 大 左 場所 ロック プレビューの詳細設定... 名前 なし OK サムネイルでプレビュー 削除 サイズ変更 右 回転 行 ファイルタイプを検索... すべて選択 フォルダを選択 すべてのファイルを表示 マッチするファイルを表示 場所を表示: 現在のアクティビティに割り当てられたファイルを表示 デスクトップフォルダを表示 サイズ 小 ソート基準 ソート: フォルダを指定: 種類 パスまたは URL を入力します ソートなし 