��          �      |      �     �  '     "   0     S     m     �  ?   �     �  &   �        %   ?     e  >        �     �  '   �  !        >     ^     }  3   �  �  �     �     �     �  
   �  
   �     �     �     �     �     �  
     #        7     H     V  
   [     f     z     �  
   �     �                                                                       	   
                       Degree, unit symbol° Forecast period timeframe1 Day %1 Days High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Low temperatureLow: %1 Short for no data available- Shown when you have not set a weather providerPlease Configure Wind conditionCalm content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind direction, speed%1 %2 %3 windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2010-01-12 22:56+0900
Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 ° 1 日 %1 日 最高: %1 最低: %2 最高: %1 最低: %1 - 設定してください 無風 湿度: %1%2 視程: %1 %2 露点: %1 湿温度不快指数 (Humidex): %1 気圧傾向: %1 気圧: %1 %2 %1%2 視程: %1 発表中の警報: 発表中の注意報: %1 %2 %3 突風: %1 突風: %1 %2 