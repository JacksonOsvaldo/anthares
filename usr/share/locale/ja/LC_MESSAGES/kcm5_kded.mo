��          �      |      �     �                (     I     ]     u     �     �     �     �     �     �     �  �   �     l  #   �  @   �  ?   �     )     -  �  5               -     4     O  $   o     �  	   �  	   �     �     �  $   �     �     �  �   �      �  .   �  Q   �  Q   F     �     �     
                                         	                                                   (c) 2002 Daniel Molkentin Daniel Molkentin Description EMAIL OF TRANSLATORSYour emails KDE Service Manager Load-on-Demand Services NAME OF TRANSLATORSYour names Not running Running Service Start Startup Services Status Stop This is a list of available KDE services which will be started on demand. They are only listed for convenience, as you cannot manipulate these services. Unable to contact KDED. Unable to start server <em>%1</em>. Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i> Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i> Use kcmkded Project-Id-Version: kcmkded
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-16 03:36+0100
PO-Revision-Date: 2008-07-15 23:26+0900
Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
 (c) 2002 Daniel Molkentin Daniel Molkentin 説明 shinobo@leo.bekkoame.ne.jp KDE サービスマネージャ 要求時に開始するサービス Noboru Sinohara 停止中 実行中 サービス 開始 起動時に開始するサービス 状態 停止 必要に応じて開始される KDE サービスの一覧です。これらは参考までに表示しているだけで、操作することはできません。 KDED に接続できません。 サーバ “%1” を起動できません。 <i></i>サービス “%1” を開始できません。<br /><br />エラー: %2 <i></i>サービス “%1” を停止できません。<br /><br />エラー: %2 使用 kcmkded 