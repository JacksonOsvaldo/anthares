��          �      \      �     �     �     �     �        	        %  &   ?     f     }     �  	   �     �  �   �  �   K  t   �  7   T  +   �     �  �  �     �     �  	   �     �     �     �  *     0   7     h     �  *   �     �     �  �   �  �   �  �   Z	     
  '   '
  	   O
                          
                          	                                                min Act like Always Define a special behavior Don't use special settings Hibernate Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2018-02-04 18:56-0800
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 分 振る舞い いつも 特別な動作の設定 特別な設定をしない ハイバネーション ディスプレーの電源を切らない シャットダウンやサスペンドしない AC アダプタで動作 バッテリーで動作 残りわずかなバッテリーで動作 シャットダウン サスペンド 電源管理サービスが起動していません。
「スタートアップとシャットダウン」の中で起動するように指定してください。 アクティビティサービスが起動していません。
アクティビティのための電源管理を行うためにはアクティビティサービスを起動する必要があります。 アクティビティサービスは必要最小限の機能だけ起動されています。アクティビティ名とアイコンが使用できない場合があります。 アクティビティ "%1" 個別の設定 (上級ユーザのみ) 何分後 