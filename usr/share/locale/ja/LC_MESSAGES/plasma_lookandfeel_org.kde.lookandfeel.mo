��    (      \  5   �      p     q     u  .   z     �     �     �     �  	   �     �     �     
       1   -     _     e     r     �  '   �     �     �  '   �     �                  5        N     `     h     o     ~  $   �  A   �     �     �  C   	  '   M     u     ~  �  �     �     �     �     �     �  	   �     �     �     �  %   	     5	     E	  4   _	     �	     �	     �	     �	     �	     �	  	   
  .   
     =
     M
  	   T
     ^
  @   t
  !   �
     �
     �
     �
       	   ,     6     H     X  !   q     �     �     �                        '               #   &          $         
                     	   (   %      !                                                                       "           %1% Back Button to change keyboard layoutSwitch layout Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Nobody logged in on that sessionUnused Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: plasma_lookandfeel_org.kde.lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2016-10-09 13:53+0900
Last-Translator: Fuminobu Takeyama <ftake@geeko.jp>
Language-Team: Japanese <kde-i18n-doc@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 %1% 戻る 配列を切り替え キャンセル Caps Lock 有効 閉じる 検索を閉じる 設定 検索プラグインを設定 デスクトップセッション: %1 別のユーザ キーボード配列: %1 1秒後にログアウト %1秒後にログアウト ログイン ログイン失敗 別のユーザでログイン  ログアウト 使用者なし パスワード 再起動 1秒後にログアウト %1秒後に再起動 最近の検索 削除 再起動 シャットダウン 1秒後にシャットダウン %1秒後にシャットダウン 新しいセッションを開始 サスペンド 切り替え セッションを切り替え ユーザを切り替え 検索... '%1' を検索... ロック解除 ロック解除に失敗 on TTY %1 (ディスプレイ %2) TTY %1 ユーザ名 %1 (%2) 