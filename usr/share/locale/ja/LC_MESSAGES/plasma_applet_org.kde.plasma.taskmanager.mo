��    P      �  k         �     �     �     �     �     �     �       .   	  U   8     �     �      �     �  /   �  /        E     Q     Z  
   f     q     �  $   �     �     �     �     �     �     �  	   	     	  
   .	  7   9	     q	     �	     �	     �	  	   �	     �	  !   �	     �	     �	  	   	
      
     4
     A
     Z
     l
     }
     �
     �
     �
     �
     �
  (   �
     �
  =     2   M     �     �  "   �     �     �  J   �  )   A  (   k  '   �  "   �  4   �          "     (     1     D     X     k  U   �  N   �  9   &  J   `  �  �  "   �     �     �  
   �      �             
   "     -  '   C  !   k  6   �     �  9   �  9        Q     X     _     x     �  0   �  0   �               2  $   ?     d     k     �  $   �     �     �     �     �  *        =     A     O  ?   V     �     �     �  7   �     �  (     "   =     `     �     �     �  $   �  !   �  (   �  E      *   f     �     �     �     �     �     �     �     �  9     <   =  9   z  0   �  B   �  !   (     J  
   N  $   Y     ~     �     �     �  "   �     �  0   �     &   O         A   B       @   ,   #       -       G   0   C   P           ;      =      !                      H          4          %         J                        3       +      "      I   ?                   >      :       (           L   <       .            /   M   K   7   D           N       2          $   1      5       8       )   	   
           9               E   6   *   '   F    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-02-04 18:56-0800
Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>
Language-Team: Japanese <kde-jp@kde.org>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Generator: Lokalize 2.0
 すべてのデスクトップ(&A) 閉じる(&C) フルスクリーン(&F) 移動(&M) 新しいデスクトップ(&N)  ピン留め(&P) シェード(&S) %1 %2(&%1) %1 でも利用可能 現在のアクティビティに追加 すべてのアクティビティ このプログラムのグループ化を許可する アルファベット順 同程度の行を使ってタスクを常に列で配置 同程度の列を使ってタスクを常に行で配置 配置 挙動 アクティビティで デスクトップで プログラム名で ウインドウまたはグループを閉じる マウスホイールでタスクを巡回する グループ化しない ソートしない フィルタ 最近のドキュメントを消去 全般 グループ化とソート グループ化: ウィンドウを強調表示する アイコンサイズ: — 常に最前面に表示(&A) 常に最背面に表示(&B) ランチャーを分離して配置する 大 最大化(&X) 手動 音声を再生するアプリケーションに印を付ける 最大列数: 最大行数: 最小化(&N) ウインドウまたはグループを最小化/復元 その他のアクション 現在のデスクトップに移動(&T) アクティビティに移動(&A) デスクトップに移動(&D) ミュート 新しいインスタンス %1 で すべてのアクティビティで 現在のアクティビティで 中ボタンをクリックしたとき: タスクバーがいっぱいになったときのみグループ化 グループをポップアップで開く 復元 9,999+ 一時停止 次のトラック 前のトラック 終了 サイズ変更(&S) ピン留めを外す 現在のスクリーンのタスクのみを表示する 現在のデスクトップのタスクのみを表示する 現在のスクリーンのタスクのみを表示する 最小化されたタスクのみを表示する 進捗とステータス情報をタスクボタンに表示する ツールチップを表示する 小 ソート: 新しいインスタンスを起動 再生 停止 なし ピン留め(&P)  グループ化/グループ解除 %1 で利用可能 すべてのアクティビティで利用可能 