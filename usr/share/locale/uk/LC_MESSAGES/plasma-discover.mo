��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �     �  $   �                    )  b   7  a   �  R   �  R   O  K   �  %   �       L   *  !   w     �  9   �  5   �  7   %     ]     p     �     �     �     �  F   �  !        A     a  
   �     �     �  $   �  +   �  /     4   4  �   i  :   �  '   -  %   U  m   {     �  
   �  E        M  :   e     �     �  !   �     �          $     ;     K     ^  M   x  M   �       J   0     {     �     �     �  �   �  �   �          )     7     H     e     ~  �   �  
   "     -     E     S     b     v     �  )   �     �  5   �        0   &      W      i   3   w      �      �      �   1   �      !  !   !!     C!     [!     n!  %   |!  *   �!  %   �!  B   �!         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: plasma-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-17 10:22+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 
Також доступний у %1 %1 %1 (%2) %1 (Типове) <b>%1</b>, %2 <em>%1 з %2 користувачів вважають цю рецензію корисною</em> <em>Повідомте нам про свою думку щодо цієї рецензії!</em> <em>Корисна? <a href='true'><b>Так</b></a>/<a href='false'>Ні</a></em> <em>Корисна? <a href='true'>Так</a>/<a href='false'><b>Ні</b></a></em> <em>Корисна? <a href='true'>Так</a>/<a href='false'>Ні</a></em> Отримуємо оновлення Отримуємо… Час останнього пошуку оновлень невідомий Шукаємо оновлення Немає оновлень Доступних оновлень не виявлено Слід перевірити на оновлення Система не потребує оновлення Оновлення Оновлюємо… Прийняти Додати джерело… Додатки Aleix Pol Gonzalez Програма для вивчення сховища програм Застосувати зміни Доступні модулі:
 Доступні режими:
 Назад Скасувати Категорія: Шукаємо оновлення… Коментар є надто довгим Коментар є надто коротким Компактний режим (auto/compact/full). Не вдалося завершити роботу програми, слід виконати заплановані завдання. Не вдалося знайти категорію «%1» Не вдалося відкрити %1 Вилучити походження Безпосередньо відкрити вказану за назвою пакунка програму. Відкинути Пошук Показати список записів з категорією. Розширення… Не вдалося вилучити джерело «%1» Рекомендовані Довідка… Домашня сторінка:  Поліпшити резюме Встановити Встановлено Jonathan Thomas Запустити Ліцензування: Побудувати список всіх доступних модулів. Побудувати список всіх доступних режимів. Завантаження… Локальний файл пакунка для встановлення Зробити типовим Докладніше… Більше… Немає оновлень Відкрити Discover у вказаному режимі. Назви режимів відповідають назвам кнопок панелі інструментів. Відкрити за допомогою програми, яка здатна обробляти дані цього типу MIME. Продовжити Оцінка: Вилучити Ресурси для «%1» Рецензування Рецензування «%1» Працювати від імені користувача <em>root</em> не варто, у цьому немає потреби. Пошук Пошук у «%1»… Пошук… Пошук: %1 Пошук: %1 + %2 Параметри Коротке резюме… Показати рецензії (%1)… Розмір: Вибачте, нічого не знайдено… Джерело: Вказати нове джерело для %1 Шукаємо… Резюме: Підтримує appstream: схема адрес Завдання Завдання (%1%) %1 %2 Не вдалося знайти ресурс: %1 Оновити всі Оновити позначені Оновлення (%1) Оновлення Версія: невідомий рецензент оновлення не позначено оновлення позначено © Команда розробників Плазми, 2010–2016 