��          4      L       `   $   a   /   �     �   K   �  r                       Finds Konsole sessions matching :q:. Lists all the Konsole sessions in your account. Project-Id-Version: plasma_runner_konsolesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2009-04-01 18:14+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 0.3
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Знаходить сеанси Konsole, що відповідають :q:. Показує список всіх сеансів Konsole для вашого облікового запису. 