��          L      |       �   >   �      �   9   �   A   &  
   h  �  s  G   s     �     �  `   �     1                                         '%1' needs a password to be accessed. Please enter a password. ... A default name for an action without proper labelUnknown A new device has been detected.<br><b>What do you want to do?</b> Do nothing Project-Id-Version: soliduiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-27 09:22+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Для доступу до «%1» слід вказати пароль. … Невідомо Виявлено новий пристрій.<br><b>Яку дію слід виконати?</b> Нічого не робити 