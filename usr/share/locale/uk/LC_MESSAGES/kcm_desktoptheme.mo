��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b    �  8   �     �     �  %   �  %        7     S     o  ,   �  H   �  /   �  .   /  w   ^         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-17 18:23+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Налаштовування теми стільниці David Rosca yurchor@ukr.net Отримати нові теми… Встановити з файла… Юрій Чорноіван Відкриття теми Вилучити тему файли тем (*.zip *.tar.gz *.tar.bz2) Помилка під час спроби встановити тему. Тему успішно встановлено. Не вдалося вилучити тему. За допомогою цього модуля ви зможете налаштувати тему стільниці. 