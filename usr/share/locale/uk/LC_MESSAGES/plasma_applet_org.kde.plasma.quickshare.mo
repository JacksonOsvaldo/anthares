��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �    
     #     7  *   F  U   q  �   �  5   l     �     �     �  '   �  4   	     >     V     m  %   �  8   �  Q   �     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: plasma_applet_org.kde.plasma.quickshare
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 14:22+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Закрити Копіювати автоматично: Не показувати це вікно, копіювати автоматично. Перетягніть сюди текст або зображення, щоб їх було вивантажено на сервер інтернет-служби. Помилка під час вивантаження Загальне Розмір журналу: Вставити Будь ласка, зачекайте Будь ласка, спробуйте ще раз. Надсилання… Оприлюднити Спільне для «%1» Вивантажено успішно Адресу було тільки-но поширено Вивантажити %1 до служби інтернет-зберігання 