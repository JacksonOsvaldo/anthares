��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �    �  ?   �     	          -  *   I     t     �     �     �     �     �     �  
           (   #     L  |   Y  o   �     F     W     s  J   �     �  /   �  
     �   '  �   �  '   Y  4   �     �     �  I   �  9   -     g     ~  ,   �  5   �     �            ;   ?  ,   {     �  N   �  5        M  5   ^     �     �  -   �     �  t   �  N   f  4   �  6   �      !  $   B  %   g  .   �     �  D   �  3        $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: oxygen_kdecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-13 08:07+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 &Відповідність властивості вікна:  Величезний Великий Без обрамлення Без бічного обрамлення Звичайний Найбільший Крихітний Гігантський Дуже великий Великий Звичайний Малий Дуже великий Сяйво активного вікна Додати Додавати елемент керування для зміни розмірів вікон без обрамлення Дозволити зміну розмірів максимізованих вікон з країв вікна Анімація Р&озмір кнопок: Ширина рамки: Переходи наведення вказівника на кнопки Посередині Посередині (на всю ширину) Клас:  Налаштування поступового переходу між тінню та підсвічування при зміні активного стану вікна Налаштувати анімацію підсвічування при наведенні вказівника на кнопки Параметри обрамлення Визначити властивості вікна Діалогове вікно Змінити Виключення редагування — параметри Oxygen Увімкнути/Вимкнути цей виняток Тип винятку  Загальне Сховати заголовок вікна Інформація про вибране вікно Ліворуч Пересунути вниз Пересунути вгору Нове виключення — параметри Oxygen Питання — параметри Oxygen Формальний вираз Синтаксис формального виразу є помилковим Формальний ви&раз для пошуку: Вилучити Вилучити позначений виняток? Праворуч Тіні Ви&рівнювання заголовка: Заголовок:  Використовувати один колір для смужки заголовка і вмісту вікна Використовувати клас вікна (цілу програму) Використати заголовок вікна Попередження — параметри Oxygen Назва класу вікна Вікно відкидає тінь Ідентифікація вікна Вибір властивостей вікна Заголовок вікна Переходи зміни активного стану вікон Параметри для окремих вікон 