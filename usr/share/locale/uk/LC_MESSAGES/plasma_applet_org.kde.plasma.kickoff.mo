��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �              /  
   5     @  )   R  $   |     �     �     �  "   �     �  �     -   �     	     �	  !   �	     �	     �	  
   �	     �	  )   
  '   2
  /   Z
  &   �
  ,   �
  3   �
  0     ;   C  0        �                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: plasma_applet_org.kde.plasma.kickoff
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-09 08:40+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Вибрати… Спорожнити піктограму Додати до улюблених Всі програми Вигляд Програми Програми оновлено. Комп'ютер Перетягніть вкладки між панелями, щоб показати або приховати їх, або перевпорядкуйте видимі вкладки перетягуванням. Змінити список програм… Розширити пошук на закладки, файли та повідомлення електронної пошти Улюблене Приховані вкладки Журнал Піктограма: Вийти Кнопки меню Часто використовувані На усіх просторах дій На поточному просторі дій Вилучити з улюблених Показати в «Улюбленому» Показати програми за назвою Впорядкувати за алфавітом Перемикати вкладки під курсором Введіть текст для пошуку… Видимі вкладки 