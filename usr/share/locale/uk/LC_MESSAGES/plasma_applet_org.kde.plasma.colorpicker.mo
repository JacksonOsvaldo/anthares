��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �    �  S   	  !   ]  !     &   �  -   �     �  ?        G     ]     s  G   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_org.kde.plasma.colorpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-14 08:54+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Автоматично копіювати колір до буфера обміну Спорожнити журнал Параметри кольору Скопіювати до буфера Типовий формат кольорів: Загальне Відкрити діалогове вікно кольорів Взяти колір Взяти колір Показати журнал Якщо натиснуто таку комбінацію клавіш: 