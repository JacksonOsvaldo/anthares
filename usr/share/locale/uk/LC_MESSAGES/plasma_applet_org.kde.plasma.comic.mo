��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t    �     �  0     &   J     q  '   �      �  /   �            E         f  P   s  L   �       0        I     V     o  U   �  /   �  0        @  �   X     �     �  <     S   U     �     �     �  +   �  \     "   {     �  4   �  0   �  %     #   =  �  a  �   �  R   m     �  J   �  2     2   P  @   �  8   �  +   �  O   )     y  0   �  3   �     �     �  
   �  ?        G     O     U      b     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_org.kde.plasma.comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2015-04-17 17:20+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 

Виберіть попередню стрічку, щоб перейти до останньої стрічки з кешу. С&творити архів коміксів… &Зберегти комікс як… &Номер стрічки: *.cbz|архів коміксів (Zip) &Фактичний розмір Зберегти поточну &позицію Додатково Всі Сталася помилка для ідентифікатора %1. Вигляд Спроба архівування коміксу зазнала невдачі Автоматичне оновлення додатків коміксів: Кеш Шукати нові стрічки кожні: Комікс Кеш коміксів: Налаштувати… Не вдалося створити архів за вказаною адресою. Створити архів коміксів %1 Створення архіву коміксів Призначення: Показувати повідомлення про помилку, якщо не вдалося отримати комікси Отримати комікси Обробка помилок Не вдалося додати файл до архіву. Не вдалося створити файл з ідентифікатором %1. Від початку до… Від кінця до… Загальне Отримати нові комікси… Спроба отримання стрічки коміксу зазнала невдачі: Перейти до стрічки Відомості Перейти до п&оточної стрічки Перейти до пер&шої стрічки Перейти до стрічки… Нетиповий діапазон Можливо, інтернет-з’єднання було розірвано.
Можливо, додаток коміксів містить помилки.
Ще однією з причин може бути те, що не існує коміксу з вказаним значенням дня/номеру/рядка, отже можна спробувати інші значення. Клацання середньою кнопкою — перегляд коміксів у початковому розмірі Немає файла zip, виконання завдання перервано. Діапазон: Показувати стрілочки лише при наведенні Показувати адресу коміксів Показувати автора коміксів Показувати ідентифікатор коміксів Показувати заголовок коміксів І&дентифікатор стрічки: Діапазон стрічок коміксів для архівування. Оновити Відвідати вебсайт коміксу Відвідати ве&бсайт магазина №%1 днів dd.mm.yyyy &Наступна вкладка з новою стрічкою Від: До: хвилин стрічок на комікс 