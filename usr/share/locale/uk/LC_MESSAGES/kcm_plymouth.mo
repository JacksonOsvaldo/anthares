��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  1   �  ;   �  E     =   J     �  <   �  G   �  O   	     m	     �	     �	  Y   �	  3   
  W   C
  s   �
       n   ,  %   �  �   �  H   �     �       9   *  Y   d                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-06 09:23+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Не вдалося запустити initramfs. Не вдалося запустити update-alternatives. Налаштовування сторінки вітання Plymouth Отримання нових сторінок вітання yurchor@ukr.net Отримати нові сторінки вітання… Помилка під час спроби запустити initramfs. Initramfs повернуто повідомлення про помилку %1. Встановити тему. Marco Martin Юрій Чорноіван У параметрах допоміжного засобу не вказано тему. Засіб встановлення тем Plymouth Виберіть загальну сторінку вітання для системи Тема для встановлення має зберігатися у наявному файлі архіву. Теми %1 не існує. Дані теми пошкоджено: у пакунку теми не знайдено файла .plymouth. Теки теми %1 не існує. За допомогою цього модуля ви можете налаштувати вигляд усього робочого простору. Передбачено декілька готових наборів налаштувань. Не вдалося розпізнати/виконати дію: %1, %2 Вилучити Вилучити тему. Не вдалося виконати update-alternatives. update-alternatives повернуто повідомлення про помилку %1. 