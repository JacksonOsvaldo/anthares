��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m    s     �  9   �     �  $   �               +     ?  g   ^  ?   �  B        I     Q     o  ?   u  )   �  '   �  ?   	     G	  A   a	  ,   �	  A   �	     
  .   +
  H   Z
     �
  "   �
     �
     �
                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_applet_org.kde.image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-11-18 09:08+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1, автор — %2 Додати нетипове зображення тла Додати теку… Додати зображення… Тло: Розмивання За центром Змінювати кожні: Каталог із зображеннями тла, з якого слід брати картинки Отримати зображення тла стільниці Отримати зображення тла стільниці… год. Файли зображень хв. Наступне зображення тла стільниці Відкрити теку об’єкта Відкриття зображення Відкрити зображення тла стільниці Розташування: Рекомендований файл зображення тла Вилучити зображення тла Відновити початкове зображення тла Масштабоване Масштабоване та обрізане Масштабоване, зі збереженням пропорцій сек. Виберіть колір тла Суцільний колір Плиткою 