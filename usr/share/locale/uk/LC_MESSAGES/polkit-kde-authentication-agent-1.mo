��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �    �     �     �     �     �  �   �     �     2	  0   D	  J   u	  2   �	  .   �	     "
     5
  '   E
     m
     }
     �
     �
     �
     �
     �
  5   �
  7   '  2   _     �     �  !   �     �                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: polkit-kde-authentication-agent-1
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-09 16:24+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 %1 (%2) %1: © Red Hat, Inc., 2009 Дія: Програма намагається виконати дію, яка потребує певних привілеїв. Щоб виконати цю дію, слід зареєструватися. Вже розпізнається інший клієнт, будь ласка, повторіть спробу пізніше. Програма: Слід пройти розпізнавання Помилка розпізнавання. Спробуйте ще раз. Натисніть, щоб редагувати %1 Натисніть, щоб відкрити %1 Подробиці yurchor@ukr.net Колишній супровідник Jaroslav Reznik Lukáš Tinkl Супровідник Юрій Чорноіван &Пароль: Пароль %1: Пароль root: Пароль або відбиток пальця %1: Пароль або відбиток пальця root: Пароль або відбиток пальця: Пароль: Агент PolicyKit1 KDE Вибір користувача Джерело запиту: 