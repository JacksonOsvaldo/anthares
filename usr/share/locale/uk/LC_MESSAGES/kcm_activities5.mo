��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �     �  %   �	  7   �	  L   '
     t
     �
     �
     �
  *   �
  *   �
  *        B  )   Z  0   �  ;   �  k   �  =   ]  )   �  	   �  �   �     �     �     �  (   �  /        ;     L     a     �  ,   �     �      �  c   �  D   H  �   �  W   <  +   �     �     �     �  9        ?                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-29 21:55+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 &Не запам’ятовувати Перехід списком просторів дій Перехід списком просторів дій (зворотний) Застосувати Скасувати Змінити… Створити Параметри простору дій Створення простору дій Вилучення простору дій Простори дій Дані щодо простору дій Перемикання просторів дій Ви справді бажаєте вилучити «%1»? Не запам’ятовувати дані для всіх програм поза цим списком Спорожнити журнал останніх подій Створити простір дій… Опис: Помилка під час спроби завантажити файли QML. Перевірте, чи усі файли встановлено належним чином.
Не вистачає %1 Для &всіх програм Забути день Забути все Забути останню годину Забути останні дві години Загальне Піктограма Зберігати журнал Назва: &Лише для певних програм Інше Конфіденційність Конфіденційний — не стежити за діями у цьому просторі Запам’ятовувати відкриті документи: Запам’ятовувати поточну віртуальну стільницю для кожного простору дій (потребує перезапуску) Скорочення для перемикання на цей просторі дій: Клавіатурні скорочення Перемикання Зображення тла протягом   місяця  місяці  місяців  місяця без обмежень 