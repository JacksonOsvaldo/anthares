��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
    �
     �     �     �     �       )     !   @     b  -   y  !   �     �  #   �     �               1     D     X     g     i  ,   �  )   �     �  +   �               +  (   I     r  +     %   �  +   �  g   �  z   e     �     �     	       %   9  '   _     �     �  (   �     �     �  %   �          9     J     b     x     �  &   �  )   �     �  �   �  �  �  
     %   #     I     Z  '   z     �     �  #   �     �       8        J     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.plasma.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-09 08:41+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Простори дій Додавання дії Додати розпірку Додати віджети… Alt Альтернативні віджети Показувати завжди Застосувати Застосування параметрів Застосувати зараз Автор: Автоматично ховати Кнопка «Назад» Вниз Скасувати Категорії За центром Закрити + Налаштувати Налаштувати простір дій Створити простір дій… Ctrl Використовується зараз Вилучити Ел. пошта: Кнопка «Вперед» Отримати нові віджети Висота Горизонтальне гортання Тут слід ввести дані Клавіатурні скорочення Доки віджети заблоковано, компонування не можна змінити Перш ніж вносити інші зміни, слід застосувати зміни у компонуванні Компонування: Ліворуч Ліва кнопка Ліцензування: Заблокувати віджети Максимізувати панель Meta Середня кнопка Додаткові параметри… Дії для миші Гаразд Вирівнювання панелі Вилучити панель Праворуч Права кнопка Край екрана Шукати… Shift Зупинити простір дій Зупинені простори дій: Перемкнутися Параметри роботи поточного модуля було змінено. Застосувати зміни чи відкинути їх? За допомогою цього скорочення можна відкрити аплет: на цей аплет буде пересунуто фокус введення з клавіатури, якщо ж у аплеті передбачено контекстну панель (наприклад панель меню запуску), буде відкрито цю панель. Вгору Скасувати вилучення Вилучити Віджет вилучення Вертикальне гортання Видимість Зображення тла Тип зображення тла: Віджети Ширина Дозволити перекривати вікнами Перекриває вікна 