��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �    �     �  H   �  <   �  I   )  Q   s  '   �     �  :   �     9	  #   S	  ]   w	  Q   �	  �   '
  �   �
  J   9  H   �  P   �  F     7   e  N   �  #   �  Z     
   k     v     �     �     �  X   �     (  2   <                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-17 07:36+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
  мс &Видимість клавіатурних акселераторів: &Тип кнопки для верхньої стрілки: Завжди ховати клавіатурні акселератори Завжди показувати клавіатурні акселератори &Тривалість анімації: Анімації Тип кнопки для &нижньої стрілки: Параметри Breeze Центрувати вкладки Перетягувати вікна за будь-яку з порожніх областей Перетягувати вікна лише за смужку заголовка Перетягувати вікна за смужку заголовка, смужку меню і панелі інструментів. Малювати тонку лінію для позначення фокусування у меню та на панелях меню Малювати позначку фокусування у списках Малювати рамки навколо рухомих панелей Малювати рамки навколо заголовків сторінок Малювати рамки навколо бічних панелей Малювати позначки на повзунку Показувати роздільник елементів у пеналах Увімкнути анімацію Увімкнути розширені елементи керування розміром Рамки Загальне Без кнопок Одна кнопка Смужки гортання Показувати клавіатурні акселератори за потреби Дві кнопки Реж&им перетягування вікон: 