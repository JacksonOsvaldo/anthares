��    
      l      �       �      �            	               .  I   3     }  	   �    �     �     �     �     �  6   �     4  �   ;     �                    
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-08-16 18:41+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 &Слово-перемикач: Додати пункт Замінник Замінник: Налаштування набору символів Код Створює символи на основі :q:, якщо вказано шістнадцятковий код або визначений замінник назви символу. Вилучити пункт Шістн. код: 