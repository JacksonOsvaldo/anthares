��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �          %  5   &  -   \     �  9   �     �  3   �     .     ;     N     a     v  #   �  p   �  4   %  5   Z  
   �     �     �     �  <   �  =   	  <   T	  =   �	  ,   �	  -   �	                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-03 11:18+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.0
 %1 файл %1 файли %1 файлів %1 файл %1 тека %1 теки %1 тек %1 тека %1 з %2 оброблено %1 з %2 оброблено на швидкості %3/с оброблено — %1 %1 оброблено на швидкості %2/с Вигляд Поведінка Скасувати Спорожнити Налаштувати... Завершені завдання Список запущених завдань з передавання/обробки файлів (kuiserver) Пересунути їх в інший список Пересунути їх в інший список. Пауза Вилучити їх Вилучити їх. Поновити Показувати всі завдання в списку Показувати всі завдання в списку. Показувати всі завдання в дереві Показувати всі завдання в дереві. Показувати окремі вікна Показувати окремі вікна. 