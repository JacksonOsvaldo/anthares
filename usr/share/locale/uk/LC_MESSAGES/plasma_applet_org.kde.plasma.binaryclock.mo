��    	      d      �       �   
   �      �      �   	              "     C  "   ]    �     �     �  #   �     �  9   �  e   1  N   �  X   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_org.kde.plasma.binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-02 12:10+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 Вигляд Кольори: Показувати секунди Малювати сітку Показувати незадіяні лампочки: Використовувати нетиповий колір для активних лампочок Використовувати нетиповий колір для сітки Використовувати нетиповий колір для неактивних 