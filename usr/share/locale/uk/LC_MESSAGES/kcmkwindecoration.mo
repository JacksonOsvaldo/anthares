��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g    p     s     �     �     �     �     �     �          $     <     T     n     {  �   �  v   ~	  #   �	  g   
  E   �
  1   �
     �
          /     J     S  "   l  
   �     �     �     �                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-06 19:04+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Величезний Великий Без обрамлення Без бічної рамки Звичайний Найбільший Крихітний Гігантський Дуже великий Меню програм &Ширина рамки: Кнопки Закрити Закрити можна подвійним клацанням лівою кнопкою миші.
 Щоб відкрити меню, утримуйте кнопку натиснутою, аж доки меню не буде показано. Закривати вікна у відповідь на по&двійне клацання на кнопці меню Контекстна довідка Перетягніть кнопки між цією областю і смужкою заголовка Перетягніть сюди, щоб вилучити кнопку Отримати нові обрамлення… Тримати зверху Тримати знизу Максимізувати Меню Мінімізувати На всіх стільницях Пошук Згорнути Тема Смужка заголовка 