��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     	  <  =   F  K   �  i   �  K   :  Q   �  O   �  !   (     J  E   j         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-08-19 08:44+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Не вдалося визначити тип MIME файла Не вдалося знайти всіх потрібних функцій За вказаним призначенням не вдалося знайти сховища даних Помилка під час спроби виконання скрипту Некоректний шлях до вказаного сховища даних Не вдалося прочитати вміст вибраного файла Служба недоступна Невідома помилка Вам слід вказати адресу URL цієї служби 