��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       	  
   	     	     6	  ?   D	  ;   �	  A   �	  A   
  G   D
     �
  
   �
     �
     �
     �
  �   �
  >   �  �   �     `     u  !   �     �  !   �     �     �       '   ,     T     k     t     �  9   �  �   �     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: kcm_pulseaudio
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-03 09:23+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 1.5
 100% Автор © Harald Sitter, 2015 Harald Sitter Немає програм, що відтворюють звук Немає програм, що записують звук Немає доступних профілів пристроїв Немає доступних пристроїв введення Немає доступних пристроїв відтворення Профіль: PulseAudio Додатково Програми Пристрої Додати віртуальний пристрій виведення для одночасного виведення на усі локальні звукові картки Додаткові налаштування виведення Автоматично перемикати усі запущені потоки, якщо стає доступним нове виведення Захоплення Типовий Профілі пристроїв yurchor@ukr.net Пристрої введення Вимкнути звук Юрій Чорноіван Звуки сповіщень Пристрої відтворення Відтворення Порт  (недоступний)  (від’єднано) Потрібен модуль PulseAudio «module-gconf» За допомогою цього модуля можна налаштувати роботу підсистеми роботи зі звуком Pulseaudio. %1: %2 100% %1% 