��          t      �         P     )   b  %   �  @   �     �  V   	     `     {     �     �  	  �     �  �   �    o  �   �  5   ?  E  u  R   �  7   	     F	  #   Y	                         	      
                             %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Update Updates available Project-Id-Version: plasma-discover-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2018-01-15 08:38+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 %1, %2 %1 пакунок для оновлення %1 пакунки для оновлення %1 пакунків для оновлення Один пакунок для оновлення %1 оновлення для забезпечення захисту %1 оновлення для забезпечення захисту %1 оновлень для забезпечення захисту Одне оновлення для забезпечення захисту %1 пакунок для оновлення %1 пакунки для оновлення %1 пакунків для оновлення Один пакунок для оновлення Немає пакунків для оновлення з яких %1 оновлення для забезпечення захисту з яких %1 оновлення для забезпечення захисту з яких %1 оновлень для забезпечення захисту з яких одне оновлень для забезпечення захисту Випущено оновлення для забезпечення захисту Система не потребує оновлення Оновлення Доступні оновлення 