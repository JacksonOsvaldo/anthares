��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  	  �  !   �  !   �  Q   �  �   N  �   �  �   �	  �   2
  �   �
  �   �  %   l     �     �     �     �     �     �  #   �  '   �                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-06 08:48+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <translation@linux.org.ua>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Заблокувати носій Виштовхнути носій Знаходить пристрої, назва яких відповідає :q: Показує список всіх пристроїв і надає змогу монтувати, демонтувати або виштовхувати носії. Показує список всіх пристроїв, які може бути виштовхнуто, і надає змогу їх виштовхнути. Показує список всіх пристроїв, які може бути змонтовано, і надає змогу їх змонтувати. Показує список всіх пристроїв, які може бути демонтовано, і надає змогу їх демонтувати. Показує список всіх пристроїв з зашифрованими даними, які може бути заблокувати, і надає змогу їх заблокувати. Показує список всіх пристроїв з зашифрованими даними, які може бути розблоковано, і надає змогу їх розблокувати. Змонтувати пристрій device eject lock mount unlock unmount Розблокувати носій Демонтувати пристрій 