��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P     \  J   ]  �   �  R   *     }  0   �  M   �     	  +   "	  f   N	  /   �	     �	     �	     	
  T   %
  D  z
  6   �  �   �  7   �  
  "  �   -     �                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-20 09:21+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
 Застосувати пакунок вигляду і поведінки Засіб командного рядка для застосування пакунків вигляду і поведінки. Налаштування параметрів вигляду і поведінки © Marco Martin, 2017 Параметри курсору змінено Отримати нові пакунки вигляду і поведінки yurchor@ukr.net Отримати новий вигляд… Показати список доступних пакунків вигляду і поведінки Засіб вигляду і поведінки Супровідник Marco Martin Юрій Чорноіван Повернутися до компонування стільниці Плазми Визначтеся із загальним виглядом і поведінкою вашого робочого простору (зокрема теми Плазми, схеми кольорів, перемикачі вікон і стільниць, вікна вітання, вікна блокування тощо) Показати попередній перегляд За допомогою цього модуля ви можете налаштувати вигляд усього робочого простору. Передбачено декілька готових наборів налаштувань. Компонування стільниці з теми Попередження: параметри створеного вами компонування стільниці Плазми буде втрачено, їх буде замінено на типові для компонування вибраної теми. Щоб зміни у параметрах курсора було застосовано, вам слід перезапустити KDE. назва_пакунка 