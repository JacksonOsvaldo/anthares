��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �         0     7     =     E  0   R  "   �     �     �  (   �  -   �           4     D  4   U  "   �     �  -   �     �          $     0  %   A     g  '   p     �     �  Q   �            	     
   (  K   3       S   �  )   �            $   (     M  +   a     �     �     �     �     �     �  +     
   B     M     f     y     �     �     �     �     �     �     �  
      
                  #     0     =     E     J     R     _     l     s     y  
   |  
   �     �     �  )   �     �     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_org.kde.plasma.weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-15 08:38+0200
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <kde-i18n-uk@kde.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Lokalize 2.0
  хв. %1 %2 %1 (%2) Вигляд Шкала Бофорта, за Бофортом Градуси Цельсія, °C ° Подробиці Градуси Фаренгейта, °F %1 день %1 дні %1 днів %1 день Гектопаскалі, гПа В.: %1 Н.: %2 Висока: %1 Дюйми ртутного стовпчика, inHg Градуси Кельвіна, K кілометри Кілометри на годину, км/г Кілопаскалі, кПа Вузли, вузол Місце: Низька: %1 Метри на секунду, м/с милі Милі на годину, миль/г Мілібари, мбар н/д Для «%1» не знайдено метеорологічних станцій Примітки % Тиск: Пошук Виберіть служби надання даних про погоду – Показувати температуру у компактному режимі: Будь ласка, налаштуйте Температура: Одиниці Інтервал оновлення: Видимість: Метеорологічна станція Вітру немає Швидкість вітру: %1 (%2%) Вологість: %1%2 Видимість: %1 %2 Точка роси: %1 Коефіцієнт вологості: %1 падає піднімається незмінний Зміна тиску: %1 Тиск: %1 %2 %1%2 Видимість: %1 %1 (%2) Попередження: Спостереження: С С-Пв-С С-Пд-С Пн Пв-С Пн-Пн-С Пн-Пн-З Пн-З Пд Пд-С Пд-Пд-С Пд-Пд-З ПдЗ Зм. З З-Пн-З З-Пд-З %1 %2 %3 Вітру немає Коефіцієнт різкості: %1 Пориви вітру: %1 %2 