��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  &   Y  0   �  2   �  *   �       1      )   R  8   |     �     �     �  :   �  #   "	  :   F	  @   �	     �	  D   �	  #   
  u   C
  2   �
     �
     �
  3     B   E                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: kcm_plymouth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-06 14:09+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 No s'ha pogut iniciar l'«initramfs». No s'ha pogut iniciar l'«update-alternatives». Configura la pantalla de presentació del Plymouth Descarrega pantalles de presentació noves txemaq@gmail.com Obtén pantalles de presentació d'inici noves... L'«initramfs» ha fallat en executar-se. L'«initramfs» ha retornat amb la condició d'error %1. Instal·la un tema. Marco Martin Josep Ma. Ferrer No s'ha especificat cap tema als paràmetres de l'ajudant. Instal·lador de temes del Plymouth Selecciona una pantalla de presentació global pel sistema El tema a instal·lar, cal que sigui un fitxer d'arxiu existent. El tema %1 no existeix. Tema corrupte: El fitxer «.plymouth» no s'ha trobat dins del tema. La carpeta %1 del tema no existeix. Aquest mòdul us permetrà configurar l'aspecte de tot l'espai de treball amb algunes predefinicions a punt per usar. No s'ha pogut autenticar/executar l'acció: %1, %2 Desinstal·la Desinstal·la un tema. L'«update-alternatives» ha fallat en executar-se. L'«update-alternatives» ha retornat amb la condició d'error %1. 