��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     Q     _     g     s     �  	   �  
   �     �  +   �  #   �          *  0   ?  	   p     z     �     �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org.kde.plasma.userswitcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-09 15:38+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 Usuari actual General Disposició Bloqueig de pantalla Sessió nova Sense ús Sortida... Mostra l'avatar i el nom Nom complet de l'usuari (si és disponible) Mostra el nom d'usuari de connexió Mostra només l'avatar Mostra només el nom Mostra informació tècnica quant a les sessions a %1 (%2) TTY %1 Nom d'usuari a mostrar Esteu connectat com a <b>%1</b> 