��          �      |      �     �  2        B     b       #   �      �     �  %   �       
   &     1     >     ]  �   }       ]   (     �  p   �  :        P  �  \  )      H   J  4   �     �  #   �  .   	  3   8     l  6   �     �  
   �     �  3   �  .   %	  �   T	     )
  u   B
  /   �
  �   �
  H   {     �                                                                      
                	           Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. packagename Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-12-20 22:06+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Aplica un paquet d'aspecte i comportament Eina de la línia d'ordres per aplicar paquets d'aspecte i comportament. Configura els detalls per a l'aspecte i comportament Copyright 2017, Marco Martin Ha canviat l'arranjament del cursor Baixa de paquets nous d'aspecte i comportament sps@sastia.com,antonibella5@yahoo.com,aacid@kde.org Obtén aspectes nous... Llista de paquets disponibles d'aspecte i comportament Eina d'aspecte i comportament Mantenidor Marco Martin Sebastià Pla i Sanz,Antoni Bella,Albert Astals Cid Reinicia la disposició de l'escriptori Plasma Selecciona un tema global per l'escriptori (incloent-hi el tema del plasma, l'esquema de colors, el cursor del ratolí, commutador de finestres i escriptoris, pantalla de presentació, pantalla de bloqueig, etc.) Mostra una vista prèvia Aquest mòdul us permetrà configurar l'aspecte de tot l'espai de treball amb algunes predefinicions a punt per usar. Usa la disposició de l'escriptori des del tema Avís: la vostra disposició de l'escriptori Plasma serà perduda i restablida a la disposició predeterminada proporcionada pel tema seleccionat. Per tal que el canvi del cursor tingui efecte, s'ha de reiniciar el KDE. nomdepaquet 