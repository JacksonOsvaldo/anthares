��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w  �  �  "   �  	   �  +   �  %   �       
   6     A     Y     m                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: plasma_applet_org.kde.plasma.kscreen
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-09-01 09:42+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.4
 S'ha detectat una pantalla nova %1 Desactiva Ha fallat en connectar-se al dimoni KScreen Ha fallat en carregar l'objecte arrel Clona la sortida primària Cap acció Configuració avançada Estén a l'esquerra Estén a la dreta 