��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
  
   f     q     �     �     �     �     �     �     �  
   �     �     �          #  
   (  
   3     >     E     K  	   M     W     m     �     �  	   �     �     �     �     �     �            C   *  F   n     �     �     �     �     �     �               !     5     I     Q     g     y  
        �     �     �     �     �     �  U   �  �   8     �     �               '     >     J     \     x          �     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.plasma.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-09-09 15:26+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 Activitats Afegeix una acció Afegeix un espaiador Afegeix estris... Alt Estris alternatius Sempre visible Aplica Aplica l'arranjament Aplica ara Autor: Oculta automàticament Botó enrere Baix Cancel·la Categories Centre Tanca + Configura Configura l'activitat Crea una activitat... Ctrl Actualment s'està usant Suprimeix Correu electrònic: Botó endavant Obtén estris nous Alçada Desplaçament horitzontal Introduïu-ho aquí Dreceres de teclat La disposició no es pot canviar mentre els estris estan bloquejats Cal aplicar els canvis de disposició abans de poder fer altres canvis Disposició: Esquerra Botó esquerre Llicència: Bloqueja els estris Maximitza el plafó Meta Botó del mig Més paràmetres... Accions del ratolí D'acord Alineació del plafó Elimina el plafó Dreta Botó dret Vora de la pantalla Cerca... Majúscules Atura l'activitat Activitats aturades: Commuta L'arranjament del mòdul actual ha canviat. Voleu aplicar els canvis o descartar-los? Aquesta drecera activarà la miniaplicació: li donarà el focus del teclat, i si la miniaplicació té una finestra emergent (com el menú d'inici), aquesta s'obrirà. Dalt Desfés la desinstal·lació Desinstal·la Desinstal·la l'estri Desplaçament vertical Visibilitat Fons d'escriptori Tipus de fons d'escriptori: Estris Amplada Les finestres el poden tapar Les finestres van a sota 