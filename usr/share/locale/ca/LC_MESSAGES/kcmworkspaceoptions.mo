��    	      d      �       �      �   -   �   *   %      P  '   q  
   �     �     �  �  �     �  E   �  3   �     (  3   9  
   m     x     �                   	                           (c) 2009 Marco Martin Display informational tooltips on mouse hover Display visual feedback for status changes EMAIL OF TRANSLATORSYour emails Global options for the Plasma Workspace Maintainer Marco Martin NAME OF TRANSLATORSYour names Project-Id-Version: kcmworkspaceoptions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-11-08 18:48+0100
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 2.0
 (c) 2009 Marco Martin Mostra els consells informatius en passar-hi per sobre amb el ratolí Mostra retroalimentació visual pels canvis d'estat txemaq@gmail.com Opcions globals per a l'espai de treball del Plasma Mantenidor Marco Martin Josep Ma. Ferrer 