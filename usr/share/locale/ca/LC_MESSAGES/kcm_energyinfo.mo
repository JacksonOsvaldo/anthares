��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  	   �	     �	     
     "
     2
     9
     =
     I
     V
     g
     o
  #   �
     �
     �
     �
     �
     �
               .     @     R     a     {  	   �     �     �     �     �     �  	   �     �     �     �     �     �       L        \  (   n  
   �     �     �     �     �     �     �     �     �     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: kcm_energyinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-17 17:00+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 % %1 %2 %1: Consum d'energia per aplicació Bateria Capacitat Percentatge de càrrega Estat de la càrrega Està carregant Actual °C Detalls: %1 Descarregant txemaq@gmail.com Energia Consum d'energia Estadístiques del consum d'energia Entorn Càrrega completa per disseny Totalment carregada Té subministrament d'energia Kai Uwe Broulik Darreres 12 hores Darreres 2 hores Darreres 24 hores Darreres 48 hores Darrers 7 dies Última càrrega completa Darrera hora Fabricant Model Josep Ma. Ferrer No No està carregant PID: %1 Camí: %1 Recarregable Refresca Número de sèrie W Sistema Temperatura Actualment aquest tipus d'historial no és disponible per aquest dispositiu. Període de temps Període de temps de les dades a mostrar Proveïdor V Voltatge Activacions per segon: %1 (%2%) W Wh Sí Consum % 