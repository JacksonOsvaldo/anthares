��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  3   �     �  1   �     0     C     Z  !   x     �     �  	   �  	   �     �     �     �     �  
   �          !  #   ?      c     �     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_org.kde.plasma.pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-25 22:41+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
 %1 finestra minimitzada: %1 finestres minimitzades: %1 finestra: %1 finestres: ... i %1 altra finestra ... i %1 altres finestres Nom de l'activitat Número de l'activitat Afegeix un escriptori virtual Configuració dels escriptoris... Nom d'escriptori Número d'escriptori Pantalla: No fa res General Horitzontal Icones Disposició: Sense text Només la pantalla actual Elimina un escriptori virtual En seleccionar l'escriptori actual: Mostra el gestor d'activitats... Mostra l'escriptori Predeterminada Vertical 