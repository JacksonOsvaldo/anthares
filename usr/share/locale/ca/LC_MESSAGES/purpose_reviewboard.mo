��          �      \      �     �     �  	   �  $   �          ,     C     ^     j  	   y  
   �     �     �     �     �     �  "   �  	   �  '   �  �  &     �     �  
   �  ,   	  ,   6  %   c     �     �     �     �  
   �     �     �  	             &  "   =     `  "   n                      
            	                                                                 / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Repository: Request Error: %1 Server: Update Review: Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: purpose_reviewboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-09-27 13:21+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.5
 / Autenticació Dir. base: No s'ha pogut crear la nova sol·licitud:
%1 No s'ha pogut obtenir la llista de revisions No s'han pogut establir les metadades No s'ha pogut pujar el pedaç Destinació Error del JSON: %1 Contrasenya: Repositori Repositori: Error de sol·licitud: %1 Servidor: Actualitza la revisió: Actualitza la revisió Nom d'usuari al servei especificat Nom d'usuari: D'on va ser extret aquest projecte 