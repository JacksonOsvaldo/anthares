��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �          )     I     \  	   v     �  
   �     �     �     �  E   �          $     C  
   I     T     `  &   i  '   �  $   �  %   �          !                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-05-20 21:16+0200
Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>
Language-Team: Catalan <kde-i18n-ca@kde.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Accelerator-Marker: &
X-Generator: Lokalize 1.0
 %1 fitxer %1 fitxers %1 carpeta %1 carpetes S'han processat %1 de %2 S'han processat %1 de %2 a %3/s S'han processat %1 S'han processat %1 a %2/s Aparença Comportament Cancel·la Neteja Configura... Treballs finalitats Llista de transferències/treballs de fitxer en execució (kuiserver) Mou-los a una llista diferent Mou-los a una llista diferent. Pausa Elimina'ls Elimina'ls. Continua Mostra tots els treballs en una llista Mostra tots els treballs en una llista. Mostra tots els treballs en un arbre Mostra tots els treballs en un arbre. Mostra en finestres separades Mostra en finestres separades. 