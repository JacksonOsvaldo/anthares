��          4      L       `   �   a   L   �     E  �   X  #                       Converts the value of :q: when :q: is made up of "value unit [>, to, as, in] unit". You can use the Unit converter applet to find all available units. list of words that can used as amount of 'unit1' [in|to|as] 'unit2'in;to;as Project-Id-Version: plasma_runner_converterrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2011-01-16 18:11+0200
Last-Translator: Lasse Liehu <lliehu@kolumbus.fi>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:26+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 Muuntaa :q::n arvon, missä :q: on ”arvo yksikkö [yksikköön;yksikössä;->;in;to;as] yksikkö”. Voit katsoa Yksikkömuunnin-sovelmasta, mitä kaikkia yksikköjä on käytettävissä. yksikköön;yksikössä;->;in;to;as 