��          <      \       p      q   *   �   /   �   �  �   %   �  +   �  7                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2013-12-22 14:01+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:22+0000
X-Generator: Lokalize 1.5
 ”%1” ei löytynyt lähteestä %2. Yhteys sääpalvelimeen %1 aikakatkaistiin. Säätietojen hakeminen sijainnille %1 aikakatkaistiin. 