��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     d  U   {     �     �     �     �          &     .     @     W     k     {     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-20 11:58+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Lisää käynnistin… Lisää käynnistimiä vetämällä ja pudottamalla tai käyttäen kontekstivalikkoa. Ulkoasu Asettelu Muokkaa käynnistintä… Käytä ponnahdusikkunaa Kirjoita otsikko Yleiset Piilota kuvakkeet Sarakkeita enintään: Rivejä enintään: Pikakäynnistin Poista käynnistin Näytä piilotetut kuvakkeet Näytä käynnistinten nimet Näytä otsikko Otsikko 