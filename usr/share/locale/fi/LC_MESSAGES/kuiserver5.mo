��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %                /  "   B     e     s     �     �     �  	   �     �     �  J   �          5     Q     W     ^     f     l      �     �     �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-12-22 13:56+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:15+0000
X-Generator: Lokalize 1.5
 %1 tiedosto %1 tiedostoa %1 kansio %1 kansiota %1 / %2 käsitelty %1 / %2 käsitelty nopeudella %3/s %1 käsitelty %1 käsitelty nopeudella %2/s Ulkoasu Toiminta Peru Tyhjennä Aseta… Päättyneet työt. Käynnissä olevien tiedostonsiirtojen tai tehtävien luettelo (kuiserver) Siirrä toiseen luetteloon. Siirrä toiseen luetteloon. Tauko Poista Poista. Jatka Näytä kaikki työt luettelona Näytä kaikki työt luettelona. Näytä kaikki työt puuna Näytä kaikki työt puuna. Näytä erilliset ikkunat Näytä erilliset ikkunat. 