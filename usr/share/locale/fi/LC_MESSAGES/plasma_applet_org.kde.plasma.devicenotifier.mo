��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  <   (  
   e     p       J   �  +   �  8   	     ?	  �   G	  �   �	  /   �
     	       &   7  "   ^  0   �     �     �     �  "   �                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-04 21:59+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:31+0000
X-Generator: Lokalize 2.0
 %1 toiminto tälle laitteelle %1 toimintoa tälle laitteelle %1 vapaana Liitetään… Kaikki laitteet Salli muiden sovellusten käyttää tätä laitetta napsauttamalla tästä Poista levy asemasta napsauttamalla tästä Irrota tämä laite turvallisesti napsauttamalla tästä Yleiset Tämän laitteen poistaminen <b>ei tällä hetkellä ole turvallista</b>. Ohjelmat saattavat käyttää sitä. Napsauta poistamispainiketta poistaaksesi laitteen turvallisesti Tämän laitteen poistaminen <b>ei tällä hetkellä ole turvallista</b>. Ohjelmat voivat käyttää laitteen muita taltioita. Napsauta näiden taltioiden poistamispainiketta, jotta voit poistaa tämän laitteen turvallisesti Tämän laitteen poistaminen on nyt turvallista Viimeisin laite Ei laitteita käytettävissä Vain muut kuin siirrettävät laitteet Irrotettavien laitteiden asetukset Avaa ponnahdusikkuna, kun uusi laite on kytketty Vain siirrettävät laitteet Poistetaan… Tämä laite on nyt saatavilla Tämä laite ei ole nyt saatavilla 