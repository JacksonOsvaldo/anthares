��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  *   =  4   h     �     �     �  #   �  )        ;     Z     h     u  3   �     �  *   �  3   �     .	  9   I	  !   �	  \   �	  8   
     ;
     J
  3   a
  (   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-06-08 20:48+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Ei voida käynnistää ohjelmaa initramfs. Ei voida käynnistää ohjelmaa update-alternatives. Plymouth-aloituskuvan asetukset Lataa uusi aloituskuvia translator@legisign.org Hae uusi käynnistysaloituskuvia… initramfs-ohjelman suoritus epäonnistui. initramfs palautti virheen %1. Asenna teema. Marco Martin Tommi Nieminen Avustajan parametreissa ei ole määritetty teemaa. Plymouth-teema-asennin Valitse järjestelmän yleinen aloituskuva Asennettava teema (olemassa oleva arkistotiedosto). Teemaa %1 ei ole olemassa. Vioittunut teema: teemasta ei löydy .plymouth-tiedostoa. Teemakansiota %1 ei ole olemassa. Tällä asetusohjelmalla voit muuttaa koko työtilan ulkoasua valmiiden esiasetusten avulla. Toimintoa ei saatu todennetuksi tai suoritetuksi: %1, %2 Poista asennus Poista teeman asennus. update-alternatives-ohjelman suoritus epäonnistui. update-alternatives palautti virheen %1. 