��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �     �  A     +   T  N   �  N   �  G        f     ~  9   �     �     �     �          -     K     X     j     s  	   �     �     �     �  .   �          !     *     /     7     U     l      �  =   �     �     �       :         [     d  *   l     �  %   �  	   �     �  	   �     �     �  	               
   *  =   5  .   s     �  '   �     �     �  
   �       L     E   `     �  
   �     �     �  	   �     �  X   �     K     P     d     l     u  	   �     �     �     �  '   �     �  $   �          *     6  
   S     ^     p     v     �     �     �     �     �     �     �       $            J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: muon-discover
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-22 17:37+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2012-12-01 22:21:39+0000
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 
Saatavilla myös luokassa %1 %1 %1 (%2) %1 (oletus) <b>%1</b> tekijältä %2 <em>%1/%2 ihmisen mielestä tämä arvostelu on hyödyllinen</em> <em>Kerro meille tästä arvostelusta!</em> <em>Hyödyllinen? <a href='true'><b>Kyllä</b></a>/<a href='false'>Ei</a></em> <em>Hyödyllinen? <a href='true'>Kyllä</a>/<a href='false'><b>Ei</b></a></em> <em>Hyödyllinen? <a href='true'>Kyllä</a>/<a href='false'>Ei</a></em> Noudetaan päivityksiä Noudetaan… Ei tiedetä, milloin päivitykset on viimeksi tarkistettu Etsitään päivityksiä Ei päivityksiä Päivityksiä ei ole saatavilla Päivitykset tulisi tarkistaa Järjestelmä on ajan tasalla Päivitykset Päivitetään… Hyväksy Lisää lähde… Lisäosat Aleix Pol Gonzalez Sovellusten löytämisohjelma Toteuta muutokset Käytettävissä olevat taustajärjestelmät:
 Käytettävissä olevat tilat:
 Takaisin Peru Luokka: Tarkistetaan päivityksiä… Liian pitkä kommentti Liian lyhyt kommentti Tiivis tila (auto/compact/full). Ohjelman sulkeminen epäonnistui, koska tehtäviä on kesken. Luokkaa ”%1” ei löytynyt Ei voitu avata: %1 Poista lähde Avaa määräsovellus suoraan sen pakettinimen perusteella Hylkää Löydä Näyttää luettelon luokan sovelluksista. Laajennukset… Lähteen ”%1” poisto epäonnistui Esitellyt Ohje… Kotisivu: Paranna yhteenvetoa Asenna Asennetut Jonathan Thomas Käynnistä Lisenssi:  Luettele kaikki käytettävissä olevat taustajärjestelmät. Luettele kaikki käytettävissä olevat tilat. Ladataan… Asennettava paikallinen pakettitiedosto Aseta oletukseksi Lisätietoa… Lisää… Ei päivityksiä Avaa Discover mainitussa tilassa. Tilat vastaavat työkalurivin painikkeita. Avaa ohjelmalla, joka osaa käsitellä annetun MIME-tyypin tiedostoja Jatka Arvostelu: Poista Resurssit: %1 Arvostele ”%1” – arvosteleminen Ohjelman suorittamista <em>pääkäyttäjänä</em> ei suositella, ja se on tarpeetonta. Etsi Etsi luokasta %1… Etsi… Etsi: %1 Etsi: %1 + %2 Asetukset Lyhyt yhteenveto… Näytä arvostelut (%1)… Koko: Mitään ei valitettavasti löytynyt… Lähde: Määritä uusi lähde lähteelle %1 Etsitään yhä… Yhteenveto: Tukee appstream:-URL-skeemaa Tehtävät Tehtävät (%1 %) %1 %2 Resurssia ei löydy: %1 Päivitä kaikki Päivitys valittu Päivitä (%1) Päivitykset Versio: tuntematon tarkastaja päivitystä ei valittu päivitystä valittu © 2010–2016 Plasman kehitysryhmä 