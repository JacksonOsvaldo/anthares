��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �            	     '     )  "   I     l     �     �     �  L   �               "     9     A  �   V  |   �  o   V	     �	  >   �	     
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-09 15:17+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:38+0000
X-Generator: Lokalize 2.0
  min Käytä samoja asetuksia kuin   Määritä poikkeavat asetukset Älä käytä poikkeavia asetuksia lasse.liehu@gmail.com Siirry lepotilaan Lasse Liehu Älä sammuta näyttöä Älä koskaan sammuta tietokonetta tai aseta sitä valmius- saati lepotilaan Verkkovirrassa Akkuvirrassa Akun ollessa vähissä Sammuta Siirry valmiustilaan Virranhallintapalvelu ei ole käynnissä.
Käynnistä tai ajasta se järjestelmäasetusten ”Käynnistys ja sammutus” -osiosta. Aktiviteettipalvelu ei ole käynnissä.
Sen tulee olla käynnissä, jotta aktiviteettikohtaisia asetuksia voi määrittää. Aktiviteeteista on käytössä vain vähimmäistoiminnallisuus.
Nimet ja kuvakkeet eivät ehkä ole saatavilla. Aktiviteetti ”%1” Käytä erillisiä asetuksia (vain edistyneille käyttäjille) aina jälkeen 