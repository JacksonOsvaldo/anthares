��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �    �     �     �  ,   �  @     I   V  K   �  I   �  M   6	  d   �	     �	     �	     �	     
     
     
     
     
     0
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-08 00:51+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:37+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 Lukitse sisällytin Poista levy asemasta Etsi laitteita, joiden nimi vastaa hakua :q: Luettelee kaikki laitteet ja antaa liittää ja irrottaa niitä. Luettelee kaikki laitteet, joista voi poistaa levyn, ja antaa tehdä sen. Luettelee kaikki liitettävissä olevat laitteet ja antaa liittää niitä. Luettelee kaikki irrotettavissa olevat laitteet ja antaa irrottaa niitä. Luettelee kaikki lukittavissa olevat salatut laitteet ja antaa lukita niitä. Luettelee kaikki laitteet, joiden lukitus voidaan poistaa, ja sallii niiden lukituksen poistettavan. Liitä laite device eject lock mount unlock unmount Irrota sisällytin Irrota laite 