��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     f  %   v  *   �  7   �  ,   �  4   ,	  -   a	  (   �	  *   �	  7   �	  )   
  +   E
  9   q
  a   �
  '        5  !   Q     s       
   �     �     �  !   �     �     �     �                                                         	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-19 11:17+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:31+0000
X-Generator: Lokalize 2.0
 Commit-toiminto Lisätty tiedostoja SVN-tietokantaan. Lisätään tiedostoja SVN-tietokantaan… Tiedostojen lisääminen SVN-tietokantaan epäonnistui. SVN-muuutosten commit-toiminto epäonnistui. SVN-muutokset, joille on suoritettu commit-toiminto. Suoritetaan commit-toiminto SVN-muutoksiin… Tiedostot poistettiin SVN-tietokannasta. Poistetaan tiedostoja SVN-tietokannasta… Tiedostojen poistaminen SVN-tietokannasta epäonnistui. Tiedostot palautettiin SVN-tietokannasta. Palautetaan tiedostoja SVN-tietokannasta… Tiedostojen palauttaminen SVN-tietokannasta epäonnistui. SVN-tilapäivitys epäonnistui. Otetaan pois käytöstä valitsin ”Näytä SVN-päivitykset”. SVN-tietokannan päivitys epäonnistui. Päivitetty SVN-tietokanta. Päivitetään SVN-tietokantaa… SVN lisää SVN Commit-toiminto… SVN poista SVN palauta SVN-päivitys Näytä paikalliset SVN-muutokset Näytä SVN-päivitykset Kuvaus: SVN Commit-toiminto Näytä päivitykset 