��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  )   �	  /   �	  7   
     V
  ,   l
     �
     �
  /   �
       $   &  1   K  !   }  %   �  +   �  /   �  7   !  "   Y     |     �  $   �  &   �  ,   �      &  &   G     n     �     �     �     �     �     �  $                                                                 	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-12-22 14:01+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2012-12-01 22:25:31+0000
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Tiedostot lisätty Bazaar-tietovarastoon. Lisätään tiedostoja Bazaar-tietovarastoon… Tiedostojen lisäys Bazaar-tietovarastoon epäonnistui. Bazaar-loki suljettu. Bazaar-muutosten toteuttaminen epäonnistui. Bazaar-muutokset toteutettu. Toteutetaan Bazaar-muutoksia… Bazaar-tietovarastosta vetäminen epäonnistui. Vedetty Bazaar-tietovarastosta. Vedetään Bazaar-tietovarastosta… Bazaar-tietovarastoon työntäminen epäonnistui. Työnnetty Bazaar-tietovarastoon. Työnnetään Bazaar-tietovaratoon… Tiedostot poistettu Bazaar-tietovarastosta. Poistetaan tiedostoja Bazaar-tietovarastosta… Tiedostojen poisto Bazaar-tietovarastosta epäonnistui. Muutoksien tarkistus epäonnistui. Muutokset tarkistettu. Tarkistetaan muutoksia… Bazaar-lokin hakeminen epäonnistui. Bazaar-lokin hakeminen meneillään… Bazaar-tietovaraston päivitys epäonnistui. Bazaar-tietovarasto päivitetty. Päivitetään Bazaar-tietovarastoa… Bazaar – lisää… Bazaar – commit… Bazaar – poista Bazaar – loki Bazaar – vedä (pull) Bazaar – työnnä (push) Bazaar – päivitä Näytä paikalliset Bazaar-muutokset 