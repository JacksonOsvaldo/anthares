��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  0   }     �  *   �     �               3     M     _     s     |     �     �  	   �  	   �     �     �     �  !   �  "        B     V     ]        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-11-22 23:29+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 %1 pienennetty ikkuna: %1 pienennettyä ikkunaa: %1 ikkuna: %1 ikkunaa: …ja %1 muu ikkuna …ja %1 muuta ikkunaa Aktiviteetin nimi Aktiviteetin numero Lisää virtuaalityöpöytä Työpöytien asetukset… Työpöydän nimi Työpöydän numero Näytä: Ei tehdä mitään Perusasetukset Vaaka Kuvakkeet Asettelu: Ei tekstiä Vain nykyiseltä näytöltä Poista virtuaalityöpöytä Valittaessa nykyinen työpöytä: Näytä aktiviteettien hallinta… Näytä työpöytä Oletus Pysty 