��          �            x     y  
   �  
   �     �  .   �     �     �     �        *   )      T  ,   u  ,   �  0   �        �    !   �  
   �     �     �  (   �               .  1   >  8   p  /   �  	   �     �  7   �     !                        
                                            	           &Lock screen on resume: Activation Appearance Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2018-01-22 17:36+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Lukitse &näyttö herättäessä: Aktivointi Ulkoasu Virhe Näyttölukon kokeileminen epäonnistui. Heti Pikanäppäin: Lukitse istunto Lukitse näyttö automaattisesti, kun on kulunut: Lukitse näyttö herättäessä valmius- tai lepotilasta &Vaadi salasana, kun lukitsemisesta on kulunut:  min  min  s  s Näytön lukitseva työpöydänlaajuinen pikanäppäin. Taustan &tyyppi: 