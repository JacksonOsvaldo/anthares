��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '       <  (   M  )   v  4   �  (   �  -   �  -   ,     Z     t     �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-08 00:53+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:33+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 Tiedosto MIME-tyyppiä ei saatu selville Kaikki vaadittuja funktioita ei löydetty Palveluntarjoajaa ei löytynyt annetusta sijainnista Virhe yritettäessä suorittaa skriptiä Väärä polku pyydetylle palveluntarjoajalle Valittua tiedostoa ei ollut mahdollista lukea Palvelu ei ole saatavilla Tuntematon virhe Palvelun osoite on annettava 