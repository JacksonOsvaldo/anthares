��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  .     -   K     y  	   �     �     �     �  	   �     �     �  5   �             /   5     e     q     �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-11-21 00:32+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:31+0000
X-Generator: Lokalize 2.0
 Haluatko keskeyttää järjestelmän muistiin? Haluatko keskeyttää järjestelmän levylle? Yleiset Toiminnot Lepotila Lepotila (keskeytä levylle) Poistu Poistu… Lukitse Lukitse näyttö Kirjaudu ulos, sammuta tai käynnistä kone uudelleen Ei Valmiustila (keskeytä muistiin) Aloita rinnakkaisistunto toisena käyttäjänä Valmiustila Vaihda käyttäjää Vaihda käyttäjää Kyllä 