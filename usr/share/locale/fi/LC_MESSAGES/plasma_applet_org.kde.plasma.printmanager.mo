��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �     �     �     �  
     +     *   I     t     �     �  
   �     �  :   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-26 16:47+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Tulostinten &asetukset… Vain aktiiviset työt Kaikki työt Vain valmiit työt Tulostinasetukset Yleiset Ei aktiivisia töitä Ei töitä Tulostimia ei ole havaittu tai määritetty Yksi aktiivinen työ %1 aktiivista työtä Yksi työ %1 työtä Avaa tulostusjono Tulostusjono on tyhjä Tulostimet Etsi tulostinta… Jonossa on yksi tulostustyö Jonossa on %1 tulostustyötä 