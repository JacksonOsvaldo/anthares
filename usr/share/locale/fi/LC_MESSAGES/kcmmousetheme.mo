��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     h  �   �  W   
	     b	     x	  I   �	  	   �	     �	     �	     

  '   
  �   9
          $  $   3     X  �   k     �     �       ?     I   Y  %   �  O   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-09-24 18:41+0300
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:22+0000
X-Generator: Lokalize 1.5
 © 2003–2007 Fredrik Höglund <qt>Haluatko varmasti poistaa osoitinteeman ”<i>%1</i>”?<br />Tämä poistaa kaikki kyseisen teeman asentamat tiedostot.</qt> <qt>Et voi poistaa nyt käyttämääsi teemaa.
Ota ensin toinen teema käyttöön.</qt> (Saatavilla koot: %1) Riippuu tarkkuudesta Teema %1 löytyy jo kuvaketeemakansiostasi. Haluatko korvata sen tällä? Varmistus Osoitinasetuksia muutettiin Osoitinteema Kuvaus Vedä tai kirjoita teeman verkko-osoite eleknader@phnet.fi, kim.enkovaara@iki.fi, teemu.rytilahti@kde-fi.org, ilpo@iki.fi, niklas.laxstrom+kdetrans@gmail.com, mikko.piippo@helsinki.fi, karvonen.jorma@gmail.com, lasse.liehu@gmail.com, translator@legisign.org Fredrik Höglund Hae uusi teema Hae uusia väriteemoja internetistä Asenna tiedostosta Tapio Kautto, Kim Enkovaara, Teemu Rytilahti, Ilpo Kantonen, Niklas Laxström, Mikko Piippo, Jorma Karvonen, Lasse Liehu, Tommi Nieminen Nimi Korvataanko teema? Poista teema Tiedosto %1 ei vaikuta olevan oikeanlainen osoitinteemapaketti. Ei voitu ladata osoitinteemapakettia. Tarkasta, että osoite %1 on oikea. Osoitinteemapakettia %1 ei löytynyt. Plasma-istunto on käynnistettävä uudestaan, jotta muutokset tulevat voimaan. 