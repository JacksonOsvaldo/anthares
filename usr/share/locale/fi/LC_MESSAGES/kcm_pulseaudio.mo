��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �  #   �     �  #   �  %   	     ?	     ]	     t	  	   �	  
   �	     �	     �	     �	  U   �	     
  [   /
     �
     �
     �
  .   �
     �
     �
     �
          (     7     >     E     \  '   k  F   �     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-06-08 20:50+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 100 % Tekijä Tekijänoikeudet 2015 Harald Sitter Harald Sitter Mikään ohjelma ei toista ääntä Mikään ohjelma ei tallenna ääntä Ei laiteprofiileja saatavilla Ei äänitulolaitteita Ei toistolaitteita Profiili: PulseAudio Lisäasetukset Sovellukset Laitteet Lisää virtuaalilaite kaikkien paikallisten äänikorttien samanaikaiseen ulostuloon Ulostulon lisäasetukset Vaihda kaikki käynnissä olevat virrat automaattisesti uuden ulostulon tullessa saataville Kaappaus Oletus Laiteprofiilit lasse.liehu@gmail.com, translator@legisign.org Äänitulolaitteet Vaimenna ääni Lasse Liehu, Tommi Nieminen Ilmoitusäänet Toistolaitteet Toisto Portti  (ei käytettävissä)  (ei liitetty) Vaatii PulseAudio-moduulin module-gconf Tässä osiossa voi muokata Pulseaudio-äänijärjestelmän asetuksia. %1: %2 100 % %1 % 