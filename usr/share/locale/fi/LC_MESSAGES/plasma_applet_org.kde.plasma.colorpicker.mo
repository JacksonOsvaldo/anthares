��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  ,   �               #     :     N     W     i     w     �     �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_kolourpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-01-13 22:58+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:24+0000
X-Generator: Lokalize 2.0
 Kopioi väri leikepöydälle automaattisesti Tyhjennä historia Värivalinnat Kopioi leikepöydälle Värin oletusmuoto: Yleistä Avaa väri-ikkuna Valitse väri Valitse väri Näytä historia Painettaessa pikanäppäintä: 