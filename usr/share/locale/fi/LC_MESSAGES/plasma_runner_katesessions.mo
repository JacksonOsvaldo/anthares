��          <      \       p   !   q   3   �      �     �   0   �  -        H                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: plasma_runner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2011-01-14 22:21+0200
Last-Translator: Lasse Liehu <lliehu@kolumbus.fi>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:26+0000
X-Generator: MediaWiki 1.21alpha (963ddae); Translate 2012-11-08
 Etsii Kate-istuntoja, jotka vastaavat hakua :q:. Luettelee kaikki käyttäjäsi Kate-istunnot. Avaa Kate-istunto 