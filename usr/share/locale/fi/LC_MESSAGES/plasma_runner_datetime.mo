��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �      �  =   �     6  ;   U  	   �  
   �     �                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-02-07 18:20+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:26+0000
X-Generator: Lokalize 2.0
 Kello on nyt %1 Näyttää nykyisen päiväyksen Näyttää nykyisen päiväyksen annetulla aikavyöhykkeellä Näyttää nykyisen kellonajan Näyttää nykyisen kellonajan annetulla aikavyöhykkeellä päiväys kellonaika Tänään on %1 