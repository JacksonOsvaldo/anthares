��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  $   �     �     �     �     �  
   �     �                    +  
   1     <     B     N     g  8   o  =   �  
   �     �            
   .     9     T  a   ]  9   �     �          *     1  )   9  2   c     �     �     �     �  
   �     �     �  $        4     S  3   k  (   �     �     �     �     �     �  	     @     %   \     �     �     �     �     �     �       -   !     O     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-26 10:01+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:29+0000
X-Generator: Lokalize 2.0
 &Täsmättävä ikkunan ominaisuus:  Valtava Suuri Ei reunusta Ei sivureunusta Tavallinen Ylisuuri Pikkuruinen Aivan valtava Hyvin suuri Suuri Tavallinen Pieni Hyvin suuri Aktiivisen ikkunan hehku Lisää Lisää kahva reunattomien ikkunoiden koon muuttamiseksi Muuta myös suurennettujen ikkunoiden kokoa ikkunan reunoilta Animoinnit Pa&inikkeen koko: Reunuksen koko: Hiiren leijunnan siirtymä Keskitetty Keskitetty (täysi leveys) Luokka:  Aseta häivytys ikkunan varjostuksen ja hehkutuksen välillä ikkunan aktiivisen tilan muuttuessa Aseta ikkunapainikkeiden hiirileijunnan korostusanimaatio Kehyksen asetukset Tunnista ikkunan ominaisuudet Ikkuna Muokkaa Muokkaa poikkeusta – Oxygenin asetukset Ota poikkeus käyttöön tai poista se käytöstä Poikkeustyyppi Yleistä Piilota ikkunan otsikkopalkki Valitun ikkunan tiedot Vasemmalla Siirrä alemmas Siirrä ylemmäs Uusi poikkeus – Oxygenin asetukset Kysymys – Oxygenin asetukset Säännöllinen lauseke Säännöllisen lausekkeen syntaksi on virheellinen Täsmättävä säännöllinen lauseke:  Poista Poistetaanko valittu poikkeus? Oikealla Varjot &Otsikon sijainti: Otsikko:  Käytä samoja värejä otsikkopalkissa ja ikkunan sisällössä Käytä ikkunaluokkaa (koko sovellus) Käytä ikkunan otsikkoa Varoitus – Oxygenin asetukset Ikkunaluokan nimi Ikkunan pudotusvarjo Ikkunan tunnistus Ikkunan ominaisuuden valinta Ikkunan otsikko Ikkunan aktiivisen tilan muutoksen siirtymät Ikkunakohtaiset asetukset 