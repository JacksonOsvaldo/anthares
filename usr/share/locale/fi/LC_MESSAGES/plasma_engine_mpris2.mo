��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  D   n     �     �     �     �       I     1   d  !   �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-20 22:03+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2012-12-01 22:22:32+0000
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Toiminnon ”%1” suoritusyritys epäonnistui viestillä: ”%2”. Toista seuraava media Toista edellinen media Mediaohjain Toista mediaa / tauko Pysäytä median toisto Toiminnon ”%2” parametri ”%1” puuttuu tai on väärää tyyppiä. Mediasoitin "%1" ei voi suorittaa toimintoa "%2". Operaatio ”%1” on tuntematon. Tuntematon virhe. 