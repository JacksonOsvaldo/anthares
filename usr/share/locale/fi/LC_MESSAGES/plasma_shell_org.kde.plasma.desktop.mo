��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     3     @     Q     `     t     x     �     �     �     �     �     �     �     �            	               	         *     I     ]     b     n     u     �     �     �     �     �     �  6   �  F     	   ]  
   g     r  	   �     �     �     �     �     �     �     �     �                     .     =     E     L     c     }  ^   �  �   �     �     �     �     �     �  	   �     �                    %     >     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-12-08 16:30+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Aktiviteetit Lisää toiminto Lisää erotin Lisää sovelmia… Alt Vaihtoehtoiset sovelmat Aina näkyvissä Käytä Käytä asetuksia Käytä nyt Tekijä: Piilota automaattisesti Takaisin-painike Alhaalla Peru Luokat Keskellä Sulje + Asetukset Muokkaa aktiviteetin asetuksia Luo aktiviteetti… Ctrl Käytössä Poista Sähköposti: Eteenpäin-painike Hae uusia sovelmia Korkeus Vaakavieritys Syötä tähän Pikanäppäimet Asettelua ei voi muuttaa, kun sovelmat ovat lukittuina Asettelumuutoksia täytyy käyttää ennen muiden muutosten tekemistä Asettelu: Vasemmalla Vasen painike Lisenssi: Lukitse sovelmat Kasvata paneeli enimmäiskokoon Meta Keskipainike Lisää asetuksia… Hiiren toiminnot OK Paneelin sijainti Poista paneeli Oikealla Oikea painike Näytön reuna Etsi… Vaihto Pysäytä aktiviteetti Pysäytetyt aktiviteetit: Vaihda Nykyisen asetusosion asetukset ovat muuttuneet. Haluatko käyttää muutoksia vai hylätä ne? Tämä pikanäppäin aktivoi sovelman: Se siirtää näppäimistökohdistuksen sovelmaan, ja jos sovelmalla on ponnahdusvalikko (kuten käynnistysvalikolla), ponnahdusvalikko avautuu. Ylhäällä Kumoa asennuksen poisto Poista asennus Poista sovelman asennus Pystyvieritys Näkyvyys Tausta Taustan tyyppi: Sovelmat Leveys Ikkunat voivat peittää Ikkunat jäävät alle 