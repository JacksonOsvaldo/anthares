��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �     >     Q     q  I   �     �     �     �             	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-15 18:52+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Poista käytöstä Poista kosketuslevy käytöstä Ota kosketuslevy käyttöön Hiirtä ei havaittu.
Haluatko varmasti poistaa kosketuslevyn käytöstä? Kosketuslevyä ei löydy Kosketuslevy Kosketuslevy ei käytössä Kosketuslevy käytössä 