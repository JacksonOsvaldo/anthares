��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     |     �  K   �     �       -     
   I     T  )   a     �     �     �  '   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-12-08 13:54+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:22:19+0000
X-Generator: Lokalize 2.0
 Työpöytäteeman asetukset David Rosca teemu.rytilahti@kde-fi.org,karvonen.jorma@gmail.com,translator@legisign.org Hae uusia teemoja… Asenna tiedostosta… Teemu Rytilahti,Jorma Karvonen,Tommi Nieminen Avaa teema Poista teema Teematiedostot (*.zip *.tar.gz *.tar.bz2) Teeman asennus epäonnistui. Teeman asennus onnistui. Teeman poisto epäonnistui. Tästä voit muokata työpöytäteemaa. 