��    	      d      �       �      �   >      #   ?  '   c  3   �  .   �  "   �  A     �  S  '     I   G  0   �  /   �  5   �  D   (  )   m  C   �                                    	          Default to the switch user UI. Delay till the lock user interface gets shown in milliseconds. Don't show any lock user interface. File descriptor for connecting to ksld. Greeter for the KDE Plasma Workspaces Screen locker Lock immediately, ignoring any grace time etc. Starts the greeter in testing mode Starts the greeter with the selected theme (only in Testing mode) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-20 02:56+0200
PO-Revision-Date: 2016-10-02 14:04+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
X-POT-Import-Date: 2012-12-01 22:22:27+0000
 Näytä käyttäjän vaihto oletuksena. Viivästä lukkokäyttöliittymän näyttämistä (aika millisekunteina). Älä näytä mitään lukkokäyttöliittymää. Tiedostokuvaaja ksld:hen yhdistämistä varten. Tervehdys KDE Plasma -työtilojen näytönlukitukseen Lukitse välittömästi; lukituksen poistoon tarvitaan heti salasana Käynnistää tervehdyksen testaustilassa Käynnistää tervehdyksen valitulla teemalla (vain testaustilassa) 