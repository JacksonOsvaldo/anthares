��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w  �  �     i     �  ,   �  "   �     �               "     6                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2013-07-11 18:32+0300
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <lokalisointi@lists.coss.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Havaittiin uusi näyttö %1 Poista käytöstä KScreen-palveluun yhdistäminen epäonnistui Juuriolion lataaminen epäonnistui Monista ensisijainen ulostulo Ei toimintoa Lisäasetukset Laajenna vasemmalle Laajenna oikealle 