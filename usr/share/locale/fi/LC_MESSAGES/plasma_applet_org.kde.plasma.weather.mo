��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  �                                 .     A     D     S     i     �     �     �     �  
   �  
   �     �     �  	     	                   8     ?     T     e  +   i     �     �     �     �  $   �     �     �  $   �     "     /     8  
   H  
   S     ^     f  
   u     �     �     �     �     �     �     �     �     �                    !     6     R     T     X     \     ^     a     e     i     l     n     q     u     y     |     �     �     �     �     �     �     �     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: plasma_applet_weather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-22 17:35+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2012-12-01 22:25:26+0000
X-Generator: Lokalize 2.0
  min %1 %2 %1 (%2) Ulkoasu Boforiasteikko bft Celsius-asteet °C ° Yksityiskohdat Fahrenheit-asteet °F 1 päivän %1 päivän Hehtopascalit hPa Ylin: %1 Alin: %2 Ylin: %1 Elohopeatuumat inHg Kelvinit K Kilometrit Kilometrejä tunnissa km/h Kilopascalit kPa Solmut kt Sijainti: Alin: %1 Metrejä sekunnissa m/s Mailit Maileja tunnissa mph Millibaarit mbar – Kohteelle ”%1” ei löytynyt sääasemia Huomautukset % Ilmanpaine: Etsi Valitse sääpalvelutiedon tarjoajat – Näytä lämpötila tiiviisti: Sääaseman sijainti täytyy asettaa Lämpötila: Yksiköt Päivitä joka: Näkyvyys: Sääasema Tyyntä Tuulen nopeus: %1 (%2 %) Ilmankosteus: %1%2 Näkyvyys: %1 %2 Kastepiste: %1 Kosteusindeksi: %1 laskee nousee vakaata Ilmanpaineen muutos: %1 Ilmanpaine: %1 %2 %1%2 Näkyvyys: %1 %1 (%2) Annetut varoitukset: Annetut ennakkovaroitukset: E ENE ESE N NE NNE NNW NW S SE SSE SSW SW Vaiht. W WNW WSW %1 %2 %3 Tyyntä Tuulen hyytävyys: %1 Puuska: %1 %2 