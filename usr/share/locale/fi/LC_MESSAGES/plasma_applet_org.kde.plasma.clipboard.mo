��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �        �     �     �  !        )     @     I     R     m     �     �     �     �     �     �     �     �     �                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-02-20 22:08+0200
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Vaihda kuviokoodin tyyppiä Tyhjennä historia Leikepöydän sisältö Leikepöydän historia on tyhjä. Leikepöytä on tyhjä Koodi 39 Koodi 93 Leikepöydän asetukset… Kuviokoodin luonti epäonnistui Data Matrix Muokkaa sisältöä +%1 Käytä toimintoa QR-koodi Poista historiasta Palaa leikepöydälle Etsi Näytä kuviokoodi 