��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  
   �     �     �                     '  	   @  �   J     �  @   �     /     8     O     X     `     g     y     �     �     �     �     �     	  !   
	     ,	     B	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-12-08 16:26+0200
Last-Translator: Tommi Nieminen <translator@legisign.org>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Valitse… Tyhjennä kuvake Lisää suosikkeihin Kaikki sovellukset Ulkoasu Sovellukset Sovellukset päivitetty. Tietokone Vedä välilehteä laatikosta toiseen näyttääksesi tai piilottaaksesi niitä tai muuta näkyvien välilehtien järjestystä vetämällä. Muokkaa sovelluksia… Laajenna haku kirjanmerkkeihin, tiedostoihin ja sähköposteihin Suosikit Piilotetut välilehdet Historia Kuvake: Poistu Valikkopainikkeet Usein käytetyt Kaikissa aktiviteeteissa Nykyisessä aktiviteetissa Poista suosikeista Näytä suosikeissa Näytä sovellukset nimeltä Aakkosta Vaihda välilehteä osoitettaessa Hae kirjoittamalla… Näkyvät välilehdet 