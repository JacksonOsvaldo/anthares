��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �     �               &     8     P     ^  D   g  
   �  	   �  
   �      �     �     
     (     <  )   D  )   n     �  <   �  (   �  >   	  >   W	     �	     �	     �	     �	                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-12-03 21:08+0900
Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>
Language-Team: Finnish <kde-i18n-doc@kde.org>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Lisää tiedostoja… Lisää kansio… Taustakehys Vaihda kuvaa joka Valitse kansio Valitse tiedostot Plasmoidin asetukset… Täyttötapa: Yleistä Hiiren vasemman painikkeen napsautus avaa ulkoiseen katseluohjelmaan Keskitetty Sijainnit Sijainnit: Keskeytä hiiren leijuessa yllä Suhteet säilyttävä rajaus Suhteet säilyttävä sovitus Satunnaisjärjestys Venytä Kuva monistetaan vaaka- ja pystysuunnassa Kuvan kokoa tai mittasuhteita ei muunneta Kuva skaalataan sopivaksi Kuva skaalataan täyttämään tila ja rajataan tarvittaessa Kuva sovitetaan rajaamatta mitään pois Kuva venytetään vaakasuunnassa ja monistetaan pystysuunnassa Kuva venytetään pystysuunnassa ja monistetaan vaakasuunnassa Monista Monista vaakasuunnassa Monista pystysuunnassa  s 