��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  K  �  @   ;     |     �  "   �  5   �  
     $        ;     S  !   k  =   �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_org.kde.plasma.colorpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-18 21:56+0100
Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>
Language-Team: Serbian <kde-i18n-sr@kde.org>
Language: sr@ijekavian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Accelerator-Marker: &
X-Text-Markup: kde4
X-Environment: kde
 Аутоматски копирај боју у клипборд Очисти историјат Опција боје Копирај у клипборд Подразумијевани формат боје: Опште Отвори дијалог боја Изабери боју Изабери боју Прикажи историјат На притисак пречице са тастатуре: 