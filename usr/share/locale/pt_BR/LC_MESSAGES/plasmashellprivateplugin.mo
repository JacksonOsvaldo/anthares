��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .  	   �            /   !     Q     p     w     �  #   �     �  "   �     �     �       	   $     .     <     V     o  #   �      �  4   �  8    	     9	     A	     F	  9   M	     �	     �	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: plasmashellprivateplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2017-07-29 20:51-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Executar Todos os widgets Categorias: Console de criação de script do Desktop Shell Baixar novos widgets do Plasma Editor Executando o script em %1 Filtros Instalar widget de arquivo local... Falha na instalação Erro na instalação do pacote %1. Carregar Nova sessão Abrir arquivo de script Resultado Em execução Tempo de execução: %1ms Salvar arquivo de script Bloqueio de tela ativo Tempo de espera do protetor de tela Selecione o arquivo do plasmoide Define a quantidade de minutos para bloquear a tela. Define se a tela será bloqueada após o tempo indicado. Modelos KWin Plasma Não foi possível carregar o arquivo de script <b>%1</b> Pode ser desinstalado Usar 