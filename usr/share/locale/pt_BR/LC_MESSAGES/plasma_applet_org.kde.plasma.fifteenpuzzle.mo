��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  
   �  
   �     �     �  :   �       %        =     L  
   ]     h     p     �  	   �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_org.kde.plasma.fifteenpuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-03-02 18:50-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Aparência Navegar... Escolher uma imagem Quebra-cabeça Quinze Arquivos de imagem (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Cor dos números Localização da imagem personalizada Cor das peças Mostrar números Embaralhar Tamanho Resolver organizando na ordem Resolvido! Tentar novamente. Tempo: %1 Usar imagem personalizada 