��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �            �     "         #  $   5  4   Z  %   �     �     �  '   �  0   �          *     ;     ?     N     V  #   l     �     �                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: plasma_applet_org.kde.plasma.clipboard
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-03-31 20:15-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Alterar o tipo de código de barra Limpar histórico Conteúdo da área de transferência O histórico da área de transferência está vazio. A área de transferência está vazia Code 39 Code 93 Configurar a Área de Transferência... Ocorreu um erro na criação do código de barra Data Matrix Editar conteúdo +%1 Invocar ação QR Code Remover do histórico Retornar à Área de Transferência Procurar Mostrar código de barras 