��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �  %   X     ~     �     �  "   �     �  
   �     �  +        2     Q  %   m  ?   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-25 11:49-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Configura o tema da área de trabalho David Rosca alvarenga@kde.org Baixar novos temas... Instalar a partir de um arquivo... André Marcelo Alvarenga Abrir tema Remover tema Arquivos de tema (*.zip *.tar.gz *.tar.bz2) Falha na instalação do tema. Tema instalado com sucesso. Ocorreu um erro na remoção do tema. Este módulo permite-lhe configurar o tema da área de trabalho 