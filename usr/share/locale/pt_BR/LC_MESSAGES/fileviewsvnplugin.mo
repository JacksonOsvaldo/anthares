��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     T  5   [  1   �  @   �  1   	  &   6	  &   ]	  3   �	  /   �	  A   �	  4   *
  0   _
  A   �
  a   �
  8   4  "   m  !   �     �     �     �     �     �  %        +     I     V     c                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-08 09:18-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Enviar Os arquivos foram adicionados ao repositório de SVN. Adicionando os arquivos ao repositório de SVN... A adição dos arquivos ao repositório de SVN foi mal-sucedida. O envio das alterações de SVN foi mal-sucedido. As alterações no SVN foram enviadas. Enviando as alterações para o SVN... Os arquivos foram removidos do repositório de SVN. Removendo os arquivos do repositório de SVN... A remoção dos arquivos do repositório de SVN foi mal-sucedida. Os arquivos foram revertidos do repositório de SVN. Revertendo os arquivos do repositório de SVN... A reversão dos arquivos do repositório de SVN foi mal-sucedida. Falha na atualização do status do SVN. Desabilitando a opção "Mostrar atualizações do SVN". A atualização do repositório de SVN foi mal-sucedida. O repositório SVN foi atualizado. Atualizando o repositório SVN... Adição ao SVN Enviar ao SVN... Remoção do SVN Reversão do SVN Atualizar o SVN Mostrar as alterações locais do SVN Mostrar atualizações do SVN Descrição: Envio ao SVN Exibir atualizações 