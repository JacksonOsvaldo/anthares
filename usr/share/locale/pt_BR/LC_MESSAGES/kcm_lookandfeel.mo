��          �      �       H     I     i  #   �      �     �     �     �  �        �  ]   �       p   *  :   �  �  �      �  #   �  "   �               +     8  �   Q     <  g   S  (   �  �   �  K   f                     
          	                                  Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: kcm_lookandfell
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-10-25 11:16-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Configura detalhes da aparência Configurações do cursor alteradas Baixar novos pacotes de aparência alvarenga@kde.org Baixar novos visuais... Marco Martin André Marcelo Alvarenga Selecione um tema de aparência e comportamento global para sua área de trabalho (incluindo o tema do Plasma, esquema de cores, cursor do mouse, seletor de janelas e áreas de trabalho, tela de apresentação, bloqueio de tela, etc.) Mostrar visualização Este módulo permite-lhe configurar o visual de todo o espaço de trabalho com algumas predefinições. Usar layout da área de trabalho do tema Aviso: o layout de sua área de trabalho Plasma será perdido e redefinido para o layout padrão fornecido pelo tema selecionado. O KDE deve ser reiniciado para que as alterações do cursor tenham efeito. 