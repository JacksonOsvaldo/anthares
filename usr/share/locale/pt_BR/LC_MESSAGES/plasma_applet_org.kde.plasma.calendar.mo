��    	      d      �       �   	   �      �   <   �   5   0     f     �  4   �     �  �  �     �     �     �     �  .   �                (                                          	    Ends at 5 General Show the number of the day (eg. 31) in the iconDay in month Show the week number (eg. 50) in the iconWeek number Show week numbers in Calendar Starts at 9 What information is shown in the calendar iconIcon: Working Day Project-Id-Version: plasma_applet_org.kde.plasma.calendar
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-09-12 10:01-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Termina às 17:00 Geral Dia no mês Número da semana Mostrar os números das semanas no calendário Começa às 9:00 Ícone: Dia de trabalho 