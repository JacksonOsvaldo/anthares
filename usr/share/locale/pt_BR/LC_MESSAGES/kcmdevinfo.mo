��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  ;   �     (  	   >     H     Q     n     w     �     �     �     �  
   �     �     �     �     �  ;        R     o     |  (   �     �     �     �     �               /     D     H  ?   Q  	   �  	   �  
   �  
   �  
   �     �     �  	   �     �     �          !     '  %   <     b  	   g      q     �     �     �     �     �  	   �     �     �     	  	        !     &     2     7     <     M  "   k     �     �     �  "   �     �                    "  a   )     �  
   �     �     �     �     �  
   �     �             
             &     3     @     M  	   Z           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-09 08:04-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 
Módulo de visualização de dispositivos baseado no Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 livres de %2 (%3% usados) Baterias Tipo de bateria:  Barramento:  Câmera Câmeras Estado da carga:  Carregando Recolher tudo Leitor de Compact Flash Um dispositivo Informações do dispositivo Exibe todos os dispositivos que estão atualmente listados. Visualizador de dispositivos Dispositivos Descarregando marcus.gama@gmail.com, alvarenga@kde.org Criptografado Expandir tudo Sistema de arquivos Tipo de sistema de arquivos:  Totalmente carregada Unidade de disco rígido Acoplável a quente? IDE IEEE1394 Exibe informações sobre o dispositivo atualmente selecionado. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Teclado Teclado + Mouse Rótulo:  Velocidade máxima:  Leitor de Memory Stick Montado em:  Mouse Leitores multimídia Marcus Gama, André Marcelo Alvarenga Não Sem carga Nenhuma informação disponível Não montado Não definido Unidade óptica PDA Tabela de partições Primário Processador %1 Número do processador:  Processadores Produto:  RAID Removível? SATA SCSI Leitor de SD/MMC Mostrar todos os dispositivos Mostrar os dispositivos relevantes Leitor de Smart Media Unidades de armazenamento Controladores suportados:  Tipos de instruções suportados:  Protocolos suportados:  UDI:  UPS USB UUID:  Exibe o UDI (Unique Device Identifier - Identificador Único do Dispositivo) do dispositivo atual Unidade desconhecida Não usado Fabricante:  Espaço do volume: Utilização do volume:  Sim kcmdevinfo Desconhecido Nenhum Nenhum Plataforma Desconhecido Desconhecido Desconhecido Desconhecido Desconhecido Leitor xD 