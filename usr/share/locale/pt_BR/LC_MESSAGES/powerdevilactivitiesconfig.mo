��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �       !     &   /     V     h     q     �  (   �      �     �     �       	   %  �   /  �   �  �   �	     1
  A   @
     �
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-05-06 22:39-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  min Funcionar como Sempre Definir um comportamento especial Não usar as configurações especiais alvarenga@kde.org Hibernar André Marcelo Alvarenga Nunca desligar a tela Nunca suspender ou desligar o computador PC usando o adaptador de energia PC usando a bateria PC com pouca carga de bateria Desligar Suspender O serviço de gerenciamento de energia não parece estar em execução.
Isto pode ser resolvido se iniciá-lo ou fazer um agendamento em "Inicialização e desligamento" O serviço de atividades não está em execução.
É necessário ter o gerenciador de atividades em execução para configurar o comportamento do gerenciamento de energia específico da atividade. O serviço de atividades está em execução com as funcionalidades básicas.
Os nomes e os ícones das atividades podem não estar disponíveis. Atividade "%1" Usar configurações separadas (apenas para usuários avançados) após 