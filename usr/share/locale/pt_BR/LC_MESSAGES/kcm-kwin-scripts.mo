��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  +   �     �     �               2     M      ]     ~  4   �  (   �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-10-25 12:42-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n > 1);
 *.kwinscript|Scripts do KWin (*.kwinscript) Configurar os scripts do KWin alvarenga@kde.org Baixar novos scripts... Importar script do KWin Importar script do KWin... Scripts do KWin Configuração do script do KWin André Marcelo Alvarenga Não foi possível importar o script selecionado.
%1 O script "%1" foi importado com sucesso. Tamás Krutki 