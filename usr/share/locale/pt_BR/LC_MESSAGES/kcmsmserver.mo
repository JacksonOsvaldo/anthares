��          �      <      �     �     �     �  7  �  �  #  +   �  ^              �     �  x   �  �   %     �               ;     U  �  r     <	     W	     o	    �	    �
  -   �  k   �     W     i     �  �   �  �   0  $   !     F  .   Y     �     �                          
       	                                                                    &End current session &Restart computer &Turn off computer <h1>Session Manager</h1> You can configure the session manager here. This includes options such as whether or not the session exit (logout) should be confirmed, whether the session should be restored again when logging in and whether the computer should be automatically shut down after session exit by default. <ul>
<li><b>Restore previous session:</b> Will save all applications running on exit and restore them when they next start up</li>
<li><b>Restore manually saved session: </b> Allows the session to be saved at any time via "Save Session" in the K-Menu. This means the currently started applications will reappear when they next start up.</li>
<li><b>Start with an empty session:</b> Do not save anything. Will come up with an empty desktop on next start.</li>
</ul> Applications to be e&xcluded from sessions: Check this option if you want the session manager to display a logout confirmation dialog box. Conf&irm logout Default Leave Option General Here you can choose what should happen by default when you log out. This only has meaning, if you logged in through KDM. Here you can enter a colon or comma separated list of applications that should not be saved in sessions, and therefore will not be started when restoring a session. For example 'xterm:konsole' or 'xterm,konsole'. O&ffer shutdown options On Login Restore &manually saved session Restore &previous session Start with an empty &session Project-Id-Version: kcmsmserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-05-28 21:55-0300
Last-Translator: André Marcelo Alvarenga <andrealvarenga@gmx.net>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.0
 &Finalizar a sessão atual &Reiniciar o computador &Desligar o computador <h1>Gerenciador de sessões</h1> Você pode configurar aqui o gerenciador de sessões. Inclui opções como confirmação de saída (logout), salvamento da sessão para o próximo login e se o computador deve ser automaticamente desligado por padrão quando a sessão é encerrada. <ul>
<li><b>Restaurar a sessão anterior:</b> Salvará todos os aplicativos em execução ao sair e restaura-os na próxima inicialização.</li>
<li><b>Restaurar a sessão que foi salva manualmente: </b> Permite salvar a sessão a qualquer momento através da opção "Salvar sessão" do Menu K. Isto significa que os aplicativos atualmente iniciados reaparecerão na próxima inicialização.</li>
<li><b>Iniciar com uma sessão vazia:</b> Não salva nada. Iniciará com uma área de trabalho vazia na próxima inicialização.</li>
</ul> Aplicativos a serem e&xcluídos das sessões: Habilite esta opção se quiser que o gerenciador de sessões mostre uma janela de confirmação de saída. Confirmar &saída Opção de saída padrão Geral Aqui você pode escolher o que deve acontecer por padrão quando você encerra a sessão. Esta opção só tem efeito se você abriu a sessão em modo gráfico (KDM). Aqui você pode digitar uma lista de aplicativos separados por vírgulas ou dois-pontos, que não devem ser salvos nas sessões, e portanto, não devem ser iniciados ao restaurar uma sessão. Por exemplo: 'xterm,konsole' ou 'xterm:konsole'. M&ostrar as opções de desligamento Na inicialização Restaurar a sessão que foi salva &manualmente Restaurar a sessão &anterior Iniciar com uma &sessão vazia 