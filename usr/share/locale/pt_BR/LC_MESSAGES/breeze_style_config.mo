��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     _  *   c  !   �  )   �  )   �             !   ,     N  $   h  3   �  5   �  I   �  G   A	  '   �	  6   �	  7   �	  3    
  &   T
  ;   {
     �
  5   �
  	               	        )  5   ;     q  "   ~                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: breeze_style_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-25 11:07-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  ms &Visibilidade dos aceleradores do teclado: &Tipo do botão da seta superior: Sempre ocultar os aceleradores do teclado Sempre mostrar os aceleradores do teclado D&uração das animações: Animações Tipo do botão da seta i&nferior: Configurações do Breeze Centralizar as abas na barra de abas Arrastar janelas a partir de todas as áreas vazias Arrastar janelas somente a partir da barra de título Arrastar janelas a partir das barras de título, de menu e de ferramentas Desenhar uma linha fina para indicar o foco nos menus e barras de menus Desenhar o indicador de foco nas listas Desenhar um contorno em volta dos painéis acopláveis Desenhar um contorno em volta dos títulos das páginas Desenhar um contorno em volta dos painéis laterais Desenhar as marcas da barra deslizante Desenhar os separadores dos itens das barras de ferramentas Ativar as animações Ativar os manipuladores de dimensionamento estendidos Contornos Geral Sem botões Um botão Barras de rolagem Mostrar os aceleradores do teclado quando necessário Dois botões &Modo de arrastamento das janelas: 