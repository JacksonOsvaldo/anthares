��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  Z   �  �   �  $   �  o   �  $        A  K   X     �  I   �                     )  =   G  ;   �  ;   �     �       e   #     �     �  �   �  -   I  H   w     �     �     �     �          ,     5     P  	   c  +   m     �     �  *   �  #   �     �                    /     A     R  "   b     �     �     �     �  #   �  	   �     �      �       �   "     �  M   �  �   -     �  J   �  >   1  >   p  3   �  "   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-12-20 19:00-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n > 1);
 Para aplicar a alteração de infraestrutura você precisa encerrar e reiniciar a sessão. Lista de infraestruturas do Phonon encontradas em seu sistema. A sequência indicada aqui determina a ordem em que o Phonon irá usá-las. Aplicar a lista de dispositivos a... Aplicar a lista de preferências apresentada atualmente para as seguintes categorias de reprodução de áudio: Configuração do hardware de áudio Reprodução de áudio Preferência do dispositivo de reprodução de áudio para a categoria '%1' Gravação de áudio Preferência do dispositivo de gravação de áudio para a categoria '%1' Infraestrutura Colin Guthrie Conector Copyright 2006 Matthias Kretz Preferência padrão do dispositivo de reprodução de áudio Preferência padrão do dispositivo de gravação de áudio Preferência padrão do dispositivo de gravação de vídeo Categoria padrão/indefinida Preterir Define a ordenação padrão dos dispositivos que podem ser substituídos por categorias individuais. Configuração do dispositivo Preferência do dispositivo Dispositivos encontrados no seu sistema, adequados à categoria selecionada. Escolha o dispositivo que será usado pelos aplicativos. diniz.bortolotto@gmail.com, alvarenga@kde.org Não foi possível definir o dispositivo de saída de áudio selecionado Frontal central Frontal esquerdo Frontal à esquerda do centro Frontal direito Frontal à direita do centro Hardware Dispositivos independentes Níveis de entrada Inválido Configuração do hardware de áudio do KDE Matthias Kretz Mono Diniz Bortolotto, André Marcelo Alvarenga Módulo de configuração do Phonon Reprodução (%1) Preferir Perfil Traseiro central Traseiro esquerdo Traseiro direito Gravação (%1) Mostrar os dispositivos avançados Lado esquerdo Lado direito Placa de som Dispositivo de som Colocação e teste do alto-falante Subwoofer Testar Testar o dispositivo selecionado Testando %1 A ordem determina a preferência dos dispositivos. Se por algum motivo o primeiro dispositivo não puder ser usado, o Phonon tentará usar o segundo, e assim por diante. Canal desconhecido Usar a lista de dispositivos atualmente apresentada para ver mais categorias. As diversas categorias de casos de uso multimídia. Para cada categoria, é possível escolher qual o dispositivo que você prefere usar nos aplicativos do Phonon. Gravação de vídeo Preferência do dispositivo de gravação de vídeo para a categoria '%1'  A sua infraestrutura pode não suportar a gravação de áudio A sua infraestrutura pode não suportar a gravação de vídeo nenhuma preferência para o dispositivo selecionado preferir o dispositivo selecionado 