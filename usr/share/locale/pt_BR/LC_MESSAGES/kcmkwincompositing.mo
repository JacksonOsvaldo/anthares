��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  E   �  s   /  X   �     �  4        9     @  �   Z     A     X     d  
   s     ~     �     �  3   �     �     �     �     �  &     G   8  0   �     �     �  e   �     N  !   [     }  �   �     N     g      m     �  
   �  
   �     �     �  V  �  !         B  !   a  �   �       	   #     -     3  &   F     m     y         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-25 12:44-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 "Atualizações da tela inteira" pode causar problemas de desempenho. "Apenas quando for econômico" somente evita as intermitências nas mudanças para tela inteira, como em um vídeo. "Reutilizar o conteúdo da tela" causa sérios problemas de desempenho nos drivers MESA. Preciso Permitir que os aplicativos bloqueiem a composição Sempre Velocidade da animação: Os aplicativos podem definir uma dica para bloquear a composição quando a janela estiver aberta. 
  Isto proporciona melhorias de desempenho, por ex. jogos. 
  A configuração pode ser anulada por regras específicas da janela. Autor: %1
Licença: %2 Automático Acessibilidade Aparência Beleza Foco Ferramentas Animação da mudança de área de trabalho virtual Gerenciamento de janelas Configurar o filtro Rígido alvarenga@kde.org Ativar o compositor na inicialização Excluir os efeitos da área de trabalho não suportados pelo Compositor Excluir os efeitos internos da área de trabalho Atualizações da tela inteira Baixar novos efeitos... Dica: Para localizar ou configurar a ativação de um efeito, verifique as configurações do efeito. Instantâneo Equipe de desenvolvimento do KWin Manter as miniaturas da janela: Manter a miniatura da janela interfere sempre com o estado minimizado das janelas. Isso poderá fazer com que as janelas não suspendam o seu trabalho se estiverem minimizadas. André Marcelo Alvarenga Nunca Apenas para as janelas visíveis Apenas quando for econômico OpenGL 2.0 OpenGL 3.1 EGL GLX A composição OpenGL (a padrão) fez o KWin falhar no passado.
Isso provavelmente ocorreu por um erro no driver.
Se você acha que atualizou para um driver mais estável,
poderá redefinir esta proteção, mas tome cuidado que isto poderá resultar em uma falha imediata!
Alternativamente, você pode querer usar a infraestrutura do XRender. Reabilitar a detecção do OpenGL Reutilizar o conteúdo da tela Infraestrutura de renderização: O método de escala "Preciso" não é suportado por todo o hardware e poderá provocar problemas de desempenho e defeitos na renderização. Método de escala: Pesquisar Suave Suave (mais lento) Prevenção de deslizamento ("vsync"): Muito lenta XRender 