��          <      \       p      q   *   �   /   �   �  �   ,   �  C   �  G                      Cannot find '%1' using %2. Connection to %1 weather server timed out. Weather information retrieval for %1 timed out. Project-Id-Version: libplasmaweather
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-13 03:17+0100
PO-Revision-Date: 2017-10-25 11:32-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Não foi possível encontrar '%1' usando %2. A conexão com o servidor meteorológico %1 expirou o tempo limite. Tempo de espera excedido ao baixar a informação meteorológica de %1. 