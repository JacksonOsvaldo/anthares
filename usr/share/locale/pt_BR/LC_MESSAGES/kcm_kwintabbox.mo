��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �  
   �     �  "   �               -     9  	   M     W     g     x  
   �     �  \   �     	  (   	     >	  *   N	  	   y	     �	      �	     �	     �	     �	     �	     �	     �	     
  �   *
  f   �
     ;     W     i                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-10-25 11:34-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Atividades Todas as outras atividades Todas as outras áreas de trabalho Todas as outras telas Todas as janelas Alternativo Área de trabalho 1 Conteúdo Atividade atual Aplicativo atual Área de trabalho atual Tela atual Filtrar janelas por A configurações da política de foco limita a funcionalidade da navegação pelas janelas. Avançar Baixar novo layout do seletor de janelas Janelas ocultas Incluir ícone "Mostrar área de trabalho" Principal Minimização Apenas uma janela por aplicativo Usados recentemente Inverter Telas Atalhos Mostrar a janela selecionada Critério de ordenação: Ordem de empilhamento A janela que estiver selecionada ficará realçada, enquanto todas as outras irão desaparecer. Essa opção requer que os efeitos da área de trabalho estejam ativados. O efeito para substituir a janela da lista, quando os efeitos da área de trabalho estiverem ativados. Áreas de trabalho virtuais Janelas visíveis Visualização 