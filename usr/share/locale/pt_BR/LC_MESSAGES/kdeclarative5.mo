��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �   !  +   �     �  &   �          2  
   9     D  �   U  p   �  7   _  *   �     �     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdeclarative5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-10-19 10:05-0200
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Clique no botão e depois informe o atalho como faria no programa.
Exemplo para o Ctrl+A: Mantenha pressionada a tecla Ctrl e pressione a tecla A. Conflito com o atalho padrão do aplicativo alvarenga@kde.org Console de aplicativos QML do KPackage André Marcelo Alvarenga Nenhum Reatribuir Atalho reservado A combinação de teclas '%1' está atribuída para a ação padrão "%2", usada por alguns aplicativos.
Deseja realmente usá-la como um atalho global? A tecla F12 está reservada no Windows e, por isso, não pode ser usada como atalho global.
Escolha outra tecla. A tecla que você pressionou não é suportada pelo Qt. O único nome do aplicativo (obrigatório) Tecla não suportada Entrada 