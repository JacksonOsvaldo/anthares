��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  -   �     �  (   �          .  '   C      k     �     �     �     �     �  
   �     �     �     �     
  #     %   C  &   i     �     �     �        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_org.kde.plasma.pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2017-03-02 18:53-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1 janela minimizada: %1 janelas minimizadas: %1 janela: %1 janelas: ...e mais %1 janela ...e mais %1 janelas Nome da atividade Número da atividade Adicionar uma área de trabalho virtual Configurar áreas de trabalho... Nome da área de trabalho Número da área de trabalho Mostrar: Não fazer nada Geral Horizontal Ícones Layout: Nenhum texto Apenas na tela atual Remover a área de trabalho virtual Selecionando área de trabalho atual: Mostrar o gerenciador de atividades... Mostra a área de trabalho Padrão Vertical 