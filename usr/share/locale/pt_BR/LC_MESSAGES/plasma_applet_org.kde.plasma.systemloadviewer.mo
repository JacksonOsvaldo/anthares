��    $      <  5   \      0     1     L     Y     o     s     |     �     �     �     �     �     �     �     �     �     �     �     �                         ,     >     L     R     f     l     q     ~     �     �     �  
   �     �  �  �     �     �     �     �     �     �     �     �     �                    $  	   5     ?     H     N     ]     l     r     ~     �     �     �     �     �     �     �  "   �          *     3     D     `  	   x                           	      "      #                                                                                                         $                             !   
    Abbreviation for secondss Application: Average clock: %1 MHz Bar Buffers: CPU CPU %1 CPU monitor CPU%1: %2% @ %3 Mhz CPU: %1% CPUs separately Cache Cache monitor Cached: Circular Colors Compact Bar Dirty memory: General IOWait: Memory Memory monitor Memory: %1/%2 MiB Monitor type: Nice: Set colors manually Show: Swap Swap monitor Swap: %1/%2 MiB Sys: System load Update interval: Used swap: User: Project-Id-Version: plasma_applet_org.kde.plasma.systemloadviewer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-29 03:31+0200
PO-Revision-Date: 2017-07-29 20:35-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 s Aplicativo: Clock (média): %1 MHz Barra Buffers: CPU CPU %1 Monitor da CPU CPU%1: %2% @ %3 Mhz CPU: %1% CPUs em separado Cache Monitor do cache Em cache: Circular Cores Barra compacta Memória suja: Geral Espera E/S: Memória Monitor da memória Memória: %1/%2 MiB Tipo de monitor: Prioridade: Definir as cores manualmente Mostrar: Swap Monitor de memória virtual (swap) Swap: %1/%2 MiB Sistema: Carga do sistema Intervalo de atualização: Memória virtual usada: Usuário: 