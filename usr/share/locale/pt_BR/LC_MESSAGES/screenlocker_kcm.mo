��          �            h     i  
   �     �  .   �     �     �     �      �  *         9  ,   Z  ,   �  0   �     �  �  �     �  
   �     �  ;   �     $     2     E  &   V  1   }  "   �  	   �     �  6   �                           
   	                                                       &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Keyboard shortcut: Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-12-12 15:00-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Blo&quear a tela ao reativar: Ativação Erro Não foi possível testar com sucesso o bloqueador de tela. Imediatamente Atalho de teclado: Bloquear sessão Bloquear a tela automaticamente após: Bloquear a tela quando retornar de uma suspensão Pedir uma senha após o b&loqueio:  min  min  s  s A combinação de teclas global para bloqueio da tela. &Tipo de papel de parede: 