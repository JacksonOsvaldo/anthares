��    =        S   �      8  ;   9     u     �     �     �  6   �  A   
     L     U  $   Y  
   ~     �  #   �     �     �     �     �     �  7        >     [     w  '   �     �     �  $   �  ,   �          3     C     K     ]     y     �     �     �     �     �  �   �  9   �	  "   �	     �	     �	     
     *
     <
     R
     c
  %   u
     �
     �
     �
     �
     �
  
   �
  7        :     T     l     t  �  �  =   [     �     �     �     �            	   -     7  (   <  
   e  ,   p  1   �     �  )   �     �            5   (     ^     ~     �  >   �     �     �  (     :   1     l          �     �  +   �     �     �     �          0     E  �   V  O   7  +   �  
   �  &   �     �     �  "        <     Y  -   q  	   �      �     �     �     �  
   �     �                    &     !          3      0      (                 1      "          =   .   6                                         	      :          5       ,   #   &   7       ;   '             -   
             8                 *           %           <   4   9                )   /   $          2             +       

Choose the previous strip to go to the last cached strip. &Create Comic Book Archive... &Save Comic As... &Strip Number: *.cbz|Comic Book Archive (Zip) @option:check Context menu of comic image&Actual Size @option:check Context menu of comic imageStore current &Position Advanced All An error happened for identifier %1. Appearance Archiving comic failed Automatically update comic plugins: Cache Check for new comic strips: Comic Comic cache: Configure... Could not create the archive at the specified location. Create %1 Comic Book Archive Creating Comic Book Archive Destination: Display error when getting comic failed Download Comics Error Handling Failed adding a file to the archive. Failed creating the file with identifier %1. From beginning to ... From end to ... General Get New Comics... Getting comic strip failed: Go to Strip Information Jump to &current Strip Jump to &first Strip Jump to Strip ... Manual range Maybe there is no Internet connection.
Maybe the comic plugin is broken.
Another reason might be that there is no comic for this day/number/string, so choosing a different one might work. Middle-click on the comic to show it at its original size No zip file is existing, aborting. Range: Show arrows only on mouse over Show comic URL Show comic author Show comic identifier Show comic title Strip identifier: The range of comic strips to archive. Update Visit the comic website Visit the shop &website an abbreviation for Number# %1 days dd.MM.yyyy here strip means comic strip&Next Tab with a new Strip in a range: from toFrom: in a range: from toTo: minutes strips per comic Project-Id-Version: plasma_applet_org.kde.plasma.comic
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-07-18 03:20+0200
PO-Revision-Date: 2017-10-25 11:32-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 

Escolha a tirinha anterior para ir para a última em cache. &Criar arquivo de tirinhas... &Salvar tirinha como... &Número da linha: *.cbz|Arquivo de tirinhas (Zip) Tamanho re&al Armazenar a &posição atual Avançado Tudo Ocorreu um erro para o identificador %1. Aparência Ocorreu uma falha no arquivamento da tirinha Atualizar automaticamente os plugins de tirinhas: Cache Procurar linhas de tirinhas novas a cada: Tirinha Cache de tirinhas: Configurar... Não foi possível criar o arquivo no local indicado. Criar um arquivo tirinhas do %1 Criando o arquivo de tirinhas Destino: Mostrar mensagem de erro quando falhar a obtenção da tirinha Baixar tirinhas Tratamento de erros Não foi possível adicionar um arquivo. Não foi possível criar o arquivo com o identificador %1. Do início até... Do fim até... Geral Baixar novas tirinhas... Ocorreu uma falha na obtenção da tirinha: Ir para a linha Informações Saltar &para a linha atual &Saltar para a primeira linha Saltar para linha... Intervalo manual Talvez não haja uma conexão com a Internet.
O plugin de tirinhas pode estar com problemas.
Outro motivo, pode ser a inexistência de uma tirinha para este dia/número/string, e a escolha de uma diferente poderá funcionar. Clique com o botão do meio do mouse para mostrar a tirinha no tamanho original Não existe nenhum arquivo ZIP, cancelando. Intervalo: Mostrar setas apenas ao passar o mouse Mostrar a URL da tirinha Mostrar o autor da tirinha Mostrar o identificador da tirinha Mostrar o título da tirinha Identificador da linha: O intervalo de linhas de tirinhas a arquivar. Atualizar Visitar a página Web da tirinha Visitar o &site da loja # %1 dias dd.MM.aaaa &Próxima aba com linha nova De: Até: minutos linhas por tirinha 