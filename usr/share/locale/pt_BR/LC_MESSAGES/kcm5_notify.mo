��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  ,   �     �     �     	          *     :  :   S         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-09-14 16:22-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 (c) 2002-2006 Equipe do KDE <h1>Notificações do sistema</h1>O Plasma permite o controle de como você será notificado quando ocorrerem determinados eventos. Há diversas opções de como o usuário pode ser notificado:<ul><li>Como o aplicativo foi originalmente projetado.</li><li>Com um bipe ou outro som.</li><li>Por meio de uma caixa de diálogo de contexto com informações adicionais.</li><li>Pela gravação do evento em um arquivo de registro sem nenhum alerta visual ou de som.</li></ul> Carsten Pfeiffer Charles Samuels Desabilitar os sons para todos estes eventos alvarenga@kde.org Origem do evento: KNotify André Marcelo Alvarenga Olivier Goffart Implementação original Módulo do painel de controle de notificações do sistema 