��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �  
   M     X     _     o     ~  .   �  '   �  3   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-07-28 12:01-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Aparência Cores: Exibir segundos Desenhar grade Mostrar LEDs desligados: Usar uma cor personalizada para os LEDs ativos Usar uma cor personalizada para a grade Usar uma cor personalizada para os LEDs desativados 