��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  �  y     9     ?     ^     n     �     �     �  	   �     �  Z   �     "	     )	  	   D	     N	  /   m	  A   �	  &   �	  '   
     .
     7
     ?
  *   N
  ]   y
     �
     �
      �
          "     >  %   G     m  )   s     �  <   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kross5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-04 20:48-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 Autor Copyright 2006 Sebastian Sauer Sebastian Sauer O script a executar. Geral Adicionar um novo script. Adicionar... Cancelar? Comentário: diniz.bortolotto@gmail.com, lisiane@kdemail.net, alvarenga@kde.org, elchevive@opensuse.org Editar Editar script selecionado. Editar... Executar o script selecionado. Falha ao criar script para o interpretador "%1" Falha ao determinar o interpretador para o arquivo de script "%1" Falha ao carregar o interpretador "%1" Falha ao abrir o arquivo de script "%1" Arquivo: Ícone: Interpretador: Nível de segurança do interpretador Ruby Diniz Bortolotto, Lisiane Sztoltz Teixeira, André Marcelo Alvarenga, Luiz Fernando Ranghetti Nome: A função "%1" não existe Não existe o interpretador "%1" Remover Remover script selecionado. Executar O arquivo de script "%1" não existe. Parar Parar a execução do script selecionado. Texto: Utilitário de linha de comando para executar scripts Kross. Kross 