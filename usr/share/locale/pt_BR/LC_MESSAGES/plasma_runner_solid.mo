��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     �     �  /   �  R   �  O   /  O     R   �  `   "	  c   �	     �	     �	     
     
     
     
  	   +
     5
     N
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-06 23:46-0200
Last-Translator: André Marcelo Alvarenga <andrealvarenga@gmx.net>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@mail.kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n > 1);
 Bloquear o recipiente Ejetar a mídia Localiza os dispositivos que correspondem a :q: Lista todos os dispositivos e permite que sejam montados, desmontados ou ejetados. Lista todos os dispositivos que podem ser ejetados, permitindo essa operação. Lista todos os dispositivos que podem ser montados, permitindo essa operação. Lista todos os dispositivos que podem ser desmontados, permitindo essa operação. Lista todos os dispositivos criptografados que podem ser bloqueados, permitindo essa operação. Lista todos os dispositivos criptografados que podem ser desbloqueados, permitindo essa operação. Montar o dispositivo dispositivo ejetar bloquear montar desbloquear desmontar Desbloquear o recipiente Desmontar o dispositivo 