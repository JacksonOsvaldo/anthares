��    8      �  O   �      �  A   �          2  /   I  8   y     �     �     �     �     �                      $   ,  	   Q     [  !   q  3   �  	   �     �      �  $   �       *   /     Z  	   _  5   i     �     �  
   �     �     �  	   �          !     @  5   O  3   �  6   �     �  ,   �  /   )	  	   Y	     c	     z	     �	     �	  O   �	  Z   �	  b   J
  	   �
  
   �
  P   �
       �  #  ;   �     !     8  -   O  (   }  
   �     �     �  #   �               5     ;     B  #   O  
   s     ~     �  6   �     �     �  -     *   2     ]     s     y       =   �      �     �  
   
          "  
   1     <  *   Z     �  H   �  N   �  G   ;     �  D   �     �     �  "   �          %     A     \  �   �  e   m     �     �  \   �     [                                            ,   	          #       )   $                                         +   (   &   1            7   4                         6           8   
   *   .             /       %      3   !           0   2      "            -       '           5        %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contains 1 item</i> <i>Contains %1 items</i> A search yelded no resultsNo items matching your search About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Back Ben Cooksley Central configuration center by KDE. Configure Configure your system Contains 1 item Contains %1 items Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically Frequently used: General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Marco Martin Mathias Soeken Most Used Most used module number %1 NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a categorized sidebar for control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Search... Show detailed tooltips Sidebar Sidebar View System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2017-10-26 02:06-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 %1 é um aplicativo externo e foi executado automaticamente (c) 2009, Ben Cooksley (c) 2017, Marco Martin <i>Contém 1 item</i> <i>Contém %1 itens</i> Nenhum item corresponde com sua pesquisa Sobre o %1 Sobre o módulo ativo Sobre a exibição ativa Sobre as Configurações do Sistema Todas as configurações Aplicar configurações Autor Voltar Ben Cooksley Central de configuração pelo KDE. Configurar Configura o seu sistema Contém 1 item Contém %1 itens Indica se devem ser usadas as informações detalhadas Desenvolvedor Diálogo diniz.bortolotto@gmail.com, alvarenga@kde.org Expandir o primeiro nível automaticamente Usado frequentemente: Geral Ajuda Exibição em ícones Representação interna do módulo, modelo interno do módulo Nome interno da exibição usada Atalho de teclado: %1 Mantenedor Marco Martin Mathias Soeken Mais usado Módulo mais usado número %1 Diniz Bortolotto, André Marcelo Alvarenga Nenhuma exibição encontrada Fornece uma exibição dos módulos de controle categorizada em ícones. Fornece uma exibição dos módulos de controle categorizada na barra lateral. Fornece uma exibição dos módulos de controle categorizada em árvore Executar novamente %1 Restaurar os valores anteriores para todas as alterações efetuadas Procurar Pesquisar... Mostrar as descrições detalhadas Barra lateral Exibição em barra lateral Configurações do Sistema O aplicativo Configurações do Sistema não conseguiu encontrar nenhuma exibição e, consequentemente, nada pode ser exibido. O aplicativo Configurações do Sistema não conseguiu encontrar nenhuma exibição e, consequentemente, nada está disponível para configurar. As configurações do módulo atual foram alteradas.
Deseja aplicar as alterações ou descartá-las? Exibição em árvore Estilo de exibição Bem-vindo às "Configurações do Sistema", o principal local para configurar o seu sistema. Will Stephenson 