��             +         �     �     �     �  	   �     �               '     3     H     a     t     �  S   �  w   �     `  M   u      �     �     �  	             2     K  %   Z     �     �  #   �     �     �     �  �  	     �     �      �  	   �                3     A     M  +   f     �     �     �  d   �  |   :	     �	  X   �	     (
     :
     H
     a
     p
     �
     �
      �
     �
     �
  *   �
     (     G     `                                            
                                                     	                                                ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Toggle alternative window switching Toggle window switching Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-07-28 11:54-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  ms % %1 - Todas as áreas de trabalho %1 - Cubo %1 - Aplicativo atual %1 - Área de trabalho atual %1 - Cilindro %1 - Esfera Atraso na &reativação: &Mudar de área de trabalho na extremidade: Atra&so na animação: Gerenciador de Atividades Sempre habilitado A quantidade de tempo necessária após acionar uma ação até o próximo acionamento poder ocorrer A quantidade de tempo necessária para o ponteiro do mouse ser empurrado contra a borda da tela antes da ação ser acionada Lançador de aplicativo Alterar a área de trabalho quando o ponteiro do mouse é empurrado para a borda da tela alvarenga@kde.org Bloquear tela André Marcelo Alvarenga Nenhuma ação Somente ao mover as janelas Executar comando Outras configurações Lado a lado em quartos para fora Exibir a área de trabalho Desabilitado Alternar a mudança alternativa de janelas Alternar a mudança de janelas Gerenciamento de janelas da tela 