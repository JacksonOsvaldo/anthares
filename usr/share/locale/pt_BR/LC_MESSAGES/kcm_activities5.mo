��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     �	     �	  !   �	     �	     �	  
   �	     �	     �	     
     0
  
   B
     M
     h
     ~
  Q   �
     �
     	       F   )     p     �     �     �     �     �     �     �        %        ,     2  /   >     n  U   �  &   �                    ,     1     >                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: kcm_activities
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-04-02 14:58-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Não lembrar Percorrer as atividades Percorrer as atividades (inverso) Aplicar Cancelar Alterar... Criar Configurações da atividade Criar uma nova atividade Excluir atividade Atividades Informações da atividade Mudança de atividade Deseja realmente excluir '%1'? Incluir na lista de proibições todos os aplicativos que não estão nesta lista Limpar histórico recente Criar atividade... Descrição: Erro ao carregar os arquivos QML. Verifique sua instalação.
Falta %1 Para todos os ap&licativos Esquecer um dia Esquecer tudo Esquecer a última hora Esquecer as duas últimas horas Geral Ícone Manter histórico Nome: Ape&nas para aplicativos específicos Outro Privacidade Privativa - não rastrear o uso desta atividade Lembrar dos documentos abertos: Lembrar a área de trabalho virtual atual para cada atividade (necessário reiniciar) Atalho para mudar para esta atividade: Atalhos Mudança Papel de parede por   mês  meses para sempre 