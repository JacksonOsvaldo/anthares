��          �      �           	               4  	   I     S      c  "   �      �     �     �  	   �     �               )  
   9     D     S     a     g     {  �  �     8     ;  !   C     e     x     �     �  '   �  ,   �            	        (     @     L     h  
   |     �     �     �     �  	   �                              	                                     
                                      K (HH:MM) (In minutes - min. 1, max. 600) Activate Night Color Automatic Detect location EMAIL OF TRANSLATORSYour emails Error: Morning not before evening. Error: Transition time overlaps. Latitude Location Longitude NAME OF TRANSLATORSYour names Night Color Night Color Temperature:  Operation mode: Roman Gilg Sunrise begins Sunset begins Times Transition duration and ends Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-12 03:09+0100
PO-Revision-Date: 2017-12-12 15:03-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
  K (HH:MM) (em minutos - mín. 1, máx. 600) Ativar cor noturna Automático Detectar localização elchevive@opensuse.org Erro: manhã não está antes da tarde. Erro: os tempos da transição se sobrepõe. Latitude Localização Longitude Luiz Fernando Ranghetti Cor noturna Temperatura da cor noturna: Modo de operação: Roman Gilg Nascer do sol começa Pôr do sol começa Tempos Duração da transição e termina 