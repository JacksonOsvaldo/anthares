��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �  '   L  1   t  /   �  $   �     �  &        9  1   X     �     �     �  5   �     �  7   	  ;   K	     �	  H   �	     �	  g   
  7   o
     �
     �
  (   �
  ;   �
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-07-31 09:55-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Não foi possível iniciar o initramfs. Não foi possível iniciar o update-alternatives. Configurar a tela de apresentação do Plymouth Baixar novas telas de apresentação elchevive@opensuse.org Baixe novas telas de apresentação... Falha ao executar o initramfs. O initramfs retornou com a condição de erro %1. Instala um tema. Marco Martin Luiz Fernando Ranghetti Nenhum tema especificado nos parâmetros do ajudante. Instalador de temas Plymouth Defina uma tela de apresentação global para o sistema O tema a instalar deve ser um arquivo comprimido existente. O tema %1 não existe. Tema corrompido: o arquivo .plymouth não foi encontrado dentro do tema. A pasta do tema %1 não existe. Este módulo permite-lhe configurar o visual de todo o espaço de trabalho com algumas predefinições. Não foi possível executar/autenticar a ação: %1, %2 Desinstalar Desinstala um tema. Falha ao executar o update-alternatives. O update-alternatives retornou com a condição de erro %1. 