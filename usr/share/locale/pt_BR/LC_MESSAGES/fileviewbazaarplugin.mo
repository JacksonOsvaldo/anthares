��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  8   �	  1   �	  9   
  !   V
  *   x
  )   �
  )   �
  5   �
  6   -  -   d  4   �  -   �  0   �  6   &  2   ]  :   �  #   �     �       *   )  "   T  1   w  (   �  '   �     �          #     7     J     f     }  (   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-09-03 11:07-0300
Last-Translator: André Marcelo Alvarenga <andrealvarenga@gmx.net>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=(n > 1);
 Os arquivos foram adicionados ao repositório do Bazaar. Adicionando arquivos ao repositório do Bazaar... Falha na adição dos arquivos ao repositório do Bazaar. O registro do Bazaar foi fechado. Falha no envio das alterações do Bazaar. As alterações do Bazaar foram enviadas. Enviando as alterações para o Bazaar... Falha na extração do repositório remoto do Bazaar. Realizada extração do repositório remoto do Bazaar. Extraindo do repositório remoto do Bazaar... Falha no envio para o repositório remoto do Bazaar. Enviado para o repositório remoto do Bazaar. Enviando para o repositório remoto do Bazaar... Os arquivos foram removidos do repositório do Bazaar. Removendo os arquivos do repositório do Bazaar... Falha na remoção dos arquivos do repositório do Bazaar. Falha na revisão das alterações. As alterações foram revistas. Revisando alterações... Falha na execução do registro do Bazaar. Executando o registro do Bazaar... Falha na atualização do repositório do Bazaar. O repositório do Bazaar foi atualizado. Atualizando o repositório do Bazaar... Adição do Bazaar... Envio do Bazaar... Exclusão do Bazaar Registro do Bazaar Extração remota do Bazaar Envio remoto do Bazaar Atualização do Bazaar Mostrar as alterações locais do Bazaar 