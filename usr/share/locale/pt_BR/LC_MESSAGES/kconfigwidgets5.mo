��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     �     �     �     �                    (     .     C     Q     c  	   w     �     �     �     �     �     �     �     �  	                  /     <  	   B     L     Y     h     y     �     �  	   �     �     �  	   �  	   �     �     �  
                  %  #   -     Q     ^     p     �  $   �  0   �     �  	     2     
   O  Z   Z     �     �     �     �                2     Q     f     }     �     �     �     �  ]   �     R     b  &   q     �  +   �     �     �            	   '     1     N  7   n     �     �     �     �     �          1     I  Y   g     �  �   �  /   q  #   �  $   �  )   �           5     B  (   Z     �  6   �  5   �           	  3     )   F  4   p     �  	   �     �     �     �     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kconfigwidgets5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-12-12 15:55-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Manual do %1 &Sobre o %1 Tamanho re&al &Adicionar favorito &Voltar Fe&char &Configurar o %1... &Copiar &Remover &Doar &Editar favoritos... &Localizar... &Primeira página A&justar à página &Avançar &Ir para... &Ir para a linha... &Ir para a página... &Última página Enviar por e-&mail... &Mover para a Lixeira &Novo Próxi&ma página &Abrir... Co&lar Página &anterior &Imprimir... Sai&r &Reexibir &Renomear... Substitui&r... &Relatar erro... &Salvar &Salvar configurações &Ortografia... &Desfazer A&cima &Zoom... &Próxima &Anterior Mo&strar dicas ao iniciar Você sabia...?
 Configurar Dica do dia Sobre o &KDE &Limpar Verificar a ortografia no documento Limpar lista Fecha o documento Configurar &notificações... Configurar atal&hos... Configurar &barras de ferramentas... Copia a seleção para a área de transferência Cria um novo documento Recor&tar Recorta a seleção para a área de transferência &Desmarcar diniz.bortolotto@gmail.com, lisiane@kdemail.net, alvarenga@kde.org, elchevive@opensuse.org Detectar automaticamente Padrão &Modo de tela inteira Localizar p&róxima Localizar &anterior Ajustar à al&tura da página Ajustar à &largura da página &Voltar no documento &Avançar no documento Ir para a primeira página Ir para a última página Ir para a próxima página Ir para a página anterior Ir pra cima Diniz Bortolotto, Lisiane Sztoltz Teixeira, André Marcelo Alvarenga, Luiz Fernando Ranghetti Nenhuma entrada Abrir &recente Abrir um documento aberto recentemente Abre um documento existente Cola o conteúdo da área de transferência Visuali&zar impressão Imprime o documento Fecha o aplicativo &Refazer Re&verter Mostra o documento novamente Refaz a última ação desfeita Reverte as alterações não salvas feitas ao documento Salvar &como... Salva o documento Salva o documento com novo nome Selecion&ar tudo Seleciona o nível de zoom Envia o documento por e-mail &Mostrar barra de menus Mostrar barra de &ferramentas Mostrar a barra de menus<p>Mostra a barra de menus novamente, após ter sido ocultada</p> Mostrar barra de st&atus Mostrar barra de status<p>Mostra a barra de status, que é a barra existente na base da janela e é usada para obter informações sobre o status.</p> Mostra uma previsão da impressão do documento Mostrar ou ocultar a barra de menus Mostrar ou ocultar a barra de status Mostrar ou ocultar a barra de ferramentas Mudar o idioma do ap&licativo... &Dica do dia Desfaz a última ação Mostra o documento no seu tamanho actual O que é is&to? Você não tem permissão para salvar a configuração Será solicitada a sua autenticação antes de salvar Ampl&iar &Reduzir Ajusta para que a altura da página caiba na janela Ajusta para que a página caiba na janela Ajusta para que a largura da página caiba na janela &Voltar &Avançar Página &inicial Aj&uda sem nome 