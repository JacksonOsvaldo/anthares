��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l     H     Q     e     {     �     �     �     �     �  	   �     �     �     �                    *     9     @     I     Q     p     �     �     �     �     �     �  #   �  	   �          "     2     D  2   K     ~     �     �     �  #   �     �     �     �          	               &     5  	   G     Q  "   X     {     �     �     �  N   �     �          "     *  	   8     B     J     P      W     x     �     �     �  "   �     �  0   �  #   /  3   S     �     �     �     �     �     �     �     �  
   �     �     �            !        4     D     ^     y  Z   �     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-12-12 14:52-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 &Excluir &Esvaziar a Lixeira &Mover para a Lixeira &Abrir Co&lar &Propriedades Atualiza&r área de trabalho Atualiza&r exibição &Recarregar &Renomear Escolher... Limpar ícone Alinhar Aparência: Organizar por Organizar por Organização: Voltar Cancelar Colunas Configurar a área de trabalho Título personalizado Data Padrão Decrescente Descrição Desmarcar tudo Layout da área de trabalho Digite aqui o título personalizado Recursos: Padrão de nomes de arquivos: Tipo de arquivo Tipos de arquivo: Filtro Janelas instantâneas de visualização das pastas Primeiro as pastas Primeiro as pastas Caminho completo Entendi Ocultar os arquivos correspondentes Enorme Tamanho do ícone Ícones Grande Esquerda Lista Localização Localização: Bloquear no local Bloqueado Médio Mais opções de visualização... Nome Nenhum OK Botão do painel: Pressione e mantenha pressionado os widgets para movê-los e mostrar as alças Plugins de visualização Miniaturas de visualização Remover Redimensionar Restaurar Direita Girar Linhas Pesquisar por tipo de arquivo... Selecionar tudo Selecionar pasta Marcadores da seleção Exibir todos os arquivos Exibir os arquivos correspondentes Exibir um local: Mostrar os arquivos associados a atividade atual Exibir a pasta da área de trabalho Mostrar a caixa de ferramentas da área de trabalho Tamanho Pequeno Pequeno Ordenar por Ordenar por Ordenação: Especificar uma pasta: Linhas de texto Minúsculo Título: Dicas Ajustes Tipo Digite aqui um caminho ou uma URL Sem ordenação Usar ícone personalizado Movimentação dos widgets Widgets desbloqueados Você pode pressionar e manter pressionados os widgets para movê-los e mostrar as alças. Modo de exibição 