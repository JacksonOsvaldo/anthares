��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �                (     D     S  
   i     t     �     �     �     �  D   �  "   �  #        @     G     S  	   `  #   j  $   �  %   �  &   �                                       
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-17 22:46-0300
Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>
Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
 %1 arquivo %1 arquivos %1 pasta %1 pastas %1 de %2 processados %1 de %2 processados a %3/s %1 processados %1 processados a %2/s Aparência Comportamento Cancelar Limpar Configurar... Tarefas concluídas Lista das transferências/trabalhos de arquivos em curso (kuiserver) Movê-las para uma lista diferente Movê-los para uma lista diferente. Pausar Removê-las Removê-los. Continuar Mostrar todas as tarefas numa lista Mostrar todas as tarefas numa lista. Mostrar todas as tarefas numa árvore Mostrar todas as tarefas numa árvore. Mostrar em janelas separadas Mostrar em janelas separadas. 