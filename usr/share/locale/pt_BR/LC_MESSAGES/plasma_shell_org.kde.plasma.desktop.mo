��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
  
   e     p     �     �     �     �     �     �     �     �          	     !     /     8  
   A     L     S     Z  
   \     g     |     �     �     �     �     �     �     �     �     �       E     T   _     �     �     �  	   �     �     �                    (     9     <     R     c     k     z     �     �     �     �     �  e   �  �   *     �     �     �               ,     9     I     b     j     r     �     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.plasma.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-10-25 11:29-0300
Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>
Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 2.0
 Atividades Adicionar ação Adicionar espaçador Adicionar widgets... Alt Widgets alternativos Sempre visível Aplicar Aplicar configurações Aplicar agora Autor: Ocultar automaticamente Botão voltar Inferior Cancelar Categorias Centro Fechar + Configurar Configurar atividade Criar atividade... Ctrl Atualmente sendo usado Excluir E-mail: Botão avançar Baixar novos widgets Altura Rolagem horizontal Digite aqui Atalhos de teclado O layout não pode ser alterado enquanto os widgets estão bloqueados Mudanças do layout devem ser aplicadas antes que outras mudanças possam ser feitas Layout: Esquerda Botão esquerdo Licença: Bloquear widgets Maximizar o painel Meta Botão do meio Mais opções... Ações do mouse OK Alinhamento do painel Remover o painel Direita Botão direito Borda da tela Procurar... Shift Parar atividade Atividades paradas: Mudar As configurações do módulo atual foram alteradas. Deseja aplicar as alterações ou descartá-las? Este atalho ativará o miniaplicativo: Ele irá colocar o teclado em primeiro plano para ele e, caso o miniaplicativo tenha uma janela (como o menu inicial), ela será aberta. Superior Desfazer desinstalação Desinstalar Desinstalar widget Rolagem vertical Visibilidade Papel de parede Tipo de papel de parede: Widgets Largura As janelas podem cobrir As janelas passam para trás 