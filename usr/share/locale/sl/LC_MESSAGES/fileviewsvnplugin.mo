��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �     [  *   d  '   �  2   �  '   �  #   	     6	  0   U	  -   �	  8   �	  .   �	  /   
  3   L
  Z   �
  *   �
  %     !   ,  	   N     X     e  
   r     }     �     �     �     �     �                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2016-11-06 12:49+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Uveljavi Datoteke so bile dodane v skladišče SVN. Dodajanje datotek v skladišče SVN ... Dodajanje datotek v skladišče SVN je spodletelo. Uveljavitev sprememb SVN je spodletela. Spremembe SVN so bile uveljavljene. Uveljavljanje sprememb SVN ... Datoteke so bile odstranjene iz skladišča SVN. Odstranjevanje datotek iz skladišča SVN ... Odstranjevanje datotek iz skladišča SVN je spodletelo. Datoteke so bile povrnjene iz skladišča SVN. Poteka povrnitev datotek iz skladišča SVN ... Povrnitev datotek iz skladišča SVN je spodletela. Posodobitev stanja SVN je spodletela. Onemogočanje možnosti »Pokaži posodobitve SVN«. Posodobitev skladišča SVN je spodletela. Skladišče SVN je bilo posodobljeno. Posodabljanje skladišča SVN ... SVN dodaj SVN uveljavi SVN izbriši SVN povrni SVN Posodobi Pokaži krajevne spremembe SVN Pokaži posodobitve SVN Opis: SVN uveljavi Pokaži posodobitve 