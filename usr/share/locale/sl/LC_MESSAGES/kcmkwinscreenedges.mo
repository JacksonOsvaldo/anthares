��    %      D  5   l      @     A     E     G  	   Y     c     |     �     �     �     �     �     �            S   ,  w   �     �  M         [     |  ?   �     �  	   �     �     
     #  %   2     X     e  F   �  #   �     �  ]     R   f     �     �  �  �     �	     �	     �	  
   �	     �	     �	  	   �	     
     
     0
     K
     c
     �
     �
  O   �
  W   �
     S  C   i     �     �  )   �     �               ,     9  0   J     {     �  5   �  $   �     �  ]     =   m     �     �               !      "          #   %                                                                            $                          	                
                            ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Active Screen Corners and Edges Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen Maximize windows by dragging them to the top edge of the screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Tile windows by dragging them to the left or right edges of the screen Toggle alternative window switching Toggle window switching Trigger an action by pushing the mouse cursor against the corresponding screen edge or corner Trigger an action by swiping from the screen edge towards the center of the screen Window Management of the screen Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2018-01-11 21:00+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
  ms  % %1 - vsa namizja %1 - kocka %1 - trenutni program %1 - trenutno namizje %1 - valj %1 - krogla Zakasnitev &ponovne sprožitve: Na robu &preklopi namizje: &Zakasnitev sprožitve: Dejavni robovi in koti zaslona Upravljalnik dejavnosti Vedno omogočeno Čas po sprožitvi dejanja, potreben, preden se lahko sproži naslednje dejanje Čas, potreben za tiščanje miškine kazalke ob rob zaslona, preden se sproži dejanje Zaganjalnik programov Spremeni namizje, ko je miškina kazalka pritisnjena ob rob zaslona andrejm@ubuntu.si Zakleni zaslon Razpni okna z vleko ob vrhnji rob zaslona Andrej Mernik Brez dejanja Samo pri premikanju oken Zaženi ukaz Druge nastavitve Četrtinsko razpostavljanje sproženo v zunanjih Pokaži namizje Onemogočeno Razpostavi okna z vleko ob levi ali desni rob zaslona Preklopi drugotno preklapljanje oken Preklopi preklapljanje oken Da sprožite dejanje, pritisnite miškino kazalko ob ustrezen rob ali v ustrezen kot zaslona. Da sprožite dejanje, povlecite od roba proti sredini zaslona Upravljanje z okni zaslona 