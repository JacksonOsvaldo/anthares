��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  =     E   F     �     �     �     �  
   �     �     �     �     �                9     
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 14:24+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Zapri Samodejno kopiraj Ne prikaži tega pogovornega okna, temveč samodejno kopiraj. Sem spustite besedilo ali sliko, da jo pošljete na spletno storitev. Napaka med pošiljanjem. Splošno Velikost zgodovine: Prilepi Počakajte Poskusite znova. Pošiljanje ... Deli Souporabe za »%1« Uspešno poslano Naslov URL je bil deljen Pošlji %1 na spletno storitev 