��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     	     	     	     >	     M	     V	     \	     o	     w	     �	  !   �	     �	     �	     �	     �	     �	     �	     
     
     
     1
     E
     S
     i
     �
     �
     �
     �
     �
     �
     �
     �
          )     @     X     ^  <   {     �     �     �  	   �  
   �     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2018-01-11 20:46+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Razpoložljive velikosti: %1) Izberite sliko Napredno Avtor Sa&modejna prijava Ozadje: Počisti sliko Ukazi Ni bilo mogoče razširiti arhiva Tema kazalke: Prilagodi temo Privzeta Opis Prejmi nove teme za SDDM andrejm@ubuntu.si Splošno Dobi novo temo Ukaz za zaustavitev: Namesti iz datoteke Namesti temo. Neveljaven paket teme Naloži iz datoteke ... Prijavni zaslon z uporabo SDDM Največji UID: Najmanjši UID: Andrej Mernik Ime Predogled ni na voljo Ukaz za ponovni zagon: Ponovna prijava po končanju Odstrani temo Nastavitve SDDM za KDE Namestilnik tem za SDDM Seja: Privzeta tema kazalke v SDDM Tema za namestitev. Mora biti obstoječa datoteka z arhivom. Tema Ni mogoče namestiti teme Odstrani temo. Uporabnik Uporabnik: 