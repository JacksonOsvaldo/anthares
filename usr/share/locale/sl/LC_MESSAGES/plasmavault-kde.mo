��    ?        Y         p  �  q  }  J  M  �        
   7     B     K     j     �  <   �     �     �  	   �     �  .     %   A     g     }     �     �     �     �     �     �     �     �  4        B  9   T     �     �     �  �   �  !   �  t   �     8     D     a     n     s  	   �     �  -   �     �  B   �  &     ?   B  '   �  )   �  7   �  .     5   ;  +   q     �     �     �  ,   �          -  9   :  V   t  C   �  �    �  �  x  �    3     F"  
   ["     f"  %   o"  #   �"     �"  A   �"  (   #     -#     ;#     C#  ,   W#  "   �#     �#     �#     �#     �#     �#      $     $     !$     A$     G$  9   b$     �$  F   �$     �$     %     !%  �   *%     &  j   &     �&     �&     �&     �&     �&     �&     �&  %   '     -'  5   3'     i'  ?   �'  -   �'  .   �'  <   #(  &   `(  ,   �(  /   �(  "   �(     )     ')  2   7)  #   j)     �)  D   �)  D   �)     &*     !      	   2   ?   
   /                 +   (   *       1            )   >           8          9   $              7             .       '   6          %   <             3   ,          ;       =   &                              -           #      "          :      5      0                                   4               <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. @title:windowCreate a New Vault Activities Backend: Can not create the mount point Can not open an unknown vault. Change Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create Create a New Vault... CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Failed to open: %1 Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Password: Plasma Vault Please enter the password to open this vault: Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2018-01-12 17:30+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Varnostno obvestilo:</b>
                             Glede na varnostni pregled s strani Taylorja Hornbya (Defuse Security),
                             je trenutna izvedba Encfs ranljiva ali potencialno ranljiva na
                             več vrst napadov.
                             Napadalec z bralnim/pisalnim dostopom do šifriranih podatkov
                             lahko na primer zniža kompleksnost šifriranja za prihodnje podatke,
                             brez da bi uporabnik to opazil, lahko pa določene podatke pridobi
                             tudi s časovnim preučevanjem.
                             <br /><br />
                             To pomeni, da šifriranih podatkov ne bi smeli
                             usklajevati z oblačnimi storitvami za shranjevanje
                             ali pa jih uporabljati v primerih, ko je omogočen
                             pogosti dostop do njih s strani napadalca.
                             <br /><br />
                             Za več podrobnosti si oglejte <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a>. <b>Varnostno obvestilo:</b>
                             CryFS šifrira vaše datoteke tako, da jih lahko shranite kjerkoli. Dobro se obnese
                             tudi pri uporabi z oblačnimi storitvami kot so npr. Dropbox, iCloud, OneDrive.
                             <br /><br />
                             V nasprotju z drugimi rešitvami za prikrivanje
                             datotečnega sistema, šifrirna oblika ne razkrije
                             zgradbe map, števila ali velikosti datotek.
                             <br /><br />
                             Zapomnite si, da čeprav se CryFS šteje za
                             varnega, ni bil predmet nobenega neodvisnega
                             varnostnega preizkušanja, ki bi to potrdilo. Ustvari novo shrambo Dejavnosti Zaledje: Ni mogoče ustvariti priklopne točke Ni mogoče odpreti neznane shrambe. Spremeni Izberite šifrirni sistem, ki ga želite uporabiti za to shrambo: Izberite uporabljen šifrirni algoritem: Zapri shrambo Nastavi Nastavi shrambo ... Nastavljenega zaledja ni mogoče začeti: %1 Nastavljeno zaledje ne obstaja: %1 Najdena pravilna različica Ustvari Ustvari novo shrambo ... CryFS Naprava je že odprta Naprava ni odprta Pogovorno okno Tega obvestila ne prikaži več EncFS Mesto šifriranih podatkov Map ni bilo mogoče ustvariti. Preverite vaša dovoljenja Izvajanje ni uspelo Ni bilo mogoče pridobiti seznama programov, ki uporabljajo to shrambo Ni bilo mogoče odpreti: %1 Vsili zaprtje Splošno Če omejite to shrambo samo na določene dejavnosti, bo v apletu prikazana samo, če se v teh dejavnostih nahajate. V kolikor boste preklopili na dejavnost, ki ni vključena v omejitev, bo shramba samodejno zaprta. Omeji na izbrane dejavnosti: Zapomnite si, da ni mogoče obnoviti pozabljenega gesla. Če pozabite geslo, bodo vaši podatki izgubljeni Priklopna točka Priklopna točka ni navedena Priklopna točka: Naprej Odpri v upravljalniku datotek Geslo: Shramba za Plasmo Vnesite geslo, da odprete to shrambo: Nazaj Mapa priklopne točke ni prazna. Shramba ne bo odprta Navedeno zaledje ni na voljo Nastavitve shrambe je mogoče spreminjati le, če je ta zaprta. Shramba ni znana, zato je ni mogoče zapreti. Shramba ni znana, zato je ni mogoče uničiti. Ta naprava je že registrirana in je ni mogoče poustvariti. Ta mapa že vsebuje šifrirane podatke Ni mogoče zapreti shrambe, ker je v uporabi Ni mogoče zapreti shrambe, ker jo uporablja %1 Ni bilo mogoče zaznati različice Ni bilo mogoče izvesti dejanja Neznana naprava Neznana napaka. Ni bilo mogoče ustvariti zaledja. Uporabi privzet šifrirni algoritem Ime sh&rambe: Nameščena je napačna različica. Zahtevana različica je %1.%2.%3 Za šifrirano shrambo in priklopno točko morate izbrati prazne mape %1: %2 