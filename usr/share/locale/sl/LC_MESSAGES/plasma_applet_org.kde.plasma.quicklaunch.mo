��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  V   �          	          ,     B     Q     Z     f     x     �     �     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2016-01-23 10:20+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Dodaj zaganjalnik ... Zaganjalnike dodajte z vlečenjem in spuščanjem ali pa z uporabo vsebinskega menija. Videz Razporeditev Uredi zaganjalnik ... Omogoči pojavno okno Vnesite naslov Splošno Skrij ikone Največ stolpcev: Največ vrstic: Hitri zagon Odstrani zaganjalnik Prikaži skrite ikone Pokaži imena zaganjalnikov Pokaži naslov Naslov 