��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     `     i  
   u     �     �     �     �     �     �  	   �  Z   �       	   "     ,     ?     O  	   ^     h     u     �     �  q   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-23 17:25+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 %1 teče %1 ne teče &Ponastavi &Začni Napredno Videz Ukaz: Prikaz Izvedi ukaz Obvestila Preostaja: %1 sekund Preostaja: %1 sekunda Preostajata: %1 sekundi Preostajajo: %1 sekunde Zaženi ukaz Zaus&tavi Prikaži obvestilo Pokaži sekunde Pokaži naslov Besedilo: Odštevalnik Odštevalnik zaključen Odštevalnik teče Naslov: Uporabite miškin kolešček za spremembo števk ali izberite vnaprej določene odštevalnike v vsebinskem meniju 