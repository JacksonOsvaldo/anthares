��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  �       �     �     �                     +     .     :     J     j     |  
   �     �  	   �  
   �     �     �  	   �     �  	   �               !     1     @  &   D  	   k     u     x     �  $   �     �  (   �  	   �     �     �     �            
   &     1  	   @     J     [     m  "   {     �     �     �     �     �     �     �     �     �          "     $     (     ,     .     1     5     9     <     >     A     E     I     L     S     U     Y     ]  
   f     q     �     /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-15 17:13+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
  min. %1 %2 %1 (%2) Videz Beaufortova lestvica bft Celzija °C ° Podrobnosti Fahrenheita °F %1 dni %1 dan %1 dneva %1 dnevi Hektopaskalov hPA V: %1 N: %2 Visoka: %1 Palcev živega srebra inHg Kelvina K Kilometrov Kilometrov na uro km/h Kilopaskalov kPa Vozlov kt Mesto: Nizka: %1 Metrov na sekundo m/s Milj Milj na uro mph Milibarov mbar N/D Za »%1« ni najdene vremenske postaje Obvestila  % Pritisk: Poišči Izberite ponudnike storitev za vreme - Pokaži temperaturo v strnjenem načinu: Nastavite Temperatura: Enote Posodobi vsakih: Vidljivost: Vremenska postaja Brezvetrje Hitrost vetra: %1 (%2 %) Vlažnost: %1 %2 Vidljivost: %1 %2 Rosišče: %1 Humidex (vlažnost in toplota): %1 padajoč naraščajoč stalen Težnja pritiska: %1 Pritisk: %1 %2 %1 %2 Vidljivost: %1 %1 (%2) Izdana opozorila: Izdana opazovanja: V VSV VJV S SV SSV SSZ SZ J JV JJV JJZ JZ SPREM. Z ZSZ ZJZ %1 %2 %3 Brezvetrje Hladnost vetra: %1 Sunki vetra: %1 %2 