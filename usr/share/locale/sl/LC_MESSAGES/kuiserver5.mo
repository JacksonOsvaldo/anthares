��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %  .   �     &     E     W     p     |     �  
   �  	   �     �     �     �  3   �          !     >     E     R     `  #   i     �  #   �     �     �                                 
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-09-01 17:28+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 %1 datotek %1 datoteka %1 datoteki %1 datoteke %1 map %1 mapa %1 mapi %1 mape Obdelano %1 od %2 Obdelano %1 od %2 s %3/s Obdelano %1 Obdelano %1 s %2/s Videz Obnašanje Prekliči Počisti Nastavi ... Zaključeni posli Seznam tekočih prenosov datotek/poslov (kuiserver) Premakni jih na drug seznam Premakni jih na drug seznam. Premor Odstrani jih Odstrani jih. Nadaljuj Prikaži vse posle v obliki seznama Prikaži vse posle v seznamu. Prikaži vse posle v obliki drevesa Prikaži vse posle v drevesu. Prikaži ločena okna Prikaži ločena okna. 