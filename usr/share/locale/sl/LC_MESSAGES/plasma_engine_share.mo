��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  -     -   <  2   j  %   �  '   �  )   �          /  $   >         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-06-24 08:49+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Vrste MIME datoteke ni bilo mogoče ugotoviti Vseh zahtevanih funkcij ni bilo mogoče najti Ponudnika z navedenim ciljem ni bilo mogoče najti Napaka med poskusom izvajanja skripta Neveljavna pot za zahtevanega ponudnika Izbrane datoteke ni bilo mogoče prebrati Storitev ni bila na voljo Neznana napaka Podati morate naslov URL te storitve 