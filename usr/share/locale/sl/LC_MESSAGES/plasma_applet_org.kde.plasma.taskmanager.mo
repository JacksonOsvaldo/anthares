��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �  	   �     �     	                    2     N  !   ]  
     4   �  2   �     �  
   �     
  
        #     5  .   L     {     �     �     �     �     �     �     �     �                    3     O     V     ^  $   e     �     �     �     �     �     �     �          (     0     =     C     W     n  -   �     �     �     �     �     �     �               (  2   .  ,   a  *   �  *   �     �  6        :     Q     X     g  	   |     �     �     �     �     �     �        L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-18 17:37+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Vs&a namizja &Zapri &Celozaslonski način Pre&makni &Novo namizje &Pripni Zvi&j &%1 %2 Na voljo tudi na %1 Dodaj k trenutni dejavnosti Vse dejavnosti Temu programu dovoli združevanje Po abecedi Vedno razvrsti opravila v stolpce s toliko vrsticami Vedno razvrsti opravila v vrstice s toliko stolpci Razporeditev Obnašanje Po dejavnosti Po namizju Po imenu programa Zapri okno ali skupino Kroženje med opravili z miškinim koleščkom Ne združuj Ne razvrščaj Filtri Pozabi nedavne dokumente Splošno Združevanje in razvrščanje Združevanje: Poudari okna Velikost ikon: — Ohrani n&ad drugimi Ohra&ni pod drugimi Ohrani zaganjalnike ločene Velika &Razpni Ročno Označi programe, ki predvajajo zvok Največ stolpcev: Največ vrstic: S&krči Skrči/obnovi okno ali skupino Več dejanj Premakni na &trenutno namizje Premakni v dej&avnost Premakni na namizj&e Utišaj Nov primerek Na %1 na vseh dejavnostih na trenutni dejavnosti Ob srednjem kliku: Združi samo, ko je upravljalnik opravil poln Odpri skupine v pojavnih oknih Obnovi 9999+ Naredi premor Naslednja skladba Predhodna skladba Končaj &Spremeni velikost Odpni Še %1 mest Še %1 mesto Še %1 mesti Še %1 mesta Pokaži samo opravila iz trenutne dejavnosti Pokaži samo opravila s trenutnega namizja Pokaži samo opravila s trenutnega zaslona Pokaži samo skrčena opravila V gumbih opravil prikaži napredek in podatke o stanju Prikaži orodne namige Majhna Razvrščanje: Zaženi nov primerek Predvajaj Zaustavi Ne naredi nič &Pripni Združi/razdruži Na voljo na %1 Na voljo na vseh dejavnostih 