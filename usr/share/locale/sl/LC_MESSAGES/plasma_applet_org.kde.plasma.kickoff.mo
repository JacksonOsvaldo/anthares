��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  
   �               -     :     @     I     `  w   l     �  2   �     *     7  	   F     P     W     _     l     �     �     �     �     �     �     	     '	     ?	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-11 21:19+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Izberi ... Počisti ikono Dodaj med priljubljene Vsi programi Videz Programi Programi posodobljeni. Računalnik Povlecite zavihke med okvirji, da jih pokažete/skrijete. Z vlečenjem vidnih zavihkov jim lahko spremenite vrstni red. Uredi programe ... Razširi iskanje na zaznamke, datoteke in e-pošto Priljubljene Skriti zavihki Zgodovina Ikona: Zapusti Gumbi menija Pogosto uporabljeno na vseh dejavnostih na trenutni dejavnosti Odstrani iz priljubljenih Pokaži v priljubljenih Programe pokaži po imenu Razvrsti po abecedi Preklopi zavihke ob prehodu Tipkajte za iskanje ... Vidni zavihki 