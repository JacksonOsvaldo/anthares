��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �  
   	     	      	     0	     8	     A	     ^	     q	     y	     �	     �	     �	     �	     �	  
   �	     �	  )   �	  '   
     @
     W
     j
     y
     �
     �
     �
     �
      �
     �
                    $        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-01-11 21:22+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 % Prilagodi glasnost za %1 Programi Zvok utišan Glasnost zvoka Obnašanje Naprave za zajem Tokovi za zajem Utišaj Privzeto Zmanjšaj glasnost mikrofona Zmanjšaj glasnost Naprave Splošno Vrata Povečaj glasnost mikrofona Povečaj glasnost Največja glasnost: Utišaj Utišaj %1 Utišaj mikrofon Noben program ne predvaja ali snema zvoka Ni najdenih izhodnih ali vhodnih naprav Naprave za predvajanje Tokovi predvajanja  (ni na voljo)  (odklopljeno) Zvišaj največjo glasnost Pokaži dodatne možnosti za %1 Glasnost Glasnost na %1 % Povratne informacije o glasnosti Korak glasnosti: %1 (%2) %1: %2 100 % %1 % 