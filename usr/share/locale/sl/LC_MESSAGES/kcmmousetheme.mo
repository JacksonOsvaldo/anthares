��          �   %   �      @     A  �   `  m   �  �   P  )   �  `        o     |     �     �     �      �     �     �  '        ,     >     ]     b     s  ?   �  Y   �  +     H   F  �  �     a     �  m    	     n	     �	  W   �	  	   �	     
     &
     3
  %   8
  8   ^
     �
     �
  #   �
     �
  &   �
               *  2   8  Q   k  '   �  =   �                                                 	                                                               
              (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @item:inlistbox sizeResolution dependent A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new Theme Get new color schemes from the Internet Install from File NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. You have to restart the Plasma session for these changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-07-12 17:23+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 (c) 2003-2007 Fredrik Höglund <qt>Ali zares želite odstraniti temo kazalk <em>%1</em>?<br /> To bo izbrisalo vse datoteke, ki jih je namestila ta tema.</qt> <qt>Teme, ki jo trenutno uporabljate, ne morete izbrisati.<br />Najprej morate preklopiti na drugo temo.</qt> (Razpoložljive velikosti: %1) Odvisno od ločljivosti Tema z imenom %1 že obstaja v vaši mapi s temami ikon. Ali jo želite zamenjati s to? Potrditev Spremenjene nastavitve kazalke Tema kazalke Opis Povlecite ali vnesite naslov URL teme gregor.rakar@kiss.si,jlp@holodeck1.com,andrejm@ubuntu.si Fredrik Höglund Dobi novo temo Dobi nove barvne sheme iz interneta Namesti iz datoteke Gregor Rakar,Jure Repinc,Andrej Mernik Ime Prepišem temo? Odstrani temo Datoteka %1 najbrž ni veljaven arhiv teme kazalk. Ni bilo mogoče prejeti arhiva teme kazalk. Preverite, če je naslov %1 pravilen. Ni mogoče najti arhiva teme kazalk %1. Za uveljavitev teh sprememb morate znova zagnati sejo Plasme. 