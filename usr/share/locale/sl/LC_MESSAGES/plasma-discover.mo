��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �     �  +   �  !   '  F   I  F   �  ?   �          0  8   A     z     �     �  $   �     �     �               "     0     8     K     f     y     �     �  	   �     �      �     �     �  5   	  A   ?  '   �     �     �  <   �            $         E  &   U     |  
   �     �     �     �     �     �     �     �  #   �  #        5     C     `     u     �     �  J   �  7   �     %     .     5     >     M     S  =   f     �     �     �     �     �  
   �     �     	  	         *     C     H     _  	   r     |     �     �     �     �     �     �     �                     -     D  "   Y         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-16 17:16+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 
Na voljo tudi v %1 %1 %1 (%2) %1 (privzeto) <b>%1</b>, avtorja %2 <em>%1 od %2 oseb je ta ocena uporabna</em> <em>Povejte nam o tej oceni!</em> <em>Uporabno? <a href='true'><b>Da</b></a>/<a href='false'>Ne</a></em> <em>Uporabno? <a href='true'>Da</a>/<a href='false'><b>Ne</b></a></em> <em>Uporabno? <a href='true'>Da</a>/<a href='false'>Ne</a></em> Pridobivanje posodobitev Pridobivanje ... Ni znano, kdaj se je nazadnje preverilo za posodobitvami Iskanje posodobitev Ni posodobitev Na voljo ni nobene posodobitve Preveriti bi morali za posodobitvami Vaš sistem je posodobljen Posodobitve Posodabljanje ... Sprejmi Dodaj vir ... Dodatki Aleix Pol Gonzalez Raziskovalnik po programih Uveljavi spremembe Razpoložljiva zaledja:
 Razpoložljivi načini:
 Nazaj Prekliči Kategorija: Preverjanje za posodobitvami ... Opomba predolga Opomba prekratka Način strnjenega prikaza (samodejno/strnjeno/polno). Programa ni bilo mogoče zapreti, ker obstajajo dejavna opravila. Ni bilo mogoče najti kategorije »%1« Ni bilo mogoče odpreti %1 Izbriši izvor Neposredno odpri podan program preko imena njegovega paketa. Zavrzi Discover Prikaži seznam vnosov s kategorijo. Razširitve ... Ni bilo mogoče odstraniti vira »%1« Izpostavljeno Pomoč ... Domača stran: Izboljšaj povzetek Namesti Nameščeno Jonathan Thomas Zaženi Licenca: Izpiše vsa razpoložljiva zaledja. Izpiše vse razpoložljive načine. Nalaganje ... Krajevni paket za namestitev Nastavi kot privzeto Več podrobnosti ... Več ... Ni posodobitev Odpre Discover v podanem načinu. Načini ustrezajo gumbom orodne vrstice. Odpri s programom, ki zna rokovati s podano vrsto MIME. Nadaljuj Ocena: Odstrani Viri za »%1« Ocena Ocenjevanje »%1« Zaganjanje kot <em>skrbnik</em> je odsvetovano in nepotrebno. Iskanje Išči v '%1' ... Poišči ... Poišči: %1 Poišči: %1 + %2 Nastavitve Kratek povzetek ... Pokaži ocene (%1) ... Velikost: Najdeno ni bilo nič ... Vir: Navedite nov vir za %1 Iskanje poteka ... Povzetek: Podpira url shemo appstream Opravila Opravila (%1 %) %1 %2 Vira ni mogoče najti: %1 Posodobi vse Posodobitev izbrana Posodobi (%1) Posodobitve Različica: neznan pregledovalec neizbranih posodobitev izbranih posodobitev © 2010-2016 Razvojna ekipa Plasme 