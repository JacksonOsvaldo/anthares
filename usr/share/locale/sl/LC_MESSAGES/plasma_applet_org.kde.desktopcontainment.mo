��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l  	   >     H     X     j     q  
   z     �     �     �  
   �  
   �     �     �     �     �     �            	              (     8     G     M  	   V     `     e     u     �     �     �     �     �     �     �            	   "     ,     4     T     \     j     p     w     |     �     �     �  
   �     �      �     �     �     �     �  L   �     >     S     h     q     �     �     �     �     �  
   �     �     �     �  !   �     !  0   0     a     ~     �     �     �     �     �     �     �     �     �     
                -     3     R     b     x     �  S   �     �     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2018-01-12 17:16+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 I&zbriši Izprazni Sm&eti Pre&makni v smeti &Odpri &Prilepi &Lastnosti &Osveži namizje &Osveži prikaz &Znova naloži P&reimenuj Izberi ... Počisti ikono Poravnaj Videz: Razporedi v Razporedi v Razporeditev: Nazaj Prekliči Stolpce Nastavi namizje Naslov po meri Datum Privzeto Padajoče Opis Odstrani izbiro Razpored namizja Sem vnesite naslov po meri Zmožnosti: Vzorec imena datoteke: Vrsta datoteke Vrste datotek: Filter Pojavna okna s predogledom map Najprej mape Najprej mape Polna pot Razumem Skrij datoteke, ki se ujemajo z Ogromna Velikost ikon Ikone Velika Levo Seznam Mesto Mesto: Zakleni položaj Zaklenjeno Srednja Več možnosti za predoglede ... Ime Brez V redu Gumb pulta: Kliknite in držite gradnike, da jih premaknete in odkrijete njihove ročice Vstavki za predogled Predogledne sličice Odstrani Spremeni velikost Obnovi Desno Zavrti Vrstice Poišči vrsto datoteke ... Izberi vse Izberite mapo Označevalniki izbora Pokaži vse datoteke Pokaži datoteke, ki se ujemajo z Pokaži mesto: Pokaži datoteke povezane s trenutno dejavnostjo Pokaži vsebino mape Namizje Pokaži namizno orodjarno Velikost Majhna Srednje majhna Razvrsti po Razvrsti po Razvrščanje: Navedite mapo: Vrstice besedila Zelo majhna Naslov: Orodni namigi Prilagoditve Vrsta Sem vnesite pot ali naslov URL Ni razvrščeno Uporabi ikono po meri Ravnanje z gradniki Gradniki odklenjeni Gradnike lahko kliknete in držite, da jih premaknete in odkrijete njihove ročice. Način prikaza 