��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  4     +   B     n     w          �     �     �     �     �  -   �       '     )   .     X     i     }     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-06 14:18+0200
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 1.5
 Ali želite zaustaviti v pomnilnik (pripravljenost)? Ali želite zaustaviti na disk (mirovanje)? Splošno Dejanja V mirovanje V mirovanje (zaustavi na disk) Zapusti Zapusti ... Zakleni Zakleni zaslon Odjavi, izklopi ali znova zaženi računalnik Ne V pripravljenost (zaustavi v pomnilnik) Zaženi vzporedno sejo kot drug uporabnik V pripravljenost Preklopi uporabnika Preklopi uporabnika Da 