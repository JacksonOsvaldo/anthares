��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �  
   �     �     �     �     �  !        /     <     D     M  
   g  
   r     }     �  
   �  	   �  	   �     �     �     �  2   �             
   :     E     Y     j          �     �     �     �     �     �     �     �     �     �            
        '     0     M     k          �     �     �     �     �  	   �     �     �               0     D     X     m     �     �  "   �     �     �     �     �     	               =     U  $   k  !   �  %   �     �     �          !     *     2  )   F     p     �     �     �     �     �     �     �     �  $        @         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2018-01-11 21:18+0100
Last-Translator: Andrej Mernik <andrejm@ubuntu.si>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Generator: Lokalize 2.0
 Upravljaj »%1« ... Izberi ... Počisti ikono Dodaj na namizje Dodaj med priljubljene Dodaj na pult (gradnik) Poravnaj rezultate iskanja na dno Vsi programi %1 (%2) Programi Programi in dokumentacija Obnašanje Kategorije Računalnik Stiki Opis (Ime) Samo opis Dokumenti Uredi program ... Uredi programe ... Končaj sejo Razširi iskanje na zaznamke, datoteke in e-pošto Priljubljene Splošči meni na en nivo Pozabi vse Pozabi vse programe Pozabi vse stike Pozabi vse dokumente Pozabi program Pozabi stik Pozabi dokument Pozabi nedavne dokumente Splošno %1 (%2) V mirovanje Skrij %1 Skrij program Ikona: Zakleni Zakleni zaslon Odjava Ime (Opis) Samo ime Pogosto uporabljeni programi Pogosto uporabljeni dokumenti Pogosto uporabljeno na vseh dejavnostih na trenutni dejavnosti Odpri z: Pripni v upravljalnik opravil Mesta Energija / seja Lastnosti Znova zaženi Nedavni programi Nedavni stiki Nedavni dokumenti Nedavno uporabljeno Nedavno uporabljeno Odstranljivi nosilci Odstrani iz priljubljenih Znova zaženi računalnik Zaženi ukaz ... Zaženi ukaz ali iskalno poizvedbo Shrani sejo Poišči Rezultati iskanja Poišči ... Iskanje »%1« Seja Pokaži podrobnosti o stiku ... Pokaži v priljubljenih Pokaži programe kot: Pokaži pogosto uporabljene programe Pokaži pogosto uporabljene stike Pokaži pogosto uporabljene dokumente Pokaži nedavne programe Pokaži nedavne stike Pokaži nedavne dokumente Pokaži: Izklopi Razvrsti po abecedi Zaženi vzporedno sejo kot drug uporabnik V pripravljenost Zaustavi v pomnilnik Zaustavi na disk Preklopi uporabnika Sistem Sistemska dejanja Izklopi računalnik Tipkajte za iskanje. Pokaži vse programe v »%1« Pokaži vse programe v tem podmeniju Gradniki 