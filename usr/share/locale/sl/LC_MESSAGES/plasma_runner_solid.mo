��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  �  �     ~     �  +   �  W   �  V   !  X   x  V   �  V   (	  b   	     �	     �	     �	     
     

     
     
     #
     3
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-15 02:09+0100
Last-Translator: Jure Repinc <jlp@holodeck1.com>
Language-Team: Slovenian <lugos-slo@lugos.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 Zakleni vsebnik Izvrzi nosilec Najde naprave, katerih ime se ujema s/z :q: Izpiše seznam vseh naprav in vam omogoča, da jih priklopite, odklopite ali izvržete. Izpiše seznam vseh naprav, ki jih lahko izvržete, in vam omogoča, da jih izvržete. Izpiše seznam vseh naprav, ki jih lahko priklopite, in vam omogoča, da jih priklopite. Izpiše seznam vseh naprav, ki jih lahko odklopite, in vam omogoča, da jih odklopite. Izpiše seznam vseh naprav, ki jih lahko zaklenete, in vam omogoča, da jih zaklenete. Izpiše seznam vseh šifriranih naprav, ki jih lahko odklenete, in vam omogoča, da jih odklenete. Priklopi napravo naprava izvrzi zakleni priklopi odkleni odklopi Odkleni vsebnik Odklopi napravo 