��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  �  �     [  #   k  $   �  2   �  5   �  &     #   D     h  "   �  0   �  V   �  +   3	     _	     |	     �	     �	     �	     �	  !   �	     �	  
   
     !
     4
                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-12-28 12:28-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Cuir i bhFeidhm Cuireadh comhaid leis an stór SVN. Comhaid á gcur leis an stór SVN... Níorbh fhéidir comhaid a chur leis an stór SVN. Níorbh fhéidir na hathruithe SVN a chur i bhfeidhm. Cuireadh na hathruithe SVN i bhfeidhm. Athruithe SVN á gcur i bhfeidhm... Baineadh comhaid ón stór SVN. Comhaid á mbaint ón stór SVN... Níorbh fhéidir comhaid a bhaint ón stór SVN. Theip ar nuashonrú stádais SVN. Rogha "Taispeáin Nuashonruithe SVN" á díchumasú. Níorbh fhéidir an stór SVN a nuashonrú. Nuashonraíodh an stór SVN. Stór SVN á nuashonrú... SVN Cur Leis SVN Cur i bhFeidhm... SVN Scriosadh SVN Nuashonrú Taispeáin Athruithe Logánta SVN Taispeáin Nuashonruithe SVN Cur Síos: SVN Cur i bhFeidhm Taispeáin nuashonruithe 