��          \      �       �      �   -   �        -   +  #   Y  #   }     �  �  �  #   �  ;   �  *   �  4        D     J     M                                       Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2011-12-28 12:28-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Taispeánann sé seo an dáta inniu Taispeánann sé seo an dáta inniu i gcrios ama roghnaithe Taispeánann sé seo an t-am faoi láthair Taispeánann sé seo an t-am i gcrios ama roghnaithe dáta am An Lá Inniu: %1 