��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  `   �  m     +   {  �   �     .     I  <   Z     �  :   �     �     �     �       *   #  (   N  '   w  %   �     �  r   �     D     X  x   j     �  5   �     +     9     G     d     r     �     �     �     �     �     �                    5     D  	   P  
   Z  
   e  
   p     {     �     �     �     �     �  #   �     �  	   
          3  �   D     �  b   �  w   a     �  9   �  4   $  3   Y  #   �  "   �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kdelibs/kcm_phonon.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2007-06-27 10:27-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Chun athrú an innill a chur i bhfeidhm, ní mór duit logáil amach agus logáil isteach arís. Liosta de na hInnill Phonon a aimsíodh ar do chóras.  Úsáidfidh Phonon iad san ord a fheiceann tú anseo. Cuir Liosta na nGléasanna i bhFeidhm Ar... Cuir an liosta tosaíochta gléasanna atá á thaispeáint faoi láthair i bhfeidhm ar na catagóirí athsheinnte fuaime seo a leanas: Socrú Crua-Earraí Fuaime Athsheinm Fuaime Gléas Réamhshocraithe Athsheinnte Fuaime i gCatagóir '%1' Taifeadadh Fuaime Gléas Réamhshocraithe Taifeadta Fuaime i gCatagóir '%1' Inneall Colin Guthrie Nascóir Cóipcheart 2006 Matthias Kretz Gléas Réamhshocraithe Athsheinnte Fuaime Gléas Réamhshocraithe Taifeadta Fuaime Gléas Réamhshocraithe Taifeadta Físe Catagóir Réamhshocraithe/Gan Socrú Níos measa Socraíonn sé seo ord réamhshocraithe na ngléasanna (cé gur féidir é seo a shárú i gcatagóirí ar leith). Cumraíocht Ghléis Sainrogha Ghléis Gléasanna a aimsíodh ar do chóras, oiriúnach don chatagóir roghnaithe.  Roghnaigh an gléas is mian leat a úsáid. kscanne@gmail.com Níorbh fhéidir an gléas aschurtha fuaime a shocrú Aghaidh: Lár Aghaidh: Clé Aghaidh: Taobh Clé den Lár Aghaidh: Deas Aghaidh: Taobh Deas den Lár Crua-Earraí Gléasanna Neamhspleácha Leibhéil Ionchurtha Neamhbhailí Socrú Crua-Earraí Fuaime KDE Matthias Kretz Mona Kevin Scannell Modúl Cumraíochta Phonon Athsheinm (%1) Níos fearr Próifíl Cúl: Lár Cúl: Clé Cúl: Deas Taifeadadh (%1) Taispeáin ardghléasanna Taobh: Clé Taobh: Deas Cárta Fuaime Gléas Fuaime Láithriú Callairí agus Tástáil Fo-dhordaire Tástáil Tástáil an gléas roghnaithe %1 á thástáil Úsáidfear na gléasanna san ord a fheiceann tú anseo. Mura bhfuil an chéad ghléas ar fáil ar fáth éigin, bainfidh Phonon triail as an dara gléas, mar sin de. Cainéal Anaithnid Úsáid an liosta gléasanna atá á thaispeáint faoi láthair le haghaidh tuilleadh catagóirí. Catagóirí éagsúla de mheáin. I ngach catagóir, is féidir gléas a roghnú a úsáidfidh na feidhmchláir Phonon. Taifeadadh Físe Gléas Réamhshocraithe Taifeadta Físe i gCatagóir '%1' Seans nach féidir le do chóras fuaim a thaifeadadh Seans nach féidir le do chóras fís a thaifeadadh ní fearr leat an gléas roghnaithe is fearr leat an gléas roghnaithe 