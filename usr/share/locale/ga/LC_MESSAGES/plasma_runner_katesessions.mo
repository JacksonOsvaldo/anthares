��          <      \       p   !   q   3   �      �   �  �   3   �  C   �     "                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: krunner_katesessions.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2008-08-16 13:55-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Aimsíonn sé seisiúin Kate comhoiriúnach do :q:. Taispeánann sé seo gach seisiún d'eagarthóir Kate i do chuntas. Oscail Seisiún Kate 