��          �            x     y     }     �     �     �  S   �  w   !  M   �     �  	   �     �          %     2     R  �  d     0     4      Q     r     �  d   �  {     \   �     �     �  !        -     <     P     _                                              	          
                        ms &Reactivation delay: &Switch desktop on edge: Activation &delay: Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Change desktop when the mouse cursor is pushed against the edge of the screen Lock Screen No Action Only When Moving Windows Other Settings Show Desktop Switch desktop on edgeDisabled Window Management Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2009-02-28 14:19-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
  ms Moill &athghníomhachtaithe: &Athraigh an deasc ar chiumhais: Moill &ghníomhachtaithe: Cumasaithe i gcónaí Méid ama tar éis gníomh a chur i bhfeidhm sular féidir an chéad ghníomh eile a chur i bhfeidhm Méid ama a chaithfear cúrsóir na luiche a bhrú in aghaidh chiumhais an scáileáin sula gcuirfear an gníomh i bhfeidhm Athraigh an deasc nuair a bhrúitear cúrsóir na luiche in aghaidh chiumhais an scáileáin Cuir an Scáileán Faoi Ghlas Gan Ghníomh Agus fuinneoga á mbogadh amháin Socruithe Eile Taispeáin an Deasc Díchumasaithe Bainisteoireacht Fuinneog 