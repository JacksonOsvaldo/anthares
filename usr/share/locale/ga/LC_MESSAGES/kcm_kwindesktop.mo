��    #      4  /   L           	       
   �  +   �  
   �     �     �     �           %     7     W  	   `      j  �   �  e   )  *   �  H   �          
     )  )   6  ;   `  	   �     �  (   �     �               ;     [     p     �  	   �  �  �     �	  �   �	     

  0   
     D
  	   M
     W
     t
  (   �
     �
  $   �
     �
     �
     �
  �     m   �  3   O  :   �     �     �     �  4   �  R     
   o  /   z  1   �     �     �            2     S  #   f  "   �     �        !                	                                                                                   
                #                    "               msec <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. Layout NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Project-Id-Version: kdebase/kcm_kwindesktop.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2007-11-05 08:28-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
  ms <h1>Deasca Iomadúla</h1>Sa mhodúl seo is féidir leat líon na ndeasc fíorúil a chumrú agus conas ba chóir iad seo a lipéadú. Beochan: Sannadh Aicearra comhchoiteann "%1" do Dheasc %2 Deasc %1 Deasc %1: Beochan Maisíochtaí Deisce Ainmneacha na nDeasc Athrú Deasca: Taispeáint Ar Scáileán Malairt na Deisce Timfhilleann nascleanúint na ndeasc Deasca Aga: kscanne@gmail.com Cumasaigh an rogha seo más mian leat dul go dtí ciumhais eile na deisce nuaí nuair a rachaidh tú thar chiumhais na deisce reatha agus nascleanúint trí imlínte gníomhacha nó tríd an mhéarchlár in éifeacht. Leis an rogha seo, taispeánfar réamhamharc beag ar leagan amach na deisce a léiríonn an deasc roghnaithe. Anseo is féidir leat ainm a thabhairt ar dheasc %1 Anseo is féidir leat líon na ndeasc fíorúil a shocrú. Leagan Amach Kevin Scannell Gan Bheochan Níor aimsíodh aon Aicearra oiriúnach do Dheasc %1 Coinbhleacht idir aicearraí: Níorbh fhéidir Aicearra %1 a shannadh do Dheasc %2 Aicearraí Taispeáin táscairí do leagan amach na deisce Taispeáin aicearraí do gach deasc atá ar fáil Téigh Síos Deasc Amháin Téigh Suas Deasc Amháin Téigh go dtí an Deasc ar chlé Téigh go dtí an Deasc ar dheis Téigh go Deasc %1 Téigh go dtí an Chéad Deasc Eile Téigh go dtí an Deasc roimhe seo Malartú 