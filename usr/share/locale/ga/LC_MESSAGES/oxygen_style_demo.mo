��    w      �  �   �      
  	   
     #
     *
     8
     D
     Q
     Y
     a
  
   h
     s
     
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
  
   	  	          
   ,     7  ,   <  /   i     �     �     �     �  
   �     �  
   �     �     �     �  
                        .     6     N     R     Z     a     g     k     n     s     {     �     �     �     �     �     �     �     �     �     �     �               #     /     <     H     W     c     v     �     �  /   �  =   �  #   /  *   S  .   ~     �     �     �     �     �     �  
   �     �            	   "     ,     =  
   L     W     d  
   v  
   �  	   �     �  
   �     �     �     �     �     �     �     �     �  	   �     �               %     .  
   3     >  �  G  
        $     (     4     A     N     V     b     g  	   s     }     �     �     �  "   �     �  
   �     �     �                0     B     W     e     x     �     �     �     �     �     �     �     �     �     �               "     0     ?     L     U     h     p     �     �     �  
   �     �     �     �     �     �     �     �     �     	          &     /     8     G     T  	   n     x     �     �     �     �     �     �  "   �  !   	     +     F  L   b  d   �  9     ;   N  D   �  !   �     �          
  	     
   "     -     C     Z      f     �     �     �     �     �     �     �          !     /     A     P     X     `     i     x     �     �     �     �     �     �     �  	   �     �                       %   H      `       s         k       g   i   '              K          F   L   /   .           W   v   u   w                  6   _      B   <   n          0   =         $             Z   E   d   ;               P       f       ^   #      7   \   Y   O   4   1   U       c          
   Q   h   [   l       m   X   I           J   V   a   :           T   ]           S   R          N                  t      5   *                  G   3      "   j       !       8                        M   (   )   o       ,                e          D   &   r   +   2   q       b       	         9   C   >   ?   p   A   @   -    Benchmark Bottom Bottom to Top Bottom-left Bottom-right Buttons Cascade Center Checkboxes Description Dialog Document mode Down Arrow East Editable combobox Editors Enabled Example text First Column First Description First Item First Label First Page First Row First Subitem First item Flat Flat frame. No frame is actually drawn.Flat Flat group box. No frame is actually drawnFlat Frame Frames GroupBox Hide tabbar Horizontal Huge (48x48) Icons Only Input Widgets Large Large (32x32) Left Arrow Left to Right Lists Medium (22x22) Modules Multi-line text editor: New New Row Normal North Off On Open Options Oxygen Demo Partial Password editor: Preview Pushbuttons Radiobuttons Raised Right Right Arrow Right to Left Right to left layout Save Second Column Second Description Second Item Second Label Second Page Second Subitem Second item Select Next Window Select Previous Window Show Corner Buttons Shows the appearance of buttons Shows the appearance of lists, trees and tables Shows the appearance of sliders, progress bars and scrollbars Shows the appearance of tab widgets Shows the appearance of text input widgets Shows the appearance of various framed widgets Single line text editor: Sliders Small Small (16x16) South Spinbox: Tab Widget Tab Widgets Tabs Text Alongside Icons Text Only Text Under Icons Text and icon: Text only: Third Column Third Description Third Item Third Page Third Row Third Subitem Third item Tile Title Title: Toolbox Toolbuttons Top Top to Bottom Top-left Top-right Up Arrow Use flat buttons Use flat widgets Vertical West Wrap words password Project-Id-Version: kdebase/kstyle_config.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:34+0200
PO-Revision-Date: 2007-08-22 12:11-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Tagarmharc Bun Bun go Barr Bun ar chlé Bun ar dheis Cnaipí Cascáidigh Lár Ticbhoscaí Cur Síos Dialóg Mód cáipéise Saighead Síos Oirthear Bosca teaglama ineagarthóireachta Eagarthóirí Cumasaithe Téacs samplach An Chéad Cholún An Chéad Chur Síos An Chéad Mhír An Chéad Lipéad An Chéad Leathanach An Chéad Ró An Chéad Fhomhír An chéad mhír Maol Maol Maol Fráma Frámaí BoscaGrúpa Folaigh barra na gcluaisíní Cothrománach Ollmhór (48×48) Deilbhíní Amháin Giuirléidí Ionchurtha Mór Mór (32×32) Saighead Chlé Clé-go-Deas Liostaí Measartha (22×22) Modúil Eagarthóir téacs il-líne: Nua Ró Nua Gnách Tuaisceart As Ann Oscail Roghanna Taispeántas Oxygen Neamhiomlán Eagarthóir focal faire: Réamhamharc Brúchnaipí Cnaipí raidió Ardaithe Ar Dheis Saighead Dheas Deas-go-Clé Leagan amach deas-go-clé Sábháil An Dara Colún An Dara Cur Síos An Dara Mír An Dara Lipéad An Dara Leathanach An Dara Fomhír An dara mír Roghnaigh an Chéad Fhuinneog Eile Roghnaigh an Fhuinneog Roimhe Seo Taispeáin Cnaipí Cúinne Taispeáin cuma na gcnaipí Taispeánann sé an dealramh atá ar liostaí, ar chrainn, agus ar tháblaí Taispeánann sé an dealramh atá ar shleamhnáin, ar bharraí dul chun cinn, agus ar scrollbharraí Taispeánann sé dealramh atá ar ghiuirléidí cluaisín Taispeánann sé dealreamh atá ar ghiuirléidí ionchurtha Taispeánann sé dealramh atá ar ghiuirléidí éagsúla frámaithe Eagarthóir téacs líne amháin: Barraí Sleamhnáin Beag Beag (16×16) Deisceart Casbhosca: Giuirléid Chluaisín Giuirléidí Cluaisín Cluaisíní Téacs Taobh Leis na Deilbhíní Téacs Amháin Téacs Faoi na Deilbhíní Téacs agus deilbhín: Téacs amháin: An Tríú Colún An Tríú Cur Síos An Tríú Mír An Tríú Leathanach An Tríú Ró An Tríú Fomhír An tríú mír Tíligh Teideal Teideal: Bosca Uirlisí Cnaipí uirlise Barr Barr go Bun Barr ar chlé Barr ar dheis Saighead Suas Úsáid cnaipí maola Úsáid giuirléidí maola Ingearach Iarthar Timfhill focail focal faire 