��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     +  %   .  �  T  �  	  [   �
     �
            $   0     U  "   m  %   �     �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2004-12-03 14:52-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0beta1
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
  s &Teorainn ama an chúrsóra tosaithe: <H1>Fógairt ar an Tascbharra</H1>
Is féidir fógairt tosaithe eile a chumasú a úsáideann an tascbharra.
Sa mhodh seo, taispeántar cnaipe sa tascbharra le horláiste rothlach air,
le cur in iúl duit go bhfuil an feidhmchlár ag luchtú.
Uaireanta, níl an feidhmchlár ar an eolas faoin fhógairt seo.
Sa chás seo, téann an cnaipe as amharc tar éis na tréimhse socraithe
sa rannán 'Teorainn ama an chúrsóra tosaithe' <h1>Cúrsóir Gnóthach</h1>
Taispeánann KDE cúrsóir gnóthach chun cur in iúl go bhfuil feidhmchlár á thosú.
Chun an cúrsóir gnóthach a chumasú, roghnaigh cineál aiseolais infheicthe
ón bhosca teaglama.
Uaireanta, níl an feidhmchlár ar an eolas faoin fhógairt seo.
Sa chás seo, stopann caochadh an chúrsóra tar éis na tréimhse socraithe
sa rannán 'Teorainn ama an chúrsóra tosaithe' <h1>Aiseolas Tosaithe</h1> Anseo is féidir leat aiseolas tosaithe feidhmchláir a chumrú. Cúrsóir Caochta Cúrsóir Preabach Cúrsóir &Gnóthach Cumasaigh fógairt ar an &tascbharra Gan Cúrsóir Gnóthach Cúrsóir Gnóthach Éighníomhach Teorainn ama an chúrsóra t&osaithe: Fógairt ar a&n Tascbharra 