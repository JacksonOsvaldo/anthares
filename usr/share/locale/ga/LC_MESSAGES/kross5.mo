��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �     �     �     �  	          8        R  #   X     |     �  E   �  L   �  1   3  .   e     �  
   �     �  .   �  4   �     	  $   $	  /   I	     y	     ~	     �	     �	     �	     �	     �	                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2004-12-14 09:11-0600
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Ginearálta Cuir script nua leis. Cuir Leis... Cealaigh? Nóta: seoc@iolfree.ie,kscanne@gmail.com,s_oceallaigh@yahoo.com Eagar Cuir an script roghnaithe in eagar. Eagar... Rith an script roghnaithe. Níorbh fhéidir script a chruthú le haghaidh léirmhínitheoir "%1" Níorbh fhéidir léirmhínitheoir a dhéanamh amach le haghaidh script "%1" Níorbh fhéidir léirmhínitheoir "%1" a luchtú Níorbh fhéidir comhad scripte "%1" a oscailt Comhad: Deilbhín: Léirmhínitheoir: Leibhéal slándála an léirmhínitheora Ruby Séamus Ó Ciardhuáin,Kevin Scannell,Sean V. Kelley Ainm: Níl a leithéid d'fheidhm ann: "%1" Níl a leithéid de léirmhínitheoir ann: "%1" Bain Bain an script roghnaithe. Rith Níl comhad scripte "%1" ann. Stad Stop an script roghnaithe. Téacs: 