��    
      l      �       �      �                2     :     W  Q   l     �     �  �  �     �     �     �     �  $   �          0     6     H     	                      
                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Could not find a dictionary. Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: krunner_spellcheckrunner.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2008-08-14 09:40-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Éiligh &truicearfhocal &Truicearfhocal: Deimhnigh litriú i :q:. Ceart Níorbh fhéidir foclóir a aimsiú. Socruithe an Litreora %1:q: Focail mholta: %1 litriú 