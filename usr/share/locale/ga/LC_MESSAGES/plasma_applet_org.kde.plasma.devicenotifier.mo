��          �      �       0  3   1  $   e  �   �       4        O  #   i  +   �     �     �     �  �      �  �  �   f     ,     4     C  ?   O  (   �  C   �  I   �     F  "   d     �     �                                
                   	       1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. It is currently safe to remove this device. No Devices Available Non-removable devices only Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... Project-Id-Version: kdebase/plasma_applet_devicenotifier.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2007-10-23 08:08-0500
Last-Translator: Kevin Scannell <kscanne@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? 3 : 4
 Gníomh amháin le haghaidh an ghléis seo %1 ghníomh le haghaidh an ghléis seo %1 ghníomh le haghaidh an ghléis seo %1 ngníomh le haghaidh an ghléis seo %1 gníomh le haghaidh an ghléis seo %1 saor Á rochtain... Gach gléas Cliceáil chun an gléas seo a rochtain ó fheidhmchláir eile. Cliceáil chun an diosca seo a dhíchur. Cliceáil chun an gléas seo a dhícheangal gan dochar a dhéanamh. Tig leat an gléas seo a bhaint faoi láthair gan dochar a dhéanamh dó. Níl Aon Ghléasanna Ar Fáil Gléasanna neamh-inbhainte amháin Gléasanna inbhainte amháin Á Bhaint... 