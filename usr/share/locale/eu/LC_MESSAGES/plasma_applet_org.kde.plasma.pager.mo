��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  +   �     �  -   �          "     4     P     k     {  
   �     �     �     �     �     �     �     �     �       !   0     R  
   j  	   u        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_org.kde.plasma.pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2017-11-05 16:30+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Ikonotutako leiho %1: Ikonotutako %1 leiho: Leiho %1: %1 leiho: ... eta beste leiho %1 ... eta beste %1 leiho Jarduera izena Jarduera zenbakia Gehitu alegiazko mahaigaina Konfiguratu mahaigainak... Mahaigain izena Mahaigain zenbakia Bistaratu: Ez du ezer egiten Orokorra Horizontala Ikonoak Antolamendua: Testurik ez Soilik uneko pantaila Kendu alegiazko mahaigaina Uneko mahaigaina aukeratzea: Erakutsi jarduera kudeatzailea... Mahaigaina erakusten du Lehenetsia Bertikala 