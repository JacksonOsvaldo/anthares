��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     �  
   �     �     �     �     �  "        1     C     K     W     s  
   |     �  
   �     �     �     �     �     �       L        [     d     �     �     �     �     �     �     �  &        5     >  	   F     P     \     p     x     �  
   �     �     �     �     �     �               +     :     V     ]     m     z     �     �     �     �     �     	          7     Q  0   g     �     �     �  	   �     �     �  $   �     �       %   /  $   U  %   z  (   �  '   �  (   �  	        $     +  2   A     t     z     �     �     �     �     �     �  '   �  2        O         Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: plasma_applet_org.kde.plasma.kicker.po
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-22 05:49+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Kudeatu '%1'... Hautatu... Berrezarri ikonoa Erantsi mahaigainera Erantsi gogokoetara Erantsi panelera (trepeta) Lerrokatu bilaketa emaitzak behean Aplikazio guztiak %1 (%2) Aplikazioak Aplikazioak eta Dokumentuak Portaera Kategoriak Ordenagailua Kontaktuak Deskribapena (Izena) Deskribapena soilik Dokumentuak Editatu aplikazioa... Editatu aplikazioak... Amaitu saioa Zabaldu bilaketa laster-marketara, fitxategietara eta posta-elektronikoetara Gogokoak Lautu menua maila bakarrera Ahaztu guztiak Ahaztu aplikazio guztiak Ahaztu kontaktu guztiak Ahaztu dokumentu guztiak Ahaztu aplikazioa Ahaztu kontaktua Ahaztu dokumentua Ahaztu berriki erabilitako dokumentuak Orokorra %1 (%2) Hibernatu Ezkutatu %1 Ezkutatu aplikazioa Ikonoa: Giltzatu Pantaila giltzatu Itxi saioa Izena (deskribapena) Izena soilik Maiz erabilitako aplikazioak Maiz erabilitako dokumentuak Maiz erabilia Jarduera guztietan Uneko jardueran Ireki honekin: Finkatu Ataza Kudeatzailean Lekuak Energia / Saioa Propietateak Berrabiarazi Berriki erabilitako aplikazioak Berriki erabilitako kontaktuak Berriki erabilitako dokumentuak Berriki erabilita Berriki erabilia Biltegi eramangarria Ezabatu gogokoen artetik Berrabiarazi ordenagailua Exekutatu komandoa... Exekutatu komando bat edo bilaketa kontsulta bat Gorde saioa Bilatu Bilatu emaitzak Bilatu... '%1' bilatzen Saioa Erakutsi kontaktuaren informazioa... Erakutsi gogokoetan Erakutsi aplikazioak honela: Erakutsi maiz erabilitako aplikazioak Erakutsi maiz erabilitako kontaktuak Erakutsi maiz erabilitako dokumentuak Erakutsi berriki erabilitako aplikazioak Erakutsi berriki erabilitako kontaktuak Erakutsi berriki erabilitako dokumentuak Erakutsi: Itzali Sailkatu alfabetikoki Hasi saio paralelo bat erabiltzaile desberdin gisa Eseki Eseki RAMera Eseki diskora Aldatu erabiltzailea Sistema Sistemaren ekintzak Itzali ordenagailua Idatzi bilatu beharrekoa. '%1': ezkutatutako aplikazioak erakutsi Azpimenu honetan ezkutatutako aplikazioak erakutsi Trepetak 