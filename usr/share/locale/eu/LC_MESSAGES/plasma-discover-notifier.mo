��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  �       B  :   I  0   �  ,   �  %   �  O     #   X     |     �            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: plasma-discover-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2017-08-06 22:58+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %1, %2 Eguneratu beharreko pakete 1 Eguneratu beharreko %1 pakete Segurtasun eguneratze 1 %1 segurtasun eguneratze Eguneratzeko pakete 1 Eguneratzeko %1 pakete Ez dago eguneratu beharreko paketerik horietako 1 segurtasun eguneratzea da horietako %1 segurtasun eguneratzeak dira Segurtasun eguneratzeak erabilgarri Sistema eguneratuta dago Eguneratzeak erabilgarri 