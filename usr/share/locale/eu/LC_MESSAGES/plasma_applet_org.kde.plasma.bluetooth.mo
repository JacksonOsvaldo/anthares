��    )      d  ;   �      �     �     �     �     �     �     �     �  	   �     �          %     :     G     _     v     ~  
   �  
   �     �     �     �     �     �     �     �     �               .  U   C  Z   �  O   �  D   D     �     �     �  	   �  	   �     �     �  �  �     �     �     �     �     �     �     �  	   	     	     (	     B	     ^	     r	     �	  	   �	     �	     �	     �	     �	     �	     �	     
     
     /
     5
  !   8
     Z
  !   q
     �
      �
  &   �
     �
  )        9  
   L     W     f     x  
   �     �                                             %                   	         (          &   !          '                          )   #          "                          $          
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: plasma_applet_org.kde.plasma.bluetooth
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2017-08-27 13:41+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Moldagailua Erantsi gailu berria Erantsi gailu berria... Helbidea Audioa Audio gailua Gailu eskuragarriak Bluetooth Bluetooth desgaituta dago Bluetooth desgaituta dago Bluetooth lerroz kanpo dago Arakatu fitxategiak Konfiguratu &Bluetooth... Konfiguratu Bluetooth... Konektatu Konektatutako gailuak Konektatzen Deskonektatu Deskonektatzen Gaitu Bluetooth Fitxategi transferentzia Sarrera Sarrerako gailua Sarea Ez Ez dago moldagailu erabilgarririk Ez da gailurik aurkitu Ez dago moldagailu erabilgarririk Ez dago konektatutako gailurik Gailurako konexioak huts egin du Ezin da gailuarekin harremanetan jarri Gailua ez dago prest gailu %1 konektatuta %1 gailu konektatuta Beste gailu batzuk Parekatuta Urruneko izena Bidali fitxategia Bidali fitxategia Fidagarria Bai 