��          4      L       `   m   a   R   �   �  "  |   �  E   h                    Calculates the value of :q: when :q: is made up of numbers and mathematical symbols such as +, -, /, * and ^. The exchange rates could not be updated. The following error has been reported: %1 Project-Id-Version: plasma_runner_calculatorrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-28 05:09+0200
PO-Revision-Date: 2014-01-26 12:24+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 :q:(r)en balioa kalkulatzen du :q: zenbaki eta ikur matematikoekin, hala nola +, -, /, * eta ^ ikurrekin, osatuta dagoenean. Ezin izan dira kanbio-tasak eguneratu. Errore honen berri eman da: %1 