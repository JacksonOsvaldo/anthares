��    6      �  I   |      �  !   �  !   �     �  
   �             (   0  I   Y  :   �  <   �  2     .   N  1   }  (   �  1   �  o   
     z  !   �     �  0   �     �          (     H     g     l  ,   {     �     �     �     �  !   �     �     	     	     0	  	   ?	  
   I	  
   T	     _	     o	  
   �	  &   �	     �	     �	     �	     �	     	
     
     
     
     3
     G
  �  Z
  '     '   :     b  
   q     |  !   �  +   �  S   �  C   :  B   ~  I   �  D     B   P  0   �  K   �  �        �  (   �  (   �  F   �  -   B     p  !   �  #   �     �     �  P   �     H     Y     t     {  *   �     �     �     �     �            	         *     >     Z  6   m     �     �     �     �       
             "     *  
   1         )   %      /          
   5      2   	                              6                 (                    ,      "   #      &             .                   !             3              $       0   1       *   4      +              '   -                  Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Animation type: Animations Bottom arrow button type: Combo box transitions Configure fading transition between tabs Configure fading transition when a combo box's selected choice is changed Configure fading transition when a label's text is changed Configure fading transition when an editor's text is changed Configure menu bars' mouseover highlight animation Configure menus' mouseover highlight animation Configure progress bars' busy indicator animation Configure progress bars' steps animation Configure toolbars' mouseover highlight animation Configure widgets' focus and mouseover highlight animation, as well as widget enabled/disabled state transition Dialog Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw toolbar item separators Draw tree branch lines Draw window background gradient Enable extended resize handles Fade Fade duration: Focus, mouseover and widget state transition Follow Mouse Follow mouse duration: Frame General Keyboard accelerators visibility: Label transitions Menu Highlight Menu bar highlight Menu highlight No button No buttons One button Oxygen Settings Progress bar animation Scrollbars Show Keyboard Accelerators When Needed Tab transitions Text editor transitions Toolbar highlight Top arrow button type: Two buttons Views px triangle sizeNormal triangle sizeSmall triangle sizeTiny Project-Id-Version: kstyle_config
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2014-02-04 09:27+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Ezkutatu beti teklatuko azeleratzaileak Erakutsi beti teklatuko azeleratzaileak Animazio mota: Animazioak Beheko geziaren botoi mota: Konbinazio-koadroaren trantsizioa Konfiguratu fitxen arteko urtze-trantsizioa Konfiguratu urtze-trantsizioa konbinazio-koadro batean hautatutakoa aldatzen denean Konfiguratu urtze-trantsizioa, etiketa baten testua aldatzen denean Konfiguratu urtze-trantsizioa editore baten testua aldatzen denean Konfiguratu menu-barren gainetik sagua igarotzean nabarmentzeko animazioa Konfiguratu menuen gainetik sagua igarotzean nabarmentzeko animazioa Konfiguratu aurrerapen-barraren lanpetuta adierazlearen animazioak Konfiguratu aurrerapen-barren urratsen animazioa Konfiguratu tresna-barren gainetik sagua igarotzean nabarmentzeko animazioa Konfiguratu trepeten fokua eta sagua gainetik igarotzean nabarmentzeko animazioa, bai eta trepeten gaituta/ezgaituta egoera-trantsizioa ere Elkarrizketa Arrastatu leihoak eremu huts guztietatik Arrastatu leihoak titulu-barratik soilik Arrastatu leihoak titulu-barratik, menu-barratik eta tresna-barretatik Marraztu tresna-barrako elementuen bereizleak Marraztu zuhaitz-adarren marrak Marraztu leiho-hondoko gradientea Gaitu tamaina aldatzeko heldulekuak Iraungi Iraungitzearen iraupena: Fokuaren, sagua gainetik igarotzeko prozesuaren eta trepeten egoeren trantsizioa Jarraitu saguari Jarraitu saguari iraupena: Markoa Orokorra Teklatuko azeleratzaileen ikusgarritasuna: Etiketen trantsizioak Menuak nabarmentzekoa Menu-barrak nabarmentzea Menuak nabarmentzea Botoirik ez Botoirik ez Botoi bat Oxygen-en ezarpenak Aurrerapen-barren animazioa Labaintzeko-barrak Erakutsi teklatuko azeleratzaileak, beharrezkoa denean Fitxen trantsizioak Testu-editoreen trantsizioak Tresna-barrak nabarmentzea Goiko geziaren botoi mota: Bi botoi Ikuspegiak px Arrunta Txikia Oso txikia 