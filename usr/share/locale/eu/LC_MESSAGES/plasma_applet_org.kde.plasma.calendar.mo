��    	      d      �       �   	   �      �   <   �   5   0     f     �  4   �     �  �  �     �     �     �     �  #   �     �                                                    	    Ends at 5 General Show the number of the day (eg. 31) in the iconDay in month Show the week number (eg. 50) in the iconWeek number Show week numbers in Calendar Starts at 9 What information is shown in the calendar iconIcon: Working Day Project-Id-Version: plasma_applet_org.kde.calendar
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2017-11-03 08:17+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 17:00etan amaitzen da Orokorra Hileko eguna Asteko zenbakia Erakutsi asteko zenbakiak egutegian 9:00etan hasten da Ikonoa: Laneguna 