��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     	           <     K     i     p     y     ~     �     �  L   �     �       	   .     8     >     E  $   N  %   s  #   �  $   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-02-21 19:11+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Fitxategi %1 %1 fitxategi Karpeta %1 %1 karpeta %1/%2 prozesatuta %1/%2 prozesatuta %3/s abiaduran %1 prozesatuta %1 prozesatuta %2/s abiaduran Itxura Portaera Utzi Garbitu Konfiguratu... Bukatutako lanak Exekutatzen ari diren fitxategi-transferentzien / lanen zerrenda (kuiserver) Eraman beste zerrenda batera Eraman beste zerrenda batera. Pausarazi Kendu Kendu. Berrekin Erakutsi lan guztiak zerrenda batean Erakutsi lan guztiak zerrenda batean. Erakutsi lan guztiak zuhaitz batean Erakutsi lan guztiak zuhaitz batean. Erakutsi aparteko leihoak Erakutsi aparteko leihoak. 