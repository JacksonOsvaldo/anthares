��          �      �           	          "     4  	   A     K  a   R     �  
   �  	   �     �     �                 3      T     u  	   �     �     �     �  7   �  �  �     �     �     �     �     �     �  c   �     U     j     s     {     �     �     �     �     �          
  
        $     0  :   7                                                               
                              	           %1 (%2) %1 (long format) %1 (short format) %1 - %2 (%3) &Numbers: &Time: <h1>Formats</h1>You can configure the formats used for time, dates, money and other numbers here. Co&llation and Sorting: Currenc&y: Currency: De&tailed Settings Format Settings Changed Measurement &Units: Measurement Units: Measurement comboboxImperial UK Measurement comboboxImperial US Measurement comboboxMetric No change Numbers: Re&gion: Time: Your changes will take effect the next time you log in. Project-Id-Version: kcmformats
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-11 03:20+0100
PO-Revision-Date: 2017-08-29 23:29+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 (%2) %1 (formatu luzea) %1 (formatu laburra) %1 - %2 (%3) Ze&nbakiak: Or&dua: <h1>Formatuak</h1>Hemen konfiguratu dezakezu ordu, data, moneta eta beste zenbaki batzuen formatua. Bi&ldu eta sailkatu: &Moneta: Moneta: Ezarpen xeha&tuak Formatuen ezarpenak aldatu dira Neurtzeko &unitateak: Neurtzeko unitateak: Inperiala, Erresuma Batua Inperiala, AEB Metrikoa Aldaketarik ez Zenbakiak: Es&kualdea: Ordua: Zure aldaketek hurrengo saio hasieran izango dute eragina. 