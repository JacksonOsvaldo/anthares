��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �     ~  
   �     �  .   �     �     �  /   �  /   (  #   X  	   |  	   �  5   �     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-03-02 01:01+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 &Giltzatu pantaila berrekitean: Aktibazioa Errorea Pantaila giltzatzailearen probak huts egin du. Berehala Giltzatu saioa Giltzatu pantaila automatikoki hau igarotakoan: Giltzatu pantaila esekitako egoeratik esnatzean Es&katu pasahitza giltzatu ondoren:  min  min  seg  seg Pantaila giltzatzeko teklatuko laster-tekla orokorra. Horma-paper &mota: 