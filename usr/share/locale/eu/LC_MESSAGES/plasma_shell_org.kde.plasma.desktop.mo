��    E      D  a   l      �  
   �  
   �  
             !     %     9     H     N  	   ]     g  	   o     y     �     �  
   �     �     �  	   �     �     �     �     �     �     �               "     )  
   ;     F  2   Y  ?   �     �     �     �     �     �     �     
               .     <     ?     O     \     b     o  	   {     �     �     �     �  b   �  �   	     �	     �	  	   �	     �	     �	  
   �	  	   �	     	
     
     !
     '
     9
  �  J
  	        "     1     B     U     Y     j     w     ~     �     �     �     �     �     �  
   �     �     �     �     �               #     8     @     I     X     n     v     �     �  @   �  I   �     @  	   N     X  
   i     t     �     �     �     �     �     �     �     �  	   �             	   #     -     3     D     [  [   b  �   �     b     h          �     �     �     �     �     �     �     �          *       D   B   3       %       C           
   ;      )   8               E      .   "                @       ?   <                        :                  A             9       &           /         0   1         !                       ,   '       5   >         =              #   -   $   +              (       7       2   4   	                  6    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.plasma.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-08-28 00:37+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Jarduerak Gehitu ekintza Gehitu bereizlea Gehitu trepetak... Alt Ordezko trepetak Beti ikusgai Ezarri Ezarpenak ezarri Ezarri orain Egilea: Automatikoki ezkutatu Atzera botoia Behean Utzi Kategoriak Erdian Itxi Konfiguratu Konfiguratu jarduera Sortu jarduera... Ktrl Une honetan erabilia Ezabatu E-posta: Aurrera botoia Lortu trepeta berriak Altuera Labaintze horizontala Datu sarrera hemen Teklatu-lasterbideak: Antolamendua ezin da aldatu trepetak giltzatuta dauden bitartean Antolamendu aldaketak ezarri behar dira beste aldaketak egin ahal izateko Antolamendua: Ezkerrean Ezkerreko botoia Lizentzia: Giltzatu trepetak Maximizatu panela Meta Erdiko botoia Ezarpen gehiago... Saguaren ekintzak Ados Panelen lerrokatzea Kendu panela Eskuinean Eskuineko botoia Pantailaren ertza Bilatu... Maius Gelditu jarduera Gelditutako jarduerak: Aldatu Jarduneko moduluaren ezarpenak aldatu egin dira. Aldaketak ezarri edo baztertu nahi dituzu? Lasterbide honek appleta aktibatuko du: saguaren fokua berari emango dio, eta appletak zati gainerakor bat badu (hasiera menua esaterako), gainerakorra irekiko da. Goian Desegin desinstalatzea Desinstalatu Desinstalatu trepeta Labaintze bertikala Ikusgaitasuna Horma-papera Horma-paper mota: Trepetak Zabalera Leihoek estali dezakete Leihoak honen azpitik 