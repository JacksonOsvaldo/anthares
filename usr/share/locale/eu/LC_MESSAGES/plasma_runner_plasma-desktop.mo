��    	      d      �       �   .   �   2     )   C  -   m     �  '   �  H   �  R   *  �  }     D     Z     t     �  %   �  1   �  W   �  c   A             	                                 Note this is a KRunner keyworddesktop console Note this is a KRunner keyworddesktop console :q: Note this is a KRunner keywordwm console Note this is a KRunner keywordwm console :q: Open KWin interactive console Open Plasma desktop interactive console Opens the KWin interactive console with a file path to a script on disk. Opens the Plasma desktop interactive console with a file path to a script on disk. Project-Id-Version: plasma_runner_plasma-desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-26 11:21+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 mahaigaineko kontsola mahaigaineko :q: kontsola wm kontsola wm kontsola :q:  Ireki KWin-en kontsola elkarreragilea Ireki Plasma mahaigaineko kontsola elkarreragilea KWin-en kontsola elkarreragilea diskoko script baterako bide-izen batekin irekitzen du. Plasma mahaigaineko kontsola elkarreragilea diskoko script baterako bide-izen batekin irekitzen du. 