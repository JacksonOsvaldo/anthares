��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �     �     �     �     �     �     �               $     4  
   N     Y     j     ~     �     �     �     �  	   �     �     �     �  	                  )     7     >     L     ]     i     �     �     �     �     �     �  
   �  	   �      �     �               (     5      >     _     p     �     �     �     �     �            
   ,  =   7     u  
   �     �     �     �     �     �               6     L     c     |  	   �  /   �     �     �  )   �  #        =     U     p     �  	   �     �     �  "   �  7   �     !     2  #   C     g     u     �     �     �  J   �          4  0   �      �  "     "   )  "   L     o     �  &   �     �  )   �  /   �     (     :  1   L  &   ~  2   �     �     �     �  	   �     �     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kconfigwidgets5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-08-19 19:38+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 e&skuliburua %1-(r)i &buruz &Uneko neurria &Gehitu laster-marka &Atzera &Itxi K&onfiguratu %1... &Kopiatu E&zabatu Egin &dohaintza Liburu-markak &editatu... &Bilatu... &Lehen orrialdea E&gokitu orrialdera A&urrerantz &Joan... &Joan lerrora... &Joan orrialdera... A&zken orrialdea &Posta... &Bota zakarrontzira &Berria &Hurrengo orrialdea I&reki... &Itsatsi &Aurreko orrialdea I&nprimatu... &Irten &Berbistaratu Be&rrizendatu... &Ordeztu... &Eman errorearen berri... &Gorde &Gorde ezarpenak &Ortografia... &Desegin &Gora &Zoom... &Hurrengoa &Aurrekoa Erakut&si iradokizunak abiatzean Bazenekien...?
 Konfiguratu Eguneko iradokizuna &KDEri buruz &Garbitu Egiaztatu ortografia dokumentuan Garbitu zerrenda Itxi dokumentua Konfiguratu &jakinarazpenak... Konfiguratu &lasterbideak... Konfiguratu tresna-&barrak... Kopiatu hautapena arbelara Sortu dokumentu berria &Ebaki Ebaki hautapena arbelara Desau&tatu marcos@euskalgnu.org,igaztanaga@gmail.com,xalba@euskalnet.net Auto-detektatu Lehenetsia Pantaila osoko m&odua Bilatu &hurrengoa Bilatu a&urrekoa Egokitu orrialde-&luzeerara Egokitu orrialde-&zabalerara Joan a&tzera dokumentuan Joan aurrerantz dokumentuan Joan lehen orrialdera &Joan azken orrialdera Joan hurrengo orrialdera Joan aurreko orrialdera Joan gora Marcos,Ion Gaztañaga,Iñigo Salvador Azurmendi Sarrerarik ez Ireki &oraintsukoa Ireki duela gutxi ireki den dokumentu bat Ireki lehendik dagoen dokumentu bat Itsatsi arbelako edukia Inprimaketaren aur&rebista Inprimatu dokumentua Irten aplikaziotik &Berregin &Leheneratu Berbistaratu dokumentua Berregin desegindako azken ekintza Leheneratu dokumentuari egindako gorde gabeko aldaketak Gorde &honela... Gorde dokumentua Gorde dokumentua izen berri batekin H&autatu dena Hautatu zoom maila Bidali dokumentua posta bidez Erakutsi &menu-barra Erakutsi &tresna-barra Erakutsi menu-barra<p>Menu-barra berriro erakusten du ezkutatu ondoren</p> Erakutsi &egoera-barra Erakutsi Egoera-barra<p>Egoera-barra erakusten du, leihoaren behealdean egoeraren informazioarentzako erabiltzen den barra.</p> Erakutsi dokumentu inprimaketaren aurrebista bat Erakutsi edo ezkutatu menu-barra Erakutsi edo ezkutatu egoera-barra Erakutsi edo ezkutatu tresna-barra Aldatu aplikazioaren &hizkuntza... Eguneko ira&dokizuna Desegin azken ekintza Ikusi dokumentua bere egiazko neurrian &Zer da hau? Ez duzu konfigurazioa gordetzeko baimenik Gorde baino lehen autentikatzeko eskatuko zaizu Zooma &handiagotu Zooma &txikiagotu Zoom egin orrialdearen luzeera leihora egokitzeko Zoom egin orrialdea leihora egokitzeko Zoom egin orrialdearen zabalera leihora egokitzeko &Atzera A&urrerantz &Hasiera &Laguntza izenik gabea 