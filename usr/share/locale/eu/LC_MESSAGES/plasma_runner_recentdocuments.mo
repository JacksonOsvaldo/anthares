��          ,      <       P   :   Q   �  �   [   T                     Looks for documents recently used with names matching :q:. Project-Id-Version: plasma_runner_recentdocuments
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2014-01-26 01:53+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 :q:(r)ekin bat datozen izenak dituzten eta berriki erabili diren dokumentuak bilatzen ditu. 