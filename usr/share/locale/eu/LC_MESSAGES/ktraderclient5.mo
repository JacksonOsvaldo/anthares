��          D      l       �   6   �   3   �   8   �      -  �  ;  S   �  ?   G  B   �     �                          A command-line tool for querying the KDE trader system A constraint expressed in the trader query language A servicetype, like KParts/ReadOnlyPart or KMyApp/Plugin KTraderClient Project-Id-Version: ktraderclient
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-01 02:51+0200
PO-Revision-Date: 2014-02-16 08:27+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 KDEren zerbitzuak bilatzeko sistemari kontsultak egiteko komando-lerroko tresna bat Murriztapen bat, zerbitzuak kontsultatzeko hizkuntzan adierazia Zerbitzu mota bat, hala nola KParts/ReadOnlyPart edo KMyApp/Plugin KTraderClient 