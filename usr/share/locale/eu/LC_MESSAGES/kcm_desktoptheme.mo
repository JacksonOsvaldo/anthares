��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     O     m  &   y     �     �  +   �  
   �  
     *        <     \     t  8   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktoptheme
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2018-02-10 19:22+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Konfiguratu mahaigaineko gaia David Rosca asieriko@gmail.com,xalba@euskalnet.net Lortu gai berriak... Instalatu fitxategitik... Asier Urio Larrea,Iñigo Salvador Azurmendi Ireki gaia Kendu gaia Gai fitxategiak (*.zip *.tar.gz *.tar.bz2) Gaia instalatzeak huts egin du. Gaia ondo instalatu da. Gaia kentzeak huts egin du. Modulu honek mahaigaineko gaia konfiguratzen uzten dizu. 