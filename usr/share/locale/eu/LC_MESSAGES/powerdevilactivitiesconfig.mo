��          �      \      �     �     �     �     �              	   <     F     e  &        �     �     �  �   �  �   y  t     7   �  +   �     �  �  �     �     �     �     �     �  #     	   (  @   2     s  &   �  -   �  +   �  )     �   7  �   �  �   i	     �	  ;   �	     6
                                        
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: powerdevilactivitiesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2014-01-25 21:24+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
  min Jokatu honela: Beti Definitu portaera berezi bat Ez erabili ezarpen berezirik xalba@euskalnet.net,hizpol@ej-gv.es Hibernatu Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Ez itzali inoiz pantaila Ez eseki edo itzali inoiz ordenagailua PCa korronte alternoarekin ari da exekutatzen PCa bateria-energiarekin ari da exekutatzen PCa bateria gutxirekin ari da exekutatzen Itxura denez, energia kudeatzeko sistema ez da exekutatzen ari.
Sistema "Abioa eta itzaltzea" aukeran abiaraziz edo antolatuz konpon daiteke hori. Jarduera-zerbitzua ez da exekutatzen ari.
Jarduera-kudeatzaileak exekutatzen aritu behar du, jarduera jakin baten energia-kudeaketako portaera konfiguratzeko. Jarduera-zerbitzua oinarrizko funtzionalitateekin ari da exekutatzen.
Baliteke jarduera-izenak eta -ikonoak erabilgarri ez egotea. "%1" jarduera Erabili ezarpen bereiziak (erabiltzaile aurreratuak soilik) honen ondoren: 