Þ                      x  !   y  "     #   ¾  '   â     
          !     §  Z   +               ª     ¯     µ     Æ  ¶  Ù                 
   ¦     ±     Á     È     O  a   å     G     a  	   {  	             §           
      	                                                              @item:inlistbox Button size:Huge @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Very Large Animate buttons Center Check this option if the window border should be painted in the titlebar color. Otherwise it will be painted in the background color. Check this option if you want the buttons to fade in when the mouse pointer hovers over them and fade out again when it moves away. Check this option if you want the titlebar text to have a 3D look with a shadow behind it. Colored window border Config Dialog Left Right Title &Alignment Use shadowed &text Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-02 03:16+0100
PO-Revision-Date: 2014-02-05 18:24+0100
Last-Translator: IÃ±igo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Itzela Handia Arrunta Oso handia Botoi animatuak Erdian Markatu aukera hau, leihoaren ertza titulu-barraren kolore berean margotu behar bada. Bestela, atzeko planoaren kolorean margotuko da. Markatu aukera hau, nahi baduzu botoiak agertzea saguaren erakuslea gainetik pasatzean eta botoiak desagertzea erakuslea beste leku batera mugitzean. Markatu aukera hau, titulu-barraren testuak 3D itxura izatea nahi baduzu, atzean itzal eta guzti. Leihoaren ertz koloreduna Konfigurazio-elkarrizketa Ezkerrean Eskuinean Tituluaren &lerrokatzea Erabili &testu itzalduna 