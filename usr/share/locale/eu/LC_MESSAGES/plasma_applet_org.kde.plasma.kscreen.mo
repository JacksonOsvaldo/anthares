��    
      l      �       �   L   �   +   >  #   j     �  P   �  	   �  (     I   -  K   w  �  �  '   �     �  +   �  &   �          /     =     U     f                                      	   
    %1 is name of the newly connected displayA new display %1 has been detected Disables the newly connected screenDisable Failed to connect to KScreen daemon Failed to load root object Makes the newly conencted screen a clone of the primary oneClone Primary Output No Action Opens KScreen KCMAdvanced Configuration Places the newly connected screen left of the existing oneExtend to Left Places the newly connected screen right of the existing oneExtend to Right Project-Id-Version: plasma_applet_org.kde.plasma.kscreen
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-11-05 14:36+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Bistaratzaile berri bat hauteman da, %1 Desgaitu Huts egin du KScreen daimonera konektatzeak Huts egin du erroko objektua zamatzeak Bikoiztu irteera nagusia Ekintzarik ez Konfigurazio aurreratua Hedatu ezkerrera Hedatu eskuinera 