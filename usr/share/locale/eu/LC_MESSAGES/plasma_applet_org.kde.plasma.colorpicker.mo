��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  %   �     �     �     
          :  !   C     e     x     �      �     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: plasma_applet_org.kde.plasma_colorpicker
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2017-11-04 12:55+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Automatikoki kopiatu kolorea arbelera Garbitu historia Koloreen aukerak Kopiatu arbelera Lehenetsitako kolore formatua: Orokorra Ireki kolore elkarrizketa-koadroa Hautatu kolore bat Hautatu kolore bat Erakutsi historia Teklatuko lasterbidea sakatzean: 