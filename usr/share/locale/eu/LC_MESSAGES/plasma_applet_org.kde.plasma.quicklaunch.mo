��          �      <      �     �  <   �  
   �     	          &     3     ?  
   G     R     c     q     }     �     �  
   �     �  �  �     �  J   �     �  
   �     	          3     A     J     [     v     �     �     �     �     �     �                                                  
   	                                                Add Launcher... Add launchers by Drag and Drop or by using the context menu. Appearance Arrangement Edit Launcher... Enable popup Enter title General Hide icons Maximum columns: Maximum rows: Quicklaunch Remove Launcher Show hidden icons Show launcher names Show title Title Project-Id-Version: plasma_applet_org.kde.plasma.quicklaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-02-26 04:09+0100
PO-Revision-Date: 2017-11-06 00:50+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Gehitu abiarazlea... Gehitu abiarazleak arrastatu eta jareginez edo testuinguru menua erabiliz. Itxura Antolaketa Editatu abiarazlea... Gaitu gainerakorrak Sartu titulua Orokorra Ezkutatu ikonoak Gehienezko zutabe kopurua: Gehienezko errenkada kopurua: Abiarazte azkarra Kendu abiarazlea Erakutsi ezkutuko ikonoak Erakutsi abiarazleen izenak Erakutsi titulua Titulua 