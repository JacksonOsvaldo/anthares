��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �       �  "   �       6   !     X     w          �     �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: plasma_applet_org.kde.plasma.diskquota
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-11-05 12:59+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Disko kuota Ez da kuota murrizketarik aurkitu. Instalatu 'quota' Kuota tresna ez da aurkitu.

Instalatu 'quota' tresna. quota exekutatzea huts egin du %1 / %2 %1 erabili gabe Kuota: %%1 erabilita %1: %2% erabilita 