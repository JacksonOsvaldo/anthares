��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  .   �  1   )  <   [  0   �  3   �  ,   �     *     J  -   [         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-26 19:26+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Ezin izan da detektatu fitxategiaren MIME mota Ezin izan dira eskatutako funtzio guztiak aurkitu Ezin izan da aurkitu adierazitako helburua duen hornitzailea Errorea gertatu da scripta exekutatzen saiatzean Bide-izen baliogabea eskatutako hornitzailearentzat Ezin izan da irakurri hautatutako fitxategia Zerbitzua ez zegoen erabilgarri Errore ezezaguna URL bat zehaztu behar duzu zerbitzu honentzat 