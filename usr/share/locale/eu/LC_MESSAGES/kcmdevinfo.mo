��    [      �     �      �  !   �     �  	          S     	   h     r     �     �     �     �     �     �     �     �     �  J   	     O	     W	      c	  	   �	  
   �	     �	     �	     �	     �	     �	     �	  L   �	  	   0
  	   :
  
   D
  
   O
  
   Z
     e
     n
     
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
                    -     1     A     I     V  
   i  	   t     ~  
   �     �     �     �     �     �     �     �     �                6     <     @     D  H   K     �     �     �     �     �     �  
   �  &   �       "        ;     Y     v     �     �     �  	   �  �  �  *   �     �  	   �     �     �               )     0     7     ?  	   Q     [     i  	   �     �  ,   �     �     �  #   �               %     7     P  $   f     �     �  A   �  	   �  	   �  
   �  
   �  
               	   )     3     H     _     q     w  @   �     �     �     �     �               $     (     7     ?     Q     j     z     �     �     �     �     �     �     �     �     �          /     K     h     o     t     y  F   �     �     �     �     �          #  
   '  	   2  
   <  
   G  
   R  	   ]  	   g  	   q  	   {  	   �     �           @           &       !   "   #            Y   >      J   K   %   4       <   R          :   7   E   $           ?      ,   6   P       5       H             =   2                   F   +                      D          3   '   L   B       *       I             1       S   ;   
       9       M   8           )              X   -      T   W              O          C       /       V             [       .      N      U   Z   G          	          A       Q   0       (    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2014-02-04 22:39+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 
Solid oinarri duen gailu-erakusle modulua (c) 2010 David Hubner AMD 3DNow ATI IVEC %1/%2 libre (%%3 erabilita) Bateriak Bateria mota:  Busa:  Kamera Kamerak Kargaren egoera:  Kargatzen Tolestu denak Compact Flash irakurlea Gailu bat Gailuari buruzko informazioa Zerrendatutako gailu guztiak erakusten ditu. Gailuak Deskargatzen xalba@euskalnet.net,hizpol@ej-gv.es Zifratua Zabaldu denak Fitxategi-sistema Fitxategi-sistema mota:  Disko-zurrun unitatea Funtzionamenduan konekta daitekeena? IDE IEEE1394 Unean hautatuta dagoen gailuari buruzko informazioa erakusten du. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Teklatua Teklatua + sagua Etiketa:  Gehieneko abiadura:  Memory Stick irakurlea Hemen muntatuta:  Sagua Multimedia-jotzaileak Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Ez Kargarik gabe Ez dago daturik eskuragarri Muntatu gabe Ezarri gabe Unitate optikoa PDA Partizio-taula Nagusia %1 Prozesatzailea Prozesatzaile-zenbakia:  Prozesatzaileak Produktua:  RAID Aldagarria? SATA SCSI SD/MMC irakurlea Erakutsi gailu guztiak Erakutsi gailu aipagarrienak Smart Media irakurlea Biltegiratzeko unitateak Onartzen diren gidariak:  Onartutako agindu multzoa:  Onartzen diren protokoloak:  UDIa:  UPSa USBa UUIDa:  Uneko gailuaren UDIa (gailuaren identifikatzaile bakarra) erakusten du Unitate ezezaguna Erabiligabea Hornitzailea:  Bolumeneko espazioa: Bolumenaren erabilera:  Bai kcmdevinfo Ezezaguna Bat ere ez Bat ere ez Plataforma Ezezaguna Ezezaguna Ezezaguna Ezezaguna Ezezaguna xD irakurlea 