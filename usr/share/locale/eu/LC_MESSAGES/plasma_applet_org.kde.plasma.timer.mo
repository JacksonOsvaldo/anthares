��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {     H     X     k     w  
   }     �  	   �  	   �     �     �  A   �     
           )     A     T     e     m     |     �     �  k   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-11-09 19:19+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 martxan dago %1 ez dago martxan Be&rrezarri Ha&si Aurreratua Itxura Komandoa: Bistaratu Exekutatu komando bat Jakinarazpenak Geratzen den denbora: segundo %1 Geratzen den denbora: %1 segundo Exekutatu komando bat Geldi&tu Erakutsi jakinarazpenak Erakutsi segundoak Erakutsi titulua Testua: Tenporizadorea Tenporizadoreak amaitu du Tenporizadorea martxan da Titulua: Erabili saguaren gurpila digituak aldatzeko edo hautatu aurrez zehaztutako tenporizadoreak testuingu-menuan 