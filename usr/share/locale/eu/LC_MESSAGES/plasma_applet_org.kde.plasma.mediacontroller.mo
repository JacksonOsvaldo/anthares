��          �            x     y  !   �     �     �     �  @   �     *  $   ?     d     k  "   �     �  %   �     �     �  �       �     �     �          '     D     J  0   O     �     �     �     �     �     �     �                        
                                    	                   Artist of the songby %1 Artist of the songby %1 (paused) Choose player automatically General No media playing Open player window or bring it to the front if already openOpen Pause playbackPause Pause playback when screen is locked Paused Play next trackNext Track Play previous trackPrevious Track Quit playerQuit Remaining time for song e.g -5:42-%1 Start playbackPlay Stop playbackStop Project-Id-Version: plasma_applet_org.kde.plasma.mediacontroller
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2017-11-05 15:08+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 Artista: %1 Artista: %1 (etenda) Hautatu jotzailea automatikoki Orokorra Ez da euskarririk jotzen ari Ireki Eten Eten atzera jotzea pantaila giltzatuta dagoenean Etenda Hurrengo pista Aurreko pista Irten -%1 Jo Gelditu 