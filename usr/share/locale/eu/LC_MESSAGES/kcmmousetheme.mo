��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �  �  �     �  �   �  j   Q  Q   �  
             9     K  !   X  8   z     �  .   �  Q   �     E	     K	  
   \	  A   g	  P   �	  -   �	                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-02-04 23:32+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) 2003-2007 Fredrik Höglund <qt>Ziur zaude <i>%1</i> kurtsore-gaia ezabatu nahi duzula?<br />Gai honetan instalatutako fitxategi guztiak ezabatu egingo dira.</qt> <qt>Ezin duzu ezabatu erabiltzen ari zaren gaia.<br />Beste gai batera aldatu behar duzu lehendabizi.</qt> %1 izeneko gaia lehendik ere badago zure ikono-gaien karpetan. Ordeztu nahi duzu? Berrespena Kurtsorearen ezarpenak aldatuta Kurtsorearen gaia Deskribapena Arrastatu edo idatzi gaiaren URLa marcos@euskalgnu.org,xalba@euskalnet.net,hizpol@ej-gv.es Fredrik Höglund Eskuratu kolore-antolaera berriak Internetetik Marcos Goyeneche,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Izena Gaia gainidatzi? Kendu gaia Ez dirudi %1 fitxategia kurtsore-gaiaren artxibo balioduna denik. Ezin da deskargatu kurtsore-gaiaren artxiboa; egiaztatu %1 helbidea zuzena dela. Ezin da aurkitu kurtsore-gaiaren %1 artxiboa. 