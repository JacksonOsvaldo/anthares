��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �     �     �          0     9     O  -   X     �     �     �     �     �     �  ?           	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: plasma_applet_org.kde.printmanager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2017-07-18 21:14+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 &Konfiguratu inprimagailuak... Aktibo dauden lanak soilik Lan guztiak Osatutako lanak soilik Konfiguratu inprimagailua Orokorra Ez dago lan aktiborik Lanik ez Ez da inprimagailurik konfiguratu edo aurkitu Lan aktibo bat %1 lan aktibo Lan bat %1 lan Ireki inprimaketa-ilara Inprimaketa-ilara hutsik dago Inprimagailuak Bilatu inprimagailu bat... Inprimaketa-lan bat dago ilaran %1 inprimaketa-lan daude ilaran 