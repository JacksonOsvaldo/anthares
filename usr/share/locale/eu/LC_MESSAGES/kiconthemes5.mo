��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  �  �     �     �  E   �     �     �     �  
   �     	               %  
   9     D     T     [  4   p     �     �                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kiconthemes5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2017-08-29 23:55+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 &Arakatu... &Bilatu: *.png *.xpm *.svg *.svgz|Ikono-fitxategiak (*.png *.xpm *.svg *.svgz) Ekintzak Guztiak Aplikazioak Kategoriak Gailuak Ikurrak Aurpegierak Ikonoaren iturburua Mime-motak &Beste ikonoak: Lekuak &Sistemaren ikonoak: Bilatu ikonoen izenak interaktiboki (adib. karpeta). Hautatu ikonoa Egoera 