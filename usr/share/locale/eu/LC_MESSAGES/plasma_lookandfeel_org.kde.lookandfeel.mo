��    *      l  ;   �      �     �     �  .   �     �     �     �     �  	             .     B     Q  1   e     �     �     �     �     �  '   �            '        :     I     P     X  5   a     �     �     �     �     �  $   �  A   �  �   :           '  C   8  '   |     �     �  �  �     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     '
     >
  4   W
  
   �
     �
  &   �
     �
     �
       	          4   &     [     l     r       *   �     �     �     �     �     �  	   �     �     
       "   1     T     r     y     �             !          
          %                )   "       $      '                                                      &                        #                                     	   (       *        %1% Back Button to change keyboard layoutSwitch layout Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout No media playing Nobody logged in on that sessionUnused Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: plasma_lookandfeel_org.kde.lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-02-21 22:50+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 %%1 Atzera Aldatu diseinua Utzi Blok Maius aktibatuta dago Itxi Itxi bilaketa Konfiguratu Konfiguratu bilatzeko pluginak Mahaigain saioa: %1 Beste erabiltzaile bat Teklatuaren diseinua: %1 Saioa amaitzeko segundo 1 Saioa amaitzeko %1 segundo Hasi saioa Saio-hasteak huts egin du Saioa hasi beste erabiltzaile bat gisa Amaitu saioa Ez da euskarririk jotzen ari Erabili gabe Pasahitza Berrabiarazi Berrabiarazteko segundo 1 Berrabiarazteko %1 segundo Azken kontsultak Kendu Berrabiarazi Itzali Itzaltzeko segundo 1 Itzaltzeko %1 segundo Hasi saio berria Eseki Aldatu Aldatu saioa Aldatu erabiltzailea Bilatu... Bilatu '%1'... Plasma KDEk egin du Giltzapetik askatu giltzapetik askatzeak huts egin du TTY %1-an (%2 bistaratzailea) TTY %1 Erabiltzaile-izena %1 (%2) 