��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b           5     ;     K     S     e     n     u     |     �     �  %   �     �  3   �  3   (  
   \     g     p     �     �     �  1   �     �     
  
     &   !     H     Q     l     y     �     �     �     �     �     �       '   	     1     K     i  $   r     �     �     �     �  	   �     �  	             #     3  4   H  #   }  
   �     �     �     �     �     �     �     �  '     )   :  '   d  '   �  >   �     �                    5     8     @     H     Q     b     x     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-10-31 08:05+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Mahaigain &guztietan &Itxi &Pantaila-betea &Mugitu Mahaigain &berria I&ltzatu Bil&du &%1 %2 %1(e)(a)n ere erabilgarri Gehitu uneko jarduerara Jarduera guztietan Baimendu programa hau taldekatu dadin Alfabetikoki Ipini atazak beti horrenbeste errenkadako zutabetan Ipini atazak beti horrenbeste zutabeko errenkadetan Antolaketa Portaera Jardueraren arabera Mahaigainaren arabera Programa-izenaren arabera Itxi leihoa edo taldea Atazatik atazara iragan saguaren gurpila erabiliz Ez taldekatu Ez sailkatu Iragazkiak Ahaztu berriki erabilitako dokumentuak Orokorra Taldekatzea eta sailkatzea Taldekatzea: Nabarmendu leihoak Ikonoaren neurria: M&antendu besteen gainean Mantendu &besteen azpian Mantendu abiarazleak banatuta Handia Ma&ximizatu Eskuz Markatu audioa jotzen duten aplikazioak Gehienezko zutabe kopuru: Gehienezko errenkada kopurua: I&konotu Ikonotu/leheneratu leihoa edo taldea Ekintza gehiago Eraman &uneko mahaigainera Eraman &jarduerara Eraman &mahaigainera Isilarazi Instantzia berria %1(e)(a)n Jarduera guztietan Uneko jardueran Erdiko klik egitean: Taldekatu soilik ataza-kudeatzailea beteta dagoenean Ireki taldeak leiho-gainerakorretan Leheneratu Eten Hurrengo pista Aurreko pista Irten &Handiera aldatu Iltzea kendu Leku %1 gehiago %1 leku gehiago Erakutsi soilik uneko jarduerako atazak Erakutsi soilik uneko mahaigaineko atazak Erakutsi soilik uneko pantailako atazak Erakutsi soilik ikonotuta dauden atazak Erakutsi aurrerapena eta egoeraren informazioa ataza-botoietan Erakutsi argibideak Txikia Sailkatzea: Abiarazi instantzia berria Jo Gelditu Ezer ez I&ltzatu Taldekatu/banatu %1(e)(a)n erabilgarri Erabilgarri jarduera guztietan 