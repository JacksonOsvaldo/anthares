��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  �     U
     t
  >   �
  5   �
     �
          &     @  8   T  l   �     �               0     9     F     S     `     l     y     �     �     �  
   �                               	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-01-26 01:42+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 <p>Mahaigaineko beste saio bat irekitzea aukeratu duzu.<br />Uneko saioa ezkutatuko da, eta saioa hasteko beste pantaila bat agertuko da.<br />Funtzio tekla bat (F teklak) esleituko zaio saio bakoitzari; F%1 normalean lehenengo saioari esleitzen zaio, F%2 bigarren saioari eta abar. Saio batetik bestera joan zaitezke Ktrl, Alt eta dagokion F tekla aldi berean sakatuz. Gainera, KDE paneleko eta mahaigaineko menuek saio batetik bestera joateko ekintzak dituzte.</p> Saio guztiak zerrendatzen ditu Blokeatu pantaila Uneko saioa giltzatzen du, eta pantaila-babeslea abiarazten du Saioa amaitzen du, uneko mahaigaineko saiotik irtenda Saio berria Ordenagailua berrabiarazten du Berrabiarazi ordenagailua Itzali ordenagailua Beste saio bat hasten du beste erabiltzaile-izen batekin :q: erabiltzailearen saio aktibora aldatzen du, edo saio aktibo guztiak zerrendatzen ditu :q: ematen ez bada Ordenagailua itzaltzen du saioak Abisua - Saio berria blokeatu amaitu saioa Amaitu saioa amaitu saioa saio berria berrabiarazi berrabiarazi itzali aldatu erabiltzailea aldatu aldatu :q: 