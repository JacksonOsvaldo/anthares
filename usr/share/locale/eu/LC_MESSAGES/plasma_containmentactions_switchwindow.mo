��          <      \       p   "   q      �   *   �   �  �   +   �  -   �  +   
                   Display a submenu for each desktop Display all windows in one list Display only the current desktop's windows Project-Id-Version: plasma_containmentactions_switchwindow
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-30 03:13+0200
PO-Revision-Date: 2014-01-27 17:16+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Bistaratu azpimenu bat mahaigain bakoitzeko Bistaratu leiho guztiak zerrenda bakar batean Bistaratu uneko mahaigaineko leihoak soilik 