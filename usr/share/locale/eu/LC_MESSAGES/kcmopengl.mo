��    B      ,  Y   <      �  A   �     �  4   �     '  
   4     ?     F     M     Q  
   `     k      w     �     �     �     �     �     �     �     �     �          !     *     A     O     d     �     �     �     �     �          *     G     e     �     �     �  %   �     �     	     $	  !   8	     Z	     o	     �	     �	     �	     �	     �	     �	  	   �	  	   �	     �	     �	     
     
     *
     <
  	   O
     Y
     o
     �
     �
  �  �
  A   P     �  2   �     �  
   �     �     �                    .  6   ;     r     �     �     �     �     �     �     �     �     �                -     <  %   X  $   ~  -   �     �     �       &   '  #   N  "   r  &   �  !   �     �  '   �  0        O  R   h     �  &   �     �  )        <     R     e     t     �     �     �     �     �     �     �     �             	   4     >     [     {  	   �        B   *                 	          9      6   @   "      #         $                 4           %       7   
                 :       0          1       )      (      ;   A   +   &      2                        3       ?          /   .      <           5           !   ,      8          >             '         -   =       (c) 2008 Ivo Anjo
(c) 2004 Ilya Korniyko
(c) 1999-2002 Brian Paul 3D Accelerator Author of glxinfo Mesa demos (http://www.mesa3d.org) Aux. buffers Brian Paul Device Driver EGL EGL Extensions EGL Vendor EGL Version EMAIL OF TRANSLATORSYour emails Frame buffer properties GLU GLU extensions GLU version GLX GLX extensions Helge Deller Ilya Korniyko Implementation specific Information Ivo Anjo KCM OpenGL Information Kernel module Max. 3D texture size Max. anisotropy filtering level Max. cube map texture size Max. display list nesting level Max. evaluator order Max. number of clipping planes Max. number of light sources Max. pixel map table size Max. recommended index count Max. recommended vertex count Max. rectangular texture size Max. texture LOD bias Max. texture size Max. vertex blend matrices Max. vertex blend matrix palette size Max. viewport dimensions NAME OF TRANSLATORSYour names Name of the Display No. of compressed texture formats No. of texture units Occlusion query counter bits Original Maintainer Points and lines Renderer Revision Stack depth limits Subpixel bits Subvendor Texturing Value Various limits Vendor client GLX extensions client GLX vendor client GLX version kcmopengl server GLX extensions server GLX vendor server GLX version unknown Project-Id-Version: kcmopengl
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-06 03:08+0100
PO-Revision-Date: 2014-02-06 07:26+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 (c) 2008 Ivo Anjo
(c) 2004 Ilya Korniyko
(c) 1999-2002 Brian Paul 3D bizkortzailea glxinfo Mesa demoen egilea (http://www.mesa3d.org) Laguntza-bufferrak Brian Paul Gailua Kontrolatzailea EGL EGL luzapenak EGL hornitzailea EGL bertsioa asieriko@gmail.com,xalba@euskalnet.net,hizpol@ej-gv.es Frame bufferraren propietateak GLU GLU luzapenak GLU bertsioa GLX GLU luzapenak Helge Deller Ilya Korniyko Inplementazio espezifikoa Informazioa Ivo Anjo KCM OpenGLri buruzko informazioa Nukleo-modulua 3D testuren tamaina maximoa Anisotropo-iragazkiaren maila maximoa Kubo-maparen testura-tamaina maximoa Bistaratze-zerrenden habiaratze-maila maximoa Ebaluatzaile-ordena maximoa Ebakidura-plano kopuru maximoa Argi-iturri kopuru maximoa Pixel-maparen taularen tamaina maximoa Gomendatutako indize kopuru maximoa Gomendatutako erpin kopuru maximoa Testura angeluzuzenaren neurri maximoa Testuren LOD desbideratze maximoa Testura-tamaina maximoa Erpin-nahasketen matrize kopuru maximoa Erpin-nahasketen matrizeen paleta-neurri maximoa Leihatila-neurri maximoa Asier Urio Larrea,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Bistaratzailearen izena Konprimatutako testura-formatu kopurua Testura-unitate kopurua Oklusio-kontsultaren kontagailuaren bitak Jatorrizko arduraduna Puntuak eta marrak Errendatzailea Berrikuspena Pilaren sakontasun-mugak Azpipixelen bitak Azpihornitzailea Testurak Balioa Hainbat muga Hornitzailea bezeroaren GLX luzapenak bezeroaren GLX hornitzailea bezeroaren GLX bertsioa kcmopengl zerbitzariaren GLX luzapenak zerbitzariaren GLX hornitzailea zerbitzariaren GLX bertsioa ezezaguna 