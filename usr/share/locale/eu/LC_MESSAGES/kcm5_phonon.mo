��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  O   �  �   �  !   f  W   �     �     �  5   	     ?  8   O     �     �  
   �     �  .   �  1   �  1   /  $   a     �  O   �     �     �  �     K   �  9   �          )     =     \     p  	   �     �     �  
   �  '   �     �       c        j     �     �     �     �     �     �     �     �               ,     ;      H     i     u     {     �  �   �     L  5   \  �   �       8   '  2   `  2   �  %   �     �     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-02-22 10:16+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Bizkarraldekoaren aldaketa aplikatzeko, saioa amaitu eta berriro hasi behar da. Zure sisteman aurkitutako Phonon-en bizkarraldekoen zerrenda. Hemengo ordenak zehazten du Phonon-ek zer ordenatan erabiliko dituen. Gailuen zerrenda honi aplikatu... Aplikatu orain erakusten ari den gailu-hobespen zerrenda audio-jotzeko kategoria hauei: Audio-hardwarearen ezarpena Audio-jotzea Audio-jotzeko gailuaren hobespena, '%1' kategoriarako Audio-grabatzea Audio-grabatzeko gailuaren hobespena, '%1' kategoriarako Bizkarraldekoa Colin Guthrie Konektorea Copyright 2006 Matthias Kretz Lehentsitako audio-jotzeko gailuaren hobespena Lehentsitako audio-grabatzeko gailuaren hobespena Lehentsitako bideo-grabatzeko gailuaren hobespena Lehentsitako/Zehaztugabeko kategoria Atzeratu Banakako kategoriek gainidatz ditzaketen gailuen ordena lehenetsia zehazten du. Gailuaren konfigurazioa Gailuaren hobespena Zure sisteman aurkitutako gailuak, hautatutako kategoriarako egokiak direnak. Aukeratu zure aplikazioek erabiltzea nahi duzun gailua. marcos@euskalgnu.org,asieriko@gmail.com,xalba@euskalnet.net,hizpol@ej-gv.es Huts egin du hautatutako audio-irteerako gailua ezartzean Aurreko erdialdea Aurreko ezkerraldea Erdialdeko aurreko ezkerraldea Aurreko eskuinaldea Erdialdeko aurreko eskuinaldea Hardwarea Gailu independenteak Sarrerako mailak Baliogabea KDEren audio-hardwarearen konfigurazioa Matthias Kretz Mono Marcos Goyeneche,Asier Urio Larrea,Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Phonon-en konfigurazio-modulua Jotzea (%1) Hobetsi Profila Atzeko erdialdea Atzeko ezkerraldea Atzeko eskuinaldea Grabatzea (%1) Erakutsi gailu aurreratuak Alboko ezkerraldea Alboko eskuinaldea Soinu-txartela Soinu-gailua Bozgorailuen kokalekua eta proba Subwoofer-a Proba Probatu hautatutako gailua %1 probatzen Ordenak gailuen lehentasuna zehazten du. Zerbaitegatik lehen gailua ezin bada erabili, Phonon bigarrena erabiltzen saiatuko da, eta hala jarraituko du, hurrenez hurren. Kanal ezezaguna Erabili gailuen uneko zerrenda kategoria gehiagorako. Euskarri-erabileren hainbat kategoria. Kategoria bakoitzerako, aukeratu dezakezu Phonon aplikazioek zer gailu erabiltzea nahi duzun. Bideo-grabatzea Bideo-grabatzeko gailuaren hobespena, '%1' kategoriarako Baliteke bizkarraldekoak audioa grabatzen ez uztea Baliteke bizkarraldekoak bideoa grabatzen ez uztea hautatutako gailuaren lehentasunik ez hobetsi hautatutako gailua 