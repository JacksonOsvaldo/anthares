��             +         �     �     �     �  	   �     �               '     3     H     a     t     �  S   �  w   �     `  M   u      �     �     �  	             2     K  %   Z     �     �  #   �     �     �     �  �  	     �     �     �  
   �     �               )     5     R     o     �     �  ^   �  h   
	     s	  R   �	  #   �	     �	  @   
     R
     `
     {
     �
  $   �
     �
  
   �
  +   �
  #        7  
   G                                            
                                                     	                                                ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Toggle alternative window switching Toggle window switching Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-08-09 02:47+0100
Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>
Language-Team: Basque <kde-i18n-doc@kde.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
  ms % %1 - Mahaigain guztiak %1 - Kuboa %1 - Uneko aplikazioa %1 - Uneko mahaigaina %1 - Zilindroa %1 - Esfera &Berraktibatzeko atzerapena: &Aldatu mahaigaina ertzetik: Aktibatzeko &atzerapena: Jarduera-kudeatzailea Beti gaituta Ekintza bat abiarazi ondoren hurrengo abiarazpena gertatu arte igaro behar duen denbora-tartea Ekintza abiaraz dadin saguaren kurtsoreak leiho ertzaren aurka bultzatzen eman behar duen denbora-tartea Aplikazio-abiarazlea Aldatu mahaigaina, saguaren kurtsorea pantailaren ertzaren aurka bultzatzen denean xalba@euskalnet.net,hizpol@ej-gv.es Giltzatu pantaila Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza Ekintzarik ez Leihoak mugitzean bakarrik Exekutatu komandoa Beste ezarpen batzuk Lauza laurdenak kanpoaldean abiarazi Erakutsi mahaigaina Desgaituta Aktibatu/desaktibatu ordezko leiho-aldatzea Aktibatu/desaktibatu leiho-aldatzea Leiho-kudeaketa leihoarena 