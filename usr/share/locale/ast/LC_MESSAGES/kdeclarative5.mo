��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �   �  +   �     �  %   �  	   �       	          �      h   �  1   )  -   [     �     �                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2016-12-20 23:02+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Primi nel menú, depués introduz l'atayu como te prestaría nel programa.
Exemplu pa Ctrl+A: ten primida la tecla Ctrl y dempués calca la A Conlfictu col atayu estándar d'aplicación alministradores@softastur.org Shell d'aplicaciones QML de KPackage  Softastur Nada Reasignar Atayu acutáu La combinación de tecles «%1» tamién s'usa pa l'aición estándar «%2» que delles aplicaciones usen.
¿De xuru que quies usala tamién como atayu global? La tecla F12 ta acutada en WIndows, asina que nun pue usase pa un atayu global.
Escueyi otra, por favor. La tecla que tas acabante primi nun la sofita Qt. El nome únicu de l'aplicación (obligatorio) Tecla ensin sofitu Entrada 