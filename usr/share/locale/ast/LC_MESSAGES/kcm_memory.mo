��            )         �     �     �     �  �   �     �     �     �  
   �     �     �      �  	             1     C     P     a     h     �     �     �  
   �  g   �     (     :     G     ^     q  	   �  
   �  �  �  
   <     G     d  �   x     -     =     Q     d     u     �     �     �     �     �     	     	     "	  	   *	     4	     D	     T	     h	  n   	     �	     
     
     (
     F
     \
  
   o
                                                                                 
                      	                                 %1 free (c) 1998 - 2002 Helge Deller 1 byte = %1 bytes = <p>The swap space is the <b>virtual memory</b> available to the system.</p> <p>It will be used on demand and is provided through one or more swap partitions and/or swap files.</p> Active memory: Application Data Disk Buffers Disk Cache Disk buffers: Disk cache: EMAIL OF TRANSLATORSYour emails Free Swap Free physical memory: Free swap memory: Helge Deller Inactive memory: Memory NAME OF TRANSLATORSYour names Not available. Physical Memory Shared memory: Swap Space This graph gives you an overview of the <b>total sum of physical and virtual memory</b> in your system. Total Free Memory Total Memory Total physical memory: Total swap memory: Used Physical Memory Used Swap kcm_memory Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-16 03:33+0200
PO-Revision-Date: 2017-02-02 14:05+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 llibres (c) 1998 - 2002 Helge Deller 1 byte = %1 bytes = <p>L'espaciu d'intercambéu ye la <b>memoria virtual</b> disponible nel sistema.</p> <p>Usaráse so demanda y fórnese pente una o más particiones y/o ficheros d'intercambéu.</p> Memoria activa: Datos d'aplicación Búferes del discu Caché del discu Búferes del discu: Caché del discu: alministradores@softastur.org Intercambéu llibre Memoria física total: Memoria llibre d'intercambéu: Helge Deller Memoria inactiva: Memoria Softastur Non disponible. Memoria física Memoria compartida: Espaciu d'intercambéu Esti gráficu date una previsualización de la <b> suma total de memoria física y virtual</b> nel to sistema. Memoria llibre total Memoria total Memoria física total: Memoria total d'intercambéu: Memoria física usada Intercambéu usáu kcm_memory 