��          �      ,      �     �      �     �  "   �  0   �  1   !  +   S          �     �     �  8   �  '        /     M     f  �  z  $        @     ^  (   f  :   �  ;   �  9        @     H     h  	   z  :   �  +   �     �          $                                                             
   	              (c) 2015, The KDE Developers EMAIL OF TRANSLATORSYour emails Entry Failed to open wallet %1. Aborting Failed to read entry %1 value from the %2 wallet Failed to read entry %1 value from the %2 wallet. Failed to write entry %1 value to %2 wallet Folder KWallet query interface Missing argument NAME OF TRANSLATORSYour names Only one mode (list, read or write) can be set. Aborting Please specify the mode (list or read). The folder %1 does not exist! Too many arguments given Wallet %1 not found Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2017-04-19 18:27+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 (c) 2015, Los desendolcadores de KDE alministradores@softastur.org Entrada Fallu al abrir la cartera %1. Albortando Fallu al cargar el valor de la entrada %1 de la cartera %2 Fallu al cargar el valor de la entrada %1 de la cartera %2. Fallu al escribir el valor de la entrada %1 na cartera %2 Carpeta Interfaz de consulta de KWallet Falta l'argumentu Softastur Namái pue afitase un mou (list, read o write). Albortando Especifica'l mou (list ó read), por favor. ¡La cartera %1 nun esiste! Diéronse milenta argumentos Nun s'alcontró la carpeta %1 