��          �   %   �      `     a  
   g  /   r  -   �     �     �  
   �     �     
        W   )     �  ,   �  	   �     �     �     �     �     �  
   �               5     I  1   ^     �  �  �     >  
   D  
   O     Z     i     }     �     �     �  	   �  r   �     ?  1   V  	   �     �  	   �     �     �     �     �     �     �       0   +     \     q                     
                                                                         	                              %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used Remove from Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-06 00:07+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Escoyer... Llimpiar iconu Amestar a favoritos Toles aplicaciones Aspeutu Aplicaciones Anováronse les aplicaciones Ordenador Arrastra les llingüetes ente les caxes p'amosales/anubriles, o reordena les llingüetes visibles arrastrándoles. Editar aplicaciones... Ampliar la gueta a marcadores, ficheros y correos Favoritos Llingüetes anubríes Historial Iconu: Colar Botones del menú Usao davezu Desaniciar de Favoritos Amosar aplicaciones pel nome Ordenar alfabéticamente Camudar les llingüetes al pasar penriba d'elles Teclexa pa guetar... Llingüetes visibles 