��    !      $  /   ,      �  Z   �  *   D     o     �     �     �     �     �           (     A     ^  -   w     �     �     �     �          '  !   :     \     {     �     �     �     �     �       (   *     S  >   f  &   �  �  �     m  )   u  !   �  !   �     �     �     	     :	  #   W	      {	  $   �	      �	  7   �	     
     1
     M
     d
  !   {
     �
  #   �
  !   �
       #         D     _      }  !   �     �  &   �       <     /   Y                                                                      
                                   	                                !                      %1 is 'the slot asked for foo arguments', %2 is 'but there are only bar available'%1, %2. %1 is not a function and cannot be called. '%1' is not a valid QLayout. '%1' is not a valid QWidget. Action takes 2 args. ActionGroup takes 2 args. Call to '%1' failed. Could not construct value Could not create temporary file. Could not open file '%1' Could not open file '%1': %2 Could not read file '%1' Exception calling '%1' function from %2:%3:%4 Failed to create Action. Failed to create ActionGroup. Failed to create Layout. Failed to create Widget. Failed to load file '%1' File %1 not found. First argument must be a QObject. Incorrect number of arguments. Must supply a filename. Must supply a layout name. Must supply a valid parent. Must supply a widget name. No classname specified No classname specified. Not enough arguments. There was an error reading the file '%1' Wrong object type. but there is only %1 available but there are only %1 available library only takes 1 argument, not %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2016-12-22 03:21+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1, %2. %1 nun ye una función y nun pue llamase. «%1» nun ye un QLayout válidu. «%1» nun ye un QWidget válidu. Action garra 2 argumentos. ActionGroup garra 2 argumentos. Falló la llamada a «%1». Nun pudo construyise'l valor Nun pudo crease'l ficheru temporal. Nun pudo abrise'l ficheru «%1» Nun pudo abrise'l ficheru «%1»: %2 Nun pudo lleese'l ficheru «%1» Esceición llamando a la función «%1» dende %2:%3:%4 Fallu al crear Action. Fallu al crear ActionGroup. Fallu al crear Layout. Fallu al crear Widget. Fallu al cargar el ficheru «%1» Nun s'alcontró'l ficheru %1. El primer argumentu ha ser QObject. Númberu incorreutu d'argumentos. Ha fornise un nome de ficheru. Ha fornise un nome de distribuión. Ha fornise un pá válidu. Ha fornise un nome de widget. Nun s'especificó'l nome de clas Nun s'especificó'l nome de clas. Nun hai argumentos abondos. Hebo un fallu lleendo'l ficheru «%1» Triba incorreuta d'oxetu pero namái hai %1 disponible pero namái hai %1 disponibles la biblioteca namái garra 1 argumentu, non %1. 