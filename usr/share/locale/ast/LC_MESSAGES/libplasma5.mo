��    K      t  e   �      `     a  (   m  +   �  ,   �     �          %     3  $   B  &   g     �     �  	   �     �  	   �     �  &   �  
             +  	   =     G     P     c     y     �     �     �     �     �     �  
   �     �     �     �     �     �  
   		     	  
    	  	   +	     5	     8	     H	     U	  Z   q	     �	     �	     �	     �	     
     
     #
     A
  
   N
     Y
     n
     |
     �
     �
  (   �
  !   �
     �
     �
     �
  	          	   *     4     C     K     ]     m       �  �     <     J     X     i     x     �     �     �     �  
   �     �     �       	     
        (  /   G     w     �     �  
   �     �     �  )   �          !  	   5     ?  	   E     O     f     o     }     �     �     �  
   �  
   �     �     �     �     
          &      3  3   T     �     �     �  
   �     �     �     �     �     �          )     <     U     [  1   c     �     �     �     �  	   �  "   �               -     5     M     d  
   u                7                 B   @          H      )   F       2   =       '       8       5   +   
                    -   G           !          /   J                        $   0       3   K         <   	   6          &   #                              >   ?   I   :   D      .   ,      (   4       ;                  1   A   %      9            E   "   *          C    %1 Settings %1 is the name of the applet%1 Settings %1 is the name of the applet%1 Settings... %1 is the name of the containment%1 Options A desktop has been removed. A panel has been removed. Accessibility Add Widgets... Agenda listview section titleEvents Agenda listview section titleHolidays Alternatives... Application Launchers Astronomy Cancel Clipboard Configuration Definitions Could not find requested component: %1 Data Files Desktop Removed Development Tools Education Examples Executable Scripts Fetching file type... File System Fun and Games Graphics Icon Images Images for dialogs Install KRunner UI Language Login Manager Logout Dialog Mapping Miscellaneous Multimedia Next Decade Next Month Next Year OK Online Services Open with %1 Package Installation Failed Package file, name of the widgetCould not open the %1 package required for the %2 widget. Panel Removed Plasma Package Previous Decade Previous Month Previous Year Productivity Reset calendar to todayToday Screenlocker Screenshot Service Descriptions Splash Screen System Information Tasks Tests The package you just dropped is invalid. The widget "%1" has been removed. Translations Undo User Interface Utilities Virtual Desktop Switcher Wallpaper Widget Removed Widgets Window Decoration Window Switcher Windows and Tasks misc categoryMiscellaneous Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-10 02:58+0100
PO-Revision-Date: 2016-12-29 16:15+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Axustes de %1 Axustes de %1 Axustes de %1... Opciones de %1 Desanició un escritoriu Desanicióse un panel. Accesibilidá Amestar widgets... Eventos Vacaciones Alternatives... Llanzadores d'aplicaciones Astornomía Encaboxar Cartafueyu Definiciones de configuración Nun pudo alcontrase'l componente solicitáu: %1 Ficheros de datos Escritoriu desaniciáu Ferramientes de desendolcu Educación Exemplos Scripts executables Diendo en cata de la triba del ficheru... Sistema de ficheros Diversión y xuegos Gráficos Iconu Imáxenes Imáxenes pa diálogos Instalar IU de KRunner Llingua Xestor d'anicios de sesión Diálogu de zarru de sesión Mapéu Miscelaina Multimedia Década siguiente Mes siguiente Añu siguiente Aceutar Servicios en llinia Abrir con %1 Instalación fallida del paquete Nun pudo abrise'l paquete %1 riquíu pal widget %2. Panel desaniciáu Paquete Plasma Década previa Mes previu Añu previu Productividá Güei Bloquiador de pantalla Captura de pantalla Descripciones de servicios Pantalla de chisca Información del sistema Xeres Pruebes Nun ye válidu'l paquete que tas acabante soltar. Desanicióse'l widget «%1» Traducciones Desfacer Interfaz d'usuariu Utilidaes Conmutador d'escritorios virtuales Fondu de pantalla Widget desaniciáu Widgets Decoración de ventanes Conmutador de ventanes Ventanes y xeres Miscelaina 