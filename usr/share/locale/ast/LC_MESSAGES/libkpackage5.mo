��    E      D  a   l      �     �               !     1     A     S     e  
   s     ~  	   �     �  )   �  +   �  !     G   0  )   x      �  >   �       
   "     -  )   ;     e  	   w     �  &   �  $   �  "   �  &   	     /	     8	  (   K	     t	     �	     �	     �	  H   �	     �	     �	     	
     
     &
     F
     N
  
   \
     g
  >   �
     �
  2   �
  %   	  2   /     b     g  !   t     �     �     �     �     �       L   "     o     |     �  	   �     �     �  �  �          �     �     �     �     �     �     �     �          &     2  )   Q  5   {  %   �  X   �  (   0  +   Y  K   �  (   �     �       1        J  
   e     p  >   �  '   �  )   �  *        <     E  *   Y     �     �  	   �  	   �  O   �                3     G  &   O     v  
   }  
   �  -   �  @   �       H     8   _  <   �     �     �  #   �       %         F     _     {     �  M   �     �  #        *  	   =     G     X            ,       $       4   %   0                   ?      B   (   :   2                     	   +       9            *          ;   .              E   >             /       3       <      8      #      
       C      '   D   =          5           -         A      &      7   @                      !                            6       )   "       1              Name : %1       Path : %1     Author : %1     Plugin : %1    Comment : %1 %1 already exists %1 does not exist Accessibility Addon Name Application Launchers Astronomy Configuration Definitions Could not copy package to destination: %1 Could not create package root directory: %1 Could not delete package from: %1 Could not load installer for package of type %1. Error reported was: %2 Could not move package to destination: %1 Could not open metadata file: %1 Could not open package file, unsupported archive format: %1 %2 Could not open package file: %1 Data Files Date and Time Desktop file that describes this package. Development Tools Education Environment and Weather Error: Can't find plugin metadata: %1
 Error: Installation of %1 failed: %2 Error: Plugin %1 is not installed. Error: Uninstallation of %1 failed: %2 Examples Executable Scripts Failed to generate a Package hash for %1 File System Fun and Games Graphics Images Impossible to remove the old installation of %1 located at %2. error: %3 KPackage Manager KPackage/Generic KPackage/GenericQML Language Listing service types: %1 in %2 Mapping Miscellaneous Multimedia No metadata file in package: %1 Not installing version %1 of %2. Version %3 already installed. Online Services Package plugin name %1 contains invalid characters Package plugin name not specified: %1 Package types that are installable with this tool: Path Productivity SHA1 hash for Package at %1: '%2' Service Type Showing info for package: %1 Successfully installed %1 Successfully uninstalled %1 Successfully upgraded %1 System Information The new package has a different type from the old version already installed. Translations Upgrading package from file: %1 User Interface Utilities Windows and Tasks package typewallpaper Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-15 03:10+0100
PO-Revision-Date: 2016-12-30 01:01+0100
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <alministradores@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
       Nome: %1       Camín: %1     Autor: %1     Complementu: %1    Comentariu: %1 %1 yá esiste %1 nun esiste Accesibilidá Nome d'add-on Llanzadores d'aplicaciones Astornomía Definiciones de configuración Nun pudo copiase'l paquete al destín: %1 Nun pudo crease'l direutoriu raigañu del paquete: %1 Nun pudo desaniciase'l paquete de: %1 Nun pudo cargase l'instalador pal paquete de la triba «%1». El fallu informáu foi: %2 Nun pudo movese'l paquete al destín: %1 Nun pudo abrise'l ficheru de datos meta: %1 Nun pue abrise'l ficheru del paquete, formatu non sofitáu d'archivu: %1 %2 Nun pue abrise'l ficheru del paquete: %1 Ficheros de datos Data y hora Ficheru d'escritoriu que describe a esti paquete. Ferramientes de desendolcu Educación Entornu y clima Fallu: Nun puen alcontrase los datos meta del complementu: %1
 Fallu: Falló la instalación de %1: %2 Fallu: Nun ta instaláu'l complementu %1. Fallu: Falló la desinstalación de %1: %2 Exemplos Scripts executables Fallu al xenerar un hash del paquete pa %1 Sistema de ficheros Diversión y xuegos Gráficos Imáxenes Nun ye posible desaniciar la instalación vieya de %1 allugáu en %2. Fallu: %3 Xestor KPackage KPackage/Xenérico KPackage/GenericQML Llingua Llistando tribes de serviciu: %1 en %2 Mapéu Miscelaina Multimedia Nun hai ficheru de datos meta nel paquete: %1 Nun ta instalándose la versión %1 de %2. Yá s'instaló la %3. Servicios en llinia El nome del complementu del paquete %1 contién caráuteres non válidos Nun s'especificó'l nome del complementu del paquete: %1 Tribes de paquetes que son instalables con esta ferramienta: Camín Productividá Hash SHA1 pal paquete en %1: «%2» Triba de serviciu Amosando información pal paquete: %1 Instalóse con ésitu %1 Desinstalóse con ésitu %1 Anovóse con ésitu %1 Información del sistema El paquete nuevu tien una triba diferente de la versión vieya yá instalada. Traducciones Anovando paquete dende'l ficheru %1 Interfaz d'usuariu Utilidaes Ventanes y xeres fondu de pantalla 