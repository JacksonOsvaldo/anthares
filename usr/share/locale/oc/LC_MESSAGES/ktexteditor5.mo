��    r      �  �   <      �	  
   �	     �	     �	     �	  
   �	     �	     �	     �	     �	     
     
     
  	   
     
  
   $
     /
      5
  $   V
  "   {
      �
  #   �
  !   �
  !     !   '  /   I  #   y  ,   �     �  !   �  H        U     \     e  
   l  	   w     �     �  	   �     �     �     �     �     �     �     �     �       	                	   ,     6     <      E     f     n     {  	   �     �     �  	   �     �     �     �     �  	   �     �     �  	   �  	   �     �  
                  =     B  	   H     R     V     Y     i     p  
   x     �  	   �     �  	   �     �     �  	   �     �     �     �               #     *     9     A     Z     ^  V   g     �     �  	   �     �  	   �     �     �                (     *  �  /  	     	             $     -  	   ;     E  
   N     Y     `  
   i     t     �  
   �  
   �  
   �     �  	   �  	   �     �     �     �     �     �  	   �     �     �            G      
   h  	   s     }  
   �  	   �     �     �     �     �  
   �     �     �     �     �     �            
        #     ,     8     E  
   L     W     l  "   y     �  	   �     �     �     �     �     �     �     �  	   �     �       
   
  	          
   <     G     W     s     w  
   }     �     �     �     �     �  
   �     �     �     �     �     �            	   #  $   -  "   R  %   u      �     �     �     �     �     �     �  @   �     5     ;  
   D     O  	   X     b     p     x     �     �     �            b       r       Z   6       ,   7   	   V   2       D      ;                                k       <       L   P   .   a   >   S       e      `       n   %      *       \   h   -          M      W   9   #       ?       C               _   Q   )      H      /       ]   1         i       0   '   d   U      T   :          m           I               q                         $   c   8   3       G   R   f       F      [   A         X       "   &      5          o   
   g       E              +   O       K      (   p   !   j   l   Y          =       J      B   4   N             ^   @    &Bookmarks &Delete &Description: &Edit &Encoding: &File &Ignore &Install &Name: &New &New... &Off &Settings &Tools &Underline &View @item:intable Text contextAlert @item:intable Text contextCharacter @item:intable Text contextComment @item:intable Text contextError @item:intable Text contextFunction @item:intable Text contextNormal @item:intable Text contextOthers @item:intable Text contextString @title:column Meaning of text in editorContext @title:column Text styleBackground @title:column Text styleBackground Selected @title:column Text styleNormal @title:column Text styleSelected A file named "%1" already exists. Are you sure you want to overwrite it? Add... Advanced Always Appearance Arguments Autoindent modeNone Autoindent modeNormal Backspace Bookmark Borders Carsten Pfeiffer Class Close Document Colors Colors: Columns Command Configure Constant Description Developer Digit Disabled EMAIL OF TRANSLATORSYour emails Editing Enlarge Font Error Execution Font Function Functions General Go Hamish Rodda Icon Installed Joseph Wenninger Latest Lowercase Macintosh Main Toolbar Maintainer Matthew Woehlke NAME OF TRANSLATORSYour names Name Name: Not found Off Or Overwrite File? Prefix Private Properties Properties of %1 Protected Public Reference Replace &All Reverse Save File Scope Select Character Left Select Character Right Select to End of Line Shrink Font Signal Simon Hausmann Sorting Syntax highlightingNone Tab Template The document "%1" has been modified.
Do you want to save your changes or discard them? Union Untitled Uppercase Variable Variables Waldo Bastian Warning What do you want to do? Word Completion ^ text Project-Id-Version: katepart4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-31 03:12+0100
PO-Revision-Date: 2008-08-05 22:27+0200
Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>
Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: KBabel 1.11.4
 &Favorits &Suprimir &Descripcion : &Edicion &Encodatge : &Fichièr &Ignorar &Installar &Nom : &Novèl  &Novèl... &Desactivat &Paramètres &Espleches &Soslinhar Aficha&tge Alèrta Caractèr Comentari Error Foncion Normal Autres Cadena Contèxte Fons Fons seleccionat Normal Seleccionat Un fichièr «%1» existís ja. Lo volètz vertadièrament l'espotir ? Apondre... A_vançat Totjorn Aparéncia Arguments Pas cap Normal Retorn enrèire Favorit Bordaduras Carsten Pfeiffer Classa Tampar lo document Colors Colors : Colomnas Comanda Configurar Contrast Descripcion Desvolopaire Chifra Desactivat yannig@marchegay.org Modificacion Augmentar la talha de las poliças Error Execucion Poliça Foncion Foncions General Anar Hamish Rodda Icòna Installat Joseph Wenninger Darrièr Minusculas Macintosh Barra d'espleches principala Manteneire Matthew Woehlke Yannig Marchegay (Kokoyaya) Nom Nom : Pas trobat Inactiu O Remplaçar lo fichièr ? Prefixe Privat Propietats Propietat de %1 Protegit Public Preferéncias &Remplaçar tot mètres Enregistrar lo fichièr Espandida Seleccionar lo caractèr d'esquèrra Seleccionar lo caractèr de drecha Seleccionar fins a la fin de la linha Redusir la talha de las poliças Senhal Simon Hausmann A s'ordenar Pas cap Onglet Modèl Lo document «%1» es estat modificat.
Lo volètz enregistrar ? Union Sens nom Majusculas Variable Variables Waldo Bastian Alèrta De que volètz faire ? Completatge de mots ^ tèxt 