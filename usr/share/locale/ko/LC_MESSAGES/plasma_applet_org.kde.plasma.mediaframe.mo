��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �     �     �               $     7     I  2   P     �     �     �  (   �      �      �     
  	   "  /   ,  $   \  *   �  ?   �  2   �  ;   	  ;   [	     �	     �	     �	     �	                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-12-02 23:40+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 파일 추가... 폴더 추가... 배경 프레임 그림 변경 주기 폴더 선택 파일 선택 Plasmoid 설정... 채우기 모드: 일반 왼쪽 단추로 눌러서 외부 뷰어로 열기 둘러싸기 경로 경로: 마우스가 지나갈 때 일시 정지 비율 유지하면서 자르기 비율 유지하면서 늘이기 무작위 순서 사용 늘이기 그림을 수평 및 수직으로 반복 배열 그림 크기를 조정하지 않음 그림 크기를 조정하여 늘입니다 필요한 경우 그림을 잘라내는 형태로 크기 조정 그림을 자르지 않고 맞도록 크기 조정 그림을 수평으로 늘이고 수직으로 반복 배열 그림을 수직으로 늘이고 수평으로 반복 배열 바둑판식 배열 수평 바둑판식 배열 수직 바둑판식 배열 초 