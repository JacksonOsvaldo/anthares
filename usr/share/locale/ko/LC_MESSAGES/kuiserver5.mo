��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �     �     �                     '  	   .  	   8     B  5   S     �  1   �     �     �     �       $      .   E  !   t  +   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: kuiserver
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-07-04 21:09+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <cho.sungjae@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 파일 %1개 폴더 %1개 %2 중 %1 처리됨 %3/초로 %2 중 %1 처리됨 %1 진행됨 %2/초로 %1 처리 중 모양 행동 취소 비우기 설정... 완료된 작업 진행 중인 파일 전송/작업 목록 (kuiserver) 다른 목록으로 옮기기 다른 목록으로 그것들을 이동합니다. 일시 정지 삭제하기 그것들을 삭제합니다. 다시 시작 모든 작업을 목록으로 보기 모든 작업을 목록으로 표시합니다. 모든 작업을 트리로 보기 모든 작업을 트리로 표시합니다. 창 나누어서 보기 창을 나누어서 봅니다. 