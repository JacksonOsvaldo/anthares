��    $      <  5   \      0      1     R     ^  -   }  #   �  #   �  ?   �  1   3     e     y  H   ~  L   �       V     N   s     �  
   �     �     �     �       2     
   P     [  )   {  8   �  #   �  .     -   1  D   _  6   �  0   �  A     =   N  9   �  �  �     U
     b
     o
  )   �
  	   �
  	   �
     �
     �
     �
               0     G  	   U     _     c     j     x     �     �  	   �     �     �     �  +   �     $  %   2  %   X  	   ~  	   �     �     �     �  
   �     �                                                
       "                                       $                                     #      	           !                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-12-02 23:41+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 알림 %1개 %2 %3 중 %1 실행 중인 작업 %1개 이벤트 알림 및 동작 설정(&C)... 10초 전 30초 전 자세한 정보 표시 자세한 정보 숨기기 알림 비우기 복사 디렉터리 %1개 중 %2개 파일 %1개 중 %2개 과거 기록 %2 중 %1 +%1 정보 작업 실패 작업 완료됨 새 알림 없음 알림과 작업 없음 열기... %1(일시 정지됨) 모두 선택 알림 기록 보이기 프로그램 및 시스템 알림 보이기 %1(%2 남음) 파일 전송과 다른 작업 추적 알림 팝업 위치 사용자 정의 %1분 전 %1일 전 어제 지금 %1: %1: 실패 %1: 완료됨 