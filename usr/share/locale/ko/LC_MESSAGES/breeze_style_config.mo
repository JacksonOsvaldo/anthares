��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �  	   '     1  #   Q  $   u  $   �     �     �  &   �       "      '   C  &   k  @   �  J   �  $   	  5   C	  .   y	  ,   �	     �	  (   �	     
  +   9
  	   e
     o
     v
     �
     �
  +   �
     �
     �
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-12-02 22:56+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 밀리초 키보드 가속기 표시(&K): 위쪽 화살표 단추 종류(&T): 항상 키보드 가속기 숨기기 항상 키보드 가속기 보이기 애니메이션 시간(&T): 애니메이션 아래쪽 화살표 단추 종류(&Y): Breeze 설정 탭 표시줄 탭 가운데 정렬 모든 빈 영역으로 드래그하기 제목 표시줄로만 드래그하기 제목 및 메뉴 표시줄, 도구 모음으로 드래그하기 메뉴 및 메뉴 표시줄에 초점을 나타내는 얇은 선 그리기 목록에 초점 표시기 그리기 도킹 가능한 패널 주위로 프레임 그리기 페이지 제목 주위로 프레임 그리기 측면 패널 주위로 프레임 그 리기 슬라이더 눈금 그리기 도구 모음 항목 구분자 그리기 애니메이션 사용하기 확장된 크기 조절 핸들 사용하기 프레임 일반 단추 없음 단추 하나 스크롤바 필요할 때 키보드 가속기 보이기 단추 두개 창 드래그 모드(&I): 