��    *      l  ;   �      �     �     �     �  
   D  +   O  
   {     �     �     �      �     �     �       	           �   @  e   �  *   D  H   o     �     �     �     �       )     ;   <  	   x     �  (   �     �     �     �          7     L     c  	   ~     �  #   �     �     �  �  �  
   �
     �
  m   �
     ,  :   =     x     �  #   �     �      �     �           '     4     C  �   T  t     /   �  U   �               &     6     B  2   Y  N   �  	   �  -   �  ;     #   O      s      �  #   �     �      �           5  '   <  1   d  '   �  1   �        
          *   &      #      '         $                               (            %                       	       !                            )              "                                          msec &Number of desktops: <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Animation: Assigned global Shortcut "%1" to Desktop %2 Desktop %1 Desktop %1: Desktop Effect Animation Desktop Names Desktop Switch On-Screen Display Desktop Switching Desktop navigation wraps around Desktops Duration: EMAIL OF TRANSLATORSYour emails Enable this option if you want keyboard or active desktop border navigation beyond the edge of a desktop to take you to the opposite edge of the new desktop. Enabling this option will show a small preview of the desktop layout indicating the selected desktop. Here you can enter the name for desktop %1 Here you can set how many virtual desktops you want on your KDE desktop. KWin development team Layout N&umber of rows: NAME OF TRANSLATORSYour names No Animation No suitable Shortcut for Desktop %1 found Shortcut conflict: Could not set Shortcut %1 for Desktop %2 Shortcuts Show desktop layout indicators Show shortcuts for all possible desktops Switch One Desktop Down Switch One Desktop Up Switch One Desktop to the Left Switch One Desktop to the Right Switch to Desktop %1 Switch to Next Desktop Switch to Previous Desktop Switching Walk Through Desktop List Walk Through Desktop List (Reverse) Walk Through Desktops Walk Through Desktops (Reverse) Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2014-08-23 23:34+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.5
  밀리초 데스크톱 개수(&N): <h1>다중 데스크톱</h1> 이 모듈에서는 데스크톱 개수와 이름을 설정할 수 있습니다. 애니메이션: 전역 단축키 "%1"을(를) 데스크톱 %2에 할당함 데스크톱 %1 데스크톱 %1: 데스크톱 전환 애니메이션 데스크톱 이름 데스크톱 전환 표시하기 데스크톱 전환 데스크톱 탐색 둘러싸기 데스크톱 표시 시간: kde@peremen.name 이 옵션을 사용하면 키보드나 경계선으로 데스크톱을 탐색할 때 데스크톱 경계선을 넘어서 탐색하면 새 데스크톱의 반대편 경계선으로 갑니다. 이 옵션을 사용하면 선택한 데스크톱을 포함한 데스크톱 레이아웃을 미리 보여 줍니다. 데스크톱 %1의 이름을 입력하십시오 KDE 데스크톱에서 사용할 가상 데스크톱의 개수를 설정하십시오. KWin 개발 팀 배치 줄 개수(&U): Shinjo Park 애니메이션 없음 데스크톱 %1을(를) 위한 단축키가 없음 단축키 충돌: 데스크톱 %2에 단축키 %1을(를) 할당할 수 없음 단축키 데스크톱 레이아웃 표시기 보이기 모든 데스크톱에 사용 가능한 단축키 보이기 아래쪽 데스크톱으로 전환 위쪽 데스크톱으로 전환 왼쪽 데스크톱으로 전환 오른쪽 데스크톱으로 전환 데스크톱 %1로 전환 다음 데스크톱으로 전환 이전 데스크톱으로 전환 전환 데스크톱 목록을 옮겨 다니기 데스크톱 목록을 거꾸로 옮겨 다니기 데스크톱 사이를 옮겨 다니기 데스크톱 사이를 거꾸로 옮겨 다니기 