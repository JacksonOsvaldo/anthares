��    K      t  e   �      `     a  ^   f  H   �          !     -     D     L  '   [     �  "   �     �     �     �  
   �     �          %  	   .     8     P     f     l          �  "   �     �     �  	   �     �     �  ?   	     D	     Q	     W	     e	     q	     �	     �	  ;   �	  &   �	      
  %   %
     K
     e
     
     �
  >   �
     �
       '   &  !   N     p     �     �     �     �     �     �     �          "     3     E     X     k     }     �     �     �     �     �     �  3     �  G     �     �     �     �  
   �     	  	     
        !     '     ;  
   Q     \     n     ~     �     �  	   �     �  
   �     �     �     �             ,        C     J     L     T     [     ]     t     |     �     �     �  	   �     �     �     �     �     �       	     	     	   &     0     B     P     U     g     y     �  	   �  	   �     �     �  	   �  	   �     �     �     �  	   �  	   �     �     �     �  	   �  	          	        $     =     '   I   8                            >   5   B           .   6   !      +       -   4       2         "       C         %       9       0   7   @      )         
   H       :         G              F   *      /   =               &                            D   #      A         K   ,       1      (   $          ?   J       <      3      	      ;       E                   min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Short for no data available- Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2016-10-20 22:44+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
  분 %1 %2 %1 (%2) 부퍼트 단위 bft 섭씨 °C ° 자세히 화씨 °F %1일 헥토파스칼 hPa 최고: %1 최저: %2 최고: %1 수은인치 inHg 절대 온도 K 킬로미터 시속 킬로미터 km/h 킬로파스칼 kPa 노트 kt 위치: 최저: %1 초속 미터 m/s 마일 시속 마일 mph 밀리바 mbar 없음 '%1'의 기상대를 찾을 수 없습니다 공지 % 기압: 찾기 - 설정해 주십시오 온도: 단위 업데이트 주기: 가시 거리: 날씨 스테이션 고요함 풍속: %1 (%2%) 습도: %1%2 가시 거리: %1 %2 이슬점: %1 Humidex: %1 저기압 고기압 일정함 기압 동향: %1 기압: %1 %2 %1%2 가시 거리: %1 발효된 경보: 발효된 주의보: 동 동북동 동남동 북 북동 북북동 북북서 북서 남 남동 남남동 남남서 남서 변함 서 서북서 서남서 %1 %2 %3 고요함 풍속 냉각 지수: %1 돌풍: %1 %2 