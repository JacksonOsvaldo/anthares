��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �  y  �     (     ?  '   S  O   {  O   �  X     m   t  I   �  W   ,	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     �	                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-11-07 14:17+0900
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <cho.sungjae@gmail.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 컨테이너 잠그기 미디어 꺼내기 이름이 :q:인 장치를 찾습니다 모든 장치를 표시하고 마운트/해제, 꺼낼 수 있도록 합니다. 꺼낼 수 있는 모든 장치를 표시하며 꺼낼 수 있도록 합니다. 마운트 가능한 모든 장치를 표시하며, 마운트할 수 있도록 합니다. 마운트를 해제할 수 있는 모든 장치를 표시하며, 마운트 해제할 수 있도록 합니다. 잠글 수 있는 모든 장치를 표시하며, 잠글 수 있습니다. 잠금을 풀 수 있는 모든 장치를 표시하며, 잠금을 풀 수 있습니다. 장치 마운트하기 장치 꺼내기 잠금 마운트 잠금 해제 마운트 해제 컨테이너 잠금 풀기 장치 마운트 해제하기 