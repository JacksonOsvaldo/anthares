��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j          #     1     8     I     a     h     |     �     �     �     �     �     �     �     �  6   �     -     =     P  !   _  	   �     �     �  X   �  ?   
     J     X     p     ~     �     �     �     �     �     �               #     1     L     c  4   t  #   �     �  -   �  	     	             3  7   <  0   t     �     �     �     �  
          
   !     ,     E         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: kwin_clients
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2016-02-14 14:49+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 일치하는 창 속성(&M):  매우 크게 크게 경계선 없음 옆쪽 경계선 없음 일반 과다하게 크게 작게 매우 매우 매우 크게 매우 매우 크게 크게 일반 작게 매우 크게 활성 창 반짝이기 추가 경계선이 없는 창에 크기 조절 핸들 추가 애니메이션 단추 크기(&U): 경계 크기: 단추 마우스 지나감 전환 가운데 가운데 (전체 너비) 클래스:  창 활성 상태가 바뀌었을 때 창 그림자와 반짝임 페이드 효과 설정 단추 위로 마우스가 지나갈 때 애니메이션 설정 장식 설정 창 속성 가져오기 대화 상자 편집 예외 편집 - Oxygen 설정 이 예외 활성/비활성화 예외 종류 일반 창 제목 표시줄 숨기기 선택한 창 정보 왼쪽 아래로 이동 위로 이동 새 예외 - Oxygen 설정 질문 - Oxygen 설정 정규 표현식 정규 표현식 문법이 올바르지 않습니다 일치하는 정규 표현식(&T):  삭제 선택한 예외를 삭제하시겠습니까? 오른쪽 그림자 제목 표시줄 정렬(&L): 제목:  창 제목 표시줄과 창 내용에 같은 색 사용 창 클래스 사용하기 (전체 프로그램) 창 제목 사용하기 경고 - Oxygen 설정 창 클래스 이름 창 드롭다운 그림자 창 식별 창 속성 선택 창 제목 창 활성 상태 전환 창 지정 설정 