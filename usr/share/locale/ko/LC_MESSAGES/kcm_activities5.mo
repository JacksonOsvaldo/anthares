��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     W	     o	     �	     �	     �	  	   �	  	   �	     �	     �	     �	     �	     �	     
  &   
  (   9
     b
     w
     �
  `   �
  $   �
          1     ?     X     q  	   x     �     �  '   �     �     �  ?   �       E   5  (   {  	   �     �     �     �     �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-10-20 23:10+0200
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 기억하지 않음(&D) 활동간 전환 활동간 전환(거꾸로) 적용 취소 변경... 만들기 활동 설정 새 활동 만들기 활동 삭제 활동 활동 정보 활동 전환 '%1'을(를) 삭제하시겠습니까? 이 목록에 없는 프로그램 차단 최근 기록 삭제 활동 만들기... 설명: QML 파일을 불러올 수 없습니다. 설치 상태를 확인하십시오.
%1이(가) 없음 모든 프로그램에 대해서(&L) 마지막 하루 삭제 모두 삭제 마지막 1시간 삭제 마지막 2시간 삭제 일반 아이콘 과거 기록 저장 이름: 특정한 프로그램에 대해서(&N) 기타 개인 정보 비밀 활동 - 이 활동의 사용 내역 기록하지 않기 열었던 문서 기억: 각 활동별 현재 가상 데스크톱 기억(다시 시작 필요) 이 활동으로 전환하는 단축키: 단축키 전환 배경 그림 기한: 개월 영구 