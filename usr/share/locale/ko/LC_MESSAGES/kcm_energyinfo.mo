��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	  	   �	     �	     �	     �	  
   �	     �	     �	     �	  
   �	     
  	   
     #
     4
     L
     S
     a
     o
     �
     �
     �
     �
     �
     �
     �
     	  	        %     ,  	   8     B     V  
   ^     i     w     �     �  	   �     �  =   �     �  !   �  	                    '     H     J     M     Q     b     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-10-20 20:21+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 % %1%2 %1: 프로그램 에너지 소모 배터리 용량 충전 비율 충전 상태 충전 중 전류 °C 자세히: %1 방전 중 kde@peremen.name 에너지 에너지 소모 에너지 소모 통계 환경 설계 용량 충전 완료 전원 공급 장치가 있음 Kai Uwe Broulik 마지막 12시간 마지막 2시간 마지막 24시간 마지막 48시간 마지막 7일 마지막 충전 마지막 1시간 제조사 모델 Shinjo Park 아니요 충전하지 않음 PID: %1 경로: %1 충전 가능 새로 고침 일련 번호 W 시스템 온도 이 장치에서 다음 기록을 사용할 수 없습니다. 시간 범위 표시할 데이터 시간 범위 제조사 V 전압 초당 깨어난 횟수: %1(%2%) W Wh 예 에너지 소모 % 