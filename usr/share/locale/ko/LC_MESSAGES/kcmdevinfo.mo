��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  "   �     �  	   �     �        	   $     .     @  	   H  	   R     \  
   k     v  	   �     �     �  1   �     �     �  
   �     �               !     2     K     _     |     �  	   �  5   �  	   �  	   �  
   �  
   �  
   �  	   	       
   *     5     D     [  	   m     w     �  	   �     �     �     �     �     �               #     3     C     X     e     m     r     �     �     �     �     �     �     �     �          7     R     X     \     `  @   f     �     �  
   �     �     �       
             %     ,  	   3     =     L     [     j     y  	   �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-10-20 21:00+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 
Solid 기반 장치 뷰어 모듈 (c) 2010 David Hubner AMD 3DNow ATI IVEC %2 중 %1 남음 (%3% 사용함) 배터리 배터리 종류: 버스: 카메라 카메라 충전 상태: 충전 중 모두 접기 CF 리더 장치 장치 정보 목록에 있는 모든 장치를 표시합니다 장치 뷰어 장치 방전 중 kde@peremen.name 암호화됨 모두 펴기 파일 시스템 파일 시스템 종류: 완전히 충전됨 하드디스크 드라이브 핫플러그 가능 IDE IEEE 1394 현재 선택한 장치의 정보를 표시합니다. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 키보드 키보드와 마우스 레이블: 최대 속도: 메모리스틱 리더 마운트 위치: 마우스 멀티미디어 재생기 Shinjo Park 아니오 충전하지 않음 데이터 없음 마운트되지 않음 설정하지 않음 광학 드라이브 PDA 파티션 테이블 프라이머리 프로세서 %1 프로세서 개수: 프로세서 제품: RAID 제거 가능 SATA SCSI SD/MMC 리더 모든 장치 보이기 관련된 장치 보이기 스마트 미디어 리더 저장소 드라이브 지원하는 드라이버: 지원하는 명령어 집합: 지원하는 프로토콜: UDI:  UPS USB UUID: 현재 장치의 UDI(고유 장치 식별자)를 표시합니다 알 수 없는 드라이브 사용하지 않음 제조사: 볼륨 빈 공간: 볼륨 사용률: 예 kcmdevinfo 알 수 없음 없음 없음 플랫폼 알 수 없음 알 수 없음 알 수 없음 알 수 없음 알 수 없음 xD 리더 