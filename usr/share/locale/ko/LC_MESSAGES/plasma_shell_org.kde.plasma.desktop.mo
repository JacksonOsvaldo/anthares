��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
          $     2     @     Q     U     c     q     x     �  
   �     �     �     �     �     �  	   �     �     �     �     �          !     &     8  
   ?     J     b     z     �     �     �  L   �  V        [     i     p     ~     �     �     �     �     �     �     �     �     �  	   
          %  	   3     =     C     Q     c  ~   j  �   �     �     �     �     �     �     �     �               !     (     A     +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-12-02 23:45+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 활동 동작 추가 공백 추가 위젯 추가... Alt 대안 위젯 항상 보임 적용 설정 적용하기 지금 적용하기 작성자: 자동 숨기기 뒤로 가기단추 아래 취소 분류 가운데 닫기 + 설정 활동 설정 활동 만들기... Ctrl 현재 사용 중 삭제 이메일: 앞으로 가기 단추 새 위젯 가져오기 높이 수평 스크롤 여기에 입력 키보드 단축키 위젯이 잠겨 있을 때에는 레이아웃을 변경할 수 없습니다 다른 항목을 변경하기 전 레이아웃 변경 사항을 적용해야 합니다 레이아웃: 왼쪽 왼쪽 단추 라이선스: 위젯 잠금 패널 최대화 Meta 가운데 단추 더 많은 설정... 마우스 동작 확인 패널 정렬 패널 삭제 오른쪽 오른쪽 단추 화면 경계 찾기... Shift 활동 정지 정지된 활동: 전환 현재 모듈에 저장되지 않은 변경 사항이 있습니다. 변경 사항을 적용하거나 무시하시겠습니까? 이 단축키는 애플릿을 활성화합니다: 애플릿에 키보드 초점을 맞추며, 애플릿에 팝업 내용이 있는 경우(예: 시작 메뉴) 해당 팝업을 표시합니다. 위 삭제 실행 취소 삭제 위젯 삭제 수직 스크롤 표시 여부 배경 그림 배경 그림 종류: 위젯 너비 창이 덮을 수 있음 창이 내려감 