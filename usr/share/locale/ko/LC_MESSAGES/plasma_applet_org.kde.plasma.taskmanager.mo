��    O      �  k         �     �     �     �     �     �     �     �  .   �  U   (     ~     �      �     �  /   �  /        5     A     J  
   V     a     q  $   �     �     �     �     �     �     �  	   	     	  
   	     )	     <	     O	     g	  	   m	     w	  !   �	     �	     �	  	   �	      �	     �	     �	     
     $
     5
     :
     G
     M
     _
     w
  (   �
     �
  =   �
            "   5     X     s  J   {  1   �  )   �  (   "  '   K  "   s  4   �     �     �     �     �     �          "  U   8  N   �  9   �  J     �  b       
        '  
   9     D  
   Y     d  
   y     �     �     �  '   �     �  $     !   (     J     Q     X     e     x     �     �     �     �     �     �     
          )     1     B     T     c     u     �     �     �  -   �     �     �       !        2  $   D     i  $   �  	   �     �     �     �     �     �  /        @     [     b     p     ~  	   �     �     �     �  +   �  1   �  +   (      T  3   u     �     �     �     �     �     �     �  
                  ,     %   N         @   A       >   +   "       ,       F   /   B   O           9      ;      $                      G          3          K         ?                        2   L   *      !      H   =                   <              '               :       -            .   I   J   6   C           M       1          #   0      4       7       (   	   
           8               D   5   )   &   E    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: plasma_applet_tasks
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-12-02 23:42+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 모든 데스크톱(&A) 닫기(&C) 전체 화면(&F) 이동(&M) 새 데스크톱(&N) 고정(&P) 말아 올리기(&S) %1 %2(&%1) 활동 %1에도 있음 현재 활동에 추가하기 모든 활동 이 프로그램을 그룹으로 묶기 가나다 순으로 창을 항상 행으로 정렬하기 창을 항상 열로 정렬하기 배열 행동 활동별로 데스크톱별로 프로그램 이름으로 창이나 그룹 닫기 마우스 휠로 작업 전환 묶지 않기 정렬하지 않기 필터 최근 문서 비우기 일반 묶기 및 정렬하기 묶기: 창 강조하기 아이콘 크기: 항상 위(&A) 항상 아래(&B) 실행기 분리하기 크게 최대화(&X) 수동으로 오디오를 재생하는 프로그램 표시 최대 행 개수: 최대 열 개수: 최소화(&N) 창이나 그룹 최소화/복원 더 많은 동작 현재 데스크톱으로 이동(&T) 다음 활동으로 이동(&A) 다음 데스크톱으로 이동(&D) 음소거 새 인스턴스 %1에 있음 모든 활동에 현재 활동에 가운데 단추 누름: 작업 표시줄이 가득 찼을 때만 묶기 팝업으로 그룹 열기 복원 일시 정지 다음 트랙 이전 트랙 끝내기 크기 조정(&S) 고정 해제 추가 위치 %1개 현재 활동에 있는 작업만 보이기 현재 데스크톱에 있는 작업만 보이기 현재 화면에 있는 작업만 보이기 최소화된 작업만 보이기 작업 단추에 진행 및 상태 정보 보이기 풍선 도움말 보이기 작게 정렬: 새 인스턴스 시작하기 재생 정지 없음 고정(&P) 묶기/풀기 %1에 있음 모든 활동에 있음 