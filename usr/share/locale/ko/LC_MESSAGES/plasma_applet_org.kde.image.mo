��            )   �      �     �     �     �     �     �     �     �     �  0        3     G     ]     c     o     w     �  
   �     �     �     �     �     �               (     A     I     a     m  �  s       %   !     G     X     i  	   q  	   {     �  B   �     �  !   �          &     4     8     T     o     }     �     �     �     �     �     �          -     1     B     I                                            
                                                                                      	       %1 by %2 Add Custom Wallpaper Add Folder... Add Image... Background: Blur Centered Change every: Directory with the wallpaper to show slides from Download Wallpapers Get New Wallpapers... Hours Image Files Minutes Next Wallpaper Image Open Containing Folder Open Image Open Wallpaper Image Positioning: Recommended wallpaper file Remove wallpaper Restore wallpaper Scaled Scaled and Cropped Scaled, Keep Proportions Seconds Select Background Color Solid color Tiled Project-Id-Version: plasma_wallpaper_image
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:39+0100
PO-Revision-Date: 2017-12-02 23:35+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 %2의 %1 사용자 정의 배경 그림 추가 폴더 추가... 그림 추가... 배경: 흐리게 가운데 그림 변경 주기: 슬라이드 쇼에 사용할 그림이 있는 디렉터리 선택 새 배경 그림 다운로드 새 배경 그림 가져오기... 시간 그림 파일 분 다음 배경 그림 파일 포함하는 폴더 열기 그림 열기 배경 그림 파일 열기 위치: 추천하는 배경 파일 배경 그림 삭제 배경 그림 복원 크기 조정 크기 조정 후 자르기 크기 조정, 비율 유지 초 배경색 선택 단색 바둑판식 배열 