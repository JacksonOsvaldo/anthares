��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �     �     �     �     �     �     �  	   �  	   �     	     	     )	     0	     7	     >	     V	     d	  	   s	     }	     �	  =   �	  /   �	     
     
     +
     B
     U
     m
     �
  
   �
     �
     �
     �
     �
     �
     �
        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-12-02 23:43+0100
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean <kde@peremen.name>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 % %1 음량 조정 프로그램 오디오 음소거됨 오디오 음량 행동 캡처 장치 캡처 스트림 음소거 기본값 마이크 음량 감소 음량 감소 장치 일반 포트 마이크 음량 증가 음량 증가 최대 음량: 음소거 %1 음소거 마이크 음소거 오디오를 재생하거나 녹음하는 프로그램 없음 입력이나 출력 장치를 찾을 수 없음 재생 장치 재생 스트림 (사용할 수 없음) (연결 해제됨) 최대 음량 높이기 %1의 추가 설정 표시 음량 음량 %1% 음량 피드백 음량 단계: %1 (%2) %1: %2 100% %1% 