��    @        Y         �     �     �     �  !   �  "   �  &   �  ,     #   <  &   `  !   �  &   �  '   �  "   �  "     '   >     f  +   j     �  
   �     �     �     �     �     �     �     �          (  !   /     Q     q      v     �     �     �     �     �  !   �     	  	   	     %	     -	     M	     h	  &   {	     �	     �	     �	     �	     �	     �	     �	     �	     
  $   
     <
     M
     g
     y
     �
     �
     �
  >   �
  �       �     �      �  	   �       
          	   -  	   7     A     I     Z     i     q     w     �  6   �     �  	   �     �     �               5     >     E     Z     r  -   z     �     �  %   �     �  !        *     7     C     W     s     z     �  "   �     �     �  )   �  "        6     B     a     h  
   n     y     �  	   �  '   �     �     �     �          $     ;     H     e     %       +      4      6         2   "       #   :   *      @                         <       !              	   >      ;   5                              )      
   $                  .   8      9           &                  0                      ?       1   '      7          (         /       -          ,   =   3         ms % &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Add Add handle to resize windows with no border Anima&tions duration: Animations B&utton size: Border size: Center Center (Full Width) Class:  Color: Decoration Options Detect Window Properties Dialog Draw a circle around close button Draw window background gradient Edit Edit Exception - Breeze Settings Enable animations Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Breeze Settings Question - Breeze Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Si&ze: Tiny Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Breeze Settings Window Class Name Window Identification Window Property Selection Window Title Window-Specific Overrides strength of the shadow (from transparent to opaque)S&trength: Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:20+0100
PO-Revision-Date: 2017-10-27 20:53+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
  ms % Zodpovedajúca vlastnosť okna:  Obrovská Veľké Bez okraja Bez bočného okraja Normálne Nadmerná Drobná Veľmi obrovská Veľmi veľká Veľké Malá Veľmi veľká Pridať Pridať spracovač na zmenu veľkosti okien bez okraja Trvanie animácií: Animácie Veľkosť tlačidiel: Veľkosť okraja: Vycentrovať Vycentrovať (plná šírka) Trieda:  Farba: Možnosti dekorácie Zistiť vlastnosti okna Dialóg Kresliť kružnicu okolo tlačidla zatvorenia Kresliť prechod pozadia okna Upraviť Upraviť výnimku - Nastavenia Vánok Povoliť animácie Povoliť/zakázať túto výnimku Typ výnimky Všeobecné Skryť titulok okna Informácie o vybranom okne Vľavo Posunúť dole Posunúť hore Nová výnimka - Nastavenia Vánok Otázka - Nastavenia Vánok Regulárny výraz Syntax regulárneho výrazu je nesprávna Zodpovedajúci regulárny výraz:  Odstrániť Odstrániť vybranú výnimku? Vpravo Tiene Veľkosť: Drobné Zarovnanie titulku: Titulok:  Použiť triedu okna (celá aplikácia) Použiť titulok okna Upozornenie - Nastavenia Vánok Názov triedy okna Identifikácia okna Výber vlastnosti okna Titulok okna Špecifické nastavenia okna Sila: 