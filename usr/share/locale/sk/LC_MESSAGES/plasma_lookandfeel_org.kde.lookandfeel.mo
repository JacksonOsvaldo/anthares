��    1      �  C   ,      8     9     =     B  .   Q  5   �     �     �     �     �  	   �     �               &  1   :     l     r          �  
   �     �  '   �     �     �     �            '        @     O     V     ^  5   g     �     �     �     �     �  $   �  A   �  �   @     &     -  C   >  '   �     �     �     �  �  �     �
     �
     �
     �
     �
            	   ,     6  	   J     T     p     �     �  O   �            "   %     H     T     i     �     �     �  "   �     �     �  C   �     +     ;     G     P  I   Y     �     �  	   �     �     �     �             
        *     =     V     ]     t     |               #   /       -                      $                +         (   1          .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: plasma_lookandfeel_org.kde.lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-10-27 21:02+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1% Späť Batéria na %1% Prepnúť rozloženie Virtuálna klávesnica Zrušiť Caps Lock je zapnutý Zatvoriť Zatvoriť hľadanie Nastaviť Nastaviť pluginy hľadania Sedenie plochy: %1 Iný používateľ &Rozloženie klávesnice: %1 Odhlásenie za 1 sekundu. Odhlásenie za %1 sekundy. Odhlásenie za %1 sekúnd. Prihlásenie Prihlásenie zlyhalo Prihlásiť ako iný používateľ Odhlásenie Nasledujúca skladba Neprehráva sa žiadne médium Nepoužité OK Heslo Prehrať alebo pozastaviť médium Predchádzajúca skladba Reštartovať Reštart za 1 sekundu Reštart za %1 sekundy Reštart za %1 sekúnd Nedávne dotazy Odstrániť Reštart Vypnúť Vypínanie za 1 sekundu Vypínanie za %1 sekundy Vypínanie za %1 sekúnd Spustiť nové sedenie Uspať Prepnúť Prepnúť sedenie Prepnúť užívateľa Hľadať... Hľadať '%1'... Plasma od KDE Odomknúť Odomknutie zlyhalo na TTY %1 (Obrazovka %2) TTY %1 Používateľské meno %1 (%2) v kategórii nedávnych dotazov 