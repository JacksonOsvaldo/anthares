��    F      L  a   |         
     
     
        "     1     5     I     X     ^  	   m     w  	        �     �     �  
   �     �     �  3   �  	   �     �               $     9     @     G     V     f     m  
        �  2   �  ?   �                    )     2     ?     N     S     a     r     �     �     �     �     �     �  	   �     �     �     �     �  b   �  �   [	     �	     �	  	   
     
     (
  
   8
  	   C
     M
     ]
     e
     k
     }
  �  �
     f     o     }     �     �     �     �     �     �     �     �     �          $     +  
   4     ?     L     U  	   W     a     t     �     �     �     �     �     �     �     �  	   �       <     7   U     �     �     �  	   �     �     �     �     �     �                    /     A     H     Y     i     u     {     �  	   �  S   �  z        |     �     �     �     �     �     �     �     �     �               +       E   C   4       &       D           
   <      *   9           !   F      /   #                A       @   =                     $   ;                  B             :       '           0         1   2                                -   (       6   ?         >              "   .   %   ,              )       8       3   5   	                  7    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Apply now Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Concatenation sign for shortcuts, e.g. Ctrl+Shift+ Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout changes must be applied before other changes can be made Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: plasma_shell_org.kde.plasma.desktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: 2017-10-27 20:53+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Aktivity Pridať akciu Pridať medzeru Pridať widgety... Alt Alternatívne widgety Vždy viditeľné Použiť Použiť nastavenie Použiť teraz Autor: Automaticky skrývať Tlačidlo vzad Spodok Zrušiť Kategórie Vycentrovať Zavrieť + Nastaviť Nastaviť aktivitu Vytvoriť aktivitu... Ctrl Aktuálne sa používa Vymazať E-mail: Tlačidlo vpred Získať nové widgety Výška Horizontálny posun Vstup sem Klávesové skratky Rozloženie nie je možné zmeniť pri zamknutých widgetoch Zmeny rozloženia sa musia použiť pred inými zmenami Rozloženie: Vľavo Ľavé tlačidlo Licencia: Zamknúť widgety Maximalizovať panel Meta Stredné tlačidlo Viac nastavení... Akcie myši OK Zarovnanie panelu Odstrániť panel Vpravo Pravé tlačidlo Hrana obrazovky Hľadať... Shift Zastaviť aktivitu Zastavené aktivity: Prepnúť Nastavenia aktuálneho modulu sa zmenili. Chcete použiť zmeny alebo zahodiť ich? Táto skratka aktivuje applet: nastaví zameranie klávesnice naň, a ak applet má popup (ako ponuku štart), otvorí sa. Vrch Vrátiť odinštalovanie Odinštalovať Odinštalovať widget Vertikálny posun Viditeľnosť Tapeta Typ tapety: Widgety Šírka Okná môžu prekrývať Okná idú pod 