��          �            x     y  !   �     �     �     �  @   �     *  $   ?     d     k  "   �     �  %   �     �     �  �       �     �          #     /     N     W  4   c     �     �     �  	   �     �     �  	   �                        
                                    	                   Artist of the songby %1 Artist of the songby %1 (paused) Choose player automatically General No media playing Open player window or bring it to the front if already openOpen Pause playbackPause Pause playback when screen is locked Paused Play next trackNext Track Play previous trackPrevious Track Quit playerQuit Remaining time for song e.g -5:42-%1 Start playbackPlay Stop playbackStop Project-Id-Version: plasma_applet_org.kde.plasma.mediacontroller
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2017-05-18 21:53+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 od %1 od %1 (pozastavené) Vybrať prehrávač automaticky Všeobecné Neprehráva sa žiadne médium Otvoriť Pozastaviť Pozastaví prehrávanie, keď je obrazovka zamknutá Pozastavené Nasledujúca skladba Predchádzajúca skladba Ukončiť -%1 Prehrať Zastaviť 