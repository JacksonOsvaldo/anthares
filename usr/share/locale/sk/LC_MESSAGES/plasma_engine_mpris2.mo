��          t      �         C        U     i  3   �     �     �  F   �  5   *     `       �  �  5   W     �     �     �  (   �       9   "  6   \     �     �                              	                          
    Attempting to perform the action '%1' failed with the message '%2'. Media playback next Media playback previous Name for global shortcuts categoryMedia Controller Play/Pause media playback Stop media playback The argument '%1' for the action '%2' is missing or of the wrong type. The media player '%1' cannot perform the action '%2'. The operation '%1' is unknown. Unknown error. Project-Id-Version: plasma_engine_mpris2
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2015-02-18 21:57+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Pokus o vykonanie akcie '%1' zlyhal so správou '%2'. Prehrávanie média ďalšie Prehrávanie média predošlé Ovládač médií Spustiť/pozastaviť prehrávanie média Zastaviť prehrávanie média Argument '%1' pre akciu '%2' chýba alebo je zlého typu. Prehrávač médií '%1' nemôže vykonať akciu '%2'. Operácia '%1' je neznáma. Neznáma chyba. 