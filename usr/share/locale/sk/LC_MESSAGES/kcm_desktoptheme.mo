��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     `     w      �     �     �     �     �     �  '        7      S     t  1   �         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-27 21:03+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nastaviť tému plochy David Rosca sparc3@azet.sk,misurel@gmail.com Získať nové témy... Nainštalovať zo súboru... sparc3,Michal Šulek Otvoriť tému Odstrániť tému Súbory tém (*.zip *.tar.gz *.tar.bz2) Inštalácia témy zlyhala. Téma nainštalovaná úspešne. Odstránenie témy zlyhalo. Tento modul vám umožní nastaviť tému plochy. 