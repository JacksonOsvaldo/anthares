��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �  �  �  "   �  
   �     �  0   �  	   �     �  #   	  -   -     [     {     �  5   �     �     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: screenlocker_kcm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2016-09-15 15:47+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Zamknúť obrazovku po prebudení: Aktivácia Chyba Zlyhalo úspešne otestovať zamykač obrazovky. Okamžite Zamknúť sedenie Zamknúť obrazovku automaticky po: Zamknúť obrazovku pri prebudení zo spánku Vyžadovať heslo po zamknutí:  min  min  min  sec sekundy  sec Globálna klávesová skratka na zamknutie obrazovky. Typ tapety: 