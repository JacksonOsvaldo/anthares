��    7      �  I   �      �  �  �  }  �  M    
   V     a     �  <   �     �     �  	          .   !  %   P     v     �     �     �     �     �     �     �     �  4        ;  9   M     �     �  �   �  !   �  t   �          *     G     T     Y     p  B   y  &   �  ?   �  '   #  )   K  7   u  .   �  5   �  +        >     [     {  ,   �     �     �  9   �  V     C   l  �  �  �  u    N  "  i     �  "   �  #   �  E   �     "     <  	   M     W  4   j  !   �     �  	   �     �     �           #      +      E   !   K   8   m      �   D   �      �      !  �   !     �!  f   �!     f"     u"     �"     �"     �"     �"  D   �"  !   &#  :   H#  *   �#  *   �#  ?   �#  .   $  3   H$  +   |$     �$     �$     �$     �$     �$     %  <   %  Q   \%     �%               -       ,      6         1      /   '      (       3      .      #      )   
          +          *   %          5          4   2   	      7                                                       $                            0   !   &          "       <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Security notice:</b>
                             According to a security audit by Taylor Hornby (Defuse Security),
                             the current implementation of Encfs is vulnerable or potentially vulnerable
                             to multiple types of attacks.
                             For example, an attacker with read/write access
                             to encrypted data might lower the decryption complexity
                             for subsequently encrypted data without this being noticed by a legitimate user,
                             or might use timing analysis to deduce information.
                             <br /><br />
                             This means that you should not synchronize
                             the encrypted data to a cloud storage service,
                             or use it in other circumstances where the attacker
                             can frequently access the encrypted data.
                             <br /><br />
                             See <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> for more information. <b>Security notice:</b>
                             CryFS encrypts your files, so you can safely store them anywhere.
                             It works well together with cloud services like Dropbox, iCloud, OneDrive and others.
                             <br /><br />
                             Unlike some other file-system overlay solutions,
                             it does not expose the directory structure,
                             the number of files nor the file sizes
                             through the encrypted data format.
                             <br /><br />
                             One important thing to note is that,
                             while CryFS is considered safe,
                             there is no independent security audit
                             which confirms this. Activities Can not create the mount point Can not open an unknown vault. Choose the encryption system you want to use for this vault: Choose the used cypher: Close the vault Configure Configure Vault... Configured backend can not be instantiated: %1 Configured backend does not exist: %1 Correct version found Create CryFS Device is already open Device is not open Dialog Do not show this notice again EncFS Encrypted data location Failed to create directories, check your permissions Failed to execute Failed to fetch the list of applications using this vault Forcefully close  General If you limit this vault only to certain activities, it will be shown in the applet only when you are in those activities. Furthermore, when you switch to an activity it should not be available in, it will automatically be closed. Limit to the selected activities: Mind that there is no way to recover a forgotten password. If you forget the password, your data is as good as gone. Mount point Mount point is not specified Mount point: Next Open with File Manager Previous The mount point directory is not empty, refusing to open the vault The specified backend is not available The vault configuration can only be changed while it is closed. The vault is unknown, can not close it. The vault is unknown, can not destroy it. This device is already registered. Can not recreate it. This directory already contains encrypted data Unable to close the vault, an application is using it Unable to close the vault, it is used by %1 Unable to detect the version Unable to perform the operation Unknown device Unknown error, unable to create the backend. Use the default cipher Vaul&t name: Wrong version installed. The required version is %1.%2.%3 You need to select empty directories for the encrypted storage and for the mount point formatting the message for a command, as in encfs: not found%1: %2 Project-Id-Version: plasmavault-kded
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-11 05:53+0100
PO-Revision-Date: 2017-10-27 21:04+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'hlv'; font-size:9pt; font-weight:400; font-style:normal;">
<p style="-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p></body></html> <b>Bezpečnostná poznámka:</b>
Podľa bezpečnostného auditu od Taylora Hornbyho (Defuse Security),
aktuálna implementácia Encfs je zraniteľná alebo potenciálne zraniteľná.
na viacero typov útokov.
Napríklad, ak útočník a prístupom na čítanie a zápis
k šifrovaným údajom môže znížiť zložitosť šifrovania
na následne šifrované údaje bez povšimnutia bežným používateľom,
alebo môže použiť časovú analýzu na zistenie informácie.
<br /><br />
To znamená, že by ste nemali synchronizovať
šifrované údaje na cloudové úložisko,
alebo použiť ich podmienkach, kde útočník
môže často pristupovať k šifrovaným údajom.
<br /><br />
Pozrite <a href='http://defuse.ca/audits/encfs.htm'>defuse.ca/audits/encfs.htm</a> pre viac informácií. <b>Bezpečnostná poznámka:</b>
CryFS šifruje vaše súbory, takže ich môžete bezpečne uložiť ich kamkoľvek.
Dobre funguje s cloudovými úložiskami ako Dropbox, iCloud, OneDrive a inými.
<br /><br />
Na rozdiel od ostatných súborových prekryvných riešení,
nevystavuje adresárovú štruktúru,
počet súborov ani ich veľkosť
cez formát šifrovaných údajov.
<br /><br />
Jedna dôležitá poznámka je tá,
že pokiaľ CryFS sa považuje za bezpečný,
nie je žiadny nezávislý bezpečnostný audit
ktorý by to potvrdil. Aktivity Nemôžem vytvoriť prípojný bod Nemôžem otvoriť neznámu klenbu. Vyberte systém šifrovania, ktorý chcete použiť pre túto klenbu: Vybrať použitú šifru: Zatvoriť klenbu Nastaviť Nastaviť Vault... Nastavený backend nie je možné inicializovať: %1 Nastavený backend neexistuje: %1 Našla sa správna verzia Vytvoriť CryFS Zariadenie je už otvorené Zariadenie nie je otvorené Dialóg Nezobrazovať tento oznam EncFS Umiestnenie šifrovaných údajov Zlyhalo vytvorenie adresárov, skontrolujte vaše práva Zlyhalo spustenie Zlyhalo stiahnutie zoznamu aplikácií používajúcich túto klenbu Nútene zatvoriť Všeobecné Ak obmedzíte túto klenbu na určité aktivity, zobrazí sa v applete iba pri použití týchto aktivít. Teda keď sa prepnete na aktivitu, kde by nemala byť dostupná, automaticky sa zavrie.s Obmedziť na vybrané aktivity: Pamätajte, že nie je možné obnoviť zabudnuté heslo. Ak zabudnete heslo, vaše údaje sa stratia. Bod pripojenia Prípojný bod nie je určený Bod pripojenia: Nasledujúci Otvoriť v správcovi súborov Predchádzajúci Adresár prípojného bodu nie je prázdny, odmietam otvoriť klenbu Určený backend nie je dostupný Nastavenie klenby sa dá zmeniť iba počas jej zatvoenia. Klenba je neznáma, nemôžem ju zavrieť. Klenba je neznáma, nemôžem ju zničiť. Toto zariadenie je už zaregistrované. Nemôžem ho vytvoriť. Tento adresár už obsahuje šifrované údaje Nemôžem zatvoriť klenbu, aplikácia ju používa Nemôžem zatvoriť klenbu, používa ju %1 Nemôžem zistiť verziu Nemôžem vykonať operáciu Neznáme zariadenie s Použiť predvolenú šifru Názov klenby: Nainštalovaná zlá verzia. Vyžadovaná verzia je %1.%2.%3 Musíte vybrať prázdne adresáre na šifrované úložisko a pre prípojný bod %1: %2 