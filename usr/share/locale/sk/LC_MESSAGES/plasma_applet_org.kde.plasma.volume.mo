��    &      L  5   |      P     Q  8   S     �     �     �     �     �     �  3   �  >        N     i     y     �  m   �     �          "     2     7     ?  *   O      z     �     �  "   �     �     �          3     :     H     X     e     �  ;   �     �  �  �     �     �  
   �     �     	  
   	     #	     @	     U	     ]	     i	     �	  
   �	     �	     �	     �	     �	     �	     
  
   
     
  6   ,
  8   c
     �
     �
     �
     �
     �
  #   
  
   .     9     K     ]     n     v     }     �        #   %                                                              "          $                      
                                           &      !                	        % Accessibility data on volume sliderAdjust volume for %1 Applications Audio Muted Audio Volume Behavior Capture Devices Capture Streams Checkable switch for (un-)muting sound output.Mute Checkable switch to change the current default output.Default Decrease Microphone Volume Decrease Volume Devices General Heading for a list of ports of a device (for example built-in laptop speakers or a plug for headphones)Ports Increase Microphone Volume Increase Volume Maximum volume: Mute Mute %1 Mute Microphone No applications playing or recording audio No output or input devices found Playback Devices Playback Streams Port is unavailable (unavailable) Port is unplugged (unplugged) Raise maximum volume Show additional options for %1 Volume Volume at %1% Volume feedback Volume step: label of device items%1 (%2) label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: plasma_applet_org.kde.plasma.volume
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-10-27 21:06+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 % Upraviť hlasitosť pre %1 Aplikácie Automaticky stlmené Hlasitosť zvuku Správanie Zariadenia pre zachytávanie Prúdy zachytávania Stlmiť Predvolené Znížiť hlasitosť mikrofónu Znížiť hlasitosť Zariadenia Všeobecné Porty Zvýšiť hlasitosť mikrofónu Zvýšiť hlasitosť Maximálna hlasitosť: Stlmiť Stlmiť %1 Stlmiť mikrofón Žiadne aplikácie neprehrávajú a nenahrávajú zvuk Nenašli sa žiadne výstupné alebo vstupné zariadenia Zariadenia pre prehrávanie Prúdy prehrávania  (nedostupné)  (nezapojené) Zvýšiť maximálnu hlasitosť Zobraziť dodatočné voľby pre %1 Hlasitosť Hlasitosť na %1% Odozva hlasitosti Krok hlasitosti: %1 (%2) %1: %2 100% %1% 