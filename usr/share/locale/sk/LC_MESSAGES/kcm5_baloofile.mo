��          �            h     i     �  #   �      �      �     �  J        [  &   y     �  T   �       *   $     O  �  ]          <  #   Z  "   ~     �     �  T   �      &  '   G     o  g   ~     �  ,   �     #                            	                                             
       Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Not allowed to exclude root folder, please disable File Search if you do not want it Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: kcm_baloofile
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-27 21:07+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Indexovať aj obsah súborov Nastavenie hľadania súborov Copyright 2007-2010 Sebastian Trüg Neprehľadávať tieto umiestnenia wizzardsk@gmail.com Povoliť hľadanie súborov Hľadanie súborov vám pomôže rýchlo nájsť všetky vaše súbory podľa obsahu Priečinok %1 je už vylúčený Rodič priečinka %1 je už vylúčený Roman Paholík Nie je povolené vylúčiť koreňový priečinok, prosím zakážte Hľadanie súborov, ak to nechcete Sebastian Trüg Vyberte priečinok, ktorý sa má vylúčiť Vishesh Handa 