��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  4   �       	   2     <  #   E     i     r  
   �     �     �     �  
   �     �     �  
   �     �  2        G  
   ^  
   i     t     �     �     �     �     �     �     �     �     �  6     	   ;  	   E  
   O  
   Z  
   e     p     |  	   �     �     �     �     �     �                          2     ?     L     _     c  	   u          �  	   �  	   �     �     �     �     �     �     �           (     F     [  !   u     �     �     �     �     �  K   �          %  	   1     ;     N     b  
   g     r     {     �  	   �     �     �     �     �     �     �           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-10 20:35+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Modul prehliadača zariadení založeného na Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 voľných z %2 (%3% použitých) Batérie Typ batérie:  Zbernica:  Fotoaparát Fotoaparáty Stav nabitia:  Nabíja sa Zabaliť všetko Čítačka kariet Compact Flash Zariadenie Informácie o zariadení Zobrazuje všetky aktuálne vypísané zariadenia. Prehliadač zariadení Zariadenia Vybíja sa misurel@gmail.com Zašifrované Rozbaliť všetko Súborový systém Typ súborového systému:  Plne nabitá Pevný disk Hotplug? IDE IEEE1394 Zobrazuje informácie o aktuálne vybranom zariadení. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Klávesnica Klávesnica a myš Popisok:  Maximálna rýchlosť:  Čítačka kariet Memory Stick Pripojené v:  Myš Multimediálne prehrávače Michal Šulek Nie Nenabíja sa Údaje nedostupné Nepripojené Nenastavený Optická mechanika PDA Tabuľka oddielov Primárna Procesor %1 Číslo procesora:  Procesory Produkt:  Raid Vymeniteľné? SATA SCSI Čítačka kariet SD/MMC Zobraziť všetky zariadenia Zobraziť relevantné zariadenia Čítačka kariet Smart Media Ukladacie zariadenia Podporované ovládače:  Podporované inštrukčné sady:  Podporované protokoly:  UDI:  UPS USB UUID:  Zobrazuje aktuálny UDI (jedinečný identifikátor zariadenia) zariadenia. Neznáme zariadenie Nepoužité Výrobca: Veľkosť zväzku: Využitie zväzku:  Áno kcmdevinfo Neznáme Žiadne Žiadne Platforma Neznáme Neznáme Neznáme Neznáme Neznáme Čítačka kariet xD 