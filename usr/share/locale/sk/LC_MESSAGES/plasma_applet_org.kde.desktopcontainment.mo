��    T      �  q   \            !     )     :     I     O     V     b     s     �     �  /   �  -   �     �  
   �                          &     3     8  
   @     K     X     g          �     �     �     �     �  	   �     �     �     �  	   	     	     	     	     	     !	  	   *	     4	     B	     I	     P	     h	     m	     r	  <   u	     �	     �	     �	     �	     �	     �	     
     
     
  
   $
     /
     =
     O
     ^
     r
  )   �
     �
     �
     �
     �
     �
     �
     �
                    "     )     .     H     Q     a  E   r  �  �     �     �     �  	   �  	   �     �     �       	        !  
   .     9  	   H     R     a     h     q     y     �     �     �     �     �     �     �               #  '   *     R     f     z     �     �  	   �     �     �     �     �     �     �     �     �  	             !     >     E     M  B   P     �     �     �     �     �     �     �     �     �          "     5     I      c     �  3   �     �  +   �  	             !     /  
   ?     J     ]     e     m     y     }     �     �     �  N   �     &   )   #   @   F   C   6            M   =   (       S          N       
   D          9             H       1       3                  R      T       4   ?      B   8   0      ,       !   E             *   A      .   7             :   O           /   5         %       '               I   	       >   ;   J          K          L   +           P       $   2   -                     G          <                  Q   "    &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Title: Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: plasma_applet_org.kde.desktopcontainment
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-10-27 20:54+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Vymazať &Vyprázdniť kôš Presunúť do &koša &Otvoriť V&ložiť &Vlastnosti &Obnoviť pracovnú plochu O&bnoviť zobrazenie &Obnoviť P&remenovať Vybrať... Vymazať ikonu Zarovnať Usporiadať do Späť Zrušiť Stĺpce Nastaviť pracovnú plochu Vlastný titulok Dátum Predvolené Zostupne Zrušiť výber Rozloženie plochy Sem zadajte vlastný titulok Prípona súborov: Typy súborov: Filter Vyskakovacie okná náhľadu priečinka Najskôr priečinky Najskôr priečinky Celá cesta Mám to Skryť súbory zodpovedajúce Obrovské Veľkosť ikon Ikony Veľké Vľavo Zoznam Umiestnenie Umiestnenie: Zamknúť na mieste Zamknuté Stredné Viac možností náhľadu... Názov Žiadne OK Stlačte a držte widgety na ich presun a uvoľnenie ich rukoväte Náhľady pluginov: Miniatúry náhľadu Odstrániť Zmeniť veľkosť Obnoviť Vpravo Otočiť Riadky Hľadať typ súboru... Vybrať všetko Vybrať priečinok Označníky výberu Zobraziť všetky súbory Zobraziť súbory zodpovedajúce Zobraziť miesto: Zobraziť súbory prepojené s aktuálnou aktivitou Zobraziť pracovnú plochu Zobraziť panel nástrojov pracovnej plochy Veľkosť Malé Stredne malé Triediť podľa Triedenie: Zadať priečinok: Drobné Názov: Vychytávky Typ Sem zadajte cestu alebo URL Netriedené Spracúvanie widgetov Widgety odomknuté Môžete stlačiť a držať widgety na ich presun a uvoľnenie ich rukoväte. 