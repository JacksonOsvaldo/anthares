��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .  	   �               !     =     \     c     z  +   �     �  "   �  
   �     �     �               &     6     N  %   l     �  B   �  4   �  	   #	     -	     2	  1   9	     k	     	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: plasmashellprivateplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-08-31 22:29+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 S&pustiť Všetky widgety Kategórie: Skriptovacia konzola plochy Stiahnuť nové Plasma widgety Editor Spúšťa sa skript %1 Filtre Inštalovať widget z lokálneho súboru... Zlyhanie inštalácie Inštalácia balíčka %1 zlyhala. Načítať Nové sedenie Otvoriť súbor skriptu Výstup Beží Čas behu: %1ms Uložiť súbor skriptu Zamknutie obrazovky povolené Časový limit pre šetrič obrazovky Vybrať súbor plazmoidu Nastaví počet minút, po uplynutí ktorých sa obrazovka zamkne. Nastaví, či sa obrazovka zamkne po určenom čase. Šablóny KWin Plasma Nepodarilo sa načítať súbor skriptu <b>%1</b> Odinštalovateľné Použiť 