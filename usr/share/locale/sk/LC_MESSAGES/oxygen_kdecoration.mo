��    >        S   �      H     I  !   e  "   �  &   �  ,   �  #   �  &   "  !   I  &   k  '   �  "   �  #   �  "     '   $     L     _  +   c  
   �     �     �     �     �     �     �  U   �  7   J     �     �     �     �      �     �     �     	     	  !   &	     H	  	   M	     W	     _	     	     �	  &   �	     �	     �	     �	     
     
     #
     5
  4   =
  $   r
     �
     �
     �
     �
     �
            &   )     P  �  j      /  	   P     Z  
   b     m  	   �  	   �     �     �     �     �  	   �     �     �     �     �  6     	   ;     E     [     m     �     �     �  O   �  H        W     l     �     �  %   �  !   �     �     �     �     
     &     -     <  "   K     n     �  )   �  "   �     �     �               !  	   5  7   ?  '   w     �     �     �  
   �     �            $   +     P         #          1   &                  	   <          6       =   0   2          +                                 '   ;              5   -             /   !      *               3   )                    
          $             ,   9   8   :           %       "      (       >   4      7      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: breeze_kwin_deco
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2016-02-23 18:22+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Zodpovedajúca vlastnosť okna:  Obrovská Veľké Bez okraja Bez bočného okraja Normálne Nadmerná Drobná Veľmi obrovská Veľmi veľká Veľké Normálne Malá Veľmi veľká Žiara aktívneho okna Pridať Pridať spracovač na zmenu veľkosti okien bez okraja Animácie Veľkosť tlačidiel: Veľkosť okraja: Prechod myšou nad tlačidlom Vycentrovať Vycentrovať (plná šírka) Trieda:  Nastaviť miznutie medzi tieňom okna a žiarou pri zmene aktívneho stavu okna Nastavenie animácie zvýraznenia pri presune myšou nad tlačidlom okna Možnosti dekorácie Zistiť vlastnosti okna Dialóg Upraviť Upraviť výnimku - Nastavenia Oxygen Povoliť/zakázať túto výnimku Typ výnimky Všeobecné Skryť titulok okna Informácie o vybranom okne Vľavo Posunúť dole Posunúť hore Nová výnimka - Nastavenia Oxygen Otázka - Nastavenia Oxygen Regulárny výraz Syntax regulárneho výrazu je nesprávna Zodpovedajúci regulárny výraz:  Odstrániť Odstrániť vybranú výnimku? Vpravo Tiene Zarovnanie titulku: Titulok:  Použiť rovnaké farby pre titulný pruh aj obsah okna Použiť triedu okna (celá aplikácia) Použiť titulok okna Upozornenie - Nastavenia Oxygen Názov triedy okna Tieň okna Identifikácia okna Výber vlastnosti okna Titulok okna Prechody zmeny aktívneho stavu okna Špecifické nastavenia okna 