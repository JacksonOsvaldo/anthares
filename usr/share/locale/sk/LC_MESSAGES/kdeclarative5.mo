��          �            h  ~   i  +   �           5     T     s     �     �  �   �  g   A  0   �  .   �     	  @     �  Z  �     ,   �  `   �     /  M   M     �     �     �  �   �  t   y  4   �  ,   #	     P	     h	                                                   	             
                Click on the button, then enter the shortcut like you would in the program.
Example for Ctrl+A: hold the Ctrl key and press A. Conflict with Standard Application Shortcut EMAIL OF TRANSLATORSYour emails KPackage QML application shell NAME OF TRANSLATORSYour names No shortcut definedNone Reassign Reserved Shortcut The '%1' key combination is also used for the standard action "%2" that some applications use.
Do you really want to use it as a global shortcut as well? The F12 key is reserved on Windows, so cannot be used for a global shortcut.
Please choose another one. The key you just pressed is not supported by Qt. The unique name of the application (mandatory) Unsupported Key What the user inputs now will be taken as the new shortcutInput Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-30 03:07+0100
PO-Revision-Date: 2015-03-05 19:19+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Kliknite na tlačidlo a potom zadajte skratku, ktorú chcete použiť v programe.
Príklad pre Ctrl+A: držte klávesu Ctrl a stlačte A. Konflikt so štandardnou skratkou aplikácie visnovsky@kde.org,Richard.Fric@kdemail.net,miguel@portugal.sk,vatrtj@gmail.com,misurel@gmail.com Shell KPackage QML aplikácie Stanislav Višňovský,Richard Frič,Michal Gašpar,Jakub Vatrt,Michal Šulek Žiadna Znovu priradiť Rezervovaná skratka Klávesová kombinácia '%1' je už používaná pre štandardnú akciu "%2", ktorú používa viacero aplikácií.
Naozaj ju chcete použiť ako globálnu klávesovú skratku? Klávesa F12 je vo Windows rezervovaná, takže nemôže byť použitá pre globálnu skratku.
Prosím vyberte inú. Práve stlačená klávesa nie je podporovaná v Qt. Jedinečný názov pre aplikáciu (povinné) Nepodporovaná klávesa Vstup 