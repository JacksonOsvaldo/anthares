��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �          #     1     :     @     S     h     y     �  O   �     �  ,   �     *	  !   8	     Z	     b	     q	     �	     �	  	   �	     �	     �	     �	     �	  �   �	  C   �
     �
     �
     �
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-08-10 22:01+0200
Last-Translator: Roman Paholík <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Aktivity Všetky ostatné aktivity Všetky ostatné plochy Všetky ostatné obrazovky Všetky okná Alternatívne Plocha 1 Obsah Aktuálna aktivita Aktuálna aplikácia Aktuálna plocha Aktuálna obrazovka Filtrovať okná podľa Nastavenie politiky zamerania obmedzuje funkcionalitu prepínania medzi oknami. Dopredu Získať nové rozloženie prepínača okien Skryté okná Vrátane ikony "Zobraziť plochu" Hlavné Minimalizácia Iba jedno okno na aplikáciu Nedávno použité Opačne Obrazovky Skratky Zobraziť vybrané okno Poradie triedenia: Poradie hromadenia Aktuálne vybrané okno bude zvýraznené zoslabením všetkých ďalších okien. Táto možnosť vyžaduje, aby boli aktívne efekty plochy. Efekt, ktorý nahradí zoznam okien, ak sú aktívne efekty plochy. Virtuálne plochy Viditeľné okná Vizualizácia 