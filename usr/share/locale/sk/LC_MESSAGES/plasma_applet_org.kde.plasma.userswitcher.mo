��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s     Q     i     u     �     �     �     �     �  &   �     �          1  ,   D  
   q     |     �     �                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: plasma_applet_org.kde.plasma.userswitcher
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-09 13:33+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Aktuálny používateľ Všeobecné Rozloženie Zamknúť obrazovku Nové sedenie Nepoužité Opustiť... Zobraziť aj avatar aj meno Zobraziť plné meno (ak je dostupné) Zobraziť meno používateľa Zobraziť iba avatar Zobraziť iba meno Zobraziť technické informácie o sedeniach na %1 (%2) TTY %1 Zobrazenie mena používateľa Ste prihlásený ako <b>%1</b> 