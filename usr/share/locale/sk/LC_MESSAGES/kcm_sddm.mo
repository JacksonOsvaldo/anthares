��    ,      |  ;   �      �  (   �     �  �   �     �     �     �     �     �     �     �     �     �               %     1      J     k     s     �     �     �     �     �     �     �               /     4     I     Y     l     y     �     �      �  7   �                     1     6  �  <     �     	     
	     $	     5	     A	     G	     a	     j	     ~	     �	     �	     �	     �	     �	     �	     �	     
     
     (
     <
     V
     l
     �
  )   �
     �
     �
     �
     �
     �
          -     M     _     s     �      �  A   �     �     �          1     ?     !                 %   +   ,      "   #                               &                  	                           )                             *      '      $      (                                
    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) @title:windowSelect Image Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: kcm_sddm
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-27 20:55+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 (Wayland) ... (Dostupné veľkosti: %1) Vybrať obrázok Pokročilé Autor Automatické prihlásenie Pozadie: Vyčistiť obrázok Príkazy Nemôžem rozbaliť archív Téma kurzora: Prispôsobiť tému Predvolené Popis Stiahnuť nové témy SDDM wizzardsk@gmail.com Všeobecné Získať novú tému Príkaz zastavenia: Nainštalovať zo súboru Nainštalovať tému. Neplatný balík témy Načítať zo súboru... Prihlasovacia obrazovka s použitím SDDM Maximálne UID: Minimálne UID: Roman Paholík Názov Náhľad nie je k dispozícii Príkaz reštartu: Znovu prihlásiť po ukončení Odstrániť tému KDE nastavenie SDDM Inštalátor tém SDDM Sedenie: Predvolená téma kurzora v SDDM Téma na nainštalovanie, musí byť existujúci súbor archívu. Téma Nemôžem nainštalovať tému Odinštalovať tému. Používateľ Užívateľ: 