��    
      l      �       �   P   �   )   B  %   l  @   �     �  V   �     @     [     m  �       A  S   H  b   �  S   �      S  �   t  &        (     :            	               
                  %1 is '%1 packages to update' and %2 is 'of which %1 is security updates'%1, %2 1 package to update %1 packages to update 1 security update %1 security updates First part of '%1, %2'1 package to update %1 packages to update No packages to update Second part of '%1, %2'of which 1 is security update of which %1 are security updates Security updates available System up to date Updates available Project-Id-Version: muon-notifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-15 03:15+0100
PO-Revision-Date: 2015-11-01 20:28+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1, %2 1 balík na aktualizáciu %1 balíkov na aktualizáciu %1 balíkov na aktualizáciu Bezpečnostná aktualizácia %1 bezpečnostných aktualizácií %1 bezpečnostných aktualizácií 1 balík na aktualizáciu %1 balíkov na aktualizáciu %1 balíkov na aktualizáciu Žiadne balíky na aktualizáciu z ktorých 1 je bezpečnostná aktualizácia z ktorých %1 sú bezpečnostné aktualizácie z ktorých %1 je bezpečnostných aktualizácií Dostupné bezpečnostné aktualizácie Systém aktuálny Dostupné aktualizácie 