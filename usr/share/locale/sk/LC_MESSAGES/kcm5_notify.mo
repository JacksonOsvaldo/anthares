��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     �     �  *   �  G   �     &     6  7   >     v     �  5   �         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-10 10:02+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 (c) 2002-2006 tím KDE <h1>Systémové upozornenia</h1>KDE poskytuje veľkú kontrolu nad spôsobom, akým budete upozornený o určitých udalostiach. Je niekoľko spôsobov upozornení:<ul><li>Tak ako bola aplikácia pôvodne navrhnutá.</li><li>Pomocou pípnutia alebo iného zvuku.</li><li>Cez vyskakovacie dialógové okno s doplňujúcimi informáciami.</li><li>Cez záznam udalosti do záznamového súboru bez ďalších prídavných vizuálnych alebo zvukových upozornení.</li></ul> Carsten Pfeiffer Charles Samuels Zakázať zvuky pre všetky tieto udalosti cavojsky@soria-grey.sk,visnovsky@nenya.ms.mff.cuni.cz,misurel@gmail.com Zdroj udalosti: KNotify Marián Čavojský,Stanislav Višňovský,Michal Šulek Olivier Goffart Pôvodná implementácia Modul ovládacieho panelu pre systémové upozornenia 