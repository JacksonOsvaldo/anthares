��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  (   �     �     �     �          -     H     U     m  )   |  '   �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: kcm-kwin-scripts
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-10-27 21:01+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 *.kwinscript|KWin skripty (*.kwinscript) Nastaviť KWin skripty wizzardsk@gmail.com Získať nové skripty... Importovať KWin skript Importovať KWin skript... KWin skripty Nastavenie KWin skriptu Roman Paholík Nemôžem importovať vybraný skript.
%1 Skript "%1" bol úspešne importovaný. Tamás Krutki 