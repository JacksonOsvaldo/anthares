��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �  	   c     m     �  8   �     �     �     �             	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: plasma_applet_touchpad
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2015-01-09 20:22+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Zakázať Zakázať touchpad Povoliť touchpad Nebola zistená myš.
Určite chcete zakázať touchpad? Touchpad nebol nájdený Touchpad Touchpad je zakázaný Touchpad je povolený 