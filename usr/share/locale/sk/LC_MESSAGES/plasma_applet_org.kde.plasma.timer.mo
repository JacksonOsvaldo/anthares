��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {  	   R     \  
   h     s     |     �     �  	   �     �     �  Y   �       	   +     5     K     ]     o  	   u          �     �  l   �                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: plasma_applet_org.kde.plasma.timer
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-04-22 21:25+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 %1 beží %1 nebeží Vynulovať Spustiť Pokročilé Vzhľad Príkaz: Obrazovka Vykonať príkaz Upozornenia Ostávajúci čas: %1 sekunda Ostávajúci čas: %1 sekundy Ostávajúci čas: %1 sekúnd Spustiť príkaz Zastaviť Zobraziť upozornenia Zobraziť sekundy Zobraziť titulok Text: Časovač Časovač ukončený Časovač beží Názov: Použite koliesko myši na zmenu číslic alebo vyberte z preddefinovaných časovačov v kontektovej ponuke 