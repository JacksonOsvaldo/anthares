��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  '   �	  *   �	  0   �	     ,
     G
     g
     
  %   �
     �
     �
  %   �
     #     B  *   b  ,   �  3   �     �     	       "   0     S  )   p  !   �  !   �     �     �               "     1     A     V                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: fileviewbazaarplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2012-07-18 13:56+0100
Last-Translator: Roman Paholík <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Pridané súbory do Bazaar repozitára. Pridávam súbory do Bazaar repozitára... Pridanie súborov do Bazaar repozitára zlyhalo. Bazaar záznam zatvorený. Odoslanie Bazaar zmien zlyhalo. Odoslané Bazaar zmeny. Odosielam Bazaar zmeny... Tlačenie Bazaar repozitára zlyhalo. Potiahnutý Bazaar repozitár. Ťahanie Bazaar repozitára... Tlačenie Bazaar repozitára zlyhala. Zatlačený Bazaar repozitár. Tlačenie Bazaar repozitára... Odstránené súbory z Bazaar repozitára. Odstraňujem súbory z Bazaar repozitára... Odstránenie súborov z Bazaar repozitára zlyhalo. Zhodnotenie zmien zlyhalo. Zhodnotené zmeny. Hodnotenie zmien... Spustenie Bazaar záznamu zlyhalo. Spúšťam Bazaar záznam... Aktualizácia Bazaar repozitára zlyhala. Aktualizovaný Bazaar repozitár. Aktualizujem Bazaar repozitár... Bazaar pridať... Bazaar odoslať... Bazaar vymazať Bazaar záznam Bazaar ťahať Bazaar tlačiť Bazaar aktualizovať Zobraziť miestne Bazaar zmeny 