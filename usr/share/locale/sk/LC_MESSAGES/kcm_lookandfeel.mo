��          �      ,      �     �     �     �  #   �           #     4  
   G     R     _  �   ~       ]   )     �  p   �  :     �  Q          4     Q      m     �     �     �     �     �     �  t        w  m   �  #   �  u     ;   �                               	                              
                 Configure Look and Feel details Copyright 2017, Marco Martin Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Select an overall theme for your workspace (including plasma theme, color scheme, mouse cursor, window and desktop switcher, splash screen, lock screen etc.) Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: kcm_lookandfeel
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-12 20:49+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nastaviť podrobnosti vzhľadu Copyright 2017, Marco Martin Nastavenie kurzora zmenené Stiahnuť nové balíky vzhľadu wizzardsk@gmail.com Získať nové vzhľady... ;Nastaviť podrobnosti vzhľadu Správca Marco Martin Roman Paholík Vyberte celkovú tému vzhľadu pre vašu pracovnú plochu (vrátane úvodnej obrazovky, zamykača obrazovky atď.)  Zobraziť náhľad Tento modul vám umožní nastaviť vzhľad celej pracovnej plochy s niekoľkými pripravenými predvoľbami. Použiť rozloženie plochy z témy Upozornenie: vaše rozloženie plochy Plasma sa stratí a vynuluje na predvolený vzhľad pokytnutý vybranou témou. Musíte reštartovať KDE, aby sa vlastné zmeny prejavili. 