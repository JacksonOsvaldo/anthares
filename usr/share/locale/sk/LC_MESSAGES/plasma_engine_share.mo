��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  &     2   (  =   [  $   �  1   �  )   �          3  $   B         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-12-15 21:46+0100
Last-Translator: Michal Sulek <misurel@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Nepodarilo sa zistiť MIME typ súboru Nepodarilo sa nájsť všetky požadované funkcie Nepodarilo sa nájsť poskytovateľa so zadaným umiestnením Chyba pri pokuse o vykonanie skriptu Neplatná cesta pre požadovaného poskytovateľa Vybraný súbor sa nepodarilo prečítať Služba nebola dostupná Neznáma chyba Pre túto službu musíte zadať URL 