��    3      �  G   L      h  6   i  M   �  K   �     :  '   C     k     r  �   �     ;  	   R  A   \  >   �  9   �  9     9   Q  W   �  E   �     )     :      @     a  7   ~      �     �     �  X   �     X	     `	     v	  �   �	     '
     F
     L
     c
  
   s
  
   ~
     �
     �
  E  �
          &     <  w   O     �     �     �     �     �  	          �  #  F   �  Y   1  ^   �     �  ,   �          %  �   ;          $     1     ?     G  	   M  	   W  )   a     �     �     �  >   �  "   �  5     !   I     k     �  <   �  
   �     �     �  �     *   �     �     �     	  
     
   '     2     6  X  :     �     �     �  �   �     n     �     �     �     �     �     �         /   +   )               ,      .                 	          &                       %   #                       '       0       3                         -       1   2                        (   
      *   !         $                           "    "Full screen repaints" can cause performance problems. "Only when cheap" only prevents tearing for full screen changes like a video. "Re-use screen content" causes severe performance problems on MESA drivers. Accurate Allow applications to block compositing Always Animation speed: Applications can set a hint to block compositing when the window is open.
 This brings performance improvements for e.g. games.
 The setting can be overruled by window-specific rules. Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Configure filter Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Full screen repaints Get New Effects... Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team Keep window thumbnails: Keeping the window thumbnail always interferes with the minimized state of windows. This can result in windows not suspending their work when minimized. NAME OF TRANSLATORSYour names Never Only for Shown Windows Only when cheap OpenGL 2.0 OpenGL 3.1 OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX OpenGL compositing (the default) has crashed KWin in the past.
This was most likely due to a driver bug.
If you think that you have meanwhile upgraded to a stable driver,
you can reset this protection but be aware that this might result in an immediate crash!
Alternatively, you might want to use the XRender backend instead. Re-enable OpenGL detection Re-use screen content Rendering backend: Scale method "Accurate" is not supported by all hardware and can cause performance regressions and rendering artifacts. Scale method: Search Smooth Smooth (slower) Tearing prevention ("vsync"): Very slow XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-10-27 20:54+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 "Prekreslenie celej obrazovky" môže spôsobiť problémy s výkonom. "Iba ak to nie je náročné" iba zabraňuje trhaniu pre zmeny celej obrazovky ako video. "Znovu použiť obsah obrazovky" spôsobuje vážne problémy s výkonom na MESA ovládačoch. Presná Umožniť aplikáciám blokovať kompozíciu Vždy Rýchlosť animácie: Aplikácie môžu poslať náznak na zablokovanie kompozície, ak je otvorení okno.
 Toto prinesie výkonnostné zlepšenie napríklad pre hry.
 Nastavenie môže byť prebité špecifickým nastavením okna. Autor: %1
Licencia: %2 Automatické Prístupnosť Vzhľad Candy Zameranie Nástroje Animácia prepínania virtuálnych plôch Správa okien Nastaviť filter Ostrá Richard.Fric@kdemail.net,misurel@gmail.com,wizzardsk@gmail.com Povoliť efekty plochy pri štarte Vylúčiť efekty plochy nepodporované kompozítorom Vylúčiť interné efekty plochy Premaľovania plnej scény Získať nové efekty... Rada: viac informácií o efekte nájdete v jeho nastavení. Okamžitá Vývojový tím KWin Ponechať náhľady okien: Ponechanie miniatúry okna vždy interferuje s minimalizovaným stavom okna. Toto môže mať za následok, že okná nepozastavia svoju činnosť pri minimalizovaní. Richard Frič,Michal Šulek,Roman Paholík Nikdy Len pre zobrazené okná Iba ak jednoduché OpenGL 2.0 OpenGL 3.1 EGL GLX OpenGL kompozícia (predvolená) zhodila KWin v minulosti.
Bolo to pravdepodobne kvôli chybe ovládača.
Ak si myslíte, že ste rozumne aktualizovali ovládač na stabilný,
môžete vynulovať túto ochranu, ale majte na pamäti, že to môže
mať za následok okamžitý pád!
Alternatívne môžete použiť namiesto toho backend XRender. Znovu povoliť detekciu OpenGl Znovu použiť obsah obrazovky Renderovací backend: Metóda škálovania "Presná" nie je podporovaná na každom hardvéri a môže spôsobiť zníženie výkonu a chyby vykresľovania. Metóda škálovania: Hľadať Jemná Jemná (pomalšia) Zabránenie trhaniu ("vsync"): Veľmi pomaly XRender 