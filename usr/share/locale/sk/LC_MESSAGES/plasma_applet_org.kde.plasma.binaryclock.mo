��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �     N     V     ]     o     �  (   �  $   �  *   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: plasma_applet_binaryclock
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-08 17:08+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Vzhľad Farby: Zobraziť sekundy Vykresliť mriežku Zobraziť neaktívne LED: Použiť vlastnú farbu pre aktívne LED Použiť vlastnú farbu pre mriežku Použiť vlastnú farbu pre neaktívne LED 