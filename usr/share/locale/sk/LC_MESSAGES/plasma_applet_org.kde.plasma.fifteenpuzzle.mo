��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �     }     �     �     �  9   �     �     �          )  
   =  	   H  #   R     v     �     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-13 20:19+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Vzhľad Prechádzať... Vybrať obrázok Hra pätnástka Súbory obrázkov (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Farba čísla Cesta k vlastnému obrázku Farby figúrky Zobraziť číslice Zamiešať Veľkosť Vyriešiť usporiadaním do poradia Vyriešené! Skúste znova. Čas: %1 Použiť vlastný obrázok 