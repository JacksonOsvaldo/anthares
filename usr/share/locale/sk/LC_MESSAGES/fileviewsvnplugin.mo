��          �   %   �      p     q  +   �  .   �  6   �  *     #   D  &   h  /   �  2   �  :   �  0   -  3   ^  ;   �  K   �  -     $   H  '   m     �     �     �     �     �  #        1     O     c     |  �  �  	   O  $   Y  *   ~  -   �     �     �     	  '   &	  ,   N	  0   {	  $   �	  $   �	  -   �	  R   $
  &   w
     �
      �
     �
     �
     �
  
             %     A     ]     d     t                                                    	   
                                                                    @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusReverted files from SVN repository. @info:statusReverting files from SVN repository... @info:statusReverting of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Revert @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: fileviewsvnplugin
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2015-11-10 20:45+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Potvrdiť Súbory pridané so SVN repozitára. Pridávanie súborov do SVN repozitára... Pridanie súborov do SVN repozitára zlyhalo. Potvrdenie SVN zmien zlyhalo. SVN zmeny potvrdené. Potvrdzovanie SVN zmien... Súbory odstránené z SVN repozitára. Odstraňovanie súborov z SVN repozitára... Odstránenie súborov z SVN repozitára zlyhalo. Súbory vrátené z SVN repozitára. Vraciam súbory z SVN repozitára... Vrátenie súborov z SVN repozitára zlyhalo. Aktualizácia SVN stavu zlyhala. Zakazujem možnost "Zobraziť SVN aktualizácie". Aktualizácia SVN repozitára zlyhala. SVN repozitár aktualizovaný. Aktualizácia SVN repozitára... SVN pridať SVN potvrdiť... SVN vymazať SVN Revert SVN aktualizácia Zobraziť miestne SVN zmeny Zobraziť aktualizácie SVN Popis: SVN potvrdenie: Zobraziť aktualizácie 