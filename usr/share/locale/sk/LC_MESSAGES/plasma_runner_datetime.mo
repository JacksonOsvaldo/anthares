��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     �     �  0   �     
  .   "     Q     X     ]                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: plasma_runner_datetime
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-31 20:12+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Aktuálny čas je %1 Zobrazí aktuálny dátum Zobrazí aktuálny dátum v danej časovej zóne Zobrazí aktuálny čas Zobrazí aktuálny čas v danej časovej zóne dátum čas Dnešný dátum je %1 