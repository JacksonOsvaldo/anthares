��          L      |       �   >   �      �   9   �   A   &  
   h  �  s  B        T     X  =   a  
   �                                         '%1' needs a password to be accessed. Please enter a password. ... A default name for an action without proper labelUnknown A new device has been detected.<br><b>What do you want to do?</b> Do nothing Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-27 08:26+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Voor toegang tot '%1' is een wachtwoord nodig. Voer deze in a.u.b. ... Onbekend Er is een nieuw apparaat gevonden.<br><b>Wat wilt u doen?</b> Niets doen 