��          �      ,      �     �     �     �     �     �     �     �     �  .   �     .     L     \     m     �     �  H   �  �  �     �     �  
   �     �     �     �     �  
     *     "   <     _     r     �     �     �  I   �        	                                                                  
    &Configure Printers... Active jobs only All jobs Completed jobs only Configure printer General No active jobs No jobs No printers have been configured or discovered One active job %1 active jobs One job %1 jobs Open print queue Print queue is empty Printers Search for a printer... There is one print job in the queue There are %1 print jobs in the queue Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2015-02-21 09:37+0000
PO-Revision-Date: 2015-02-21 23:09+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Printers instellen... Alleen actieve taken Alle taken Alleen voltooide taken Printer instellen Algemeen Geen actieve taken Geen taken Er zijn geen printers ingesteld of ontdekt Eén actieve taak %1 actieve taken Eén taak %1 taken Printwachtrij openen Printwachtrij is leeg Printers Naar een printer zoeken... Er is een afdruktaak in de wachtrij Er zijn %1 afdruktaken in de wachtrij 