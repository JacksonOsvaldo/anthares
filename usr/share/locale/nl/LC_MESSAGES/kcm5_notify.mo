��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     k     |  1   �     �     �     �     �     �       !   $         
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-08-09 12:16+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 (c) 2002-2006, het KDE-team <h1>Systeemmeldingen</h1>PLasma geeft u een grote controle over hoe u gewaarschuwd zult worden bij een bepaalde gebeurtenis. Er zijn diverse manieren waarop u kunt worden verwittigd:<ul><li>op de wijze die de toepassing normaliter gebruikt.</li><li>via een geluidssignaal.</li><li>via een dialoogvenster met additionele informatie.</li><li>door de gebeurtenis op te nemen in een logbestand, zonder enige vorm van verdere audio/visuele verwittiging.</li></ul> Carsten Pfeiffer Charles Samuels Geluiden voor al deze gebeurtenissen uitschakelen freekdekruijf@kde.nl Gebeurtenisbron: KNotify Freek de Kruijf Olivier Goffart Originele implementatie Systeemmelding Configuratiemodule 