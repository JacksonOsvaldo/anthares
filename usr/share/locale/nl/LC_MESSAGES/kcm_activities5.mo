��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �  �  �     f	     v	  &   �	  	   �	  	   �	     �	     �	     �	     �	     
     4
     A
     \
     y
  >   �
     �
     �
       O        b     z     �     �     �     �  	   �     �     �  $   �     #     *  0   2     c  Z   �  /   �       	        "     8     >     N                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-03-29 23:59+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 &Niet onthouden Door activiteiten wandelen Door activiteiten wandelen (omgekeerd) Toepassen Annuleren Wijzigen... Aanmaken Instellingen voor activiteit Een nieuwe activiteit aanmaken Activiteit Verwijderen Activiteiten Informatie over activiteit Overschakelen van Activiteit Wilt u '%1' verwijderen? Alle toepassingen niet in deze lijst op de zwarte lijst zetten Recente geschiedenis wissen Activiteit aanmaken... Beschrijving: Fout bij laden van de QML-bestanden. Controleer uw installatie.
Er ontbreekt %1 Voor a&lle toepassingen Een dag vergeten Alles vergeten Het laatste uur vergeten De laatste twee uur vergeten Algemeen Pictogram Geschiedenis bewaren Naam: Allee&n voor specifieke toepassingen Overig Privacy Privé - gebruik van deze activiteit niet volgen Geopende documenten onthouden Onthoud het huidige virtuele bureaublad voor elke activiteit (heeft opnieuw starten nodig) Sneltoets voor omschakelen naar deze activiteit Sneltoetsen Schakelen Achtergrondafbeelding voor   maand  maanden altijd 