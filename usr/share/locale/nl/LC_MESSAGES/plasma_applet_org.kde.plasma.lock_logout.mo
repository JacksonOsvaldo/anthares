��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  #   �  &   �     $     -     4     K     j     s          �  +   �     �     �  ;   �     (     4     G     Z        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-05 21:23+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Wilt u naar de RAM-slaapstand gaan? Wilt u naar de schijf-slaapstand gaan? Algemeen Acties Slaapstand naar schijf Slaapstand (schijf-slaapstand) Verlaten Verlaten... Vergrendelen Het scherm vergrendelen Afmelden, afsluiten of herstart de computer Nee Slapen (RAM-slaapstand) Start een parallelle sessie onder een andere gebruikersnaam Onderbreken Gebruiker wisselen Gebruiker wisselen Ja 