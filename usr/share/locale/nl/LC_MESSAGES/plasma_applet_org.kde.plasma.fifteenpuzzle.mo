��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  	   f     p     |     �  <   �     �     �     
          '     0  "   8     [     w     �     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-12 13:25+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 Uiterlijk Bladeren... Een afbeelding kiezen Vijftien-puzzel Afbeeldingsbestanden (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Nummer van kleur Pad naar eigen afbeelding Kleur van stuk Nummers tonen Schudden Grootte Oplossen door volgorde te sorteren Opgelost! Opnieuw proberen. Tijd: %1 Eigen afbeelding gebruiken 