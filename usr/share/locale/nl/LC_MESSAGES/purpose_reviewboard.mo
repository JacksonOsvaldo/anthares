��          �      \      �     �     �  	   �  $   �          ,     C     ^     j  	   y  
   �     �     �     �     �     �  "   �  	   �  '   �  �  &     �     �  	   �  #   �  '        *     J  
   e     p     ~  
   �     �     �     �     �     �  ,   �       &   (                      
            	                                                                 / Authentication Base Dir: Could not create the new request:
%1 Could not get reviews list Could not set metadata Could not upload the patch Destination JSON error: %1 Password: Repository Repository: Request Error: %1 Server: Update Review: Update review User name in the specified service Username: Where this project was checked out from Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:19+0100
PO-Revision-Date: 2015-09-27 13:10+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 / Authenticatie Basismap: Kon geen nieuw verzoek aanmaken:
%1 Kon geen lijst met herzieningen ophalen Kon metagegevens niet instellen Kon de patch niet uploaden Bestemming JSON-fout: %1 Wachtwoord: Repository Repository: Fout in verzoek: %1 Server: Overzicht van bijwerken: Herziening bijwerken Gebruikersnaam in de gespecificeerde service Gebruikersnaam: Van waar dit project is "checked out". 