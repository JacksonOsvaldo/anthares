��            )   �      �     �     �     �     �     �     �     �  
           )   (     R     V     \     c     v     �     �     �  3   �     �       <   #  5   `  8   �  8   �                    /  �  1     �     �     �       
   !     ,     =  	   T     ^  9   g     �     �     �     �  $   �  '   �       	   1  6   ;  %   r  *   �  N   �  C   	  C   V	  C   �	     �	     �	     
     
                                                  
                                                                               	        Add files... Add folder... Background frame Change picture every Choose a folder Choose files Configure plasmoid... Fill mode: General Left click image opens in external viewer Pad Paths Paths: Pause on mouseover Preserve aspect crop Preserve aspect fit Randomize items Stretch The image is duplicated horizontally and vertically The image is not transformed The image is scaled to fit The image is scaled uniformly to fill, cropping if necessary The image is scaled uniformly to fit without cropping The image is stretched horizontally and tiled vertically The image is stretched vertically and tiled horizontally Tile Tile horizontally Tile vertically s Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-16 03:31+0100
PO-Revision-Date: 2017-11-16 15:06+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Bestanden toevoegen... Map toevoegen... Achtergrondframe Afbeelding wijzigen elke Map kiezen Bestanden kiezen Plasmoids instellen... Vulmodus: Algemeen Linker-muisklik op afbeelding opent in een externe viewer Pad Paden Paden: Pauzeren bij muis erboven Beeldverhouding bewaren en afsnijden Beeldverhouding bewaren en laten passen Items willekeurig maken Uitrekken De afbeelding is horizontaal en verticaal gedupliceerd De afbeelding is niet getransformeerd De afbeelding wordt geschaald om te passen De afbeelding wordt uniform geschaald om te passen, met afsnijden indien nodig De afbeelding wordt uniform geschaald om zonder afsnijden te passen De afbeelding is horizontaal uitgerekt en verticaal in tegels gezet De afbeelding is verticaal uitgerekt en horizontaal in tegels gezet Tegel Horizontaal in tegels zetten Verticale tegels s 