��          �      <      �     �     �     �     �     �  '   �  >        L     f     �     �     �  )   �  7   �  '        B     T  �  s          #     ,     5     I  
   W     b     n  )   �  #   �     �     �  (   �  
   '     2     9  !   R                                       
                      	                                        Current user General Layout Lock Screen New Session Nobody logged in on that sessionUnused Show a dialog with options to logout/shutdown/restartLeave... Show both avatar and name Show full name (if available) Show login username Show only avatar Show only name Show technical information about sessions User logged in on console (X display number)on %1 (%2) User logged in on console numberTTY %1 User name display You are logged in as <b>%1</b> Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-11 03:18+0100
PO-Revision-Date: 2016-04-09 17:02+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Huidige gebruiker Algemeen Indeling Scherm vergrendelen Nieuwe sessie Ongebruikt Verlaten... Zowel avatar als naam tonen Volledige naam tonen (indien beschikbaar) Gebruikersnaam voor aanmelden tonen Alleen avatar tonen Alleen naam tonen Technische informatie over sessies tonen op %1 (%2) TTY %1 Tonen van gebruikersnaam U bent nu aangemeld als <b>%1</b> 