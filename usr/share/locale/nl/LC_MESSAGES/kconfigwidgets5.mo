��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  5  �     *     7     E     Y     o     w     �  
   �     �     �     �  
   �     �     �     �                     1     A     O     l     s  
   �     �     �     �  
   �  	   �     �     �     �     �               &     6  
   >  	   I     S     [     {  	   �     �  	   �     �  $   �     �     �     �          +  $   D     i     �  #   �     �  w   �     4  	   C     M     ^     o     ~     �     �     �     �               4     L  �   S  
          "   +     N     i     �     �     �     �     �     �  1   �  2        P     `  &   q     �     �     �     �     �  @   �     ?  �   Q  *   �               ;     W     w     �  )   �     �  "   �  @   �  	   ?  
   I  /   T  (   �  0   �     �     �     �     �           o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-09-15 10:36+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Files: kfarch.cpp kfdird.cpp kfind.cpp kfindtop.cpp kfoptions.cpp kfsave.cpp kftabdlg.cpp kftypes.cpp kfwin.cpp main.cpp mkfdird.cpp mkfind.cpp
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 %1 &handboek Info &over %1 &Werkelijke grootte Bladwijzer &toevoegen Vo&rige Sl&uiten %1 &instellen... &Kopiëren Verwij&deren &Doneren Bladwijzers &bewerken... &Zoeken... &Eerste pagina &Passend in pagina &Verder &Ga naar... &Ga naar regel... &Ga naar pagina... &Laatste pagina &Verzenden... &Naar prullenbak verplaatsen &Nieuw V&olgende pagina &Openen... P&lakken &Vorige pagina Af&drukken... A&fsluiten He&rladen He&rnoemen... &Vervangen... Bug &rapporteren... Op&slaan Instellingen ops&laan &Spelling... &Ongedaan maken &Omhoog &Zoomen... Volge&nde Vo&rige Tips tonen tijdens het &starten Wist u dat...?
 Instellen Tip van de dag Over &KDE &Wissen Spelling controleren in het document Lijst wissen Document sluiten &Meldingen instellen... Sne&ltoetsen instellen... Werk&balken instellen... Selectie kopiëren naar het klembord Nieuw document aanmaken Kni&ppen Selectie knippen naar het klembord  Dese&lecteren rinsedevries@kde.nl,wbsoft@xs4all.nl,bramschoenmakers@kde.nl,tomalbers@kde.nl,tijmenbaarda@kde.nl,,freekdekruijf@kde.nl Autodetecteren Standaard Volledig scher&m Vol&gende zoeken Vo&rige zoeken Passend in pagina&hoogte Passend in pagina&breedte Terug gaan in het document Vooruit gaan in het document Naar eerste pagina gaan Naar laatste pagina gaan Naar volgende pagina gaan Naar vorige pagina gaan Omhoog Rinse de Vries - 2000 t/m 2008,Wilbert Berendsen - 2003; 2004,Bram Schoenmakers - 2004 t/m 2007,Tom Albers - 2004,Tijmen Baarda - 2005,Sander Koning - 2005, Freek de Kruijf - 2009 t/m 2017 Geen items &Recent geopend Een recent geopend document openen Open een bestaand document Inhoud klembord plakken Afdruk&voorbeeld Document afdrukken Toepassing afsluiten Opnie&uw Beginwaar&de Document opnieuw weergeven Laatste ongedaan gemaakte actie opnieuw uitvoeren Niet opslagen wijzigingen in document terugdraaien Opslaan &als... Document opslaan Document opslaan onder een nieuwe naam &Alles selecteren Zoomniveau selecteren Document per e-mail verzenden &Menubalk tonen &Werkbalk tonen Menubalk tonen<p>Toont de menubalk wanneer deze verborgen is</p> &Statusbalk tonen Statusbalk tonen<p>Toont de statusbalk, die de balk is aan de onderkant van het venster voor het weergeven van statusinformatie.</p> Een afdrukvoorbeeld van het document tonen Menubalk tonen of verbergen Statusbalk tonen of verbergen Werkbalk tonen of verbergen Taa&l van programma wijzigen... Tip van de &dag Laatste actie ongedaan maken Document in zijn actuele grootte bekijken Wat is di&t? U mag de configuratie niet opslaan U zal worden gevraagd zich te authenticeren alvorens op te slaan &Inzoomen &Uitzoomen Zoomen naar hoogte passend maken in het venster Zoomen naar passend maken in het venster Zoomen naar breedte passend maken in het venster &Terug &Vooruit &Startpagina &Help naamloos 