��    "      ,  /   <      �  
   �               ,     >     J  !   V     x     �     �     �     �     �  L   �     #     +     J     Y     u     z     �     �     �     �  	   �     �     �     �  �   �  F   �     �     �     �  �  �     �     �     �     �     �     	          "     )     <     O     b     p  ]   �     �  ,   �     	  %   +	     Q	     Z	  #   i	     �	  	   �	     �	     �	     �	     �	     �	  �   �	  W   �
     �
     �
     
                    !                                            	                                                           "               
                    Activities All other activities All other desktops All other screens All windows Alternative An example Desktop NameDesktop 1 Content Current activity Current application Current desktop Current screen Filter windows by Focus policy settings limit the functionality of navigating through windows. Forward Get New Window Switcher Layout Hidden windows Include "Show Desktop" icon Main Minimization Only one window per application Recently used Reverse Screens Shortcuts Show selected window Sort order: Stacking order The currently selected window will be highlighted by fading out all other windows. This option requires desktop effects to be active. The effect to replace the list window when desktop effects are active. Virtual desktops Visible windows Visualization Project-Id-Version: kcm_kwintabbox
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-05-18 22:03+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Activiteiten Alle andere activiteiten Alle andere bureaubladen Alle andere schermen Alle vensters Alternatief Bureaublad 1 Inhoud Huidige activiteit Huidige toepassing Huidige bureaublad Huidig scherm Vensters filteren op Het beleid voor focusinstellingen beperkt de functionaliteit van het navigeren door vensters. Vooruit Indeling van nieuwe vensterwisselaar ophalen Verborgen vensters Pictogram "Bureaublad tonen" invoegen Algemeen Minimalisering Alleen één venster per toepassing Recent gebruikt Omgekeerd Schermen Sneltoetsen Geselecteerd venster tonen Sorteer volgorde: Stapelvolgorde Het huidige geselecteerde venster zal oplichten door het uitvagen van alle andere vensters. deze optie vereist dat bureaubladeffecten actief is. Het effect van het vervangen van lijst van vensters als bureaubladeffecten actief zijn. Virtuele bureaubladen Zichtbare vensters Visualisatie 