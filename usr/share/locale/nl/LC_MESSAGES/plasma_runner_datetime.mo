��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �  �  �     }     �  -   �     �  ,   �          !     &                                   	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2015-01-29 12:58+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 De huidige tijd is %1 Toont de huidige datum Toont de huidige datum in de gegeven tijdzone Toont de huidige tijd Toont de huidige tijd in de gegeven tijdzone datum tijd Vandaag is de datum %1 