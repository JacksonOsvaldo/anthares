��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �  �       �     �     �     �  %   �  $   
	  "   /	      R	  !   s	     �	  
   �	     �	     �	  	   �	  Z   �	      '
  X   H
     �
  	   �
     �
     �
     �
     �
     �
     �
               !     '     ;  (   K  L   t     �     �     �     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-05-05 13:24+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 100% Auteur Copyright 2015 Harald Sitter Harald Sitter Geen toepassingen die geluid afspelen Geen toepassingen die geluid opnemen Geen apparaatprofielen beschikbaar Geen invoerapparaten beschikbaar Geen uitvoerapparaten beschikbaar Profiel: PulseAudio Geavanceerd Toepassingen Apparaten Virtueel uitvoerapparaat toevoegen voor gezamenlijke uitvoer op alle lokale geluidskaarten Geavanceerde uitvoerconfiguratie Automatisch alle actieve streams omschakelen wanneer een nieuwe uitvoer beschikbaar komt Opnemen Standaard Apparaatprofielen freekdekruijf@kde.nl Invoer Geluid dempen Freek de Kruijf Meldingsgeluiden Uitvoer Afspelen Poort  (niet beschikbaar)  (losgekoppeld) Vereist PulseAudio module 'module-gconf' Deze module stelt u in staat het Pulseaudio geluidssubsysteem in te stellen. %1: %2 100% %1% 