��    _                   C   	  /   M  -   }     �     �     �     �      	     	     1	     >	     J	  
   S	     ^	     g	     p	     �	  	   �	     �	     �	     �	  ,   �	  	    
     

  
   )
     4
     L
     `
     u
     �
     �
     �
     �
     �
  	   �
     �
     �
     
               !     (  	   ;     E     ]  
   r     }     �  
   �     �     �     �  
   �     �     �               $     2     @     R     h     y     �     �     �     �  	   �     �     �     �               4     Q     j     �     �     �     �  	   �     �  ,   �          !     0     @     L     S     b     t     �  #   �     �  �  �     j  	   z     �     �     �     �  %   �               %     2     >     E     R     [     k       
   �     �     �     �  @   �  
   "  #   -     Q     `     {     �     �     �     �     �                    4     A  
   V     a     n     �     �     �     �     �     �     �               )     A     J     c  
   q     |     �     �     �     �     �     �            '   5     ]     l     s  	   �     �     �  !   �     �     �  !   �  $        7     W     r     �     �  	   �     �  ;   �               +     =     P     X     f     |  5   �  <   �              Q         Y   5           %       H       J   !              \   *       @   V   4   [       A   (   
       U   B   ]   ^       $   _         '   >       D   K      -                  ?      ,   O   0       R   8   6   W   2   I   =   X   E   #       N   C       T   G   M          3   "         9             +          S          F           ;   :                    &   <   L          P         7          .       /       1                     )            	   Z       @action opens a software center with the applicationManage '%1'... @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Icon: Lock Lock screen Logout Name (Description) Name only Often Used Applications Often Used Documents Often used On All Activities On The Current Activity Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Recently used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show In Favorites Show applications as: Show often used applications Show often used contacts Show often used documents Show recent applications Show recent contacts Show recent documents Show: Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-09-09 15:54+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 '%1' beheren... Kiezen... Pictogram wissen Aan bureaublad toevoegen Aan Favorieten toevoegen Aan paneel toevoegen (widget) Zoekresultaten uitlijnen naar onderen Alle toepassingen %1 (%2) Toepassingen Apps & Docs Gedrag Categorieën Computer Contactpersonen Beschrijving (Naam) Alleen beschrijving Documenten Toepassing bewerken... Toepassingen bewerken... Sessie beëindigen Zoeken uitbreiden naar bladwijzers, bestanden en e-mailberichten Favorieten Menu afvlakken tot een enkel niveau Alles vergeten Alle toepassingen vergeten Alle contactpersonen vergeten Alle documenten vergeten Toepassing vergeten Contactpersoon vergeten Document vergeten Recente documenten vergeten Algemeen %1 (%2) Slaapstand naar schijf %1 verbergen Toepassing verbergen Pictogram: Vergrendelen Scherm vergrendelen Afmelden Naam (beschrijving) Alleen naam Vaak gebruikte toepassingen Vaak gebruikte documenten Vaak gebruikt Op alle activiteiten Op de huidige activiteit Openen met: Pin naar takenbeheerder Plaatsen Energieverbruik / sessie Eigenschappen Herstarten Recente programma's Recente contactpersonen Recente documenten Recent gebruikt Recent gebruikt Verwijderbare opslag Uit Favorieten verwijderen Computer herstarten Commando uitvoeren... Een commando uitvoeren of een zoekvraag Sessie opslaan Zoeken Zoekresultaten Zoeken... Zoeken naar '%1' Sessie Contactpersooninformatie tonen... In Favorieten tonen Toepassing tonen als: Vaak gebruikte toepassingen tonen Vaak gebruikte contactpersonen tonen Vaak gebruikte documenten tonen Recente toepassingen tonen Recente contactpersonen tonen Recente documenten tonen Tonen: Afsluiten Alfabetisch sorteren Start een parallelle sessie onder een andere gebruikersnaam Onderbreken Slaapstand naar RAM Schijf-slaapstand Gebruiker wisselen Systeem Systeemacties Computer uitschakelen Typen om te zoeken. Het verbergen van toepassingen in '%1' ongedaan maken Het verbergen van toepassingen in dit submenu ongedaan maken Widgets 