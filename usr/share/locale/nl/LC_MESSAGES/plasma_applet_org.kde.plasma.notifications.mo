��    &      L  5   |      P      Q     r     ~  -   �  #   �  #   �  ?     1   S     �     �     �  H   �  L   �     F  V   N  N   �     �  
                   (     >     W  2   _  
   �     �  )   �  8   �  #      .   D  -   s  D   �  6   �  0     A   N  =   �  9   �  �  	     �
     �
  $   �
  4   �
     '     4     A     O     a  	   r     |     �     �     �  	   �     �  
   �     �     �               0  	   H     R     b  $   s  +   �     �  )   �  9      $   :     _          �     �     �     �                  "                            
       $                      !                &                                      %      	           #                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Copy Link Address Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2018-01-14 16:46+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 melding Melding van %1 %1 van %2 %3 %1 draaiende taak %1 draaiende taken Meldingen van gebeurtenissen en acties &instellen... 10 s geleden 30 s geleden Details tonen Details verbergen Meldingen wissen Kopiëren Koppelingsadres kopiëren 1 map %2 van %1 mappen 1 bestand %2 van %1 bestanden Geschiedenis %1 van %2 +%1 Informatie Taak mislukt Job voltooid Meer opties... Geen nieuwe meldingen Geen meldingen of taken Openen... %1 [gepauzeerd] Alles selecteren Een geschiedenis van meldingen tonen Meldingen van toepassingen en systeem tonen %1 (%2 resterend) Bestandsoverdracht en andere taken volgen Gebruik aangepaste positie voor de popup voor een bericht %1 minuut geleden %1 minuten geleden %1 dag geleden %1 dagen geleden Gisteren Op dit moment %1: %1: mislukt %1: voltooid 