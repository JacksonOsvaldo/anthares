��            )   �      �  !   �  "   �  '   �  ,     #   ;  &   _  !   �  &   �  '   �     �                 V   $  1   {     �  *   �     �        
     
   "     -     6     ;     D     T     [     a     g  �  p          %     +     7     F     N  
   \     g  	   s     }     �     �     �  g   �  6        N  ,   Z  (   �  &   �     �     �     	     	     	     "	     7	     >	     G	  	   M	                                                                          
                                                    	           @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Borders @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large Application menu Border si&ze: Buttons Close Close by double clicking:
 To open the menu, keep the button pressed until it appears. Close windows by double clicking &the menu button Context help Drag buttons between here and the titlebar Drop here to remove button Get New Decorations... Keep above Keep below Maximize Menu Minimize On all desktops Search Shade Theme Titlebar Project-Id-Version: kcmkwindecoration
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-04-14 02:49+0200
PO-Revision-Date: 2015-10-06 15:12+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Enorm Groot Geen randen Geen zijranden Normaal Veel te groot Heel klein Extra enorm Erg groot Menu van toepassing Rand&grootte: Knoppen Sluiten Sluiten door dubbel te klikken:
 Houd, om het menu te openen, de knop ingedrukt totdat deze verschijnt. Vensters &sluiten door te dubbelklikken op de menuknop Contexthelp Versleep knoppen tussen hier en de titelbalk Hier laten vallen om knop te verwijderen Nieuwe vensterdecoraties verkrijgen... Altijd op voorgrond Altijd op achtergrond Maximaliseren Menu Minimaliseren Op alle bureaubladen Zoeken Oprollen Thema Titelbalk 