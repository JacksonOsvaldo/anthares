��          �      l      �  3   �  $     �   :     �  4   �     �  #        =  �   E  �   �  +   �     �     �     �  1     (   3     \  �   s  $   �  (     �  F  5         6     E     R  D   a      �  +   �     �  �   �  �   �	  0   [
     �
     �
  #   �
  !   �
  7        <     [      j  %   �                                                   	   
                                            1 action for this device %1 actions for this device @info:status Free disk space%1 free Accessing is a less technical word for Mounting; translation should be short and mean 'Currently mounting this device'Accessing... All devices Click to access this device from other applications. Click to eject this disc. Click to safely remove this device. General It is currently <b>not safe</b> to remove this device: applications may be accessing it. Click the eject button to safely remove this device. It is currently <b>not safe</b> to remove this device: applications may be accessing other volumes on this device. Click the eject button on these other volumes to safely remove this device. It is currently safe to remove this device. Most Recent Device No Devices Available Non-removable devices only Open auto mounter kcmConfigure Removable Devices Open popup when new device is plugged in Removable devices only Removing is a less technical word for Unmounting; translation shoud be short and mean 'Currently unmounting this device'Removing... This device is currently accessible. This device is not currently accessible. Project-Id-Version: plasma_applet_devicenotifier
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2016-07-03 22:39+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 1 actie voor dit apparaat %1 acties voor dit apparaat %1 beschikbaar Gebruiken... Alle apparaten Klik om toegang te geven tot dit apparaat vanuit andere programma's. Klik om de schijf uit te werpen. Klik om dit apparaat veilig te verwijderen. Algemeen Het is nu <b>niet veilig</b> om dit apparaat te verwijderen: applicaties kunnen het gebruiken. Klik op de uitwerpknop om dit apparaat te verwijderen. Het is nu <b>niet veilig</b> om dit apparaat te verwijderen: applicaties kunnen andere volumes op dit apparaat gebruiken. Klik op de uitwerpknop van deze volumes om dit apparaat veilig te verwijderen. Het is nu veilig om dit apparaat te verwijderen. Meest recente apparaat Geen apparaten beschikbaar Alleen niet verwijderbare apparaten Verwijderbare apparaten instellen Pop-up openen wanneer een nieuw apparaat is aangesloten Alleen verwijderbare apparaten Verwijderen... Dit apparaat is nu toegankelijk. Dit apparaat is nu niet toegankelijk. 