��          �      �           	          &     -     4  
   =     H     Q     Y     i  >   w     �     �     �     �  
   �     �     �     �            U   %  �  {          &  	   8     B     K  	   W  	   a     k     w  	   �  8   �     �     �     �     �                         3     ?  k   F                       	                                                
                                 %1 is running %1 not running &Reset &Start Advanced Appearance Command: Display Execute command Notifications Remaining time left: %1 second Remaining time left: %1 seconds Run command S&top Show notification Show seconds Show title Text: Timer Timer finished Timer is running Title: Use mouse wheel to change digits or choose from predefined timers in the context menu Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-01-19 14:27+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 is actief %1 is niet actief &Resetten &Starten Geavanceerd Uiterlijk Commando: Beeldscherm Commando uitvoeren Meldingen Resterende tijd: %1 seconde Resterende tijd: %1 seconden Commando uitvoeren S&toppen Melding tonen Seconden tonen Titel tonen Tekst: Timer Timer is geëindigd Timer loopt Titel: Muiswiel gebruiken om cijfers te wijzigen of maak een keuze uit voorgedefinieerde timers in het contextmenu 