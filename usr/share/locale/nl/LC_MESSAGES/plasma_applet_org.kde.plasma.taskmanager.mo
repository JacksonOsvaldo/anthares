��    Q      �  m   ,      �     �     �     �                      .   !  U   P     �     �      �     �  /   �  /   -     ]     i     r  
   ~     �     �  $   �     �     �     �     �     	     	  	   *	     4	  
   F	  7   Q	     �	     �	     �	     �	  	   �	     �	  !   �	     
     
  	   !
      +
     L
     Y
     r
     �
     �
     �
     �
     �
     �
     �
  (   �
       =   '  2   e     �     �  "   �     �       J     1   Y  )   �  (   �  '   �  "     4   )     ^     l     r     {     �     �     �  U   �  N   !  9   p  J   �  �  �     �     �     �     �     �     �  	   �     �     �           3  ,   E     r  =   ~  =   �  	   �                    (     9     R     p          �     �     �     �     �     �     �     	          #     9     X     ^  	   m  )   w     �     �     �  )   �     	  #        9     V     s     z     �     �     �     �  0   �                &     -     6     E  	   R     \     n     w  ,   �  -   �  (   �  +     6   G     ~     �  
   �     �     �     �     �     �     �                    L   )             O                      4              6   $   ?   3         B              (   K   Q               ;       .   '   D   2   -       J   >   !   
   8                       P   5         ,          7   <           F      *   @       H   1                 "          9      +   &   0           /   N          #       E   	             I   A   =   %      G   :   C                   M    &All Desktops &Close &Fullscreen &Move &New Desktop &Pin &Shade 1 = number of desktop, 2 = desktop name&%1 %2 Activities a window is currently on (apart from the current one)Also available on %1 Add To Current Activity All Activities Allow this program to be grouped Alphabetically Always arrange tasks in columns of as many rows Always arrange tasks in rows of as many columns Arrangement Behavior By Activity By Desktop By Program Name Close Window or Group Cycle through tasks with mouse wheel Do Not Group Do Not Sort Filters Forget Recent Documents General Grouping and Sorting Grouping: Highlight windows Icon size: Invalid number of new messages, overlay, keep short— Keep &Above Others Keep &Below Others Keep launchers separate Large Ma&ximize Manually Mark applications that play audio Maximum columns: Maximum rows: Mi&nimize Minimize/Restore Window or Group More Actions Move &To Current Desktop Move To &Activity Move To &Desktop Mute New Instance On %1 On All Activities On The Current Activity On middle-click: Only group when the task manager is full Open groups in popups Open or bring to the front window of media player appRestore Over 9999 new messages, overlay, keep short9,999+ Pause playbackPause Play next trackNext Track Play previous trackPrevious Track Quit media player appQuit Re&size Remove launcher button for application shown while it is not runningUnpin Show all user Places%1 more Place %1 more Places Show only tasks from the current activity Show only tasks from the current desktop Show only tasks from the current screen Show only tasks that are minimized Show progress and status information in task buttons Show tooltips Small Sorting: Start New Instance Start playbackPlay Stop playbackStop The click actionNone Toggle action for showing a launcher button while the application is not running&Pin When clicking it would toggle grouping windows of a specific appGroup/Ungroup Which activities a window is currently onAvailable on %1 Which virtual desktop a window is currently onAvailable on all activities Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2018-01-16 12:41+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Alle bureaubladen Sl&uiten &Volledig scherm &Verplaatsen &Nieuw bureaublad Vast&pinnen Op&rollen &%1 %2 Ook beschikbaar op %1 Aan huidige activiteit toevoegen Alle activiteiten Toestaan dat dit programma wordt gegroepeerd Alfabetisch Taken altijd in kolommen van zo veel mogelijke rijen plaatsen Taken altijd in rijen van zo veel mogelijke kolommen plaatsen Plaatsing Gedrag Op activiteit Per bureaublad Op programmanaam Venster of groep sluiten Door taken lopen met muiswiel Niet groeperen Niet sorteren Filters Recente documenten vergeten Algemeen Groepering en sortering Groepering: Venster op laten lichten Pictogramgrootte: — Boven &anderen houden Onder an&deren houden Programmastarters apart houden Groot Ma&ximaliseren Handmatig Toepassingen markeren die geluid afspelen Maximum aantal kolommen: Maximum aantal rijen: Mi&nimaliseren Venster of groep minimaliseren/herstellen Meer acties &Naar huidig bureaublad verplaatsen Naar &activiteit verplaatsen Naar bureaubla&d verplaatsen Dempen Nieuw exemplaar Op %1 Op alle activiteiten Op de huidige activiteit Bij midden-klik: Alleen groeperen wanneer de taakbeheerder vol is Groepen openen in pop-ups Terugzetten 9.999+ Pauzeren Volgende track Vorige track Afsluiten &Grootte wijzigen Losmaken %1 plaats meer %1 plaatsen meer Alleen taken van de huidige activiteit tonen Alleen taken van het huidige bureaublad tonen Alleen taken op het huidige scherm tonen Alleen taken die geminimaliseerd zijn tonen Voortgang- en statusinformatie tonen in de taakknoppen Tekstballonnen tonen Klein Sortering: Een nieuw exemplaar starten Afspelen Stoppen Geen Vast&pinnen Groeperen/groepering opheffen Beschikbaar op %1 Beschikbaar op alle activiteiten 