��    2      �  C   <      H     I     M     R  .   a  5   �     �     �     �     �  	   �     �          '     6  1   J     |     �     �     �  
   �     �  '   �     �     �     �          !  '   (     P     _     f     n     �  5   �     �     �     �     �     �  $   �  A   #  �   e     K     R  C   c  '   �     �     �     �  �  	     �
     �
     �
     �
     �
  	   �
     �
            	   (     2     P     e     v  -   �  	   �     �  "   �     �            
   )     4  
   7     B     b  
   o  1   z     �     �  
   �     �  	   �  1   �     *     @     L     U     h  	   {     �     �     �     �     �     �     �          
               $   0       .                      %                ,         )   2          /   "                        &   1      '       +   !   	         -                               
         (                    *            #       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Show media controls: Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) in category recent queries Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2018-01-10 01:00+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1% Terug Batterij op %1% Indeling omschakelen Virtueel toetsenbord Annuleren Caps Lock staat aan Sluiten Zoeken afsluiten Instellen Plugins voor zoeken instellen Bureaubladsessie: %1 Andere gebruiker Toetsenbordindeling: %1 Na 1 seconde afmelden Na %1 seconden afmelden Aanmelden Aanmelden mislukt Aanmelden als een andere gebruiker Afmelden Volgende track Er speelt geen medium Ongebruikt OK Wachtwoord Afspelen of pauzeren van medium Vorige track Herstarten Herstarten in 1 seconde Herstarten in %1 seconden Recente queries Verwijderen Herstarten Besturing van media tonen: Afsluiten Afsluiten na 1 seconde. Afsluiten na %1 seconden. Nieuwe sessie starten Onderbreken Wisselen Sessie omschakelen Gebruiker wisselen Zoeken... Zoeken naar '%1'... Plasma gemaakt door KDE Ontgrendelen Ontgrendelen is mislukt op TTY %1 (scherm %2) TTY %1 Gebruikersnaam %1 (%2) Recente queries in categorie 