��          �      <      �     �     �     �  +   �  @        L     a     i     w     }     �  
   �     �     �     �     �     �  �  
     �     �     �  /   �  [        g     ~     �     �     �     �     �     �     �     �       '        
         	                                                                                       <a href='%1'>%1</a> Close Copy Automatically: Don't show this dialog, copy automatically. Drop text or an image onto me to upload it to an online service. Error during upload. General History Size: Paste Please wait Please, try again. Sending... Share Shares for '%1' Successfully uploaded The URL was just shared Upload %1 to an online service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-01 04:04+0100
PO-Revision-Date: 2015-04-25 16:33+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 <a href='%1'>%1</a> Sluiten Automatisch kopiëren: Deze dialoog niet tonen, automatisch kopiëren. Sleep een stuk tekst of een afbeelding naar mij om het naar een online-service te uploaden. Fout bij het uploaden. Algemeen Geschiedenisgrootte: Plakken Even geduld Probeer opnieuw. Bezig met verzenden... Delen Shares voor '%1' Met succes geüpload De URL is gewoon gedeeld %1 naar een een online service uploaden 