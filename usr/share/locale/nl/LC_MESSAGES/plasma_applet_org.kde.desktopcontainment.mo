��    `        �         (     )     1     B     Q     W     ^     j     {     �     �  /   �  -   �     �     �  
   		  
   	     	     ,	     1	     8	     @	     R	     _	     d	  
   l	     w	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     
     
  	   #
     -
     4
     H
  	   M
     W
     ]
     c
     h
     m
  	   v
     �
     �
     �
     �
     �
     �
     �
     �
  <   �
               /     6     =     X     ^     e     j  
   ~     �     �     �     �     �  )   �               5     :     @     M     U     ]     f  
   x     �     �  	   �     �     �     �     �     �     �     �  E   �  *   A  �  l     
          )     F     N     W     f     }  	   �  
   �  	   �     �  	   �  
   �  
   �  
   �  	   �     �  	   �               %     6  	   <     F     O     \     o  !   �     �     �     �     �     �  %   �          '     4  	   A  (   K  
   t          �     �     �     �     �     �     �     �     �     �                      K         l      �     �     �  
   �     �     �     �     �     �     
          0  $   E     j  4   }     �  ,   �     �     �                 
   +     6     G     S     Y     `     o     v  !   {     �  !   �     �     �  S   �     L     J       -      M       %   Q           ^              U       L   S   $      .       H       X          Y   8   *         <   &          '       ]   N       3                     `          #   V             /   T   G   9   @   >   =   6   2   O   C   F   ?      0      
      A   E       	      P              _                D          Z               1   R   ,   +                \           I   )   B   K      :   !   W   4          ;               (      5   "       7       [           &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Appearance: Arrange In Arrange in Arrangement: Back Cancel Columns Configure Desktop Custom title Date Default Descending Description Deselect All Desktop Layout Enter custom title here Features: File name pattern: File type File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Location: Lock in place Locked Medium More Preview Options... Name None OK Panel button: Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sort by Sorting: Specify a folder: Text lines Tiny Title: Tool tips Tweaks Type Type a path or a URL here Unsorted Use a custom icon Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. whether to use icon or list viewView mode Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-11-18 11:46+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Verwij&deren Prullenbak l&egen &Naar prullenbak verplaatsen &Openen P&lakken Eigenscha&ppen Bureaublad ve&rnieuwen Weergave ve&rnieuwen &Herladen He&rnoemen Kiezen... Pictogram wissen Uitlijnen Uiterlijk: Ordenen op Ordenen in Ordening: Terug Annuleren Kolommen Bureaublad instellen Aangepaste titel Datum Standaard Aflopend Beschrijving Alles deselecteren Indeling van het bureaublad Voer hier een aangepaste titel in Mogelijkheden: Bestandsnaampatroon: Bestandstype Bestandstypen: Filter Pop-ups van voorbeeldweergave van map Mappen eerst Mappen eerst Volledig pad Ontvangen Bestanden verbergen die overeenkomen met Zeer groot Pictogramgrootte Pictogrammen Groot Links Lijst Locatie Locatie: Positie vergrendelen Vergrendeld Middelgrootte Meer voorbeeldopties... Naam Geen OK Paneelknop: Widgets indrukken en vasthouden om ze te verplaatsen en hun besturing tonen Plug-ins voor voorbeeld Voorbeeldweergave van miniaturen Verwijderen Grootte wijzigen Herstellen Rechts Draaien Rijen Bestandstype zoeken... Alles selecteren Map selecteren Markering van selectie Alle bestanden tonen Bestanden tonen die overeenkomen met Een locatie tonen: Bestanden gekoppeld aan de huidige activiteit tonen  De bureaubladmap tonen De gereedschapskist van het bureaublad tonen Grootte Klein Middel klein Sorteren op Sorteren naar Sortering: Een map opgeven: Tekstregels Klein Titel: Tekstballonnen Tweaks Type Voer hier een pad of URL-adres in Niet gesorteerd Een aangepast pictogram gebruiken Besturing van Widget Widgets ontgrendeld U kunt widgets indrukken en vasthouden om ze te verplaatsen en hun besturing tonen. Weergavemodus 