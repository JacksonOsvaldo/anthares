��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  �  �  7   v     �  /   �     �               7     Q     `     q  
   y     �     �     �  	   �  
   �     �     �     �          /  	   @  	   J        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-10-25 22:15+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 geminimaliseerd venster: %1 minimaliseerde vensters: %1 venster: %1 vensters: ...en %1 ander venster ...en %1 andere vensters Naam activiteit Nummer activiteit Virtueel bureaublad toevoegen Bureaubladen instellen... Bureaubladnaam Bureaubladnummer Scherm: Doet niets Algemeen Horizontaal Pictogrammen Indeling: Geen tekst Alleen het huidige scherm Virtueel bureaublad verwijderen Huidig bureaublad selecteren Activiteitbeheerder tonen... Bureaublad tonen Standaard Verticaal 