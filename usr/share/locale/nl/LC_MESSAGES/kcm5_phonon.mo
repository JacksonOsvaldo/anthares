��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  N   {  �   �     N  X   l  #   �     �  ;   �     9  :   L     �     �  	   �     �  .   �  -   �  ,   "  (   O     x  k   �     �       �        �  ;   �     �     
          5     D     b     k     �     �  '   �     �     �     �     �                          .     ;     I     V     s          �     �     �  	   �     �      �  	   �  �        �  ?   �  �   �     }  :   �  3   �  2   �  (   1  '   Z     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-04-08 19:10+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Om de wijziging in de backend toe te passen moet u zich af- en weer aanmelden. Een lijst met Phonon-backends die op uw systeem zijn gevonden. De volgorde hier bepaalt de volgorde waarin Phonon ze zal gebruiken. Apparaatlijst toepassen op... De nu getoonde voorkeurlijst toepassen op de volgende andere geluidsafspeelcategorieën: Geluidsinstellingen van de hardware Afspelen van geluid Voorkeur voor geluidsafspeelapparaat voor de '%1' categorie Opnemen van geluid Voorkeur voor geluidsopnameapparaat voor de '%1' categorie Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Voorkeur voor standaard geluidsafspeelapparaat Voorkeur voor standaard geluidsopnameapparaat Voorkeur voor standaard video-opnameapparaat Standaard/niet-gespecificeerde categorie Achterstellen Bepaalt de standaardvolgorde van de apparaten, welke kunnen worden omzeilt via de individuele categorieën. Apparaatinstellingen Apparaatvoorkeur Apparaten gevonden in uw systeem, die geschikt zijn voor de geselecteerde categorie. Kies het apparaat dat u wilt gebruiken door de toepassingen. freekdekruijf@kde.nl Het geselecteerd geluiduitvoerapparaat instellen is mislukt Vooraan midden Vooraan links Vooraan links van het midden Vooraan rechts Vooraan rechts van het midden Hardware Onafhankelijke apparaten Invoerniveaus Ongeldig KDE geluidsinstellingen van de hardware Matthias Kretz Mono Freek de Kruijf Phonon Configuratiemodule Afspelen (%1) Voorkeur Profiel Achter midden Achter links Achter rechts Opnemen (%1) Geavanceerde apparaten tonen Opzij links Opzij rechts Geluidskaart Geluidsapparaat Luidsprekeropstelling en testen Subwoofer Test Het geselecteerd apparaat testen %1 testen De volgorde bepaalt de voorkeur van de apparaten. Als om welke reden dan ook het eerste apparaat niet gebruikt kan worden zal Phonon de tweede proberen, etc. Onbekend kanaal De nu getoonde apparatenlijst voor meer categorieën gebruiken. Diverse soorten media gebruiken voorbeelden. Voor elke categorie kunt u kiezen welk apparaat gebruikt moet worden door Phonon-toepassingen. Opnemen van video Voorkeur voor video-opnameapparaat voor de '%1' categorie  Uw backend kan mogelijk geen opname van geluid doen Uw backend kan mogelijk geen opname van video doen geen voorkeur voor geselecteerd apparaat geselecteerd apparaat heeft de voorkeur 