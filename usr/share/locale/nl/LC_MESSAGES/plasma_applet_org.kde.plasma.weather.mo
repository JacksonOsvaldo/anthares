��    O      �  k         �     �  ^   �  H     
   f     q     �     �     �     �  '   �     �  "   �          4     K  
   T     _     x     �  	   �     �     �     �     �     �     �  "   �     	      	  	   8	     B	  !   I	     k	  !   �	  ?   �	     �	     �	     �	     
     
     (
     <
  ;   H
  &   �
      �
  %   �
     �
          &     ?  >   X     �     �  '   �  +   �  !   !     C     c     t     �     �     �     �     �     �     �               +     >     P     b     s     �     �     �     �  3   �  �       �     �     �  	   �     �     �     �     �     �               -     9     B     S  	   \     f     }  	   �     �     �     �     �     �     �     �  $   �            
        (     /     M  $   O     t     �     �     �     �     �     �     �     �     �                     4     ;     D     S     f     w  	   |     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                    /       ?       $         K   M   !   )      @                                 2   E       '               1          6              O   <      H   J      8       7      >       =   0   *          .      ,   :      N   D   G   C   3   	   #   +   4                F          %   A                   5       &   ;              9   
           -                      (   "   L      B   I             min %1 is the weather condition, %2 is the temperature,  both come from the weather provider%1 %2 A weather station location and the weather service it comes from%1 (%2) Appearance Beaufort scale bft Celsius °C Degree, unit symbol° Details Fahrenheit °F Forecast period timeframe1 Day %1 Days Hectopascals hPa High & Low temperatureH: %1 L: %2 High temperatureHigh: %1 Inches of Mercury inHg Kelvin K Kilometers Kilometers per Hour km/h Kilopascals kPa Knots kt Location: Low temperatureLow: %1 Meters per Second m/s Miles Miles per Hour mph Millibars mbar N/A No weather stations found for '%1' Notices Percent, measure unit% Pressure: Search Select weather services providers Short for no data available- Show temperature in compact mode: Shown when you have not set a weather providerPlease Configure Temperature: Units Update every: Visibility: Weather Station Wind conditionCalm Wind speed: certain weather condition (probability percentage)%1 (%2%) content of water in airHumidity: %1%2 distance, unitVisibility: %1 %2 ground temperature, unitDewpoint: %1 humidex, unitHumidex: %1 pressure tendencyfalling pressure tendencyrising pressure tendencysteady pressure tendency, rising/falling/steadyPressure Tendency: %1 pressure, unitPressure: %1 %2 temperature, unit%1%2 visibility from distanceVisibility: %1 weather services provider name (id)%1 (%2) weather warningsWarnings Issued: weather watchesWatches Issued: wind directionE wind directionENE wind directionESE wind directionN wind directionNE wind directionNNE wind directionNNW wind directionNW wind directionS wind directionSE wind directionSSE wind directionSSW wind directionSW wind directionVR wind directionW wind directionWNW wind directionWSW wind direction, speed%1 %2 %3 wind speedCalm windchill, unitWindchill: %1 winds exceeding wind speed brieflyWind Gust: %1 %2 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-28 05:50+0100
PO-Revision-Date: 2018-01-16 00:12+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  min %1 %2 %1 (%2) Uiterlijk Beaufortschaal bft Celsius °C ° Details Fahrenheit °F 1 dag %1 dagen Hectopascal hPa H: %1 L: %2 Hoog: %1 Inches kwik inHg Kelvin K Kilometer Kilometer per uur km/u Kilopascal kPa Knopen kt Locatie: Laag: %1 Meters per seconde m/s Mijl Mijl per uur mph Millibar mbar N.v.t. Geen weerstations gevonden voor '%1' Opmerkingen % Luchtdruk: Zoeken Leveranciers van Weerservices - Temperatuur in compacte modus tonen: Gaarne configuratie uitvoeren Temperatuur: Eenheden Bijwerken elke: Zicht: Weerstation Rustig Windsnelheid: %1 (%2%) Luchtvochtigheid: %1%2 Zicht: %1 %2 Dauwpunt: %1 Luchtvochtigheidindex: %1 dalend stijgend gelijkblijvend Luchtdruktrend: %1 Luchtdruk: %1 %2 %1%2 Zicht: %1 %1 (%2) Uitgebrachte waarschuwingen: Bewaking uitgebracht: O ONO OZO N NO NNO NNW NW Z ZO ZZO ZZW ZW VR W WNW WZW %1 %2 %3 Rustig Gevoelstemperatuur: %1 Windvlagen: %1 %2 