��            )         �     �  "   �     �  !   �  !        4  
   J     U     p     �  !   �     �  0   �  8        ?  !   ]          �     �     �     �                '  
   /  
   :  
   E  &   P     w     �  �  �     6  *   :  !   e  '   �  #   �     �  	   �  !   �       !   "  (   D  )   m  2   �  B   �  $   	  ,   2	  #   _	  !   �	  /   �	  $   �	     �	  8   
     I
     P
     Y
  	   f
     p
  )   }
     �
     �
                                                                                                 	                       
                 ms &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Anima&tions duration: Animations Bottom arrow button t&ype: Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw focus indicator in lists Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2017-10-17 09:56+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  ms Zichtbaarheid van &toetsenbordversnellers: Type van &bovenste pijltjestoets: Toetsenbordversnellers altijd verbergen Toetsenbordversnellers altijd tonen Anima&tieduur: Animaties T&ype van onderste pijltjestoets: Breeze-instellingen Tabbladen op de tabbalk centreren Versleep vensters met alle lege gebieden Versleep vensters alleen met de titelbalk Versleep vensters met de titel-, menu en werkbalk  Teken een dunne lijn om focus in menu's en menubalken aan te geven Focusindicator laten zien in lijsten Frames tekenen rond de te verankeren panelen Frames tekenen rond de paginatitels Frames tekenen rond de zijpanelen Schaalstreepjes van schuifregelaar voor tekenen Scheiding tussen werkbalkitems tonen Animaties inschakelen Uitgebreide handvatten voor grootte wijzigen inschakelen Frames Algemeen Geen knoppen Eén knop Schuifbalken Toetsenbordversnellers indien nodig tonen Twee knoppen Sleepmodus van W&indows: 