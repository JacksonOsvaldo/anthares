��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �  �  �     �	     �	     �	     �	     �	  
   �	  
   �	     �	     �	     �	     �	     �	     	
     
     '
     /
     >
     ]
     f
     w
     �
     �
     �
     �
     �
     �
     �
     �
       	   !     +     1     A     E     Z     b  
   j  	   u          �     �     �  ?   �     �     �  	   �     	               1     3     6     9     H     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-06-17 12:21+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % %1 %2 %1: Energiegebruik van toepassing Batterij Capaciteit Percentage Ladingsstatus Opladen Huidige °C Details: %1 Ontladen freekdekruijf@kde.nl Energie Energiegebruik Statistiek over energiegebruik Omgeving Volledig ontwerp Volledig opgeladen Heeft stroomvoorziening Kai Uwe Broulik Laatste 12 uur Laatste 2 uur Laatste 24 uur Laatste 48 uur Laatste 7 dagen Laatst volledig geladen Laatste uur Fabrikant Model Freek de Kruijf Nee Niet bezig met laden PID: %1 Pad: %1 Oplaadbaar Verversen Serienummer W Systeem Temperatuur Dit type geschiedenis is nu niet beschikbaar voor dit apparaat. Periode Weer te geven periode Fabrikant V Voltage Wakeups per seconde: %1 (%2%) W Wh Ja Energiegebruik % 