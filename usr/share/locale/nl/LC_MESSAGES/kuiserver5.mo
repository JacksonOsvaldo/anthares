��          �   %   �      P     Q     b     w     �     �     �  
   �     �     �     �     �     �  /        3     Q     p     v     �     �     �     �     �     �     �       �  %     �     �     �     �          #  	   7     A  	   H     R     Y     f  :   v  "   �  #   �     �            	        &     D  %   b  %   �     �     �                            
                                                                                             	    %1 file %1 files %1 folder %1 folders %1 of %2 processed %1 of %2 processed at %3/s %1 processed %1 processed at %2/s Appearance Behavior Cancel Clear Configure... Finished Jobs List of running file transfers/jobs (kuiserver) Move them to a different list Move them to a different list. Pause Remove them Remove them. Resume Show all jobs in a list Show all jobs in a list. Show all jobs in a tree Show all jobs in a tree. Show separate windows Show separate windows. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-06-17 23:05+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 bestand %1 bestanden Map %1 mappen %1 van %2 verwerkt %1 van %2 verwerkt op %3/s %1 verwerkt %1 verwerkt op %2/s Uiterlijk Gedrag Annuleren Wissen Instellen... Voltooide taken Lijst met draaiende bestandsoverdrachten/taken (kuiserver) Verplaats ze naar een andere lijst Verplaats ze naar een andere lijst. Pauzeren Verwijder ze Verwijder ze. Hervatten Alle taken in een lijst tonen Toon alle taken in een lijst. Alle taken in een boomstructuur tonen Toon alle taken in een boomstructuur. Gescheiden vensters tonen Toon gescheiden vensters. 