��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b  �  �     2     L     X     m     �     �     �     �  )   �  %   �  (   !  !   J  1   l         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2017-10-17 09:57+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Bureaubladthema instellen David Rosca freekdekruijf@kde.nl Nieuwe thema's ophalen... Uit bestand installeren... Freek de Kruijf Thema openen Thema verwijderen Themabestanden (*.zip *.tar.gz *.tar.bz2) Installatie van het thema is mislukt. Het thema werd succesvol geïnstalleerd. Verwijderen van thema is mislukt. Deze module laat u het bureaubladthema instellen. 