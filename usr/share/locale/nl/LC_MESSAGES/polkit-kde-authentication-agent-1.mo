��            )   �      �  ;   �  J   �          /  ~   7  A   �     �       )        G     X     i      q     �     �     �  
   �     �  
   �     �             "   <     _  	   y     �     �     �  �  �     J     R     V     m  |   t  U   �     G     S  *   i     �     �     �     �     �     �     	     	     	     -	     :	     M	      b	  "   �	     �	     �	     �	     �	  
   �	                                      
                                                                                            	        %1 is the full user name, %2 is the user login name%1 (%2) %1 is the name of a detail about the current action provided by polkit%1: (c) 2009 Red Hat, Inc. Action: An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Another client is already authenticating, please try again later. Application: Authentication Required Authentication failure, please try again. Click to edit %1 Click to open %1 Details EMAIL OF TRANSLATORSYour emails Former maintainer Jaroslav Reznik Lukáš Tinkl Maintainer NAME OF TRANSLATORSYour names P&assword: Password for %1: Password for root: Password or swipe finger for %1: Password or swipe finger for root: Password or swipe finger: Password: PolicyKit1 KDE Agent Select User Vendor: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:22+0100
PO-Revision-Date: 2015-01-09 11:53+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 %1 (%2) %1: (c) 2009 Red Hat, Inc. Actie: Een toepassing probeert een actie uit te voeren die privileges vereist. Verificatie is vereist om deze actie te kunnen doen. Voor een andere cliënt wordt de authenticatie gedaan, gaarne later opnieuw proberen. Toepassing: Authenticatie vereist Authenticatie is mislukt, probeer opnieuw. Klik om %1 te bewerken Klik om %1 te openen Details freekdekruijf@kde.nl Voormalig onderhouder Jaroslav Reznik Lukáš Tinkl Onderhouder Freek de Kruijf W&achtwoord: Wachtwoord van %1: Wachtwoord van root: Wachtwoord of vingerveeg van %1: Wachtwoord of vingerveeg van root: Wachtwoord of vingerveeg: Wachtwoord: PolicyKit1 KDE Agent Gebruiker selecteren Fabrikant: 