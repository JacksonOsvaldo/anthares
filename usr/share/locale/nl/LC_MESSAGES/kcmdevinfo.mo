��    ]           �      �  !   �       	   !     +  S   4  	   �     �     �     �     �     �     �     �     �     �     	  J   $	     o	     }	     �	      �	  	   �	  
   �	     �	     �	     �	     �	     
     
     
  L   
  	   l
  	   v
  
   �
  
   �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     	     (  	   +     5     G     S     [     i     m     }     �     �  
   �  	   �     �  
   �     �     �     �     �     �     	          +     ?     \     r     x     |     �  H   �     �     �     �     �     �       
     &        A  "   T     w     �     �     �     �       	     �  (  2   �       	        #  $   ,     Q     X     d     j     q     z     �     �     �     �     �  .   �       	        '     0     E     Q     a     q     �     �     �     �     �  4   �  	   �  	   �  
   	  
     
        *     6     I     Q     e     x     �     �     �     �     �     �     �     �     �                    '     4  
   F  	   Q     [     `     o     t     y     �     �     �     �     �     �          0     6     :     >  9   E       
   �     �     �     �     �  
   �     �     �     �     �     �                         (           B   "       (   W   #   $   %            [   @      L   M   '   6       >   T           <   9   G   &           A      .   8   R       7       J             ?   4                   H   -                      F          5   )   N   D       ,       K             3       U   =   
       ;       O   :           +           !   Z   /      V   Y              Q         E       1       X             ]       0      P         \   I          	          C       S   2       *    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Device Viewer Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2016-03-09 10:15+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 
Weergavemodule voor op Solid gebaseerde apparaten (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 beschikbaar uit %2 (%3% gebruikt) Accu's Type accu:  Bus:  Camera Camera's Ladingstatus:  Opladen Alles invouwen Compact Flash-lezer Een apparaat Apparaatinformatie Alle apparaten die nu in de lijst staan tonen. Apparaatweergaveprogramma Apparaten Ontladen freekdekruijf@kde.nl Versleuteld Alles uitvouwen Bestandssysteem Type bestandssysteem:  Volledig geladen Harde schijf Hotplugbaar? IDE IEEE1394 Toont informatie over het nu geselecteerde apparaat. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Toetsenbord Toetsenbord + muis Label:  Maximale snelheid:  Geheugensticklezer Aangekoppeld op:  Muis Multimediaspelers Freek de Kruijf Nee Geen lading Geen gegevens beschikbaar Niet aangekoppeld Niet ingesteld Optisch apparaat PDA Partitietabel Primair Processor %1 Processornummer:  Processors Product:  RAID Verwijderbaar? SATA SCSI SD/MMC-lezer Alle apparaten tonen Relevante apparaten tonen Smart Media-lezer Opslagapparaten Ondersteunde stuurprogramma's:  Ondersteunde instructiesets:  Ondersteunde protocollen:  UDI:  UPS USB UUID:  Toont het huidige apparaat UDI (Unique Device Identifier) Onbekend apparaat Ongebruikt Leverancier:  Volumeruimte: Gebruik van het volume:  Ja kcmdevinfo Onbekend Geen Geen Platform Onbekend Onbekend Onbekend Onbekend Onbekend xD-lezer 