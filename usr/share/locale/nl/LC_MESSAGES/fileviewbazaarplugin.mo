��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  ,   }	  /   �	  5   �	     
  )   (
     R
  (   r
  %   �
     �
     �
  *   �
      &  !   G  /   i  1   �  ;   �           (     ?      W     x  '   �     �     �     �     	     &  
   9     D     S     g     x                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2011-09-07 23:52+0200
Last-Translator: Freek de Kruijf <f.de.kruijf@gmail.com>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=2; plural=n != 1;
 Toegevoegde bestanden aan Bazaar-repository. Bestanden toevoegen aan de Bazaar-repository... Bestanden toevoegen aan Bazaar-repository is mislukt. Bazaar-log is gesloten. Bazaar-wijzigingen vastleggen is mislukt. Vastgelegde Bazaar-wijzigingen. Bazaar-wijzigingen vastleggen(commit)... Bazaar-repository ophalen is mislukt. Bazaar-repository opgehaald. Bazaar-repository ophalen... Bazaar-repository wegschrijven is mislukt. Bazaar-repository weggeschreven. Bazaar-repository wegschrijven... Verwijderde bestanden uit de Bazaar-repository. Bestanden verwijderen uit de Bazaar-repository... Verwijderen van bestanden uit Bazaar-repository is mislukt. Wijzigingen nakijken is mislukt. Nagekeken wijzigingen. Wijzigingen nakijken... Bazaar-log uitvoeren is mislukt. Bazaar-log uitvoeren... Bazaar-repository bijwerken is mislukt. Bazaar-repository bijgewerkt. Bazaar-repository bijwerken... Bazaar-toevoegen... Bazaar-vastleggen(commit)... Bazaar-verwijderen Bazaar-log Bazaar-ophalen Bazaar-wegschrijven Bazaar-bijwerken Lokale Bazaar-wijzigingen tonen 