��          �      l      �     �     �     �  *         +  >   ?  9   ~     �     �  
   �      �       	        (  !   :     \  $   {  	   �     �  �   �  �  8     �     �     �  +   �     #  G   7  K        �     �  
   �               &     =     Q     p  &   �  	   �     �  �   �                              
                                                  	                  % &Critical level: &Low level: <b>Battery Levels                     </b> A&t critical level: Battery will be considered critical when it reaches this level Battery will be considered low when it reaches this level Configure Notifications... Critical battery level Do nothing EMAIL OF TRANSLATORSYour emails Enabled Hibernate Low battery level Low level for peripheral devices: NAME OF TRANSLATORSYour names Pause media players when suspending: Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-11 03:05+0200
PO-Revision-Date: 2017-05-11 10:13+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 % &Kritiek niveau: &Laag niveau: <b>Batterijniveaus                     </b> O&p kritiek niveau: Batterijcapaciteit is kritiek laag wanneer het dit niveau heeft bereikt De batterijcapaciteit wordt als laag beschouwd als dit niveau bereikt wordt Meldingen instellen... Kritiek batterijniveau Niets doen freekdekruijf@kde.nl Ingeschakeld Slaapstand naar schijf Laag batterijniveau Laag niveau voor randapparaat: Freek de Kruijf Mediaspelers pauzeren bij onderbreken: Afsluiten Onderbreken De service energiebeheer lijkt niet actief te zijn.
Dit kan worden opgelost door het op te starten of te plannen in "Opstarten en afsluiten" 