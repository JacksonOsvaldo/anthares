��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "  �  $  C   �            !   .     P  #   c     �  8   �  I   �                           
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-06-03 11:28+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Verzekeren dat het scherm vergrendeld wordt alvorens te gaan slapen Sessie vergrendelen Schermvergrendeling Scherm vergrendeling ingeschakeld Scherm vergrendeld Tijdslimiet op de schermbeveiliging Scherm ontgrendeld Stelt de minuten in waarna het scherm wordt vergrendeld. Stelt in of het scherm vergrendeld zal worden na de gespecificeerde tijd. De vergrendeling van het scherm is gebroken en ontgrendelen is niet meer mogelijk.
Om te ontgrendelen schakel over naar een virtuele terminal (bijv. Ctrl+Alt+F2),
meldt u aan en voer het commando:

loginctl unlock-session %1

uit. Schakel daar terug naar de actieve sessie (Ctrl+Alt+F%2). 