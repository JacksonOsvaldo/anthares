��          �      L      �  
   �     �  >   �                  
   -     8     @     H     O  	   [     e     s     z  2   �     �     �  2  �            F        _     f     l     y  	   �     �  	   �     �     �     �     �     �  3   �     .     C                                   	                                                         
        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2016-11-19 11:56+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Files: kfarch.cpp kfdird.cpp kfind.cpp kfindtop.cpp kfoptions.cpp kfsave.cpp kftabdlg.cpp kftypes.cpp kfwin.cpp main.cpp mkfdird.cpp mkfind.cpp 
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 B&laderen... &Zoeken: *.png *.xpm *.svg *.svgz|Pictogrambestanden (*.png *.xpm *.svg *.svgz) Acties Alles Toepassingen Categorieën Apparaten Emblemen Emoticons Pictogrambron Mime-bestandstypen &Overige pictogrammen: Locaties S&ysteempictogrammen: Interactief zoeken naar pictogramnamen (zoals map). Pictogram selecteren Status 