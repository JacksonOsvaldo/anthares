��    $      <  5   \      0     1  +   E     q  4   �  &   �     �     �                     5     :     P     X  ,   u  3   �     �     �               !  '   .     V     u     {     �     �     �     �     �     �  &   �       B        b  5  y     �     �     �     �     �     	     $	  
   1	     <	  w   I	     �	     �	     �	  #   �	  .   
  4   G
     |
  "   �
     �
  
   �
     �
  )   �
  �   
     �     �  #   �             	   >      H     i  /   q     �  D   �     �                                       
                              !                                  $      #                               	                           "             @info:creditAuthor @info:creditCopyright 2006 Sebastian Sauer @info:creditSebastian Sauer @info:shell command-line argumentThe script to run. @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: application descriptionCommand-line utility to run Kross scripts. application nameKross Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-04-05 23:58+0200
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Files: kfarch.cpp kfdird.cpp kfind.cpp kfindtop.cpp kfoptions.cpp kfsave.cpp kftabdlg.cpp kftypes.cpp kfwin.cpp main.cpp mkfdird.cpp mkfind.cpp
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 Auteur Copyright 2006 Sebastian Sauer Sebastian Sauer Het uit te voeren script. Algemeen Een nieuw script toevoegen. Toevoegen... Annuleren? Toelichting: rinsedevries@kde.nl,wbsoft@xs4all.nl,bramschoenmakers@kde.nl,tomalbers@kde.nl,tijmenbaarda@kde.nl,,freekdekruijf@kde.nl Bewerken Geselecteerd script bewerken. Bewerken... Het geselecteerde script uitvoeren. Kon script niet aanmaken voor interpreter "%1" Kon interpreter voor scriptbestand "%1" niet bepalen Kon interpreter "%1" niet laden Kon scriptbestand "%1" niet openen Bestand: Pictogram: Interpreter: Veiligheidsniveau van de Ruby-interpreter Rinse de Vries - 2000 t/m 2008,Wilbert Berendsen - 2003; 2004,Bram Schoenmakers - 2004 t/m 2007,Tom Albers - 2004,Tijmen Baarda - 2005,Sander Koning - 2005, Freek de Kruijf - 2009 t/m 2012 Naam: Deze functie bestaat niet: "%1" Deze interpreter bestaat niet: "%1" Verwijderen Geselecteerd script verwijderen. Uitvoeren Scriptbestand "%1" bestaat niet. Stoppen Uitvoeren van het geselecteerde script stoppen. Tekst: Hulpmiddel voor het uitvoeren van Kross-scripts op de commandoregel. Kross 