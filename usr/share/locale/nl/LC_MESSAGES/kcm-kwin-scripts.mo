��          �      �       0  (   1     Z      q     �     �     �     �     �     �  `     ^   u     �  �  �  (   �     �     �     �     �                -     J  +   Z  ,   �     �        
              	                                    *.kwinscript|KWin scripts (*.kwinscript) Configure KWin scripts EMAIL OF TRANSLATORSYour emails Get New Scripts... Import KWin Script Import KWin script... KWin Scripts KWin script configuration NAME OF TRANSLATORSYour names Placeholder is error message returned from the install serviceCannot import selected script.
%1 Placeholder is name of the script that was importedThe script "%1" was successfully imported. Tamás Krutki Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-19 03:12+0100
PO-Revision-Date: 2017-10-14 10:10+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 *.kwinscript|KWin-scripts (*.kwinscript) KWin-scripts instellen freekdekruijf@kde.nl Nieuwe scripts ophalen... KWin-script importeren KWin-script importeren... KWin-scripts Instellingen van KWin-script Freek de Kruijf Kan geselecteerd script niet importeren.
%1 Het script "%1" is met succes geïmporteerd. Tamás Krutki 