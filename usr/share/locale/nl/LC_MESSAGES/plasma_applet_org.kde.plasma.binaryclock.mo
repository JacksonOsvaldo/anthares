��    	      d      �       �   
   �      �      �   	              "     C  "   ]  �  �  	        (     1     @     O  -   e  &   �  6   �                             	                 Appearance Colors: Display seconds Draw grid Show inactive LEDs: Use custom color for active LEDs Use custom color for grid Use custom color for inactive LEDs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-31 05:47+0100
PO-Revision-Date: 2017-05-02 10:29+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Uiterlijk Kleuren: Seconden tonen Raster tekenen Inactieve LED's tonen Aangepaste kleur voor actieve LED's gebruiken Aangepaste kleur voor raster gebruiken Gebruik een aangepaste kleur voor uitgeschakelde LED's 