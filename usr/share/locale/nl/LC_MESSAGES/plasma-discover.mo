��    d      <  �   \      �     �     �     �     �     �  5   �  #   �  E   	  E   _	  >   �	     �	     �	  7   
     E
     _
     p
     �
     �
     �
     �
     �
     �
          	          4     B     W     i     n  	   u          �     �  !   �  F   �     #     @     Q  <   c     �     �  *   �     �      �            	        &     6  	   >     H     X     _      h     �  
   �     �     �     �     �  
   �  F     :   K     �     �     �     �     �     �  8   �     �       	     
   "     -     =     F     W     l     r     �     �     �     �     �     �     �  &   �     "  
   >     I     Y     y     �     �     �     �  $   �  �  �     �     �     �     �     �  :   �  '     H   *  H   s  A   �      �  
     =   *  $   h     �  )   �  #   �  +   �     "     ;  
   R     ]     o     v  #   �     �     �     �     �  	   �  
   �  *        3     H  '   ]  K   �     �     �       @     	   Z  	   d  +   n     �  $   �     �     �     �     �                 	   /  	   9  +   C  '   o     �  (   �     �     �     �     �  R     B   k     �     �     �     �     �     �  ?        C     J  	   \  
   f     q     �     �     �     �     �     �  #   �     	     &  !   4     V     \     h  &   n     �     �     �     �     �     �  +   �  &   (  %   O         J   `   :       	   W                 [   .   E         )      (   R   M   '   O   $       7   Y   4                -                    9       8   #   S       0   P       B      
   +                         L   >              N   @      *   D      F   /   ;       5       2   ^      U               ?   6       <   V   ,       c       \   Z      G   1                  d       C   Q   &       A          _   b       =   T      !   3             a   K           ]              H      %      I   X          "    
Also available in %1 %1 %1 (%2) %1 (Default) <b>%1</b> by %2 <em>%1 out of %2 people found this review useful</em> <em>Tell us about this review!</em> <em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em> <em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em> @infoFetching updates @infoFetching... @infoIt is unknown when the last check for updates was @infoLooking for updates @infoNo updates @infoNo updates are available @infoShould check for updates @infoThe system is up to date @infoUpdates @infoUpdating... Accept Add Source... Addons Aleix Pol Gonzalez An application explorer Apply Changes Available backends:
 Available modes:
 Back Cancel Category: Checking for updates... Comment too long Comment too short Compact Mode (auto/compact/full). Could not close the application, there are tasks that need to be done. Could not find category '%1' Couldn't open %1 Delete the origin Directly open the specified application by its package name. Discard Discover Display a list of entries with a category. Extensions... Failed to remove the source '%1' Featured Help... Homepage: Improve summary Install Installed Jonathan Thomas Launch License: List all the available backends. List all the available modes. Loading... Local package file to install Make default More Information... More... No Updates Open Discover in a said mode. Modes correspond to the toolbar buttons. Open with a program that can deal with the given mimetype. Proceed Rating: Remove Resources for '%1' Review Reviewing '%1' Running as <em>root</em> is discouraged and unnecessary. Search Search in '%1'... Search... Search: %1 Search: %1 + %2 Settings Short summary... Show reviews (%1)... Size: Sorry, nothing found... Source: Specify the new source for %1 Still looking... Summary: Supports appstream: url scheme Tasks Tasks (%1%) TransactioName TransactionStatus%1 %2 Unable to find resource: %1 Update All Update Selected Update section nameUpdate (%1) Updates Version: unknown reviewer updates not selected updates selected © 2010-2016 Plasma Development Team Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:02+0100
PO-Revision-Date: 2018-01-16 12:42+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=n != 1;
 
Ook beschikbaar in %1 %1 %1 (%2) %1 (standaard) <b>%1</b> door %2 <em>%1 van %2 personen vonden deze recensie bruikbaar</em> <em>Vertel ons over deze recensie!</em> <em>Bruikbaar? <a href='true'><b>Ja</b></a>/<a href='false'>Nee</a></em> <em>Bruikbaar? <a href='true'>Ja</a>/<a href='false'><b>Nee</b></a></em> <em>Bruikbaar? <a href='true'>Ja</a>/<a href='false'>Nee</a></em> Elementen voor bijwerken ophalen Ophalen... Het tijdstip van de laatste controle op bijwerken is onbekend Zoeken naar elementen voor bijwerken Er is niets voor bijwerken Geen elementen voor bijwerken beschikbaar Zou moeten controleren op bijwerken De pakketten op het systeem zijn bijgewerkt Elementen voor bijwerken Bezig met bijwerken... Accepteren Bron toevoegen... Addons Aleix Pol Gonzalez Een zoekprogramma naar toepassingen Wijzigingen toepassen Beschikbare backends:
 Beschikbare modi:
 Terug Annuleren Categorie: Controleren op elementen voor bijwerken... Opmerking is te lang Opmerking is te kort Compacte modus (auto/compact/volledig). Kon de toepassing niet sluiten, er zijn taken die nog gedaan moeten worden. Kon categorie '%1' niet vinden Kon %1 niet openen Oorsprong verwijderen De gespecificeerde toepassing direct openen met zijn pakketnaam. Verwerpen Ontdekken Toon een lijst met items met een categorie. Extensies... Broncode '%1' verwijderen is mislukt Ondersteund Help... Homepagina: Samenvatting verbeteren Installeren Geïnstalleerd Jonathan Thomas Opstarten Licentie: Maak een lijst met de beschikbare backends. Maak een lijst met de beschikbare modi. Bezig met laden... Lokaal bestand met te installeren pakket Maak standaard Meer informatie... Meer... Er is niets voor bijwerken Discover openen in de aangegeven modus. Modi komen overeen met de werkbalkknoppen. Openen met een programma dat overweg kan met het gegeven mimetype. Verder gaan Waardering: Verwijderen Hulpbronnen voor '%1' Nakijken Nakijken van '%1' Als <em>root</em> uitvoeren is ontmoedigd en niet noodzakelijk. Zoeken Zoeken in '%1'... Zoeken... Zoeken: %1 Zoeken: %1 + %2 Instellingen Korte samenvatting... Recenties tonen (%1)... Grootte: Helaas, niets gevonden... Bron: De nieuwe bron voor %1 specificeren Nog steeds aan het zoeken... Samenvatting: Ondersteunt appstream: url schema Taken Taken (%1%) %1 %2 Hulpbron '%1' kon niet worden gevonden Alles bijwerken Selectie Bijwerken Bijwerken (%1) Elementen voor bijwerken Versie: onbekende recensent niet geselecteerde elementen voor bijwerken geselecteerde elementen voor bijwerken © 2010-2016 Ontwikkelteam van Plasma 