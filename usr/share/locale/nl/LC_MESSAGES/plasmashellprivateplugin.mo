��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .  
   �     �     �  $   �          7     >     U  (   ]     �  ,   �     �     �     �     �     �     �       !   '  #   I     m  8   �  I   �  	   	     	     	  /   "	     R	  	   f	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-08-30 22:06+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Uitvo&eren Alle widgets Categorieën: Bureaubladconsole met shellscripting Nieuwe plasmawidgets downloaden Editor Script in %1 uitvoeren Filters Widget uit lokaal bestand installeren... Installatiefout De installatie van het pakket %1 is mislukt. Laden Nieuwe sessie Scriptbestand openen Uitvoer Actief Uitvoeringstijd: %1ms Scriptbestand opslaan Scherm vergrendeling ingeschakeld Tijdslimiet op de schermbeveiliging Plasmoid bestand selecteren Stelt de minuten in waarna het scherm wordt vergrendeld. Stelt in of het scherm vergrendeld zal worden na de gespecificeerde tijd. Sjablonen KWin Plasma Laden van scriptbestand <b>%1</b> niet mogelijk Niet-te-installeren Gebruiken 