��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �     �     �     �     �  	             #     <  i   E     �  @   �  
   	          (  
   5     @     I     V     d     y     �     �     �     �  :   �     -	     C	                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-09-09 15:55+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Kies... Pictogram wissen Aan Favorieten toevoegen Alle toepassingen Uiterlijk Toepassingen Toepassingen bijgewerkt. Computer Versleep tabbladen tussen de vakken om ze te tonen/verbergen or order de zichtbare tabbladen door slepen. Toepassingen bewerken... Zoeken uitbreiden naar bladwijzers, bestanden en e-mailberichten Favorieten Verborgen tabbladen Geschiedenis Pictogram: Verlaten Menu Knoppen Vaak gebruikt Op alle activiteiten Op de huidige activiteit Uit Favorieten verwijderen In Favorieten tonen Toepassingen bij naam tonen Alfabetisch sorteren Van tabblad veranderen door muisaanwijzer erop te plaatsen Typen om te zoeken... Zichtbare tabbladen 