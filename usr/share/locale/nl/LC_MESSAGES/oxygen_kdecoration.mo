��    ?        Y         p     q  !   �  "   �  &   �  ,   �  #   &  &   J  !   q  &   �  '   �  "   �  #     "   )  '   L     t     �  +   �  2   �  
   �     �               ,     3     G  U   O  7   �     �     �     		     	      	     6	     T	     c	     k	  !   �	     �	  	   �	     �	     �	     �	     �	  &   
     /
     N
     U
     p
     v
     ~
     �
  4   �
  $   �
     �
               /     G     ]     w  &   �     �  �  �  #   c     �     �  	   �     �     �     �  
   �     �  	   �     �     �     �  	   �       	     F   )  @   p  	   �     �     �     �     �  !   �       t   $  ?   �     �     �     	       +        F     e     w     �  $   �     �     �     �  )   �          7  )   K  7   u     �  #   �     �     �     �       :     *   G     r  "   �     �     �     �  !   �       4     $   R     $                               1          "   *   )          :             	         ;   !                          9   =   5   /   3                    (         #   4       8       -   6   
   7          %                                         ?       0   &      +          '         .       ,      >       <   2        &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Allow resizing maximized windows from window edges Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use the same colors for title bar and window content Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2017-12-13 22:38+0100
Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Overeenkomende venstereigenschap:  Enorm Groot Geen rand Geen zijranden Normaal Veel te groot Heel klein Extra enorm Erg groot Groot Normaal Klein Erg groot Opgloeien van actieve venster Toevoegen Voeg handvat toe om vensters zonder rand van grootte te laten wijzigen Grootte wijzigen van gemaximaliseerde vensters uit vensterranden Animaties &Knopgrootte: Randgrootte: Muis-over-overgang van knop Midden In het midden (volledige breedte) Klasse:  Vervaging tussen vensterschaduw en verhelderen, wanneer de actieve status van een venster zich wijzigt, configureren Geanimeerde accentuering van muis-over van knoppen configureren Decoratieopties Venstereigenschappen detecteren Dialoog Bewerken Uitzondering bewerken - Oxygen-instellingen Deze exceptie in-/uitschakelen Type uitzondering Algemeen Titelbalk van venster verbergen Informatie over geselecteerd venster Links Omlaag verplaatsen Omhoog verplaatsen Nieuwe uitzondering - Oxygen-instellingen Vraag - Oxygen-instellingen Reguliere expressie Syntax van reguliere expressie is onjuist Regulier expressie waarmee overeengekomen moet worden:  Verwijderen Geselecteerde exceptie verwijderen? Rechts Verduistering Tite&luitlijning: Titel:  Dezelfde kleuren voor titelbalk en vensterinhoud gebruiken Vensterklasse gebruiken (gehele programma) Venstertitel gebruiken Waarschuwing - Oxygen-instellingen Naam vensterklasse Afrol-schaduw van venster Vensteridentificatie Selectie van venstereigenschappen Venstertitel Overgangen van actieve statuswijzigingen van venster Vensterspecifieke zaken die voorgaan 