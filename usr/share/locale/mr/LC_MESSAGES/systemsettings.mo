��    *      l  ;   �      �  A   �     �  /        2     ;     O     a     w     �     �  	   �     �  3   �  	   �     �      �  $      *   E     p  	   u  5        �     �  
   �     �          $  5   3  6   i     �  ,   �  /   �     	        O   0  Z   �  b   �  	   >  
   H  P   S     �  �  �  �   ^
  )   0  o   Z     �  ;   �  ;     ;   U  %   �     �     �     �  E   �  u   =     �     �  /   �  s        �  	   �     �  �   �  U   H  *   �     �  "   �  A     <   G  �   �  �     ;   �  �   �  	   |  >   �  +   �  �   �  �   �  �   �     �     �  �   �  %   �            )             &      $   	   %         '                                                !      
       *                     "                                                 (                 #    %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2013-02-23 17:39+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 %1 हा प्रणाली संयोजना बाहेरचा अनुप्रयोग स्वयंचलितरित्या प्रक्षेपित केलेला आहे (C) 2009, बेन कूक्सली <i>1 घटक समाविष्ट आहे</i> <i>%1 घटक समाविष्ट आहेत</i> %1 विषयी कार्यशील विभागा बद्दल कार्यशील दृश्या बद्दल प्रणाली संयोजने बद्दल संयोजना साठवा लेखक बेन कूक्सली संयोजन तुमची प्रणाली संयोजीत करा तपशीलवार टूलटिप दर्शवायचे की नाही ते ठरवितो विकासकर्ता संवाद sandeep.shedmake@gmail.com, 
chetan@kompkin.com पहिली पातळी स्वयंचलितरित्या विस्तारीत करा सामान्य मदत चिन्ह दृश्य अंतर्गत विभाग प्रतिनिधीत्व, अंतर्गत विभाग पद्धत दृश्या करिता अंतर्गत नाव वापरले कळफलक शॉर्टकट : %1 नियंत्रक माथिआस सोकेन संदिप शेडमाके, 
चेतन खोना एकही दृश्य सापडले नाही नियंत्रण कक्ष विभागवार चिन्हांच्या रुपात दर्शवितो. नियंत्रण कक्ष क्लासिक ट्री दृश्यरुपात दर्शवितो. %1 पुन्हा प्रक्षेपण करा आता केलेले सर्व बदल काढून टाकून पूर्वीची मूल्ये पुन्हस्थापित करा शोध तपशीलवार टूलटिप दर्शवा प्रणाली संयोजना प्रणाली संयोजना कोणतेही दृश्य शोधू शकत नाही व त्यामुळे दर्शविण्याकरिता काही नाही. प्रणाली संयोजना कोणतेही दृश्य शोधू शकत नाही व त्यामुळे संयोजना करण्याकरिता काही नाही. सध्याच्या विभागाच्या संयोजनेत बदल केले गेले आहेत.
तुम्हाला बदल साठवायचे आहेत का वगळायचे आहेत? ट्री दृश्य दृश्य शैली "प्रणाली संयोजनेत" तुमचे स्वागत आहे. तुमची संगणक प्रणाली संयोजीत करण्याचे हे केंद्रीय स्थान आहे. विल स्टीफन्सन 