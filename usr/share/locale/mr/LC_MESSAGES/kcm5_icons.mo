��    =        S   �      8     9     B     K     Z     l     s  �  �  �   i  -   �  )   #	  -   M	  +   {	  r   �	  	   
  	   $
     .
     F
     N
     W
  
   d
     o
     {
     �
     �
      �
     �
     �
     �
      �
     	       r   *  5   �     �     �     �          *  	   /     9     ?     G  (   T     }     �     �     �     �     �  +   �  3        K     S     a     i  "   v  S   �  )   �       �   #  �       �     �  %   �  *   �     (  -   :  �  h  H  �  o   F     �     �     �    �     �       A   1     s     �     �     �     �     �     �  A     /   I     y     �     �  O   �       B   &    i  �   m  X     "   j  +   �  A   �  	   �        	   %   !   /   )   Q   u   {   5   �   U   '!  U   }!  ^   �!     2"     @"  T   ]"  �   �"     3#  "   @#     c#  %   #  Z   �#  �    $  Z   �$  %   0%  s  V%     (            4                        
   7             1   ,          #   $      :   3              ;      0                     5   6       %       "       	            2      +            9   '       8                 <   /   &   .              -       *       )                 =         !        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2014-10-28 10:30+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);
 मात्रा (&A): परिणाम (&E): द्वितीय रंग (&S): अर्ध-पारदर्शी (&S) शैली (&T) (c) 2000-2003 गीर्त जानसन <h1>चिन्ह</h1>हे विभाग तुम्हाला डेस्कटॉपवरील चिन्ह निवडण्याकरिता सहमती देतो.<p>चिन्ह शैली निवडण्याकरिता, नावावर क्लिक करा व तुमची निवड "लागू करा" बटन दाबून लागू करा. निवड लागू करायची नसल्यास "पुन्हस्थापन" बटन दाबून बदल नकारू शकता.</p><p> "नवीन शैली प्रतिष्ठापन" बटन दाबल्यास पटलात स्थान लिहील्यास किंवा स्थानावर गेल्यास नवीन शैली चिन्ह प्रतिष्ठापीत करू शकता. प्रतिष्ठापन पूर्ण करण्याकरिता "ठिक आहे" बटनावर क्लिक करा.</p><p> या विभागाचा वापर करून तुम्ही प्रतिष्ठापीत शैली निवड केल्यास "शैली काढा" सक्रिय केले जाऊ शकते. येथे तुम्ही जागतिक स्थरावरील प्रतिष्ठापीत शैली काढू शकत नाही.</p><p>तुम्ही येथे चिन्हसह लागू होण्याजोगी परिणाम देखिल निर्देशीत करू शकता.</p> <qt>तुम्हाला नक्की चिन्ह शैली <strong>%1</strong> काढून टाकायची आहे का?<br /><br /> यामुळे शैली द्वारे प्रतिष्ठापीत फाईल्स काढून टाकल्या जातील.</qt> <qt><strong>%1</strong> शैली प्रतिष्ठापीत केली जात आहे</qt> सक्रिय निष्क्रिय मूलभूत प्रतिष्ठापन प्रक्रीयावेळी समस्या उत्पन्न झाली, तरी संग्रह मधील बरेचशे शैली प्रतिष्ठापीत झाले आहे प्रगत (&V) सर्व चिन्ह अंतोनियो लारोसा जिमेनेज रंग (&l): रंगीन पुष्टिकरण असंपृक्तता वर्णन डेस्कटॉप संवाद शैली URL ओढा किंवा टाइप करा sandeep.shedmake@gmail.com, 
chetan@kompkin.com परिणाम बाबी गॅमा गीर्त जानसन महाजाळ पासून शैली प्राप्त करा चिन्ह चिन्ह नियंत्रण पटल विभाग तुमच्याकडे शैली संग्रह स्थानिकरित्या असल्यास, ही बटन लगेचच केडीई अनुप्रयोगांना उपलब्ध करून देते आधीपासूनच स्थापीत स्थानिक शैली संग्रह फाईल प्रतिष्ठापीत करा चिन्ह शैली प्रतिष्ठापीत करत आहे... जोनाथन रिडेल मुख्य साधनपट्टी संदिप शेडमाके, 
चेतन खोना नाव परिणाम नाही पटल पूर्वावलोकन शैली काढून टाका निवडलेली शैली तुमच्या डिस्क मधून काढून टाका परिणाम निश्चित करा... सक्रिय चिन्ह परिणाम निश्चित करा मूलभूत चिन्ह परिणाम निश्चित करा निष्क्रिय चिन्ह परिणाम निश्चित करा आकार: छोटे चिन्ह फाईल वैध चिन्ह शैली संग्रह नाही. हे निवडलेली शैली तुमच्या डिस्क मधून काढून टाकते. धूसर एकरंगी करिता साधनपट्टी तॉर्स्तेन रान तात्पुरती फाईल तयार करता येत नाही. चिन्ह शैली संग्रह डाउनलोड करण्यास अक्षम;
कृपया पत्ता %1 योग्य आहे याची तपासणी करा. चिन्ह शैली संग्रह %1 शोधू शकला नाही. चिन्हाचा वापर ही क्रिया वापरण्याकरिता तुम्ही महाजाळशी जुळवणी स्थापीत करायला हवे. http://www.kde.org संकेतस्थाळ पासून शैली यादी दर्शविणारी एक संवादपट दर्शविले जाईल. शैलीशी जुळलेली प्रतिष्ठापन बटनावर क्लिक केल्यास ही शैली स्थानिकरित्या प्रतिष्ठापीत केली जाईल. 