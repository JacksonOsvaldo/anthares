��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  N   �  e   3  �   �  y   "  �   �  \   $  9   �  (   �  P   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-01-19 14:31+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 फाईलचा mime प्रकार ओळखू शकत नाही गरज असलेल्या सर्व कृती सापडल्या नाहीत दिलेल्या लक्ष्याबरोबरचा पुरविठाकर्ता सापडला नाही स्क्रिप्ट चालविण्याचा प्रयत्न करताना त्रुटी विनंती केलेल्या पुरविठाकर्ता करिता मार्ग अवैध आहे निवडलेली फाईल वाचणे शक्य झाले नाही सेवा उपलब्ध झाली नाही अपरिचीत त्रुटी या सेवेकरिता URL देणे गरजेचे आहे 