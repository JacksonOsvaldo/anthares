��    !      $  /   ,      �  .   �  1     9   J     �  -   �  &   �  )   �  .   #  &   R  )   y  .   �  &   �  )   �  2   #  5   V  =   �  #   �     �  !     '   /  "   W  0   z  '   �  *   �     �          7     R     j     �     �  &   �  �  �  U   z	  X   �	  _   )
  .   �
  A   �
  1   �
  :   ,  P   g  @   �  I   �  P   C  @   �  I   �  e     h   �  o   �  I   ^  9   �  B   �  :   %  3   `  _   �  O   �  X   D     �  &   �  )   �          "     9  /   P  ?   �                                                              	                                
                                    !                             @info:statusAdded files to Bazaar repository. @info:statusAdding files to Bazaar repository... @info:statusAdding of files to Bazaar repository failed. @info:statusBazaar Log closed. @info:statusCommit of Bazaar changes failed. @info:statusCommitted Bazaar changes. @info:statusCommitting Bazaar changes... @info:statusPull of Bazaar repository failed. @info:statusPulled Bazaar repository. @info:statusPulling Bazaar repository... @info:statusPush of Bazaar repository failed. @info:statusPushed Bazaar repository. @info:statusPushing Bazaar repository... @info:statusRemoved files from Bazaar repository. @info:statusRemoving files from Bazaar repository... @info:statusRemoving of files from Bazaar repository failed. @info:statusReview Changes failed. @info:statusReviewed Changes. @info:statusReviewing Changes... @info:statusRunning Bazaar Log failed. @info:statusRunning Bazaar Log... @info:statusUpdate of Bazaar repository failed. @info:statusUpdated Bazaar repository. @info:statusUpdating Bazaar repository... @item:inmenuBazaar Add... @item:inmenuBazaar Commit... @item:inmenuBazaar Delete @item:inmenuBazaar Log @item:inmenuBazaar Pull @item:inmenuBazaar Push @item:inmenuBazaar Update @item:inmenuShow Local Bazaar Changes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-03-22 11:48+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 बझार रीपोझिटरीत फाईल्स जोडल्या. बझार रीपोझिटरीत फाईल्स जोडत आहे... बझार रीपोझिटरीत फाईल्स जोडणे अपयशी. बझार लॉग बंद केला. बझार बदल सादर करणे अपयशी. बझार बदल सादर केले. बझार बदल सादर करत आहे... बझार रीपोझिटरी पुल करणे अपयशी. बझार रीपोझिटरी पुल केली. बझार रीपोझिटरी पुल करत आहे... बझार रीपोझिटरी पुश करणे अपयशी. बझार रीपोझिटरी पुश केली. बझार रीपोझिटरी पुश करत आहे... बझार रीपोझिटरीत फाईल्स काढून टाकल्या. बझार रीपोझिटरीत फाईल्स काढून टाकत आहे... बझार रीपोझिटरीत फाईल्स काढून टाकणे अपयशी. बदलांची समीक्षा करणे अपयशी. बदलांची समीक्षा केली. बदलांची समीक्षा करत आहे... बझार लॉग चालवणे अपयशी. बझार लॉग चालवत आहे... बझार रीपोझिटरी अद्ययावत करणे अपयशी. बझार रीपोझिटरी अद्ययावत केली. बझार रीपोझिटरी अद्ययावत करत आहे... बझार जोडा... बझार सादर करा... बझार काढून टाका बझार लॉग बझार पुल बझार पुश बझार अद्ययावत करा स्थानिक बझार बदल दर्शवा 