��    B      ,  Y   <      �  i   �  m        y  b   �     �     	  6        O  7   _     �     �  	   �     �  (   �  )   �  )   (     R     o  Y   u     �     �  �   �      y	  .   �	     �	  
   �	     �	     �	     
     
     !
     5
     B
     J
     c
     r
     w
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
  	     
     
   *     5     B  	   `     j     o  
   �  �   �     (  8   8  �   q     �  8   	  ,   B  ,   o  %   �     �  �  �  �   �  3  F  J   z  �   �  A   �  "   �  x     +   �  �   �     ;  "   Q     t  =   �  f   �  �   /  �   �  ;   F     �  �   �  "   �  .   �  Z  �  /   B  �   r          !  8   >     w  8   �     �  (   �          2  Q   ?  .   �     �  A   �  5        K  !   f     �     �     �     �  .   �  /   $     T     t  %   �  "   �  9   �          *  R   :  -   �  C  �  "   �  �   "   �   �   4   �!  �   �!  �   W"  �   #  w   �#  T   N$     $   !   %          ;   ,          B                 8   #                 7   9       +              )   ?   -   <         6                                    *   2       >                1       @       .   5   3   :   A         &      /                4              0      "             =   (   '         	   
           @info User changed Phonon backendTo apply the backend change you will have to log out and back in again. A list of Phonon Backends found on your system.  The order here determines the order Phonon will use them in. Apply Device List To... Apply the currently shown device preference list to the following other audio playback categories: Audio Hardware Setup Audio Playback Audio Playback Device Preference for the '%1' Category Audio Recording Audio Recording Device Preference for the '%1' Category Backend Colin Guthrie Connector Copyright 2006 Matthias Kretz Default Audio Playback Device Preference Default Audio Recording Device Preference Default Video Recording Device Preference Default/Unspecified Category Defer Defines the default ordering of devices which can be overridden by individual categories. Device Configuration Device Preference Devices found on your system, suitable for the selected category.  Choose the device that you wish to be used by the applications. EMAIL OF TRANSLATORSYour emails Failed to set the selected audio output device Front Center Front Left Front Left of Center Front Right Front Right of Center Hardware Independent Devices Input Levels Invalid KDE Audio Hardware Setup Matthias Kretz Mono NAME OF TRANSLATORSYour names Phonon Configuration Module Playback (%1) Prefer Profile Rear Center Rear Left Rear Right Recording (%1) Show advanced devices Side Left Side Right Sound Card Sound Device Speaker Placement and Testing Subwoofer Test Test the selected device Testing %1 The order determines the preference of the devices. If for some reason the first device cannot be used Phonon will try to use the second, and so on. Unknown Channel Use the currently shown device list for more categories. Various categories of media use cases.  For each category, you may choose what device you prefer to be used by the Phonon applications. Video Recording Video Recording Device Preference for the '%1' Category  Your backend may not support audio recording Your backend may not support video recording no preference for the selected device prefer the selected device Project-Id-Version: kcm_phonon
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-03-28 15:41+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);
 बॅकएन्ड बदल लागु होण्याकरिता तुम्हाला बाहेर पडुन परत प्रवेश करावा लागेल. प्रणालीवरील आढळलेले फोनॉन बॅकएन्डची यादी. येथिल क्रमवारी फोनॉन द्वारे वापरलेली जाणारी क्रमवारी निश्चित केली जाते. या करिता साधन यादी लागू करा... खालील इतर आवाज प्लेबॅक विभागाकरिता दर्शविलेले वर्तमान साधन प्राधान्यता यादी लागू करा : आवाज हार्डवेअर व्यवस्था आवाज प्लेबॅक '%1' विभागाकरिता आवाज प्लेबॅक साधन प्राधान्यता आवाज रेकॉर्डिंग '%1' विभागाकरिता आवाज  रेकॉर्डिंग साधन प्राधान्यता बॅकएन्ड कोलिन गुथ्री जोडणारा Copyright 2006 मात्थिआस क्रेट्झ मूलभूत आवाजाच्या साधनाची प्राधान्यता मूलभूत आवाज रेकॉर्ड करणाऱ्या साधनाची प्राधान्यता मूलभूत व्हिडीओ रेकॉर्ड करणाऱ्या साधनाची प्राधान्यता मूलभूत/अनिश्चित विभाग विलंब करा साधनाची मूलभूत क्रमवारी निश्चित करतो जे स्वतंत्र विभाग द्वारे खोडून पुन्हा लिहीले जाऊ शकतात. साधन संयोजना साधन प्राधान्यता निवडलेल्या विभागाकरिता योग्य साधने तुमच्या प्रणालीवर आढळली. अनुप्रयोगांनी वापरण्याकरिता तुमची इच्छा असलेल्या साधनाची निवड करा. sandeep.shedmake@gmail.com, 
chetan@kompkin.com निवडलेले आवाज आउटपुट देणारे साधन स्थापीत करण्यास अपयश पुढील मध्य पुढील डावा मध्यावरील पुढील डावा पुढील उजवा मध्यावरील पुढील उजवा हार्डवेअर स्वतंत्र साधने इनपुट पातळी अवैध केडीई आवाज हार्डवेअर व्यवस्था मात्थिआस क्रेट्झ एकसुरी संदिप शेडमाके, 
चेतन खोना फोनॉन संयोजना विभाग प्लेबॅक (%1) प्राधान्यता रूपरेषा मागील मध्य मागील डावा मागील उजवा रेकॉर्ड करत आहे (%1) प्रगत साधन दर्शवा बाजुचा डावा बाजुचा उजवा आवाजाचे कार्ड आवाजाचे साधन स्पीकरची जागा व चाचणी सबवूफर चाचणी निवडलेल्या साधनाची चाचणी घ्या. %1 ची चाचणी घेत आहे हा क्रम साधनांची प्राधान्यता ठरवितो. जर काही कारणांमुळे पहिले साधन वापरता आले नाही तर फोनॉन पुढच्या साधनाचा प्रयत्न करेल. अपरिचीत चैनल अधिक विभागांकरिता वर्तमान दर्शविलेली साधन यादी वापरा. फोनॉन अनुप्रयोग प्रत्येक विभागाकरिता कोणते साधन वापरेल त्याची निवड तुम्ही करू शकता. व्हिडीओ रेकॉर्डिंग '%1' विभागाकरिता व्हिडीओ रेकॉर्डिंग साधन प्राधान्यता  आवाज रेकॉर्डिंग करण्याकरिता तुमचे बॅकएन्ड कदाचित पाठिंबा देणार नाही. व्हिडीओ रेकॉर्डिंग करण्याकरिता तुमचे बॅकएन्ड कदाचित पाठिंबा देणार नाही. निवडलेले साधन करिता प्राधान्यता निवडली नाही निवडलेले साधन प्राधान्यकृत करा 