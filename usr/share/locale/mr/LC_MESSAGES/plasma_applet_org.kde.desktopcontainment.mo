��            )   �      �     �     �     �     �     �     �     �     �          	               &     3     K     ^     j     ~     �     �     �  
   �     �     �     �     �     �            �  -  !   �  :     3   >     r     �     �  4   �  +   �  .        G  %   c     �  &   �  I   �  $         2  ;   S     �     �  D   �     �     	  ,   (	  A   U	  $   �	  A   �	     �	  6   
  G   B
                                                                    
      	                                                               &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename Custom title Default Deselect All Enter custom title here File name pattern: File types: Hide Files Matching Icons Large More Preview Options... None Select All Show All Files Show Files Matching Show a place: Show the Desktop folder Small Specify a folder: Type a path or a URL here Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2013-03-28 16:09+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 काढून टाका (&D) कचरापेटी रिकामी करा (&E) कचरापेटीकडे हलवा (&M) उघडा (&O) चिकटवा (&P) गुणधर्म (&P) डेस्कटॉप ताजा करा (&R) दृश्य ताजे करा (&R) पुन्हा दाखल करा (&R) नाव बदला (&R) ऐच्छिक शिर्षक मूलभूत सर्व निवडू नका ऐच्छिक शिर्षक येथे दाखल करा फाईल नाव रचना: फाईल प्रकार: जुळलेल्या फाईल्स लपवा चिन्ह मोठे जादा पूर्वावलोकन पर्याय... काही नाही सर्व निवडा सर्व फाईल दर्शवा जुळलेल्या फाईल्स दर्शवा स्थान दर्शवा : डेस्कटॉप संचयीका दर्शवा लहान संचयीका निश्चित करा: मार्ग किंवा URL येथे दाखल करा 