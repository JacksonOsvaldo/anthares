��    .      �  =   �      �     �  ;   �  9   2  (   l  ;   �  9   �  8     $   D     i          �  /   �      �     �  ,        0     @  N   P  
   �      �  	   �  �   �     _     k  e   �     �             
   (     3     G     `     g  &   x  %   �  f   �  8   ,	  ;   e	     �	     �	     �	     �	  �   �	  "   �
  C   �
  �  �
     �  9   �  ?   �     1  5   J  ;   �  S   �       8     @   V     �  �   �  ;   c  2   �     �  3   �       �   6  #   �  /        4  F  M  8   �  A   �  �     8     >   <  K   {  +   �  ,   �  9         Z  &   v  P   �  f   �  �   U  �   ?  �   �  )   �  G   �  I     2   f  -  �  ]   �  �   %                             #          $      
   "   %   	                              !   .             &            -          +          *                                 ,   '                (   )                min @action:inmenu Global shortcutDecrease Keyboard Brightness @action:inmenu Global shortcutDecrease Screen Brightness @action:inmenu Global shortcutHibernate @action:inmenu Global shortcutIncrease Keyboard Brightness @action:inmenu Global shortcutIncrease Screen Brightness @action:inmenu Global shortcutToggle Keyboard Backlight @label:slider Brightness levelLevel AC Adapter Plugged In Activity Manager After All pending suspend actions have been canceled. Battery Critical (%1% Remaining) Battery Low (%1% Remaining) Brightness level, label for the sliderLevel Can't open file Charge Complete Could not connect to battery interface.
Please check your system configuration Do nothing EMAIL OF TRANSLATORSYour emails Hibernate KDE Power Management System could not be initialized. The backend reported the following error: %1
Please check your system configuration Lock screen NAME OF TRANSLATORSYour names No valid Power Management backend plugins are available. A new installation might solve this problem. On Profile Load On Profile Unload Prompt log out dialog Run script Running on AC power Running on Battery Power Script Switch off after The power adaptor has been plugged in. The power adaptor has been unplugged. The profile "%1" has been selected, but it does not exist.
Please check your PowerDevil configuration. This activity's policies prevent screen power management This activity's policies prevent the system from suspending Turn off screen Unsupported suspend method When laptop lid closed When power button pressed Your battery is low. If you need to continue using your computer, either plug in your computer, or shut it down and then change the battery. Your battery is now fully charged. Your battery level is critical, save your work as soon as possible. Project-Id-Version: powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-03 03:17+0100
PO-Revision-Date: 2014-10-28 10:28+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);
  मिनिटे कळफलक प्रखरता कमी करा स्क्रीन प्रखरता कमी करा हायबरनेट कळफलक प्रखरता वाढवा स्क्रीन प्रखरता वाढवा कळफलक मागील प्रकाश चालू/बंद करा स्तर AC एडॅप्टर जोडलेला आहे कार्यपध्दती व्यवस्थापक नंतर सर्व बाकी असलेल्या अकार्यक्षम करणाऱ्या क्रिया रद्द करण्यात आल्या आहेत. बॅटरी संकटात (%1% शिल्लक) बॅटरी कमी (%1% शिल्लक) पातळी फाईल उघडता येत नाही चार्ज पूर्ण बॅटरी दुव्याला जोडू शकत नाही.
कृपया तुमची प्रणाली संयोजना तपासा काहीच करू नका sandeep.shedmake@gmail.com, 
chetan@kompkin.com हायबरनेट केडीई वीज व्यवस्थापन प्रणाली सुरु होऊ शकत नाही. बॅकएन्डने पुढील त्रुटी दर्शविलेली आहे : %1
कृपया तुमची प्रणाली संयोजना तपासा स्क्रीन कुलूपबंद करा संदिप शेडमाके, 
चेतन खोना वैध वीज व्यवस्थापन बॅकएन्ड प्लगइन उपलब्ध नाहीत. नवीन प्रतिष्ठापनेने ही समस्या दूर होऊ शकेल. रूपरेषा लोड केल्यावर रूपरेषा अनलोड केल्यावर बाहेर पडण्याचा संवाद दर्शवा स्क्रिप्ट चालवा AC वीजेवर चालू आहे बॅटरी वीजेवर चालू आहे स्क्रिप्ट यानंतर बंद करा पॉवर एडॅप्टर जोडला गेलेला आहे. पॉवर एडॅप्टर काढून टाकण्यात आलेला आहे. "%1" ही रूपरेषा निवडलेली आहे. पण ती अस्तित्वात नाही.
कृपया तुमची पॉवरडेव्हिल संयोजना तपासा. या कार्यपध्दतीचे धोरण स्क्रीन वीज व्यवस्थापन प्रतिबंध करत आहे या कार्यपध्दतीचे धोरण प्रणालीस अकार्यक्षम होण्यापासून प्रतिबंध करत आहे स्क्रीन बंद करा असमर्थीत अकार्यक्षम पद्धत लैपटॉपचे झाकण बंद केल्यावर  पॉवर बटन दाबल्यावर तुमची बॅटरी कमी आहे. तुम्हाला जर तुमचा संगणक चालू ठेवायचा असेल तर त्याला वीज जोडा किंवा बंद करून त्याची बॅटरी बदला. तुमची बॅटरी पूर्ण चार्ज झालेली आहे. तुमची बॅटरी संकटाच्या पातळी पर्यंत पोहचली आहे. तुमचे काम लवकरात लवकर साठवा. 