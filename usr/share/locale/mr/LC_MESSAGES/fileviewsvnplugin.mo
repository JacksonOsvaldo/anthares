��          �   %   �      0     1  +   G  .   s  6   �  *   �  #     &   (  /   O  2     :   �  K   �  -   9  $   g  '   �     �     �     �     �  #        8     V     j     �  �  �     ,  a   C  d   �  k   
  M   v  =   �  F   	  q   I	  t   �	  {   0
  �   �
  k   �  [     d   ^  %   �  2   �  5     ;   R  K   �  D   �       /   1  +   a                                                    	   
                                                                        @action:buttonCommit @info:statusAdded files to SVN repository. @info:statusAdding files to SVN repository... @info:statusAdding of files to SVN repository failed. @info:statusCommit of SVN changes failed. @info:statusCommitted SVN changes. @info:statusCommitting SVN changes... @info:statusRemoved files from SVN repository. @info:statusRemoving files from SVN repository... @info:statusRemoving of files from SVN repository failed. @info:statusSVN status update failed. Disabling Option "Show SVN Updates". @info:statusUpdate of SVN repository failed. @info:statusUpdated SVN repository. @info:statusUpdating SVN repository... @item:inmenuSVN Add @item:inmenuSVN Commit... @item:inmenuSVN Delete @item:inmenuSVN Update @item:inmenuShow Local SVN Changes @item:inmenuShow SVN Updates @labelDescription: @title:windowSVN Commit Show updates Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:16+0100
PO-Revision-Date: 2013-03-22 11:52+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.5
 सादर करा एसव्हीएन रीपोझिटरीत फाईल्स जोडल्या. एसव्हीएन रीपोझिटरीत फाईल्स जोडत आहे... एसव्हीएन रीपोझिटरीत फाईल्स जोडणे अपयशी. एसव्हीएन बदल सादर करणे अपयशी. एसव्हीएन बदल सादर केले. एसव्हीएन बदल सादर करत आहे... एसव्हीएन रीपोझिटरीत फाईल्स काढून टाकल्या. एसव्हीएन रीपोझिटरीत फाईल्स काढून टाकत आहे... एसव्हीएन रीपोझिटरीत फाईल्स काढून टाकणे अपयशी. एसव्हीएन स्थिती अद्ययावत अपयशी. "एसव्हीएन अद्ययावत दर्शवा" पर्याय अकार्यान्वित करत आहे. एसव्हीएन रीपोझिटरी अद्ययावत करणे अपयशी. एसव्हीएन रीपोझिटरी अद्ययावत केली. एसव्हीएन रीपोझिटरी अद्ययावत करत आहे... एसव्हीएन जोडा एसव्हीएन सादर करा... एसव्हीएन काढून टाका एसव्हीएन अद्ययावत करा स्थानिक एसव्हीएन बदल दर्शवा एसव्हीएन अद्ययावत दर्शवा वर्णन : एसव्हीएन सादर करा अद्ययावत दर्शवा 