��    $      <  5   \      0  &   1  �   X     �     �                     )     7     D     U      e  	   �  \   �  c   �     Q     b     n     �     �     �     �     �     �     �     �     �       	     C     j   b  E   �               2  �  @  [   �  �   @	      ?
  *   `
  	   �
     �
  $   �
  '   �
  "   �
  1        P  /   e     �    �    �  ,   �       A   .     p  0   �  !   �     �  %   �  F   #     j     v  ;   �  A   �        �       �  �   �  !   8  =   Z     �                                 
                  #                           "            	                            !                                       $                       (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Applications @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box If you enable this option, KDE Applications will show small icons alongside most menu items. If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No Text No description available. Preview Radio button Ralf Nolden Show icons in menus: Tab 1 Tab 2 Text Below Icons Text Beside Icons Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Toolbars Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2013-02-23 17:39+0530
Last-Translator: Chetan Khona <chetan@kompkin.com>
Language-Team: Marathi <kde-i18n-doc@kde.org>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);
 (c) 2002 केरोल श्वेड, डेनियल मॉल्केनटिन <h1>शैली</h1> हा विभाग वापरकर्ता संवादाचे दर्शनीय घटक (जसे विजेट शैली व परिणाम) बदलण्याकरिता मदत करतो. अनुप्रयोग (&A) फाईन ट्युनिंग (&F) बटन चेकबॉक्स कॉम्बोबॉक्स् संयोजीत करा (&F)... %1 संयोजीत करा डेनियल मॉल्केनटिन वर्णन : %1 sandeep.shedmake@gmail.com, 
chetan@kompkin.com समूह बॉक्स् जर तुम्ही हा पर्याय कार्यान्वित केला तर, केडीई कार्यक्रम मेन्यू घटकांच्या बरोबर लहान चिन्हे दर्शवतील. जर तुम्ही हा पर्याय कार्यान्वित केला तर, केडीई कार्यक्रम काही महत्वाच्या बटनांबरोबर लहान चिन्हे दर्शवतील. केडीई शैली विभाग केरोल श्वेड संदिप शेडमाके, 
चेतन खोना मजकूर नाही वर्णन उपलब्ध नाही. पूर्वावलोकन रेडियो बटन राल्फ नोल्डेन मेन्यूमध्ये चिन्ह दर्शवा : टॅब 1 टॅब 2 चिन्हाच्या खाली मजकूर चिन्हाच्या बाजूने मजकूर फक्त मजकूर या शैली करिता संयोजना संवाद दाखल करतेवेळी त्रुटी आढळली. हा भाग तुम्ही आता निवडलेल्या शैलीचे पूर्वावलोकन दर्शवितो. (ती शैली पूर्ण डेस्कटॉपला लागू न करिता.) विजेट शैली पर्यायांचे तपशील निवडण्याकरिताचे पान साधनपट्ट्या संवाद दाखल करू शकत नाही विजेट शैली : 