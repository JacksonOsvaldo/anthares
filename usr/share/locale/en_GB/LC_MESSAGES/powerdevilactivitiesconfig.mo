��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �       �       �     �     �     �     �       	   +     5     D  &   ^     �     �     �  	   �     �  �   �  �   j  t   �     t	  +   �	     �	                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-09-17 12:56+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  min Act like Always Define a special behaviour Don't use special settings steve.allewell@gmail.com Hibernate Steve Allewell Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behaviour. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. Activity "%1" Use separate settings (advanced users only) after 