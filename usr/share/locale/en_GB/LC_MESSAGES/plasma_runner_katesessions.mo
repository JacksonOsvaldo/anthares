��          <      \       p   !   q   3   �      �   �  �   !   m  3   �     �                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2009-03-30 20:49+0100
Last-Translator: Malcolm Hunter <malcolm.hunter@gmx.co.uk>
Language-Team: British English <kde-en-gb@kde.me.uk>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session 