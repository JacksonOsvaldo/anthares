��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7  �  ;  &   �  +   �     &     .  	   6     @     \     b     k     p  (   �     �     �  ,   �     �     �                  
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-07-19 12:06+0100
Last-Translator: Stephen Allewell
Language-Team: British English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes 