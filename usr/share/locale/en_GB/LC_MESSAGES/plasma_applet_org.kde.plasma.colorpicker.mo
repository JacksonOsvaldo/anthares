��          |      �          %   !     G     U     c     u     �     �  
   �     �     �  $   �  �  �  &   �     �     �     �     �               "     .     <  $   I     
   	                                                   Automatically copy color to clipboard Clear History Color Options Copy to Clipboard Default color format: General Open Color Dialog Pick Color Pick a color Show history When pressing the keyboard shortcut: Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-12 03:03+0200
PO-Revision-Date: 2016-12-17 17:22+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Automatically copy colour to clipboard Clear History Colour Options Copy to Clipboard Default colour format: General Open Colour Dialogue Pick Colour Pick a colour Show history When pressing the keyboard shortcut: 