��    *      l  ;   �      �     �     �     �     �     �     �     �  	   �     	          5     J     W     o     �     �  
   �     �  
   �     �     �     �     �     �     �                    -     C  U   X  Z   �  O   	  D   Y     �     �     �  	   �  	   �     �     �  �  �     �     �     �     �     �     �     �  	   �     �     	     	     .	     ;	     S	     j	     r	  
   �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     '
     <
     \
     v
  (   �
     �
     �
     �
  	   �
  	   �
     �
     �
                                             &                   	         )          '   "         (                           *   $          #                          %   !      
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Copy Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2017-12-22 16:02+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Copy Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Connection to the device failed The device is unreachable The device is not ready %1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes 