��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *  �  .     �     �     �     �          4     ;     R  !   Z     |  !   �     �     �     �     �     �     �     �               ,  2   A  @   t  	   �     �     �  $   �     �     �     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-09-03 14:12+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates KWin Plasma Unable to load script file <b>%1</b> Uninstallable Use 