��    %      D  5   l      @      A     b     n  -   �  #   �  #   �  ?     1   C     u     �  H   �  L   �     $  V   ,  N   �     �  
   �     �     �               5  2   =  
   p     {  )   �  8   �  #   �  .   "  -   Q  D     6   �  0   �  A   ,  =   n  9   �  �  �      �
     �
     �
  -   �
               !     .     ;     O     T     h     ~     �     �     �  
   �     �     �     �     �     �     �  
   
       )   5     _  #   q  .   �     �     �  	   �     �       
                          !                             
       #                                       %                                     $      	           "                                %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 seconds ago, keep short10 s ago 30 seconds ago, keep short30 s ago A button tooltip; expands the item to show detailsShow Details A button tooltip; hides item detailsHide Details Clear Notifications Copy Either just 1 dir or m of n dirs are being processed1 dir %2 of %1 dirs Either just 1 file or m of n files are being processed1 file %2 of %1 files History How much many bytes (or whether unit in the locale has been copied over total%1 of %2 Indicator that there are more urls in the notification than previews shown+%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... Placeholder is job name, eg. 'Copying'%1 (Paused) Select All Show a history of notifications Show application and system notifications Speed and estimated time to completion%1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup minutes ago, keep short%1 min ago %1 min ago notification was added n days ago, keep short%1 day ago %1 days ago notification was added yesterday, keep shortYesterday notification was just added, keep shortJust now placeholder is row description, such as Source or Destination%1: the job, which can be anything, failed to complete%1: Failed the job, which can be anything, has finished%1: Finished Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:07+0100
PO-Revision-Date: 2017-12-26 16:41+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1 notification %1 notifications %1 of %2 %3 %1 running job %1 running jobs &Configure Event Notifications and Actions... 10 s ago 30 s ago Show Details Hide Details Clear Notifications Copy 1 dir %2 of %1 dirs 1 file %2 of %1 files History %1 of %2 +%1 Information Job Failed Job Finished More Options... No new notifications. No notifications or jobs Open... %1 (Paused) Select All Show a history of notifications Show application and system notifications %1 (%2 remaining) Track file transfers and other jobs Use custom position for the notification popup %1 min ago %1 min ago %1 day ago %1 days ago Yesterday Just now %1: %1: Failed %1: Finished 