��    	      d      �       �      �      �      �   E   
     P     f     o     �  �  �     I     Q     b  E   r     �     �     �     �        	                                      Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-12-23 13:02+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Disable Disable touchpad Enable touchpad No mouse was detected.
Are you sure you want to disable the touchpad? No touchpad was found Touchpad Touchpad is disabled Touchpad is enabled 