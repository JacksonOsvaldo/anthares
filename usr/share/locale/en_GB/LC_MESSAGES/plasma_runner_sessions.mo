��          �   %   �      P  �  Q          )  6   9  -   p     �     �     �     �  (   �  d        x     �     �     �     �     �     �                7  "   X     {     �     �  �  �  �  j     /
     B
  6   R
  -   �
     �
     �
     �
     �
  (     d   ,     �     �     �     �     �     �     �     �     �     �     �            
                                  	                                                                      
                        <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer User sessionssessions Warning - New Session lock screen commandlock log out log out commandLogout log out commandlogout new session restart computer commandreboot restart computer commandrestart shutdown computer commandshutdown switch user switch user commandswitch switch user commandswitch :q: Project-Id-Version: plasma_runner_sessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-12 11:59+0000
Last-Translator: Andrew Coles <andrew_coles@yahoo.co.uk>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 <p>You have chosen to open another desktop session.<br />The current session will be hidden and a new login screen will be displayed.<br />An F-key is assigned to each session; F%1 is usually assigned to the first session, F%2 to the second session and so on. You can switch between sessions by pressing Ctrl, Alt and the appropriate F-key at the same time. Additionally, the KDE Panel and Desktop menus have actions for switching between sessions.</p> Lists all sessions Lock the screen Locks the current sessions and starts the screen saver Logs out, exiting the current desktop session New Session Reboots the computer Restart the computer Shutdown the computer Starts a new session as a different user Switches to the active session for the user :q:, or lists all active sessions if :q: is not provided Turns off the computer sessions Warning - New Session lock log out Logout logout new session reboot restart shutdown switch user switch switch :q: 