��          �   %   �      @     A  !   Y      {     �      �     �     �  +        =     N     [  (   z     �  ,   �  7   �     !  7   :     r  ]   �  1   �  	   "     ,  "   ?  5   b  �  �     A  !   Y      {     �     �     �     �  +   	     5     F     S  (   b     �  ,   �  7   �     		  7   "	     Z	  ]   z	  1   �	  	   

     
  "   '
  5   J
                    
         	                                                                                                 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens EMAIL OF TRANSLATORSYour emails Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin NAME OF TRANSLATORSYour names No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-28 03:05+0200
PO-Revision-Date: 2017-01-07 15:12+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Cannot start initramfs. Cannot start update-alternatives. Configure Plymouth Splash Screen Download New Splash Screens steve.allewell@gmail.com Get New Boot Splash Screens... Initramfs failed to run. Initramfs returned with error condition %1. Install a theme. Marco Martin Steve Allewell No theme specified in helper parameters. Plymouth theme installer Select a global splash screen for the system The theme to install, must be an existing archive file. Theme %1 does not exist. Theme corrupted: .plymouth file not found inside theme. Theme folder %1 does not exist. This module lets you configure the look of the whole workspace with some ready to go presets. Unable to authenticate/execute the action: %1, %2 Uninstall Uninstall a theme. update-alternatives failed to run. update-alternatives returned with error condition %1. 