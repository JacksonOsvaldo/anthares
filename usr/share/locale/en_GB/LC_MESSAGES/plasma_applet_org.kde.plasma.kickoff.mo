��            )   �      �     �  
   �  /   �  -   �          !  
   2     =     J     `  W   i     �  ,   �  	                  !     '     -  
   :     E     W     o     �     �     �     �  1   �       �       �  
   �  	   �  
   �     �        
             )     ?  W   H     �  ,   �  
   �     �     �                 
        %     7     O     f     y     �     �     �     �                                                                                  
                                	                       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favorites Show In Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-11-26 15:41+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 %1@%2 %2@%3 (%1) Choose... Clear Icon Add to Favourites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favourites Hidden Tabs History Icon: Leave Menu Buttons Often Used On All Activities On The Current Activity Remove from Favourites Show In Favourites Show applications by name Sort alphabetically Switch tabs on hover Type to search... Visible Tabs 