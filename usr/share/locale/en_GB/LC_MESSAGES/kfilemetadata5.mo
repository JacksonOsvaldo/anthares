��    H      \  a   �            !     5     D     R     f     s     �     �     �     �     �     �     �     �          "     0     B     P     ]     m     }     �     �     �     �     �     �     �               &     3     @     \     s     �      �     �     �     
	     %	     =	  #   \	     �	     �	     �	  "   �	     
  $   !
     F
     c
     
     �
     �
  3   �
       $   (     M  &   f  <   �  !   �  8   �  0   %  !   V  C   x  X   �  P     R   f      �  +   �  �       �     �     �     �     �     �     �     �     �       	             %     .     D     M  
   T     _     f     l     u     ~  
   �     �  	   �     �     �     �     �     �     �     �     �     �  
   �               &     ;     O     c     r     ~     �     �     �     �     �     �          '     8     H     \     n     �     �     �     �     �     �  
   �     �     �  
   �                -     F     `     g        -      0   G   C   4                      +   ,                 "      '   	          <             ;   9   3              *   A   2   %      )   :   =                           @   5                   H   &   B   $             /   
      ?       E       D   6                 F          #   !   1                  8       7       .      (       >       @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDocument Generated By @labelDuration @labelFolder @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music disc numberDisc Number @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label the URL a file was originally downloded fromDownloaded From @label the message ID of an email this file was attached toE-Mail Attachment Message ID @label the sender of an email this file was attached toE-Mail Attachment Sender @label the subject of an email this file was attached toE-Mail Attachment Subject @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2017-11-26 15:54+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Album Artist Archive Artist Aspect Ratio Audio Author Bitrate Channels Comment Composer Copyright Creation Date Document Document Generated By Duration Folder Frame Rate Height Image Keywords Language Lyricist Page Count Presentation Publisher Release Year Sample Rate Spreadsheet Subject Text Title Video Width Image Date Time Image Make Image Model Image Orientation Photo Aperture Value Photo Exposure Bias Photo Exposure Time Photo F Number Photo Flash Photo Focal Length Photo Focal Length 35mm Photo GPS Altitude Photo GPS Latitude Photo GPS Longitude Photo ISO Speed Rating Photo Metering Mode Photo Original Date Time Photo Saturation Photo Sharpness Photo White Balance Photo X Dimension Photo Y Dimension Template Creation Album Disc Number Genre Track Number Draft Translations Line Count Translatable Units Translations Word Count Downloaded From E-Mail Attachment Message ID E-Mail Attachment Sender E-Mail Attachment Subject Author Last Update 