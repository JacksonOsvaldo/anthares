��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �  �  �  	   �  	   �     �     �     �     �     �     �     �     �     �          )     H  
   `     k  ?   w     �  >   �  	   �     	       S   6  8   �     �  �  �     c     k     {  
   �     �     �     �  
   �     �  A   �          5  
   D     O     X     s     �     �     �     �     �     �     �     �               &     :     I     `     w     �  T   �     �  S   �            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2014-06-28 18:17+0100
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
  [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. anarchist_tomato@herzeleid.net, steve.allewell@gmail.com Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up Ken Knight, Steve Allewell New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? 