��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �  �  �  
   V  	   a     k     {  3   �     �     �     �     �     �          	     %     8     A     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-01-15 12:10+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: English <kde-l10n-en_gb@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number colour Path to custom image Piece colour Show numerals Shuffle Size Solve by arranging in order Solved! Try again. Time: %1 Use custom image 