��          �      �       0     1  �  H     �       &         ?     `     n     v     �     �  (   �  �  �     �  �  �     f     w  &   �     �     �     �     �     �     �  (            
                 	                                (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialog box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events EMAIL OF TRANSLATORSYour emails Event source: KNotify NAME OF TRANSLATORSYour names Olivier Goffart Original implementation System Notification Control Panel Module Project-Id-Version: kcmnotify
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-09 07:18+0200
PO-Revision-Date: 2017-11-26 15:36+0000
Last-Translator: Steve Allewell <steve.allewell@gmail.com>
Language-Team: British English <kde-i18n-doc@kde.org>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
 (c) 2002-2006 KDE Team <h1>System Notifications</h1>Plasma allows for a great deal of control over how you will be notified when certain events occur. There are several choices as to how you are notified:<ul><li>As the application was originally designed.</li><li>With a beep or other noise.</li><li>Via a popup dialogue box with additional information.</li><li>By recording the event in a logfile without any additional visual or audible alert.</li></ul> Carsten Pfeiffer Charles Samuels Disable sounds for all of these events clulow@upnaway.com Event source: KNotify Aston Clulow Olivier Goffart Original implementation System Notification Control Panel Module 