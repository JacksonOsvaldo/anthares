��          �      \      �     �  �   �  m   r  `   �     A     N     f     s           �     �  '   �     �               %  ?   2  Y   r  +   �    �       �   -  �   �  }   w     �  (   	     -	  
   C	  7   N	  '   �	     �	  B   �	  4   
  
   7
     B
     [
  F   m
  �   �
  G   D                                                             	                               
       (c) 2003-2007 Fredrik Höglund <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Cursor Theme Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Fredrik Höglund Get new color schemes from the Internet NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-02-09 19:08+0200
Last-Translator: Abdalrahim Fakhouri <abdilra7eem@yahoo.com>
Language-Team: Arabic <linuxac-kde-arabic-team@googlegroups.com>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.2
 (c) 2003-2007 Fredrik Höglund <qt>أمتأكد أنك تريد إزالة سمة المؤشر <i>%1</i>؟<br />هذا سوف يحذف جميع الملفات التي ثبتت بواسطة هذه السمة.</qt> <qt>لا يمكن حذف السمة التي تستخدمها حاليا.<br />يجب عليك أن تبدل أولا إلى سمة أخرى.</qt> توجد مسبقا سمة باسم %1 في مجلد سمات الأيقونات, هل تريد استبدالها بهذه؟ التأكيد تغيَرت إعدادات المؤشر سِمة المؤشر الوصف اسحب أو أكتب وصلة عنوان السِمة metehyi@free.fr,zayed.alsaidi@gmail.com Fredrik Höglund احصل على مخطط ألوان جديد من الإنترنت Mohamed SAAD محمد سعد,زايد السعيدي الاسم أطمس السِمة ؟ أزل السمة الملف %1 لا يظهرعلى أنه أرشيف سِمة مؤشر. غير قادر على تنزيل  أرشيف سِمة المؤشر، من فضلك افحص إذا ما كان العنوان %1 صحيحاً. غير قادر على  إيجاد أرشيف سِمة المؤشر %1. 