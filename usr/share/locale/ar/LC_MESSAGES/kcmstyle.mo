��            )   �      �  &   �  �   �     O     g     n     w     �     �     �     �      �  	   �  �   �  c   �          %     1     P     j     r          �     �  	   �  C   �  j   �  E   P     �     �  
  �  &   �  �   �     �	     �	     �	     �	     �	     �	     
     
  '   &
     N
  #  d
  �   �      "     C  4   O     �     �     �     �  
   �  
   �     �  Y   �  �   G  W   �  7   -     e            	                                                                                
                                             (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2008-12-03 06:16+0400
Last-Translator: zayed <zayed.alsaidi@gmail.com>
Language-Team: Arabic <linuxac-kde-arabic-team@googlegroups.com>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: KBabel 1.11.4
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>الأسلوب</h1>هذه الوحدة تسمح لك بتعديل المظهر المرئي لعناصر واجهة الرسومية مثل التأثيرات و أسلوب الودجات. &تضبيط دقيق زر خانة تأشير مربع مركب اض&بط... اضبط %1 Daniel Molkentin الوصف: %1 metehyi@free.fr,zayed.alsaidi@gmail.com مربع مجموعة من هنا تستطيع اختيار قائمة من أساليب ودج (مثلاً الطريقة التي ترسم بها الأزرار) و الذي يمكن دمجه أو لا في سِمة (معلومات إضافية مثل تركيب رخامي أو تدرّج في الألوان). إذا مكّنت هذا الخيار، فإن تطبيقات كدي ستظهر  أيقونات صغيرة بجانب بعض الأزرار المهمة. وحدة الأسلوب لكدي Karol Szwed Mohamed SAAD محمد سعد,زايد السعيدي لا يوجد وصف. معاينة زر الدائرة Ralf Nolden لسان 1 لسان 2 النص فقط هناك خطأ في تحميل مربع حوار التشكيل لهذا الأسلوب. هذه المنطقة تظهر معاينة للأسلوب المنتقى حالياً بدون تطبيقه على كل سطح المكتب. تسمح لك هذه الصفحة باختيار تفاصيل أسلوب الودجات غير قادر على تحميل مربع الحوار أسلوب الودجة: 