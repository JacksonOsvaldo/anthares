��          �      \      �     �     �     �     �              	   <     F     e  &        �     �     �  �   �  �   y  t     7   �  +   �     �    �  
          
          +  )   L  $   v     �  A   �  .   �  @     @   V  8   �  6   �  �   	  �   �	  �   �
       Y        q                                        
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2014-07-04 21:18+0300
Last-Translator: Abdalrahim G. Fakhouri <abdilra7eem@yahoo.com>
Language-Team: Arabic <Arabic <doc@arabeyes.org>>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
X-Project-Style: kde
 دقيقة تصرف هكذا دائما عرف سلوكًا خاصًّا لا تستخدم إعدادات خاصة it@abdelos.com,abdilra7eem@yahoo.com أسبت عبدالرحمن أسامة,عبدالرحيم الفاخوري لا تقم بإطفاء الشاشة أبدا لا تقم بتعليق أو إطفاء الحاسوب أبدا الحاسوب يعمل على الطاقة الاعتيادية الحاسوب يعمل على طاقة البطارية الحاسوب يعمل على بطارية ضعيفة يبدو أن خدمة إدارة الطاقة لا تعمل.
يمكن حل تلك المشكلة عن طريق بدء الخدمة أو جدولتها من خلال خيارات "بدء التشغيل و الإطفاء" . خدمة النشاط لا تعمل.
يجب تشغيل مدير النشاط حتى تستطيع ضبط نشاط خاص بسلوك إدارة الطاقة. خدمة النشاط تعمل بوظائف ضئيلة.
أسماء و أيقونات الأنشطة قد لا تكون متاحة. النشاط "%1" استخدم إعدادات منفصلة (للمستخدمين المتقدمين فقط) بعد 