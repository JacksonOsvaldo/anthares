��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '     �  <  .   6  C   e  K   �  6   �  6   ,  =   c  '   �     �  2   �         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: KDE 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2014-07-04 11:38+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 تعذّر اكتشاف نوع MIME للملف تعذّر العثور على كلّ الدوال المطلوبة تعذّر العثور على الموفّر بالمقصد المحدّد خطأ عند محاولة تنفيذ السكرِبت مسار غير صالح للموفّر المطلوب لم يكن ممكنًا قراءة الملف المحدّد لم تكن الخدمة متوفّرة خطأ مجهول عليك تحديد وصلة لهذه الخدمة 