��    Q      �  m   ,      �  C   �     %     4     E     [     z     �     �     �     �  
   �     �     �     �     �  	             ,     A  ,   M  	   z     �  
   �     �     �     �     �     	     	     !	     9	     A	  	   a	     k	     s	     �	     �	     �	     �	  	   �	  
   �	     �	     �	     �	  
   �	     �	     
     
     %
     6
     D
     V
     l
     }
     �
     �
     �
     �
  	   �
     �
     �
     �
          &     ?     T  	   j     t  ,   �     �     �     �     �     �     �     �             #   <     `  �  h     ]  !   p     �  %   �  2   �     
     &     .  )   C     m     |     �     �     �     �     �     �           (  h   >     �  4   �     �  $     $   &  "   K     n     �     �  "   �     �     �     �     �               &     >     G     _     q  %   �     �     �  
   �     �     �          +     E     e     �      �     �  .   �     	  
        *     @     Q     q  +   ~  $   �  $   �  $   �  "        <     E  8   _     �  5   �     �     �               9     Q  '   f  I   �     �                    2           	   H   '       .   L       %      (   ;                    #   :       Q       9   ,   F   !      3   1      )   A      C   
              O   0   7                             @   6      8       -   G       E           B       D   5         >       $   P   I   /      ?   M   *   K                 =   N          +   4                      <                &   "   J          @action opens a software center with the applicationManage '%1'... Add to Desktop Add to Favorites Add to Panel (Widget) Align search results to bottom All Applications App name (Generic name)%1 (%2) Applications Apps & Docs Behavior Categories Computer Contacts Description (Name) Description only Documents Edit Application... Edit Applications... End session Expand search to bookmarks, files and emails Favorites Flatten menu to a single level Forget All Forget All Applications Forget All Contacts Forget All Documents Forget Application Forget Contact Forget Document Forget Recent Documents General Generic name (App name)%1 (%2) Hibernate Hide %1 Hide Application Lock Lock screen Logout Name (Description) Name only Open with: Pin to Task Manager Places Power / Session Properties Reboot Recent Applications Recent Contacts Recent Documents Recently Used Removable Storage Remove from Favorites Restart computer Run Command... Run a command or a search query Save Session Search Search results Search... Searching for '%1' Session Show Contact Information... Show applications as: Show recent applications Show recent contacts Show recent documents Shut Down Sort alphabetically Start a parallel session as a different user Suspend Suspend to RAM Suspend to disk Switch User System System actions Turn off computer Type to search. Unhide Applications in '%1' Unhide Applications in this Submenu Widgets Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:03+0100
PO-Revision-Date: 2017-02-14 20:16+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 أدر ’%1‘... أضف إلى سطح المكتب أضف إلى المفضّلة أضف إلى اللوحة (ودجة) حاذِ نتائج البحث إلى الأسفل كلّ التّطبيقات %1 (%2) التّطبيقات التّطبيقات والمستندات السّلوك الفئات الحاسوب المتراسلون الوصف (الاسم) الوصف فقط المستندات حرّر التّطبيق... حرّر التّطبيقات... أنهِ الجلسة وسّع البحث إلى العلامات، والملفّات والبُرد الإلكترونيّة المفضّلة سطّح القائمة إلى مستوًى واحد انسَ الكلّ انسَ كلّ التّطبيقات انسَ كلّ المتراسلين انسَ كلّ المستندات انسَ التّطبيق انسَ المتراسل انسَ المستند انسَ آخر المستندات عامّ %1 (%2) أسبت أخفِ %1 أخفِ التّطبيق اقفل اقفل الشّاشة اخرج الاسم (الوصف) الاسم فقط افتح ب‍: ثبّت في مدير المهامّ الأماكن الطّاقة/الجلسة خصائص أعد الإقلاع آخر التّطبيقات آخر المتراسلون آخر المستندات المستخدمة حديثًا التّخزين المنفصل أزل من المفضّلة أعد تشغيل الحاسوب شغّل أمرًا... شغّل أمرًا أو استعلام بحث احفظ الجلسة البحث نتائج البحث ابحث عن... نتائج البحث عن '%1' الجلسة أظهر معلومات المتراسل... أظهر التّطبيقات ك‍: أظهر آخر التّطبيقات أظهر آخر المتراسلين أظهر آخر المستندات أطفئ افرز أبجديًّا ابدأ جلسة موازية كمستخدم مختلف علّق علّق إلى الذّاكرة العشوائيّة علّق إلى القرص بدّل المستخدم النّظام إجراءات النّظام أطفئ الحاسوب اكتب للبحث. أظهر التّطبيقات في '%1' أظهر التّطبيقات في هذه القائمة الفرعيّة الودجات 