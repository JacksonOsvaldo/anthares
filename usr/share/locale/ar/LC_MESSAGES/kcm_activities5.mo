��    (      \  5   �      p     q     �  )   �     �     �     �          &  #   F     j  
   �     �  %   �  +   �     �          "  @   /     p     �     �     �     �     �     �     �     �     �            .   "     Q  F   l  (   �  	   �  	   �  	   �  7   �  "   2  �  U     J	      ]	  /   ~	     �	     �	     �	     �	     �	  "   �	     
     .
     =
  %   [
  Q   �
  &   �
     �
       X        x      �     �     �     �                    9  %   E     k     t  J   �  /   �  q      ;   r     �     �     �  O   �  
   5        %       (                         '                                   
      #      $      "                                 &                          	                !        &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-06-16 18:41+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 لا تت&ذكّر تنقّل بين الأنشطة تنقّل بين الأنشطة (بالعكس) طبّق ألغِ غيّر... أنشئ إعدادات النّشاط أنشئ نشاطًا جديدًا احذف النّشاط الأنشطة معلومات النّشاط أمتأكّد من حذف ’%1‘؟ امنع كلّ التّطبيقات التي ليست في هذه القائمة امسح التّأريخ الأخير أنشئ نشاطًا... الوصف: خطأ في تحميل ملفّات QML. تحقّق من التّثبيت.
ينقصه %1 ل&كلّ التّطبيقات انسَ يومًا واحدًا انسَ كلّ شيء انسَ آخر ساعة انسَ آخر ساعتين عامّ الأيقونة أبقِ التّأريخ الاسم: ف&قط لتطبيقات محدّدة أخرى السّياسة خاصّ - لا تتعقّب الاستخدام في هذا النّشاط تذكّر المستندات المفتوحة: تذكّر سطح المكتب الوهميّ الحاليّ لكلّ نشاط (يحتاج إعادة تشغيل) اختصار التّبديل إلى هذا النّشاط: الاختصارات التّبديل الخلفيّة  (لا أشهر)  (شهر واحد)  (شهران)  أشهر  شهرًا  شهر للأبد 