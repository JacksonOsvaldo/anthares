��    w      �  �   �      
     
  	   &
     0
     =
     K
     Q
     X
     i
     o
     w
     
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
       
             !     (  	   7     A  
   G  
   R     ]     i     x     ~     �     �     �     �  ;   �  -   �  #        ;     T     l  
   �     �     �  
   �     �     �     �               7     K     P  	   k      u     �     �     �  
   �     �     �               .     E     V     f     v     �     �  
   �     �  )   �     �          #     2     A     R     X     `     s  '   �     �     �     �     �     �     
           .  C   <     �  s   �           %     :     Q     f     �     �      �     �  -   �  /        4  	   =  !   G     i      �     �     �     �     �     �  �  �     �           	     &     9  	   A     K  	   [  	   e     o     {     �     �     �     �     �          "     A     `  !   o  	   �      �     �  	   �      �     �  	             !     9     J  	   e     o     �     �     �     �     �     �  *   �          /     8     N  	   [  +   e     �     �     �  !   �  &     /   *  $   Z       -   �     �  I   �          2     G     c     �  ,   �  &   �  %   �  +     -   E  /   s  1   �  1   �       F        d     v  <   �  &   �  "   �          /     G  	   a  	   k     u  ,   �  Z   �          /  '   G     o  $   �  &   �  !   �  !   �  j          �   �  3   F   '   z   %   �   )   �   "   �      !  !   ,!  3   N!     �!  .   �!  6   �!  	   �!  	   "  Z   "  M   f"  T   �"     	#     #     #     +#     9#     o   5   l   W   	             1               R   6          J         >   u   d           V          M   T       X   r   v          G   .   Q   \       a              E   -       O               ^          b   7           +   Y   C      [       4                 H       k   B   c       ]   I   *   P   (   h   w   ;   N   3   
                @      "   S      _                   L   $      9   F      D          m   U   %       K       &                     t   Z   `   !       ,   0   g   f   p          i      2   e       n   /      q       ?      <   A   #   8          =   '   s   )               :   j           %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help without name Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-16 03:10+0100
PO-Revision-Date: 2017-11-30 11:49+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 ك&تيّب %1 &عن %1 الم&قاس الأصليّ أ&ضف علامة &عُد أ&غلق ا&ضبط %1... ا&نسخ ا&حذف تبرّ&ع &حرّر العلامات... ابح&ث... الصّفحة الأ&ولى لا&ئم إلى الصّفحة ت&قدّم ا&نتقل إلى... ا&نتقل إلى سطر... ا&نتقل إلى صفحة... الصّفحة الأ&خيرة أبرِ&د... ا&نقل إلى المهملات &جديد الصّفحة ال&تّالية ا&فتح ... أل&صق الصّفحة ال&سّابقة ا&طبع... أ&نهِ أ&عِد العرض &غيّر الاسم... ا&ستبدل... أ&بلغ عن علّة... ا&حفظ ا&حفظ الإعدادات الإ&ملاء... ت&راجع لأ&على التّ&قريب... ال&تّالية ال&سّابقة أ&ظهر الفوائد عند البدء هل تعلم...؟
 اضبط فائدة اليوم عن &كدي ام&سح دقّق الإملاء في المستند امسح القائمة أغلق المستند اضبط الإ&خطارات... اضبط الا&ختصارات... اضبط أشرطة الأ&دوات... انسخ التّحديد إلى الحافظة أنشئ مستندًا جديدًا ق&صّ قصّ التّحديد إلى الحافظة أ&زل التّحديد zayed.alsaidi@gmail.com,hannysabbagh@hotmail.com,safaalfulaij@hotmail.com اكتشفه آليًّا الافتراضيّ وضع ملء ال&شاشة ابحث عن ال&تّالي ابحث عن ال&سّابق لائم إلى ارت&فاع الصّفحة لائم إلى &عرض الصّفحة عُد وراءً في المستند تقدّم أمامًا في المستند انتقل إلى الصّفحة الأولى انتقل إلى الصّفحة الأخيرة انتقل إلى الصّفحة التّالية انتقل إلى الصّفحة السّابقة انتقل لأعلى زايد السعيدي,محمد هاني صباغ,صفا الفليج لا مدخلات افتح &حديثًا افتح مستندًا كان مفتوحًا مؤخّرًا افتح مستندًا موجودًا ألصق محتوى الحافظة &عاين الطّباعة اطبع المستند أنهِ التّطبيق أ&عِد ا&عكس أعِد عرض المستند أعد آخر إجراء تراجعت عنه اعكس التّغييرات غير المحفوظة المجراة على المستند احفظ &ك‍... احفظ المستند احفظ المستند باسم آخر حدّد ال&كلّ اختر مستوى التّقريب أرسل المستند بالبريد أظهر شريط ال&قوائم أظهر شريط الأ&دوات أظهر شريط القوائم<p>يعرض شريط القوائم مجدّدًا بعد إخفائه</p> أظهر شريط ال&حالة أظهر شريط الحالة<p>يعرض شريط الحالة، وهو شريط بأسفل النّافذة يُستخدم لعرض معلومات الحالة.</p> أظهر معاينة للمستند مطبوعًا أظهر/أخف شريط القوائم أظهر/أخف شريط الحالة أظهر/أخفِ شريط الأدوات بدّل &لغة التطبيق... &فائدة اليوم تراجع عن آخر إجراء اعرض المستند بمقاسه الأصليّ ما &هذا؟ ليس مسموحًا لك حفظ الضّبط ستُسأل عن الاستيثاق قبل الحفظ &قرّب ب&عّد غيّر التّقريب لملاءمة ارتفاع الصّفحة في النّافذة غيّر التّقريب لملاءمة الصّفحة في النّافذة غيّر التّقريب لملاءمة عرض الصّفحة في النّافذة &عُد ت&قدّم ال&منزل م&ساعدة بلا اسم 