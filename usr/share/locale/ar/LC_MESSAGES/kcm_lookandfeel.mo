��          �      L      �     �  2   �          2  #   J      n     �  %   �     �  
   �     �     �          0  ]   =     �  p   �  :   *  �  e  %   Z  K   �  1   �  *   �  .   )     X  !   s  >   �  $   �     �            0   '     X  x   n  =   �  �   %	  ^   �	        
                                                                	                            Apply a look and feel package Command line tool to apply look and feel packages. Configure Look and Feel details Cursor Settings Changed Download New Look And Feel Packages EMAIL OF TRANSLATORSYour emails Get New Looks... List available Look and feel packages Look and feel tool Maintainer Marco Martin NAME OF TRANSLATORSYour names Reset the Plasma Desktop layout Show Preview This module lets you configure the look of the whole workspace with some ready to go presets. Use Desktop Layout from theme Warning: your Plasma Desktop layout will be lost and reset to the default layout provided by the selected theme. You have to restart KDE for cursor changes to take effect. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2018-01-26 08:09+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 طبّق حزمة مظهر وملمس أداة سطر أوامر لتطبيق حزم المظهر والملمس. اضبط تفاصيل المظهر والملمس تغيّرت إعدادات المؤشّر نزّل حزم مظهر وملمس جديدة safa1996alfulaij@gmail.com اجلب مظاهر جديدة... اسرد حزم المظهر والملمس المتوفّرة أداة المظهر والملمس المصين Marco Martin صفا الفليج صفّر تخطيط سطح مكتب بلازما اعرض معاينة تتيح لك هذه الوحدة ضبط مظهر كامل مساحة العمل ببعض العناصر الجاهزة. استخدم تخطيط سطح المكتب من السّمة تحذير: سيُفقد تخطيط سطح مكتب بلازما وسيُصفّر إلى المبدئيّ الذي توفّره السّمة المحدّدة. عليك إعادة تشغيل كدي لتأخذ تغييرات المؤشّر مفعولها. 