��          �            x     y  -   �     �     �     �     �     �          /     J     R     d     x  !   �  !   �  �  �     �  
   �     �  *        -     @  "   S  2   v  1   �     �     �     �          8     E                           	                                     
               Configure Notifications Description of the notified eventDescription Log to a file Mark &taskbar entry Play a &sound Run &command Select the command to run Select the sound to play Show a message in a &popup Sp&eech Speak Custom Text Speak Event Message Speak Event Name State of the notified eventState Title of the notified eventTitle Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2012-01-21 14:59+0100
Last-Translator: Abderrahim Kitouni <a.kitouni@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 اضبط التنبيهات الوصف سجل في ملف علّم مدخلة &شريط المهام اقرأ &صوتا نفذ الأ&مر اختر أمرًا لتشغيله اختر الصوت الذي تريد تشغيله أظهر رسالة في نافذة من&بثقة ال&نطق انطق نص مخصص انطق رسالة الحدث انطق اسم الحدث الحالة العنوان 