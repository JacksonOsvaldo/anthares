��    	      d      �       �      �      �   -        <  -   V  #   �  #   �     �     �     �  (   �  \   %  "   �  V   �     �  
                                           	           Current time is %1 Displays the current date Displays the current date in a given timezone Displays the current time Displays the current time in a given timezone Note this is a KRunner keyworddate Note this is a KRunner keywordtime Today's date is %1 Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: ٢٠١٥-٠٨-١٧ ١٢:٣٩+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 الوقت الآن هو %1 يعرض التّاريخ الحاليّ يعرض التّاريخ الحاليّ في المنطقة الزّمنية المعطاة يعرض الوقت الحاليّ يعرض الوقت الحاليّ في المنطقة الزّمنية المعطاة التاريخ الوقت تاريخ اليوم هو %1 