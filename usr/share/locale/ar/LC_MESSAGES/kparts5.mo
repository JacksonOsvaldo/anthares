��          �   %   �      `     a  :   i     �     �  =   �          *     G     [     t     �  6   �     �     �  #   �           >     F     T     d     �     �  ;   �  V   �  G   %     m  �  v  	   q  K   {     �     �  4   �          -  	   <     F     V     g  =   w     �     �  )   �  I    	     J	     S	  !   j	  F   �	     �	     �	  O   �	  Z   =
  R   �
     �
                                                  
                                                                   	       &Search <qt>Do you want to search the Internet for <b>%1</b>?</qt> @action:inmenuOpen &with %1 @infoOpen '%1'? @info:whatsthisThis is the file name suggested by the server @label File nameName: %1 @label Type of fileType: %1 @label:button&Open @label:button&Open with @label:button&Open with %1 @label:button&Open with... @label:checkboxRemember action for files of this type Accept Close Document Do you really want to execute '%1'? EMAIL OF TRANSLATORSYour emails Execute Execute File? Internet Search NAME OF TRANSLATORSYour names Reject Save As The Download Manager (%1) could not be found in your $PATH  The document "%1" has been modified.
Do you want to save your changes or discard them? Try to reinstall it  

The integration with Konqueror will be disabled. Untitled Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2013-08-08 13:54+0300
Last-Translator: Safa Alfulaij <safaalfulaij@hotmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 1.5
 ا&بحث <qt>هل تريد أن تبحث في الإنترنت عن <b>%1</b>؟</qt> افتح &بـ%1 ا&فتح '%1'؟ هذا الاسم الذي اقترحه الخادم الاسم: %1 النوع: %1 ا&فتح ا&فتح ب... ا&فتح بـ%1 ا&فتح ب... تذكر الإجراء للملفات من هذا النوع اقبل أغلق الوثيقة هل تريد فعلا تنفيذ '%1' ؟ zayed.alsaidi@gmail.com,hannysabbagh@hotmail.com,safaalfulaij@hotmail.com نفّذ نفِذ الملف ؟ ابحث على الانترنت  زايد السعيدي,محمد هاني صباغ,صفا الفليج ارفض احفط كــ  مدبر تنزيل البرامج (%1) لا يمكن إيجاده في PATH$ . تم تعديل الملف "%1". 
هل تريد حفظ تغييراتك أو حذفها ؟ حاول إعادة تثبيتها  

التكامل مع كونكير معطّل. غير معنون 