��    '      T  5   �      `  A   a  /   �     �     �     �               %     4  	   ;     E  3   [  	   �     �      �  $   �  *   �       	     5         V     v  
   �     �     �  5   �  6   �     2  ,   >  /   k     �     �  O   �  Z     b   m  	   �  
   �  P   �    6  =   E
  �   �
     <      D     e  &   �     �     �     �     �     �  ]     
   i     t  C   �  )   �     �     �       [     9   {  *   �     �  9   �     '  R   5  b   �     �  X         Y  $   g     �  ~   �  �   )  u   �     )     ?  o   Q        	      
                "                  &                                  #                            !         $       %       '                                            %1 is an external application and has been automatically launched <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About Active View About System Settings All Settings Apply Settings Author Configure Configure your system Determines whether detailed tooltips should be used Developer Dialog EMAIL OF TRANSLATORSYour emails Expand the first level automatically General config for System SettingsGeneral Help Icon View Internal module representation, internal module model Internal name for the view used Keyboard Shortcut: %1 Maintainer NAME OF TRANSLATORSYour names No views found Provides a categorized icons view of control modules. Provides a classic tree-based view of control modules. Relaunch %1 Reset all current changes to previous values Search through a list of control modulesSearch Show detailed tooltips System Settings System Settings was unable to find any views, and hence has nothing to display. System Settings was unable to find any views, and hence nothing is available to configure. The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View View Style Welcome to "System Settings", a central place to configure your computer system. Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: ٢٠١٦-٠٢-١٠ ١٢:٥٣+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 التّطبيق %1 خارجي وقد أُطلق آليًّا <i>لا يحوي شيئًا</i> <i>يحوي عنصر واحد</i> <i>يحوي عنصرين</i> <i>يحوي %1 عناصر</i> <i>يحوي %1 عنصرًا</i> <i>يحوي %1 عنصر</i> عن %1 عن الوحدة النّشطة عن العرض النّشط عن «إعدادات النّظام» كلّ الإعدادات طبّق الإعدادات المؤلّف اضبط اضبط نظامك يحدّد فيما إذا كان يجب استخدام التّلميحات المفصّلة مطوّر حواريّ alsadk@gmail.com,zayed.alsaidi@gmail.com,safa1996alfulaij@gmail.com وسّع أوّل مستوى آليًّا عامّ مساعدة وضع الأيقونات تمثيل الوحدات الدّاخلي، نموذج الوحدات الدّاخليّة الاسم الدّاخليّ للعرض المستخدم اختصار لوحة المفاتيح: %1 المصين الصادق, زايد السعيدي,صفا الفليج لا عروض يوفّر وضعًا لأيقونات مصنّفة لوحدات التّحكّم. يوفّر وضعًا تقليديًّا مبنيّ على شجرة لوحدات التّحكّم. أعد إطلاق %1 صفّر كلّ التّعديلات الحاليّة إلى قيمها السّابقة ابحث عن أظهر تلميحات مفصّلة إعدادات النّظام تعذّر على «إعدادات النّظام» العثور على أيّة عروض، لذلك لا شيء ليُعرض. تعذّر على «إعدادات النّظام» العثور على أيّة عروض، لذلك لا شيء متوفّر لضبطه. تغيّرت إعدادات الوحدة الحاليّة.
أتريد تطبيق الإعدادات أو رفضها؟ وضع الشّجرة نمط العرض مرحبا في "إعدادات النّظام"، مكان مركزيّ لضبط إعدادات الحاسوب. 