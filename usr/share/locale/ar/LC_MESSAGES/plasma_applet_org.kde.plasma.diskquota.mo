��    
      l      �       �   
   �      �        .   0     _     t     �  (   �  8   �  �         3        L  D   h     �  
   �     �      �      �        
                             	        Disk Quota No quota restrictions found. Please install 'quota' Quota tool not found.

Please install 'quota'. Running quota failed e.g.: 12 GiB of 20 GiB%1 of %2 e.g.: 8 GiB free%1 free example: Quota: 83% usedQuota: %1% used usage of quota, e.g.: '/home/bla: 38% used'%1: %2% used Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-02-14 20:06+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 حصّة القرص لم يُعثر على تحديدات للحصّة. فضلًا ثبّت 'quota' لم يُعثر على أداة Quota.

فضلًا ثبّت 'quota'. فشل تشغيل quota %1 من %2 %1 حرّة الحصّة: %1% مستخدمة ‏‎%1‎: ‏%2% مستخدم 