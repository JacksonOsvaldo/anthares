��          �      �       H     I     a      m     �     �     �  
   �     �  &   �          .     L  1   b    �  #   �     �  2   �          #  +   =     i     y  6   �     �  %   �  !     D   .         	                                    
                    Configure Desktop Theme David Rosca EMAIL OF TRANSLATORSYour emails Get New Themes... Install from File... NAME OF TRANSLATORSYour names Open Theme Remove Theme Theme Files (*.zip *.tar.gz *.tar.bz2) Theme installation failed. Theme installed successfully. Theme removal failed. This module lets you configure the desktop theme. Project-Id-Version: kcm_desktopthemedetails
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-22 05:21+0100
PO-Revision-Date: 2018-01-26 08:07+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 اضبط سمة سطح المكتب David Rosca zayed.alsaidi@gmail.com,safa1996alfulaij@gmail.com اجلب سمات جديدة... ثبّت من ملفّ... زايد السعيدي,صفا الفليج افتح سمة أزِل السّمة ملفّات السّمات (*.zip *.tar.gz *.tar.bz2) فشل تثبيت السّمة. ثُبّتت السّمة بنجاح. فشلت إزالة السّمة. تتيح لك هذه الوحدة ضبط سمة سطح المكتب. 