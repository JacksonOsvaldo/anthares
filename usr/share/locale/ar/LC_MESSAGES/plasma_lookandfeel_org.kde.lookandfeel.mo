��    0      �  C         (     )     -     2  .   A  5   p     �     �     �     �  	   �     �     �            1   *     \     b     o     �  
   �     �  '   �     �     �     �     �       '        0     ?     F     N  5   W     �     �     �     �     �  $   �  A   �  �   0            C   .  '   r     �     �  �  �     �
     �
     �
     �
  *   �
     $     -     M     V     j      s      �     �  (   �  �   �     �     �     �     �  !   �          (  
   >     I  %   _  !   �     �    �  %   �     �     �       �        �     �     �               8     D      Z     {     �  3   �     �     �                    #   /       -                      $                +         (              .   !                        %   0      &       *       	         ,                               
         '                    )            "       %1% Back Battery at %1% Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout Next track No media playing Nobody logged in on that sessionUnused OK Password Play or Pause media Previous track Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-09-01 10:50+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 %1% عُد البطّاريّة عند %1% بدّل التّخطيط لوحة المفاتيح الوهميّة ألغِ مفتاح Caps Lock مفعّل أغلق أغلق البحث اضبط اضبط ملحقات البحث جلسة سطح المكتب: %1 مستخدم آخر تخطيط لوحة المفاتيح: %1 يخرج بعد لحظات يخرج بعد دقيقة واحدة يخرج بعد دقيقتين يخرج بعد %1 دقائق يخرج بعد %1 دقيقة يخرج بعد %1 دقيقة لِج فشل الولوج لِج كمستخدم آخر اخرج المقطوعة التّالية لا وسائط تعمل غير مستخدمة حسنًا كلمة السّرّ شغّل الوسيط أو ألبثه المقطوعة السّابقة أعد الإقلاع يعيد الإقلاع بعد لحظات يعيد الإقلاع بعد دقيقة واحدة يعيد الإقلاع بعد دقيقتين يعيد الإقلاع بعد %1 دقائق يعيد الإقلاع بعد %1 دقيقة يعيد الإقلاع بعد %1 دقيقة الاستعلامات الأخيرة أزل أعد التّشغيل أطفئ يطفئ بعد لحظات يطفئ بعد دقيقة واحدة يطفئ بعد دقيقتين يطفئ بعد %1 دقائق يطفئ بعد %1 دقيقة يطفئ بعد %1 دقيقة ابدأ جلسة جديدة علّق بدّل بدّل الجلسة بدّل المستخدم ابحث... ابحث ب‍'%1'... «بلازما» من «كدي» فكّ القفل فشل فكّ القفل في جلسة الطّرفيّة %1 (العرض %2) جلسة الطرفيّة %1 اسم المستخدم %1 (%2) 