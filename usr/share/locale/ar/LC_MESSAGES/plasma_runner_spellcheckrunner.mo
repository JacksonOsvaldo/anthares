��    	      d      �       �      �      �           "     *  Q   ?     �     �    �  &   �     �          +  .   4     c  #   i     �                            	                  &Require trigger word &Trigger word: Checks the spelling of :q:. Correct Spell Check Settings Spelling checking runner syntax, first word is trigger word, e.g.  "spell".%1:q: Suggested words: %1 spell Project-Id-Version: krunner_spellcheckrunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2008-12-26 17:49+0400
Last-Translator: zayed <zayed.alsaidi@gmail.com>
Language-Team: Arabic <linuxac-kde-arabic-team@googlegroups.com>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: KBabel 1.11.4
 ت&حتاج إلى كلمة قادحة ال&كلمة القادحة: يتأكد من إملاء :q:. صحيح إعدادات التدقيق الإملائي %1:q: الكلمات المقترحة: %1 تهجئ 