��            )   �      �     �     �     �  	   �  A   �  >     9   S  9   �  9   �  W     E   Y     �      �     �  7   �        X   <     �     �     �     �     �          %     @     V     ]     d     t    |     �  
   �  #   �     �     �     �     �     �  
   	  I   	     c	  
   }	  )   �	  H   �	  f   �	  >   b
  �   �
     '     0  0   I  &   z     �     �  %   �  1   �       
             )                                                                                                       	                 
                 Accurate Always Author: %1
License: %2 Automatic Category of Desktop Effects, used as section headerAccessibility Category of Desktop Effects, used as section headerAppearance Category of Desktop Effects, used as section headerCandy Category of Desktop Effects, used as section headerFocus Category of Desktop Effects, used as section headerTools Category of Desktop Effects, used as section headerVirtual Desktop Switching Animation Category of Desktop Effects, used as section headerWindow Management Crisp EMAIL OF TRANSLATORSYour emails Enable compositor on startup Exclude Desktop Effects not supported by the Compositor Exclude internal Desktop Effects Hint: To find out or configure how to activate an effect, look at the effect's settings. Instant KWin development team NAME OF TRANSLATORSYour names Only for Shown Windows OpenGL Platform InterfaceEGL OpenGL Platform InterfaceGLX Re-enable OpenGL detection Re-use screen content Search Smooth Smooth (slower) XRender Project-Id-Version: kcmkwincompositing
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2014-07-04 19:16+0300
Last-Translator: Abdalrahim G. Fakhouri <abdilra7eem@yahoo.com>
Language-Team: Arabic <Arabic <doc@arabeyes.org>>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 دقيق دائما المؤلف: %1
الترخيص: %2 آليّ إتاحة الوصول المظهر حلوى التركيز أدوات مؤثر التبديل بين أسطح المكتب الافتراضية إدارة النوافذ الحدة chahibi@gmail.com,zayed.alsaidi@gmail.com مكّن تأثيرات سطح المكتب عند بدء التشغيل استثنِ مؤثرات سطح المكتب التي لا يدعمها برنامج المؤثرات استثنِ مؤثرات سطح المكتب الداخلية تلميحة: لإيجاد أو لضبط كيفية تفعيل هذا التأثير ، انظر إلى إعدادات التأثير لحظي فريق تطوير KWin يوسف الشهيبي, زايد السعيدي فقط للنوافذ المعروضة EGL GLX أعد تفعيل اكتشاف OpenGL استخدام مسبق لمحتوى الشاشة بحث سَلِس سَلِس (بطيء) XRender 