��          �   %   �      P     Q  
   W  /   b  -   �     �     �  
   �     �     �       W        q  ,   �  	   �     �     �     �     �     �     �                .  1   C     u  �  �     w     �     �     �     �     �     �       "        <  �   K      �  l        y     �     �     �     �     �     �  *   	     8	  6   R	     �	     �	                     
                                                                         	                               %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Add to Favorites All Applications Appearance Applications Applications updated. Computer Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Expand search to bookmarks, files and emails Favorites Hidden Tabs History Icon: Leave Menu Buttons Remove from Favorites Show applications by name Sort alphabetically Switch tabs on hover Type is a verb here, not a nounType to search... Visible Tabs Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-16 05:38+0100
PO-Revision-Date: 2017-02-14 20:17+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 %1 على %2 %2 على %3 (%1) اختر... امسح الأيقونة أضف إلى المفضّلة كلّ التّطبيقات المظهر التّطبيقات حُدّثت التّطبيقات. الحاسوب اسحب الألسنة داخل المربّعات لإظهارها أو إخفائها، أو أعد ترتيب الألسنة المرئيّة بسحبها. حرّر التّطبيقات... وسّع البحث ليشمل العلامات، والملفّات والبُرد الإلكترونيّة المفضّلة الألسنة المخفيّة التّأريخ الأيقونة: اترك أزرار القائمة أزل من المفضّلة أظهر التّطبيقات بالاسم افرز أبجديًّا بدّل الألسنة عند المرور عليها اكتب للبحث... الألسنة المرئيّة 