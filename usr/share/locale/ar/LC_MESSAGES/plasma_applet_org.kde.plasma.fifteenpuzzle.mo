��          �            x  
   y  	   �     �     �  3   �     �     �                    %     *     F  B   Y     �    �     �     �     �     �  ?   	     I  (   [     �     �     �  
   �  >   �          "  "   1     	                                                  
                        Appearance Browse... Choose an image Fifteen Puzzle Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) Number color Path to custom image Piece color Show numerals Shuffle Size Solve by arranging in order Solved! Try again. The time since the puzzle started, in minutes and secondsTime: %1 Use custom image Project-Id-Version: plasma_applet_fifteenPuzzle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-12 03:59+0100
PO-Revision-Date: 2017-02-14 20:06+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 المظهر تصفّح... اختر صورة أحجية الخمسة عشر ملفات الصّور (*.png *.jpg *.jpeg *.bmp *.svg *.svgz) لون العدد مسار الصّورة المخصّصة لون القطعة أظهر الأعداد اخلط الحجم حلّها بترتيب القطع ترتيبًا صحيحًا حُلّت! جرّب أخرى. الوقت: %1 استخدم صورة مخصّصة 