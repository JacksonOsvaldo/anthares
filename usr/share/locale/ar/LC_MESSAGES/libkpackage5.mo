��    =        S   �      8     9     I     Y     i     y     �     �     �  
   �     �  	   �  	   �  )   �  !     G   <  )   �  >   �     �  
             &  7   8  :   p  5   �  	   �     �  $     "   (  &   K     r     {     �     �     �     �     �     �     �     �  
   �     	     #	     4	  2   D	  %   w	  2   �	     �	     �	     �	     �	     
     !
     ;
     W
     p
     �
     �
     �
  	   �
     �
  
  �
     �     �          '     >     T     o     �     �  #   �  
   �     �  6   �  '     y   E  6   �  Q   �  )   H     r     �     �  )   �  +   �  !        <     K      e  .   �  "   �  
   �  '   �          #     C  
   V  
   a  $   l  &   �     �  !   �  9   �     !  &   :  G   a  6   �  ?   �           -      @     a  (   u     �     �     �     �       +         L  
   j     u               :   &   %       2              ,          7   3   ;   
      5   9   .      6          $                          /          8                                   *           1              !   "      =       4   (             -      )             '   +   <          #          0         	          Name : %1       Path : %1     Author : %1     Plugin : %1    Comment : %1 %1 already exists %1 does not exist Accessibility Addon Name Application Launchers Astronomy Built in: Could not copy package to destination: %1 Could not delete package from: %1 Could not load installer for package of type %1. Error reported was: %2 Could not move package to destination: %1 Could not open package file, unsupported archive format: %1 %2 Could not open package file: %1 Data Files Date and Time Development Tools Do not translate <name>Remove the package named <name> Do not translate <name>Show information of package <name> Do not translate <path>Install the package at <path> Education Environment and Weather Error: Installation of %1 failed: %2 Error: Plugin %1 is not installed. Error: Uninstallation of %1 failed: %2 Examples Executable Scripts File System Fun and Games Graphics Images Language List installed packages Main Script File Miscellaneous Multimedia No metadata file in package: %1 No such file: %1 Online Services Package plugin name %1 contains invalid characters Package plugin name not specified: %1 Package types that are installable with this tool: Path Productivity Provided by plugins: Service Type Showing info for package: %1 Successfully installed %1 Successfully uninstalled %1 Successfully upgraded %1 System Information Translations Upgrading package from file: %1 User Interface Utilities Windows and Tasks Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-15 03:10+0100
PO-Revision-Date: 2014-06-28 05:59+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Virtaal 0.7.1
X-Project-Style: kde
       الاسم: %1       المسار: %1     المؤلف: %1     الملحقة: %1    التعليق: %1 %1 موجود بالفعل %1 غير موجود الإتاحة اسم الإضافة مُطلِقات التطبيقات الفلك مدمَجة: تعذّر نسخ الحزمة إلى المقصد: %1 تعذّر حذف الحزمة من: %1 تعذّر تحميل المثبِّت المناسب لنوع الحزم %1. الخطأ المبلّغ عنه كان: %2 تعذّر نقل الحزمة إلى المقصد: %1 تعذّر فتح الملف، هيئة الأرشيف غير مدعومة: %1 %2 تعذّر فتح ملف الحزمة: %1 ملفات البيانات التاريخ والوقت أدوات التطوير أزل الحزمة المسماة <name> أظهر معلومات الحزمة <name> ثبّت الحزمة في <path> التعليم البيئة والطقس خطأ: فشل تثبيت %1: %2 خطأ: الملحقة %1 غير مثبّتة. خطأ: فشلت إزالة %1: %2 أمثلة السكرِبتات التنفيذية نظام الملفات الترفيه والألعاب الرسوميات الصور اللغة اسرد الحزم المثبّتة ملف السكرِبت الرئيسي متنوّع الوسائط المتعدّدة لا ملف بيانات وصفية في الحزمة: %1 لا ملف كهذا: %1 الخدمات على الإنترنت اسم ملحقة الحزمة %1 يحوي محارف غير صالحة اسم ملحقة الحزمة غير محدَّد: %1 الحزم القابلة للتثبيت بهذه الأداة: المسار الإنتاجية توفّرها الملحقات: نوع الخدمة يظهر معلومات الحزمة: %1 ثُبِّتت %1 بنجاح أُزيلت %1 بنجاح رُقِّيت %1 بنجاح معلومات النظام الترجمات يرقّي الحزمة من الملف: %1 واجهة المستخدِم أدوات النوافذ والمهام 