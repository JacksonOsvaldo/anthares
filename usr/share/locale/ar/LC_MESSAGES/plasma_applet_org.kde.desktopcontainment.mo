��    Q      �  m   ,      �     �     �     �     	               "     3     A     I  /   Q  -   �     �  
   �     �     �     �     �     �     �     �  
                   '     ?     R     ^     e     {     �  	   �     �     �     �  	   �     �     �     �     �     �     �     �     �     	     	     !	  <   $	     a	     q	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     !
  )   /
     Y
     q
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     	  E     �  `  	   U     _  !   z  	   �  	   �     �     �     �     �               )     C     S     c     j  
   s     ~     �     �     �     �     �       /   "     R     n     �  .   �     �     �          !  ,   *  
   W     b     |  
   �     �  
   �     �     �     �  %   �  
          
   !  s   ,     �     �     �     �  
   �                           @     T     j      �  ,   �     �  J   �  %   8  2   ^  
   �  
   �     �     �     �     �  
   �       
     ,   !     N  $   \     �    �                I   9       -      D       L      A       N   >                2   #       6       7          K   )         8   H   E   :   *      1   4              M          B       "   J   
      +            P      %                    	       3                 Q   G   '   F           =   ;   <          (   ,           0      C   .       ?   /   &   @          !   $   5           O                     &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Back Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Huge Icon Size Icons Large Left List Location Lock in place Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Restore from trashRestore Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Small Medium Sort By Sorting: Specify a folder: Tiny Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2017-09-01 11:06+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Lokalize 2.0
 ا&حذف أ&فرغ المهملات ا&نقل إلى المهملات ا&فتح ا&لصق &خصائص أ&نعش سطح المكتب أ&نعش العرض أ&عد التحميل أ&عد التسمية اختر... امحُ الأيقونة حاذِ إلى رتّب على عُد ألغِ أعمدة اضبط سطح المكتب عنوان مخصّص التاريخ الافتراضي تنازليًا ألغِ تحديد الكلّ تخطيط سطح المكتب أدخِل العنوان المخصّص هنا نمط اسم الملفّ: أنواع الملفّات: التّرشيح منبثقات معاينة المجلّدات المجلّدات أوّلًا المجلدات أولًا المسار كاملًا فهمت أخفِ الملفّات المتطابقة كبيرة حجم الأيقونات الأيقونات كبيرة اليسار قائمة المكان اقفل في المكان متوسّطة خيارات معاينة أكثر... الاسم بلا حسنًا انقر على الودجات فترة من الزّمن لتحريكها وإظهار مقابض التّحريك عاين الملحقات مصغّرات المعاينة أزل غيّر الحجم استعد اليمين دوّر صفوف ابحث عن نوع ملفّ... حدّد الكلّ اختر مجلدًا علامات التّحديد أظهر كلّ الملفّات أظهر الملفّات المتطابقة أظهر مكانًا: أظهر الملفّات المرتبطة بالنّشاط الحاليّ أظهر مجلد سطح المكتب أظهر صندوق أدوات سطح المكتب الحجم صغيرة متوسّطة الصّغر افرز حسب الفرز: حدّد مجلدًا: صغيرة التّعديلات النوع اطبع مسارًا أو وصلةً هنا بلا فرز التّعامل مع الودجات فُتحت الودجات يمكنك النّقر على الودجات فترة من الزّمن لتحريكها وإظهار مقابض التّحريك.يمكنك النّقر على الودجات فترة من الزّمن لتحريكها وإظهار مقابض التّحريك. 