��    C      4  Y   L      �  
   �  
   �  
   �     �     �     �     �                 	   %     /     ;     B  
   I     T     [  	   a     k     ~     �     �     �     �     �     �     �     �  
   �     �  2        B     J     O     [     d     q     �     �     �     �     �     �     �     �     �     �  	   �     �               #  b   *  �   �     ,	     0	  	   ?	     I	     Z	  
   j	  	   u	     	     �	     �	     �	     �	    �	     �     �     �     �               ,     B     K     g     w     �     �     �     �  
   �     �     �     �     �               1  $   :     _     w     �     �     �  *   �  C        F     X     e          �     �     �     �     �     �               9     M     Z     t     �     �     �     �     �  u   �  �   V     0  +   =     i     �  !   �     �     �     �     �  
   
  (     $   >     %                 /             -   <   >          1            .   +   5   A      ,   B   ;   $   7                                  C      4       =       0       3       ?                  :      6       #                8                    !   &             	   *   2   '   "      (              9          @   )   
    Activities Add Action Add Spacer Add Widgets... Alt Alternative Widgets Always Visible Apply Apply Settings Author: Auto Hide Back-Button Bottom Cancel Categories Center Close Configure Configure activity Create activity... Ctrl Currently being used Delete Email: Forward-Button Get new widgets Height Horizontal-Scroll Input Here Keyboard shortcuts Layout cannot be changed whilst widgets are locked Layout: Left Left-Button License: Lock Widgets Maximize Panel Meta Middle-Button More Settings... Mouse Actions OK Panel Alignment Remove Panel Right Right-Button Screen Edge Search... Shift Stop activity Stopped activities: Switch The settings of the current module have changed. Do you want to apply the changes or discard them? This shortcut will activate the applet: it will give the keyboard focus to it, and if the applet has a popup (such as the start menu), the popup will be open. Top Undo uninstall Uninstall Uninstall widget Vertical-Scroll Visibility Wallpaper Wallpaper Type: Widgets Width Windows Can Cover Windows Go Below Project-Id-Version: KDE 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-09 03:10+0200
PO-Revision-Date: ٢٠١٦-٠٩-٠٧ ١٢:١٢+0300
Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 الأنشطة أضف إجراءً أضف مُباعد أضف ودجات... Alt ودجات بديلة أظهر دائمًا طبّق طبّق الإعدادات المؤلّف: أخف آليًّا زرّ العودة الأسفل ألغ الفئات الوسط أغلق اضبط اضبط النّشاط أنشئ نشاطًا... Ctrl مُستخدم حاليًّا احذف البريد الإلكترونيّ: زرّ التّقدّم اجلب ودجات جديدة الارتفاع التّمرير الأفقيّ أدخل هنا اختصارات لوحة المفاتيح لا يمكن تغير التّخطيط والودجات مقفلة التّخطيط: اليسار الزّرّ الأيسر الرّخصة: اقفل الودجات كبّر اللوحة Meta الزّرّ الأوسط إعدادات أخرى... إجراءات الفأرة حسنًا. محاذاة اللوحة أزل اللوحة اليمين الزّرّ الأيمن حافّة الشّاشة ابحث... Shift أوقف النّشاط الأنشطة الموقفة: بدّل تغيّرت إعدادات الوحدة الحاليّة. أتريد تطبيق الإعدادات أو رفضها؟ سينشّط هذا الاختصار البريمج: سيعطي تركيز لوحة المفاتيح إليه، وإن كان للبريمج مبثقة (كقائمة البدء)، ستُفتح المنبثقة هذه. الأعلى تراجع عن إزالة التّثبيت أزل التّثبيت أزل تثبيت الودجة التّمرير الرّأسيّ الرّؤية الخلفيّة نوع الخلفيّة: الودجات العرض يمكن للنّوافذ تغطيتها النّوافذ تذهب تحتها 