��          �      ,      �     �  /   �     �     �     �     �            I   ,     v  "   z     �  6   �  *   �          )  �  6  	   �  �   �  e   i     �  �   �  <   Y     �  :   �  �   �     �  �   �  u   �  �   
	  �   �	     �
     �
                                                           
      	              to  A non-proportional font (i.e. typewriter font). Ad&just All Fonts... BGR Click to change all fonts Configure Anti-Alias Settings Configure... E&xclude range: Hinting is a process used to enhance the quality of fonts at small sizes. RGB Used by menu bars and popup menus. Used by the window titlebar. Used for normal text (e.g. button labels, list items). Used to display text beside toolbar icons. Vertical BGR Vertical RGB Project-Id-Version: kcmfonts 1.0
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-24 05:54+0100
PO-Revision-Date: 2004-10-28 01:44-0800
Last-Translator: Tamil PC <tamilpc@ambalam.com>
Language-Team: TAMIL <tamilinix@yahoogroups.com>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 வரை ஒரு வீத சமமற்ற எழுத்துரு (உதா. தட்டச்சுப்பொறி எழுத்துவகை). அணைத்து எழுத்துருக்களையும் சரிசெய்... BGR எல்லா எழுத்துருக்களையும் மாற்ற இங்கே அழுத்தவும் Anti-Alias அமைப்புகளை உள்ளமை உள்ளாமை... விலக்கப்படும் வீச்சு ஹின்டிங் என்பது எழுத்துருவ்ச்ங்களின் தரத்தை சிறிய அளவில் மேன்படுத்தும் செயற்படுத்தும் RGB பட்டிப்பட்டைகள் மற்றும் மேல்மீட்புப் பட்டிகளால் பாவிக்கப்படும். சாளரத் தலைப்புப்பட்டையால் பாவிக்கப்படும். இயல்பான உரைக்குப் பாவிக்கப்படும் (உதா. பொத்தான் அடையாளம், பட்டி உருப்படிகள்). கருவிபட்டைக் குறும்படங்களின் அருகிலுள்ள உரையைக்குப் பாவிக்கப்படும். நெடுவரிசை BGR நெடுவரிசை RGB 