��          D      l       �      �   
   	       *      �  K  ^  ,     �     �  Q   �                          <h1>Multiple Desktops</h1>In this module, you can configure how many virtual desktops you want and how these should be labeled. Desktop %1 Desktop %1: Here you can enter the name for desktop %1 Project-Id-Version: kcm_kwindesktop
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-06 05:35+0100
PO-Revision-Date: 2008-03-11 17:46+0530
Last-Translator: ஆமாச்சு <amachu@ubuntu.com>
Language-Team: தமிழ் <https://lists.ubuntu.com/mailman/listinfo/ubuntu-l10n-tam>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: KBabel 1.11.4
 <h1>பலத் திரைகள்</h1>இப்பாகத்தில், தாங்கள் எவ்வளவு நிகர்தி்திரைகள் வேண்டுமென்பதனையும் இவை பெயரிடப்படும் விதத்தையும் வடிவமைக்கலாம். திரை %1 திரை %1: %1 திரைக்கான பெயரை இங்கே இடலாம் 