��    2      �  C   <      H     I     O     d     q     �     �  !   �  '   �     �       -        M     R     h     o     x     �     �     �     �      �     �  	   �     �            
   )     4     S     e  A   ~     �     �     �     �     �                  3   !     U  �   d     
               &     4  %   @  b   f    �     �
     �
     �
     
     (      H     i     �     �     �  *   �     �                     2     O     e     t     �  1   �  	   �     �     �     �      �       )   .     X  "   o  ;   �     �  	   �     �     �     �          "  	   @  5   J     �  �   �  
   <  
   G     R     j       (   �    �           	   .   /      2               +              
   *                                  !   1      %          #   (   "                            )         '         -                             $              &         ,   0       %1 Hz &Disable All Outputs &Reconfigure (c), 2012-2013 Daniel Vrátil 90° Clockwise 90° Counterclockwise @title:windowDisable All Outputs @title:windowUnsupported Configuration Active profile Advanced Settings Are you sure you want to disable all outputs? Auto Break Unified Outputs Button Checkbox Combobox Configuration for displays Daniel Vrátil Display Configuration Display: EMAIL OF TRANSLATORSYour emails Enabled Group Box Identify outputs KCM Test App Laptop Screen Maintainer NAME OF TRANSLATORSYour names No Primary Output No available resolutions No kscreen backend found. Please check your kscreen installation. Normal Orientation: Primary display: Radio button Refresh rate: Resolution: Scale Display Scale: Scaling changes will come into effect after restart Screen Scaling Sorry, your configuration could not be applied.

Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU. Tab 1 Tab 2 Unified Outputs Unify Outputs Upside Down Warning: There are no active outputs! Your system only supports up to %1 active screen Your system only supports up to %1 active screens Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-24 03:18+0200
PO-Revision-Date: 2017-05-13 13:50+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1 Hz &Išjungti visas išvestis Perkonfigūruoti (c), 2012-2013 Daniel Vrátil 90° pagal laikrodžio rodyklę 90° prieš laikrodžio rodyklę Išjungti visas išvestis Nepalaikoma konfigūracija Veikiamasis profilis Sudėtingesnės nuostatos Tikrai norite uždrausti visus išvedimus? Automatinis Atsieti išvedimus Mygtukas Žymimas langelis Išskleidžiamasis sąrašas Ekrano konfigūracija Daniel Vrátil Ekrano konfigūracija Rodyti: liudas@akmc.lt, opensuse.lietuviu.kalba@gmail.com Įjungtas Grupė Identifikuoti išvedinius KCM bandymas Nešiojamojo kompiuterio ekranas Prižiūrėtojas Liudas Ališauskas, Mindaugas Baranauskas Pagrindinis išvedimas Ekrano skiriamoji geba neprieinama Nerasta kscreen sąsaja. Patikrinkite, ar įdiegta kscreen. Normali Padėtis: Pagrindinis ekranas: Akutė Atnaujinimo dažnis: Skiriamoji geba: Keisti vaizdo ekrane mastelį Mastelis: Mastelio pakeitimai įsigalios tik iš naujo paleidus Ekrano mastelis Atleiskite, konfigūracijos pritaikyti neįmanoma.

Galbūt parinkote per didelę ekrano raišką arba įgalinote daugiau ekranų negu kad palaiko jūsų vaizdo plokštė. 1 kortelė 2 kortelė Suvienodinti išvedimus Suvienyti išvedimus Aukštyn-kojom Įspėjimas: nėra veikiamojo išvedimo! Jūsų sistemoje gali veikti ne daugiau kaip %1 veikiamasis ekranas Jūsų sistemoje gali veikti ne daugiau kaip %1 veikiamieji ekranai Jūsų sistemoje gali veikti ne daugiau kaip %1 veikiamųjų ekranų Jūsų sistemoje gali veikti ne daugiau kaip %1 veikiamasis ekranas 