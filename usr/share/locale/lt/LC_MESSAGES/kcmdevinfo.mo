��    \      �     �      �  !   �     �  	          S   $  	   x     �     �     �     �     �     �     �     �     �     	  J   	     _	     g	      s	  	   �	  
   �	     �	     �	     �	     �	     �	     �	     �	  L   
  	   N
  	   X
  
   b
  
   m
  
   x
     �
     �
     �
     �
     �
     �
     �
     �
     �
     
  	             )     5     =     K     O     _     g     t  
   �  	   �     �  
   �     �     �     �     �     �     �     �          !     >     T     Z     ^     b  H   i     �     �     �     �     �     �  
   �  &   �     #  "   6     Y     w     �     �     �     �  	        
  0   #     T  	   j     t     }     �     �     �     �     �     �     �            
   1     <  &   X          �  "   �     �     �     �     �               "     8     <  .   E  	   t  	   ~  
   �  
   �  
   �     �     �     �     �     �                   $   A     f     i     y     �  
   �     �     �     �     �     �     �                          .     3     8     K     d     �  	   �     �  "   �     �     �     �     �       E   	     O     d     q     }     �     �  
   �  
   �     �     �  	   �  
   �  
   �  
   �  
   �  
   �     �           A   !       '       "   #   $            Z   ?      K   L   &   5       =   S          ;   8   F   %           @      -   7   Q       6       I             >   3                   G   ,                      E          4   (   M   C       +       J             2       T   <   
       :       N   9           *               Y   .      U   X              P   V      D       0       W             \       /      O         [   H          	          B       R   1       )    
Solid Based Device Viewer Module (c) 2010 David Hubner AMD 3DNow ATI IVEC Available space out of total partition size (percent used)%1 free of %2 (%3% used) Batteries Battery Type:  Bus:  Camera Cameras Charge Status:  Charging Collapse All Compact Flash Reader Default device tooltipA Device Device Information Device Listing Whats ThisShows all the devices that are currently listed. Devices Discharging EMAIL OF TRANSLATORSYour emails Encrypted Expand All File System File System Type:  Fully Charged Hard Disk Drive Hotpluggable? IDE IEEE1394 Info Panel Whats ThisShows information about the currently selected device. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Keyboard Keyboard + Mouse Label:  Max Speed:  Memory Stick Reader Mounted At:  Mouse Multimedia Players NAME OF TRANSLATORSYour names No No Charge No data available Not Mounted Not Set Optical Drive PDA Partition Table Primary Processor %1 Processor Number:  Processors Product:  Raid Removable? SATA SCSI SD/MMC Reader Show All Devices Show Relevant Devices Smart Media Reader Storage Drives Supported Drivers:  Supported Instruction Sets:  Supported Protocols:  UDI:  UPS USB UUID:  Udi Whats ThisShows the current device's UDI (Unique Device Identifier) Unknown Drive Unused Vendor:  Volume Space: Volume Usage:  Yes kcmdevinfo name of something is not knownUnknown no device UDINone no instruction set extensionsNone platform storage busPlatform unknown battery typeUnknown unknown deviceUnknown unknown device typeUnknown unknown storage busUnknown unknown volume usageUnknown xD Reader Project-Id-Version: kcmdevinfo
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-23 03:11+0200
PO-Revision-Date: 2017-09-04 09:58+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 
Įrenginių žiūryklės modulis paremtas Solid (c) 2010 David Hubner AMD 3DNow ATI IVEC %1 laisva iš %2 (%3% užimta) Akumuliatoriai Akumuliatoriaus tipas:  Magistralė:  Fotoaparatas Fotoaparatai Įkrovos būsena:  Kraunama Visus sutraukti Compact flash skaitytuvas Įrenginys Informacija apie įrenginį Rodo visus dabar rodomus įrenginius.  Įrenginiai Išsikrauna andrius@stikonas.eu,liudas@akmc.lt Užšifruotas Visus išskleisti Failų sistema Failų sistemos tipas:  Pilnai pakrauta Standusis diskas Greitai prijungiamas? IDE IEEE1394 Rodo informaciją apie pažymėtą įrenginį. Intel MMX Intel SSE Intel SSE2 Intel SSE3 Intel SSE4 Klaviatūra Klaviatūra + Pelė Žymė:  Maksimalus greitis:  Atminties kortelės skaitytuvas Prijungimo vieta:  Pelė Įvairialypės terpės grotuvės Andrius Štikonas,Liudas Ališauskas Ne Nėra įkrovimo Duomenys neprieinami Neprijungta Nenurodyta Optinis įrenginys PDA Skaidinių lentelė Pagrindinis Procesorius %1 Procesorių skaičius:  Procesoriai Produktas:  Raid Pašalinamas? SATA SCSI SD/MMC skaitytuvas Rodyti visus įrenginius Rodyti aktualius įrenginius Smart media skaitytuvas Laikmenos Palaikomos tvarkyklės:  Palaikomi instrukcijų rinkiniai:  Palaikomi protokolai UDI:  UPS USB UUID:  Rodo dabartinio įrenginio UDI (Unikalų įrenginio identifikatorių) Nežinomas kaupiklis Nenaudojamas Tiekėjas:  Talpa: Talpos naudojimas:  Taip kcmdevinfo Nežinomas Joks Nėra Platforma Nežinomas Nežinomas Nežinomas Nežinomas Nežinomas xD skaitytuvas 