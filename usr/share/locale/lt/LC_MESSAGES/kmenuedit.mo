��    >        S   �      H  	   I  	   S     ]     e     s     y          �     �     �     �     �     �     �  
        "  ?   .     n  >   w  	   �     �     �  S   �      A     b  �  z     	     
	     	  
   '	     2	     B	     R	  
   _	     j	  A   y	     �	     �	  
   �	     �	     �	     
     #
     ,
     ;
     G
     X
     h
     |
     �
     �
     �
     �
     �
     �
               (  T   ;     �  S   �    �     
          %     1     >     F     N     \  !   p  	   �      �  !   �  (   �  )        2     E  ?   W     �  5   �  	   �     �     �  O     8   h  *   �  �  �     �     �     �     �     �     �     �            A   +  *   m     �     �     �  4   �                &     8     H     a     s     �     �     �     �     �            %   9     _     u  X   �     �  N   �            3          1   6       2   4      !   *         #   7                    (   9          &           :   5          +                    "                        '           -          >   =   /   	      )                ;   
      .      $   %       <   ,                    8      0            [Hidden] &Comment: &Delete &Description: &Edit &File &Name: &New Submenu... &Run as a different user &Sort &Sort all by Description &Sort all by Name &Sort selection by Description &Sort selection by Name &Username: &Work path: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Advanced All submenus of '%1' will be removed. Do you want to continue? Co&mmand: Could not write to %1 Current shortcut &key: Do you want to restore the system menu? Warning: This will remove all custom menus. EMAIL OF TRANSLATORSYour emails Enable &launch feedback Following the command, you can have several place holders which will be replaced with the actual values when the actual program is run:
%f - a single file name
%F - a list of files; use for applications that can open several local files at once
%u - a single URL
%U - a list of URLs
%d - the folder of the file to open
%D - a list of folders
%i - the icon
%m - the mini-icon
%c - the caption General General options Hidden entry Item name: KDE Menu Editor KDE menu editor Main Toolbar Maintainer Matthias Elter Menu changes could not be saved because of the following problem: Menu entry to pre-select Montel Laurent Move &Down Move &Up NAME OF TRANSLATORSYour names New &Item... New Item New S&eparator New Submenu Only show in KDE Original Author Previous Maintainer Raffaele Sandrini Restore to System Menu Run in term&inal Save Menu Changes? Show hidden entries Spell Checking Spell checking Options Sub menu to pre-select Submenu name: Terminal &options: Unable to contact khotkeys. Your changes are saved, but they could not be activated. Waldo Bastian You have made changes to the menu.
Do you want to save the changes or discard them? Project-Id-Version: kmenuedit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2017-06-25 01:21+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
  [Paslėptas] &Komentaras: Paša&linti &Aprašymas: &Keisti &Failas &Pavadinimas: &Naujas submeniu... Vykdyti &kito naudotojo teisėmis &Rikiuoti &Rikiuoti visus pagal aprašymą &Rikiuoti visus pagal pavadinimą &Rikiuoti pažymėjimą pagal aprašymą &Rikiuoti pažymėjimą pagal pavadinimą &Naudotojo vardas: &Darbinis kelias: (C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter Sudėtingiau Bus pašalinti visi „%1“ submeniu. Norite tęsti? Ko&manda: Nepavyksta rašyti į %1 Esamas &klavišų derinys: Ar atstatyti sistemos meniu? Perspėjimas: bus pašalinti visi naudotojo meniu. rch@richard.eu.org, dgvirtual@akl.lt, skroblas@erdves.lt Įgalinti &paleidimo grįžtamąjį ryšį Po komandos galite nurodyti keletą parametrų įrašydami simbolių derinius, kurie, vykdant komandą, bus pakeisti tikromis reikšmėmis:
%f - vieno failo pavadinimas
%F - failų sąrašas; naudoti su programomis, galinčiomis iš karto atverti keletą vietinių failų
%u - vienas URL
%U - URL sąrašas
%d - aplankas, kuriame yra atvertinas failas
%D - aplankų sąrašas
%i - paveikslėlis
%m - mini-paveikslėlis
%c - lango antraštė Bendra Bendros parinktys Paslėptas įrašas Elemento pavadinimas: KDE meniu redaktorius KDE meniu redaktorius Pagrindinė įrankinė Prižiūrėtojas Matthias Elter Meniu pakeitimai negalėjo būti išsaugoti dėl šios problemos: Meniu įrašas išankstiniam pažymėjimui Montel Laurent Perkelti ž&emyn Perkelti a&ukštyn Ričardas Čepas, Donatas Glodenis, Valdas Jankūnas Naujas &elementas... Naujas elementas Nauja&s skyriklis Naujas submeniu Rodyti tik KDE aplinkoje Pradinis autorius Ankstesnis prižiūrėtojas Raffaele Sandrini Atstatyti į sistemos meniu Vykdyti term&inale Įrašyti meniu pakeitimus? Rodyti paslėptus įrašus Rašybos tikrinimas Rašybos tikrinimo nustatymai Sub meniu išankstiniam pažymėjimui Submeniu pavadinimas: Terminalo &nustatymai: Nepavyko susisiekti su khotkeys. Jūsų pakeitimai įrašyti, bet nepavyko jų įjungti. Waldo Bastian Jūs padarėte pakeitimų šiame meniu.
Ar norite juos išsaugoti, ar atmesti? 