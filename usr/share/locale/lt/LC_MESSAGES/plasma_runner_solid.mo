��          �      L      �  m   �     /  "   <  F   _  F   �  F   �  J   4  N     R   �     !  %   2  $   X  #   }  $   �  %   �  &   �  �        �    �     �     �  3   �  J     I   \  P   �  K   �  ]   C	  X   �	     �	  
   
     
  
   !
  	   ,
  	   6
     @
     I
     _
                                
                          	                                          Close the encrypted container; partitions inside will disappear as they had been unpluggedLock the container Eject medium Finds devices whose name match :q: Lists all devices and allows them to be mounted, unmounted or ejected. Lists all devices which can be ejected, and allows them to be ejected. Lists all devices which can be mounted, and allows them to be mounted. Lists all devices which can be unmounted, and allows them to be unmounted. Lists all encrypted devices which can be locked, and allows them to be locked. Lists all encrypted devices which can be unlocked, and allows them to be unlocked. Mount the device Note this is a KRunner keyworddevice Note this is a KRunner keywordeject Note this is a KRunner keywordlock Note this is a KRunner keywordmount Note this is a KRunner keywordunlock Note this is a KRunner keywordunmount Unlock the encrypted container; will ask for a password; partitions inside will appear as they had been plugged inUnlock the container Unmount the device Project-Id-Version: plasma_runner_solid
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2012-01-15 10:46+0200
Last-Translator: Donatas G. <dgvirtual@akl.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.2
 Užrakinti konteinerį Išimti laikmeną Rasti įrenginius, kurių pavadinimas atitinka :q:. Randa visus įrenginius ir leidžia juos prijungti, atjungti arba išimti. Rodo visus įrenginius, kuriuos galima išimti, ir leidžia juos išimti. Parodo visus įrenginius, kruriuos galima prijungti, ir leidžia juos prijungti. Rodo visus įrenginius, kuriuos galima atjungti, ir leidžia juos atjungti. Rodo visus užšifruotus įrenginius, kuriuos galima užrakinti, ir leidžia juos užrakinti. Rodo visus šifruotus įrenginius, kuriuos galima atrakinti, ir leidžia juos atrakinti. Prijungti laikmeną įrenginys išimti užrakinti prijungti atrakinti atjungti Atrakinti konteinerį Atjungti laikmeną 