��          �      |      �     �     �     �                 ;  	   \     f     �  &   �     �     �     �  	          �   %  �   �  t   ?  7   �  +   �              2     7     I     P     l     �     �     �     �  1   �  2     -   C  >   q  	   �     �  v   �  �   C	  g   �	     8
  :   H
     �
                                      
   	                                                         min Act like Always Define a special behavior Don't use special settings EMAIL OF TRANSLATORSYour emails Hibernate NAME OF TRANSLATORSYour names Never shutdown the screen Never suspend or shutdown the computer PC running on AC power PC running on battery power PC running on low battery Shut down Suspend The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" The activity service is not running.
It is necessary to have the activity manager running to configure activity-specific power management behavior. The activity service is running with bare functionalities.
Names and icons of the activities might not be available. This is meant to be: Act like activity %1Activity "%1" Use separate settings (advanced users only) after Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2016-11-21 18:18+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
  min Tegul veikia kaip Visada Nustatyti ypatingą elgesį Nenaudoti ypatingų nuostatų dgvirtual@akl.lt Sustabdyti į diską Donatas Glodenis Niekuomet neišjungti ekrano Niekuomet neužmigdyti ir neišjungti kompiuterio Kompiuteris veikia naudodamas išorinį maitinimą Kompiuteris veikia maitinamas akumuliatoriaus Kompiuteris veikia maitinamas beveik išsikrovusios  baterijos Išjungti Sustabdyti į RAM Energijos valdymo tarnyba atrodo neveikia.
Ši problema gali būti išspręsta „Paleidimo ir išjungimo“ modulyje. Veiklų tarnyba neveikia.
Jei norite konfigūruoti nuo veiklų priklausančius energijos valdymo nustatymus, veiklų tvarkyklė turi veikti. Veiklų tarnyba veikia ribotu funkcionalumu.
Veiklų pavadinimai ir ženkliukai gali būti neprieinami. Veikla „%1“ Naudoti atskirus nuostatas (tik pažengusiems naudotojams) po 