��          �   %   �      `  "   a     �  !   �  !   �  
   �     �     �  !        0  0   P  8   �  !   �     �     �          /     L     ^     }     �  
   �  
   �  
   �  &   �     �     �    �     �  &     !   >      `  
   �     �  $   �  (   �  (   �  :     F   P  /   �  .   �  ,   �      #	     D	     c	  2   w	     �	     �	     �	     �	     �	  $   �	     
     
                                                         
   	                                                                &Keyboard accelerators visibility: &Top arrow button type: Always Hide Keyboard Accelerators Always Show Keyboard Accelerators Animations Breeze Settings Center tabbar tabs Drag windows from all empty areas Drag windows from titlebar only Drag windows from titlebar, menubar and toolbars Draw a thin line to indicate focus in menus and menubars Draw frame around dockable panels Draw frame around page titles Draw frame around side panels Draw slider tick marks Draw toolbar item separators Enable animations Enable extended resize handles Frames General No Buttons One Button Scrollbars Show Keyboard Accelerators When Needed Two Buttons W&indows' drag mode: Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:42+0200
PO-Revision-Date: 2015-08-25 14:33+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 &Prieigos klavišų matomumas: &Viršutinės rodyklės mygtuko tipas: Visada slėpti prieigos klavišus Visada rodyti prieigos klavišus Animacijos Breeze nustatymai Centruoti kortelių juostos korteles Tempti langus už visų tuščių vietų Tempti langus tik už pavadinimo juostos Tempti langus už pavadinimo juostos, meniu ir įrankinių Piešti ploną liniją, kad pabrėžti fokusą meniu ir meniu juostose Piešti rėmelius aplink prikabinamus skydelius Piešti rėmelius aplink puslapių pavadinimus Piešti rėmelius aplink šoninius skydelius Rodyti slinkiklio padalos žymas Piešti įrankinės skyriklius Įjungti animacijas Įjungti išplėstinio dydžio keitimo rankenėles Kadrai Bendra Be mygtukų Vienas mygtukas Slinkties juostos Rodyti prieigos klavišus kai reikia Du mygtukai Langų tempimo režimas: 