��          �      <      �  
   �     �  >   �            
        $     ,     4     ;  	   G     Q     _     f  2   u     �     �  �  �     �  
   �  F   �       	         *     6     B     K     `  
   u     �     �     �  @   �     �                        
                                                                          	        &Browse... &Search: *.png *.xpm *.svg *.svgz|Icon Files (*.png *.xpm *.svg *.svgz) Actions Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:11+0100
PO-Revision-Date: 2013-06-17 13:36+0300
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Naršyti... &Ieškoti: *.png *.xpm *.svg *.svgz|Ženkliukų failai (*.png *.xpm *.svg *.svgz) Veiksmai Programos Kategorijos Įrenginiai Emblemos Emocijų ženkleliai Ženkliuko šaltinis Mime tipai &Kiti ženkliukai: Vietos Sis&temos ženkliukai: Interaktyviai ieškoti ženkliukų pavadinimų (pvz., aplankas). Parinkti ženkliuką Būsena 