��    ,      |  ;   �      �     �     �  .   �  5        7     >     N     T  	   a     k     �     �     �  1   �     �     �                  '   0     X     [     d  '   k     �     �     �     �  5   �     �          
             $   ,  A   Q  �   �     y     �  C   �  '   �     �         "     ;
     ?
     E
     [
  	   p
     z
     �
     �
     �
  #   �
     �
     �
       w   ,     �     �     �  
   �     �     �                 �   ,     �  
   �     �  	     w        �     �  	   �     �     �     �     �  
   �  	   	          &     :     A     R             #                    '                +   $       &      )                	                                    !   (      "                %                                    
   *       ,        %1% Back Button to change keyboard layoutSwitch layout Button to show/hide virtual keyboardVirtual Keyboard Cancel Caps Lock is on Close Close Search Configure Configure Search Plugins Desktop Session: %1 Different User Keyboard Layout: %1 Logging out in 1 second Logging out in %1 seconds Login Login Failed Login as different user Logout No media playing Nobody logged in on that sessionUnused OK Password Reboot Reboot in 1 second Reboot in %1 seconds Recent Queries Remove Restart Shutdown Shutting down in 1 second Shutting down in %1 seconds Start New Session Suspend Switch Switch Session Switch User Textfield placeholder textSearch... Textfield placeholder text, query specific KRunnerSearch '%1'... This is the first text the user sees while starting in the splash screen, should be translated as something short, is a form that can be seen on a product. Plasma is the project name so shouldn't be translated.Plasma made by KDE Unlock Unlocking failed User logged in on console (X display number)on TTY %1 (Display %2) User logged in on console numberTTY %1 Username Username (location)%1 (%2) Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-09 03:21+0100
PO-Revision-Date: 2017-06-25 01:38+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1% Atgal Pakeisti išdėstymą Virtuali klaviatūra Atšaukti Įjungtos didžiosios raidės Užverti Užverti paiešką Konfigūruoti Konfigūruoti paieškos papildinius Darbalaukio sesija: %1 Kitas naudotojas Klaviatūros išdėstymas: %1 Atsijungiama po %1 sekundės Atsijungiama po %1 sekundžių Atsijungiama po %1 sekundžių Atsijungiama po %1 sekundės Prisijungti Prisijungti nepavyko Prisijungti kitu naudotoju Atsijungti Niekas negroja Nenaudojama Gerai Slaptažodis Paleisti iš naujo Paleidžiama iš naujo po %1 sekundės Paleidžiama iš naujo po %1 sekundžių Paleidžiama iš naujo po %1 sekundžių Paleidžiama iš naujo po %1 sekundės Paskiausios užklausos Pašalinti Paleisti iš naujo Išjungti Išjungiama po vienos sekundės Išjungiama po %1 sekundžių Išjungiama po %1 sekundžių Išjungiama po %1 sekundės Pradėti naują seansą Sustabdyti į RAM Perjungti Perjungti seansą Pakeisti naudotoją Ieškoti... Ieškoti „%1“... KDE Plasma Atrakinti Atrakinti nepavyko TTY %1 (%2 ekranas) TTY %1 Naudotojo vardas %1 (%2) 