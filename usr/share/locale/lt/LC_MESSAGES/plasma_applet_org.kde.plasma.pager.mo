��          �   %   �      0  *   1     \  .   s     �     �     �     �     �     �                 
   $     /     5     =     E     ]     t     �     �     �     �  !  �  V   �  '   Q  V   y     �     �     �          1     I     ]     e     u     |     �     �     �     �      �  %   �          4     J     V        
                             	                                                                                          %1 Minimized Window: %1 Minimized Windows: %1 Window: %1 Windows: ...and %1 other window ...and %1 other windows Activity name Activity number Add Virtual Desktop Configure Desktops... Desktop name Desktop number Display: Does nothing General Horizontal Icons Layout: No text Only the current screen Remove Virtual Desktop Selecting current desktop: Show Activity Manager... Shows desktop The pager layoutDefault Vertical Project-Id-Version: plasma_applet_pager
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-08-19 03:29+0200
PO-Revision-Date: 2016-11-04 19:44+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 %1 sumažintas langas %1 sumažinti langai %1 sumažintų langų %1 sumažintas langas %1 langas %1 langai %1 langų %1 langas ...ir %1 kitas langas ...ir %1 kiti langai ...ir %1 kitų langų ...ir %1 kitas langas Veiklos pavadinimą Veiklos numerį Pridėti virtualų darbalaukį Konfigūruoti darbalaukius... Darbalaukio pavadinimą Darbalaukio numerį Rodyti: Nieko nenutinka Bendri Horizontaliai Ženkliukus Išdėstymas: Nerodyti jokio teksto Tik veikiamojo ekrano Pašalinti virtualų darbalaukį Spragtelėjus dabartinį darbalaukį: Rodyti veiklų tvarkyklę... Parodomas darbalaukis Numatytasis Vertikaliai 