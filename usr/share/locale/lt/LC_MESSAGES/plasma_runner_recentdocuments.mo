��          4      L       `   :   a      �   E  �   E   �  $   ?                    Looks for documents recently used with names matching :q:. Open Containing Folder Project-Id-Version: plasma_runner_recentdocuments
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-12-17 03:48+0100
PO-Revision-Date: 2016-08-30 17:30+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
X-Generator: Lokalize 1.1
 Ieško neseniai naudotų dokumentų, kurių pavadinimas atitinka :q:. Atverti aplanką, kuriame patalpinta 