��    
      l      �       �      �            	               .  I   3     }  	   �    �     �     �  	   �  
   �     �       T        ]     p               
   	                           &Trigger word: Add Item Alias Alias: Character Runner Config Code Creates Characters from :q: if it is a hexadecimal code or defined alias. Delete Item Hex Code: Project-Id-Version: plasma_runner_CharacterRunner
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2012-02-18 19:43+0200
Last-Translator: Donatas G. <dgvirtual@akl.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.4
 &Iššaukiantis žodis: Pridėti įrašą Santrumpa Santrumpa: Character Runner konfigūracija Kodas Sukuria simbolius iš :q: jei :q: yra šešioliktainis kodas ar aprašyta santrumpa. Ištrinti įrašą Šešioliktainis kodas: 