��          �      L      �     �     �     �     �          )     1     9     P     h     t  K   �     �     �     �     �                    #     B     V  "   k     �     �     �     �  %   �     �     	               0     9     R     j     s                                                  
   	                                               Change the barcode type Clear history Clipboard Contents Clipboard history is empty. Clipboard is empty Code 39 Code 93 Configure Clipboard... Creating barcode failed Data Matrix Edit contents Indicator that there are more urls in the clipboard than previews shown+%1 Invoke action QR Code Remove from history Return to Clipboard Search Show barcode Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-10 03:07+0200
PO-Revision-Date: 2015-08-25 10:57+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Keisti brūkšninio kodo tipą Išvalyti istoriją Iškarpinės turinys Iškarpinė istorija yra tuščia. Iškarpinė tuščia Kodas 39 Kodas 93 Konfigūruoti iškarpinę... Brūkšninio kodo sukūrimas nepavyko Duomenų matrica Redaguoti turinį +%1 Vykdyti veiksmą QR kodas Pašalinti iš istorijos Grįžti į iškarpinę Ieškoti Rodyti brūkšninį kodą 