��          |      �             !  *   <     g  @   �  �   �  B   Q  R   �  &   �  *     ,   9  4   f    �     �  -   �     �  6     �   U      "  ,   C  %   p  '   �  +   �     �        	       
                                            Could not eject this disc. Could not mount this device as it is busy. Could not mount this device. One or more files on this device are open within an application. One or more files on this device are opened in application "%2". One or more files on this device are opened in following applications: %2. Remove is less technical for unmountCould not remove this device. Remove is less technical for unmountYou are not authorized to remove this device. This device can now be safely removed. You are not authorized to eject this disc. You are not authorized to mount this device. separator in list of apps blocking device unmount,  Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2017-06-25 00:23+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Nepavyksta išstumti disko. Nepavyksta atjungti tebenaudojamos laikmenos. Nepavyksta prijungti laikmenos. Kažkuri programa naudoja šios laikmenos failą(-us). „%2“ programa naudoja šios laikmenos failą(-us). %2 programos naudoja šios laikmenos failą(-us). %2 programos naudoja šios laikmenos failą(-us). %2 programos naudoja šios laikmenos failą(-us). Nepavyksta pašalinti laikmenos. Neturite leidimo pašalinti šį įrenginį. Jau galite saugiai išimti laikmeną. Neturite leidimo išstumti šį diską. Neturite leidimo prijungti šį įrenginį. ,  