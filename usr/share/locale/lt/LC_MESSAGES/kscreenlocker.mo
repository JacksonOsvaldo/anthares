��          t      �         :        L     Y     g     {     �     �  2   �  @   �    "    $  9   8     r     �     �     �  #   �     �  0   �  <   .  �   k                      
                      	                Ensuring that the screen gets locked before going to sleep Lock Session Screen Locker Screen lock enabled Screen locked Screen saver timeout Screen unlocked Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. The screen locker is broken and unlocking is not possible anymore.
In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),
log in and execute the command:

loginctl unlock-session %1

Afterwards switch back to the running session (Ctrl+Alt+F%2). Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-03 03:06+0200
PO-Revision-Date: 2017-06-25 01:22+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Užtikrina, kad ekranas bus užrakintas prieš užmiegant Užrakinti sesiją Ekrano užraktas Ekrano užraktas įgalintas Ekranas užrakintas Ekrano užsklandos skirtasis laikas Ekranas atrakintas Nustato, po kiek minučių ekranas užrakinamas. Galite nustatyti, ar ekranas užsirakins po tam tikro laiko. Ekrano užraktas sugedo, tad atrakinti nebepavyks.
Norėdami atrakinti, persijunkite į virtualų terminalą (pvz., Vald+Alt+F2),
prisijunkite ir įvykdykite komandą:

loginctl unlock-session %1

Tuomet galėsite grįžti prie seanso (Ctrl+Alt+F%2). 