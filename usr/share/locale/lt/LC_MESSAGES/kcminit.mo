��          L      |       �      �      �   :   �   $        -  �  A     A     a  4   i  /   �     �                                         Configuration module to run KCMInit KCMInit - runs startup initialization for Control Modules. List modules that are run at startup Module %1 not found Project-Id-Version: kcminit
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-15 03:18+0100
PO-Revision-Date: 2009-10-18 17:25+0300
Last-Translator: Donatas Glodenis <dgvirtual@akl.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.0
 Vykdomas konfigūravimo modulis KCMInit KCMInit - startuoja valdymo modulius paleidimo metu. Parodyti modulius, kurie vykdomi paleidimo metu %1 modulio nepavyko rasti 