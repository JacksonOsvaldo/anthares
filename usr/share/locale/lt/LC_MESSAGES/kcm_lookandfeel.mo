��    
      l      �       �      �            )     J     [     h     �     �  :   �    �  "        )     G  '   V     ~     �     �  7   �  J   �               
             	                 Configure Look and Feel details Cursor Settings Changed EMAIL OF TRANSLATORSYour emails Get New Looks... Marco Martin NAME OF TRANSLATORSYour names Show Preview Use Desktop Layout from theme You have to restart KDE for cursor changes to take effect. Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-27 05:46+0100
PO-Revision-Date: 2017-06-25 01:00+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Konfigūruoti išvaizdą išsamiau Žymeklio nustatymai pakeisti liudas@akmc.lt Parsisiųsti naują apipavidalinimą... Marco Martin Liudas Ališauskas Rodyti peržiūrą Naudoti darbalaukio išdėstymą pagal apipavidalinimą Turite perstartuoti KDE jei norite, kad žymeklio pakeitimai įsigaliotų. 