��    )      d  ;   �      �     �     �     �     �     �     �     �  	   �     �          %     :     G     _     v     ~  
   �  
   �     �     �     �     �     �     �     �     �               .  U   C  Z   �  O   �  D   D     �     �     �  	   �  	   �     �     �    �  	   �     �     �     	     	     	     .	  	   E	     O	     h	     �	     �	     �	     �	     �	     �	  
   
  
   
     
     %
     8
     I
     R
     g
     o
     r
     �
     �
     �
  $   �
             d   ;     �  
   �     �     �     �     �     �                                             %                   	         (          &   !          '                          )   #          "                          $          
                            Adapter Add New Device Add New Device... Address Audio Audio device Available devices Bluetooth Bluetooth is Disabled Bluetooth is disabled Bluetooth is offline Browse Files Configure &Bluetooth... Configure Bluetooth... Connect Connected devices Connecting Disconnect Disconnecting Enable Bluetooth File transfer Input Input device Network No No Adapters Available No Devices Found No adapters available No connected devices Notification when the connection failed due to FailedConnection to the device failed Notification when the connection failed due to Failed:HostIsDownThe device is unreachable Notification when the connection failed due to NotReadyThe device is not ready Number of connected devices%1 connected device %1 connected devices Other device Paired Remote Name Send File Send file Trusted Yes Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-13 06:01+0100
PO-Revision-Date: 2015-08-25 10:55+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Adapteris Pridėti naują įrenginį Pridėti naują įrenginį... Adresas Audio Audio įrenginys Pasiekiami įrenginiai Bluetooth Bluetooth yra išjungtas Bluetooth yra išjungtas Bluetooth yra atjungtas Naršyti failus Konfigūruoti &Bluetooth... Konfigūruoti Bluetooth... Prisijungti Prijungti įrenginiai Jungiamasi Atsijungti Atsijungiama Įjungti Bluetooth Failų siuntimas Įvestis Įvesties įrenginys Tinklas Ne Nėra pasiekiamu adapterių Nerasta įrenginių Nėra pasiekiamu adapterių Nėra prijungtų įrenginių Nepavyko prisijungti prie įrenginio Įrenginys yra nepasiekiamas Įrenginys nėra pasiruošęs %1 prijungtas įrenginys %1 prijungti įrenginiai %1 prijungtų įrenginių %1 prijungtas įrenginys Kitas įrenginys Suporuotas Nuotolinio pavadinimas Siųsti failą Siųsti failą Patikimi Taip 