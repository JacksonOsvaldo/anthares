��          �      L      �  &   �  +   �       @     	   ]     g     �     �     �     �  (   �     �     �  ,   �               +     7    ;  *   I  (   t     �     �  
   �     �  
   �  
   �  
   �     �  6   
     A     D  +   c  
   �     �     �     �        
                          	                                                                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Heading for list of actions (leave, lock, shutdown, ...)Actions Hibernate Hibernate (suspend to disk) Leave Leave... Lock Lock the screen Logout, turn off or restart the computer No Sleep (suspend to RAM) Start a parallel session as a different user Suspend Switch User Switch user Yes Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2015-08-25 11:02+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Ar norite užlaikyti atmintyje (užmigti)? Ar norite užlaikyti diske (sustabdyti)? Bendri Veiksmai Sustabdyti Sustabdyti (užlaikyti diske) Išeiti... Išeiti... Užrakinti Užrakinti ekraną Išsiregistruoti, išjungti arba perkrauti kompiuterį Ne Miegoti (užlaikyti atmintyje) Pradėti lygiagretų seansą kitu naudotoju Užlaikyti Pakeisti naudotoją Pakeisti naudotoją Taip 