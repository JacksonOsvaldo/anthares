��          <      \       p   !   q   3   �      �     �   '   �  8        R                   Finds Kate sessions matching :q:. Lists all the Kate editor sessions in your account. Open Kate Session Project-Id-Version: plasma_runner_katesessions
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-03-18 03:08+0100
PO-Revision-Date: 2010-12-29 09:08+0200
Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.1
 Randa Kate sesijas, atitinkančias :q:. Rodo visas jūsų paskyrai priklausančias Kate sesijas. Atverti Kate sesiją 