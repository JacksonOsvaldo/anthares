��    >        S   �      H     I     R     [     j     |     �  �  �  �   y  -   	  )   3	  -   ]	  +   �	  r   �	  	   *
  	   4
     >
     V
     ^
     g
  
   t
     
     �
     �
     �
      �
     �
     �
     �
      �
            r   :  5   �     �     �                -     L  	   Q     [     a     i  (   v     �     �     �     �     �       +     3   9     m     u     �     �  "   �  S   �  )        9  �   E    #  	   :  	   D     N     _     p     �  _  �  �   �  ,   �     �  
   �  	   �  d   �     Q     ]     n     �  	   �     �     �  
   �     �     �  /   �               *     /  +   <     h  !   t  �   �  @        ]  *   p     �     �     �     �  
   �     �  
   �     �  1        H  %   [  &   �  &   �     �     �  6   �  )     	   I     S  
   _     j  !   w  [   �  .   �     $  �   :     )            5                        
   8             2   -          $   %      ;   4              <      1   7                 #   6       &       "       	            3      ,            :   (       9                 =   0   '   /              .       +       *                 >         !        &Amount: &Effect: &Second color: &Semi-transparent &Theme (c) 2000-2003 Geert Jansen <h1>Icons</h1>This module allows you to choose the icons for your desktop.<p>To choose an icon theme, click on its name and apply your choice by pressing the "Apply" button below. If you do not want to apply your choice you can press the "Reset" button to discard your changes.</p><p>By pressing the "Install Theme File..." button you can install your new icon theme by writing its location in the box or browsing to the location. Press the "OK" button to finish the installation.</p><p>The "Remove Theme" button will only be activated if you select a theme that you installed using this module. You are not able to remove globally installed themes here.</p><p>You can also specify effects that should be applied to the icons.</p> <qt>Are you sure you want to remove the <strong>%1</strong> icon theme?<br /><br />This will delete the files installed by this theme.</qt> <qt>Installing <strong>%1</strong> theme</qt> @label The icon rendered as activeActive @label The icon rendered as disabledDisabled @label The icon rendered by defaultDefault A problem occurred during the installation process; however, most of the themes in the archive have been installed Ad&vanced All Icons Antonio Larrosa Jimenez Co&lor: Colorize Confirmation Desaturate Description Desktop Dialogs Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails Effect Parameters Gamma Geert Jansen Get new themes from the Internet Icons Icons Control Panel Module If you already have a theme archive locally, this button will unpack it and make it available for KDE applications Install a theme archive file you already have locally Install from File Installing icon themes... Jonathan Riddell Main Toolbar NAME OF TRANSLATORSYour names Name No Effect Panel Preview Remove Theme Remove the selected theme from your disk Set Effect... Setup Active Icon Effect Setup Default Icon Effect Setup Disabled Icon Effect Size: Small Icons The file is not a valid icon theme archive. This will remove the selected theme from your disk. To Gray To Monochrome Toolbar Torsten Rahn Unable to create a temporary file. Unable to download the icon theme archive;
please check that address %1 is correct. Unable to find the icon theme archive %1. Use of Icon You need to be connected to the Internet to use this action. A dialog will display a list of themes from the http://www.kde.org website. Clicking the Install button associated with a theme will install this theme locally. Project-Id-Version: kcmicons
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-08 06:27+0100
PO-Revision-Date: 2017-06-24 23:58+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 &Apimtis: &Efektas: &Antroji spalva: Pu&siau skaidrus &Apipavidalinimas (c) 2000-2003 Geert Jansen <h1>Ženkliukai</h1>Šiame modulyje galite parinkti ženkliukus savo darbalaukiui.<p>Norėdami pasirinkti ženkliukų apipavidalinimą, spauskite jos pavadinimą ir pritaikykite savo pasirinkimą paspausdami žemiau esantį mygtuką „Pritaikyti“ Jei nenorite pasirinkto apipavidalinimo pritaikyti, spauskite mygtuką „Atstatyti“, ir pakeitimai bus atmesti.</p><p>Paspaudę mygtuką „Įdiegti naują apipavidalinimą“ galėsite įdiegti naują apipavidalinimą įrašydami apipavidalinimo vietą arba naršydami iki jo. Norėdami užbaigti diegimą, spauskite mygtuką „Gerai“.</p><p>Mygtukas „Pašalinti apipavidalinimą“ bus aktyvus tik tuomet, jei apipavidalinimas įdiegtas naudojant šį modulį. Čia negalėsite pašalinti apipavidalinimų, kurie įdiegti globaliai.</p><p>Čia taip pat galite nurodyti ženkliukams taikomus efektus.</p> <qt>Ar tikrai norite pašalinti <strong>%1</strong> ženkliukų apipavidalinimą?<br /><br />Tokiu būdu pašalinsite ir šio apipavidalinimo įdiegtus failus.</qt> <qt>Įdiegiama <strong>%1</strong> tema</qt> Aktyvus Išjungtas Numatytas Įdiegimo proceso metu iškilo problemų, tačiau dauguma apipavidalinimų iš archyvo buvo įdiegti Pa&pildomai Visi ženkliukai Antonio Larrosa Jimenez Spa&lva: Spalvinti Patvirtinimas Mažiau sodrumo Aprašymas Darbalaukis Dialogai Nutempkite arba įrašykite apipavidalinimo URL dgvirtual@akl.lt Efekto parametrai Gama Geert Jansen Gauti naujų apipavidalinimų iš interneto Ženkliukai Ženkliukų valdymo pulto modulis Jei jūs jau turite temos archyvą kompiuteryje, šis mygtukas jį išpakuos ir įgalins kitas KDE programas naudoti apipavidalinimą Įdiegti apipavidalinimą iš archyvo, kurį turite kompiuteryje Įdiegti iš failo Įdiegiami ženkliukų apipavidalinimai... Jonathan Riddell Pagrindinė įrankinė Donatas Glodenis Vardas Be efektų Skydelis Peržiūra Pašalinti apipavidalinimą Pašalinti pažymėtą apipavidalinimą iš disko Rinktis efektą... Nustatyti aktyvaus ženkliuko efektą Nustatyti numatytą ženkliuko efektą Nustatyti išjungto ženkliuko efektą Dydis: Maži ženkliukai Ši failas nėra teisingas ženkliukų temos archyvas. Pažymėta tema bus pašalinta iš disko. Neryškus Vienspalvis Įrankinė Torsten Rahn Nepavyksta sukurti laikino failo. Nepavyksta atsisiųsti ženkliukų temos archyvo;
patikrinkite ar adresas %1 yra teisingas. Nepavyksta rasti ženkliukų temos archyvo %1. Ženkliuko naudojimas Jūs turite būti prisijungę prie interneto norėdami naudotis šia funkcija. Dialogas parodys apipavidalinimus iš http://www.kde.org. Paspaude mygtuką įdiegti jūs įdiegsite apipavidalinimą į kompiuterį 