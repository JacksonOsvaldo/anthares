��    
      l      �       �   $   �   %     :   <     w  '   �  -   �     �       '       <  "   M  (   p  .   �  !   �  $   �  #        3     G  #   X         	                            
        Could not detect the file's mimetype Could not find all required functions Could not find the provider with the specified destination Error trying to execute script Invalid path for the requested provider It was not possible to read the selected file Service was not available Unknown Error You must specify a URL for this service Project-Id-Version: plasma_engine_share
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2011-01-25 22:54+0200
Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.1
 Nepavyko nustatyti failo mime tipo Nepavyko rasti visų reikiamų funkcijų Nepavyko rasti tiekėjo su nurodyta paskirtimi Klaida bandant vykdyti scenarijų Neteisingas prašomo tiekėjo kelias Nepavyko perskaityti parinkto failo Paslauga neteikiama Nežinoma klaida Turite nurodyti šios paslaugos URL 