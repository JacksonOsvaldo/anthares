��          �      L      �     �     �  �   �  T   }  )   �  (   �  0   %  $   V  &   {  &   �  %   �  ?   �  F   /     v     �     �     �     �  "  �            �   8  \   �     .  
   A     L  
   ^  
   i  	   t     ~     �     �     �     �     �     �     	                                              
                    	                                  Dim screen by half Dim screen totally Lists screen brightness options or sets it to the brightness defined by :q:; e.g. screen brightness 50 would dim the screen to 50% maximum brightness Lists system suspend (e.g. sleep, hibernate) options and allows them to be activated Note this is a KRunner keyworddim screen Note this is a KRunner keywordhibernate Note this is a KRunner keywordscreen brightness Note this is a KRunner keywordsleep Note this is a KRunner keywordsuspend Note this is a KRunner keywordto disk Note this is a KRunner keywordto ram Note this is a KRunner keyword; %1 is a parameterdim screen %1 Note this is a KRunner keyword; %1 is a parameterscreen brightness %1 Set Brightness to %1 Suspend to Disk Suspend to RAM Suspends the system to RAM Suspends the system to disk Project-Id-Version: plasma_runner_powerdevil
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-09-26 03:33+0200
PO-Revision-Date: 2011-08-06 12:33+0300
Last-Translator: Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.2
 Pusiau užtemdyti ekraną Pilnai užtemdyti ekraną Išvardija ekrano ryškumo parinktis arba nustato jo ryškumą, nustatytą :q:; pvz., ekrano ryškumas 50 pritemdys ekraną iki 50% maksimalaus ryškumo Išvardija sistemos pristabdymo (užmigdymo, sustabdymo) parinktis ir leidžia jas aktyvuoti užtemdyti ekraną Sustabdyti ekrano šviesumas Užmigdyti sustabdyti į diską į ram užtemdyti ekraną %1 ekrano šviesumas %1 Nustatyti šviesumą į %1 Sustabdyti į diską Sustabdyti į RAM Sustabdo sistemą į RAM Sustabdo sistemą į diską 