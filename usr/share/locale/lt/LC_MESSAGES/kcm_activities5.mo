��    *      l  ;   �      �     �     �  )   �               /     H     ^  #   ~     �  
   �     �     �  %   �  +        E     Z     m  @   z     �     �     �     �               '     ,     9     ?     _     e  .   m     �  F   �  (   �  	   '  	   1  	   ;  '   E  7   m  "   �    �     �	     �	      �	  
   
  	   "
  	   ,
     6
     >
     P
     j
     }
     �
     �
  %   �
  I   �
  &   $     K  	   ^  K   h     �      �     �  "   �  &        A     H     T     n     {     �  
   �  *   �     �  X   �  1   J     |     �     �     �  )   �     �                   $                                 %                      *                                               	   #   )   "             &           
                        '             (   !    &Do not remember @actionWalk through activities @actionWalk through activities (Reverse) @action:buttonApply @action:buttonCancel @action:buttonChange... @action:buttonCreate @title:windowActivity Settings @title:windowCreate a New Activity @title:windowDelete Activity Activities Activity information Activity switching Are you sure you want to delete '%1'? Blacklist all applications not on this list Clear recent history Create activity... Description: Error loading the QML files. Check your installation.
Missing %1 For a&ll applications Forget a day Forget everything Forget the last hour Forget the last two hours General Icon Keep history Name: O&nly for specific applications Other Privacy Private - do not track usage for this activity Remember opened documents: Remember the current virtual desktop for each activity (needs restart) Shortcut for switching to this activity: Shortcuts Switching Wallpaper for in 'keep history for 5 months'for  unit of time. months to keep the history month  months unlimited number of monthsforever Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2016-11-23 20:49+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 &Neprisiminti Eiti per veiklas Eiti per veiklas (atvirkščiai) Pritaikyti Atšaukti Keisti... Sukurti Veiklos nuostatos Sukurti naują veiklą... Pašalinti veiklą Veiklos Veiklos informacija Persijungimas tarp veiklų Ar tikrai norite pašalinti „%1“? Jei programos nėra šiame sąraše, jas įtraukti į juodąjį sąrašą Išvalyti naujausius žurnalo įrašus Sukurti veiklą... Aprašas: Klaida įkeliant QML. Patikrinkite sistemoje įdiegtus paketus.
Trūksta %1 &Visų programų Pamiršti apie paskutinę dieną Viską pamiršti Pamiršti apie paskutinę valandą Pamiršti apie paskutines dvi valandas Bendra Ženkliukas Žurnale įrašus laikyti Pavadinimas: &Tik nurodytoms programoms Kita Privatumas Privatus - nesekti šios veiklos naudojimo Prisiminti atvertus dokumentus: Įsiminti paskirų veiklų paskiausiai naudotus darbalaukius (reikia paleisti iš naujo) Klavišų derinys persijungimui į šią veiklą: Klavišų deriniai Perjungimas Darbalaukio fonas    mėnesį  mėnesius  mėnesių  mėnesį amžinai 