��             +         �     �  $   �     �     �  
             $     8     H     O     c     {     �  &   �      �  	   �  �   �  �   �  �   #  �   �     I     N     ]     p     �     �     �     �     �  S   �  G   L    �  
   �	  $   �	     �	     �	     �	     �	     
     (
     =
      J
     k
     �
     �
  (   �
     �
  	   �
  �   �
  �   �  �   s  �   ,     �     �     �     �       !         B     _  
   y  d   �  Y   �                                                                                                          	                               
       &Folder: &Microsoft® Windows® network drive &Name: &Port: &Protocol: &Recent connection: &Secure shell (ssh) &Use encryption &User: &WebFolder (webdav) (c) 2004 George Staikos Add Network Folder C&onnect Cr&eate an icon for this remote folder EMAIL OF TRANSLATORSYour emails Encoding: Enter a name for this <i>File Transfer Protocol connection</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Microsoft Windows network drive</i> as well as a server address and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>Secure shell connection</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. Enter a name for this <i>WebFolder</i> as well as a server address, port and folder path to use and press the <b>Save & Connect</b> button. FT&P George Staikos KDE Network Wizard NAME OF TRANSLATORSYour names Network Folder Information Network Folder Wizard Primary author and maintainer Save && C&onnect Se&rver: Select the type of network folder you wish to connect to and press the Next button. Unable to connect to server.  Please check your settings and try again. Project-Id-Version: knetattach
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-10 05:48+0100
PO-Revision-Date: 2015-12-29 20:05+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Aplankas: &Microsoft® Windows® tinklo diskas &Vardas: &Prievadas: &Protokolas: &Nesenas prisijungimas: &Saugus apvalkalas (ssh) &Naudoti šifravimą &Naudotojas: Ži&niatinklio aplankas (webdav) (c) 2004 George Staikos Pridėti tinklo aplanką P&risijungti Sukur&ti ženkliuką nutolusiam aplankui dgvirtual@akl.lt Koduotė: Įrašykite pavadinimą šiam <i>Failų perdavimo protokolo (FTP) prisijungimui</i> bei serverio adresą ir naudotiną aplanko kelią ir spauskite spauskite mygtuką <b>„Įrašyti ir prisijungti“</b>. Įrašykite vardą šiam <i>Microsoft Windows tinklo diskui</i> bei serverio adresą ir naudotiną aplanko kelią ir spauskite spauskite mygtuką <b>„Įrašyti ir prisijungti“</b>. Įrašykite vardą šiam <i>Saugaus apvalkalo prisijungimui</i> bei serverio adresą, prievadą ir aplanko kelią ir spauskite spauskite mygtuką <b>„Įrašyti ir prisijungti“</b>. Įrašykite šio <i>Tinklo aplanko</i> pavadinimą bei serverio adresą, prievadą ir aplanko kelią ir spauskite mygtuką <b>„Įrašyti ir prisijungti“</b>. FT&P George Staikos KDE tinklo vedlys Donatas Glodenis Tinklo aplanko informacija Tinklo aplankų prijungimo vedlys Autorius ir prižiūrėtojas Įrašyti ir &prisijungti Se&rveris: Pasirinkite tinklo aplanko, prie kurio norite prisijungti, tipą, ir spauskite mygtuką „Toliau“ Nepavyksta prisijungti prie serverio. Prašome patikrinti nuostatas ir bandyti iš naujo. 