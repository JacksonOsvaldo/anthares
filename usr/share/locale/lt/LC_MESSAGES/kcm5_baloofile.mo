��          �      �       H     I     a  #   w      �      �     �  J   �     ;  &   Y     �     �  *   �     �    �  "   �       ,   >     k     �     �  C   �     �  0        B     U  +   e     �               
              	                                    Also index file content Configure File Search Copyright 2007-2010 Sebastian Trüg Do not search in these locations EMAIL OF TRANSLATORSYour emails Enable File Search File Search helps you quickly locate all your files based on their content Folder %1 is already excluded Folder's parent %1 is already excluded NAME OF TRANSLATORSYour names Sebastian Trüg Select the folder which should be excluded Vishesh Handa Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-06-25 01:08+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Taip pat indeksuoti failų turinį Konfigūruoti failų paiešką Autorinės teisės 2007-2010 Sebastian Trüg Neieškoti šiose vietose liudas@akmc.lt Įjungti failų paiešką Failo paieška leidžia Jums greitai rasti failus pagal jų turinį Aplankas %1 jau yra išskirtas Aplanko tėvinis elementas %1 jau yra išskirtas Liudas Ališauskas Sebastian Trüg Nurodykite aplanką, kurį reikia išskirti Vishesh Handa 