��    L      |  e   �      p     q     y     �     �     �     �     �     �     �     �  /   �  -        ?  
   E     P     W     _     q     ~     �  
   �     �     �     �     �     �     �     �            	   "     ,     3  	   G     Q     W     ]     b     k     y     �     �     �     �     �  <   �     �     �     	     	     	      	     '	     ,	  
   @	     K	     Y	     k	     z	     �	  )   �	     �	     �	     �	     �	     
     

     
     %
     ,
     1
     K
     T
     d
  E   u
    �
     �     �     �          %  	   .     8     P     d     w     �     �  	   �     �  	   �  
   �     �     �     �  	                  .  "   G     j     �     �  '   �     �     �     �     �     �          *     6     =     F     L     _  	   k      u     �     �     �  T   �            
   5     @  
   M     X     ^     g     ~     �     �     �     �     �  *   �     '  (   C     l     r     x     �     �     �     �     �  
   �     �     �  U   	     G   -       3             :             F   8      1   4           (      C   7                   <   )      +          9   =          ,   /   5   $   J         %           H   ?                             '   >   *       #         I          B       &   
   !      0      K   "   A       .      6                  L   	   E                 2   ;          @      D        &Delete &Empty Trash Bin &Move to Trash &Open &Paste &Properties &Refresh Desktop &Refresh View &Reload &Rename @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Align Arrange In Cancel Columns Configure Desktop Custom title Date Default Descending Deselect All Desktop Layout Enter custom title here File name pattern: File types: Filter Folder preview popups Folders First Folders first Full path Got it Hide Files Matching Icon Size Icons Large Left Location Lock in place Locked Medium More Preview Options... Name None OK Press and hold widgets to move them and reveal their handles Preview Plugins Preview thumbnails Remove Resize Right Rotate Rows Search file type... Select All Select Folder Selection markers Show All Files Show Files Matching Show a place: Show files linked to the current activity Show the Desktop folder Show the desktop toolbox Size Small Sort By Sorting: Specify a folder: Tweaks Type Type a path or a URL here Unsorted Widget Handling Widgets unlocked You can press and hold widgets to move them and reveal their handles. Project-Id-Version: plasma_applet_folderview
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:11+0100
PO-Revision-Date: 2015-12-29 23:01+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: lt <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Trinti &Ištuštinti šiukšlinę Perkelti į šiu&kšliadėžę At&verti &Padėti &Savybės &Atnaujinti darbalaukį &Atnaujinti vaizdą Įkelti iš &naujo &Pervadinti Pasirinkti... Atkurti ženkliuką Lygiuotė Išdėstyti Atšaukti Stulpeliai Konfigūruoti darbalaukį Savitas pavadinimas Data Numatytas Mažėjančiai Atžymėti viską Darbalaukio išdėstymas Įveskite savitą pavadinimą čia Failų pavadinimų šablonai: Failų tipai Filtras Aplanko peržiūros iššokantys langai Pirma aplankai Pirma aplankai Pilnas kelias Supratau Slėpti failus atitinkančius Ženkliukų dydis Ženkliukai Dideli Kairėje Vieta Užrakinti vietoje Užrakintas Vidutinis Daugiau peržiūros parinkčių: Pavadinimas Nieko Gerai Paspauskite ir palaikykite valdiklius, kad perkelti juos ir pamatyti jų rankenėles Peržiūros papildiniai: Peržiūros miniatiūros Pašalinti Keisti dydį Dešinėje Sukti Eilutės Ieškoti failo tipo... Pažymėti viską Pasirinkite aplanką Pasirinkimo žymekliai Rodyti visus failus Rodyti failus atitinkančius Rodyti vietą: Rodyti failus susietus su dabartine veikla Rodyti darbalaukio aplanką Rodyti darbalaukio priemonių komplektą Dydis Maži Rikiuoti pagal Rikiavimas: Rodyti aplanką: Pagerinimai Tipas Čia surinkite kelią arba URL Nerikiuota Valdiklio tvarkymas Valdikliai atrakinti Paspauskite ir palaikykite valdiklius, kad perkelti juos ir pamatyti jų rankenėles. 