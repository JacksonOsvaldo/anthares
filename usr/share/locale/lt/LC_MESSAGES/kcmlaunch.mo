��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     3     8  �  V  �  �  f   x
     �
     �
       )   &     P     n     �      �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2005-09-02 17:28+0300
Last-Translator: Donatas Glodenis <dgvirtual@akl.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
  sek Paleidimo indikacijos &delsa: <H1>Užduočių juostos pranešimas</H1>
Jūs galite įjungti tokį startavimo pranešimo metodą, kada yra naudojamas
užduočių juostoje besisukantis mygtukas, simbolizuojantis, kad
įkeliama Jūsų paleista programa. Gali būti, kad kai kurios
programos nežino apie tokį paleidimo pranešimą. Tokiu atveju,
mygtukas pranyks, praėjus laikui, nurodytam sekcijoje
„Paleidimo indikacijos delsa“ <h1>Užimtumo žymeklis</h1>
KDE siūlo informavimui apie paleidžiamas programas
naudoti „užimtumo žymeklį“.
Norėdami naudoti užimtumo žymeklį, pažymėkite 
„Naudoti užimtumo žymeklį“.
Gali būti, kad kai kurios programos neatpažins šitų paleidimo
pranešimų. Tokiu atveju, žymeklis nustos mirkčioti, 
praėjus laikui, nurodytam sekcijoje „Paleidimo indikacijos delsa“ <h1>Paleisties grįžtamasis ryšys</h1> Čia galite suderinti programų paleidimo grįžtamą ryšį. Mirkčiojantis žymeklis Šokinėjantis žymeklis Užimt&umo žymeklis &Įjungti užduočių juostos pranešimą Nenaudoti užimtumo žymeklio Pasyvus užimtumo žymeklis Paleidimo indikacijos &delsa: Užduočių juostos pra&nešimas 