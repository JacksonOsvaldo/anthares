��    =        S   �      8     9  !   U  "   w  &   �  ,   �  #   �  &     !   9  &   [  '   �  "   �  #   �  "   �  '        <     O  +   S  
        �     �     �     �     �     �  U   �  7   :     r     �     �     �      �     �     �     �      	  !   	     8	  	   =	     G	     O	     o	     �	  &   �	     �	     �	     �	     
     
     
     %
  $   -
     R
     c
     }
     �
     �
     �
     �
  &   �
         %     (     E     R     Z     f     z     �     �     �     �     �     �     �     �     �     �  5     
   7     B     R     f     �     �     �  ]   �  8        D     Y     o     x  #     !   �     �     �     �  #   �          $     4  "   F     i     �  ,   �  "   �  
   �      �  
        &     2     L  $   Y     ~      �     �     �     �     �       +   %  "   Q         #          1   &                  	   ;          5       <   0   2          +                                 '   :              4   -             /   !      *               3   )                    
          $             ,   8   7   9           %       "      (       =          6      .       &Matching window property:  @item:inlistbox Border size:Huge @item:inlistbox Border size:Large @item:inlistbox Border size:No Border @item:inlistbox Border size:No Side Borders @item:inlistbox Border size:Normal @item:inlistbox Border size:Oversized @item:inlistbox Border size:Tiny @item:inlistbox Border size:Very Huge @item:inlistbox Border size:Very Large @item:inlistbox Button size:Large @item:inlistbox Button size:Normal @item:inlistbox Button size:Small @item:inlistbox Button size:Very Large Active Window Glow Add Add handle to resize windows with no border Animations B&utton size: Border size: Button mouseover transition Center Center (Full Width) Class:  Configure fading between window shadow and glow when window's active state is changed Configure window buttons' mouseover highlight animation Decoration Options Detect Window Properties Dialog Edit Edit Exception - Oxygen Settings Enable/disable this exception Exception Type General Hide window title bar Information about Selected Window Left Move Down Move Up New Exception - Oxygen Settings Question - Oxygen Settings Regular Expression Regular Expression syntax is incorrect Regular expression &to match:  Remove Remove selected exception? Right Shadows Tit&le alignment: Title:  Use window class (whole application) Use window title Warning - Oxygen Settings Window Class Name Window Drop-Down Shadow Window Identification Window Property Selection Window Title Window active state change transitions Window-Specific Overrides Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-13 05:20+0100
PO-Revision-Date: 2015-08-25 15:02+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 &Atitinkanti lango savybė:  Milžiniška Didelė Be rėmelio Be krašto rėmelio Normali Per didelė Smulki Labai milžiniška Labai didelė Didelis Normalus Mažas Labai didelis Aktyvaus lango švytėjimas Pridėti Pridėti rankenėlę keisti langų be rėmų dydžiui Animacijos Mygtukų dydis: Kraštinės plotis: Palės virš mygtuko pakitimas Centre Vidurys (pilnas plotis) Klasė:  Konfigūruoti išblukimą tarp šešėlio ir spindesio kai pakeičiama lango aktyvumo būsena Konfigūruoti lango mygtuko paryškinimą užvedus pelę Išvaizdos parinktys Aptikti lango savybes Dialogas Keisti Keisti išimtį - Oxygen nustatymai Įjungti/išjungti šią išimtį Išimtinės tipas Bendra Slėpti lango antraštę Informacija apie pasirinktą langą Kairėje Perkelti žemyn Perkelti aukštyn Nauja išimtis - Oxygen nustatymai Klausimas - Oxygen nustatymai Reguliarusis reiškinys Neteisinga reguliarios išraiškos sintaksė Atitinkanti reguliari išraiška:  Pašalinti Pašalinti pažymėtą išimtį? Dešinėje Šešėliai Antraščių &lygiavimas: Antraštė:  Naudoti lango klasę (visa programa) Naudoti lango antraštę Perspėjimas - Oxygen nustatymai Lango klasė Nuo lango krentantis šešėlis Lango atpažinimas Lango savybių parinkimas: Lango pavadinimas Langų aktyvios būsenos keitimo perėjimas Nuo lango priklausantys pakeitimai 