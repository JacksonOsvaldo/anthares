��          L      |       �   `   �      
          "     *    ;  m   J  
   �     �  
   �  #   �                                         Finds local directories and files, network locations and Internet sites with paths matching :q:. Go to %1 Launch with %1 Open %1 Send email to %1 Project-Id-Version: plasma_runner_locations
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2013-04-25 14:12+0300
Last-Translator: Liudas Alisauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Randa vietinius aplankus ir failus, tinklo vietas ir žiniatinklio svetaines su keliais, atitinkančiais :q:. Eiti į %1 Paleisti su %1 Atverti %1 Išsiųsti el. laišką gavėjui %1 