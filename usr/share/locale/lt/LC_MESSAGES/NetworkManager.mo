��    :     �  �  �#      �/     �/     �/     �/     �/  !   �/      0  #   -0     Q0     m0     �0  7   �0     �0     �0  	   �0     �0  &   1      31     T1  (   q1  .   �1  /   �1     �1  -   2     C2      X2     y2  0   �2     �2     �2     3     3     33     R3     k3  '   �3      �3  !   �3  "   �3  ,   4  "   B4  *   e4     �4  7   �4  !   �4  '   �4  #   $5  0   H5     y5     �5  *   �5  ,   �5     �5      6  %   :6  -   `6     �6     �6     �6  	   �6  2  �6      9     	9     9  &   9     @9     _9  /   x9  -   �9  	   �9  %   �9  5   :  4   <:     q:     v:     �:     �:     �:     �:  	   �:  $   �:  %   �:  5   ;      =;     ^;     s;     �;     �;     �;     �;     �;     �;  	   �;     �;     <     <     .<     5<     J<     Q<     f<     }<     �<  
   �<  )   �<  +   �<  ,   =  )   :=  +   d=     �=  /   �=  +   �=  :   >  ?   B>     �>  #   �>     �>     �>     �>  %   ?      +?      L?     m?     �?     �?  '   �?  '   �?  &   @      D@      e@     �@     �@     �@     �@     �@     �@     �@     A     A     -A  %   <A     bA     xA  '   �A  -   �A  )   �A     B  (   &B     OB     dB  &   vB     �B     �B     �B     �B  0   �B  *   ,C  #   WC     {C  J   �C  6  �C  I  E     ZF  '   rF  
   �F  '   �F     �F  
   �F     �F     G      G  )   3G  2   ]G     �G     �G     �G     �G     �G     H     $H      AH  .   bH  #   �H  0   �H  .   �H  5   I  A   KI  '   �I  %   �I  )   �I     J     #J  ;   BJ  /   ~J  %   �J     �J  -   �J      K     >K     ^K  '   }K  1   �K  >   �K  6   L  >   ML  5   �L  0   �L  !   �L     M     4M     TM  6   tM  /   �M  *   �M  (   N  *   /N  !   ZN  !   |N  %   �N  "   �N  .   �N  (   O  '   ?O  0   gO  )   �O     �O  #   �O  #   P  #   (P  /   LP  !   |P     �P     �P  ;   �P  5   Q      LQ  (   mQ  )   �Q  +   �Q  (   �Q  9   R  )   OR  )   yR     �R     �R     �R     �R  !   S  $   .S     SS     pS  +   �S  J   �S     T  $   T     :T  "   XT  &   {T  <   �T  =   �T     U  5   4U  ,   jU  )   �U  )   �U  '   �U  +   V  0   ?V  ,   pV  0   �V  ,   �V  -   �V  "   )W      LW      mW  +   �W  +   �W     �W     �W     	X  !   &X     HX     ^X     eX     mX     }X  L   �X     �X     �X     Y     Y     1Y  #   ?Y  -   cY  
   �Y  A   �Y     �Y  1   �Y     )Z     /Z     >Z  (   JZ  )   sZ  0   �Z  !   �Z  B   �Z     3[     8[      H[     i[  5   ~[     �[     �[     �[     �[     �[     �[  4   	\  0   >\  9   o\  0   �\  /   �\  4   
]     ?]     D]     `]     ~]  (   �]  !   �]  #   �]  )   	^     3^  0   7^     h^     �^     �^  "   �^     �^      _     _  �   2_     `     *`     @`  
   ]`  (   h`     �`     �`     �`     �`     �`     �`     a     a     ,a  $   0a  &   Ua  !   |a     �a  
   �a     �a     �a     �a     �a     b  
   b     &b  %   -b  $   Sb     xb     �b  Z   �b     �b     �b     
c     c     c  (   #c     Lc  '   \c     �c     �c      �c  )   �c     �c  "   �c     d     "d     4d  5   <d  9   rd  K   �d  E   �d  >   >e  E   }e  @   �e  E   f  F   Jf  G   �f  C   �f     g     "g  F   5g  ,   |g  '   �g  $   �g  1   �g     (h  *   ?h  ,   jh     �h     �h  )   �h  "   �h  5   i     Si     di     �i  S   �i  
   �i  *    j     +j  %   /j  +   Uj     �j     �j     �j     �j     �j     �j     �j     k  M   *k     xk     �k     �k     �k     �k     �k     �k     �k     �k  )   �k  $   l     ;l     Tl     cl     ul     �l     �l     �l     �l     �l     �l     m     m     m     &m     6m     ;m     @m  J   Rm  J   �m  F   �m     /n     Bn     Rn     fn     un     �n  *   �n  (   �n     �n     o     (o  	   Eo  
   Oo     Zo     ho     oo     to  !   yo     �o  O   �o  	    p     
p     !p  
   7p  %   Bp     hp  %   �p      �p     �p  +   �p     	q     q     'q     4q  �   <q  �   �q  /   mr  0   �r     �r     �r     �r  (   �r     s     #s     >s  &   Ps  0   ws  4   �s  !   �s  (   �s     (t     Gt     at     {t  %   �t  #   �t     �t     �t     u     %u      8u     Yu     hu     nu     �u  #   �u     �u  #   �u     �u     v     v     v     -v     Hv     Wv     cv     gv  $   jv  	   �v  �   �v  �   w  $   �w     x     x     .x  0   Bx  .   sx  �   �x     *y  3   Hy     |y     �y  U   �y     �y  5   z     Az  (   _z     �z     �z     �z     �z  	   �z  ?   �z  :   {  "   N{     q{  �  u{  #   ^}     �}     �}  '   �}  (   �}     �}  -   ~     2~  '   R~     z~  <   �~     �~     �~  	   �~     �~  *   
  #   5      Y  7   z  4   �  :   �  '   "�  1   J�     |�  +   ��  #   ��  8   �     �  #   :�     ^�     z�  %   ��     ��     ܁  1   ��  )   .�  1   X�  ,   ��  0   ��  &   �  %   �     5�  M   L�  -   ��  7   ȃ  1    �  >   2�     q�  !   ��  <   ��  /   �  (   �  &   B�  (   i�  1   ��     ą     �     �     ��  t  	�     ~�  	   ��     ��  )   ��          ߈  ;   ��  0   4�  	   e�     o�  4   ��  )   É     �     �     ��     �     	�  !   �     4�     <�  %   Z�  .   ��  &   ��     ֊  #   �  !   �     3�     I�     `�     n�     s�  	   y�     ��     ��     ��  
   ��     ŋ  
   ׋     �     �     �     9�  
   U�  +   `�  (   ��  +   ��  ,   �  )   �  '   8�  4   `�  1   ��  8   Ǎ  D    �     E�  &   N�  $   u�  ,   ��     ǎ  0   �  %   �  %   <�  $   b�  !   ��  !   ��  ,   ˏ  ,   ��  +   %�  &   Q�  &   x�  !   ��     ��     Ր     ��     �  	   "�     ,�  
   H�     S�     l�  +   ��     ��     ͑  .   �  :   �  (   W�     ��  -   ��     ��     В  +   �     �     &�     D�  (   `�  J   ��  D   ԓ  5   �     O�  L   k�  d  ��    �     ��  *   ��     �  )   �     �  
   6�     A�     N�  $   ^�  #   ��  @   ��     �     ��  !   �     :�     X�     x�  !   ��  %   ��  4   ޙ  '   �  2   ;�  3   n�  =   ��  C   ��  %   $�  %   J�  '   p�  %   ��  "   ��  B   �  0   $�      U�  "   v�  4   ��     Μ     �     	�  *   %�  B   P�  O   ��  8   �  O   �  7   l�  6   ��  !   ۞      ��  &   �  #   E�  @   i�  6   ��  -   �  6   �  ,   F�  &   s�  %   ��  $   ��  $   �  6   
�  /   A�  1   q�  D   ��  3   �  $   �  $   A�  $   f�  2   ��  >   ��  '   ��     %�  !   D�  D   f�  C   ��  -   �  0   �  ,   N�  2   {�  '   ��  K   ֤  +   "�  +   N�  !   z�     ��     ��     ϥ     �  /   �  '   2�  "   Z�  +   }�  `   ��     
�  -   %�     S�  &   r�  *   ��  H   ħ  A   �     O�  2   g�  9   ��  1   Ԩ  &   �  +   -�  /   Y�  /   ��  ,   ��  ,   �  )   �  '   =�      e�  %   ��  "   ��  4   Ϫ  1   �     6�  (   =�  (   f�  !   ��     ��     ˫     ҫ     �     ��  _   ��      ^�     �     ��      ��     ά  '   ެ  -   �  
   4�  C   ?�     ��  0   ��     ɭ     ϭ  	   ޭ  )   �  *   �  @   =�  )   ~�  `   ��     	�     �  %   $�     J�  =   c�     ��     ��     ϯ     ׯ     ߯  %   �  A   	�  B   K�  L   ��  @   ۰  G   �  O   d�     ��     ��  ,   ۱     �  &   �     B�      b�  %   ��     ��  B   ��     ��  $   �  !   =�     _�     �     ��     ��  �   ʳ     ��     ȴ     ߴ     ��  (   ��     '�     C�     b�     x�     ��     ��     ��     ��     �  .   �  /   �  0   C�     t�     ��     ��     ��     ֶ     ܶ      �     �     #�  +   0�  ,   \�     ��     ��  j   ��      �     9�     K�     T�     f�  &   k�     ��  %   ��     ʸ     Ҹ  *   �  (   �     >�     E�     a�     v�     ��  3   ��  E   Ź  g   �  a   s�  R   պ  R   (�  A   {�  <   ��  F   ��  O   A�  L   ��     ޼     �  0   ��  .   ,�     [�     z�  -   ��     Ľ     �  &    �     '�  %   E�  .   k�     ��  3   ��     �     ��      �  A   0�     r�  ;   ��     ��  (   ��  )   �  	   �  )   �     H�     e�  "   v�  )   ��     ��     ��  R   ��     K�     `�     q�     v�     ��     ��     ��     ��     ��  *   ��  +   ��     �  
   9�     D�     R�     k�     z�     ��     ��     ��  #   ��     ��     ��     ��     ��     ��     ��     ��  D   �  S   S�  O   ��     ��     �     (�     .�     D�     W�      t�  $   ��     ��     ��     ��  	   �     �     &�  	   4�     >�     C�  0   O�     ��  [   ��     ��     ��     �  
   ,�  &   7�     ^�  &   {�  '   ��     ��  1   ��     �  
   (�     3�     A�  |   J�  �   ��  4   \�  1   ��  	   ��  	   ��     ��  %   ��     
�  &   �     :�  &   R�  /   y�  /   ��  )   ��  &   �  +   *�      V�      w�     ��  6   ��  /   ��  *   �     I�     d�     j�  #   y�     ��     ��     ��     ��  *   ��  !   �  $   $�     I�     b�     }�     ��     ��     ��     ��  	   ��     ��  7   ��  	   �  �   $�  �   ��  -   m�     ��     ��     ��  +   ��  9    �  �   :�  +   ��  %   ��  %   �     A�  a   H�     ��  J   ��     	�  $   "�     G�  	   T�     ^�     u�  
   ��  X   ��  S   ��  %   I�     o�             �  6  �   &  �                 
      �  D  ]  _   �  �      s  4      q        U   �   �  Y              +  �  �  /       �  ]   O   �      E       �       H     $  �               �   b   g  �            �      �          \   O  �  o     �       N  N   �   �              �   �  �  �  S   �  {   �   �       �      �  :        �  n        �              ?      �             }  ,  /          �                         #      3          V  �  Z   m     �   �  �   7  !     �  o      7       �                 �    2  �      �   �  v      �   �  �  �  W   �       �   �       !      �       �  �   �   !  �  v   L  1  g   �   R  �  t  ,   -       �    �  X   �  �                 �   Q     P              �  �   �   u   %   �           y  �       �  �   �   �  ?   f   �  #           �   �   �      �      #  �         �   p       �   �  �   �       :   �       /  �   3   �          �  T      �   �   F  �   j           �  n   �  h     9            L       0  �  �  �       8           �         �      @   �  �   �     k          �  �           �  �     �   i  =       �   �   �       �   �  �   c  �      �       E  �    �   �      �   �  h   �  ~  V       �   l       x       �   �        �  5  $  X  $   a   �  1   �        �     �   �   �  z  �  m         @  �  �   <  r   >   �                 �       Q  (  �       "  �      �   [  �   J      |  *  �      �  )       �      u  �   �  �   �  K             D       �   [   e       �       �                  (   =  <   �  	      �  �   �  
   �   �   ;       H   J           �  �   �  p  G      *         8  �  i   �   �      �   �           I  �  �       ~      �          U  8  �  {  5  �                 ;  �   "  �   �                      >  &      �     B   �   �      �         �  R   9     ^   �   �   )  �       �   &   +  I   �     �   B  �   '      �  s       .         �   �  (  �  �  �  �  6  �        �           +   M       f  �  �  	  1  G     �           �   �   �  �  �           "       w  3  �   �          �       �        .  `  �  �   �     ,  �   j  6     d  Z  C   �     �  d   '        P   C  0  A    �   �               \  Y           �       `   9   |   �  �     7     �  _      �  �      '   A   �  a      �   F     -  �       *  	   �          �  �  �          �       �   .  e  �  x          2       )  �      ^  �              
  �        c     �  z   0   �       �   t           �   �  �  �     �      %  2  5   q     -  S      �  �  �    4      l        �        �  %  �         �                 M      K      �   }               r      T       k          �  W  �  �    �   �   :      �   4  �   b              �           y       w   �    # Created by NetworkManager
 # Merged from %s

 %d (disabled) %d (enabled, prefer public IP) %d (enabled, prefer temporary IP) %d (unknown) %d. IPv4 address has invalid prefix %d. IPv4 address is invalid %d. route has invalid prefix %d. route is invalid %s.  Please use --help to see a list of valid options.
 %u MHz %u Mb/s %u Mbit/s '%d' is not a valid channel '%d' is out of valid range <128-16384> '%d' value is out of range <0-3> '%ld' is not a valid channel '%s' can only be used with '%s=%s' (WEP) '%s' connections require '%s' in this property '%s' contains invalid char(s) (use [A-Za-z._-]) '%s' is ambiguous (%s x %s) '%s' is neither an UUID nor an interface name '%s' is not a number '%s' is not a valid Ethernet MAC '%s' is not a valid IBoIP P_Key '%s' is not a valid IPv4 address for '%s' option '%s' is not a valid MAC '%s' is not a valid MAC address '%s' is not a valid PSK '%s' is not a valid UUID '%s' is not a valid Wi-Fi mode '%s' is not a valid band '%s' is not a valid channel '%s' is not a valid channel; use <1-13> '%s' is not a valid duplex value '%s' is not a valid hex character '%s' is not a valid interface name '%s' is not a valid number (or out of range) '%s' is not a valid value for '%s' '%s' is not a valid value for the property '%s' is not valid '%s' is not valid master; use ifname or connection UUID '%s' is not valid; use 0, 1, or 2 '%s' is not valid; use <option>=<value> '%s' is not valid; use [%s] or [%s] '%s' length is invalid (should be 5 or 6 digits) '%s' not among [%s] '%s' option is empty '%s' option requires '%s' option to be set '%s' security requires '%s' setting presence '%s' security requires '%s=%s' '%s' value doesn't match '%s=%s' '%s=%s' is incompatible with '%s > 0' '%s=%s' is not a valid configuration for '%s' '--order' argument is missing (none) (unknown error) (unknown) ---[ Property menu ]---
set      [<value>]               :: set new value
add      [<value>]               :: add new option to the property
change                           :: change current value
remove   [<index> | <option>]    :: delete the value
describe                         :: describe property
print    [setting | connection]  :: print property (setting/connection) value(s)
back                             :: go to upper level
help/?   [<command>]             :: print this help or command description
quit                             :: exit nmcli
 0 (NONE) 0 (none) 802.1X 802.1X supplicant configuration failed 802.1X supplicant disconnected 802.1X supplicant failed 802.1X supplicant took too long to authenticate ===| nmcli interactive connection editor |=== A (5 GHz) A dependency of the connection failed A problem with the RFC 2684 Ethernet over ADSL bridge A secondary connection of the base connection failed ADSL ADSL connection APN Ad-Hoc Add Adding a new '%s' connection Addresses Allow control of network connections Allowed values for '%s' property: %s
 An http(s) address for checking internet connectivity Ask for this password every time AutoIP service error AutoIP service failed AutoIP service failed to start Available properties: %s
 Available settings: %s
 B/G (2.4 GHz) BOND BSSID Bluetooth Bluetooth device address Bond Bond connection %d Bridge Bridge connection %d Cancel Carrier/link changed Closing %s failed: %s
 Config directory location Config file location Connecting Connection '%s' (%s) successfully added.
 Connection '%s' (%s) successfully deleted.
 Connection '%s' (%s) successfully modified.
 Connection '%s' (%s) successfully saved.
 Connection '%s' (%s) successfully updated.
 Connection profile details Connection sharing via a protected WiFi network Connection sharing via an open WiFi network Connection successfully activated (D-Bus active path: %s)
 Connection with UUID '%s' created and activated on device '%s'
 Cookie Could not daemonize: %s [error %u]
 Could not decode private key. Could not generate random data. Could not load file '%s'
 Couldn't convert password to UCS2: %d Couldn't decode PKCS#12 file: %d Couldn't decode PKCS#12 file: %s Couldn't decode PKCS#8 file: %s Couldn't decode certificate: %d Couldn't decode certificate: %s Couldn't initialize PKCS#12 decoder: %d Couldn't initialize PKCS#12 decoder: %s Couldn't initialize PKCS#8 decoder: %s Couldn't verify PKCS#12 file: %d Couldn't verify PKCS#12 file: %s Current nmcli configuration:
 DHCP client error DHCP client failed DHCP client failed to start DNS servers Delete Destination port [8472] Device Device '%s' not found Device details Device disconnected by user or client Device is now managed Device is now unmanaged Do you also want to clear '%s'? [yes]:  Do you also want to set '%s' to '%s'? [yes]:  Doesn't look like a PEM private key file. Don't become a daemon Don't become a daemon, and log to stderr Don't print anything Edit '%s' value:  Editing existing '%s' connection: '%s' Enable PI [no] Enable VNET header [no] Enable encryption [yes] Enable or disable WiFi devices Enable or disable WiMAX mobile broadband devices Enable or disable mobile broadband devices Enable or disable system networking Enter '%s' value:  Enter a list of IPv4 addresses of DNS servers.

Example: 8.8.8.8, 8.8.4.4
 Enter a list of bonding options formatted as:
  option = <value>, option = <value>,... 
Valid options are: %s
'mode' can be provided as a name or a number:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

Example: mode=2,miimon=120
 Enter bytes as a list of hexadecimal values.
Two formats are accepted:
(a) a string of hexadecimal digits, where each two digits represent one byte
(b) space-separated list of bytes written as hexadecimal digits (with optional 0x/0X prefix, and optional leading 0).

Examples: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 Enter connection type:  Error initializing certificate data: %s Error: %s
 Error: %s - no such connection profile. Error: %s argument is missing. Error: %s. Error: %s.
 Error: %s: %s. Error: '%s' argument is missing. Error: '%s' is not an active connection.
 Error: '%s' is not valid argument for '%s' option. Error: '%s': %s Error: 'autoconnect': %s. Error: 'connection show': %s Error: 'device show': %s Error: 'device status': %s Error: 'device wifi': %s Error: 'general logging': %s Error: 'general permissions': %s Error: 'networking' command '%s' is not valid. Error: 'type' argument is required. Error: <setting>.<property> argument is missing. Error: Access point with bssid '%s' not found. Error: Argument '%s' was expected, but '%s' provided. Error: BSSID to connect to (%s) differs from bssid argument (%s). Error: Connection activation failed: %s Error: Connection deletion failed: %s Error: Device '%s' is not a Wi-Fi device. Error: Device '%s' not found. Error: Device '%s' not found.
 Error: Failed to add/activate new connection: Unknown error Error: Failed to save '%s' (%s) connection: %s
 Error: NetworkManager is not running. Error: No Wi-Fi device found. Error: No access point with BSSID '%s' found. Error: No arguments provided. Error: No connection specified. Error: No interface specified. Error: No network with SSID '%s' found. Error: Option '%s' is unknown, try 'nmcli -help'. Error: Option '--pretty' is mutually exclusive with '--terse'. Error: Option '--pretty' is specified the second time. Error: Option '--terse' is mutually exclusive with '--pretty'. Error: Option '--terse' is specified the second time. Error: Parameter '%s' is neither SSID nor BSSID. Error: SSID or BSSID are missing. Error: Timeout %d sec expired. Error: Unexpected argument '%s' Error: Unknown connection '%s'. Error: bssid argument value '%s' is not a valid BSSID. Error: cannot delete unknown connection(s): %s. Error: connection verification failed: %s
 Error: extra argument not allowed: '%s'. Error: failed to create temporary file %s. Error: failed to export '%s': %s. Error: failed to import '%s': %s. Error: failed to load connection: %s. Error: failed to modify %s.%s: %s. Error: failed to read temporary file '%s': %s. Error: failed to reload connections: %s. Error: failed to set '%s' property: %s
 Error: invalid '%s' argument: '%s' (use on/off). Error: invalid <setting>.<property> '%s'. Error: invalid argument '%s'
 Error: invalid connection type; %s
 Error: invalid connection type; %s. Error: invalid extra argument '%s'. Error: invalid or not allowed setting '%s': %s. Error: invalid property '%s': %s. Error: invalid property: %s
 Error: invalid property: %s%s
 Error: invalid property: %s, neither a valid setting name.
 Error: invalid setting argument '%s'; valid are [%s]
 Error: invalid setting name; %s
 Error: missing argument for '%s' option. Error: missing setting for '%s' property
 Error: no setting selected; valid are [%s]
 Error: not all active connections found. Error: only one of 'id', uuid, or 'path' can be provided. Error: openconnect failed with signal %d
 Error: openconnect failed with status %d
 Error: openconnect failed: %s
 Error: property %s
 Error: save-confirmation: %s
 Error: status-line: %s
 Error: the connection is not VPN. Error: unknown extra argument: '%s'. Error: unknown setting '%s'
 Error: unknown setting: '%s'
 Error: value for '%s' argument is required. Error: wep-key-type argument value '%s' is invalid, use 'key' or 'phrase'. Ethernet device Failed to decode PKCS#8 private key. Failed to decode certificate. Failed to decrypt the private key. Failed to decrypt the private key: %d. Failed to decrypt the private key: decrypted data too large. Failed to decrypt the private key: unexpected padding length. Failed to encrypt: %d. Failed to finalize decryption of the private key: %d. Failed to find expected PKCS#8 end tag '%s'. Failed to find expected PKCS#8 start tag. Failed to initialize the MD5 context: %d. Failed to initialize the crypto engine. Failed to initialize the crypto engine: %d. Failed to initialize the decryption cipher slot. Failed to initialize the decryption context. Failed to initialize the encryption cipher slot. Failed to initialize the encryption context. Failed to register with the requested network Failed to select the specified APN Failed to set IV for decryption. Failed to set IV for encryption. Failed to set symmetric key for decryption. Failed to set symmetric key for encryption. GROUP GSM Modem's SIM PIN required GSM Modem's SIM PUK required GSM Modem's SIM card not inserted GSM Modem's SIM wrong GVRP,  Gateway Group ID [none] Hide IP configuration could not be reserved (no available address, timeout, etc.) IPv4 address (IP[/plen]) [none] IPv4 gateway [none] IPv4 protocol IPv6 address (IP[/plen]) [none] IPv6 protocol IV contains non-hexadecimal digits. IV must be an even number of bytes in length. InfiniBand InfiniBand P_Key connection did not specify parent interface name InfiniBand connection %d InfiniBand device does not support connected mode Infra Interface(s):  Interface:  Invalid IV length (must be at least %d). Invalid IV length (must be at least %zd). Invalid configuration option '%s'; allowed [%s]
 Invalid device Bluetooth address. Invalid option.  Please use --help to see a list of valid options. LEAP LOOSE_BINDING,  List of plugins separated by ',' Local address [none] Log domains separated by ',': any combination of [%s] Log level: one of [%s] MACsec connection MKA CAK MKA_CKN MTU Make all warnings fatal Malformed PEM file: DEK-Info was not the second tag. Malformed PEM file: Proc-Type was not first tag. Malformed PEM file: invalid format of IV in DEK-Info tag. Malformed PEM file: no IV found in DEK-Info tag. Malformed PEM file: unknown Proc-Type tag '%s'. Malformed PEM file: unknown private key cipher '%s'. Mode Modem initialization failed Modem now ready and available ModemManager is unavailable Modify network connections for all users Modify persistent system hostname Modify personal network connections Must specify a P_Key if specifying parent N/A Necessary firmware for the device may be missing Network registration denied Network registration timed out NetworkManager active profiles NetworkManager connection profiles NetworkManager has started NetworkManager has stopped NetworkManager logging NetworkManager monitors all network connections and automatically
chooses the best connection to use.  It also allows the user to
specify wireless access points which wireless cards in the computer
should associate with. NetworkManager permissions NetworkManager status NetworkManager went to sleep Networking Networkmanager is now in the '%s' state
 New connection name:  No carrier could be established No dial tone No reason given Not searching for networks OK Opening %s failed: %s
 Output file name:  PCI PEM certificate had no end tag '%s'. PEM certificate had no start tag '%s'. PEM key file had no end tag '%s'. PIN check failed PPP failed PPP service disconnected PPP service failed to start PPPoE PPPoE username Password must be UTF-8 Password:  Prefix Print NetworkManager version and exit Private key cipher '%s' was unknown. Property name?  Proxy Put NetworkManager to sleep or wake it up (should only be used by system power management) REORDER_HEADERS,  Radio switches Remove SCI port [1] SSID SSID length is out of range <1-32> bytes SSID or BSSID:  Secrets were required, but not provided Service Setting name?  Shared connection service failed Shared connection service failed to start Show Specify the location of a PID file State file location Status of devices Success System policy prevents control of network connections System policy prevents enabling or disabling WiFi devices System policy prevents enabling or disabling WiMAX mobile broadband devices System policy prevents enabling or disabling mobile broadband devices System policy prevents enabling or disabling system networking System policy prevents modification of network settings for all users System policy prevents modification of personal network settings System policy prevents modification of the persistent system hostname System policy prevents putting NetworkManager to sleep or waking it up System policy prevents sharing connections via a protected WiFi network System policy prevents sharing connections via an open WiFi network Team Team connection %d The Bluetooth addresses of the device and the connection didn't match. The Bluetooth connection failed or timed out The IP configuration is no longer valid The Wi-Fi network could not be found The device could not be readied for configuration The device was removed The device's active connection disappeared The device's existing connection was assumed The dialing attempt failed The dialing request timed out The error cannot be fixed automatically.
 The expected start of the response The interval between connectivity checks (in seconds) The line is busy The modem could not be found The supplicant is now available Time to wait for a connection, in seconds (without the option, default value is 30) Tun device Type 'help' or '?' for available commands. USB Unable to determine private key type. Unexpected amount of data after encrypting. Unknown Unknown command argument: '%s'
 Unknown command: '%s'
 Unknown error Unknown log domain '%s' Unknown log level '%s' Unknown parameter: %s Unknown parameter: %s
 Usage: nmcli agent { COMMAND | help }

COMMAND := { secret | polkit | all }

 User ID [none] Username VLAN VLAN ID (<0-4094>) VLAN connection VLAN connection %d VPN VPN connected VPN connecting VPN connecting (getting IP configuration) VPN connecting (need authentication) VPN connecting (prepare) VPN connection VPN connection %d VPN connection failed VPN disconnected VXLAN ID VXLAN connection Valid connection types: %s
 Verify connection: %s
 Verify setting '%s': %s
 WEP WEP key index2 WEP key index3 WEP key index4 WPA1 WPA2 WWAN radio switch Waits for NetworkManager to finish activating startup network connections. Warning: editing existing connection '%s'; 'con-name' argument is ignored
 Warning: editing existing connection '%s'; 'type' argument is ignored
 Wi-Fi radio switch Wi-Fi scan list Wi-Fi securityNone WiMAX NSP name Wired connection %d Writing to %s failed: %s
 You may edit the following properties: %s
 You may edit the following settings: %s
 ['%s' setting values]
 [NM property description] [nmcli specific description] activated activating agent-owned,  asleep auth auto back  :: go to upper menu level

 bluetooth connection change  :: change current value

Displays current value and allows editing it.
 connected connected (local only) connected (site only) connecting connecting (checking IP connectivity) connecting (configuring) connecting (getting IP configuration) connecting (need authentication) connecting (prepare) connecting (starting secondary connections) connection failed deactivated deactivating default describe  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 describe [<setting>.<prop>]  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 device '%s' not compatible with connection '%s' device '%s' not compatible with connection '%s': disabled disconnected disconnecting don't know how to get the property value enabled field '%s' has to be alone flags are invalid has to match '%s' property for PKCS#12 help/? [<command>]  :: help for nmcli commands

 help/? [<command>]  :: help for the nmcli commands

 index '%d' is not in range <0-%d> index '%d' is not in the range of <0-%d> invalid '%s' or its value '%s' invalid IPv4 address '%s' invalid IPv6 address '%s' invalid option '%s' invalid option '%s' or its value '%s' invalid prefix '%s'; <1-%d> allowed invalid priority map '%s' is not a valid MAC address long device name%s %s macvlan connection mandatory option '%s' is missing missing option never nmcli tool, version %s
 no no active connection on device '%s' no active connection or device no device found for connection '%s' no item to remove no priority to remove none not a file (%s) not a valid interface name not required,  not saved,  off on only one of '%s' and '%s' can be set preparing print [all]  :: print setting or connection values

Shows current property or the whole connection.

Example: nmcli ipv4> print all
 print [property|setting|connection]  :: print property (setting, connection) value(s)

Shows property value. Providing an argument you can also display values for the whole setting or connection.
 priority '%s' is not valid (<0-%ld>) property is empty property is invalid property is missing property is not specified and neither is '%s:%s' property value '%s' is empty or too long (>64) quit  :: exit nmcli

This command exits nmcli. When the connection being edited is not saved, the user is asked to confirm the action.
 requires '%s' or '%s' setting requires presence of '%s' setting in the connection requires setting '%s' property running set [<value>]  :: set new value

This command sets provided <value> to this property
 setting not found setting this property requires non-zero '%s' property the property can't be changed this property is not allowed for '%s=%s' unavailable unknown unknown connection '%s' unknown device '%s'. unmanaged use 'goto <setting>' first, or 'describe <setting>.<property>'
 use 'goto <setting>' first, or 'set <setting>.<property>'
 value '%d' is out of range <%d-%d> yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-11-10 15:29+0100
PO-Revision-Date: 2017-04-21 06:10-0400
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Lietuvių <gnome-lt@lists.akl.lt>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Zanata 3.9.6
 # Sukurta NetworkManager programos
 # Sulieta iš %s

 %d (išjungta) %d (įjungta, pageidaujamas viešas IP) %d (įjungta, pageidaujamas laikinas IP) %d (nežinoma) %d. IPv4 adresas turi netinkamą priešdėlį %d. IPv4 adresas yra netinkamas %d. kelias turi netinkamą priešdėlį %d. kelias yra netinkamas %s.  Naudokite --help, galimų parinkčių sąrašui gauti.
 %u MHz %u Mb/s %u Mbit/s „%d“ yra netinkamas kanalas „%d“ yra už galimų ribų <128-16384> „%d“ vertė yra už ribų <0-3> „%ld“ nėra tinkamas kanalas „%s“ gali būti naudojamas tik su „%s=%s“ (WEP) „%s“ ryšys šioje savybėje reikalauja „%s“ „%s“ turi netinkamų simbolių (naudokite [A-Za-z._-]) „%s“ yra nevienareikšmis (%s x %s) „%s“ nėra nei UUID, nei sąsajos pavadinimas „%s“ nėra skaičius „%s“ nėra tinkamas laidinio tinklo MAC „%s“ nėra tinkamas IBoIP P_Key „%s“ yra netinkamas IPv4 adresas parametrui „%s“ „%s“ nėra tinkamas MAC „%s“ nėra tinkamas MAC adresas „%s“ nėra tinkamas PSK „%s“ nėra tinkamas UUID „%s“ nėra tinkama Wi-Fi veiksena „%s“ nėra tinkamos bangos „%s“ nėra tinkamas kanalas „%s“ yra netinkamas kanalas; naudokite <1-13> „%s“ nėra tinkama dupleksinė vertė „%s“ nėra tinkamas šešioliktainis simbolis „%s“ nėra tinkamas sąsajos pavadinimas „%s“ yra netinkamas numeris (arba už ribų) „%s“ yra netinkama „%s“ vertė „%s“ yra netinkama vertė savybei „%s“ yra netinkama „%s“ nėra tinkamas pagrindinis ryšys; naudokite ifname arba ryšio UUID „%s“ yra netinkamas; naudokite 0,1 arba 2 „%s“ yra netinkama; naudokite <parametras>=<vertė> „%s“ yra netinkamas; naudokite [%s] arba [%s] „%s“ ilgis yra netinkamas (turi būti 5 arba 6 skaitmenys) „%s“ nėra tarp [%s] „%s“ parametras yra tuščias „%s“ parametras reikalauja nustatyti parametrą „%s“ „%s“ saugumas reikalauja „%s“ nustatymo „%s“ saugumas reikalauja „%s=%s“ „%s“ vertė neatitinka „%s=%s“ „%s=%s“ nesuderinama su „%s > 0“ „%s=%s“ yra netinkama „%s“ konfigūracija trūksta argumento "--order" (jokios) (nežinoma klaida) (nežinoma) ---[ Savybių meniu ]---
set      [<vertė>]               :: nustatyti naują vertę
add      [<vertė>]               :: pridėti naują parametrą ar savybę
change                           :: keisti dabartinę vertę
remove   [<indeksas> | <param>]  :: ištrinti vertę
describe                         :: apibūdinti savybę
print    [nustatymas | ryšys]    :: atspausdinti savybės (nustatymo/ryšio) vertę(es)
back                             :: pereiti į aukštesnį lygį
help/?   [<komanda>]             :: atspausdinti šią pagalbą arba komandos aprašymą
quit                             :: išeiti iš nmcli
 0 (JOKS) 0 (nėra) 802.1X 802.1X prašytojo konfigūracija nepavyko 802.1X prašytojas atjungtas 802.1X prašytojas dingo 802.1X prašytojui per ilgai užtruko patvirtinti tapatybę ===| nmcli interaktyvus ryšių redaktorius |=== A (5 GHz) Ryšio priklausomybė nepavyko Problema su RFC 2684 laidiniu per ADSL tiklų tiltą Bazinio ryšio antraeilis ryšys nepavyko ADSL ADSL ryšys APN Ad-Hoc Pridėti Pridedamas naujas „%s“ ryšys Adresai Leisti valdyti tinklo ryšius Galimos vertės „%s“ savybei: %s
 http(s) adresas interneto jungiamumui tikrinti Kas kartą klausti šio slaptažodžio AutoIP tarnybos klaida Nepavyko AutoIP tarnybos paleidimas AutoIP tarnybos nepavyko paleisti Galimos savybės: %s
 Galimi nustatymai: %s
 B/G (2.4 GHz) BOND BSSID Bluetooth Bluetooth įrenginio adresas Susieti Susieti ryšį %d Susiejimas Susieti ryšį %d Atsisakyti Pernešėjas/saitas pasikeitė Nepavyko užverti %s: %s
 Konfigūracijos aplanko vieta Konfigūracijos failo vieta Jungiamasi Ryšys „%s“ (%s) sėkmingai pridėtas.
 Ryšys "%s" (%s) sėkmingai ištrintas.
 Ryšys „%s“ (%s) sėkmingai pakeistas.
 Ryšys „%s“ (%s) sėkmingai įrašytas.
 Ryšys "%s" (%s) sėkmingai atnaujintas.
 Išsamesnė ryšio profilio informacija Dalijimasis ryšiu naudojant apsaugotą WiFi tinklą Dalijimasis ryšiu naudojant atvirą WiFi tinklą Ryšys sėkmingai aktyvuotas (D-Bus aktyvus kelias: %s)
 Ryšys su UUID „%s“ sukurtas ir aktyvuotas įrenginyje „%s“
 Slapukas Nepavyko tapri demonu: %s [klaida %u]
 Nepavyko iškoduoti asmeninio rakto. Nepavyko sugeneruoti atsitiktinių duomenų. Nepavyko įkelti failą "%s"
 Nepavyko konvertuoti slaptažodžio į UCS-2: %d Nepavyko iškoduoti PKCS#12 failo: %d Nepavyko iškoduoti PKCS#12 failo: %s Nepavyko iškoduoti PKCS#8 failo: %s Nepavyko iškoduoti liudijimo: %d Nepavyko iškoduoti liudijimo: %s Nepavyko inicializuoti PKCS#12 dekoderio: %d Nepavyko inicializuoti PKCS#12 dekoderio: %s Nepavyko inicializuoti PKCS#8 dekoderio: %s Nepavyko patvirtinti PKCS#12 failo: %d Nepavyko patvirtinti PKCS#12 failo: %s Dabartinė nmcli konfigūracija:
 DHCP kliento klaida DHCP klientas patyrė nesėkmę nepavyko paleisti DHCP kliento DNS serveriai Ištrinti Paskirties prievadas [8472] Įrenginys Įrenginys "%s" nerastas Įrenginio informacija Įrenginys atjungtas naudotojo arba kliento Įrenginys dabar yra valdomas Įrenginys dabar yra nevaldomas Ar norite taip pat išvalyti „%s“? [yes]:  Ar norite taip pat nustatyti „%s“ į „%s“? [yes]:  Nepanašu į PEM asmeninio rakto failą. Netapti demonu Netapti demonu ir rašyti žurnalą į stderr Nespausdinti nieko Klaida „%s“ vertė:  Keičiamas esamas „%s“ ryšys: „%s“ Įjungti PI [ne] Įjungti VNET antraštę [ne] Įjungti šifravimą [taip] Įjungti arba išjungti WiFi įrenginius Įjungti arba išjungti WiMAX mobiliojo plačiajuosčio ryšio įrenginius Įjungti arba išjungti mobiliojo plačiajuosčio ryšio įrenginius Įjungti arba išjungti sistemos prieigą prie tinklo Įveskite „%s“ vertę:  Įveskite DNS serverių IPv4 adresų sąrašą.

Pavyzdys: 8.8.8.8, 8.8.4.4
 Įveskite susiejamų parametrų sąrašą, suformatuotą taip:
  options = <vertė>, parametras = <vertė>,..
Galimi parametrai yra: %s
„mode“ gali būti pateiktas kaip pavadinimas arba skaičius:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

Pavyzdys: mode=2,miimon=120
 Įveskite baitus kaip šešioliktainių verčių sąrašą.
Galimi du formatai:
(a) šešioliktainių skaitmenų eilutė, kurie kiekviena skaitmenų pora reiškia vieną baitą
(b) tarpais skiriamas baitų sąrašas, parašytas kaip šešioliktianiai skaitmenys (su nebūtinais 0x/0X priešdėliu bei 0 pabaigoje).

Pavyzdžiai: ab0455a6ea3a74C2
            ab 4 55 0xa6 ea 3a 74 C2
 Įveskite ryšio tipą:  Klaida inicijuojant liudijimo duomenis: %s Klaida: %s
 Klaida: %s - tokio ryšio profilio nėra. Klaida: trūksta argumento %s. Klaida: %s Klaida: %s.
 Klaida: %s: %s. Klaida: trūksta argumento „%s“. Klaida: "%s" nėra aktyvus ryšys.
 Klaida: „%s“ yra netinkamas argumentas parinkčiai „%s“. Klaida: "%s": %s Klaida: „autoconnect“: %s. Klaida: „connection show“: %s Klaida: „device show“: %s Klaida: „device status“: %s Klaida: „device wifi“: %s Klaida: „general logging“: %s Klaida: „general permissions“: %s Klaida: netinkama „networking“ komanda „%s“. Klaida: būtinas argumentas „type“. Klaida: trūksta <nustatymo>.<savybės> argumento. Klaida: prieigos taškas su bssid „%s“ nerastas Klaida: tikėtasi argumento „%s“, bet pateiktas „%s“. Klaida: BSSID prisijungimui (%s) skiriasi nuo bssid argumento (%s). Klaida: nepavyko aktyvuoti ryšio: %s Klaida: nepavyko ištrinti ryšio: %s Klaida: „%s“ nėra WiFi įrenginys. Klaida: nerastas įrenginys „%s“. Klaida: Įrenginys "%s" nerastas.
 Klaida: nepavyko pridėti/aktyvuoti naujo ryšio: nežinoma klaida Klaida: Nepavyko įrašyti "%s" (%s) ryšio: %s
 Klaida: NetworkManager neveikia. Klaida: nerastas Wi-Fi įrenginys. Klaida: nerastas prieigos taškas su BSSID „%s“. Klaida: nepateikta argumentų. Klaida: nenurodytas ryšys. Klaida: nenurodyta sąsaja. Klaida: nerastas tinklas su SSID „%s“. Klaida: nežinoma parinktis „%s“, bandykite „nmcli -help“. Klaida: parinktis „--pretty“ yra tarpusavyje nesuderinama su „--terse“. Klaida: parinktis „--pretty“ nurodyta antrą kartą. Klaida: parinktis „--terse“ yra tarpusavyje nesuderinama su „--pretty“. Klaida: parinktis „--terse“ nurodyta antrą kartą. Klaida: parametras „%s“ nėra nei SSID, nei BSSID. Klaida: trūksta SSID arba BSSID. Klaida: baigėsi %d sek. laikas. Klaida: netikėtas argumentas „%s“ Klaida: nežinomas ryšys „%s“. Klaida: bssid argumento reikšmė „%s“ nėra tinkamas BSSID. Klaida: negalima ištrinti nežinomo ryšio(-ių): %s. Klaida: ryšio patikrinimas nesėkmingas: %s
 Klaida: papildomas argumentas neleidžiamas: „%s“. Klaida: nepavyko sukurti laikinojo failo %s. Klaida: nepavyko eksportuoti "%s": %s. Klaida: nepavyko importuoti "%s": %s. Klaida: nepavyko įkelti ryšio: %s. Klaida: nepavyko pakeisti %s.%s: %s. Klaida: nepavyko perskaityti laikinojo failo "%s": %s. Klaida: nepavyko iš naujo įkelti ryšius: %s. Klaida: nepavyko nustatyti „%s“ savybės: %s
 Klaida: netinkamas „%s“ argumentas: „%s“ (naudokite on/off). Klaida: netinkamas <nustatymas>.<savybė> „%s“. Klaida: neteisingas argumentas "%s"
 Klaida: nežinomas ryšio tipas; %s
 Klaida: netinkamas ryšio tipas; %s. Klaida: netinkamas papildomas argumentas „%s“. Klaida: netinkamas arba neleidžiamas nustatymas „%s“: %s. Klaida: netinkama savybė „%s“: %s. Klaida: netinkama savybė: %s
 Klaida: neteisinga savybė: %s%s
 Klaida: netinkama savybė: %s, nei teisingas nustatymo pavadinimas.
 Klaida: netinkamas nustatymo argumentas „%s“; tinkami yra [%s]
 Klaida: netinkamas nustatymo pavadinimas; %s
 Klaida: trūksta argumento parinkčiai „%s“. Klaida: trūksta nustatymo savybei „%s“
 Klaida: nepasirinktas nustatymas; galimi yra [%s]
 Klaida: rasti ne visi aktyvūs ryšiai. Klaida: gali būti pateiktas tik vienas iš „id“, uuid arba „path“. Klaida: openconnect nepavyko su signalu %d
 Klaida: openconnect nepavyko su būsena %d
 Klaida: openconnect nepavyko: %s
 Klaida: savybė %s
 Klaida: save-confirmation: %s
 Klaida: status-line: %s
 Klaida: ryšys nėra VPN. Klaida: nežinomas papildomas argumentas: "%s". Klaida: nežinomas nustatymas „%s“
 Klaida: nežinomas nustatymas: %s
 Klaida: „%s“ argumentui būtina vertė. Klaida: wep-key-type argumento vertė „%s“ netinkama, naudokite „key“ arba „phrase“. Laidinio tinklo įrenginys Nepavyko iššifruoti PKCS#8 asmeninio rakto. Nepavyko iškoduoti liudijimo. Nepavyko iššifruoti asmeninio rakto. Nepavyko iššifruoti asmeninio rakto: %d. Nepavyko iššifruoti asmeninio rakto: iššifruoti duomenys per dideli. Nepavyko iššifruoti asmeninio rakto: netikėtas užpildo ilgis. Nepavyko šifruoti: %d. Nepavyko baigti asmeninio rakto iššifravimo: %d. Nepavyko rasti laukiamos PKCS#8 pabaigos žymos „%s“. Nepavyko rasti laukiamos PKCS#8 pradžios žymos. Nepavyko inicijuoti MD5 konteksto: %d. Nepavyko inicijuoti šifravimo posistemės. Nepavyko inicijuoti šifravimo posistemės: %d. Nepavyko inicijuoti iššifravimo šifro lizdo. Nepavyko inicijuoti iššifravimo konteksto. Nepavyko inicijuoti šifravimo šifro lizdo. Nepavyko inicijuoti šifravimo konteksto. Nepavyko registruoti su prašomu tinklu Nepavyko pasirinkti nurodyto APN Nepavyko iššifravimui nustatyti IV. Nepavyko šifravimui nustatyti IV. Nepavyko iššifravimui nustatyti simetriško rakto. Nepavyko šifravimui nustatyti simetriško rakto. GRUPĖ Reikalingas GSM modemo SIM kortelės PIN Reikalingas GSM modemo SIM kortelės PUK GSM modemo SIM kortelė neįdėta Neteisinga GSM modemo SIM GVRP,  Tinklų sietuvas Grupės ID [nėra] Slėpti Nepavyko rezervuoti IP konfigūracijos (nėra prieinamo adreso, baigėsi skirtas laikas ir kt.) IPv4 adresas (IP[/plen]) [nėra] IPv4 tinklų sietuvas [nėra] IPv4 protokolas IPv6 adresas (IP[/plen]) [nėra] IPv6 protokolas IV yra ne šešioliktainių skaitmenų. IV ilgis turi būti lyginis baitų skaičius. InfiniBand InfiniBand P_Key ryšiui nenurodytas tėvinės sąsajos pavadinimas InfiniBand ryšys %d InfiniBand įrenginys nepalaiko ryšio veiksenos Infra Sąsaja(-os):  Sąsaja:  Netinkamas IV ilgis (turi būti bent %d). Netinkamas IV ilgis (turi būti bent %zd). Netinkamas konfigūracijos parametras „%s“; leidžiami [%s]
 Neteisingas Bluetooth įrenginio adresas. Netinkama parinktis.  Galimų parinkčių sąrašą galite pamatyti naudodami parametrą --help. LEAP PRARASTI_SUSIEJIMĄ,  Įskiepių sąrašas skiriant „,“ Vietinis adresas [nėra] Žurnalo domenai, skiriami „,“: bet kuri [%s] kombinacija Žurnalo lygis: vienas iš [%s] MACsec ryšys MKA CAK MKA_CKN MTU Paversti visus įspėjimus lemtingais Netinkamai suformuotas PEM failas: DEK-Info buvo ne antra gairė. Netinkamai suformuotas PEM failas: Proc-Type buvo ne pirma gairė. Netinkamai suformuotas PEM failas: DEK-Info gairėje netinkamas IV formatas. Netinkamai suformuotas PEM failas: DEK-Info gairėje nerasta IV. Netinkamai suformuotas PEM failas: nežinoma Proc-Type gairė „%s“. Netinkamai suformuotas PEM failas: nežinomas asmeninio rakto šifras „%s“. Veiksena Nepavyko inicializuoti modemo Modemas dabar yra pasiruošęs ir prieinamas Nėra ModemManager Keisti visų naudotojų tinklo ryšius Keisti pastovų sistemos vardą Keisti asmeninius tinklo ryšius Reikia nurodyti P_Key nurodant tėvą Neprieinama Trūksta įrenginiui reikalingos aparatinės programinės įrangos Tinklo registracija draudžiama Baigėsi tinklo registracijos laikas NetworkManager aktyvūs profiliai NetworkManager ryšio profiliai NetworkManager paleista NetworkManager sustabdyta NetworkManager žurnalai NetworkManager stebi visus tinklo ryšius ir automatiškai
pasirenka geriausią ryšį. Jis taip pat leidžia naudotojui
nurodyti belaidžio ryšio prieigos taškus, su kuriais susieti
jūsų kompiuterio belaidžio ryšio kortas. NetworkManager leidimai NetworkManager būsena NetworkManager užmigo Tinklai Networkmanager dabar yra '%s' būsenoje
 Naujas ryšio pavadinimas:  Nepavyko nustatyti pernešėjo Nėra skambinimo tono Nenurodyta priežastis Neieškoma tinklų Gerai Nepavyko atverti %s: %s
 Išvesties failo pavadinimas:  PCI PEM liudijime nėra pabaigos gairės „%s“. PEM liudijime nėra pradžios gairės „%s“. PEM rakto faile nėra pabaigos gairės „%s“. Nepavyko patikrinti PIN PPP patyrė nesėkmę PPP tarnyba atjungta nepavyko paleisti PPP tarnybos PPPoE PPPoE naudotojo vardas Slaptažodis privalo būti UTF-8 Slaptažodis:  Priešdėlis Parodyti NetworkManager versiją ir išeiti Asmeninio rakto šifras „%s“ nežinomas. Savybės pavadinimas? Įgaliotasis serveris Užmigdyti arba pažadinti NetworkManager (tai turėtų naudoti tik sistemos energijos valdymo posistemė) PERRIKIUOTI_ANTRAŠTES,  Radijo jungikliai Šalinti SCI prievadas [1] SSID SSID ilgis yra už ribū <1-32> baitai SSID arba BSSID:  Reikalingos bet nepateiktos paslaptys Tarnyba Nustatomas pavadinimas? Nepavyko bendro ryšio tarnybos paleidimas Bendro ryšio tarnybos nepavyko paleisti Rodyti Nurodykite PID failo vietą Būsenos failo vieta Įrenginių būsena Pavyko Sistemos politika neleidžia valdyti tinklo ryšių Sistemos politika neleidžia įjungti arba išjungti WiFi įrenginių Sistemos politika neleidžia įjungti arba išjungti WiMAX mobiliojo plačiajuosčio ryšio įrenginių Sistemos politika neleidžia įjungti arba išjungti mobiliojo plačiajuosčio ryšio įrenginių Sistemos politika neleidžia įjungti arba išjungti sistemos prieigos prie tinklo Sistemos politika neleidžia keisti tinklo nustatymų, skirtų visiems naudotojams Sistemos politika neleidžia keisti asmeninių tinklų nustatymų Sistemos politika neleidžia keisti įsiminto sistemos vardo Sistemos politika neleidžia užmigdyti arba pažadinti NetworkManager Sistemos politika nelaidžia dalytis ryšiais naudojant apsaugotą WiFi tinklą Sistemos politika neleidžia dalytis ryšiais naudojant atvirą WiFi tinklą Komanda Komandinis ryšys %d Bluetooth įrenginio ir ryšio adresai nesutapo. Bluetooth ryšys nepavyko arba baigėsi laikas IP konfigūracija nebeteisinga Nepavyko rasti Wi-Fi tinklo Nepavyko paruošti įrenginio konfigūravimui Įrenginys buvo pašalintas Įrenginio aktyvus ryšys dingo Įrenginio esamas ryšys buvo priimtas Mėginimas skambinti nepavyko Skambinimo užklausos laikas baigėsi Klaida negali būti ištaisyta automatiškai.
 Tikėtina atsakymo pradžia Intervalas tarp jungiamumo tikrinimų (sekundėmis) Linija užimta Nepavyko rasti modemo Prašytojas dabar yra prieinamas Laikas sekundėmis, kiek laukti ryšio (be parametro numatyta 30) Tun įrenginys Galimoms komandoms gauti rašykite „help“ arba „?“. USB Nepavyko nustatyti asmeninio rakto tipo. Netikėtas duomenų kiekis po šifravimo. Nežinoma Nežinomas komandos argumentas: „%s“
 Nežinoma komanda: „%s“
 Nežinoma klaida Nežinoma žurnalo sritis „%s“ Nežinomas žurnalo vedimo lygis „%s“ Nežinomas parametras: %s Nežinomas parametras: %s
 Naudojimas: nmcli agent { COMMAND | help }

COMMAND := { secret | polkit | all }

 Naudotojo ID [nėra] Naudotojo vardas VLAN VLAN ID (<0-4094>) VLAN ryšys VLAN ryšys %d VPN VPN prisijungta VPN jungiamasi VPN jungiamasi (gaunama IP konfigūracija) VPN jungiamasi (reikia nustatyti tapatybę) VPN jungiamasi (ruošiama) VPN ryšys VPN ryšys %d VPN prisijungti nepavyko VPN atsijungta VXLAN ID VXLAN ryšys Galimi ryšio tipai: %s
 Patikrinti ryšį: %s
 Patikrinti nustatymą „%s“: %s
 WEP 2 3 4 WPA1 WPA2 WWAN radijo jungiklis Laukia, kol NetworkManager baigs aktyvuoti pradinius tinklo ryšius. Įspėjimas: keičiamas esamas ryšys „%s“; „con-name“ argumento nepaisoma
 Įspėjimas: keičiamas esamas ryšys „%s“; „type“ argumento nepaisoma
 Wi-Fi radijo jungiklis Wi-Fi skenavimo sąrašas Nėra WiMAX NSP pavadinimas Laidinis ryšys %d Nepavyko rašymas į %s: %s
 Galite keisti šias savybes: %s
 Galite keisti šiuos nustatymus: %s
 [„%s“ nustatomos vertės]
 [NM savybės aprašymas] [nmcli specifinis aprašymas] aktyvuota aktyvuojama agent-owned,  miegantis auth automatinis back  :: pereiti į aukštesnį meniu lygmenį

 bluetooth ryšys change  :: pakeisti dabartinę vertę

Parodo dabartinę vertę ir leidžia ją redaguoti.
 prisijungta prisijungta (vietinis) prisijungta (tik puslapyje) jungiamasi jungiamasi (tikrinamas IP jungiamumas) jungiamasi (konfigūruojama) jungiamasi (gaunama IP konfigūracija) jungiamasi (reikia nustatyti tapatybę) jungiamasi (ruošiama) jungiamasi (paleidžiami antriniai prisijungimai) prisijungti nepavyko išjungtas deaktyvuojama numatyta describe  :: apibūdinti savybę

Parodo savybės aprašymą. Visus NM nustatymus ir savybes rasite nm-settings(5) žinyne.
 describe [<nustatymas>.<savybė>]  :: apibūdinti savybę

Parodo savybės aprašymą. Visus NM nustatymus ir savybes rasite nm-settings(5) vadove.
 įrenginys „%s“ nesuderinamas su ryšiu „%s“ įrenginys "%s" nėra suderinamas su ryšiu "%s": išjungta atjungtas atsijungiama nežinoma, kaip gauti savybės vertę įjungta laukas „%s“ turi būti vienintelis požymiai yra netinkami PKCS#12 turi atitikti „%s“ savybę help/? [<komanda>]  :: nmcli komandų pagalba

 help/? [<komanda>]  :: nmcli komandų pagalba

 indeksas „%d“ nėra rėžiuose <0-%d> indeksas „%d“ yra už ribų <0-%d> netinkamas „%s“ arba jo vertė „%s“ netinkamas IPv4 adresas „%s“ netinkamas IPv6 adresas „%s“ netinkamas parametras „%s“ netinkamas parametras „%s“ arba jo vertė „%s“ netinkamas priešdėlis '%s'; leidžiama <1-%d> netinkamas prioritetų susiejimas „%s“ nėra tinkamas MAC adresas %s %s macvlan ryšys trūksta būtino parametro „%s“ trūksta parametro niekada nmcli įrankis, versija %s
 ne nėra aktyvaus ryšio įrenginyje „%s“ nėra aktyvaus ryšio įrenginyje nerastas įrenginys ryšiui „%s“ nėra šalinamo elemento nėra šalinamo prioriteto jokia nėra failas (%s) netinkamas sąsajos pavadinimas nebūtina,  neįrašyta,  išjungta įjungta gali būti nustatyta tik viena iš „%s“ ir „%s“ ruošiama print [all]  :: atspausdinti nustatymo ar ryšio vertes

Parodo dabartinę savybę ar visą ryšį.

Pavyzdys: nmcli ipv4> print all
 print [savybė|nustatymas|ryšys]  :: atspausdinti savybės (nustatymo, ryšio) vertę(es)

Parodo savybės vertę. Pateikę argumentą taip pat galite pamatyti viso nustatymo ar ryšio vertes.
 prioritetas „%s“ yra netinkamas (<0-%ld>) savybė yra tuščia savybė netinkama trūksta savybės savybė yra nenurodyta ir nėra „%s:%s“ savybės vertė „%s“ yra tuščia arba per ilga (>64) quit  :: išeiti iš nmcli

Ši komanda išeina iš nmcli. Kai redaguojamas ryšys nėra įrašytas, naudotojo prašoma patvirtinti veiksmą.
 reikalauja nustatymo „%s“ arba „%s“ reikalauja „%s“ nustatymo ryšyje reikalauja nustatyti „%s“ savybę veikia set [<vertė>]  :: nustatyti naują vertę

Ši komanda šiai savybei nustato pateiktą <vertę>
 nustatymas nerastas šios savybės nustatymas reikalauja, kad savybė „%s“ būtų ne nulis savybės negalima keisti ši savybė neleidžiama „%s=%s“ neprieinamas nežinoma nežinomas ryšys "%s" nežinomas įrenginys "%s". nevaldomas pirmiausia naudokite „goto <nustatymas>“ arba „descrive <nustatymas>.<savybė>“
 pirmiausia naudokite „goto <nustatymas>“ arba „set <nustatymas>.<savybė>“
 vertė „%d“ yra už ribų <%d-%d> taip 