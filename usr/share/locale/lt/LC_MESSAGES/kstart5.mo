��          �   %   �      `  0   a  .   �  s  �  X   5     �     �  *   �     �  H   �     4  &   ;  S   b     �     �      �     �       8   0     i     z  /   �  -   �  S   �  *   C  *   n  �   �    1  0   B
  4   s
  e  �
  �        �     �  *   �     �  I   �     I  ,   P  M   }     �     �     �          ,  @   ?     �     �  .   �  )   �  �     8   �  7   �  �   	                                       	                                                          
                           (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) A regular expression matching the window title A string matching the window class (WM_CLASS property)
The window class can be found out by running
'xprop | grep WM_CLASS' and clicking on a window
(use either both parts separated by a space or only the right part).
NOTE: If you specify neither window title nor window class,
then the very first window to appear will be taken;
omitting both options is NOT recommended. Alternative to <command>: desktop file to start. D-Bus service will be printed to stdout Command to execute David Faure Desktop on which to make the window appear Iconify the window Jump to the window even if it is started on a 
different virtual desktop KStart Make the window appear on all desktops Make the window appear on the desktop that was active
when starting the application Matthias Ettrich Maximize the window Maximize the window horizontally Maximize the window vertically No command specified Optional URL to pass <desktopfile>, when using --service Richard J. Moore Show window fullscreen The window does not get an entry in the taskbar The window does not get an entry on the pager The window type: Normal, Desktop, Dock, Toolbar, 
Menu, Dialog, TopMenu or Override Try to keep the window above other windows Try to keep the window below other windows Utility to launch applications with special window properties 
such as iconified, maximized, a certain virtual desktop, a special decoration
and so on. Project-Id-Version: kstart
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-01-07 03:59+0100
PO-Revision-Date: 2011-08-05 18:44+0300
Last-Translator: Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.2
 (C) 1997-2000 Matthias Ettrich (ettrich@kde.org) Įprastoji išraiška, atitinkanti lango pavadinimą Eilutė, atitinkanti lango klasę (WM_CLASS savybė)
Lango klasė gali būti aptikta įvykdžius
'xprop | grep WM_CLASS' ir paspaudus pele langą
(naudokite arba abi dalis atskirtas tarpu, arba tik dešinę dalį).
PASTABA: jei nenurodysite nei lango tipo, nei lango klasės,
bus paimtas pirmasis pasirodęs langas;
praleisti abi parinktis NEREKOMENDUOJAMA. Alternatyva <command>: darbalaukio failas, kurį reikia paleisti. D-Bus tarnybos pranešimai bus nukreipiami į stdoutia bus spausdinambu Komanda vykdymui David Faure Darbalaukis, kuriame turi atsirasti langas Paversti langą į ženkliuką Peršokti į langą net jeigu jis paleistas kitame 
menamame darbalaukyje KStart Langas turi atsirasti visuose darbalaukiuose Langas turi atsirasti darbalaukyje, kuris buvo aktyvus
paleidžiant programą Matthias Ettrich Išdidinti langą Išdidinti langą horizontaliai Išdidinti langą vertikaliai Nenurodyta komanda Neprivalomas URL, perduodamas <desktopfile>, naudojant --service Richard J. Moore Rodyti langą visame ekrane Langas neturės pozicijos užduočių juostoje Langas neturės pozicijos puslapiuotojuje Lango tipas: Normal (normalus), Desktop (darbalaukis), Dock (pritvirtintas),
Tool (įrankis), Menu, Dialog, TopMenu ar Override (turėti viršų) Bandyti priversti langą visada būti virš kitų langų Bandyti priversti langą visada būti po kitais langais Pagalbinė programa, skirta programų paleidimui su specialiomis 
lango savybėmis, tokiomis kaip sutrauktas į ženkliuką, išdidintas, tam tikrame menamame
darbalaukyje, su specialia dekoracija ir t.t. 