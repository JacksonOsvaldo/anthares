��    $      <  5   \      0     1     6  )   J     t  $   �  &   �  #   �  !   �  "   !     D     T     f     z     �  J   �     �  L        [     c     k      {     �  
   �     �     �     �     �     �  "   �       )   9  <   c     �  ;   �     �         (	     -	  %   6	     \	  "   j	  $   �	  '   �	  (   �	  )   
  	   -
  
   7
  	   B
  	   L
     V
  c   b
  '   �
  W   �
  
   F     Q     Z     p  	        �     �     �  
   �     �  	   �     �     �  ,   �  C         d     k     p     	                                                                         !       #             "                                        
           $                               100% @info:creditAuthor @info:creditCopyright 2015 Harald Sitter @info:creditHarald Sitter @labelNo Applications Playing Audio @labelNo Applications Recording Audio @labelNo Device Profiles Available @labelNo Input Devices Available @labelNo Output Devices Available @labelProfile: @titlePulseAudio @title:tabAdvanced @title:tabApplications @title:tabDevices Add virtual output device for simultaneous output on all local sound cards Advanced Output Configuration Automatically switch all running streams when a new output becomes available Capture Default Device Profiles EMAIL OF TRANSLATORSYour emails Inputs Mute audio NAME OF TRANSLATORSYour names Notification Sounds Outputs Playback Port Port is unavailable (unavailable) Port is unplugged (unplugged) Requires 'module-gconf' PulseAudio module This module allows to set up the Pulseaudio sound subsystem. label of stream items%1: %2 only used for sizing, should be widest possible string100% volume percentage%1% Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-03 03:00+0200
PO-Revision-Date: 2017-06-25 01:04+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 100% Autorius Autorinės teisės 2015 Harald Sitter Harald Sitter Nėra garsą grojančių programų Nėra garsą įrašančių programų Nėra pasiekiamų įrenginių profilių Nėra pasiekiamų įvesties įrenginių. Nėra pasiekiamų išvesties įrenginių. Profilis: PulseAudio Išsamiau Programos Įrenginiai Pridėti virtualų išvedimo įrenginį vienalaikiam išvedimui visose vietinėse garso plokštėse Sudėtingesnė išvedimo konfigūracija Kai atsiranda naujas išvedimo įrenginys, automatiškai perjungti visus esamus srautus Įrašymas Numatyta Įrenginių profiliai liudas@akmc.lt Įvedimas Nutildyti garsą Liudas Ališauskas Pranešimų garsai Išvedimas Grojimas Prievadas  (nėra)  (atjungta) Reikia PulseAudio modulio „module-gconf“ Šis modulis leidžia sukonfigūruoti PulseAudio garso subsistemą. %1: %2 100% %1% 