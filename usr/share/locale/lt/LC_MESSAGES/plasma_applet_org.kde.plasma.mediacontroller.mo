��          �            x     y  !   �     �     �     �  @   �     *  $   ?     d     k  "   �     �  %   �     �     �               ,      F     g     n     }     �     �     �  
   �     �     �     �     �     �                        
                                    	                   Artist of the songby %1 Artist of the songby %1 (paused) Choose player automatically General No media playing Open player window or bring it to the front if already openOpen Pause playbackPause Pause playback when screen is locked Paused Play next trackNext Track Play previous trackPrevious Track Quit playerQuit Remaining time for song e.g -5:42-%1 Start playbackPlay Stop playbackStop Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-06-06 03:09+0200
PO-Revision-Date: 2017-06-25 01:28+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 autorius %1 autorius %1 (pristabdyta) Automatiškai parinkti leistuvę Bendra Niekas negroja Atverti Pristabdyti Pristabdyti užrakinant ekraną Pristabdyta Kita daina Ankstesnė daina Baigti -%1 Groti Stabdyti 