��    +      t  ;   �      �  (   �     �  �   �     z     �     �     �     �     �     �     �     �     �     �                @     H     V     d     v     �     �     �     �     �     �          	          .     A     N     ^     s      |  7   �     �     �     �                   *	     7	     ;	  	   Q	     [	     d	     ~	     �	     �	     �	     �	     �	     �	  
   
  )   
     8
     G
  $   N
     s
     �
     �
  "   �
     �
  "   �
          "     3     F     R     r  !   �     �     �     �            .   *     Y  #   j     �  
   �     �                       %   *   +      !   "                                                            
                (                             )      &      #      '                             $   	    %1 is the name of a session%1 (Wayland) ... @info The argument is the list of available sizes (in pixel). Example: 'Available sizes: 24' or 'Available sizes: 24, 36, 48'(Available sizes: %1) Advanced Author Auto &Login Background: Clear Image Commands Could not decompress archive Cursor Theme: Customize theme Default Description Download New SDDM Themes EMAIL OF TRANSLATORSYour emails General Get New Theme Halt Command: Install From File Install a theme. Invalid theme package Load from file... Login screen using the SDDM Maximum UID: Minimum UID: NAME OF TRANSLATORSYour names Name No preview available Reboot Command: Relogin after quit Remove Theme SDDM KDE Config SDDM theme installer Session: The default cursor theme in SDDM The theme to install, must be an existing archive file. Theme Unable to install theme Uninstall a theme. User User: Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:43+0200
PO-Revision-Date: 2017-06-25 01:07+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1 (Wayland) ... (Galimi dydžiai: %1) Išsamiau Autorius Automatinis prisijungimas Fonas: Išvalyti paveikslėlį Komandos Nepavyksta išpakuoti archyvo Žymeklio apipavidalinimas: Tinkinti apipavidalinimą Numatyta Aprašymas Parsisiųsti naujus SDDM apipavidalinimus liudas@akmc.lt Bendri Parsisiųsti naują apipavidalinimą Sustabdymo komanda: Įdiegti iš failo Įdiegti apipavidalinimą. Netinkamas apipavidalinimo paketas Įkelti iš failo... Prisijungimo langas naudojant SDDM Didžiausias UID: Mažiausias UID: Liudas Ališauskas Pavadinimas Atleiskite, peržiūra negalima Paleidimo iš naujo komanda: Pakartotinai jungtis po išėjimo Pašalinti apipavidalinimą SDDM KDE konfigūracija SDDM apipavidalinimų diegyklė Sesija: Numatyta žymeklio SDDM tema Įdiegtinas apipavidalinimas archyvo pavidalu. Apipavidalinimas Nepavyksta įdiegti apipavidalinimo Pašalinti apipavidalinimą. Naudotojas Naudotojas: 