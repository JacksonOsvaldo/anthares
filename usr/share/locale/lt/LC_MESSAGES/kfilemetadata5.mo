��    A      $  Y   ,      �     �     �     �     �     �     �     �                     /     @     U     e     u     �     �     �     �     �     �     �     �     	          0     C     R     ^     k     x     �     �     �     �      �          /     O     j     �  #   �     �     �     	  "   #	     F	  $   f	     �	     �	     �	     �	     
  3    
     T
     m
  &   �
  <   �
  !   �
  8     0   E  !   v      �  +   �  �  �     �     �  
   �  
   	               #     1  
   9     D     R     e  
   t          �     �     �     �     �     �     �     �  	   �     �               -     2     :     F     L     S     f     |     �     �  !   �     �          #     7  "   T     w     �     �  $   �     �  "   �     !     5  "   J     m     �     �     �     �     �     �     �     �               .     7                         .   6   7   9                  !   '                 <   &            :   /       	   A                           +         (   $   )       "   4          #                  0   3       
   =   ?      *      ,       5      2   1                ;          -                 >   8       %      @            @labelAlbum Artist @labelArchive @labelArtist @labelAspect Ratio @labelAudio @labelAuthor @labelBitrate @labelChannels @labelComment @labelComposer @labelCopyright @labelCreation Date @labelDocument @labelDuration @labelFrame Rate @labelHeight @labelImage @labelKeywords @labelLanguage @labelLyricist @labelPage Count @labelPresentation @labelPublisher @labelRelease Year @labelSample Rate @labelSpreadsheet @labelSubject @labelText @labelTitle @labelVideo @labelWidth @label EXIFImage Date Time @label EXIFImage Make @label EXIFImage Model @label EXIFImage Orientation @label EXIFPhoto Aperture Value @label EXIFPhoto Exposure Bias @label EXIFPhoto Exposure Time @label EXIFPhoto F Number @label EXIFPhoto Flash @label EXIFPhoto Focal Length @label EXIFPhoto Focal Length 35mm @label EXIFPhoto GPS Altitude @label EXIFPhoto GPS Latitude @label EXIFPhoto GPS Longitude @label EXIFPhoto ISO Speed Rating @label EXIFPhoto Metering Mode @label EXIFPhoto Original Date Time @label EXIFPhoto Saturation @label EXIFPhoto Sharpness @label EXIFPhoto White Balance @label EXIFPhoto X Dimension @label EXIFPhoto Y Dimension @label date of template creation8Template Creation @label music albumAlbum @label music genreGenre @label music track numberTrack Number @label number of fuzzy translated stringsDraft Translations @label number of linesLine Count @label number of translatable stringsTranslatable Units @label number of translated stringsTranslations @label number of wordsWord Count @label translation authorAuthor @label translations last updateLast Update Project-Id-Version: l 10n
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-10-17 03:36+0200
PO-Revision-Date: 2015-01-28 00:12+0200
Last-Translator: Liudas Ališauskas <liudas@akmc.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Albumo atlikėjas Archyvas Atlikėjas Proporcija Audio Autorius Bitų dažnis Kanalai Komentaras Kompozitorius Autorinės teisės Sukūrimo data Dokumentas Trukmė Kadrų dažnis Aukštis Paveikslėlis Raktažodžiai Kalba Žodžių autorius Puslapių skaičius Pristatymas Leidėjas Išleidimo metai Kadrų dažnis Skaičiuoklė Tema Tekstas Pavadinimas Video Plotis Paveikslėlio data Paveikslėlio modelis Paveikslėlio modelis Paveikslėlio orientacija Nuotraukos diafragmos reikšmė Nuotraukos  ekspozicijos paklaida Nuotraukos išlaikymo trukmė Nuotraukos F skaičius Nuotraukos blykstė Nuotraukos židinio nuotolis Nuotraukos židinio nuotolis 35 mm Nuotraukos GPS aukštis Nuotraukos GPS platuma Nuotraukos GPS ilguma Nuotraukos ISO greičio įvertinimas Nuotraukos matavimo veiksena Nuotraukos originalus datos laikas Nuotraukos sodrumas Nuotraukos ryškumas Nuotraukos baltos spalvos balansas Nuotraukos X išmatavimas Nuotraukos Y išmatavimas Šablono kūrimas Albumas Žanras Takelio numeris Juodraštiniai vertimai Eilučių skaičius Verstini vienetai Vertimai Žodžių skaičius Autorius Paskutinis atnaujinimas 