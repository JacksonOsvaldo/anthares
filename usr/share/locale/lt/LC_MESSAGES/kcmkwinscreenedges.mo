��             +         �     �     �     �  	   �     �               '     3     H     a     t     �  S   �  w   �     `  M   u      �     �     �  	             2     K  %   Z     �     �  #   �     �     �     �     	     *     .     0  
   H     S     l     �  
   �     �  #   �     �     �     	  p    	  c   �	     �	  G   
     S
     b
     u
     �
     �
     �
     �
  /   �
       	     '   "     J     e     v                                            
                                                     	                                                ms % %1 - All Desktops %1 - Cube %1 - Current Application %1 - Current Desktop %1 - Cylinder %1 - Sphere &Reactivation delay: &Switch desktop on edge: Activation &delay: Activity Manager Always Enabled Amount of time required after triggering an action until the next trigger can occur Amount of time required for the mouse cursor to be pushed against the edge of the screen before the action is triggered Application Launcher Change desktop when the mouse cursor is pushed against the edge of the screen EMAIL OF TRANSLATORSYour emails Lock Screen NAME OF TRANSLATORSYour names No Action Only When Moving Windows Open krunnerRun Command Other Settings Quarter tiling triggered in the outer Show Desktop Switch desktop on edgeDisabled Toggle alternative window switching Toggle window switching Window Management of the screen Project-Id-Version: kcmkwinscreenedges
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-30 03:13+0100
PO-Revision-Date: 2017-06-25 01:17+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
  ms % %1 - Visus darbalaukius %1 - Kubas %1 - Dabartinė programa %1 - Dabartinis darbalaukis %1 - Cilindras %1 - Sfera &Reaktyvavimo uždelsimas: &Perjungti darbalaukį pakraštyje: Aktyvavimo už&laikymas: Veiklų tvarkytuvė Visuomet įjungta Laiko tarpas, kurį pelės žymeklis turi prabūti ekrano pakraštyje, prieš iššaukiant pakartotinį veiksmą Laiko tarpas, kurį pelės žymeklis turi prabūti ekrano pakraštyje, prieš iššaukiant veiksmą Programų paleidiklis Pakeisti darbalaukį, kai pelės žymeklis nustumiamas į ekrano kampą liudas@akmc.lt Užrakinti ekraną Liudas Ališauskas Veiksmas nepriskirtas Tik perkeliant langus Vykdyti komandą Kiti nustatymai Ketvirtinis išdėstymas iššaukiamas pasiekus Rodyti darbalaukį Išjungta Įjungti alternatyvų lango perjungimą Įjungti lango perjungimą Langų tvarkymas ekrano krašto 