��            )   �      �     �     �     �     �     �     �            !   $     F  !   [     }     �     �     �     �     �     �     �     �     �  2     @   >  	     4   �  8   �  $   �          *    .     G     P     `  '   m  $   �     �     �     �  (   �          &     C     K     X  	   s     }     �     �     �  #   �     �  0   	  <   B	  	   	     �	     �	  .   �	     �	     �	     
                                                             	                                                                       &Execute All Widgets Categories: Desktop Shell Scripting Console Download New Plasma Widgets Editor Executing script at %1 Filters Install Widget From Local File... Installation Failure Installing the package %1 failed. Load New Session Open Script File Output Running Runtime: %1ms Save Script File Screen lock enabled Screen saver timeout Select Plasmoid File Sets the minutes after which the screen is locked. Sets whether the screen will be locked after the specified time. Templates Toolbar Button to switch to KWin Scripting ModeKWin Toolbar Button to switch to Plasma Scripting ModePlasma Unable to load script file <b>%1</b> Uninstallable Use Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-12-23 03:23+0100
PO-Revision-Date: 2016-11-04 11:47+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Vykdyti Visi valdikliai Kategorijos: Darbalaukio terpės scenarijų konsolė Atsisiųsti naujų plazma valdiklių Redaktorius Vykdomas scenarijus, laikas: %1 Filtrai Įdiegti valdiklį iš vietinio failo... Diegimo klaida Paketo %1 diegimas nepavyko. Įkelti Nauja sesija Atverti scenarijaus failą Išvestis Veikia Vykdymo laikas: %1ms Įrašyti scenarijaus failą Ekrano užraktas įgalintas Ekrano užsklandos skirtasis laikas Pasirinkti Plasmoid failą Nustato, po kiek minučių ekranas užrakinamas. Galite nustatyti, ar ekranas užsirakins po tam tikro laiko. Šablonai KWin Plasma Nepavyksta įkelti scenarijaus failo <b>%1</b> Pašalinti negalima Naudoti 