��    7      �  I   �      �     �     �     �     �     �     �               $     -     5     H     T      `     �     �     �     �     �     �     �     �                     )     7  	   C  	   M     W     d     j     �     �     �     �     �     �     �     �     �     �  @   �     7     @     \     c     j     r     �     �     �  4   �     �    �     
     
     
      
     :
     I
     O
     c
     u
  
   ~
     �
     �
     �
     �
     �
     �
      �
     �
               !     7     G     [     n     �     �     �     �  
   �     �     �     �  
   �       
          
   /     :     L     Q     Y  :   f     �  !   �  	   �     �     �      �       	   
               &     &   0      7                  #                
          	   '   (   +                       6              3      1                   ,   2                  "       *          $      4   )               %      5                     !          /   .   -       % %1 is value, %2 is unit%1 %2 %1: Application Energy Consumption Battery Capacity Charge Percentage Charge state Charging Current Degree Celsius°C Details: %1 Discharging EMAIL OF TRANSLATORSYour emails Energy Energy Consumption Energy Consumption Statistics Environment Full design Fully charged Has power supply Kai Uwe Broulik Last 12 hours Last 2 hours Last 24 hours Last 48 hours Last 7 days Last full Last hour Manufacturer Model NAME OF TRANSLATORSYour names No Not charging PID: %1 Path: %1 Rechargeable Refresh Serial Number Shorthand for WattsW System Temperature This type of history is currently not available for this device. Timespan Timespan of data to display Vendor VoltV Voltage Wakeups per second: %1 (%2%) WattW Watt-hoursWh Yes current power draw from the battery in WConsumption literal percent sign% Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:20+0100
PO-Revision-Date: 2016-08-30 16:53+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 % %1 %2 %1: Programos energijos sunaudojimas Akumuliatorius Talpa Įkrovimo procentai Įkrovimo būklė Kraunasi Dabartinis °C Informacija: %1 Išsikrauna liudas@akmc.lt Energija Energijos sunaudojimas Energijos sunaudojimo statistika Aplinka Pilno dizainas Pilnai pakrautas Turi maitinimo bloką Kai Uwe Broulik Paskutinių 12 val. Paskutinės 2 val. Paskutinės 24 val. Paskutinės 48 val. Paskutinės 7 dienos Paskiausiai pilnas Paskutinę valandą Gamintojas Modelis Liudas Ališauskas Ne Nesikrauna PID: %1 Kelias: %1 Iš naujo pakraunamas Atnaujinti Serijinis numeris Vat. Sistema Temperatūra Šio tipo istorija šiuo metu negalima Jūsų įrenginiui. Laiko tarpas Laiko tarpas duomenų vaizdavimui Tiekėjas V Įtampa Pabudimų per sekundę: %1 (%2%) Vat. Vat. Val. Taip Sunaudojimas % 