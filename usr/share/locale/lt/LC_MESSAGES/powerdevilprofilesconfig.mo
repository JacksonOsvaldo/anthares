��    	      d      �       �       �           !  
   -     8     G  �   `  �   (  
  �     �     �     �     �            �   9  v        	                                         EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names On AC Power On Battery On Low Battery Restore Default Profiles The KDE Power Management System will now generate a set of defaults based on your computer's capabilities. This will also erase all existing modifications you made. Are you sure you want to continue? The Power Management Service appears not to be running.
This can be solved by starting or scheduling it inside "Startup and Shutdown" Project-Id-Version: powerdevilprofilesconfig
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-05-09 03:03+0200
PO-Revision-Date: 2012-01-15 10:18+0200
Last-Translator: Donatas G. <dgvirtual@akl.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.2
 andrius@stikonas.eu Andrius Štikonas Išorinis maitinimas Iš akumuliatoriaus Akumuliatorius senka Atkurti numatytus profilius KDE energijos valdymo sistema dabar sukurs keletą numatytųjų nustatymų pagal tai, ką gali jūsų kompiuteris. Tai taipogi panaikins visus esamus jūsų atliktus pakeitimus. Ar tikrai norite tęsti? Energijos valdymo tarnyba atrodo neveikia.
Ši problema gali būti išspręsta „Paleidimo ir išjungimo“ modulyje. 