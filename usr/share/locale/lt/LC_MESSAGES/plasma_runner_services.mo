��          D      l       �      �   6   �   a   �      /  $  ?  	   d  A   n     �     �                          Applications Finds applications whose name or description match :q: Jump list search result, %1 is action (eg. open new tab), %2 is application (eg. browser)%1 - %2 System Settings Project-Id-Version: plasma_runner_services
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2017-11-14 03:09+0100
PO-Revision-Date: 2016-08-30 17:30+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Programos Randa programas, kurių pavadinimas arba aprašymas atitinka :q:. %1 - %2 Sistemos nuostatos 