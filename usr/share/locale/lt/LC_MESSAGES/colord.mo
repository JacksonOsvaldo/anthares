��    6      �  I   |      �     �     �  !   �     �     �  
   �     �               '     7     I     Z  	   l     v          �     �      �     �     �     �               5     >     D     P     X     ^     f  
   n  	   y     �  	   �  $   �     �     �     �          $     :     W     m  (   �     �      �     �  '   �          "     '     /  �  7     
     *
  +   =
     i
     �
     �
     �
     �
     �
     �
     �
     �
            
   !  	   ,  "   6     Y  (   v     �     �     �  !   �     �          !     )  
   8  
   C     N     \     e     q     ~     �  $   �  -   �      �               :     U     o  -   �  ,   �     �  '   �     %  +   -     Y     e  	   k     u                   !                    $          +   "                             -         4      &              *             	              %              0   )   ,      '   3              .          
      2      5       6   #   /         (   1               %B %e %Y, %I:%M:%S %p CRT Clear any metadata in the profile Client version: Color Colorspace Create a device Create a profile Created Daemon version: Debugging Options Deletes a device Deletes a profile Device ID Embedded Enabled Enables or disables the device Exit after a small delay Exit after the engine has loaded Export the tag data Filename Format Get a standard colorspace Locks the color sensor Metadata Model Object Path Options Owner Printer Profile Profile ID Projector Sensor Sensor ID Sets extra properties on the profile Sets one or more sensor options Sets the copyright string Sets the description string Sets the device kind Sets the device model Sets the manufacturer string Sets the model string Show client and daemon versions Show debugging information for all files Show debugging options Show extra debugging information State There are no supported sensors attached Title Type Unknown Warning Project-Id-Version: colord
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-21 09:32+0100
PO-Revision-Date: 2017-08-21 08:32+0000
Last-Translator: Richard Hughes <richard@hughsie.com>
Language-Team: Lithuanian (http://www.transifex.com/freedesktop/colord/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
 %Y %m %e, %H:%M:%S CRT (Kineskopinis) Išvalyti profilyje bet kokius metaduomenis Kliento programos versija: Spalva Spalvų erdvė Sukurti įrenginį Sukurti profilį Sukurtas Tarnybos versija: Derinimo parametrai Ištrina įrenginį Ištrina profilį Įrenginio ID Įtaisytas Įjungtas Įjungia arba išjungia įrenginį Išeiti po nedidelės delsos Išeiti po to, kai modulis buvo įkeltas Eksportuoti žymės duomenis Failo pavadinimas Formatas Gauti standartinę spalvų erdvę Užrakina spalvos jutiklį Metaduomenys Modelis Objekto kelias Parametrai Savininkas Spausdintuvas Profilis Profilio ID Projektorius Jutiklis Jutiklio ID Nustato profiliui papildomas savybes Nustato vieną ar daugiau jutiklio parametrų Nustato autorių teisių eilutę Nustato aprašo eilutę Nustato įrenginio rūšį Nustato įrenginio modelį Nustato gamintojo eilutę Nustato modelio eilutę Rodyti kliento programos ir tarnybos versijas Rodyti derinimo informaciją visiems failams Rodyti derinimo parametrus Rodyti papildomą derinimo informaciją Būsena Nėra prijungta jokių palaikomų jutiklių Pavadinimas Tipas Nežinoma Įspėjimas 