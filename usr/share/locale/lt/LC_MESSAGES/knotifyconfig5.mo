��          �      ,      �  V  �     �  -   �     &     4     H     V     c     }     �     �     �     �     �  !   �  !       4  W  8     �  
   �     �  (   �     �               8  )   R     |     �     �     �     �     �                           
                       	                            <qt>Specifies how Jovie should speak the event when received.  If you select "Speak custom text", enter the text in the box.  You may use the following substitution strings in the text:<dl><dt>%e</dt><dd>Name of the event</dd><dt>%a</dt><dd>Application that sent the event</dd><dt>%m</dt><dd>The message sent by the application</dd></dl></qt> Configure Notifications Description of the notified eventDescription Log to a file Mark &taskbar entry Play a &sound Run &command Select the command to run Select the sound to play Show a message in a &popup Sp&eech Speak Custom Text Speak Event Message Speak Event Name State of the notified eventState Title of the notified eventTitle Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2014-03-16 20:46+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 <qt>Nurodo, kaip Jovie turėtų ištarti gautą įvykį. Jei pasirinksite „Ištarti savadarbį tekstą“, įveskite tekstą į langelį. Tekste galite naudoti šiuos keitinius:<dl><dt>%e</dt><dd>Įvykio pavadinimas</dd><dt>%a</dt><dd>Programa, kuri išsiuntė įvykį</dd><dt>%m</dt><dd>Pranešimas, kurį išsiuntė programa</dd></dl></qt> Konfigūruoti pranešimus Aprašymas Rašyti žurnalą į failą Pažymėti &užduočių juostos įrašą Groti &garsą Vykdyti &komandą Pasirinkite komandą vykdymui Parinkite garsą grojimui Rodyti pranešimą &pasirodančiame lange Kal&ba Ištarti savadarbį tekstą Ištarti įvykio pranešimą Ištarti įvykio pavadinimą Būsena Pavadinimas 