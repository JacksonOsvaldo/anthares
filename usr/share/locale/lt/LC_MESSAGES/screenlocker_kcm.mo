��          �      �       H     I  
   a     l  .   r     �     �      �  *   �        ,   '  ,   T  0   �     �    �     �     �       /        <     E  $   X  .   }  )   �     �     �  >        A     	                                              
              &Lock screen on resume: Activation Error Failed to successfully test the screen locker. Immediately Lock Session Lock screen automatically after: Lock screen when waking up from suspension Re&quire password after locking: Spinbox suffix. Short for minutes min  mins Spinbox suffix. Short for seconds sec  secs The global keyboard shortcut to lock the screen. Wallpaper &Type: Project-Id-Version: trunk-kf 5
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-10 05:10+0100
PO-Revision-Date: 2017-06-25 01:49+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 Pabudus užra&kinti ekraną: Aktyvavimas Klaida Nepavyko sėkmingai išbandyti ekrano užrakto. Tuoj pat Užrakinti sesiją Užrakinti ekraną automatiškai po: Užrakina ekraną, kai pabundama po sustabdymo Reikalauti slaptažodžio po užrakinimo:  min  min  min  min  sek.  sek.  sek.  sek. Globalus klaviatūros greitasis klavišas ekrano užrakinimui. Apmušalo &tipas: 