��          �            x     y  -   �     �     �     �     �     �          /     J     R     d     x  !   �  !   �  �  �  6   �     �  "   �  H        [     p  B   �  :   �  ;        G  ,   S  3   �  -   �     �     �                           	                                     
               Configure Notifications Description of the notified eventDescription Log to a file Mark &taskbar entry Play a &sound Run &command Select the command to run Select the sound to play Show a message in a &popup Sp&eech Speak Custom Text Speak Event Message Speak Event Name State of the notified eventState Title of the notified eventTitle Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-01-27 10:39+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 Конфигурирање на известувања Опис Запиши во датотека Обележи елемент во &лентата со програми Пушти &звук Изврши &наредба Изберете ја наредбата за извршување Изберете го звукот за емитување Прикажи порака во &скок-прозорец Г&овор Изговори сопствен текст Изговори порака за настанот Изговори име на настанот Состојба Наслов 