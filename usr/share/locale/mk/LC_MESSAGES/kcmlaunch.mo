��          �      �       H     I     N  �  k  ^  �  P   Z     �     �     �     �     �               5  �  K     .  T   6    �  �  �
  �   q     Z     z     �  R   �  "     *   +  T   V  A   �                                          
      	                  sec &Startup indication timeout: <H1>Taskbar Notification</H1>
You can enable a second method of startup notification which is
used by the taskbar where a button with a rotating hourglass appears,
symbolizing that your started application is loading.
It may occur, that some applications are not aware of this startup
notification. In this case, the button disappears after the time
given in the section 'Startup indication timeout' <h1>Busy Cursor</h1>
KDE offers a busy cursor for application startup notification.
To enable the busy cursor, select one kind of visual feedback
from the combobox.
It may occur, that some applications are not aware of this startup
notification. In this case, the cursor stops blinking after the time
given in the section 'Startup indication timeout' <h1>Launch Feedback</h1> You can configure the application-launch feedback here. Blinking Cursor Bouncing Cursor Bus&y Cursor Enable &taskbar notification No Busy Cursor Passive Busy Cursor Start&up indication timeout: Taskbar &Notification Project-Id-Version: kcmlaunch
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2006-05-21 13:14+0200
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
  сек Истек на време на индикацијата за &стартување: <H1>Известување во лентата со програми</H1>
Може да овозможите втор вид на известување за стартување кое се
користи од лентата со програми. Таму се појавува копче со ротирачки песочен часовник,
кое симболизира дека стартуваната апликација се вчитува.
Може да се случи некои апликации да не се свесни за ова известување.
Во тој случај копчето исчезнува после времето
зададено во делот „Истек на време на индикацијата за стартување“ <h1>Зафатен курсор</h1>
KDE нуди приказ на зафатен курсор за известување при стартување на апликации.
За да го овозможите зафатениот курсор изберете еден вид на повратна визуелна информација од комбо полето.
Може да се случи некои апликации да не се свесни за ова известување.
Во тој случај курсорот престанува да трепка после времето
зададено во делот „Истек на време на индикацијата за стартување“ <h1>Повратна информација за стартување</h1> Тука може да ја конфигурирате повратната информација од стартувањето на апликациите. Трепкачки курсор Скокачки курсор З&афатен курсор Овозможи &известување во лентата со програми Без зафатен курсор Пасивен зафатен курсор Истек на време на индикацијата за &стартување: Известување во &лентата со програми 