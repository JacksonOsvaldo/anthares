��          |      �          &   !  +   H     t  	   |     �     �     �     �  (   �     �        �    L   �  [   D  
   �     �  ?   �             &   !  Z   H  0   �     �     	                                    
                  Do you want to suspend to RAM (sleep)? Do you want to suspend to disk (hibernate)? General Hibernate Hibernate (suspend to disk) Leave... Lock Lock the screen Logout, turn off or restart the computer Sleep (suspend to RAM) Suspend Project-Id-Version: plasma_applet_lockout
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-23 03:02+0100
PO-Revision-Date: 2010-01-31 23:25+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 Дали сакате да суспендирате во RAM (спиење)? Дали сакате да суспендирате на диск (хибернација)? Општо Хибернирај Хибернација (суспендирање на диск) Напушти... Заклучи Го заклучува екранот Се одјавува, го гаси или го рестартира компутерот Спиење (суспендирање во RAM) Суспендирај 