��          �            x  �   y  m   �  `   i     �     �     �     �           3     R     W     h  ?   u  Y   �  +     �  ;  �     �     �   �     w  F   �     �  2   �     		     "	     B	  (   I	  !   r	  v   �	  �   
  a   �
        	                                                  
                     <qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This will delete all the files installed by this theme.</qt> <qt>You cannot delete the theme you are currently using.<br />You have to switch to another theme first.</qt> A theme named %1 already exists in your icon theme folder. Do you want replace it with this one? Confirmation Cursor Settings Changed Description Drag or Type Theme URL EMAIL OF TRANSLATORSYour emails NAME OF TRANSLATORSYour names Name Overwrite Theme? Remove Theme The file %1 does not appear to be a valid cursor theme archive. Unable to download the cursor theme archive; please check that the address %1 is correct. Unable to find the cursor theme archive %1. Project-Id-Version: kcminput
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:21+0100
PO-Revision-Date: 2010-01-29 00:39+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 <qt>Дали навистина сакате да ја отстраните темата на курсори <i>%1</i>?<br />Ова ќе ги избрише сите датотеки инсталирани од оваа тема.</qt> <qt>Не може да ја избришете темата што тековно ја користите.<br />Прво треба да се префрлите на друга тема.</qt> Во вашата папка со теми на икони веќе постои тема наречена %1. Дали сакате да ја замените со оваа? Потврда Поставувањата за курсорот се изменети Опис Довлечи или впиши URL на тема bobibobi@freemail.com.mk Божидар Проевски Име Да запишам врз темата? Отстранување тема Изгледа дека датотеката %1 не е валидна архива со теми на курсори. Не може да се симне архивата со теми на курсори; ве молиме проверете дали е точна адресата %1. Не може да ја пронајдам архивата со теми на курсори %1. 