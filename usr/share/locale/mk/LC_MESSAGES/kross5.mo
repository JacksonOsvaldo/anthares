��            )   �      �  &   �     �     �     �     �      �               .     6  ,   S  3   �     �     �     �     �     �  '        4     S     Y     o     �     �     �     �     �  &   �     �  �  �  
   �  %   �               ,     >     W  6   j     �  8   �  \   �  ]   E  L   �  @   �     1	     C	     O	  G   g	     �	     �	  -   �	  3   
     9
  <   J
     �
  .   �
     �
  X   �
     %                                       
                      	                                                                            @title:group Script propertiesGeneral Add a new script. Add... Cancel? Comment: EMAIL OF TRANSLATORSYour emails Edit Edit selected script. Edit... Execute the selected script. Failed to create script for interpreter "%1" Failed to determine interpreter for scriptfile "%1" Failed to load interpreter "%1" Failed to open scriptfile "%1" File: Icon: Interpreter: Level of safety of the Ruby interpreter NAME OF TRANSLATORSYour names Name: No such function "%1" No such interpreter "%1" Remove Remove selected script. Run Scriptfile "%1" does not exist. Stop Stop execution of the selected script. Text: Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2016-11-19 20:12+0100
PO-Revision-Date: 2010-01-27 10:39+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 Општо Додава нова скрипта. Додај... Откажи? Коментар: bobibobi@freemail.com.mk Уредување Ја уредува избраната скрипта. Уреди... Ја извршува избраната скрипта. Не може да се креира скрипта за интерпретерот „%1“ Не успеав да одредам интерпретер за скриптата „%1“ Не успеав да го вчитам интерпретерот „%1“ Не може да се отвори скриптата „%1“ Датотека: Икона: Интерпретер: Ниво на безбедност на Ruby-интерпретерот Божидар Проевски Име: Нема таква функција „%1“ Нема таков интерпретер „%1“ Отстрани Ја отстранува избраната скрипта. Изврши Скриптата „%1“ не постои. Стоп Го прекинува извршувањето на избраната скрипта. Текст: 