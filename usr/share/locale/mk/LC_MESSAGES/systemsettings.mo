��          �   %   �      p  A   q     �  /   �     �               -     <     C  	   P     Z  	   p     z      �  *   �     �  	   �     �  
   �     �       /   +     [     r  b   �  	   �     �  �  �  e   �     M  u   d     �  "   �  4     %   :  
   `     k     x  9   �     �     �     �  
   	  
   	     	  0   9	     j	     {	     �	     �	  *   �	  )   �	  �   
     �
     �
                                                                          	                                 
                %1 is an external application and has been automatically launched (c) 2009, Ben Cooksley <i>Contains 1 item</i> <i>Contains %1 items</i> About %1 About Active Module About System Settings Apply Settings Author Ben Cooksley Configure Configure your system Developer Dialog EMAIL OF TRANSLATORSYour emails General config for System SettingsGeneral Help Icon View Keyboard Shortcut: %1 Maintainer Mathias Soeken NAME OF TRANSLATORSYour names Search through a list of control modulesSearch Show detailed tooltips System Settings The settings of the current module have changed.
Do you want to apply the changes or discard them? Tree View Will Stephenson Project-Id-Version: systemsettings
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-02-07 06:08+0100
PO-Revision-Date: 2010-02-01 00:06+0100
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 %1 е надворешна апликација и беше автоматски стартувана (c) 2009, Ben Cooksley <i>Содржи %1 елемент</i> <i>Содржи %1 елементи</i> <i>Содржи %1 елементи</i> За %1 За активниот модул За „Системски поставувања“ Примени поставувања Автор Ben Cooksley Конфигурација Конфигурирање на Вашиот систем Развивач Дијалог bobibobi@freemail.com.mk Општо Помош Приказ со икони Кратенка на тастатурата: %1 Одржувач Mathias Soeken Божидар Проевски Пребарување Прикажи детални совети Системски поставувања Поставувањата во активниот модул беа сменети.
Дали сакате да ги примените или да ги отфрлите? Приказ со стебло Will Stephenson 