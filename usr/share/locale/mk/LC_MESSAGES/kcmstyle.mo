��            )   �      �  &   �  �   �     O     g     n     w     �     �     �     �      �  	   �  �   �  c   �          %     1     P     j     r          �     �  	   �  C   �  j   �  E   P     �     �  �  �  &   �  1  �     �	  
   
     
     8
     L
     i
     �
     �
     �
      �
  �  �
  �   �     J     g     s  #   �     �     �     �     �     �       �     �   �  �   k  5     4   O            	                                                                                
                                             (c) 2002 Karol Szwed, Daniel Molkentin <h1>Style</h1>This module allows you to modify the visual appearance of user interface elements, such as the widget style and effects. @title:tab&Fine Tuning Button Checkbox Combobox Con&figure... Configure %1 Daniel Molkentin Description: %1 EMAIL OF TRANSLATORSYour emails Group Box Here you can choose from a list of predefined widget styles (e.g. the way buttons are drawn) which may or may not be combined with a theme (additional information like a marble texture or a gradient). If you enable this option, KDE Applications will show small icons alongside some important buttons. KDE Style Module Karol Szwed NAME OF TRANSLATORSYour names No description available. Preview Radio button Ralf Nolden Tab 1 Tab 2 Text Only There was an error loading the configuration dialog for this style. This area shows a preview of the currently selected style without having to apply it to the whole desktop. This page allows you to choose details about the widget style options Unable to Load Dialog Widget style: Project-Id-Version: kcmstyle
Report-Msgid-Bugs-To: http://bugs.kde.org
POT-Creation-Date: 2018-01-17 05:44+0100
PO-Revision-Date: 2009-06-21 20:33+0200
Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : 2;
 (c) 2002 Karol Szwed, Daniel Molkentin <h1>Стил</h1>Овој модул ви овозможува да го промените визуелниот изглед на елементите на корисничкиот интерфејс, како што се стиловите и ефектите на графичките контроли. &Фино нагодување Копче Поле за избор Комбо поле Кон&фигурирај... Конфигурирај %1 Daniel Molkentin Опис: %1 bobibobi@freemail.com.mk Поле за групирање Тука од листата може да изберете претходно дефинирани стилови на графичките контроли (пр. начинот на кој се цртаат копчињата) што можат но и не мора да бидат комбинирани со тема (дополнителни информации како мермерна текстура или градиенти). Ако ја овозможите оваа опција, KDE апликациите ќе покажуваат мали икони покрај некои важни копчиња. KDE Модул за стил Karol Szwed Божидар Проевски Нема достапен опис. Преглед Радио копче Ralf Nolden Ливче 1 Ливче 2 Само текст Имаше грешка при вчитувањето на дијалогот за конфигурација за овој стил. Оваа област прикажува преглед на тековно избраниот стил, без да има потреба тој да се примени на целата површина. Оваа страница ви овозможува да изберете детали за опциите за стиловите на графичките контроли Не може да се вчита дијалогот Стил на графичките контроли: 