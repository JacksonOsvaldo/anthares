# importando os modulos do chatbot


from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot

import os

import speech_recognition as sr

bot = ChatBot('elise')

bot.set_trainer(ListTrainer) # define o treinamento do bot

for _file in os.listdir('chats'): # percorrer todos os arquivos em chats
	lines = open('chats/' + _file, 'r').readlines() # lendo as linhas

	bot.train(lines)
