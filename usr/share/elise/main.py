# importando os modulos do chatbot
from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot

import os

import speech_recognition as sr

bot = ChatBot('elise', read_only=True)

r = sr.Recognizer()

with sr.Microphone() as s:
	r.adjust_for_ambient_noise(s)

	while True:
		try:
			audio = r.listen(s)

			speech = r.recognize_google(audio, language='pt')
			
			cmd = speech

			if cmd == 'Abra a calculadora':
				os.system("gnome-calculator &")
			elif cmd == 'Abra o navegador':
				print("abrindo...")
				os.system("firefox &")
			elif cmd == 'Quero compactar um arquivo':
				print("abrindo...")
				os.system("file-roller &")
			elif cmd == 'Toque uma musica':
				print("abrindo...")
				os.system("vlc &")
			elif cmd == 'Abra o bloco de notas':
				print("abrindo...")
				os.system("gedit &")
			elif cmd == 'Abra o programa de torrents':
				print("abrindo...")
				os.system("transmission &")
			elif cmd == 'Abra o gerenciador de partições':
				print("abrindo...")
				os.system("gparted &")
			elif cmd == 'Abra o gerenciador de tarefas':
				print("abrindo...")
				os.system("gnome-system-monitor &")
			elif cmd == 'Abra o gerenciador de pacotes':
				print("abrindo...")
				os.system("synaptic &")
				
			else:
				print('\n')
				print('Você: ', speech)
				print('-----------------------------------------------')
				response = bot.get_response(speech)
				print('Élise: ', response)
				
				
		except:
			print('Audio não reconhecido')

