<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `Iter` struct in crate `collections`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, Iter">

    <title>collections::slice::Iter - Rust</title>

    <link rel="stylesheet" type="text/css" href="../../normalize.css">
    <link rel="stylesheet" type="text/css" href="../../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../../main.css">
    

    <link rel="shortcut icon" href="https://doc.rust-lang.org/favicon.ico">
    
</head>
<body class="rustdoc struct">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../../collections/index.html'><img src='https://www.rust-lang.org/logos/rust-logo-128x128-blk-v2.png' alt='logo' width='100'></a>
        <p class='location'>Struct Iter</p><div class="block items"><ul><li><a href="#methods">Methods</a></li><li><a href="#implementations">Trait Implementations</a></li></ul></div><p class='location'><a href='../index.html'>collections</a>::<wbr><a href='index.html'>slice</a></p><script>window.sidebarCurrent = {name: 'Iter', ty: 'struct', relpath: ''};</script><script defer src="sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content">
<h1 class='fqn'><span class='in-band'>Struct <a href='../index.html'>collections</a>::<wbr><a href='index.html'>slice</a>::<wbr><a class="struct" href=''>Iter</a></span><span class='out-of-band'><span class='since' title='Stable since Rust version 1.0.0'>1.0.0</span><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a class='srclink' href='../../src/core/slice/mod.rs.html#1371-1375' title='goto source code'>[src]</a></span></h1>
<pre class='rust struct'>pub struct Iter&lt;'a, T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: 'a,&nbsp;</span> { /* fields omitted */ }</pre><div class='docblock'><p>Immutable slice iterator</p>

<p>This struct is created by the <a href="../../std/primitive.slice.html#method.iter"><code>iter</code></a> method on <a href="../../std/primitive.slice.html">slices</a>.</p>

<h1 id='examples' class='section-header'><a href='#examples'>Examples</a></h1>
<p>Basic usage:</p>

<pre class="rust rust-example-rendered">
<span class="comment">// First, we declare a type which has `iter` method to get the `Iter` struct (&amp;[usize here]):</span>
<span class="kw">let</span> <span class="ident">slice</span> <span class="op">=</span> <span class="kw-2">&amp;</span>[<span class="number">1</span>, <span class="number">2</span>, <span class="number">3</span>];

<span class="comment">// Then, we iterate over it:</span>
<span class="kw">for</span> <span class="ident">element</span> <span class="kw">in</span> <span class="ident">slice</span>.<span class="ident">iter</span>() {
    <span class="macro">println</span><span class="macro">!</span>(<span class="string">&quot;{}&quot;</span>, <span class="ident">element</span>);
}</pre>
</div>
                    <h2 id='methods' class='small-section-header'>
                      Methods<a href='#methods' class='anchor'></a>
                    </h2>
                <h3 id='impl' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1391-1434' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.as_slice' class="method"><span id='as_slice.v' class='invisible'><code>fn <a href='#method.as_slice' class='fnname'>as_slice</a>(&amp;self) -&gt; &amp;'a [T]</code></span><span class='out-of-band'><div class='ghost'></div><div class='since' title='Stable since Rust version 1.4.0'>1.4.0</div><a class='srclink' href='../../src/core/slice/mod.rs.html#1417-1419' title='goto source code'>[src]</a></span></h4>
<div class='docblock'><p>View the underlying data as a subslice of the original data.</p>

<p>This has the same lifetime as the original slice, and so the
iterator can continue to be used while this exists.</p>

<h1 id='examples-1' class='section-header'><a href='#examples-1'>Examples</a></h1>
<p>Basic usage:</p>

<pre class="rust rust-example-rendered">
<span class="comment">// First, we declare a type which has the `iter` method to get the `Iter`</span>
<span class="comment">// struct (&amp;[usize here]):</span>
<span class="kw">let</span> <span class="ident">slice</span> <span class="op">=</span> <span class="kw-2">&amp;</span>[<span class="number">1</span>, <span class="number">2</span>, <span class="number">3</span>];

<span class="comment">// Then, we get the iterator:</span>
<span class="kw">let</span> <span class="kw-2">mut</span> <span class="ident">iter</span> <span class="op">=</span> <span class="ident">slice</span>.<span class="ident">iter</span>();
<span class="comment">// So if we print what `as_slice` method returns here, we have &quot;[1, 2, 3]&quot;:</span>
<span class="macro">println</span><span class="macro">!</span>(<span class="string">&quot;{:?}&quot;</span>, <span class="ident">iter</span>.<span class="ident">as_slice</span>());

<span class="comment">// Next, we move to the second element of the slice:</span>
<span class="ident">iter</span>.<span class="ident">next</span>();
<span class="comment">// Now `as_slice` returns &quot;[2, 3]&quot;:</span>
<span class="macro">println</span><span class="macro">!</span>(<span class="string">&quot;{:?}&quot;</span>, <span class="ident">iter</span>.<span class="ident">as_slice</span>());</pre>
</div></div>
            <h2 id='implementations' class='small-section-header'>
              Trait Implementations<a href='#implementations' class='anchor'></a>
            </h2>
        <h3 id='impl-TrustedLen' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/iter/traits/trait.TrustedLen.html" title="trait core::iter::traits::TrustedLen">TrustedLen</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-TrustedLen' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1449' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'></div><h3 id='impl-Clone' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/clone/trait.Clone.html" title="trait core::clone::Clone">Clone</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-Clone' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1452-1454' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.clone' class="method"><span id='clone.v' class='invisible'><code>fn <a href='../../core/clone/trait.Clone.html#tymethod.clone' class='fnname'>clone</a>(&amp;self) -&gt; <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1453' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-AsRef&lt;[T]&gt;' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/convert/trait.AsRef.html" title="trait core::convert::AsRef">AsRef</a>&lt;[T]&gt; for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-AsRef&lt;[T]&gt;' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><div class='since' title='Stable since Rust version 1.13.0'>1.13.0</div><a class='srclink' href='../../src/core/slice/mod.rs.html#1457-1461' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.as_ref' class="method"><span id='as_ref.v' class='invisible'><code>fn <a href='../../core/convert/trait.AsRef.html#tymethod.as_ref' class='fnname'>as_ref</a>(&amp;self) -&gt; &amp;[T]</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1458-1460' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-Debug' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../collections/fmt/trait.Debug.html" title="trait collections::fmt::Debug">Debug</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: 'a + <a class="trait" href="../../collections/fmt/trait.Debug.html" title="trait collections::fmt::Debug">Debug</a>,&nbsp;</span></code><a href='#impl-Debug' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><div class='since' title='Stable since Rust version 1.9.0'>1.9.0</div><a class='srclink' href='../../src/core/slice/mod.rs.html#1378-1384' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.fmt' class="method"><span id='fmt.v' class='invisible'><code>fn <a href='../../collections/fmt/trait.Debug.html#tymethod.fmt' class='fnname'>fmt</a>(&amp;self, f: &amp;mut <a class="struct" href="../../collections/fmt/struct.Formatter.html" title="struct collections::fmt::Formatter">Formatter</a>) -&gt; <a class="enum" href="../../core/result/enum.Result.html" title="enum core::result::Result">Result</a>&lt;(), <a class="struct" href="../../collections/fmt/struct.Error.html" title="struct collections::fmt::Error">Error</a>&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1379-1383' title='goto source code'>[src]</a></span></h4>
<div class='docblock'><p>Formats the value using the given formatter.</p>
</div></div><h3 id='impl-Send' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/marker/trait.Send.html" title="trait core::marker::Send">Send</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../../core/marker/trait.Sync.html" title="trait core::marker::Sync">Sync</a>,&nbsp;</span></code><a href='#impl-Send' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1389' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'></div><h3 id='impl-FusedIterator' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/iter/traits/trait.FusedIterator.html" title="trait core::iter::traits::FusedIterator">FusedIterator</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-FusedIterator' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1446' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'></div><h3 id='impl-Iterator' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-Iterator' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1128-1226' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='associatedtype.Item' class="type"><span id='Item.t' class='invisible'><code>type <a href='../../core/iter/iterator/trait.Iterator.html#associatedtype.Item' class="type">Item</a> = &amp;'a T</code></span></h4>
<h4 id='method.next' class="method"><span id='next.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#tymethod.next' class='fnname'>next</a>(&amp;mut self) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;&amp;'a T&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1132-1145' title='goto source code'>[src]</a></span></h4>
<h4 id='method.size_hint' class="method"><span id='size_hint.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.size_hint' class='fnname'>size_hint</a>(&amp;self) -&gt; (usize, <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;usize&gt;)</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1148-1151' title='goto source code'>[src]</a></span></h4>
<h4 id='method.count' class="method"><span id='count.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.count' class='fnname'>count</a>(self) -&gt; usize</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1154-1156' title='goto source code'>[src]</a></span></h4>
<h4 id='method.nth' class="method"><span id='nth.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.nth' class='fnname'>nth</a>(&amp;mut self, n: usize) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;&amp;'a T&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1159-1162' title='goto source code'>[src]</a></span></h4>
<h4 id='method.last' class="method"><span id='last.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.last' class='fnname'>last</a>(self) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;&amp;'a T&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1165-1167' title='goto source code'>[src]</a></span></h4>
<h4 id='method.all' class="method"><span id='all.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.all' class='fnname'>all</a>&lt;F&gt;(&amp;mut self, predicate: F) -&gt; bool <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="../../core/ops/function/trait.FnMut.html" title="trait core::ops::function::FnMut">FnMut</a>(&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>) -&gt; bool,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1169-1179' title='goto source code'>[src]</a></span></h4>
<h4 id='method.any' class="method"><span id='any.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.any' class='fnname'>any</a>&lt;F&gt;(&amp;mut self, predicate: F) -&gt; bool <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="../../core/ops/function/trait.FnMut.html" title="trait core::ops::function::FnMut">FnMut</a>(&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>) -&gt; bool,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1181-1185' title='goto source code'>[src]</a></span></h4>
<h4 id='method.find' class="method"><span id='find.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.find' class='fnname'>find</a>&lt;F&gt;(&amp;mut self, predicate: F) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="../../core/ops/function/trait.FnMut.html" title="trait core::ops::function::FnMut">FnMut</a>(&amp;&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>) -&gt; bool,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1187-1197' title='goto source code'>[src]</a></span></h4>
<h4 id='method.position' class="method"><span id='position.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.position' class='fnname'>position</a>&lt;F&gt;(&amp;mut self, predicate: F) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;usize&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="../../core/ops/function/trait.FnMut.html" title="trait core::ops::function::FnMut">FnMut</a>(&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>) -&gt; bool,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1199-1211' title='goto source code'>[src]</a></span></h4>
<h4 id='method.rposition' class="method"><span id='rposition.v' class='invisible'><code>fn <a href='../../core/iter/iterator/trait.Iterator.html#method.rposition' class='fnname'>rposition</a>&lt;F&gt;(&amp;mut self, predicate: F) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;usize&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="../../core/ops/function/trait.FnMut.html" title="trait core::ops::function::FnMut">FnMut</a>(&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>) -&gt; bool,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1213-1225' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-Sync' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/marker/trait.Sync.html" title="trait core::marker::Sync">Sync</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;T: <a class="trait" href="../../core/marker/trait.Sync.html" title="trait core::marker::Sync">Sync</a>,&nbsp;</span></code><a href='#impl-Sync' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1387' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'></div><h3 id='impl-ExactSizeIterator' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/iter/traits/trait.ExactSizeIterator.html" title="trait core::iter::traits::ExactSizeIterator">ExactSizeIterator</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-ExactSizeIterator' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1439-1443' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.is_empty' class="method"><span id='is_empty.v' class='invisible'><code>fn <a href='../../core/iter/traits/trait.ExactSizeIterator.html#method.is_empty' class='fnname'>is_empty</a>(&amp;self) -&gt; bool</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1440-1442' title='goto source code'>[src]</a></span></h4>
</div><h3 id='impl-DoubleEndedIterator' class='impl'><span class='in-band'><code>impl&lt;'a, T&gt; <a class="trait" href="../../core/iter/traits/trait.DoubleEndedIterator.html" title="trait core::iter::traits::DoubleEndedIterator">DoubleEndedIterator</a> for <a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt;</code><a href='#impl-DoubleEndedIterator' class='anchor'></a></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1229-1258' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.next_back' class="method"><span id='next_back.v' class='invisible'><code>fn <a href='../../core/iter/traits/trait.DoubleEndedIterator.html#tymethod.next_back' class='fnname'>next_back</a>(&amp;mut self) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;&amp;'a T&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1231-1244' title='goto source code'>[src]</a></span></h4>
<h4 id='method.rfind' class="method"><span id='rfind.v' class='invisible'><code>fn <a href='../../core/iter/traits/trait.DoubleEndedIterator.html#method.rfind' class='fnname'>rfind</a>&lt;F&gt;(&amp;mut self, predicate: F) -&gt; <a class="enum" href="../../core/option/enum.Option.html" title="enum core::option::Option">Option</a>&lt;&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>&gt; <span class="where fmt-newline">where<br>&nbsp;&nbsp;&nbsp;&nbsp;F: <a class="trait" href="../../core/ops/function/trait.FnMut.html" title="trait core::ops::function::FnMut">FnMut</a>(&amp;&lt;<a class="struct" href="../../collections/slice/struct.Iter.html" title="struct collections::slice::Iter">Iter</a>&lt;'a, T&gt; as <a class="trait" href="../../core/iter/iterator/trait.Iterator.html" title="trait core::iter::iterator::Iterator">Iterator</a>&gt;::<a class="type" href="../../core/iter/iterator/trait.Iterator.html#associatedtype.Item" title="type core::iter::iterator::Iterator::Item">Item</a>) -&gt; bool,&nbsp;</span></code></span><span class='out-of-band'><div class='ghost'></div><a class='srclink' href='../../src/core/slice/mod.rs.html#1246-1256' title='goto source code'>[src]</a></span></h4>
</div></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../../";
        window.currentCrate = "collections";
    </script>
    <script src="../../main.js"></script>
    <script defer src="../../search-index.js"></script>
</body>
</html>