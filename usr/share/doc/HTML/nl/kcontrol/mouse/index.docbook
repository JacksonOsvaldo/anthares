<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % Dutch "INCLUDE"
> <!-- change language only here -->
]>

<article id="mouse" lang="&language;">
<articleinfo>
<title
>Muis</title>
<authorgroup>
<author
>&Mike.McBride; &Mike.McBride.mail;</author>
<author
>&Brad.Hards; &Brad.Hards.mail;</author>
&Niels.Reedijk;&Sander.Koning; 
</authorgroup>

<date
>2016-06-01</date>
<releaseinfo
>Plasma 5.6</releaseinfo>

<abstract>
<para
>Dit is de documentatie voor de module &systemsettings; die muizen en andere aanwijsapparaten instelt. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Systeeminstellingen</keyword>
<keyword
>muis</keyword>
</keywordset>
</articleinfo>

<sect1 id="kcm_mouse">
<title
>Muis</title>

<para
>Deze module geeft u de mogelijkheid om uw aanwijsapparaat te configureren. Uw aanwijsapparaat kan een muis, een trackball, een touchpad of andere hardware zijn die een soortgelijke functie heeft.</para>

<para
>Deze module is opgedeeld in diverse tabbladen:<link linkend="mouse-general"
>Algemeen</link
>, <link linkend="mouse-advanced"
>Geavanceerd</link
> en <link linkend="mouse-navigation"
>Muisnavigatie</link
>. </para>

<sect2 id="mouse-general">
<title
>Algemeen</title>

<variablelist>
<varlistentry>
<term
><guilabel
>Knopvolgorde</guilabel
></term>
<listitem
><para
>Als u linkshandig bent dan hebt u misschien liever dat de muisknoppen <mousebutton
>links</mousebutton
> en <mousebutton
>rechts</mousebutton
> omgedraaid zijn. Dat kunt u veranderen door <quote
>Linkshandig</quote
> te selecteren. Als uw aanwijsapparaat meer dan twee knoppen heeft, worden alleen de knoppen die functioneren als <mousebutton
>links</mousebutton
> en <mousebutton
>rechts</mousebutton
> beïnvloed. Bijvoorbeeld als u een drieknopsmuis hebt, heeft dit geen invloed op de knop <mousebutton
>midden</mousebutton
>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Schuifrichting omdraaien</guilabel
></term>
<listitem
><para
>Als deze optie geselecteerd is, zal het schuifwiel (als u dat hebt) in omgekeerde richting werken. Als bijvoorbeeld eerst het wiel naar u toe draaien een verschuiving naar beneden veroorzaakte, zal de verschuiving nu naar boven zijn. Dit kan nuttig zijn om met een ongebruikelijke instelling van de X-server te werken. </para>
</listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Dubbelklik om bestanden en mappen te openen (eerste klik selecteert ze)</guilabel
></term>
<listitem>
<para
>Als deze optie niet is geselecteerd, worden pictogrammen/bestanden geopend door een enkele klik met de <mousebutton
>linker</mousebutton
> muisknop. Dit standaardgedrag is consistent met wat u kunt verwachten wanneer u op een koppeling klikt in de meeste browsers. Als dit is geselecteerd, worden bestanden/pictogrammen geopend met een dubbele klik terwijl een enkelklik het bestand of pictogram alleen maar selecteert. Dit is het gedrag dat u misschien kent van andere bureaubladen of besturingssystemen.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Enkele klik opent bestanden en mappen</guilabel
></term>
<listitem>
<para
>Dit is de standaardinstelling. U hoeft maar één keer op een pictogram te klikken om het te openen. Om het te selecteren kunt u met de muis rond het pictogram slepen, met <keycombo action="simul"
>&Ctrl;<mousebutton
>rechts</mousebutton
></keycombo
> klikken of alleen klikken en de knop ingedrukt houden.</para>
</listitem>
</varlistentry>
</variablelist>
</sect2>

<sect2 id="mouse-advanced">
<title
>Geavanceerd</title>

<variablelist>
<varlistentry>
<term id="peripherals-mouse-acceleration">
<guilabel
>Aanwijzerversnelling</guilabel
></term>
<listitem>
<para
>Deze optie geeft u de mogelijkheid om de relatie tussen de afstand die de muis aflegt over het scherm en de relatieve verplaatsing van het fysieke apparaat zelf (dat een muis, een trackball, of een ander apparaat kan zijn) in te stellen.</para>

<para
>Een hoge waarde voor de cursorversnelling zal leiden tot een grote verplaatsing van de muiscursor over het scherm zelfs wanneer u met het fysieke apparaat maar kleine bewegingen maakt.</para>

<tip
><para
>Een versnallingsfactor tussen <guilabel
>1x</guilabel
> en <guilabel
>3x</guilabel
> werkt het beste op de meeste systemen. Met een factor die boven de <guilabel
>3x</guilabel
> ligt, kan de muis erg moeilijk te besturen zijn.</para
></tip>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Acceleratiedrempel</guilabel
></term>
<listitem>
<para
>De drempel is de kleinste afstand die de muis over het scherm moet bewegen voor de acceleratie ook maar enig effect heeft. als de verplaatsing binen de grens ligt dan zal de muis zich gedragen alsof de versnelling op <guilabel
>1x</guilabel
> staat.</para>

<para
>Daarom zal wanneer u kleine bewegingen met het fysieke apparaat (&bijv; de muis) maakt u steeds een goede controle over de muiscursor op het scherm hebben, terwijl grotere bewegingen van het fysieke apparaat de muiscursor snel naar de verschillende delen van het scherm brengen.</para>

<para
>U kunt de drempelwaarde veranderen door een waarde in het bewerkingsvakje te zetten of door op de omhoog en omlaag pijlen te klikken.</para>

<tip
><para
>In het algemeen geldt dat hoe hoger u de <guilabel
>Aanwijzerversnelling</guilabel
> instelt, hoe hoger u de <guilabel
>Acceleratiedrempel</guilabel
> wilt instellen. Bijvoorbeeld een <guilabel
>Acceleratiedrempel</guilabel
> met een waarde van 4 pixels kan geschikt zijn voor een <guilabel
>Aanwijzerversnelling</guilabel
> van 2x, maar 10 pixels is misschien beter voor 3x.</para
></tip>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Dubbelklikinterval</guilabel
></term>
<listitem>
<para
>Dit is het maximum hoeveelheid tijd tussen twee klikken om deze te herkennen als een dubbelklik. Als u twee keer klikt en de tijd tussen deze twee klikken is kleiner dan deze waarde, dan wordt dit als een dubbelklik herkend. Als de tijd tussen deze twee klikken groter is dan deze waarde dan zal dit herkent worden als twee <emphasis
>aparte</emphasis
> enkele klikken.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Starttijd voor verslepen</guilabel
> en <guilabel
>Startafstand voor verslepen</guilabel
></term>

<listitem>
<para
>Als u <itemizedlist
> <listitem
><para
>klikt met de muis</para
></listitem
> <listitem
><para
>versleept met de muis binnen de tijd die aangegeven is in <guilabel
>Starttijd voor verslepen</guilabel
> en </para
></listitem
> <listitem
><para
>over een afstand verplaatst die groter is dan het aantal pixels aangegeven in <guilabel
>Startafstand voor verslepen</guilabel
></para
> </listitem
> </itemizedlist
> dan zal het geselecteerde pictogram versleept worden.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Muiswiel scrollt per</guilabel
></term>
<listitem>
<para
>Als u een muis heeft met een wieltje, gebruik dan het spinvak om aan te geven hoeveel regels tekst een <quote
>stap</quote
> van het muiswiel zal doorbladeren.</para>
</listitem>
</varlistentry>

</variablelist>

</sect2>

<sect2 id="mouse-navigation">
<title
>Muisnavigatie</title>

<para
>Met dit tabblad kunt u het numerieke toetsenblok van uw toetsenbord als een muisachtig apparaat instellen. Dit kan nuttig zijn als u op een apparaat werkt zonder ander aanwijsapparaat, of als u het numerieke toetsenblok nergens anders voor nodig hebt. </para>

<variablelist>

<varlistentry>
<term
><guilabel
>Muis met toetsenbord verplaatsen (via het numerieke toetsenblok)</guilabel
></term>
<listitem>
<para
>Om verplaatsing met het toetsenbord mogelijk te maken, dient u de optie <guilabel
>Muis met toetsenbord verplaatsen (via het numerieke toetsenblok)</guilabel
> in te schakelen. Als u dit doet, worden de andere instellingen geactiveerd, en kunt u het gedrag van het toetsenbord verder instellen, indien nodig. </para>
<para
>De toetsen op het numerieke blok bewegen in de richting die u zou verwachten. Merk op dat u naast omhoog, omlaag, links en rechts ook diagonaal kunt verplaatsen. De toets <keycap
>5</keycap
> bootst een muisklik na, typisch met de &LMB;. Met welke knop de klik wordt nagebootst, stelt u in met de toetsen <keycap
>/</keycap
> (&LMB;), <keycap
>*</keycap
> key (&MMB;) en <keycap
>-</keycap
> (&RMB;). <keycap
>+</keycap
> emuleert een dubbelklik op de geselecteerde muisknop. U kunt de toets <keycap
>0</keycap
> gebruiken om vasthouden na te bootsen (voor eenvoudig slepen) en dan de toets <keycap
>.</keycap
> om loslaten van de knop na te bootsen. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Versnellingsdrempel</guilabel
></term>
<listitem>
<para
>Dit is de tijd (in milliseconden) tussen de eerste toetsaanslag en de eerste herhaalde beweging voor toetsversnelling. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Herhalingsinterval</guilabel
></term>
<listitem>
<para
>Dit is de tijd (in milliseconden) tussen herhaalde bewegingen voor toetsversnelling. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Versnellingstijd</guilabel
></term>
<listitem>
<para
>Dit is tijd in milliseconden voordat de aanwijzer de maximumsnelheid behaalt voor toetsversnelling. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Maximum snelheid</guilabel
></term>
<listitem>
<para
>Dit is de maximumsnelheid in pixels per seconde die de aanwijzer kan behalen voor toetsversnelling. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Versnellingsprofiel</guilabel
></term>
<listitem>
<para
>Dit is de hellingshoek van de versnellingscurve voor toetsversnelling. </para>
</listitem>
</varlistentry>

</variablelist>

</sect2>

</sect1>

</article>
