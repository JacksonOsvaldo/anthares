<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % German "INCLUDE"
> <!-- change language only here -->
]>

<article id="translations" lang="&language;">
<articleinfo>
<title
>Sprache</title>
<authorgroup>
<author
>&Mike.McBride; &Mike.McBride.mail;</author>
<author
>&Krishna.Tateneni; &Krishna.Tateneni.mail;</author>
<othercredit role="translator"
><firstname
>Matthias</firstname
><surname
>Kiefer</surname
><contrib
>Deutsche Übersetzung</contrib
></othercredit
> 
 </authorgroup>

	  <date
>2015-05-18</date>
	  <releaseinfo
>Plasma 5.3</releaseinfo>

	  <keywordset>
		<keyword
>KDE</keyword>
		<keyword
>Systemeinstellungen</keyword>
		<keyword
>locale</keyword>
		<keyword
>country</keyword>
		<keyword
>language</keyword>
		<keyword
>Übersetzung</keyword>
		<keyword
>Sprache</keyword>
	  </keywordset>
</articleinfo>

<para
>Auf dieser Seite können Sie die Sprachen auswählen, die in der &kde;-Arbeitsumgebung sowie Anwendungen bevorzugt verwendet werden sollen. </para>

<para
>Die &kde;-Arbeitsumgebung und -Anwendungen werden in Amerikanischem Englisch entwickelt und von Freiwilligen in verschiedenste Sprachen übersetzt. Diese Übersetzungen müssen zuerst installiert werden, bevor Sie sie auswählen können. Die Liste der <guilabel
>Verfügbaren Sprachen</guilabel
> zeigt übersetzten Namen der Sprachen an, für die Übersetzungen der &systemsettings; auf Ihrem System installiert und verfügbar sind. Falls die gewünschte Sprache nicht auf der Liste steht, müssen Sie sie installieren. Verwenden Sie dazu die auf Ihrem System übliche Methode. </para>
<note
><para
>Bitte überprüfen Sie, ob die &kde;-Sprachpakete oder -Übersetzungen für die von Ihnen verwendeten Sprachen installiert sind.</para>
<para
>Da &kde; die &Qt;-Bibliotheken verwendet, müssen Sie für eine vollständig übersetzte &GUI; außerdem auch die &Qt;-Übersetzungen der ausgewählten Sprachen installieren.</para
></note>
<!--FIXME 
Toooltip in GUI about available languages is wrong as well
-->

<para
>Die Liste der <guilabel
>Bevorzugten Sprachen</guilabel
> zeigt die übersetzten Namen der Sprachen an, die in der &kde;-Arbeitsumgebung sowie den Anwendungen verwendet werden. Die &kde;-Arbeitsumgebung sowie die Anwendungen sind nicht unbedingt vollständig in jede Sprache übersetzt. Sollte eine Übersetzung in Ihre Sprache fehlen, sucht &kde; anhand der Liste <guilabel
>Bevorzugter Sprachen</guilabel
> nach einer passenden Übersetzung. Falls für keine der dort aufgeführten Sprachen eine Übersetzung vorhanden ist, wird der ursprüngliche Text in amerikanischem Englisch verwendet. </para>

<para
>Sie können eine Sprache zu den <guilabel
>Bevorzugten Sprachen</guilabel
> hinzufügen, indem Sie sie in der Liste <guilabel
>Verfügbarer Sprachen</guilabel
> auswählen und den Pfeil zum Hinzufügen drücken. Ist die Liste aktiv und hat den Fokus, geben Sie einfach nur den ersten Buchstaben der gewünschten Sprache auf der Tastatur ein und der erste passende Eintrag in der Liste ist ausgewählt.  </para>
<para
>Zum Entfernen einer Sprache aus der Liste <guilabel
>Bevorzugte Sprachen</guilabel
> wählen Sie sie aus und klicken Sie auf den Pfeil zum Entfernen. Sie können die Reihenfolge der <guilabel
>Bevorzugten Sprachen</guilabel
> ändern, indem Sie eine Sprache in der Liste auswählen und die Pfeile nach oben bzw. nach unten anklicken. </para>

<para
>Nur Sprachen, die in der Liste <guilabel
>Bevorzugte Sprachen</guilabel
> oder <guilabel
>Verfügbare Sprachen</guilabel
> enthalten sind, können im Dialog <guilabel
>Sprache der Anwendung umschalten</guilabel
> des Menüs <guimenu
>Hilfe</guimenu
> als <guilabel
>Hauptsprache</guilabel
> oder als <guilabel
>Ausweichsprache</guilabel
> ausgewählt werden. </para>

<note>
<para
>Die Einstellungen für Sprachen und Formate sind unabhängig. Mit der Auswahl einer neuen Sprache werden die Einstellungen für Zahlenformate, Währung &etc; <emphasis
>nicht</emphasis
> automatisch an das Land oder die Region, in der die Sprache verwendet wird, angepasst. </para>
</note>

</article>
