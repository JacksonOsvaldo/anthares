<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Brazilian-Portuguese "INCLUDE">
]>
<article id="phonon" lang="&language;">
<title
>Configurações de áudio e vídeo</title>
<articleinfo>
<authorgroup>
<author
><firstname
>Matthias</firstname
><surname
>Kretz</surname
></author>
<othercredit role="translator"
><firstname
>Marcus</firstname
><surname
>Gama</surname
><affiliation
><address
><email
>marcus.gama@gmail.com</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
><othercredit role="translator"
><firstname
>André Marcelo</firstname
><surname
>Alvarenga</surname
><affiliation
><address
><email
>alvarenga@kde.org</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
> 
</authorgroup>

<date
>05/12/2013</date>
<releaseinfo
>&kde; 4.12</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Configurações do Sistema</keyword>
<keyword
>hardware</keyword>
<keyword
>multimídia</keyword>
<keyword
>som</keyword>
<keyword
>vídeo</keyword>
<keyword
>infraestrutura</keyword>
</keywordset>
</articleinfo>

<para
>Este módulo das &systemsettings; permite-lhe configurar a preferência do dispositivo de som e vídeo, assim como as infraestruturas usadas pelo Phonon. </para>

<variablelist>
<varlistentry
><term
>Página de <guilabel
>Preferência do dispositivo</guilabel
></term>
<listitem
><para
>Do lado esquerdo, aparece uma árvore com várias categorias de reprodução e gravação. Para cada categoria, você poderá escolher o dispositivo que deseja usar.</para>
<para
>Os itens de <guilabel
>Reprodução de áudio</guilabel
> e <guilabel
>Gravação de áudio</guilabel
> definem a ordem padrão para os dispositivos, podendo esta ordem ser ajustada individualmente para cada subitem existente.</para>
<para
>Ao clicar no botão <guibutton
>Aplicar a lista de dispositivos a</guibutton
>, aparecerá uma janela que lhe permite copiar a configuração selecionada de uma categoria para as demais.</para>
<para
>Selecione uma categoria, para que os dispositivos disponíveis para essa categoria sejam apresentados na lista à direita. A ordem nesta lista define a preferência que tem pelos dispositivos de saída e captura. Se, por alguma razão, o primeiro dispositivo não puder ser usado, o Phonon irá tentar usar o segundo e assim por diante.</para>
<para
>Use os botões <guibutton
>Preferir</guibutton
> e <guibutton
>Preterir</guibutton
> para alterar a ordem, bem como o botão <guibutton
>Testar</guibutton
> como forma de reproduzir um som de teste no dispositivo selecionado. </para
></listitem>
</varlistentry>

<varlistentry
><term
>Página de <guilabel
>Configuração do hardware de áudio</guilabel
></term>
<listitem
><para
>As diversas listas nesta aba permitem o controle total sobre todas as placas conectadas ao sistema.</para>

<variablelist>
<varlistentry
><term
><guilabel
>Hardware</guilabel
></term>
<listitem
><para
>Selecione a <guilabel
>Placa de som</guilabel
> e o <guilabel
>Perfil</guilabel
> disponível para ser usado.</para>
</listitem
></varlistentry>

<varlistentry
><term
><guilabel
>Configuração do dispositivo</guilabel
></term>
<listitem
><para
>Selecione o <guilabel
>Dispositivo de som</guilabel
> e um <guilabel
>Conector</guilabel
>.</para>
</listitem
></varlistentry>

<varlistentry
><term
><guilabel
>Colocação e teste do alto-falante</guilabel
> ou <guilabel
>Níveis de entrada</guilabel
></term>
<listitem>
<para
>Para um dispositivo de reprodução: Os botões deste painel permitem-lhe testar cada alto-falante separadamente. </para>
<para
>Para um dispositivo de gravação: Uma barra mostra os <guilabel
>Níveis de entrada</guilabel
> do <guilabel
>Conector</guilabel
> selecionado. </para>
</listitem
></varlistentry>
</variablelist>



</listitem>
</varlistentry>

<varlistentry
><term
>Página de <guilabel
>Infraestrutura</guilabel
></term>
<listitem
><para
>Do lado esquerdo deste módulo, é apresentada uma lista com as infraestruturas do Phonon encontradas no seu sistema. A ordem aqui determina a sequência na qual o Phonon irá usar essas infraestruturas. Use os botões <guibutton
>Preferir</guibutton
> e <guibutton
>Preterir</guibutton
> para modificar essa ordem.</para>
</listitem>
</varlistentry>

</variablelist>

</article>
