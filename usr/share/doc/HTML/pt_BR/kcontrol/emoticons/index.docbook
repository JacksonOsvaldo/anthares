<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Brazilian-Portuguese "INCLUDE">
]>
<article id="emoticons" lang="&language;">
<articleinfo>
<title
>Emoticons</title>
<authorgroup>
<author
>&Anne-Marie.Mahfouf;</author>
<othercredit role="translator"
><firstname
>Marcus</firstname
><surname
>Gama</surname
><affiliation
><address
><email
>marcus.gama@gmail.com</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
> 
</authorgroup>

<date
>08/04/2015</date>
<releaseinfo
>Plasma 5.3</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Configurações do Sistema</keyword>
<keyword
>emoticons</keyword>
<keyword
>KCMEmoticons</keyword>
</keywordset>
</articleinfo>

<sect1 id="kcm_emoticons">
<title
>Gerenciador de temas emoticons</title>
<sect2 id="icons-intro">
<title
>Introdução</title>

<para>
<screenshot>
<screeninfo
>Imagem do Gerenciador de temas emoticons</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="emoticons.png" format="PNG"/>
    </imageobject>
     <textobject>
      <phrase
>O Gerenciador de temas emoticons</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

<para
>Os emoticons podem ser usados em vários aplicativos do &kde;: &kopete;, &konversation;, &kmail;... Este módulo permite-lhe gerenciar facilmente os seus temas emoticons. Você poderá:</para>

<itemizedlist>
<listitem
><para
>adicionar temas</para
></listitem>
<listitem
><para
>remover temas</para
></listitem>
<listitem
><para
>instalar um tema existente</para
></listitem>
<listitem
><para
>adicionar, remover ou editar ícones dentro de um tema</para
></listitem>
</itemizedlist>

<important
><para
>Por favor, repare que você irá configurar os temas de emoticons por aplicativo.</para
></important>

<para
>O módulo está separado em duas partes: do lado esquerdo, você poderá gerenciar os seus temas e à direita poderá gerenciar os ícones dentro dos temas. </para>

<para
>Um emoticon consiste de duas partes: um ícone (normalmente um arquivo <filename
>.png</filename
>, <filename
>.mng</filename
> ou <filename
>.gif</filename
>) e um texto que descreve o emoticon. O texto poderá ser uma sequência de símbolos ASCII, como por exemplo <userinput
>]:-></userinput
> ou uma descrição breve do emoticon, entre <userinput
>*</userinput
>, como por exemplo <userinput
>*AMIGO*</userinput
>. O usuário digita o símbolo ou o texto e o emoticon irá substituir esse texto. </para>

<para
>Os temas de emoticons serão salvos localmente na sua pasta pessoal, em <filename class="directory"
>$KDEHOME/share/emoticons</filename
>. </para>

</sect2>

<sect2 id="themes">
<title
>Temas emoticons</title>

<para
>Esta seção permite-lhe gerenciar os temas emoticons. </para>

<variablelist>
<varlistentry
><term
><guilabel
>Espaço necessário ao redor dos emoticons</guilabel
></term>
<listitem>
<para
>Com esta opção assinalada, você terá que separar o emoticon com um espaço antes e depois, para que ele possa ser alterado para um ícone. Se você escrever "Olá:-)", este texto irá ficar como está, enquanto se escrever "Olá :-)", ficará com "Olá" seguido do emoticon ':-)'. </para>
<para
>Quando esta opção não está assinalada (padrão), cada texto de emoticon será substituído pelo seu respectivo ícone. </para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Novo tema...</guilabel
></term>
<listitem>
<para
>Você pode criar o seu próprio tema a partir do zero. Clicar neste botão, irá abrir uma janela que solicitará o nome do novo tema. Você poderá então usar o botão <guibutton
>Adicionar...</guibutton
> à direita para criar novos emoticons e o seu texto associado. </para>
</listitem>
</varlistentry>
<varlistentry
><term
><guilabel
>Obter novos temas de ícones...</guilabel
></term>
<listitem>
<para
>Você deverá estar conectado à Internet para usar esta ação. Uma janela irá apresentar uma lista de temas de emoticons do <ulink url="http://www.kde-look.org"
>http://www.kde-look.org</ulink
> e você poderá instalar um deles clicando no botão <guibutton
>Instalar</guibutton
> existente à direita da descrição do tema na lista. </para>
</listitem>
</varlistentry>
<varlistentry
><term
><guilabel
>Instalar o arquivo de tema...</guilabel
></term>
<listitem>
<para
>Esta ação permite-lhe instalar um tema que você mesmo tenha baixado. Um arquivo de tema é um pacote, normalmente um arquivo <filename
>.tar.gz</filename
> ou <filename
>.zip</filename
>, ou qualquer outro arquivo de pacote. Uma janela irá solicitar o local deste arquivo e, depois de você tê-lo informado ou arrastado o URL e pressionado <guibutton
>OK</guibutton
>, o tema é instalado e irá aparecer na lista de temas. </para>
</listitem>
</varlistentry>
<varlistentry
><term
><guilabel
>Remover tema</guilabel
></term>
<listitem>
<para
>Esta ação irá remover o tema selecionado do seu disco. Selecione o tema que deseja remover na lista. Clique no botão <guibutton
>Remover tema</guibutton
>. Este tema será removido do disco se você clicar em <guibutton
>Aplicar</guibutton
>. </para>
</listitem>
</varlistentry>
</variablelist>

</sect2>

<sect2 id="emoticons-management">
<title
>Gerenciamento de emoticons</title>

<para
>Esta seção permite-lhe gerenciar os emoticons dentro do tema selecionado. A lista à direita mostra todos os emoticons do tema selecionado. Você poderá adicionar, editar ou remover qualquer emoticon. Selecione um tema na lista da esquerda e depois selecione a ação que deseja efetuar. </para>

<variablelist>
<varlistentry
><term
><guilabel
>Adicionar...</guilabel
></term>
<listitem>
<para
>Este botão permite-lhe adicionar um emoticon ao tema selecionado. A janela solicitará que você escolha um ícone para o seu emoticon e para inserir o símbolo ASCII correspondente ou texto relacionado. Depois disso, pressionar <guibutton
>OK</guibutton
> irá adicionar o seu novo emoticon à lista, e você poderá ver o ícone e o texto correspondente apresentado. Se quiser modificar o ícone ou o texto, poderá usar o botão <guibutton
>Editar...</guibutton
>, descrito abaixo. </para>
</listitem>
</varlistentry>

<varlistentry
><term
><guilabel
>Editar...</guilabel
></term>
<listitem>
<para
>O botão <guibutton
>Editar...</guibutton
> permite-lhe alterar o ícone ou o texto do emoticon selecionado. Você terá que pressionar o botão <guibutton
>OK</guibutton
> para validar esta ação. </para>
</listitem>
</varlistentry>
<varlistentry
><term
><guilabel
>Remover</guilabel
></term>
<listitem>
<para
>Isto irá remover o emoticon selecionado do tema selecionado. Você terá que pressionar o botão <guibutton
>Sim</guibutton
> para validar esta ação. </para>
</listitem>
</varlistentry>
</variablelist>
</sect2>

</sect1>
</article>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
