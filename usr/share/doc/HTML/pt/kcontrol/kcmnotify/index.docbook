<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % Portuguese "INCLUDE"
> <!-- change language only here -->
]>

<article id="kcmnotify" lang="&language;">
<title
>Configuração da Notificação do Sistema</title>
<articleinfo>

<authorgroup>
<author
>&Mike.McBride; &Mike.McBride.mail;</author>
<othercredit role="translator"
><firstname
>José</firstname
><surname
>Pires</surname
><affiliation
><address
><email
>zepires@gmail.com</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
> 
</authorgroup>

<date
>2015-04-09</date>
<releaseinfo
>Plasma 5.3</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Systemsettings</keyword>
<keyword
>notificação do sistema</keyword>
<keyword
>notificação do sistema</keyword>
</keywordset>
</articleinfo>

<sect1 id="sys-notify">
<title
>Configuração da Notificação do Sistema</title>

<para
>O &kde;, como todas as aplicações, precisa de informar o utilizador quando ocorre um problema, termina uma tarefa ou acontece algo. O &kde; utiliza um conjunto de <quote
>Notificações do Sistema</quote
> para manter o utilizador informado do que acontece.</para>

<para
>Se usar este módulo, poderá determinar o que o &kde; faz para comunicar cada evento.</para>

<para
>Para configurar uma notificação, basta seleccionar a aplicação da lista no topo da janela chamada <guilabel
>Origem do evento:</guilabel
>. Isto irá levar a uma lista com todas as notificações configuráveis para a aplicação. A lista de notificações inclui 6 colunas à esquerda do nome da notificação. Estas colunas (da esquerda para a direita) são:</para>

<variablelist>
<varlistentry>
<term
><guiicon
>Tocar um som</guiicon
></term>
<listitem
><para
>Isto faz exactamente o que você está a pensar. Se estiver presente um ícone nesta coluna, o &kde; irá tocar um determinado som. Isto normalmente é usado pelos jogo no &kde; para iniciar um jogo novo ou para outra acção qualquer dentro do jogo.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Mostrar uma mensagem numa janela</guiicon
></term>
<listitem
><para
>Se estiver presente um ícone nesta coluna, o &kde; irá abrir uma janela de mensagem e avisar o utilizador na notificação propriamente dita. Esta é provavelmente a opção mais seleccionada para alertar os utilizadores de um erro.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Registar num ficheiro</guiicon
></term>
<listitem
><para
>Se estiver presente um ícone nesta coluna, o &kde; irá escrever alguma informação num ficheiro do disco, para posterior consulta. Isto é útil para registar problemas ou mudanças importantes no sistema.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Marcar o item da barra de tarefas</guiicon
></term>
<listitem
><para
>Se estiver presente um ícone nesta coluna, o &kde; fará com que a barra de tarefas pisque até que o utilizador tenha carregado no item respectivo da barra. Isto é mais útil quando deseja que o utilizador olhe para o programa (por exemplo, quando chega uma mensagem de e-mail nova ou quando o nome do utilizador foi mencionado num canal de IRC).</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Fala</guilabel
></term>
<listitem
><para
>Se esta opção estiver assinalada, o &kde; fará com que a barra de tarefas do programa que enviou a notificação pisque até que o utilizador carregue no item respectivo. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Executar um comando</guiicon
></term>
<listitem
><para
>Se estiver presente um ícone nesta coluna, será executado um programa separado sempre que esta notificação for efectuada. Isto poderá ser usado para executar um programa que ajude a recuperar dados, terminar um sistema potencialmente instável ou para enviar por e-mail uma notificação a outro utilizador, alertando-o de um problema.</para
></listitem>
</varlistentry>

</variablelist>

<sect2
><title
>Mudar uma notificação</title>
<para
>Para fazer uma alteração numa notificação, carregue no nome da notificação com o &LMB;. A notificação propriamente dita ficará seleccionada e as opções para todos os tipos de notificações ficarão activas.</para>

<tip
><para
>Poderá despoletar mais que um evento por cada notificação. Por exemplo, é fácil ter um som a tocar e uma mensagem a aparecer em resposta a uma notificação do sistema. Uma notificação não impede que as outras notificações funcionem.</para
></tip>

<para
>A seguinte lista detalha cada um dos tipos de notificações e como usá-los. </para>

<variablelist>
<varlistentry>
<term
><guilabel
>Tocar um som</guilabel
></term>
<listitem
><para
>Se esta opção estiver assinalada, o &kde; irá tocar um som sempre que esta notificação for iniciada. Para indicar o ficheiro de som a usar, indique o seu local no campo de texto à direita da opção. Poderá usar o botão da pasta (à direita da janela) para navegar pela sua árvore de pastas. Para ouvir um teste do som, basta carregar no botão para Tocar (o pequeno botão logo à direita do <guilabel
>Tocar um som</guilabel
>.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Mostrar uma mensagem numa janela</guilabel
></term>
<listitem
><para
>Se estiver presente um ícone nesta coluna, o &kde; irá abrir uma janela de mensagem e avisar o utilizador na notificação propriamente dita. O texto da janela não poderá ser alterado nesta janela.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Registar num ficheiro</guilabel
></term>
<listitem
><para
>Se esta opção estiver assinalada, o &kde; irá escrever algumas informações no disco para serem obtidas posteriormente. Para indicar o ficheiro de registo a usar, indique o seu local no campo de texto à direita da opção. Poderá usar o botão da pasta (à direita da janela) para navegar pela sua árvore de pastas. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Marcar o item da barra de tarefas</guilabel
></term>
<listitem
><para
>Se esta opção estiver assinalada, o &kde; fará com que a barra de tarefas do programa que enviou a notificação pisque até que o utilizador carregue no item respectivo.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Executar um comando</guilabel
></term>
<listitem
><para
>Se esta opção estiver assinalada, será executado um programa em separado, quando esta notificação for efectuada. Para indicar o ficheiro de registo a usar, indique o seu local no campo de texto à direita da opção. Poderá usar o botão da pasta (à direita da janela) para navegar pela sua árvore de pastas.</para
></listitem>
</varlistentry>
<!--not in 5.3
<varlistentry>
<term
><guilabel
>Speech</guilabel
></term>
<listitem
><para
>If a mark is in this checkbox, &kde; will use Jovie to speak the event 
message, event name or custom text.</para>
<para
>If you select <guilabel
>Speak Custom Text</guilabel
>, enter the text in the box.  
You may use the following 
substitution strings in the text:
<simplelist>
<member
> <userinput
>%e</userinput
> Name of the event </member>
<member
> <userinput
>%a</userinput
> Application that sent the event </member>
<member
> <userinput
>%m</userinput
> Message sent by the application </member>
</simplelist>
</para
></listitem>
</varlistentry>
-->
</variablelist>

<tip
><para
>Poderá usar as colunas dos ícones (localizadas à esquerda das notificações) para seleccionar ou desligar rapidamente as opções. Se carregar na coluna com o &LMB; irá activar ou desactivar a notificação.</para
></tip>
</sect2>

</sect1>

</article>
