<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
 <!ENTITY kappname "&kde; Menu Editor">
  <!ENTITY package "kdebase">
  <!ENTITY firefox "<application
>Firefox</application
>">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Portuguese "INCLUDE"
> <!-- change language only here -->
]>

<book id="kmenuedit" lang="&language;">

<bookinfo>

<title
>O Manual do Editor de Menus do &kde;</title>

<authorgroup>
<author
>&Milos.Prudek; &Milos.Prudek.mail;</author>
<author
>&Anne-Marie.Mahfouf; &Anne-Marie.Mahfouf.mail;</author>
<othercredit role="reviewer"
>&Lauri.Watts; &Lauri.Watts.mail; </othercredit>
<othercredit role="translator"
><firstname
>José</firstname
><surname
>Pires</surname
><affiliation
><address
><email
>zepires@gmail.com</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
> 
</authorgroup>

<copyright>
<year
>2000</year>
<holder
>&Milos.Prudek;</holder>
</copyright>
<copyright>
<year
>2008</year>
<holder
>&Anne-Marie.Mahfouf;</holder>
</copyright>

<legalnotice
>&FDLNotice;</legalnotice>

<date
>2015-04-27</date>
<releaseinfo
>Plasma 5.3</releaseinfo>

<abstract
><para
>O &kmenuedit; permite a edição dos lançadores de aplicações do &kde;. </para
></abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Editor de Menus do KDE</keyword>
<keyword
>kmenuedit</keyword>
<keyword
>aplicação</keyword>
<keyword
>programa</keyword>
<keyword
>menu</keyword>
<keyword
>kickoff</keyword>
<keyword
>Lancelot</keyword>
</keywordset>

</bookinfo>
<chapter id="introduction">
<title
>Introdução</title>

<para
>O &kmenuedit; permite a edição dos lançadores de aplicações do &kde;.</para>

<para
>O &kmenuedit; pode ser iniciado carregando com o &RMB; no botão de lançamento de aplicações no painel e escolhendo <guimenuitem
>Editar as Aplicações...</guimenuitem
> ou se escrever <userinput
>kmenuedit </userinput
> da linha de comandos do &krunner;.</para>

<para
>O &kmenuedit; permite-lhe:</para>

<itemizedlist>
<listitem
><para
>Ver e editar o menu usado pelo lançador de aplicações actual</para
></listitem>
<listitem
><para
><guimenuitem
>Cortar</guimenuitem
>, <guimenuitem
>Copiar</guimenuitem
> e <guimenuitem
>Colar</guimenuitem
> itens e submenus do menu</para
></listitem>
<listitem
><para
>Criar e remover sub-menus e itens</para
></listitem>
<listitem
><para
>Alterar a ordem dos submenus e itens</para
></listitem>
<listitem
><para
>Esconder os itens e adicionar submenus e itens novos</para
></listitem>
</itemizedlist>

<para
>Por omissão, todas as aplicações instaladas no computador aparecem no menu de lançadores de aplicações de todos os utilizadores. As aplicações poderão aparecer mais que uma vez em vários menus diferentes. As categorias de submenus vazias que estiverem definidas na especificação do menu irão também aparecer, mas não estão visíveis nos lançadores de aplicações, a menos que instale as aplicações que pertencem a essas categorias. </para>

<para
>A árvore tem dois tipos diferentes de elementos:</para>
<itemizedlist>
<listitem
><para
>Submenu: Só os campos <guilabel
>Nome</guilabel
>, <guilabel
>Comentário</guilabel
> e <guilabel
>Descrição</guilabel
>, bem como o botão para seleccionar um ícone, é que estão activos; a página <guilabel
>Avançado</guilabel
> está desactivada. Qualquer submenu poderá conter vários submenus e/ou itens adicionais. </para
></listitem>
<listitem
><para
>Itens: Use este elemento para introduzir os dados da aplicação que deseja adicionar. Para informações mais detalhadas, veja como <link linkend="using-kmenuedit"
>Usar o &kmenuedit;</link
>. </para
></listitem>
</itemizedlist>

<para
>O &kmenuedit; tem dois modos de visualização da árvore - normal e com os itens escondidos. Para ver os últimos, assinale a opção <guilabel
>Mostrar os itens escondidos</guilabel
> da janela de configuração da página de <guilabel
>Opções gerais</guilabel
>. </para>
<para
>Diversos itens adicionais aparecerão no modo de visualização escondida na árvore. Alguns dos itens escondidos parecem ser simplesmente duplicados para têm opções do comando diferentes. Normalmente não deverá alterar nunca estes itens escondidos, caso contrário arriscar-se-á a perder alguma funcionalidade do sistema. </para>
<para
>No modo escondido, irá ter um submenu especial <guilabel
>.hidden [Hidden]</guilabel
> (escondido) como item de topo na árvore. Este submenu especial não pode ser editado. Neste submenu todos os itens apagados serão apresentados no próximo arranque do &kmenuedit;.</para>
<para
>Não é possível apagar os itens com a &GUI; neste submenu especial. Eles irão aparecer de novo no próximo arranque do &kmenuedit;. </para>

<sect1 id="use-cases">
<title
>Casos de Uso</title>

<sect2 id="use-cases-adapt">
<title
>Adaptar o Menu para um Utilizador</title
> 

<sect3 id="use-cases-adapt-order">
<title
>Reordenar os Itens</title
> 

<para
>Isto deverá ser feito no modo de visualização escondida, onde só os submenus e itens visíveis no menu de lançamento de aplicações é que aparecem. </para>
<para
>Por omissão, o menu aparece ordenado alfabeticamente pelos nomes ou descrições em Inglês. Se usar outra língua que não o Inglês, então alguns submenus e itens irão aparecer assim desordenados. </para>
<para
>Use as opções em <menuchoice
><guimenu
>Editar</guimenu
><guimenuitem
>Ordenar</guimenuitem
></menuchoice
> para ordenar pelo nome ou pela descrição. Se usar o lançador de aplicações clássico, terá de escolher o <guilabel
>Formato</guilabel
> correspondente na janela de configuração. No lançador &kickoff;, assinale a opção <guilabel
>Mostrar as aplicações pelo nome</guilabel
> se as ordenar pelo nome aqui. </para>
<para
>Se agrupar os seus submenus ou itens usados com maior frequência em conjunto &eg; no topo do menu, torna a sua selecção mais simples. Para alterar a ordem dos itens individuais ou submenus na árvore, use os botões <guibutton
>Subir</guibutton
> ou <guibutton
>Descer</guibutton
> na barra de ferramentas ou estas acções no menu. </para>
<para
>Todos os lançadores de aplicações irão usar a ordem dos submenus definida no &kmenuedit;. </para>
</sect3>

<sect3 id="use-cases-adapt-hide">
<title
>Remover os Itens da Área do Menu</title
> 

<para
>Ter todas as aplicações instaladas num computador poderá ser confuso para alguns utilizadores, pelo que poderá querer esconder alguns dos itens ou submenus menos usados. Existem duas formas diferentes de o fazer: </para>

<para
>Mude para o modo de visualização normal sem itens escondidos. Se apagar os itens, estes serão movidos para o submenu <guilabel
>.hidden [Hidden]</guilabel
> (escondido). Podê-los-á repor na árvore, caso os queira recuperar. </para>
<para
>Se apagar um submenu, o mesmo será realmente apagado, bem como todos os seus submenus e itens. Para os voltar a criar de novo, poderá usar a opção <menuchoice
><guimenu
>Editar</guimenu
><guimenuitem
>Repor no Menu do Sistema</guimenuitem
></menuchoice
>, mas isto irá remover todos os seus submenus e itens personalizados, bem como os seus ficheiros <filename class="extension"
>.desktop</filename
> correspondentes. Esta acção não pode ser anulada. </para>

<para
>A forma preferida de remover os submenus e itens no menu de um lançador de aplicações é assinalar a opção <guilabel
>Item escondido</guilabel
> na página <guilabel
>Geral</guilabel
> e mudar para o modo de visualização escondida. Neste modo é fácil voltar atrás nas alterações, sem destruir a estrutura do menu. A única desvantagem é que terá de esconder todos os itens de um submenu para remover o submenu inteiro da árvore. </para>
</sect3>

</sect2>

<sect2 id="use-cases-additems">
<title
>Adicionar Itens Personalizados</title
> 

<para
>Para adicionar itens personalizados (submenus ou itens), use as acções no menu ou na barra de ferramentas. Os itens precisam de um nome e um comando; sem a definição do comando, o item não será gravado e a sua adição perder-se-á. </para>
<para
>Se adicionar um item, este será introduzido como sub-item na posição actual da árvore. Mova um item, arrastando-o para tal com o rato ou usando o botão <guibutton
>Descer</guibutton
> para o fundo da árvore para o tornar um item de topo. </para>

</sect2>

<sect2 id="use-cases-transfer">
<title
>Transferir as definições do lançador de aplicações</title
> 

<para
>Não existe forma de transferir as definições do menu com a &GUI;; terá de o fazer manualmente e copiar os seguintes ficheiros para o utilizador de destino:</para>
<para
>O &kmenuedit; grava a hierarquia de menus em <filename class="directory"
>$(qtpaths --paths GenericConfigLocation)</filename
>, no ficheiro <filename
>menus/applications-kmenuedit.menu</filename
> e a pasta <filename class="directory"
>$(qtpaths --paths GenericDataLocation)</filename
> <filename
>desktop-directories</filename
> contém os ficheiros 'desktop' para os submenus que criou. Em <filename class="directory"
>$(qtpaths --paths GenericDataLocation)</filename
>, na sub-pasta <filename
>applications</filename
>, irá encontrar os ficheiros 'desktop' para os itens personalizados que criou. </para>
<para
>Os atalhos de cada aplicação são guardados em <filename
>kglobalshortcutsrc</filename
>, na pasta <filename class="directory"
>$(qtpaths --paths GenericConfigLocation)</filename
>,, mas a exportação/importação não funciona porque os UUID's dos atalhos não correspondem entre sistemas, mesmo que os ficheiros <filename class="extension"
>.desktop</filename
> sejam os mesmos. Terá de atribuir manualmente todos os atalhos de novo. </para>

</sect2>

</sect1>
</chapter>

<chapter id="quickstart">
<chapterinfo>
<authorgroup>
<author
>&Virgil.J.Nisly; &Virgil.J.Nisly.mail;</author>
</authorgroup>
</chapterinfo>
<title
>Adicionar um Item do Menu</title>

  <para
>Neste exemplo, será adicionado o &firefox; ao menu <guisubmenu
>Internet</guisubmenu
>.</para>
  <para
>Para começar, necessitamos de abrir o &kmenuedit;, como tal carregue com o &RMB; no menu de lançamento de aplicações, carregue em <guimenuitem
>Editar as Aplicações...</guimenuitem
> para iniciar o &kmenuedit;. Após o &kmenuedit; ter iniciado, seleccione a <guisubmenu
>Internet</guisubmenu
> como aparece na imagem abaixo. <screenshot>
<screeninfo
>Seleccionar a <guisubmenu
>Internet</guisubmenu
></screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="selectinternet.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Seleccionar a <guisubmenu
>Internet</guisubmenu
></phrase>
</textobject>
</mediaobject>
</screenshot
></para>
  <para
>Logo que tenha seleccionado a <guisubmenu
>Internet</guisubmenu
>, carregue em <menuchoice
><guimenu
>Ficheiro</guimenu
><guimenuitem
>Novo Item...</guimenuitem
></menuchoice
>, o que irá abrir a janela <guilabel
>Novo Item</guilabel
>, como aparece em baixo. Escreva o nome do programa que deseja adicionar que é, neste caso, o <userinput
>firefox</userinput
>. <screenshot>
<screeninfo
>Janela do <guilabel
>Novo Item</guilabel
></screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="itemname.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>A janela do <guilabel
>Novo Item</guilabel
>.</phrase>
</textobject>
</mediaobject>
</screenshot>
</para>
<para
>Carregue em Return, para que possa ver algo como a imagem abaixo na janela principal. <screenshot>
<screeninfo
>Novo Item</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="new.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>O item novo criado.</phrase>
</textobject>
</mediaobject>
</screenshot
></para>

<para
>Agora, será preenchida a <guilabel
>Descrição:</guilabel
>, que neste caso poderá ser <userinput
>Navegador Web</userinput
>. <note
><para
>A descrição e o nome serão apresentados no &kmenu; como <quote
>Navegador Web (Firefox)</quote
>.</para
></note
> Terá de preencher o nome do executável no campo <guilabel
>Comando:</guilabel
>, onde neste caso se irá escrever <userinput
><command
>firefox</command
></userinput
>.</para>
<para
>O comando terá de estar acessível pela sua variável <envar
>PATH</envar
> ou então terá de indicar a localização completa do executável. Se não souber o nome do executável de uma aplicação, use o comando <userinput
><command
>locate</command
></userinput
> para procurar pelo ficheiro 'desktop' e indique o texto da linha 'Exec' como comando aqui. </para>
<note
><para
>A seguir ao comando, poderá ter vários itens de substituição por outros valores actuais, quando o programa for executado: <simplelist>
<member
>%f - um único nome de ficheiro</member>
<member
>%F - uma lista de ficheiros; use nas aplicações que podem abrir vários ficheiros locais de uma vez</member>
<member
>%u - um único &URL;</member>
<member
>%U - uma lista de &URL;s</member>
<member
>d% - a pasta do ficheiro a abrir</member>
<member
>%D - uma lista de pastas</member>
<member
>%i - o ícone</member>
<member
>m% - o mini-ícone</member>
<member
>%c - o título</member>
</simplelist
></para>
<informalexample
><para
>Por exemplo: se quiser que o 'firefox' inicie a sua navegação Web em 'www.kde.org' - em vez do <command
>firefox</command
>, poderá escrever <command
>firefox %u www.kde.org</command
>.</para
></informalexample
></note>
<para
>A maioria das aplicações aceitam opções adicionais &eg; o nome de um perfil definido como o &konqueror; ou o &konsole;. Para ver todas as opções de uma aplicação, execute <userinput
><command
><replaceable
>aplicação</replaceable
></command
> <option
>--help</option
></userinput
> no &konsole;.</para>
<para
>Seria bom ter um ícone mais criativo, como tal vai-se carregar no ícone genérico que está ao lado do <guilabel
>Nome:</guilabel
> (como nota, o ícone predefinido poderá estar em branco; se for o caso, carregue na área à direita do campo de texto do nome). Ele irá invocar a janela para <guilabel
>Seleccionar um Ícone</guilabel
>, que permitirá escolher o ícone novo, como aparece em baixo. <screenshot>
<screeninfo
>A janela para <guilabel
>Seleccionar um Ícone</guilabel
></screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="selecticon.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>A janela para <guilabel
>Seleccionar um Ícone</guilabel
>.</phrase>
</textobject>
</mediaobject>
</screenshot
></para>

  <para
>Foi escolhido o ícone do 'firefox' na lista; de seguida, carregue em &Enter;. O seu ecrã final deverá ser algo semelhante à imagem em baixo. <screenshot>
<screeninfo
>Imagem final</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="done.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>Isto é como deverá ficar o item do menu completo.</phrase>
</textobject>
</mediaobject>
</screenshot
></para>

<para
>O local do novo item do menu poderá ser alterado agora com os botões <guibutton
>Subir</guibutton
> e <guibutton
>Descer</guibutton
> da barra de ferramentas do &kmenuedit; ou então arrastando-o com o rato.</para>
 <para
>Os itens do submenu podem ser ordenados com o botão <guibutton
>Ordenar</guibutton
> da barra de ferramentas do &kmenuedit; ou com a opção do menu <menuchoice
><guimenu
>Editar</guimenu
><guisubmenu
>Ordenar</guisubmenu
></menuchoice
>.</para>

<para
>Carregue em <menuchoice
><guimenu
>Ficheiro</guimenu
><guimenuitem
>Gravar</guimenuitem
></menuchoice
>, espere que a janela <guilabel
>A Actualizar a Configuração do Sistema</guilabel
> termine, para depois encontrar o &firefox; no sub-menu <guisubmenu
>Internet</guisubmenu
>.</para>

</chapter>

<chapter id="using-kmenuedit">
<title
>Usar o &kmenuedit;</title>

<para
>O painel esquerdo da aplicação mostra a estrutura do lançador de aplicações. Quando escolher os itens do painel esquerdo, o painel direito mostra a informação detalhada sobre o item de menu realçado.</para>

<sect1 id="details-general">
<title
>Página Geral</title>

<variablelist>
<varlistentry>
<term
><guilabel
>Nome:</guilabel
></term>
<listitem
><para
>Este é o nome do seu programa tal como aparece no lançador de aplicações. Pode ser diferente do nome verdadeiro do executável. Por exemplo, o nome do executável <command
>mc</command
> é "<application
>Midnight Commander</application
>".</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Descrição:</guilabel
></term>
<listitem
><para
>A descrição será apresentada em conjunto com o nome no lançador de aplicações. O mesmo é inteiramente opcional.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Comentário:</guilabel
></term>
<listitem
><para
>Descreve o programa com um maior detalhe neste campo. O mesmo é inteiramente opcional.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Comando:</guilabel
></term>
<listitem
><para
>Este é o nome do programa executável. Certifique-se que tem permissões para executar o programa.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Activar o aviso de execução</guilabel
></term>
<listitem
><para
>Se esta opção estiver assinalada, isto irá mostrar uma reacção visual sempre que for iniciada uma aplicação. </para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Colocar na bandeja do sistema</guilabel
></term>
<listitem
><para
>Quando estiver assinalada a opção, o ícone da aplicação irá aparecer na bandeja do painel. Poderá então esconder ou mostrar a aplicação, carregando para tal no ícone da bandeja do sistema. Se carregar nele com o &RMB;, poderá também desacoplar ou sair da aplicação. </para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Mostrar apenas no &kde;</guilabel
></term>
<listitem
><para
>Se a opção estiver assinalada, o item da aplicação só será visível nos lançadores de aplicações do &kde;, não o sendo nos outros ambientes gráficos. </para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Item escondido</guilabel
></term>
<listitem
><para
>Remove um item da área do menu no lançador de aplicações. </para
></listitem>
</varlistentry>
</variablelist>
</sect1>

<sect1 id="details-advanced">
<title
>Página Avançado</title>

<variablelist>
<varlistentry>
<term
><guilabel
>Pasta de trabalho:</guilabel
></term>
<listitem
><para
>Especifica a directoria de trabalho do programa. Esta será a directoria actual quando o programa for iniciado. Não precisa ser a mesma que a localização do executável.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Correr num terminal</guilabel
></term>
<listitem
><para
>Precisa assinalar isto se o seu programa necessitar de um emulador de terminal para se executar. Isso aplica-se principalmente às <link linkend="gloss-console-application"
>aplicações da consola</link
>.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Opções do terminal:</guilabel
></term>
<listitem
><para
>Ponha todas as opções do terminal neste campo.</para
></listitem>
</varlistentry>
<varlistentry>
<term
><guilabel
>Correr como outro utilizador</guilabel
></term>
<listitem
><para
>Se quiser executar este programa como um utilizador diferente (que não você), assinale esta opção e indique o nome do utilizador no campo <guilabel
>Utilizador:</guilabel
>.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Tecla de atalho actual:</guilabel
></term>
<listitem
><para
>Poderá atribuir uma combinação de teclas especial para lançar o seu programa.</para>

<para
>Carregue no botão <guibutton
>Nenhum</guibutton
>, à direita da opção <guilabel
>Tecla de atalho actual:</guilabel
>.</para>

<para
>O texto do botão irá mudar para <guilabel
>Introduzir...</guilabel
>, pelo que poderá pressionar a combinação de teclas que deseja atribuir ao seu programa. </para>
<para
>Poderá repor o atalho como <guilabel
>Nenhum</guilabel
> se usar este botão: <inlinemediaobject
><imageobject
> <imagedata fileref="reset.png" format="PNG"/></imageobject
></inlinemediaobject
>. </para>

<para
>Não se esqueça de guardar a sua configuração, carregando para tal no ícone <guiicon
>Gravar</guiicon
> ou usando o item do menu <menuchoice
> <guimenu
>Ficheiro</guimenu
><guimenuitem
>Gravar</guimenuitem
> </menuchoice
>.</para>

</listitem>
</varlistentry>
</variablelist>
</sect1>

</chapter>

<chapter id="menu-reference">
<title
>Referência do Menu</title>

<para
>A maioria das acções do menu estão também disponíveis no menu de contexto que aparece quando se carrega com o &RMB; sobre um item da árvore.</para>

<variablelist>
<varlistentry id="file-new-item">
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl; <keycap
>N</keycap
></keycombo
></shortcut
> <guimenu
>Ficheiro</guimenu
> <guimenuitem
>Novo Item...</guimenuitem
> </menuchoice
></term
> <listitem
><para
><action
>Adiciona um novo item do menu.</action
></para
></listitem>
</varlistentry>

<varlistentry id="file-new-submenu">
<term
><menuchoice
><guimenu
>Ficheiro</guimenu
> <guimenuitem
>Novo Sub-Menu...</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Adiciona um novo sub-menu.</action
></para
></listitem>
</varlistentry>

<varlistentry id="file-save">
<term
><menuchoice
><shortcut
><keycombo action="simul"
>&Ctrl; <keycap
>S</keycap
></keycombo
></shortcut
> <guimenu
>Ficheiro</guimenu
> <guimenuitem
>Gravar</guimenuitem
> </menuchoice
></term>
<listitem>
<para
><action
>Grava o menu</action
></para>
</listitem>
</varlistentry>

<varlistentry id="file-quit">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>Q</keycap
></keycombo
> </shortcut
> <guimenu
>Ficheiro</guimenu
> <guimenuitem
>Sair</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Sai</action
> do &kmenuedit;.</para
></listitem>
</varlistentry>

<varlistentry id="edit-move-up">
<term
><menuchoice
><guimenu
>Editar</guimenu
><guimenuitem
>Subir</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Move</action
> o item seleccionado para cima no seu submenu.</para
></listitem>
</varlistentry>

<varlistentry id="edit-move-down">
<term
><menuchoice
><guimenu
>Editar</guimenu
><guimenuitem
>Descer</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Move</action
> o item seleccionado para baixo no seu submenu.</para
></listitem>
</varlistentry>


<varlistentry id="edit-cut">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>X</keycap
></keycombo
> </shortcut
> <guimenu
> Editar</guimenu
> <guimenuitem
>Cortar</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Corta o item do menu actual para a área de transferência.</action
> Se quiser mover o item do menu, deve primeiro cortá-lo para a área de transferência, passar para o local de destino com o painel esquerdo e usar a função <guimenuitem
>Colar</guimenuitem
> para colar o item do menu na área de transferência.</para
></listitem>
</varlistentry>

<varlistentry id="edit-copy">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>C</keycap
></keycombo
> </shortcut
> <guimenu
> Editar</guimenu
> <guimenuitem
>Copiar</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Copia o item do menu seleccionado para a área de transferência</action
>. Poderá usar posteriormente a função <guimenuitem
>Colar</guimenuitem
> para repor o item de menu copiado no seu destino, a partir da área de transferência. Pode colar o mesmo item várias vezes.</para
></listitem>
</varlistentry>

<varlistentry id="edit-paste">
<term
><menuchoice
><shortcut
> <keycombo action="simul"
>&Ctrl;<keycap
>V</keycap
></keycombo
> </shortcut
> <guimenu
>Editar</guimenu
> <guimenuitem
>Colar</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Cola o item do menu a partir da área de transferência</action
> no local seleccionado no momento do menu principal. Deverá usar em primeiro lugar o comando <guimenuitem
>Cortar</guimenuitem
> ou <guimenuitem
>Copiar</guimenuitem
> antes de o poder <guimenuitem
>Colar</guimenuitem
>.</para
></listitem>
</varlistentry>

<varlistentry id="edit-delete">
<term
><menuchoice
><shortcut
><keycap
>Del</keycap
></shortcut
> <guimenu
>Editar</guimenu
> <guimenuitem
>Apagar</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Apaga o item seleccionado no menu.</action
></para
></listitem>
</varlistentry>

<varlistentry id="edit-sort">
<term
><menuchoice
><guimenu
>Editar</guimenu
> <guisubmenu
>Ordenar</guisubmenu
> </menuchoice
></term>
<listitem
><para
><action
>Abre</action
> um submenu para ordenar o submenu seleccionado ou toda a árvore em si. Existem dois métodos de ordenação implementados, nomeadamente pelo nome e pela descrição.</para
></listitem>
</varlistentry>

<varlistentry id="edit-restore">
<term
><menuchoice
><guimenu
>Editar</guimenu
> <guimenuitem
>Repor no Menu de Sistema</guimenuitem
> </menuchoice
></term>
<listitem
><para
><action
>Isto irá repor o lançador de aplicações como estava predefinido e remover todas as suas configurações personalizadas. Existirá uma mensagem que lhe perguntará se deseja realmente fazê-lo.</action
></para
></listitem>
</varlistentry>

</variablelist>

<para
>O &kmenuedit; tem os menus de <guimenu
>Configuração</guimenu
> e <guimenu
>Ajuda</guimenu
> normais do &kde;; para mais informações, leia as secção acerca dos menus de <ulink url="help:/fundamentals/ui.html#menus-settings"
>Configuração</ulink
> e <ulink url="help:/fundamentals/ui.html#menus-help"
>Ajuda</ulink
> dos Fundamentos do &kde;. </para>

</chapter>

<chapter id="credits">

<title
>Créditos e Licença</title>

<para
>&kmenuedit; </para>
<para
>Programa copyright &copy; 2002, &Raffaele.Sandrini;</para>

<para
>Contribuições:</para>
<itemizedlist>
<listitem
><para
>&Matthias.Elter; &Matthias.Elter.mail; - Autoria Original</para>
</listitem>
<listitem
><para
>&Matthias.Ettrich; &Matthias.Ettrich.mail;</para>
</listitem>
<listitem
><para
>&Daniel.M.Duley; &Daniel.M.Duley.mail;</para>
</listitem>
<listitem
><para
>&Preston.Brown; &Preston.Brown.mail;</para>
</listitem>
</itemizedlist>

<para
>Documentação com 'copyright' &copy; 2000 de &Milos.Prudek;</para>
<para
>Documentação com 'copyright' &copy; 2008 de &Anne-Marie.Mahfouf;</para>
<para
>Actualizado para o &kde; 3.0 por &Lauri.Watts; &Lauri.Watts.mail; 2002</para>

<para
>Tradução de José Nuno Pires <email
>zepires@gmail.com</email
></para
> 
&underFDL; &underGPL; </chapter>

<glossary id="glossary">
<title
>Glossário</title>

<glossentry id="gloss-console-application">
<glossterm
>Aplicação de Consola</glossterm>
<glossdef>

<para
>Uma aplicação feita originalmente para os ambientes não-gráficos e orientados para o texto. Essas aplicações devem ser executadas dentro de um emulador de consola como o &konsole;. Não são notificadas automaticamente quando o utilizador termina a sua sessão do &kde;. Como tal, o utilizador não se deve esquecer de gravar os documentos abertos nessas aplicações antes de sair do &kde;.</para>

<para
>As aplicações da consola suportam a cópia e colagem das aplicações do &kde;. Basta marcar o texto na aplicação de consola com o seu rato, voltar para aplicação do &kde; e carregar em <keycombo action="simul"
>&Ctrl; <keycap
>V</keycap
></keycombo
> para colar o texto. Se quiser copiar a partir da aplicação do &kde; para uma aplicação de consola, carregue em <keycombo action="simul"
>&Ctrl; <keycap
>C</keycap
></keycombo
>, mude para a aplicação de consola e carregue no botão do meio do seu rato<footnote
><para
>Se o seu rato não tiver o botão do meio, terá de carregar nos botões <mousebutton
>esquerdo</mousebutton
> e <mousebutton
>direito</mousebutton
> ao mesmo tempo. A isto dá-se o nome de <quote
>emulação do botão do meio</quote
> e deverá ser suportada pelo seu sistema operativo para funcionar.</para
></footnote
>.</para>

</glossdef>
</glossentry>

</glossary>

&documentation.index;

</book>
<!--
Local Variables:
mode: sgml
sgml-minimize-attributes: nil
sgml-general-insert-case: lower
End:
-->

