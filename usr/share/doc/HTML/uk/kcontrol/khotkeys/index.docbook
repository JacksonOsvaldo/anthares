<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Ukrainian "INCLUDE">
]>
<article id="khotkeys" lang="&language;">
<title
>Нетипові скорочення</title>

<articleinfo>
<authorgroup>
<author
><firstname
>Subhashish</firstname
> <surname
>Pradhan</surname
></author>
<author
>&TC.Hollingsworth; &TC.Hollingsworth.mail;</author>
<othercredit role="translator"
><firstname
>Юрій</firstname
><surname
>Чорноіван</surname
><affiliation
><address
><email
>yurchor@ukr.net</email
></address
></affiliation
><contrib
>Переклад українською</contrib
></othercredit
> 
</authorgroup>

<copyright>
<year
>2011</year>
<holder
>Subhashish Pradhan</holder>
</copyright>
<copyright>
<year
>2012</year>
<holder
>&TC.Hollingsworth;</holder>
</copyright>
<legalnotice
>&FDLNotice;</legalnotice>

<date
>22 січня 2017 року</date>
<releaseinfo
>Плазма 5.8</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Системні параметри</keyword>
<keyword
>клавіатурні скорочення</keyword>
<keyword
>скорочення</keyword>
<keyword
>Нетипові скорочення</keyword>
<keyword
>khotkeys</keyword>
</keywordset>
</articleinfo>

<sect1 id="intro">
<title
>Вступ</title>

<para
>За допомогою модуля <application
>Нетипові скорочення</application
> програми «Системні параметри» ви можете налаштувати нетипові клавіатурні скорочення та жести мишею. За допомогою цих скорочень і жестів ви зможете керувати програмами, віддавати певні команди тощо.</para>

</sect1>


<sect1 id="manage">
<title
>Керування клавіатурними скороченнями і групами</title>

<sect2 id="manage-add-group">
<title
>Додавання груп</title>

<para
>За допомогою цього модуля ви зможете впорядкувати пов’язані клавіатурні скорочення за групами. Наприклад, якщо у вас є деякі клавіатурні скорочення, які пов’язано з керуванням музичним програвачем, ви можете створити групу для цих скорочень з назвою <replaceable
>Музичний програвач</replaceable
>. </para>

<para
>Щоб додати нову групу, натисніть кнопку <guibutton
>Змінити</guibutton
>, розташовану під лівою панеллю, і виберіть у меню пункт <guimenuitem
>Нова група</guimenuitem
>.</para>

</sect2>

<sect2 id="manage-add-shortcut">
<title
>Додавання скорочень</title>

<para
>Щоб додати нове клавіатурне скорочення, натисніть кнопку <guibutton
>Змінити</guibutton
>, розташовану під лівою панеллю і виберіть у меню пункт <guisubmenu
>Створити</guisubmenu
>.</para>

<para
>За допомогою першого меню, яке буде показано ви зможете вибрати тип події. Можна вибрати один з таких типів:</para>

<variablelist>

<varlistentry id="manage-add-shortcut-global">
<term
><guisubmenu
>Загальне скорочення</guisubmenu
></term>
<listitem
><para
>Загальні скорочення — це стандартні клавіатурні скорочення, скористатися якими можна буде будь-де, якщо ви працюєте у робочому просторі Плазми &kde;.</para
></listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-window-action">
<term
><guisubmenu
>Дія для вікна</guisubmenu
></term>
<listitem
><para
>Дії для вікна — дії, які виконуються, коли щось відбувається з певним вікном, зокрема, коли вікно з’являється на екрані, отримує фокус або закривається.</para>
</listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-mouse-gesture">
<term
><guisubmenu
>Дія на жест мишею</guisubmenu
></term>
<listitem
><para
>Дія на жест мишею виконується у відповідь на виконання користувачем певного руху мишею (або на сенсорній панелі).</para
></listitem>
</varlistentry>

</variablelist>

<para
>Після вибору типу події буде відкрито підменю, за допомогою якого ви зможете вибрати тип дії. Передбачено такі типи дій:</para>

<variablelist>

<varlistentry id="manage-add-shortcut-command">
<term
><guimenuitem
>Команда/Адреса &URL;</guimenuitem
></term>
<listitem
><para
>Ця дія призведе до виконання команди або відкриття певної адреси у відповідь на натискання клавіатурного скорочення.</para
></listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-dbus">
<term
><guimenuitem
>Команда &DBus;</guimenuitem
></term>
<listitem
><para
>За допомогою цієї дії можна викликати метод &DBus; у запущеній програмі або фоновій службі. Щоб дізнатися більше про &DBus;, ознайомтеся з <ulink url="http://techbase.kde.org/Development/Tutorials/D-Bus/Introduction"
>Вступом до технології &DBus; на TechBase &kde;</ulink
>.</para
></listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-keyboard-input">
<term
><guimenuitem
>Надіслати ввід з клавіатури</guimenuitem
></term>
<listitem
><para
>За допомогою цього пункту можна надіслати певні символи до запущеної програми так, наче їх було введено вами безпосередньо за допомогою клавіатури.</para
></listitem>
</varlistentry>

</variablelist>

<para
>Щойно буде вибрано тип дії, можна перейти до редагування скорочення, пов’язаного з цією дією. Щоб дізнатися більше, зверніться до розділу <xref linkend="shortcuts"/>.</para>

</sect2>

<sect2 id="manage-delete">
<title
>Вилучення скорочень і груп</title>

<para
>Щоб вилучити скорочення або групу, позначте відповідний пункт у списку, а потім натисніть кнопку <guibutton
>Змінити</guibutton
> під лівою панеллю і виберіть пункт <guimenuitem
>Вилучити</guimenuitem
>.</para>

</sect2>

<sect2 id="manage-export">
<title
>Експортування груп</title>

<para
>Ви можете експортувати дані групи з метою використання скорочень групи на іншому комп’ютері або резервного копіювання.</para>

<para
>Щоб експортувати групу, позначте пункт групи у списку, а потім натисніть кнопку <guibutton
>Змінити</guibutton
>, розташовану під лівою панеллю, і виберіть пункт <guimenuitem
>Експортувати групу...</guimenuitem
>. У відповідь програма відкриє вікно, за допомогою якого ви зможете визначитися з параметрами експортування. Передбачено такі параметри:</para>

<variablelist>

<varlistentry id="manage-export-actions">
<term
><guilabel
>Дії з експортування</guilabel
></term>
<listitem
><para
>За допомогою цього пункту можна визначити стан скорочень групи після їх наступного імпортування. Виберіть варіант <guilabel
>Поточний стан</guilabel
>, щоб зберегти поточний стан скорочень, <guilabel
>Увімкнений</guilabel
>, щоб всі скорочення було увімкнено, і варіант <guilabel
>Вимкнений</guilabel
>, щоб всі скорочення було вимкнено. </para
></listitem>
</varlistentry>

<varlistentry id="manage-id">
<term
><guilabel
>Ідентифікатор</guilabel
></term>
<listitem
><para
>Тут ви можете вказати текст-ідентифікатор групи. Якщо групу типово включено, у цьому полі вже бути типовий текст.</para
></listitem>
</varlistentry>

<varlistentry id="manage-merge">
<term
><guilabel
>Дозволити об’єднання</guilabel
></term>
<listitem
><para
>За допомогою цього пункту можна визначити дію, яку буде виконано, якщо група вже існує у системі, до якої імпортується експортована група. Якщо пункт буде позначено, всі нові дії буде додано до групи у системі призначення, а дії, які у системі призначення вже було визначено, буде перезаписано на основі даних з імпортованого файла. Якщо пункт не буде позначено, модуль відмовиться імпортувати файл. </para
></listitem>
</varlistentry>

<varlistentry id="manage-filename">
<term
><guilabel
>Назва файла</guilabel
></term>
<listitem
><para
>За допомогою цього пункту можна визначити назву файла, до якого ви хочете експортувати скорочення. Ви також можете натиснути кнопку <inlinemediaobject
><imageobject
><imagedata fileref="document-open.png" format="PNG"/></imageobject
></inlinemediaobject
>, розташовану праворуч від поля для введення тексту, щоб відкрити діалогове вікно вибору файла і вибрати файл за допомогою цього вікна. </para>

<tip
><para
>Для експортованих файлів типово використовується суфікс назви <literal
>.khotkeys</literal
>.</para
></tip>
</listitem>
</varlistentry>

</variablelist>

<screenshot id="screenshot-manage-export">
<screeninfo
>Експортування групи</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="manage-export.png" format="PNG"/></imageobject>
<textobject
><phrase
>Діалогове вікно експортування групи.</phrase
></textobject>
<caption
><para
>Експортування групи клавіатурних скорочень.</para
></caption>
</mediaobject>
</screenshot>

</sect2>

<sect2 id="manage-import">
<title
>Імпортування груп</title>

<para
>Щоб імпортувати групу, натисніть кнопку <guibutton
>Змінити</guibutton
>, розташовану під лівою панеллю, і виберіть пункт <guilabel
>Імпортувати...</guilabel
>. У відповідь модуль відкриє діалогове вікно вибору файла, за допомогою якого ви зможете вибрати раніше створений за допомогою дії експортування файл для імпортування.</para>

</sect2>

</sect1>


<sect1 id="groups">
<title
>Внесення змін до груп</title>

<para
>У лівій частині вікна типові скорочення розподілено за групами. Ці групи можна розгорнути натисканням кнопок зі стрілочками з метою перегляду скорочень групи.</para>

<para
>Після натискання пункту групи буде показано дві вкладки, призначені для налаштовування групи. За допомогою вкладки <guilabel
>Коментар</guilabel
> ви можете зберегти нотатки щодо групи, система не використовуватиме ці дані. За допомогою вкладки <guilabel
>Умови</guilabel
> ви можете обмежити перелік вікон, до яких застосовуватимуться скорочення групи.</para>

<screenshot id="screenshot-groups-comment">
<screeninfo
>Вкладка «Коментар»</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="groups-comment.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вкладка «Коментар» групи скорочень.</phrase
></textobject>
<caption
><para
>Внесення змін до коментаря групи.</para
></caption>
</mediaobject>
</screenshot>

<para
>Умови буде показано у форматі ієрархії, верхнім рівнем якої є <guilabel
>Та</guilabel
>. Всі умови у групі <guilabel
>Та</guilabel
> має бути задоволено під час вмикання скорочень групи.</para>

<para
>Ви можете додати додаткові групи умов натисканням кнопки спадного списку<guibutton
>Створити</guibutton
> праворуч від ієрархії умов. Серед типів груп згадана раніше <guimenuitem
>Та</guimenuitem
>, <guimenuitem
>Або</guimenuitem
>, у разі використання цього типу умову має задовольняти хоча б один запис, та <guimenuitem
>Ні</guimenuitem
>, у разі використання цього типу скорочення групи вмикатимуться, якщо значення всіх записів не задовольнятимуться.</para>

<para
>Щоб додати визначення вікна до списку, натисніть кнопку <guibutton
>Створити</guibutton
>. Ви можете вибрати <guimenuitem
>Активне вікно...</guimenuitem
>, якщо хочете, що скорочення вмикалося, лише якщо ви працюєте у певному вікні, або можете вибрати <guimenuitem
>Існуюче вікно...</guimenuitem
>, якщо ви бажаєте, щоб скорочення вмикалося, якщо вікно відкрито, незалежно від того, чи отримано цим вікном фокус. Вибір будь-якого з цих варіантів відкриває вікно, за допомогою якого ви можете змінити визначення вікон.</para>

<para
>Натисніть кнопку <guibutton
>Змінити...</guibutton
>, щоб змінити вже створений набір визначень вікон. У відповідь на натискання буде відкрито вікно редактора визначень. Щоб дізнатися більше, ознайомтеся з розділом <xref linkend="windows"/>.</para>

<para
>Щоб вилучити визначення вікна зі списку умов, натисніть кнопку <guibutton
>Вилучити</guibutton
>.</para>

</sect1>


<sect1 id="shortcuts">
<title
>Внесення змін до скорочень</title>

<para
>У лівій частині вікна типові скорочення розподілено за групами. Ці групи можна розгорнути натисканням кнопок зі стрілочками з метою перегляду скорочень групи.</para>

<para
>Типово, буде встановлено групи <guilabel
>KMenuEdit</guilabel
>, <guilabel
>Жести Konqueror</guilabel
> та <guilabel
>Приклади</guilabel
>. Програми можуть надавати у ваше розпорядження додаткові скорочення. Наприклад, &spectacle; додає групу <guilabel
>Знімки вікон</guilabel
>. У цій групі, якщо розгорнути її список, ви побачите декілька записів клавіатурних скорочень, зокрема <guilabel
>Запустити інструмент створення знімків</guilabel
>, якщо ви натиснете цей пункт, у правій частині буде відкрито панель з трьома вкладками:</para>

<sect2 id="shortcuts-comment">
<title
>Вкладка «Коментар»</title>

<para
>За допомогою вкладки <guilabel
>Коментар</guilabel
> ви можете описати спосіб використання скорочення, призначення скорочення та всі інші дані, які ви бажаєте вказати.</para>

</sect2>

<sect2 id="shortcuts-trigger">
<title
>Вкладка «Тригер»</title>
<para
>На вкладці <guilabel
>Тригер</guilabel
> буде показано пункти налаштування тригера, перелік яких залежить від типу вказаного тригера:</para>

<variablelist>

<varlistentry id="shortcuts-trigger-keyboard">
<term
>Загальне скорочення</term>
<listitem>

<para
>Щоб змінити клавіатурне скорочення, натисніть кнопку з зображенням гайкового ключа, а потім введіть бажане клавіатурне скорочення. Щоб вилучити скорочення, натисніть кнопку з піктограмою <inlinemediaobject
><imageobject
><imagedata fileref="oxygen-22x22-edit-clear-locationbar-rtl.png" format="PNG"/></imageobject
></inlinemediaobject
>, розташовану праворуч від кнопки зміни клавіатурного скорочення.</para>

<screenshot id="screenshot-shortcuts-trigger-keyboard">
<screeninfo
>Внесення змін до клавіатурного тригера</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-trigger-keyboard.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вкладка «Тригер» клавіатурного скорочення.</phrase
></textobject>
<caption
><para
>Зміна тригера клавіатурного скорочення.</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-trigger-window">
<term
>Дія для вікна</term>
<listitem>

<para
>Для дій для вікна передбачено декілька варіантів:</para>

<variablelist>

<varlistentry id="shortcuts-trigger-window-trigger">
<term
><guilabel
>Задіяти, якщо</guilabel
></term>
<listitem>

<para
>За допомогою цього пункту ви можете визначити певну дію для вікна, яку має бути виконано, що скороченням можна було скористатися. Можна вибрати один з таких варіантів:</para>

<itemizedlist>

<listitem
><para
><guilabel
>З’явиться вікно</guilabel
> — вмикається, якщо відкривається вікно.</para
></listitem>

<listitem
><para
><guilabel
>Вікно зникає</guilabel
> — вмикається, якщо вікно закривається.</para
></listitem>

<listitem
><para
><guilabel
>Вікно отримає фокус</guilabel
> — вмикається, коли ви перемикаєтеся на вікно.</para
></listitem>

<listitem
><para
><guilabel
>Вікно втрачає фокус</guilabel
> — вмикається, коли ви перемикаєтеся з вікна.</para
></listitem>

</itemizedlist>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-trigger-window-window">
<term
><guilabel
>Вікно</guilabel
></term>
<listitem
><para
>За допомогою цієї панелі ви можете визначити вікно або вікна, яких стосується тригер. Щоб дізнатися більше, ознайомтеся з розділом <xref linkend="windows"/>.</para
></listitem>
</varlistentry>

</variablelist>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-trigger-mouse">
<term
>Жести мишею</term>
<listitem>

<para
>Жест мишею може бути змінено натисканням кнопки <guibutton
>Змінити</guibutton
>, розташованої під панеллю, на якій показано жест мишею. У відповідь буде відкрито вікно. Натисніть і утримуйте ліву кнопку миші і намалюйте жест мишею у відповідній області вікна. Жест буде збережено після відпускання лівої кнопки миші.</para>

<screenshot id="screenshot-shortcuts-trigger-mouse">
<screeninfo
>Внесення змін у тригер жесту мишею</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-trigger-mouse.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вкладка «Тригер» для скорочення за жестом мишею.</phrase
></textobject>
<caption
><para
>Зміна тригер жесту мишею.</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

</variablelist>

</sect2>

<sect2 id="shortcuts-action">
<title
>Вкладка «Дія»</title>

<para
>За допомогою вкладки <guilabel
>Дія</guilabel
> ви можете налаштувати дію, яку буде виконано у разі вмикання скорочення. Передбачено декілька типів, для яких передбачено різні типи параметрів налаштовування:</para>

<variablelist>

<varlistentry id="shortcuts-action-command">
<term
>Команда/Адреса &URL;</term>
<listitem>

<para
>Якщо вибрано тригер «Команда або адреса &URL;», буде показано панель, за допомогою якої можна ввести команду, яку слід виконати, або адресу &URL;, яку слід відкрити у відповідь на використання скорочення. Ви можете також натиснути кнопку <guibutton
>Навігація</guibutton
>, розташовану праворуч від текстового поля, щоб відкрити діалогове вікно вибору файлів, за допомогою якого ви можете вибрати файл у локальній або віддаленій системі.</para>

<screenshot id="screenshot-shortcuts-action-command">
<screeninfo
>Внесення змін до дії «Команда»</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-action-command.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вкладка «Дія» для команди.</phrase
></textobject>
<caption
><para
>Внесення змін до дії «Команда».</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus">
<term
>Команда &DBus;</term>
<listitem>

<screenshot id="screenshot-shortcuts-action-dbus">
<screeninfo
>Внесення змін до дії &DBus;</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-action-dbus.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вкладка «Дія» для команди &DBus;.</phrase
></textobject>
<caption
><para
>Внесення змін до дії &DBus;.</para
></caption>
</mediaobject>
</screenshot>

<para
>Передбачено такі варіанти, за допомогою яких ви можете вказати &DBus;, який слід викликати:</para>

<variablelist>

<varlistentry id="shortcuts-action-dbus-application">
<term
><guilabel
>Віддалена програма</guilabel
></term>
<listitem
><para
>Назва служби віддаленої програми, метод якої слід викликати, наприклад <userinput
>org.kde.spectacle</userinput
>, якщо дію слід викликати у засобі для створення знімків &spectacle;.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-object">
<term
><guilabel
>Віддалений об'єкт</guilabel
></term>
<listitem
><para
>Шлях до віддаленого об’єкта методу, над яким слід виконати дію, наприклад <userinput
>/</userinput
> для дій зі створення знімків за допомогою &spectacle; або <userinput
>/Document/1</userinput
>, якщо ви хочете виконати метод над першим документом, відкритим у &kate;.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-function">
<term
><guilabel
>Функція</guilabel
></term>
<listitem
><para
>Назва методу &DBus;, який буде викликано, наприклад <userinput
>Fullscreen</userinput
>, якщо вам потрібен повноекранний знімок, або <userinput
>print</userinput
>, якщо ви бажаєте надрукувати документ.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-arguments">
<term
><guilabel
>Параметри</guilabel
></term>
<listitem
><para
>Вкажіть додаткові параметри методу &DBus;, який буде викликано.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-call">
<term
><guilabel
>Виклик</guilabel
></term>
<listitem
><para
>За допомогою цієї кнопки можна перевірити, чи усе працює як слід.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-launch-dbus-browser">
<term
><guilabel
>Запустити навігатор D-Bus</guilabel
></term>
<listitem
><para
>Запустити програму <application
>QDBusViewer</application
> для навігації методами та параметрами &DBus; для запущеної програми.</para
></listitem>
</varlistentry>

</variablelist>

<para
>Щоб дізнатися більше про &DBus;, ознайомтеся з <ulink url="http://techbase.kde.org/Development/Tutorials/D-Bus/Introduction"
>Вступом до технології &DBus; на TechBase &kde;</ulink
>.</para>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-action-keyboard">
<term
>Надіслати ввід з клавіатури</term>
<listitem>

<para
>У верхній частині вкладки <guilabel
>Дія</guilabel
> розташовано велику область для введення тексту, до якої ви можете ввести послідовність натискань клавіш, які слід надіслати у відповідь на використання скорочення.</para>

<para
>Більшість клавіш відповідають одному символу, отже, щоб ввести їх, вам слід ввести відповідний символ. Наприклад, щоб ввести «А», просто натисніть <userinput
>А</userinput
>. Деякі з клавіш мають довші назви, і ви можете скористатися цими назвами. Наприклад, щоб натиснути клавішу &Alt;, просто введіть <userinput
>Alt</userinput
>. </para>

<para
>Окремі клавіші слід розділяти двокрапкою (<userinput
>:</userinput
>). Наприклад, щоб визначити <quote
>цьомки</quote
>, введіть <userinput
>Ц:Ь:О:М:К:И</userinput
>.</para>

<para
>Клавіші, які слід натискати одночасно, має бути відокремлено символом плюса. Наприклад, щоб визначити <keycombo action="simul"
>&Ctrl;<keycap
>C</keycap
></keycombo
>, введіть <userinput
>Ctrl+C</userinput
>.</para>

<tip>
<para
>Пам’ятайте, вам слід вводити набори символів точно так само, як їх має бути натиснуто на клавіатурі. Щоб вказати великі літери, вам слід додати клавішу &Shift;. Наприклад, щоб визначити «Привіт», введіть <userinput
>Shift+П:Р:И:В:І:Т</userinput
>.</para>

<para
>Все це стосується спеціальних символів. Наприклад, щоб ввести символ комерційного «at» на у англійський розкладці, введіть <userinput
>Shift+2</userinput
>. </para>
</tip>

<warning>
<para
>Дія, яку буде виконано, залежить від поточної вибраної розкладки клавіатури. Якщо ви зміните розкладку клавіатури і увімкнете скорочення, наслідки можуть бути непередбаченими.</para>
</warning>

<para
>Під полем для введення набору клавіш ви можете вибрати, у яке вікно буде спрямовано набір клавіш. Передбачено такі варіанти:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Активне вікно</guilabel
> — поточне активне вікно.</para
></listitem>

<listitem
><para
><guilabel
>Певне вікно</guilabel
> — вікно, яке можна описати за допомогою наведеної нижче форми. Докладніше про описання вікон можна дізнатися з розділу <xref linkend="windows"/>.</para
></listitem>

<listitem
><para
><guilabel
>Дія вікна</guilabel
> — якщо буде використано тип тригера «Дія вікна», вивести набір клавіш у вікно, яким було увімкнено скорочення.</para
></listitem>

</itemizedlist>

<screenshot id="screenshot-shortcuts-action-keyboard">
<screeninfo
>Внесення змін до дії «Надіслати ввід з клавіатури»</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-action-keyboard.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вкладка «Дія» для введення з клавіатури.</phrase
></textobject>
<caption
><para
>Внесення змін до дії «Надіслати ввід з клавіатури».</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

</variablelist>

</sect2>

</sect1>


<sect1 id="windows">
<title
>Визначення вікон</title>

<para
>На декількох з панелей модуля можна визначати список вікон. На всіх цих панелях використовується однаковий інтерфейс, у якому використано такі пункти:</para>

<variablelist>

<varlistentry id="windows-comment">
<term
><guilabel
>Коментар</guilabel
></term>
<listitem
><para
>Це просто поле для якогось інформативного фрагмента тексту, яким ви можете скористатися для опису тригера, до якого застосовується дія, або інша корисна інформація. Введені тут дані не використовуються системою.</para
></listitem>
</varlistentry>

<varlistentry id="windows-list">
<term
>Список вікон</term>
<listitem
><para
>Під полем <guilabel
>Коментар</guilabel
> ліворуч буде показано список всіх визначень вікон, передбачених для тригера. Для виконання дії над визначенням слід просто клацнути лівою кнопкою миші на пункті визначення.</para
></listitem>
</varlistentry>

<varlistentry id="windows-edit">
<term
><guibutton
>Змінити...</guibutton
></term>
<listitem>

<para
>Натисніть цю кнопку, щоб внести зміни до поточного позначеного визначення вікна. У відповідь буде відкрито нове вікно, за допомогою якого ви зможете виконати редагування. У цьому вікні містяться такі пункти:</para>

<variablelist>

<varlistentry id="windows-edit-data">
<term
><guilabel
>Дані вікна</guilabel
></term>
<listitem>

<para
>Тут можна описати вікно, якого має стосуватися тригер.</para>

<para
>У верхній частині передбачено поле <guilabel
>Коментар</guilabel
>, яке є суто інформативним, подібним до поля на вкладці основного вікна <guilabel
>Тригер</guilabel
>.</para>

<para
>Передбачено три характеристики вікна:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Заголовок вікна</guilabel
> — заголовок вікна, який має бути показано на смужці заголовка у верхній частині вікна.</para
></listitem>

<listitem
><para
><guilabel
>Клас вікна</guilabel
> — зазвичай назва програми.</para
></listitem>

<listitem
><para
><guilabel
>Роль вікна</guilabel
> — зазвичай назва класу &Qt;, відповідального за створення вікна.</para
></listitem>

</itemizedlist>

<para
>Для кожної характеристики вікна передбачено спадний список та поле для введення тексту під ним. У текстове поле можна ввести значення, за яким слід перевірити вікно. Ви можете вибрати у спадному списку варіант <guilabel
>Є</guilabel
>, якщо відповідність має бути повною, варіант <guilabel
>Містить</guilabel
>, якщо введений рядок має міститися десь у значенні вікна, або варіант <guilabel
>Збігається з формальним виразом</guilabel
>, якщо для визначення відповідності слід скористатися формальним виразом. Ви також можете визначити обернене визначення для всіх умов. Виберіть варіант <guilabel
>Не є важливим</guilabel
>, якщо ви не хочете, щоб виконувалася перевірка певної характеристики.</para>

<para
>Найпростішим способом визначення цих даних є відкриття бажаного вікна з наступним натисканням кнопки <guibutton
>Автовизначення</guibutton
> у нижній частині цього розділу і натисканням бажаного вікна тригера. Всі три характеристики вікна буде заповнено на основі інформації вказаного вікна. Після цього можна скоригувати параметри, якщо це потрібно.</para>

</listitem>
</varlistentry>

<varlistentry id="windows-edit-types">
<term
><guilabel
>Типи вікон</guilabel
></term>
<listitem>

<para
>За допомогою цієї панелі ви можете обмежити відповідність певним типом вікон. Передбачено такі пункти:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Звичайне</guilabel
> — звичайне вікно програми.</para
></listitem>

<listitem
><para
><guilabel
>Стільниця</guilabel
> — основна стільниця, яка, власне, є вікном особливого типу.</para
></listitem>

<listitem
><para
><guilabel
>Діалогове</guilabel
> — невеличке вікно, яке є частиною звичайної програми, наприклад вікно повідомлення або налаштовування.</para
></listitem>

<listitem
><para
><guilabel
>Швартоване</guilabel
> — невеличке вікно, яке можна з’єднувати з основним вікном програми або від’єднувати від цього основного вікна. </para
></listitem>

</itemizedlist>

</listitem>
</varlistentry>

</variablelist>

</listitem>
</varlistentry>

<varlistentry id="windows-new">
<term
><guibutton
>Створити...</guibutton
></term>
<listitem
><para
>Створити нове визначення вікна. Відкриває діалогове вікно, описане вище.</para
></listitem>
</varlistentry>

<varlistentry id="windows-duplicate">
<term
><guibutton
>Здублювати...</guibutton
></term>
<listitem
><para
>За допомогою цього пункту можна створити нове визначення вікна з тими самими специфікаціями, як і у поточному позначеному визначенні вікна. У відповідь буде відкрито вікно редагування, описане вище, отже ви зможете внести подальші зміни.</para
></listitem>
</varlistentry>

<varlistentry id="windows-delete">
<term
><guibutton
>Вилучити</guibutton
></term>
<listitem
><para
>Вилучає поточне позначене визначення вікна.</para
></listitem>
</varlistentry>

</variablelist>

</sect1>


<sect1 id="settings">
<title
>Параметри</title>

<para
>Після першого запуску модуля або після натискання кнопки <guibutton
>Параметри</guibutton
>, розташованої під лівою панеллю, ви отримаєте доступ до пунктів налаштовування на правій панелі:</para>

<variablelist>

<varlistentry id="settings-start-daemon">
<term
><guilabel
>Запускати фонову службу вводу при вході</guilabel
></term>
<listitem
><para
>За допомогою цього пункту ви можете визначити, чи слід активувати фонову програму, яка стежитиме за клавіатурними скороченнями і вмикатиме налаштовані дії. Типово пункт позначено.</para
></listitem>
</varlistentry>

<varlistentry id="settings-gestures">
<term
><guilabel
>Жести</guilabel
></term>
<listitem>
<para
>Позначте цей пункт, щоб увімкнути жести мишею.</para>

<para
>З жестами мишею пов’язано два пункти:</para>

<variablelist>

<varlistentry id="settings-gestures-timeout">
<term
><guilabel
>Час очікування</guilabel
></term>
<listitem
><para
>Визначає максимальну тривалість у мілісекундах стеження за жестом миші і розпізнавання його системою. </para
></listitem>
</varlistentry>

<varlistentry id="settings-gestures-mouse-button">
<term
><guilabel
>Кнопка миші</guilabel
></term>
<listitem
><para
>Визначає кнопку миші, яку буде використано для виконання жестів мишею. Зазвичай, <userinput
>1</userinput
> — ліва кнопка миші, <userinput
>2</userinput
> — права кнопка миші, а <userinput
>3</userinput
> — середня кнопка миші або коліщатко. Якщо на вашій миші є додаткові кнопки, ви можете скористатися цими кнопками.</para>

<note>
<para
>Кнопка 1 недоступна, отже жести миші не суперечитимуть звичайним діям у вашій системі.</para>
</note>
</listitem>
</varlistentry>

</variablelist>
</listitem>
</varlistentry>

</variablelist>

<screenshot id="screenshot-settings">
<screeninfo
>Параметри</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="settings.png" format="PNG"/></imageobject>
<textobject
><phrase
>Вікно «Параметри».</phrase
></textobject>
<caption
><para
>Редагування параметрів нетипових скорочень.</para
></caption>
</mediaobject>
</screenshot>

</sect1>


<sect1 id="credits">
<title
>Авторські права та ліцензування</title>

<para
>Особливі подяки за написання більшої частини цієї статті учасникові Google Code-In 2011 Subhashish Pradhan.</para>

<para
>Переклад українською: Юрій Чорноіван<email
>yurchor@ukr.net</email
></para
> 
&underFDL; &underGPL; </sect1>

</article>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
