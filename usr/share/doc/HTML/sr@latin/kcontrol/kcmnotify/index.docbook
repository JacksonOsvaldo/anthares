<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
<!ENTITY % addindex "IGNORE">
<!ENTITY % Serbian-Latin "INCLUDE"
> <!-- change language only here -->
]>

<article id="kcmnotify" lang="&language;">
<title
>Postavke sistemskih obaveštenja</title>
<articleinfo>

<authorgroup>
<author
><personname
><firstname
>Majk</firstname
> <surname
>Mekbrajd</surname
></personname
> &Mike.McBride.mail;</author>
<othercredit role="translator"
><firstname
>Dragan</firstname
><surname
>Pantelić</surname
><affiliation
><address
><email
>falcon-10@gmx.de</email
></address
></affiliation
><contrib
>prevod</contrib
></othercredit
> 
</authorgroup>

<date
>9. 4. 2015.</date>
<releaseinfo
>Plasma 5.3</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Sistemske postavke</keyword>
<keyword
>sistemsko obaveštenje</keyword>
<keyword
>obaveštenje</keyword>
</keywordset>
</articleinfo>

<sect1 id="sys-notify">
<title
>Postavke sistemskih obaveštenja</title>

<para
>Kao i sav softver, KDE mora da obavesti korisnika kad nastane neki problem, kad je završen zadatak, ili se nešto dogodilo. KDE koristi skup „sistemskih obaveštenja“ za informisanje korisnika o događanjima.</para>

<para
>U ovom modulu možete odrediti na koji način KDE objavljuje pojedine događaje.</para>

<para
>Da biste podesili obaveštenje, izaberite program iz padajućeg spiska <guilabel
>Izvor događaja:</guilabel
> na vrhu dijaloga. To će vas prebaciti na spisak podesivih obaveštenja za taj program. Spisak obaveštenja sadrži šest kolona levo od naziva obaveštenja. S leva na desno, kolone su:</para>

<variablelist>
<varlistentry>
<term
><guiicon
>Pusti zvuk</guiicon
></term>
<listitem
><para
>Ovo izvodi tačno ono što mislite. Ako je ikonica prisutna u ovoj koloni, KDE će preko zvučnika pustiti zadati zvuk. U KDE‑u se obično koristi u igrama, za otpočinjanje nove igre ili kakvu radnju tokom igre.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Prikaži iskačuću poruku</guiicon
></term>
<listitem
><para
>Ako je ikonica prisutna u ovoj koloni, KDE će otvoriti dijalog s porukom koja korisniku ispostavlja obaveštenje. Ovo je verovatno najobičnija opcija za upozoravanje korisnika na grešku.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Zabeleži u fajl</guiicon
></term>
<listitem
><para
>Ako je ikonica prisutna u ovoj koloni, KDE će ispisati neke podatke u fajl na disku, radi kasnijeg dobavljanja. Korisno za praćenje problema ili važnih sistemskih izmena.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Označi stavku trake zadataka</guiicon
></term>
<listitem
><para
>Ako je ikonica prisutna u ovoj koloni, KDE će učiniti da stavka trake zadataka trepće dok korisnik ne klikne na nju. Najkorisnije kad želite da korisnik pogleda u program (npr. po prijemu nove poruke e‑pošte, ili pominjanju korisnikovog nadimka na <acronym
>IRC</acronym
> kanalu).</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Govor</guilabel
></term>
<listitem
><para
>Ako je kućica popunjena, KDE će učiniti da stavka programa koji šalje obaveštenje na traci zadataka trepće dok korisnik ne klikne na nju. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><guiicon
>Izvrši naredbu</guiicon
></term>
<listitem
><para
>Ako je ikonica prisutna u ovoj koloni, obaveštenje se izvodi izvršavanjem zasebne naredbe. Može se uposliti za izvršenje naredbe koja će pomoći u povraćanju podataka, ugasiti potencijalno kompromitovani sistem, ili poslati e‑poštu drugom korisniku kao pozor na problem.</para
></listitem>
</varlistentry>

</variablelist>

<sect2
><title
>Izmena obaveštenja</title>
<para
>Da izmenite obaveštenje, kliknite <mousebutton
>levim</mousebutton
> na naziv obaveštenja, po čemu će ono biti istaknuto i aktivirane kućice za sve tipove obaveštenja.</para>

<tip
><para
>Možete zadati da jedno obaveštenje okine više od jednog događaja. Na primer, da se i odsvira zvuk i pojavi dijalog u odgovor na sistemsko obaveštenje. Jedno obaveštenje ne sprečava funkcionisanje ostalih.</para
></tip>

<para
>Sledeći spisak nabraja detalje svakog tipa obaveštenja i način njihovog korišćenja. </para>

<variablelist>
<varlistentry>
<term
><guilabel
>Pusti zvuk</guilabel
></term>
<listitem
><para
>Ako je kućica popunjena, KDE će odsvirati zvuk svaki na svako pristizanje ovog obaveštenja. Da biste zadali zvuk, u polje desno od kućice unesite lokaciju zvučnog fajla koji hoćete da KDE pusti. Pritom možete upotrebiti dugme fascikle (desno od polja) za pregledanje stabla fascikli. Da biste isprobali zvuk, kliknite na dugme za puštanje (malo dugme desno od <guilabel
>Pusti zvuk</guilabel
>).</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Prikaži iskačuću poruku</guilabel
></term>
<listitem
><para
>Ako je kućica popunjena, KDE će korisniku ispostavljati obaveštenja preko dijaloga s porukom. Tekst poruke se ne može izmeniti iz ovog dijaloga.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Zabeleži u fajl</guilabel
></term>
<listitem
><para
>Ako je kućica popunjena, KDE će upisati neke podatke u fajl na disku, odakle se mogu kasnije dobaviti. Fajl dnevnika zadajete unošenjem njegove putanju u polje desno od kućice. Pritom možete upotrebiti dugme fascikle (desno od polja) za pregledanje stabla fascikli. </para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Označi stavku trake zadataka</guilabel
></term>
<listitem
><para
>Ako je kućica popunjena, KDE će učiniti da stavka programa koji šalje obaveštenje na traci zadataka trepće dok korisnik ne klikne na nju.</para
></listitem>
</varlistentry>

<varlistentry>
<term
><guilabel
>Izvrši naredbu</guilabel
></term>
<listitem
><para
>Ako je kućica popunjena, obaveštenje se izvodi izvršavanjem zasebne naredbe. Da biste zadali naredbu koju treba izvršiti, unesite njenu putanju u polje desno od kućice. Pritom možete upotrebiti dugme fascikle (desno od polja) za pregledanje stabla fascikli.</para
></listitem>
</varlistentry>
<!--not in 5.3
<varlistentry>
<term
><guilabel
>Speech</guilabel
></term>
<listitem
><para
>If a mark is in this checkbox, &kde; will use Jovie to speak the event 
message, event name or custom text.</para>
<para
>If you select <guilabel
>Speak Custom Text</guilabel
>, enter the text in the box.  
You may use the following 
substitution strings in the text:
<simplelist>
<member
> <userinput
>%e</userinput
> Name of the event </member>
<member
> <userinput
>%a</userinput
> Application that sent the event </member>
<member
> <userinput
>%m</userinput
> Message sent by the application </member>
</simplelist>
</para
></listitem>
</varlistentry>
-->
</variablelist>

<tip
><para
>Možete koristiti kolone ikonica levo od obaveštenja za brzo uključivanje i isključivanje opcija. Klik <mousebutton
>levim</mousebutton
> u kolonu isključiće ili uključiti obaveštenje.</para
></tip>
</sect2>

</sect1>

</article>
