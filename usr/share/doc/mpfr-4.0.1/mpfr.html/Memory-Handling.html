<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents how to install and use the Multiple Precision
Floating-Point Reliable Library, version 4.0.1.

Copyright 1991, 1993-2018 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU Free Documentation License, Version 1.2 or any later
version published by the Free Software Foundation; with no Invariant Sections,
with no Front-Cover Texts, and with no Back-Cover Texts.  A copy of the
license is included in GNU Free Documentation License. -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Memory Handling (GNU MPFR 4.0.1)</title>

<meta name="description" content="How to install and use GNU MPFR, a library for reliable multiple precision
floating-point arithmetic, version 4.0.1.">
<meta name="keywords" content="Memory Handling (GNU MPFR 4.0.1)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Concept-Index.html#Concept-Index" rel="index" title="Concept Index">
<link href="MPFR-Basics.html#MPFR-Basics" rel="up" title="MPFR Basics">
<link href="Getting-the-Best-Efficiency-Out-of-MPFR.html#Getting-the-Best-Efficiency-Out-of-MPFR" rel="next" title="Getting the Best Efficiency Out of MPFR">
<link href="Exceptions.html#Exceptions" rel="prev" title="Exceptions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<a name="Memory-Handling"></a>
<div class="header">
<p>
Next: <a href="Getting-the-Best-Efficiency-Out-of-MPFR.html#Getting-the-Best-Efficiency-Out-of-MPFR" accesskey="n" rel="next">Getting the Best Efficiency Out of MPFR</a>, Previous: <a href="Exceptions.html#Exceptions" accesskey="p" rel="prev">Exceptions</a>, Up: <a href="MPFR-Basics.html#MPFR-Basics" accesskey="u" rel="up">MPFR Basics</a> &nbsp; [<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Memory-Handling-1"></a>
<h3 class="section">4.7 Memory Handling</h3>

<p>MPFR functions may create caches, e.g., when computing constants such
as <em>Pi</em>, either because the user has called a function like
<code>mpfr_const_pi</code> directly or because such a function was called
internally by the MPFR library itself to compute some other function.
When more precision is needed, the value is automatically recomputed;
a minimum of 10% increase of the precision is guaranteed to avoid too
many recomputations.
</p>
<p>MPFR functions may also create thread-local pools for internal use
to avoid the cost of memory allocation. The pools can be freed with
<code>mpfr_free_pool</code> (but with a default MPFR build, they should not
take much memory, as the allocation size is limited).
</p>
<p>At any time, the user can free various caches and pools with
<code>mpfr_free_cache</code> and <code>mpfr_free_cache2</code>. It is strongly advised
to free thread-local caches before terminating a thread, and all caches
before exiting when using tools like &lsquo;<samp>valgrind</samp>&rsquo; (to avoid memory leaks
being reported).
</p>
<p>MPFR allocates its memory either on the stack (for temporary memory only)
or with the same allocator as the one configured for GMP:
see Section &ldquo;Custom Allocation&rdquo; in <cite>GNU MP</cite>.
This means that the application must make sure that data allocated with the
current allocator will not be reallocated or freed with a new allocator.
So, in practice, if an application needs to change the allocator with
<code>mp_set_memory_functions</code>, it should first free all data allocated
with the current allocator: for its own data, with <code>mpfr_clear</code>,
etc.; for the caches and pools, with <code>mpfr_mp_memory_cleanup</code> in
all threads where MPFR is potentially used. This function is currently
equivalent to <code>mpfr_free_cache</code>, but <code>mpfr_mp_memory_cleanup</code>
is the recommended way in case the allocation method changes in the future
(for instance, one may choose to allocate the caches for floating-point
constants with <code>malloc</code> to avoid freeing them if the allocator
changes). Developers should also be aware that MPFR may also be used
indirectly by libraries, so that libraries based on MPFR should provide
a clean-up function calling <code>mpfr_mp_memory_cleanup</code> and/or warn
their users about this issue.
</p>
<p>Note: For multithreaded applications, the allocator must be valid in
all threads where MPFR may be used; data allocated in one thread may
be reallocated and/or freed in some other thread.
</p>
<p>MPFR internal data such as flags, the exponent range, the default
precision and rounding mode, and caches (i.e., data that are not
accessed via parameters) are either global (if MPFR has not been
compiled as thread safe) or per-thread (thread local storage, TLS).
The initial values of TLS data after a thread is created entirely
depend on the compiler and thread implementation (MPFR simply does
a conventional variable initialization, the variables being declared
with an implementation-defined TLS specifier).
</p>
<p>Writers of libraries using MPFR should be aware that the application and/or
another library used by the application may also use MPFR, so that changing
the exponent range, the default precision, or the default rounding mode may
have an effect on this other use of MPFR since these data are not duplicated
(unless they are in a different thread). Therefore any such value changed in
a library function should be restored before the function returns (unless
the purpose of the function is to do such a change). Writers of software
using MPFR should also be careful when changing such a value if they use
a library using MPFR (directly or indirectly), in order to make sure that
such a change is compatible with the library.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Getting-the-Best-Efficiency-Out-of-MPFR.html#Getting-the-Best-Efficiency-Out-of-MPFR" accesskey="n" rel="next">Getting the Best Efficiency Out of MPFR</a>, Previous: <a href="Exceptions.html#Exceptions" accesskey="p" rel="prev">Exceptions</a>, Up: <a href="MPFR-Basics.html#MPFR-Basics" accesskey="u" rel="up">MPFR Basics</a> &nbsp; [<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
