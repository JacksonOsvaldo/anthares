<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta charset="UTF-8" />
<title>Delegates - Vala Reference Manual</title>
<link rel="stylesheet" type="text/css" href="default.css"><meta name="viewport" content="initial-scale=1">
</head>
<body>
<div class="o-fixedtop c-navbar"><div class="o-navbar">
<span class="c-pageturner u-float-left"><a href="index.html">Contents</a></span><span>Vala Reference Manual</span><div class="u-float-right">
<span class="c-pageturner o-inlinewidth-4"><a href="Methods.html">Prev</a></span><span class="c-pageturner o-inlinewidth-4"><a href="Errors.html">Next</a></span>
</div>
</div></div>
<h2>8. Delegates</h2>
<ul class="page_toc">
<li><a href="Delegates.html#Types_of_delegate">8.1 Types of delegate</a></li>
<li><a href="Delegates.html#Delegate_declaration">8.2 Delegate declaration</a></li>
<li><a href="Delegates.html#Using_delegates">8.3 Using delegates</a></li>
<li><a href="Delegates.html#Examples">8.4 Examples</a></li>
</ul>
<p>A delegate declaration defines a method type: a type that can be invoked, accepting a set of values of certain types, and returning a value of a set type. In Vala, methods are not first-class objects, and as such cannot be created dynamically; however, any method can be considered to be an instance of a delegate's type, provided that the method signature matches that of the delegate. </p>
<p>Methods are considered to be an immutable reference type.  Any method can be referred to by name as an expression returning a reference to that method - this can be assigned to a field (or variable, or parameter), or else invoked directly as a standard method invocation (see <a href="https://wiki.gnome.org/Projects/Vala/Manual/Export/Projects/Vala/Manual/Expressions#Invocation_expressions">Expressions/Invocation expressions</a>). </p>

<h3 id="Types_of_delegate">8.1 Types of delegate</h3>
<p>All delegate types in Vala are defined to be either static or instance delegates.  This refers to whether the methods that may be considered instances of the delegate type are instance members of classes or structs, or not. </p>
<p>To assign an instance of an instance delegate, you must give the method name qualified with an identifier that refers to a class or struct instance.  When an instance of an instance delegate is invoked, the method will act as though the method had been invoked directly: the "this" keyword will be usuable, instance data will be accessible, etc. </p>
<p>Instance and static delegate instances are not interchangeable. </p>

<h3 id="Delegate_declaration">8.2 Delegate declaration</h3>
<p>The syntax for declaring a delegate changes slightly based on what sort of delegate is being declared.  This section shows the form for a namespace delegate.  Many of the parts of the declaration are common to all types, so sections from here are referenced from class delegates, interface delegates, etc. </p>
<blockquote class="o-box c-rules">delegate-declaration:
	instance-delegate-declaration
	static-delegate-declaration

instance-delegate-declaration:
	[ access-modifier ] <span class="literal">delegate</span> return-type qualified-delegate-name <span class="literal">(</span> method-params-list <span class="literal">)</span> [ <span class="literal">throws</span> error-list ] <span class="literal">;</span>

static-delegate-declaration:
	[ access-modifier ] <span class="literal">static</span><span class="literal">delegate</span> return-type qualified-delegate-name <span class="literal">(</span> method-params-list <span class="literal">)</span> [ <span class="literal">throws</span> error-list ] <span class="literal">;</span>

qualified-delegate-name:
	[ qualified-namespace-name <span class="literal">.</span> ] delegate-name

delegate-name:
	identifier

</blockquote>
<p>Parts of this syntax are based on the respective sections of the method decleration syntax (see <a href="https://wiki.gnome.org/Projects/Vala/Manual/Export/Projects/Vala/Manual/Methods#">Methods</a> for details). </p>

<h3 id="Using_delegates">8.3 Using delegates</h3>
<p>A delegate declaration defines a type.  Instances of this type can then be assigned to variables (or fields, or paramaters) of this type.  Vala does not allow creating methods at runtime, and so the values of delegate-type instances will be references to methods known at compile time.  To simplify the process, inlined methods may be written (see <a href="https://wiki.gnome.org/Projects/Vala/Manual/Export/Projects/Vala/Manual/Methods#Lambdas">Methods/Lambdas</a>). </p>
<p>To call the method referenced by a delegate-type instance, use the same notation as for calling a method; instead of giving the method's name, give the identifier of the variable, as described in <a href="https://wiki.gnome.org/Projects/Vala/Manual/Export/Projects/Vala/Manual/Expressions#Invocation_expressions">Expressions/Invocation expressions</a>. </p>

<h3 id="Examples">8.4 Examples</h3>
<p>Defining delegates: </p>
<pre class="o-box c-program"><span class="c-program-comment">// Static delegate taking two ints, returning void:</span>
<span class="c-program-comment"></span><span class="c-program-token">static</span> <span class="c-program-token">void</span> <span class="c-program-methodname">DelegateName</span> (<span class="c-program-token">int</span> <span class="c-program-methodname">a</span>, <span class="c-program-token">int</span> <span class="c-program-methodname">b</span>);

<span class="c-program-comment">// Instance delegate with the same signature:</span>
<span class="c-program-comment"></span><span class="c-program-token">void</span> <span class="c-program-methodname">DelegateName</span> (<span class="c-program-token">int</span> <span class="c-program-methodname">a</span>, <span class="c-program-token">int</span> <span class="c-program-methodname">b</span>);

<span class="c-program-comment">// Static delegate which may throw an error:</span>
<span class="c-program-comment"></span><span class="c-program-token">static</span> <span class="c-program-token">void</span> <span class="c-program-methodname">DelegateName</span> () <span class="c-program-token">throws</span> <span class="c-program-methodname">GLib</span>.<span class="c-program-methodname">Error</span>;
</pre>
<p>Invoking delegates, and passing as parameters. </p>
<pre class="o-box c-program"><span class="c-program-token">void</span> <span class="c-program-methodname">f1</span>(<span class="c-program-token">int</span> <span class="c-program-methodname">a</span>) { <span class="c-program-methodname">stdout</span>.<span class="c-program-methodname">printf</span>(<span class="c-program-phrase">"</span><span class="c-program-phrase">%d</span><span class="c-program-phrase">\n</span><span class="c-program-phrase">"</span>, <span class="c-program-methodname">a</span>); }
...
<span class="c-program-token">void</span> <span class="c-program-methodname">f2</span>(<span class="c-program-methodname">DelegateType</span> <span class="c-program-methodname">d</span>, <span class="c-program-token">int</span> <span class="c-program-methodname">a</span>) {
        <span class="c-program-methodname">d</span>(<span class="c-program-methodname">a</span>);
}
...
<span class="c-program-methodname">f2</span>(<span class="c-program-methodname">f1</span>, 5);
</pre>
<p>Instance delegates: </p>
<pre class="o-box c-program"><span class="c-program-token">class</span> <span class="c-program-methodname">Test</span> : <span class="c-program-methodname">Object</span> {
        <span class="c-program-token">private</span> <span class="c-program-token">int</span> <span class="c-program-methodname">data</span> = 5;
        <span class="c-program-token">public</span> <span class="c-program-token">void</span> <span class="c-program-methodname">method</span> (<span class="c-program-token">int</span> <span class="c-program-methodname">a</span>) {
                <span class="c-program-methodname">stdout</span>.<span class="c-program-methodname">printf</span>(<span class="c-program-phrase">"</span><span class="c-program-phrase">%d %d</span><span class="c-program-phrase">\n</span><span class="c-program-phrase">"</span>, <span class="c-program-methodname">a</span>, <span class="c-program-token">this</span>.<span class="c-program-methodname">data</span>);
        }
}

<span class="c-program-token">delegate</span> <span class="c-program-token">void</span> <span class="c-program-methodname">DelegateType</span> (<span class="c-program-token">int</span> <span class="c-program-methodname">a</span>);

<span class="c-program-token">public</span> <span class="c-program-token">static</span> <span class="c-program-token">void</span> <span class="c-program-methodname">main</span> (<span class="c-program-token">string</span>[] <span class="c-program-methodname">args</span>) {
        <span class="c-program-token">var</span> <span class="c-program-methodname">t</span> = <span class="c-program-token">new</span> <span class="c-program-methodname">Test</span>();
        <span class="c-program-methodname">DelegateType</span> <span class="c-program-methodname">d</span> = <span class="c-program-methodname">t</span>.<span class="c-program-methodname">method</span>;
        
        <span class="c-program-methodname">d</span>(1);
}
</pre>
<p>With Lambda: </p>
<pre class="o-box c-program"><span class="c-program-methodname">f2</span>(<span class="c-program-methodname">a</span> =&gt; { <span class="c-program-methodname">stdout</span>.<span class="c-program-methodname">printf</span>(<span class="c-program-phrase">"</span><span class="c-program-phrase">%d</span><span class="c-program-phrase">\n</span><span class="c-program-phrase">"</span>, <span class="c-program-methodname">a</span>); }, 5);
</pre>
</body>
</html>
