
#ifndef KDECORATIONS2_EXPORT_H
#define KDECORATIONS2_EXPORT_H

#ifdef KDECORATIONS2_STATIC_DEFINE
#  define KDECORATIONS2_EXPORT
#  define KDECORATIONS2_NO_EXPORT
#else
#  ifndef KDECORATIONS2_EXPORT
#    ifdef kdecorations2_EXPORTS
        /* We are building this library */
#      define KDECORATIONS2_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KDECORATIONS2_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KDECORATIONS2_NO_EXPORT
#    define KDECORATIONS2_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KDECORATIONS2_DEPRECATED
#  define KDECORATIONS2_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KDECORATIONS2_DEPRECATED_EXPORT
#  define KDECORATIONS2_DEPRECATED_EXPORT KDECORATIONS2_EXPORT KDECORATIONS2_DEPRECATED
#endif

#ifndef KDECORATIONS2_DEPRECATED_NO_EXPORT
#  define KDECORATIONS2_DEPRECATED_NO_EXPORT KDECORATIONS2_NO_EXPORT KDECORATIONS2_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KDECORATIONS2_NO_DEPRECATED
#    define KDECORATIONS2_NO_DEPRECATED
#  endif
#endif

#endif
