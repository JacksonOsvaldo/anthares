
#ifndef KBOOKMARKS_EXPORT_H
#define KBOOKMARKS_EXPORT_H

#ifdef KBOOKMARKS_STATIC_DEFINE
#  define KBOOKMARKS_EXPORT
#  define KBOOKMARKS_NO_EXPORT
#else
#  ifndef KBOOKMARKS_EXPORT
#    ifdef KF5Bookmarks_EXPORTS
        /* We are building this library */
#      define KBOOKMARKS_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KBOOKMARKS_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KBOOKMARKS_NO_EXPORT
#    define KBOOKMARKS_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KBOOKMARKS_DEPRECATED
#  define KBOOKMARKS_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KBOOKMARKS_DEPRECATED_EXPORT
#  define KBOOKMARKS_DEPRECATED_EXPORT KBOOKMARKS_EXPORT KBOOKMARKS_DEPRECATED
#endif

#ifndef KBOOKMARKS_DEPRECATED_NO_EXPORT
#  define KBOOKMARKS_DEPRECATED_NO_EXPORT KBOOKMARKS_NO_EXPORT KBOOKMARKS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KBOOKMARKS_NO_DEPRECATED
#    define KBOOKMARKS_NO_DEPRECATED
#  endif
#endif

#endif
