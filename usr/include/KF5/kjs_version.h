// This file was generated by ecm_setup_version(): DO NOT EDIT!

#ifndef KJS_VERSION_H
#define KJS_VERSION_H

#define KJS_VERSION_STRING "5.43.0"
#define KJS_VERSION_MAJOR 5
#define KJS_VERSION_MINOR 43
#define KJS_VERSION_PATCH 0
#define KJS_VERSION ((5<<16)|(43<<8)|(0))

#endif
