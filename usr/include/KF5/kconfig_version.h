// This file was generated by ecm_setup_version(): DO NOT EDIT!

#ifndef KCONFIG_VERSION_H
#define KCONFIG_VERSION_H

#define KCONFIG_VERSION_STRING "5.43.0"
#define KCONFIG_VERSION_MAJOR 5
#define KCONFIG_VERSION_MINOR 43
#define KCONFIG_VERSION_PATCH 0
#define KCONFIG_VERSION ((5<<16)|(43<<8)|(0))

#endif
