
#ifndef KNEWSTUFFCORE_EXPORT_H
#define KNEWSTUFFCORE_EXPORT_H

#ifdef KNEWSTUFFCORE_STATIC_DEFINE
#  define KNEWSTUFFCORE_EXPORT
#  define KNEWSTUFFCORE_NO_EXPORT
#else
#  ifndef KNEWSTUFFCORE_EXPORT
#    ifdef KF5NewStuffCore_EXPORTS
        /* We are building this library */
#      define KNEWSTUFFCORE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KNEWSTUFFCORE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KNEWSTUFFCORE_NO_EXPORT
#    define KNEWSTUFFCORE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KNEWSTUFFCORE_DEPRECATED
#  define KNEWSTUFFCORE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KNEWSTUFFCORE_DEPRECATED_EXPORT
#  define KNEWSTUFFCORE_DEPRECATED_EXPORT KNEWSTUFFCORE_EXPORT KNEWSTUFFCORE_DEPRECATED
#endif

#ifndef KNEWSTUFFCORE_DEPRECATED_NO_EXPORT
#  define KNEWSTUFFCORE_DEPRECATED_NO_EXPORT KNEWSTUFFCORE_NO_EXPORT KNEWSTUFFCORE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KNEWSTUFFCORE_NO_DEPRECATED
#    define KNEWSTUFFCORE_NO_DEPRECATED
#  endif
#endif

#endif
