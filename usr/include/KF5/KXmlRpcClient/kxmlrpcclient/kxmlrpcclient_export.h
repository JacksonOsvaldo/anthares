
#ifndef KXMLRPCCLIENT_EXPORT_H
#define KXMLRPCCLIENT_EXPORT_H

#ifdef KXMLRPCCLIENT_STATIC_DEFINE
#  define KXMLRPCCLIENT_EXPORT
#  define KXMLRPCCLIENT_NO_EXPORT
#else
#  ifndef KXMLRPCCLIENT_EXPORT
#    ifdef KF5XmlRpcClient_EXPORTS
        /* We are building this library */
#      define KXMLRPCCLIENT_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KXMLRPCCLIENT_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KXMLRPCCLIENT_NO_EXPORT
#    define KXMLRPCCLIENT_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KXMLRPCCLIENT_DEPRECATED
#  define KXMLRPCCLIENT_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KXMLRPCCLIENT_DEPRECATED_EXPORT
#  define KXMLRPCCLIENT_DEPRECATED_EXPORT KXMLRPCCLIENT_EXPORT KXMLRPCCLIENT_DEPRECATED
#endif

#ifndef KXMLRPCCLIENT_DEPRECATED_NO_EXPORT
#  define KXMLRPCCLIENT_DEPRECATED_NO_EXPORT KXMLRPCCLIENT_NO_EXPORT KXMLRPCCLIENT_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KXMLRPCCLIENT_NO_DEPRECATED
#    define KXMLRPCCLIENT_NO_DEPRECATED
#  endif
#endif

#endif
