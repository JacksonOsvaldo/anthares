
#ifndef KIDLETIME_EXPORT_H
#define KIDLETIME_EXPORT_H

#ifdef KIDLETIME_STATIC_DEFINE
#  define KIDLETIME_EXPORT
#  define KIDLETIME_NO_EXPORT
#else
#  ifndef KIDLETIME_EXPORT
#    ifdef KF5IdleTime_EXPORTS
        /* We are building this library */
#      define KIDLETIME_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KIDLETIME_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KIDLETIME_NO_EXPORT
#    define KIDLETIME_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KIDLETIME_DEPRECATED
#  define KIDLETIME_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KIDLETIME_DEPRECATED_EXPORT
#  define KIDLETIME_DEPRECATED_EXPORT KIDLETIME_EXPORT KIDLETIME_DEPRECATED
#endif

#ifndef KIDLETIME_DEPRECATED_NO_EXPORT
#  define KIDLETIME_DEPRECATED_NO_EXPORT KIDLETIME_NO_EXPORT KIDLETIME_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KIDLETIME_NO_DEPRECATED
#    define KIDLETIME_NO_DEPRECATED
#  endif
#endif

#endif
