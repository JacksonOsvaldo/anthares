
#ifndef KDECLARATIVE_EXPORT_H
#define KDECLARATIVE_EXPORT_H

#ifdef KDECLARATIVE_STATIC_DEFINE
#  define KDECLARATIVE_EXPORT
#  define KDECLARATIVE_NO_EXPORT
#else
#  ifndef KDECLARATIVE_EXPORT
#    ifdef KF5Declarative_EXPORTS
        /* We are building this library */
#      define KDECLARATIVE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KDECLARATIVE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KDECLARATIVE_NO_EXPORT
#    define KDECLARATIVE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KDECLARATIVE_DEPRECATED
#  define KDECLARATIVE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KDECLARATIVE_DEPRECATED_EXPORT
#  define KDECLARATIVE_DEPRECATED_EXPORT KDECLARATIVE_EXPORT KDECLARATIVE_DEPRECATED
#endif

#ifndef KDECLARATIVE_DEPRECATED_NO_EXPORT
#  define KDECLARATIVE_DEPRECATED_NO_EXPORT KDECLARATIVE_NO_EXPORT KDECLARATIVE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KDECLARATIVE_NO_DEPRECATED
#    define KDECLARATIVE_NO_DEPRECATED
#  endif
#endif

#endif
