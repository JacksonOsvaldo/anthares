
#ifndef BALOO_CORE_EXPORT_H
#define BALOO_CORE_EXPORT_H

#ifdef BALOO_CORE_STATIC_DEFINE
#  define BALOO_CORE_EXPORT
#  define BALOO_CORE_NO_EXPORT
#else
#  ifndef BALOO_CORE_EXPORT
#    ifdef KF5Baloo_EXPORTS
        /* We are building this library */
#      define BALOO_CORE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define BALOO_CORE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef BALOO_CORE_NO_EXPORT
#    define BALOO_CORE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef BALOO_CORE_DEPRECATED
#  define BALOO_CORE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef BALOO_CORE_DEPRECATED_EXPORT
#  define BALOO_CORE_DEPRECATED_EXPORT BALOO_CORE_EXPORT BALOO_CORE_DEPRECATED
#endif

#ifndef BALOO_CORE_DEPRECATED_NO_EXPORT
#  define BALOO_CORE_DEPRECATED_NO_EXPORT BALOO_CORE_NO_EXPORT BALOO_CORE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef BALOO_CORE_NO_DEPRECATED
#    define BALOO_CORE_NO_DEPRECATED
#  endif
#endif

#endif
