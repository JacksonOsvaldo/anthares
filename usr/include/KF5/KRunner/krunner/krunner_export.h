
#ifndef KRUNNER_EXPORT_H
#define KRUNNER_EXPORT_H

#ifdef KRUNNER_STATIC_DEFINE
#  define KRUNNER_EXPORT
#  define KRUNNER_NO_EXPORT
#else
#  ifndef KRUNNER_EXPORT
#    ifdef KF5Runner_EXPORTS
        /* We are building this library */
#      define KRUNNER_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KRUNNER_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KRUNNER_NO_EXPORT
#    define KRUNNER_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KRUNNER_DEPRECATED
#  define KRUNNER_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KRUNNER_DEPRECATED_EXPORT
#  define KRUNNER_DEPRECATED_EXPORT KRUNNER_EXPORT KRUNNER_DEPRECATED
#endif

#ifndef KRUNNER_DEPRECATED_NO_EXPORT
#  define KRUNNER_DEPRECATED_NO_EXPORT KRUNNER_NO_EXPORT KRUNNER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KRUNNER_NO_DEPRECATED
#    define KRUNNER_NO_DEPRECATED
#  endif
#endif

#endif
