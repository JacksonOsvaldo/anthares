
#ifndef KDESU_EXPORT_H
#define KDESU_EXPORT_H

#ifdef KDESU_STATIC_DEFINE
#  define KDESU_EXPORT
#  define KDESU_NO_EXPORT
#else
#  ifndef KDESU_EXPORT
#    ifdef KF5Su_EXPORTS
        /* We are building this library */
#      define KDESU_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KDESU_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KDESU_NO_EXPORT
#    define KDESU_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KDESU_DEPRECATED
#  define KDESU_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KDESU_DEPRECATED_EXPORT
#  define KDESU_DEPRECATED_EXPORT KDESU_EXPORT KDESU_DEPRECATED
#endif

#ifndef KDESU_DEPRECATED_NO_EXPORT
#  define KDESU_DEPRECATED_NO_EXPORT KDESU_NO_EXPORT KDESU_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KDESU_NO_DEPRECATED
#    define KDESU_NO_DEPRECATED
#  endif
#endif

#endif
