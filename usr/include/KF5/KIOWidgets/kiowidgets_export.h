
#ifndef KIOWIDGETS_EXPORT_H
#define KIOWIDGETS_EXPORT_H

#ifdef KIOWIDGETS_STATIC_DEFINE
#  define KIOWIDGETS_EXPORT
#  define KIOWIDGETS_NO_EXPORT
#else
#  ifndef KIOWIDGETS_EXPORT
#    ifdef KF5KIOWidgets_EXPORTS
        /* We are building this library */
#      define KIOWIDGETS_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KIOWIDGETS_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KIOWIDGETS_NO_EXPORT
#    define KIOWIDGETS_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KIOWIDGETS_DEPRECATED
#  define KIOWIDGETS_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KIOWIDGETS_DEPRECATED_EXPORT
#  define KIOWIDGETS_DEPRECATED_EXPORT KIOWIDGETS_EXPORT KIOWIDGETS_DEPRECATED
#endif

#ifndef KIOWIDGETS_DEPRECATED_NO_EXPORT
#  define KIOWIDGETS_DEPRECATED_NO_EXPORT KIOWIDGETS_NO_EXPORT KIOWIDGETS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KIOWIDGETS_NO_DEPRECATED
#    define KIOWIDGETS_NO_DEPRECATED
#  endif
#endif

#endif
