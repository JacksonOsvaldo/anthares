
#ifndef KEMOTICONS_EXPORT_H
#define KEMOTICONS_EXPORT_H

#ifdef KEMOTICONS_STATIC_DEFINE
#  define KEMOTICONS_EXPORT
#  define KEMOTICONS_NO_EXPORT
#else
#  ifndef KEMOTICONS_EXPORT
#    ifdef KF5Emoticons_EXPORTS
        /* We are building this library */
#      define KEMOTICONS_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KEMOTICONS_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KEMOTICONS_NO_EXPORT
#    define KEMOTICONS_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KEMOTICONS_DEPRECATED
#  define KEMOTICONS_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KEMOTICONS_DEPRECATED_EXPORT
#  define KEMOTICONS_DEPRECATED_EXPORT KEMOTICONS_EXPORT KEMOTICONS_DEPRECATED
#endif

#ifndef KEMOTICONS_DEPRECATED_NO_EXPORT
#  define KEMOTICONS_DEPRECATED_NO_EXPORT KEMOTICONS_NO_EXPORT KEMOTICONS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KEMOTICONS_NO_DEPRECATED
#    define KEMOTICONS_NO_DEPRECATED
#  endif
#endif

#endif
