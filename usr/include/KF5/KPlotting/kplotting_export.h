
#ifndef KPLOTTING_EXPORT_H
#define KPLOTTING_EXPORT_H

#ifdef KPLOTTING_STATIC_DEFINE
#  define KPLOTTING_EXPORT
#  define KPLOTTING_NO_EXPORT
#else
#  ifndef KPLOTTING_EXPORT
#    ifdef KF5Plotting_EXPORTS
        /* We are building this library */
#      define KPLOTTING_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KPLOTTING_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KPLOTTING_NO_EXPORT
#    define KPLOTTING_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KPLOTTING_DEPRECATED
#  define KPLOTTING_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KPLOTTING_DEPRECATED_EXPORT
#  define KPLOTTING_DEPRECATED_EXPORT KPLOTTING_EXPORT KPLOTTING_DEPRECATED
#endif

#ifndef KPLOTTING_DEPRECATED_NO_EXPORT
#  define KPLOTTING_DEPRECATED_NO_EXPORT KPLOTTING_NO_EXPORT KPLOTTING_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KPLOTTING_NO_DEPRECATED
#    define KPLOTTING_NO_DEPRECATED
#  endif
#endif

#endif
