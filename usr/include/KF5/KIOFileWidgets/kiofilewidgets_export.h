
#ifndef KIOFILEWIDGETS_EXPORT_H
#define KIOFILEWIDGETS_EXPORT_H

#ifdef KIOFILEWIDGETS_STATIC_DEFINE
#  define KIOFILEWIDGETS_EXPORT
#  define KIOFILEWIDGETS_NO_EXPORT
#else
#  ifndef KIOFILEWIDGETS_EXPORT
#    ifdef KF5KIOFileWidgets_EXPORTS
        /* We are building this library */
#      define KIOFILEWIDGETS_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KIOFILEWIDGETS_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KIOFILEWIDGETS_NO_EXPORT
#    define KIOFILEWIDGETS_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KIOFILEWIDGETS_DEPRECATED
#  define KIOFILEWIDGETS_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KIOFILEWIDGETS_DEPRECATED_EXPORT
#  define KIOFILEWIDGETS_DEPRECATED_EXPORT KIOFILEWIDGETS_EXPORT KIOFILEWIDGETS_DEPRECATED
#endif

#ifndef KIOFILEWIDGETS_DEPRECATED_NO_EXPORT
#  define KIOFILEWIDGETS_DEPRECATED_NO_EXPORT KIOFILEWIDGETS_NO_EXPORT KIOFILEWIDGETS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KIOFILEWIDGETS_NO_DEPRECATED
#    define KIOFILEWIDGETS_NO_DEPRECATED
#  endif
#endif

#endif
