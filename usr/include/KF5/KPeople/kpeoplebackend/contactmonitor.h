/*
    Abstract class to load a monitor changes for a single contact
    Copyright (C) 2013  David Edmundson <davidedmundson@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef CONTACTMONITOR_H
#define CONTACTMONITOR_H

#include <QObject>

#include <kpeoplebackend/kpeoplebackend_export.h>

#include "abstractcontact.h"

namespace KPeople
{

class ContactMonitorPrivate;

/**
 * This class loads data for a single contact from a datasource.
 *
 * Datasources should subclass this and call setContact() when the contact loads or changes.
 * It is used for optimising performance over loading all contacts and filtering the results.
 * Subclasses are expected to be asynchronous in loading data.
 *
 * @since 5.8
 */
class KPEOPLEBACKEND_EXPORT ContactMonitor: public QObject
{
    Q_OBJECT
public:
    ContactMonitor(const QString &contactUri);
    virtual ~ContactMonitor();

    /**
     * The ID of the contact being loaded
     */
    QString contactUri() const;

    /**
     * The currently loaded information on this contact.
     */
    AbstractContact::Ptr contact() const;
Q_SIGNALS:
    /**
     * Emitted whenever the contact changes
     */
    void contactChanged();
protected:
    /**
     * Sets or updates the contact and emits contactChanged
     * Subclasses should call this when data is loaded or changes
     */
    void setContact(const AbstractContact::Ptr &contact);
private:
    Q_DISABLE_COPY(ContactMonitor)
    Q_DECLARE_PRIVATE(ContactMonitor)
    ContactMonitorPrivate *d_ptr;
};

}
typedef QSharedPointer<KPeople::ContactMonitor> ContactMonitorPtr;

#endif // CONTACTMONITOR_H
