
#ifndef KIOCORE_EXPORT_H
#define KIOCORE_EXPORT_H

#ifdef KIOCORE_STATIC_DEFINE
#  define KIOCORE_EXPORT
#  define KIOCORE_NO_EXPORT
#else
#  ifndef KIOCORE_EXPORT
#    ifdef KF5KIOCore_EXPORTS
        /* We are building this library */
#      define KIOCORE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KIOCORE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KIOCORE_NO_EXPORT
#    define KIOCORE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KIOCORE_DEPRECATED
#  define KIOCORE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KIOCORE_DEPRECATED_EXPORT
#  define KIOCORE_DEPRECATED_EXPORT KIOCORE_EXPORT KIOCORE_DEPRECATED
#endif

#ifndef KIOCORE_DEPRECATED_NO_EXPORT
#  define KIOCORE_DEPRECATED_NO_EXPORT KIOCORE_NO_EXPORT KIOCORE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KIOCORE_NO_DEPRECATED
#    define KIOCORE_NO_DEPRECATED
#  endif
#endif

#endif
