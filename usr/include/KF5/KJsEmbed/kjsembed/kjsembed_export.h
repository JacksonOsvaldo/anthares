
#ifndef KJSEMBED_EXPORT_H
#define KJSEMBED_EXPORT_H

#ifdef KJSEMBED_STATIC_DEFINE
#  define KJSEMBED_EXPORT
#  define KJSEMBED_NO_EXPORT
#else
#  ifndef KJSEMBED_EXPORT
#    ifdef KF5JsEmbed_EXPORTS
        /* We are building this library */
#      define KJSEMBED_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KJSEMBED_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KJSEMBED_NO_EXPORT
#    define KJSEMBED_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KJSEMBED_DEPRECATED
#  define KJSEMBED_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KJSEMBED_DEPRECATED_EXPORT
#  define KJSEMBED_DEPRECATED_EXPORT KJSEMBED_EXPORT KJSEMBED_DEPRECATED
#endif

#ifndef KJSEMBED_DEPRECATED_NO_EXPORT
#  define KJSEMBED_DEPRECATED_NO_EXPORT KJSEMBED_NO_EXPORT KJSEMBED_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KJSEMBED_NO_DEPRECATED
#    define KJSEMBED_NO_DEPRECATED
#  endif
#endif

#endif
