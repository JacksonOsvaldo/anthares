
#ifndef KDEWEBKIT_EXPORT_H
#define KDEWEBKIT_EXPORT_H

#ifdef KDEWEBKIT_STATIC_DEFINE
#  define KDEWEBKIT_EXPORT
#  define KDEWEBKIT_NO_EXPORT
#else
#  ifndef KDEWEBKIT_EXPORT
#    ifdef KF5WebKit_EXPORTS
        /* We are building this library */
#      define KDEWEBKIT_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KDEWEBKIT_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KDEWEBKIT_NO_EXPORT
#    define KDEWEBKIT_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KDEWEBKIT_DEPRECATED
#  define KDEWEBKIT_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KDEWEBKIT_DEPRECATED_EXPORT
#  define KDEWEBKIT_DEPRECATED_EXPORT KDEWEBKIT_EXPORT KDEWEBKIT_DEPRECATED
#endif

#ifndef KDEWEBKIT_DEPRECATED_NO_EXPORT
#  define KDEWEBKIT_DEPRECATED_NO_EXPORT KDEWEBKIT_NO_EXPORT KDEWEBKIT_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KDEWEBKIT_NO_DEPRECATED
#    define KDEWEBKIT_NO_DEPRECATED
#  endif
#endif

#endif
