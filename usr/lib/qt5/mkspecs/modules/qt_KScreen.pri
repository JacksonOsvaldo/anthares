QT.KScreen.VERSION = 5.12.1
QT.KScreen.MAJOR_VERSION = 5
QT.KScreen.MINOR_VERSION = 12
QT.KScreen.PATCH_VERSION = 1
QT.KScreen.name = KF5Screen
QT.KScreen.defines = 
QT.KScreen.includes = /usr/include/KScreen
QT.KScreen.private_includes =
QT.KScreen.libs = /usr/lib
QT.KScreen.depends = core
