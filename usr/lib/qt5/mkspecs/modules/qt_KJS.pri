QT.KJS.VERSION = 5.43.0
QT.KJS.MAJOR_VERSION = 5
QT.KJS.MINOR_VERSION = 43
QT.KJS.PATCH_VERSION = 0
QT.KJS.name = KF5JS
QT.KJS.defines = 
QT.KJS.includes = /usr/include/KF5/KJS
QT.KJS.private_includes =
QT.KJS.libs = /usr/lib
QT.KJS.depends = core
