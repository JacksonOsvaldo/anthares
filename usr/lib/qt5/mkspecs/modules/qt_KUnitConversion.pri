QT.KUnitConversion.VERSION = 5.43.0
QT.KUnitConversion.MAJOR_VERSION = 5
QT.KUnitConversion.MINOR_VERSION = 43
QT.KUnitConversion.PATCH_VERSION = 0
QT.KUnitConversion.name = KF5UnitConversion
QT.KUnitConversion.defines = 
QT.KUnitConversion.includes = /usr/include/KF5/KUnitConversion
QT.KUnitConversion.private_includes =
QT.KUnitConversion.libs = /usr/lib
QT.KUnitConversion.depends = core
